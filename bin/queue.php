<?php

require_once '../../../../init.php';

use BlueSky\Framework\Registry\Config;

use BlueSky\Framework\Queue\Consumer;
$config = new Config('master');
if (($argc >= 2) && $config->get('queue.enqueue', false)) {
    $name = $argv[1];
    $consumer = new Consumer($config, $name);
    $consumer->run();
}
