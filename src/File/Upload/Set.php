<?php
namespace BlueSky\Framework\File\Upload;

/**
 * File upload validation set
 * @package BlueSky\Framework\File
 */
class Set
{

    /**
     * The file validators in this set
     * @access private
     * @var array $_validators
     * */
    private $_validators = [];

    /**
     * The errors in this validation set, indexed by the file upload name
     * @access private
     * @var array $_errors
     * */
    private $_errors = [];

    /**
     * Validate the current upload files
     * @access public
     * @return boolean
     * */
    public function validate()
    {
        $result = true;
        /** @var Validator $v */
        foreach($this->_validators as $k=>$v){
            if(!$v->validate()){
                $this->_errors[$k] = $v->getValidationErrors();
                $result = false;
            }
        }
        return $result;
    }

    /**
     * Get the errors for the current validation set
     * @access public
     * @return array
     * */
    public function getErrors()
    {
        return $this->_errors;
    }

    /**
     * Add a file to the validation set
     * @param String $inputName - the key in the $_FILES array for this file
     * @param bool $isMandatory - true if the file upload is mandatory
     * @access public
     * @return Validator
     */
    public function addFile($inputName, $isMandatory = false)
    {
        $v = new Validator($inputName, $isMandatory);
        $this->_validators[$inputName] = $v;
        return $v;
    }

}
