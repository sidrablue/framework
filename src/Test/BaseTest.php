<?php
namespace BlueSky\Framework\Test;

use BlueSky\Framework\State\Base;
use BlueSky\Framework\State\Cli;

require_once __DIR__.'/../../../../../../init.php';

class BaseTest extends \Codeception\Test\Unit
{

    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var Base
     */
    protected $state;

    public function __construct()
    {
        parent::__construct();
        $this->state = new Cli();
        $this->state->getApps();
    }


    protected function _before()
    {
    }

    protected function _after()
    {
    }

    protected function getState()
    {
        return $this->state;
    }

    protected function setState($state)
    {
        $this->state = $state;
    }



}