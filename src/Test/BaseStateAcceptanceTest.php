<?php
namespace BlueSky\Framework\Test;

use BlueSky\Framework\State\Base;
use BlueSky\Framework\State\Cli;

require_once __DIR__.'/../../../../../../init.php';

class BaseStateAcceptanceTest
{
    /**
     * @var Base
     */
    protected $state;


    public function __construct()
    {
        $this->state = new Cli();
        $this->state->getApps();
    }

    protected function getState()
    {
        return $this->state;
    }

    protected function setState($state)
    {
        $this->state = $state;
    }
}