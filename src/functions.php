<?php


/**
 * Get the account reference for this lote framework request
 * @access public
 * @param string $default - the default value
 * @return string
 */
function getLoteAccountRef($default = '')
{
    $result = $default;
    if (getenv('ACCOUNT_REF') && php_sapi_name() != 'cli') {
        $result = getenv('ACCOUNT_REF');
    } elseif (defined('ACCOUNT_BASE_URL') && php_sapi_name() != 'cli') {
        $result = trim(str_replace(ACCOUNT_BASE_URL, '', $_SERVER['HTTP_HOST']), '.');
    } elseif (php_sapi_name() == 'cli') {

        $climate = new League\CLImate\CLImate;
        $climate->arguments->add([
                'reference' => [
                    'prefix' => 'r',
                    'longPrefix' => 'reference',
                    'description' => 'Reference',
                    'required' => false
                ]
            ]
        );
        $climate->arguments->parse();
        if ($mode = $climate->arguments->get("reference")) {
            $result = $mode;
        }
        else {
            $c = new \BlueSky\Framework\Registry\Config();
            if($acc = $c->get("lote.account", false)) {
                $result = $acc;
            }
        }
    } else {
        $c = new \BlueSky\Framework\Registry\Config();
        return $c->get("lote.account");
    }
    return $result;
}

function func_get_args_with_keys(array $args, $class = null, $method = null, $includeOptional = false)
{
    if (is_null($class) || is_null($method)) {
        $trace = debug_backtrace()[1];
        $class = $trace['class'];
        $method = $trace['function'];
    }
    $reflection = new \ReflectionMethod($class, $method);

    if (count($args) < $reflection->getNumberOfRequiredParameters()) {
        throw new \RuntimeException("Something went wrong! We had less than the required number of parameters.");
    }

    foreach ($reflection->getParameters() as $param) {
        if (isset($args[$param->getPosition()])) {
            $args[$param->getName()] = $args[$param->getPosition()];
            unset($args[$param->getPosition()]);
        } else {
            if ($includeOptional && $param->isOptional()) {
                $args[$param->getName()] = $param->getDefaultValue();
            }
        }
    }
    return $args;
}

/**
 * Check if this system is currently running on Windows
 *
 * @return boolean
 * */
function isWindows()
{
    return strtoupper(substr(PHP_OS, 0, 3)) === 'WIN';
}

/**
 * Check if this system is currently running on linux
 *
 * @return boolean
 * */
function isUnix()
{
    return !isWindows();
}

/**
 * Check if this system is currently running on linux
 *
 * @return boolean
 * */
function loteAutoloadAllApps()
{

    return !isWindows();
}

function ddd()
{
    foreach (func_get_args() as $v) {
        dump($v);
    }
    die;
}

function setXDebugLimits($sane = true)
{
    $depth = 4;
    $maxChildren = 256;
    $maxData = 1024;
    if(!$sane) {
        $depth = $maxChildren = $maxData = -1;
    }
    ini_set("xdebug.var_display_max_depth", $depth);
    ini_set("xdebug.var_display_max_children", $maxChildren);
    ini_set("xdebug.var_display_max_data", $maxData);
}

function free(&$var)
{
    if (is_object($var) && method_exists($var, '__destruct')) {
        /** @var object $var */
        $var->__destruct();
    }
    $var = null;
    unset($var);
}
