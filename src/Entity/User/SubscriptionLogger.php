<?php
namespace BlueSky\Framework\Entity\User;

use BlueSky\Framework\Object\Entity\Base;

/**
 * Password class for users
 * @package BlueSky\Framework\Entity\User
 * @dbEntity true
 */
class SubscriptionLogger extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__user_subscription_logger';
    public const TABLE_NAME = 'sb__user_subscription_logger';

    /**
     * @dbColumn user_id
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $user_id - The user ID to which this password belongs
     */
    public $user_id;

    /**
     * @dbColumn channel
     * @fieldType string
     * @dbOptions length=64, notnull = false
     * @dbIndex true
     * @var string $channel - The subscription channel
     */
    public $channel;

    /**
     * @dbColumn subscription_status
     * @fieldType integer
     * @dbOptions default=true
     * @dbIndex true
     * @var int $subscription_status
     */
    public $subscription_status;

    /**
     * @dbColumn is_current
     * @fieldType boolean
     * @dbOptions notnull = false, default=true
     * @dbIndex true
     * @var boolean $is_current - The token for this password
     */
    public $is_current;

    /**
     * @dbColumn description
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @var string $description - The token for this password
     */
    public $description;

    /**
     * @var boolean $isAudited
     * True if this entity is audited
     * */
    protected $isAudited = true;

}
