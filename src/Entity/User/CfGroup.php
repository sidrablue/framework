<?php
namespace BlueSky\Framework\Entity\User;

use BlueSky\Framework\Model\CfGroup as GroupModel;
use BlueSky\Framework\Model\CfGroup as CfGroupModel;
use BlueSky\Framework\Object\Entity\Base;

/**
 * Class CfGroup
 * @package BlueSky\Framework\Entity\User
 * @deprecated to be deleted once EP 2.0 is stable
 */
class CfGroup extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__cf_group';
    public const TABLE_NAME = 'sb__cf_group';


    /**
     * @dbColumn reference
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @var string $reference - the reference of th group
     */
    public $reference;

    /**
     * @dbColumn object_id
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $object_id - the object that the group applies to
     */
    public $object_id;

    /**
     * @dbColumn name
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @var string $name - the name of the field
     */
    public $name;

    /**
     * @dbColumn description
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @var string $description - the description of the field
     */
    public $description;

    /**
     * @dbColumn sort_order
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $sort_order - the sort order of this group for its object
     */
    public $sort_order;

    /**
     * @dbColumn active
     * @fieldType boolean
     * @dbOptions notnull = false, default = false
     * @var boolean $active - True if the group is active
     * @OA\Property(description="Active", title="active")
     */
    public $active = true;

    /**
     * @dbColumn hidden
     * @fieldType boolean
     * @dbOptions notnull = false, default = false
     * @var boolean $hidden - True if the group is hidden
     * @OA\Property(description="Hidden", title="hidden")
     */
    public $hidden = false;

    /**
     * @var boolean $fields - The list of fields that make up this group
     */
    public $fields = [];

    /**
     * @var boolean $childFields - The list of child fields that belong to this group
     */
    public $childFields = [];


    /**
     * @access protected
     * @return void
     * */
   /* protected function beforeCreate()
    {
        $this->privateProperties[] = 'childFields';
        $this->privateProperties[] = 'fields';
    }*/

    /**
     * Set the name of this field
     * @access public
     * @param string $column - the column name
     * @throws \Exception
     * @return void
     */
    public function setName($column)
    {
        if (is_string($column) && preg_match("/^[\w]+$/", $column) > 0) {
            $this->_column = $column;
        } else {
            throw new \Exception('Column name must be alpha numeric');
        }
    }

    /**
     * Set the data for the fields in this group
     * @param array $fieldData
     * @param bool $loadValues
     * @return void
     * */
    public function setFieldData($fieldData, $loadValues = true)
    {
        $this->childFields = [];
        foreach ($fieldData as $d) {
            $f = new CfField($this->getState(), $d);
            if ($loadValues) {
                if(isset($d['value_id'])) {
                    $d['id'] = $d['value_id'];
                }
                $v = new CfValue($this->getState(), $d);
                $v->setTableName(str_replace("_group", "", $this->getTableName()));
                $f->setValueObject($v);
            }
            if ($f->config_data) {
                $f->config_data = json_decode($f->config_data, true);
            }
            $this->childFields[] = $f;
        }
    }

    /**
     * Load all of the fields for this group
     * @access public
     * @return void
     *  */
    public function loadAllFields($active_only = false)
    {
        if ($this->id) {
            $m = new CfGroupModel($this->getState());
            $this->setFieldData($m->getCfFields($this->id, $active_only));
        }
    }

    /**
     * Function to call before inserting data so that data can be manipulated if required
     * @param array $data
     * @return array
     * */
    protected function beforeInsert($data)
    {
        $m = new GroupModel($this->getState());
        $data['sort_order'] = $m->getNextSortOrder($this->object_id);
        return $data;
    }

    /**
     * Function to call before updating data so that data can be manipulated if required
     * @param array $data
     * @return array
     * */
    protected function beforeUpdate($data)
    {
        //unset($data['reference']);
        unset($data['object_id']);
        return $data;
    }

    /**
     * Get the custom fields belonging to this group
     * @access public
     * @return array
     * */
    public function getChildFields()
    {
        return $this->childFields;
    }

    /**
     * Get the exportable custom fields belonging to this group
     * @access public
     * @return array
     * */
    public function getExportFields()
    {
        $result = [];
        foreach ($this->childFields as $v) {
            /**
             * @var CfField $v
             * */
            if ($v->getDataDefinition()->supportsExporting()) {
                $result[] = $v;
            }
        }
        return $result;
    }

}
