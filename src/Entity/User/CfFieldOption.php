<?php
namespace BlueSky\Framework\Entity\User;

use BlueSky\Framework\Object\Data\Field\Factory;
use BlueSky\Framework\Object\Data\Field\Type\Base as BaseFieldType;
use BlueSky\Framework\Object\Entity\Base;
use BlueSky\Framework\Model\CfField as FieldModel;

/**
 * Class CfField
 * @package BlueSky\Framework\Entity\User
 * @deprecated to be deleted once EP 2.0 is stable
 */
class CfFieldOption extends Base
{
    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__cf_field_option';
    public const TABLE_NAME = 'sb__cf_field_option';

    /**
     * @dbColumn field_id
     * @fieldType integer
     * @dbOptions notnull = true, default = 0
     * @var int $field_id - the ID of the field that the option belongs to
     */
    public $field_id;

    /**
     * @dbColumn parent_id
     * @fieldType integer
     * @dbOptions notnull = true, default = 0
     * @var int $parent_id - the ID of the parent option of this option
     */
    public $parent_id;

    /**
     * @dbColumn reference
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @var string $reference - the reference of the field
     */
    public $reference;

    /**
     * @dbColumn value
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @var string $value - The value of the field
     */
    public $value;

    /**
     * @dbColumn sort_order
     * @fieldType integer
     * @dbOptions notnull = false
     * @var int $sort_order - the sort order of this field in its group
     */
    public $sort_order;

    /**
     * @dbColumn weighting
     * @fieldType integer
     * @dbOptions notnull = false
     * @var int $weighting - the weighting of the field used in the sorting of the options
     */
    public $weighting;

    /**
     * @dbColumn direction
     * @fieldType string
     * @dbOptions length = 32, notnull = false
     * @var string $direction - the direction of the option
     */
    public $direction;

    /**
     * @dbColumn selection_limit
     * @fieldType integer
     * @dbOptions notnull = false
     * @var int $selection_limit - the number of times that this option can be selected
     */
    public $selection_limit;

    /**
     * @dbColumn is_selectable
     * @fieldType boolean
     * @dbOptions notnull = false, default = true
     * @var boolean $is_selectable - true if it can be selected
     */
    public $is_selectable;

    /**
     * @dbColumn is_hidden
     * @fieldType boolean
     * @dbOptions notnull = false, default = false
     * @var boolean $is_hidden - true if this field is hidden
     */
    public $is_hidden;

    /**
     * @dbColumn is_active
     * @fieldType boolean
     * @dbOptions notnull = false, default = true
     * @var boolean $is_active - true if this field is active
     */
    public $is_active;

    /**
     * @dbColumn is_correct
     * @fieldType boolean
     * @dbOptions notnull = false, default = false
     * @var boolean $is_correct - true if this field is a correct answer
     */
    public $is_correct;

}
