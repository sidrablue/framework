<?php
namespace BlueSky\Framework\Entity\User;

/**
 * Class CfValueOptionSelected
 * @package BlueSky\Framework\Entity
 * @deprecated to be deleted once EP 2.0 is stable
 */
class FormCfValueOptionSelected extends CfValueOptionSelected
{

    protected $tableName = 'sb_form_record__cf_value_option_selected';
    public const TABLE_NAME = 'sb_form_record__cf_value_option_selected';

}
