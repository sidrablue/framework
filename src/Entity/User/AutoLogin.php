<?php
declare(strict_types=1);

namespace BlueSky\Framework\Entity\User;

use Hashids\Hashids;
use BlueSky\Framework\Object\Entity\Base;

/**
 * @package BlueSky\Framework\Entity\User
 * @dbEntity true
 */
class AutoLogin extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__user_auto_login';
    public const TABLE_NAME = 'sb__user_auto_login';

    /**
     * @dbColumn user_id
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $user_id - The user ID to which this password belongs
     */
    public $user_id;

    /**
     * @dbColumn user_hash
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var string $user_hash - The users hash
     */
    public $user_hash;

    /**
     * @dbColumn login_hash
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var string $login_hash - The login hash
     */
    public $login_hash;

    /**
     * @dbColumn expires
     * @fieldType datetime
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var \DateTime $expires - The expiry of this auto login
     */
    public $expires;

    /**
     * @dbColumn redirect
     * @fieldType string
     * @dbOptions length=1500, notnull = false
     * @var string $redirect - The redirect to do after the login
     */
    public $redirect;

    protected function afterInsertData($data)
    {
        $h = new Hashids();
        $this->user_hash = $h->encode(max(1,($data['user_id'] + $data['id']) % 314159265) );
        $this->login_hash = $h->encode($data['id']);
        $data = ['user_hash' => $this->user_hash, 'login_hash' => $this->login_hash];
        $this->getWriteDb()->update($this->getTableName(), $data, ['id' => $this->id]);
    }

    /**
     * Get the URI for a login link
     * @access public
     * @return string
     * */
    public function getLinkUri()
    {
        return "/_l/{$this->login_hash}/{$this->user_hash}";
    }

    /**
     * Get the URL for a login link
     * @access public
     * @return string
     * */
    public function getLinkUrl()
    {
        if(php_sapi_name()=='cli') {
            $result = $this->getState()->getAccount()->getUrl() . $this->getLinkUri();
        }
        else {
            $result = $this->getState()->getRequest()->getSchemeAndHttpHost() . $this->getLinkUri();
        }
        return $result;
    }

}
