<?php
/**
 * Copyright (c) BlueSky Digital Labs Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Entity\User;

use BlueSky\Framework\Object\Entity\Base;

/**
 * Password class for users
 * @package BlueSky\Framework\Entity\User
 * @dbEntity true
 * @dbUniqueIndex [user_id]
 * @relation from_entity_reference = User, from_field_reference = id, to_field_reference = user_id, type = ONE_TO_ONE, sort_order = 0
 */
class Subscription extends Base
{

    public const ENUM_EMAIL_SUBSCRIBED = 1;
    public const ENUM_EMAIL_PERMANENT_UNSUBSCRIBED = 0;
    public const ENUM_MOBILE_SUBSCRIBED = 1;
    public const ENUM_MOBILE_PERMANENT_UNSUBSCRIBED = 0;

    public const TYPE_EMAIL = 'email';
    public const TYPE_MOBILE = 'mobile';

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__user_subscription';
    public const TABLE_NAME = 'sb__user_subscription';

    /**
     * @var int $id
     * @fieldType primary_key_int
     * @dbOptions autoincrement = true
     * */
    public $id;

    /**
     * @fieldType datetime_immutable
     * @dbOptions notnull = false
     * @var \DateTimeImmutable $lote_created - the time that this row was created. This is a \DateTime compatible 'c' format value
     */
    public $lote_created;

    /**
     * @fieldType datetime
     * @dbOptions notnull = false
     * @var \DateTimeImmutable $lote_updated - the last time that this row was updated. This is a \DateTime compatible 'c' format value
     */
    public $lote_updated;

    /**
     * @dbColumn user_id
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $user_id - The user ID to which this password belongs
     */
    public $user_id;

    /**
     * @dbColumn email
     * @fieldType enum_integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @flags [ FLAG_QUERYABLE, FLAG_IMPORTABLE, FLAG_EXPORTABLE, FLAG_LISTABLE ]
     * @var boolean $email - The subscription of email
     */
    public $email;

    /**
     * @dbColumn mobile
     * @fieldType enum_integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @flags [ FLAG_QUERYABLE, FLAG_IMPORTABLE, FLAG_EXPORTABLE, FLAG_LISTABLE ]
     * @var boolean $mobile - The subscription of mobile
     */
    public $mobile;

}
