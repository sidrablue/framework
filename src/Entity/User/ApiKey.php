<?php

namespace BlueSky\Framework\Entity\User;

use BlueSky\Framework\Object\Entity\Base;

/**
 * Password class for users
 * @package BlueSky\Framework\Entity\User
 * @dbEntity true
 */
class ApiKey extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__user_api_key';
    public const TABLE_NAME = 'sb__user_api_key';

    public const KEY_TYPE_X_API = "x-api";
    public const KEY_TYPE_HMAC = 'hmac';

    /**
     * @dbColumn user_id
     * @fieldType integer
     * @dbOptions notnull = true
     * @dbIndex true
     * @var int $user_id - The user ID to which this password belongs
     */
    public $user_id;

    /**
     * @dbColumn key_type
     * @fieldType string
     * @dbOptions length=64, notnull = false
     * @var string $key_type - The type of key
     */
    public $key_type;

    /**
     * @dbColumn key_value
     * @fieldType string
     * @dbOptions length=64, notnull = false
     * @var string $key - The encoded password
     */
    public $key_value;

    /**
     * @dbColumn key_secret
     * @fieldType string
     * @dbOptions length=64, notnull = false
     * @var string $key - Used for HMAC keys
     */
    public $key_secret;

    /**
     * @dbColumn is_active
     * @fieldType boolean
     * @dbOptions notnull = false, default=false
     * @dbIndex true
     * @var boolean $is_active
     */
    public $is_active;

    /**
     * @dbColumn is_neverexpires
     * @fieldType boolean
     * @dbOptions notnull = false, default=true
     * @dbIndex true
     * @var boolean $is_neverexpires
     */
    public $is_neverexpires;

    /**
     * @dbColumn begin_at
     * @fieldType datetime
     * @dbOptions notnull = false
     * @dbIndex true
     * @var \DateTime $begin_at
     */
    public $begin_at;

    /**
     * @dbColumn end_at
     * @fieldType datetime
     * @dbOptions notnull = false
     * @dbIndex true
     * @var \DateTime $end_at
     */
    public $end_at;

    /**
     * @var boolean $isAudited
     * True if this entity is audited
     * */
    protected $isAudited = true;

    public function generateXApiKey(): string
    {
        $this->key_type = self::KEY_TYPE_X_API;
        $this->key_value = md5(uniqid());
        $this->key_secret = null;
        return $this->key_value;
    }

    public function generateHmac()
    {
        throw new \Exception("HMAC not implemented");
    }

    protected function beforeInsert($data)
    {
        if ($this->key_type != self::KEY_TYPE_X_API) {
            throw new \Exception("Only X-API authentication is currently implemented");
        }
        return $data;
    }

    public function loadByXApiKey($key): int
    {
        $q = $this->getState()->getReadDb()->createQueryBuilder();
        $q->select("k.*")
            ->from(self::TABLE_NAME, "k")
            ->andWhere("k.key_value = :key_value")
            ->setParameter("key_value", $key)
            ->andWhere("k.lote_deleted is null")
            ->andWhere("k.is_active is true")
            ->andWhere("k.is_neverexpires is true or (now() between k.begin_at and k.end_at)");
        if ($data = $q->execute()->fetch()) {
            $this->setData($data);
        }
        return (int)$this->id;
    }

}
