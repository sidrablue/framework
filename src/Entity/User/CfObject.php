<?php
namespace BlueSky\Framework\Entity\User;

use BlueSky\Framework\Object\Entity\Base;

/**
 * Class CfObject
 * @package BlueSky\Framework\Entity\User
 * @deprecated to be deleted once EP 2.0 is stable
 */
class CfObject extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__cf_object';
    public const TABLE_NAME = 'sb__cf_object';

    /**
     * @dbColumn reference
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @var string $reference
     */
    public $reference;

    /**
     * @dbColumn reference_id
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $reference_id - The reference id is form id
     */
    public $reference_id;

    /**
     * @dbColumn name
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @var string $name -the name of the form
     */
    public $name;

    /**
     * @dbColumn description
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @var string $description
     */
    public $description;

    /**
     * @dbColumn display_order
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $display_order
     */
    public $display_order;

    /**
     * @dbColumn active
     * @fieldType boolean
     * @dbOptions notnull = false, default = false
     * @var boolean $active - True if the group is active
     * @OA\Property(description="Active", title="active")
     */
    public $active = true;

    /**
     * @dbColumn hidden
     * @fieldType boolean
     * @dbOptions notnull = false, default = false
     * @var boolean $hidden - True if the group is hidden
     * @OA\Property(description="Hidden", title="hidden")
     */
    public $hidden = false;

    /**
     * @dbColumn is_core
     * @fieldType boolean
     * @dbOptions notnull = false, default = false
     * @var boolean $is_core - true if this is a core object
     * @OA\Property(description="Is Core", title="is_core")
     */
    public $is_core = false;


    /**
     * @var array $groups - The list of groups that make up this object
     * */
    protected $cfGroups = [];

    /**
     * Before create event handler to be implemented in the child
     * @access protected
     * @return void
     * */
   /* protected function beforeCreate()
    {
        $this->privateProperties[] = 'cfGroups';
    }*/

    /**
     * Set the data for the groups in this object
     * @param array $groupData
     * @return void
     * */
    public function setGroupData($groupData)
    {
        $this->cfGroups = [];
        foreach ($groupData as $d) {
            $g = new CfGroup($this->getState(), $d);
            $g->setFieldData($g);
            $this->cfGroups[] = $g;
        }
    }

    /**
     * Get the groups in this object
     * @return array
     * */
    public function getGroups()
    {
        return $this->cfGroups;
    }

    /**
     * Get the groups in this object
     * @return array
     * */
    public function getActiveGroups()
    {
        $result = [];
        foreach ($this->cfGroups as $g) {
            if ($g->active) {
                $result[] = $g;
            } else {
                //dump('inactive, inactive');
                //dump($g);
            }
        }
        return $result;
    }

    /**
     * Function to call before inserting data so that data can be manipulated if required
     * @param array $data
     * @return array
     * */
    protected function beforeInsert($data)
    {
        $data['display_order'] = $this->getNextSortOrder();
        return $data;
    }

    /**
     * Get the next sort order for an object
     * @access public
     * @return int
     * */
    public function getNextSortOrder()
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select('max(o.display_order) + 1')
            ->from('sb__cf_object', 'o');
        return max(1, $data->execute()->fetchColumn());
    }

    /**
     * Function to call before updating data so that data can be manipulated if required
     * @param array $data
     * @return array
     * */
    protected function beforeUpdate($data)
    {
        unset($data['is_core']);
        unset($data['user_id']);
        return $data;
    }

    /**
     * Load the object by its ID or reference
     * @param int|string $id
     * @param bool $loadGroups - true if the groups for this object are to be loaded
     * @param bool $includeDeleted - Whether or not to include objects that have been deleted
     * @return Object
     * @todo - create a prent function where you can pass in a field to load by, instead of the ID
     */
    public function load($id, $loadGroups = false, $includeDeleted = false, $active_only = false)
    {
        if (is_numeric($id)) {
            parent::load($id, $includeDeleted);
        } elseif (is_string($id) && $id) {
            $q = $this->getReadDb()->createQueryBuilder();
            $q->select('t.*')
                ->from($this->getTableName(), 't')
                ->where('reference = :reference')
                ->setParameter('reference', $id);

            if (!$includeDeleted) {
                $q->andWhere('lote_deleted is null');
            }
            if ($active_only) {
                $this->loadGroups("t.active = 1");
            }

            $s = $q->execute();
            $this->setData($s->fetch(\PDO::FETCH_ASSOC));
        }
        if ($loadGroups) {
            $this->loadGroups($active_only);
        }
        return $this->id;
    }

    /**
     * Load the object by its reference and reference ID or reference
     * @param string $reference
     * @param int $referenceId
     * @param bool $loadGroups - true if the groups for this object are to be loaded
     * @return Object
     */
    public function loadByReferences($reference, $referenceId, $loadGroups = false)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('t.*')
            ->from($this->getTableName(), 't')
            ->where('reference = :reference')
            ->setParameter('reference', $reference)
            ->andWhere('reference_id = :reference_id')
            ->setParameter('reference_id', $referenceId);
        $s = $q->execute();
        $this->setData($s->fetch(\PDO::FETCH_ASSOC));
        if ($loadGroups) {
            $this->loadGroups();
        }
        return $this->id;
    }

    public function loadGroups($active_only = false)
    {
        if ($this->id) {
            $m = new \BlueSky\Framework\Model\CfObject($this->getState());
            $groups = $m->getCfGroups($this->id);
            foreach ($groups as $group) {
                $g = new CfGroup($this->getState());
                $g->setData($group);
                $g->loadAllFields($active_only);
                if ($active_only) {
                    if ($g->active == 1) {
                        $this->cfGroups[] = $g;
                    }
                } else {
                    $this->cfGroups[] = $g;
                }
            }
        }
    }

    /**
     * Retrieves map of all custom fields tied to this CfObject
     *
     * @param bool $groupByGroups - Groups fields within their CfGroups in the resultant array
     */
    public function getCustomFieldsMap($groupByGroups = false)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('f.*')
            ->from($this->getTableName(), 't')
            ->leftJoin('t', 'sb__cf_group', 'g', 'g.object_id = t.id')
            ->leftJoin('g', 'sb__cf_field', 'f', 'f.group_id = g.id')
            ->andWhere('t.id = :id')
            ->setParameter('id', $this->id)
            ->andWhere('t.lote_deleted is null')
            ->andWhere('g.lote_deleted is null')
            ->andWhere('f.lote_deleted is null')
            ->orderBy('g.sort_order, f.sort_order');
        $result = $q->execute();
        $data = [];

        if ($groupByGroups) {
            $cfg = new CfGroup($this->getState());
            $cfGroups = $cfg->listByField('object_id', $this->id);
            $data = array_combine(array_column($cfGroups, 'id'), $cfGroups);
            $data = array_map(function ($a) {
                $a['_fields'] = [];
                return $a;
            }, $data);

            while ($row = $result->fetch(\PDO::FETCH_ASSOC)) {
                $data[$row['group_id']]['_fields'][$row['reference']] = $row;
            }
        } else {
            while ($row = $result->fetch(\PDO::FETCH_ASSOC)) {
                $data[$row['reference']] = $row;
            }
        }

        return $data;
    }
    
}
