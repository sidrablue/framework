<?php
namespace BlueSky\Framework\Entity\User;

use Lote\App\MasterBase\Entity\Master\Account;
use BlueSky\Framework\Object\Entity\BaseExtensible;
use BlueSky\Framework\Service\Timezone;
use BlueSky\Framework\Util\Strings;
use Lote\App\Content\Model\Snippet;

/**
 * Base class for users
 * @package BlueSky\Framework\Entity\User
 * @dbEntity true
 *
 * @OA\Schema(
 *     description="User",
 *     title="User",
 * )
 */
class User extends BaseExtensible
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__user';
    public const TABLE_NAME = 'sb__user';

    /**
     * @var int $id
     * The id of the table row
     * @dbColumn id
     * @fieldType primary_key_int
     * @dbOptions autoincrement = true
     * @flags FLAG_LISTABLE
     * @customName User Id
     * */
    public $id;

    /**
     * @dbColumn lote_created
     * @fieldType datetime_immutable
     * @dbOptions notnull = false
     * @dbIndex = true
     * @customName Created Date
     * @flags [ FLAG_QUERYABLE, FLAG_LISTABLE ]
     * @var \DateTimeImmutable $lote_created - the time that this row was created. This is a \DateTime compatible 'c' format value
     */
    public $lote_created;

    /**
     * @dbColumn lote_updated
     * @fieldType datetime
     * @dbOptions notnull = false
     * @dbIndex = true
     * @customName Updated Date
     * @flags [ FLAG_QUERYABLE, FLAG_LISTABLE ]
     * @var \DateTimeImmutable $lote_updated - the last time that this row was updated. This is a \DateTime compatible 'c' format value
     */
    public $lote_updated;

    /**
     * @dbColumn username
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @dbIndex true
     * @flags [ FLAG_QUERYABLE, FLAG_IMPORTABLE, FLAG_EXPORTABLE, FLAG_LISTABLE ]
     * @var string $username - the users username
     * @OA\Property(description="Username", title="username")
     */
    public $username;

    /**
     * @dbColumn email
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @dbIndex true
     * @flags [ FLAG_QUERYABLE, FLAG_IMPORTABLE, FLAG_EXPORTABLE, FLAG_LISTABLE ]
     * @var string $email - The users email
     * @OA\Property(description="Email", title="email")
     */
    public $email;

    /**
     * @dbColumn mobile
     * @fieldType string
     * @dbOptions length = 64, notnull = false
     * @dbIndex true
     * @flags [ FLAG_QUERYABLE, FLAG_IMPORTABLE, FLAG_EXPORTABLE, FLAG_LISTABLE ]
     * @var string $mobile - The users mobile
     * @OA\Property(description="Mobile", title="mobile")
     */
    public $mobile;

    /**
     * @dbColumn domain
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @dbIndex true
     * @flags [ FLAG_QUERYABLE, FLAG_IMPORTABLE, FLAG_EXPORTABLE, FLAG_LISTABLE ]
     * @var string $domain - The users domain
     * @OA\Property(description="domain", title="domain")
     */
    public $domain;

    /**
     * @dbColumn title
     * @fieldType string
     * @dbOptions length = 64, notnull = false
     * @dbIndex true
     * @flags [ FLAG_QUERYABLE, FLAG_IMPORTABLE, FLAG_EXPORTABLE, FLAG_LISTABLE ]
     * @var string $title - The users title, such as Mr, Mrs, Miss
     * @OA\Property(description="Title", title="title")
     */
    public $title;

    /**
     * @dbColumn first_name
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @dbIndex true
     * @flags [ FLAG_QUERYABLE, FLAG_IMPORTABLE, FLAG_EXPORTABLE, FLAG_LISTABLE ]
     * @var string $first_name - The users first name
     * @OA\Property(description="First Name", title="first_name")
     */
    public $first_name;

    /**
     * @dbColumn middle_name
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @dbIndex true
     * @flags [ FLAG_QUERYABLE, FLAG_IMPORTABLE, FLAG_EXPORTABLE, FLAG_LISTABLE ]
     * @var string $middle_name - The users middle name
     * @OA\Property(description="Middle Name", title="middle_name")
     */
    public $middle_name;

    /**
     * @dbColumn last_name
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @dbIndex true
     * @flags [ FLAG_QUERYABLE, FLAG_IMPORTABLE, FLAG_EXPORTABLE, FLAG_LISTABLE ]
     * @var string $last_name - The users last name
     * @OA\Property(description="Last Name", title="last_name")
     */
    public $last_name;

    /**
     * @dbColumn preferred_greeting
     * @fieldType string
     * @dbOptions length = 64, notnull = false
     * @dbIndex true
     * @flags [ FLAG_QUERYABLE, FLAG_IMPORTABLE, FLAG_EXPORTABLE, FLAG_LISTABLE ]
     * @var string $preferred_greeting - The users preferred greeting, such as "Howdy", "Hi", "Hello"
     * @OA\Property(description="Preferred Greeting", title="preferred_greeting")
     */
    public $preferred_greeting;

    /**
     * @dbColumn preferred_name
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @dbIndex true
     * @flags [ FLAG_QUERYABLE, FLAG_IMPORTABLE, FLAG_EXPORTABLE, FLAG_LISTABLE ]
     * @var string $preferred_name - The users preferred name
     * @OA\Property(description="Preferred Name", title="preferred_name")
     */
    public $preferred_name;

    /**
     * @dbColumn phone
     * @fieldType string
     * @dbOptions length = 64, notnull = false
     * @dbIndex true
     * @flags [ FLAG_QUERYABLE, FLAG_IMPORTABLE, FLAG_EXPORTABLE, FLAG_LISTABLE ]
     * @var string $phone - The users phone
     * @OA\Property(description="Phone", title="phone")
     */
    public $phone;

    /**
     * @dbColumn extension
     * @fieldType string
     * @dbOptions length = 8, notnull = false
     * @dbIndex true
     * @flags [ FLAG_QUERYABLE, FLAG_IMPORTABLE, FLAG_EXPORTABLE ]
     * @var string $extension - extension
     * @OA\Property(description="Extension", title="extension")
     */
    public $extension;

    /**
     * @dbColumn company_name
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @dbIndex true
     * @flags [ FLAG_QUERYABLE, FLAG_IMPORTABLE, FLAG_EXPORTABLE, FLAG_LISTABLE ]
     * @var string $company_name - The users company name
     * @OA\Property(description="Company Name", title="company_name")
     */
    public $company_name;

    /**
     * @dbColumn position
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @dbIndex true
     * @flags [ FLAG_QUERYABLE, FLAG_IMPORTABLE, FLAG_EXPORTABLE, FLAG_LISTABLE ]
     * @var string $position - The users job title
     * @OA\Property(description="Position", title="position")
     */
    public $position;

    /**
     * @dbColumn date_of_birth
     * @fieldType date
     * @dbOptions notnull = false
     * @dbIndex true
     * @flags [ FLAG_QUERYABLE, FLAG_IMPORTABLE, FLAG_EXPORTABLE, FLAG_LISTABLE ]
     * @var \DateTime $date_of_birth - Date of birth
     * @OA\Property(description="Date of Birth", title="date_of_birth")
     */
    public $date_of_birth;

    /**
     * @dbColumn gender
     * @fieldType string
     * @dbOptions length = 32, notnull = false
     * @dbIndex true
     * @flags [ FLAG_QUERYABLE, FLAG_IMPORTABLE, FLAG_EXPORTABLE, FLAG_LISTABLE ]
     * @var boolean $gender - gender
     * @OA\Property(description="Gender", title="gender")
     */
    public $gender;

    /**
     * @dbColumn is_admin
     * @fieldType boolean
     * @dbOptions notnull = false, default = false
     * @dbIndex true
     * @flags [ FLAG_QUERYABLE, FLAG_EXPORTABLE, FLAG_LISTABLE ]
     * @var boolean $is_admin - True if the user is an admin
     * @OA\Property(description="Is Admin", title="is_admin")
     */
    public $is_admin;

    /**
     * @dbColumn is_owner
     * @fieldType boolean
     * @dbOptions notnull = false, default = false
     * @var boolean $is_owner - True if the user is the owner
     * @OA\Property(description="Is Owner", title="is_owner")
     */
    public $is_owner;

    /**
     * @dbColumn is_active
     * @fieldType boolean
     * @dbOptions notnull = false, default = false
     * @dbIndex true
     * @flags [ FLAG_QUERYABLE, FLAG_IMPORTABLE, FLAG_EXPORTABLE, FLAG_LISTABLE ]
     * @var boolean $is_active - True if the user is active
     * @OA\Property(description="Is Active", title="is_active")
     */
    public $is_active;

    /**
     * @dbColumn timezone_id
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @var int $timezone_id - The users default timezone ID
     * @OA\Property(description="Timezone ID", title="timezone_id")
     */
    public $timezone_id;

    /**
     * @dbColumn token
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @var string $token - The encryption token
     * @OA\Property(description="Token", title="token")
     */
    public $token;

    /**
     * @dbColumn external_id
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @dbIndex true
     * @flags [ FLAG_QUERYABLE, FLAG_IMPORTABLE, FLAG_EXPORTABLE, FLAG_LISTABLE ]
     * @var string $external_id - External reference id
     * @OA\Property(description="External ID", title="external_id")
     */
    public $external_id;

    /**
     * @dbColumn master_id
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var string $master_id - Master ID
     * @OA\Property(description="Master ID", title="master_id")
     */
    public $master_id;

    /**
     * @dbColumn source
     * @fieldType string
     * @dbOptions length = 32, notnull = false, default = local
     * @dbIndex true
     * @flags [ FLAG_QUERYABLE, FLAG_EXPORTABLE, FLAG_LISTABLE ]
     * @var string $source - the source of this user, such as 'local', 'google', 'facebook', etc.
     * @OA\Property(description="Source", title="source")
     */
    public $source;

    /**
     * @dbColumn locality
     * @fieldType string
     * @dbOptions length = 32, notnull = false
     * @dbIndex true
     * @flags [ FLAG_QUERYABLE, FLAG_IMPORTABLE, FLAG_EXPORTABLE, FLAG_LISTABLE ]
     * @var string $locality - the region for which the user is active
     * @OA\Property(description="Locality", title="locality")
     */
    public $locality;

    /**
     * @dbColumn code
     * @fieldType string
     * @dbOptions length = 32, notnull = false
     * @dbIndex true
     * @flags [ FLAG_QUERYABLE, FLAG_IMPORTABLE, FLAG_EXPORTABLE, FLAG_LISTABLE ]
     * @var string $code
     */
    public $code;

    /**
     * @var boolean $isAudited
     * True if this entity is audited
     * @OA\Property(description="Is Audited", title="is_audited")
     * */
    protected $isAudited = true;

    ///// MOVE TO MODEL ///////////
    /**
     * Load a user by their email
     * @param string $email
     * @param boolean $includeDeleted
     * @return boolean
     * */
    public function loadByEmail($email, $includeDeleted = false): bool
    {
        return $this->loadByField('email', $email, $includeDeleted);
    }

    /**
     * Load a user by their email or mobile
     * @param string $email
     * @param string $mobile
     * @param bool $includeDeleted
     * @return boolean
     * */
    public function loadByEmailOrMobile(?string $email, ?string $mobile, bool $includeDeleted = false): bool
    {
        // Validate arguments
        $email = filter_var($email, FILTER_VALIDATE_EMAIL);
        $mobile = preg_match("/[\+0-9]{0,1}([0-9]*)/", $mobile) ? $mobile : null;

        if (empty($email) && !empty($mobile)) {
            $this->loadByField('mobile', $mobile, $includeDeleted);
        } elseif (empty($mobile) && !empty($email)) {
            $this->loadByEmail($email, $includeDeleted);
        } elseif (!empty($mobile) && !empty($email)) {
            $this->loadByFields(
                [
                    [
                        'mobile' => $mobile,
                        'email' => $email
                    ]
                ],
                $includeDeleted
            );
        }

        return $this->id > 0;
    }

    /**
     * Load a user by their email or mobile
     * @param string $mobile
     * @param boolean $includeDeleted
     * @return boolean
     * */
    public function loadByMobileAndNoEmail($mobile, $includeDeleted = false): bool
    {
        return $this->loadByFields(
            [
                'mobile' => $mobile,
                [
                    'email' => null,
                    '_email' => ''
                ]
            ],
            $includeDeleted
        );
    }

    /**
     * Load a user by their username
     * @param string $username
     * @return boolean
     * */
    public function loadByUsername($username): bool
    {
        $output = false;
        if (!empty($username)) {
            $output = $this->loadByField('username', $username);
        }
        return $output;
    }

    /**
     * Load a user by their username
     * @param string $username
     * @return boolean
     * */
    public function loadForLogin($username): bool
    {
        return $this->loadByFields(
            [
                [
                    'username' => $username,
                    'email' => $username
                ]
            ]
        );
    }

///// MOVE TO MODEL ///////////

    /**
     * Helper function to get the name of the current user
     * */
    public function getName()
    {
        $result = '';
        if ($this->first_name && $this->last_name) {
            $result = "{$this->first_name} {$this->last_name}";
        } elseif ($this->first_name) {
            $result = $this->first_name;
        } elseif ($this->last_name) {
            $result = $this->last_name;
        }
        return $result;
    }

    /**
     * Function to call before inserting data so that data can be manipulated if required
     * @param array $data
     * @return array
     * */
    protected function beforeInsert($data)
    {

        $this->token = $data['token'] = uniqid();
        $isAdmin = $this->getState()->getUser()->isAdmin();
        if(!isset($data['is_admin']) || $data['is_admin'] !='1' || !$isAdmin) {
            $data['is_admin'] = 0;
        }
        if(!isset($data['is_owner']) || $data['is_owner'] !='1') {
            $data['is_owner'] = 0;
        }
        if(!isset($data['is_active']) || $data['is_active'] !='0') {
            $data['is_active'] = 1;
        }
        if(isset($data['email']) && trim($data['email']) == '') {
            $data['email'] = null;
        }
        if(isset($data['mobile']) && trim($data['mobile']) == '') {
            $data['mobile'] = null;
        }
        elseif(isset($data['mobile']) && $data['mobile'] != '') {
            $data['mobile'] = Strings::removeSpaces($data['mobile']);
        }
        return $data;
    }

    /**
     * Function to call before inserting data so that data can be manipulated if required
     * @param array $data
     * @return array
     * */
    protected function beforeUpdate($data)
    {
        $isAdmin = $this->getState()->getUser()->isAdmin();
        if(isset($data['is_admin']) && $data['is_admin'] && !$isAdmin) {
            $data = false;
        } else {
            if(array_key_exists('confirm_password', $data)) {
                unset($data['confirm_password']);
            }
            if(isset($data['email']) && trim($data['email']) == '') {
                $data['email'] = null;
            }
            if(isset($data['mobile']) && trim($data['mobile']) == '') {
                $data['mobile'] = null;
            }
            elseif(isset($data['mobile']) && $data['mobile'] != '') {
                $data['mobile'] = Strings::removeSpaces($data['mobile']);
            }
        }

        return $data;
    }

    /**
     * Check if this user is an admin
     * */
    public function isAdmin()
    {
        return $this->is_admin;
    }

    /**
     * Get the core list of fields for this database
     * @todo - potentially look at making this dynamic in some way
     * @deprecated this will be deleted soon
     * @return array
     * */
    public function getCoreFields()
    {
        $fields = [];
        $fields['first_name'] = 'First Name';
        $fields['last_name'] = 'Last Name';
        $fields['email'] = 'Email';
        $fields['mobile'] = 'Mobile';
        $fields['title'] = 'Title';
        $fields['middle_name'] = 'Middle Name';
        $fields['preferred_name'] = 'Preferred Name';
        $fields['preferred_greeting'] = 'Preferred Greeting';
        $fields['username'] = 'Username';
        $fields['password'] = 'Password';
//        $fields['email_secondary'] = 'Secondary Email';
        $fields['company_name'] = 'Company Name';
        $fields['position'] = 'Job Title';
        $fields['phone'] = 'Phone';
        $fields['active'] = 'Active';
        $fields['address_location'] = "Location Address";
        $fields['address_street'] = "Street Address";
        $fields['address_street2'] = "Street Address 2";
        $fields['address_city'] = "City/Suburb";
        $fields['address_state'] = "State";
        $fields['address_postcode'] = "Postcode";
        $fields['address_country'] = "Country";
        $fields['subscribed'] = "Subscription Status";
        $fields['is_suppressed'] = "Is Suppressed";
//        $fields['code'] = "Code";
        $fields['lote_created'] = "Created";
        $fields['lote_updated'] = "Updated";
        return $fields;
    }

    public function getWildcardValues(
        $wildcardTypes = [
            'user',
            'custom_field',
            'edm',
            'date',
            'content_holder',
            'setting',
            'account'
        ]
    )
    {
        $wildcards = [];

        $data = $this->getData();
        if (in_array('user', $wildcardTypes))
        {
            foreach($data as $k => $v) {
                if ($k[0] !== '_') {
                    if ($v instanceof \DateTimeInterface) {
                        $v = $v->format("c");
                    }
                    $wildcards["%%$k%%"] = is_scalar($v)?$v:"";
                }
            }
           if(in_array(strtolower($data['gender']),['m','male'])){
               $wildcards['%%gender_pronoun%%'] = "His";
           }else if(in_array(strtolower($data['gender']),['f','female'])){
                $wildcards['%%gender_pronoun%%'] = "Her";
            }else {
               $wildcards['%%%%gender_pronoun%%'] = 'Their';
           }

        }

        if (in_array('custom_field', $wildcardTypes))
        {
            foreach($data as $k => $v) {
                if ($k[0] === '_' && !Strings::startsWith("__", $k)) {
                    $internalData = $v;
                    if ($internalData !== null) {
                        foreach ($internalData as $ik => $iv) {
                            if ($iv instanceof \DateTimeInterface) {
                                $iv = $iv->format("c");
                            }
                            $wildcards["%%__cf__{$k}__{$ik}%%"] = $iv;
                        }
                    }
                }
            }
        }

        if(in_array('date', $wildcardTypes))
        {
            $ts = new Timezone($this->getState());
            $wildcards["%%__date_MdY__%%"] = $ts->convertToDisplay(date("Y-m-d H:i:s"), 'F jS Y');
            $wildcards["%%__date_mdY__%%"] = $ts->convertToDisplay(date("Y-m-d H:i:s"), 'M jS Y');
            $wildcards["%%__date_MD__%%"] = $ts->convertToDisplay(date("Y-m-d H:i:s"), 'F j');
            $wildcards["%%__date_dMY__%%"] = $ts->convertToDisplay(date("Y-m-d H:i:s"), 'jS F Y');
            $wildcards["%%__date_dmY__%%"] = $ts->convertToDisplay(date("Y-m-d H:i:s"), 'jS M Y');
            $wildcards["%%__date_dmy__%%"] = $ts->convertToDisplay(date("Y-m-d H:i:s"), 'd/m/y');
            $wildcards["%%__date_dmY__%%"] = $ts->convertToDisplay(date("Y-m-d H:i:s"), 'd/m/Y');
            $wildcards["%%__date_mdy__%%"] = $ts->convertToDisplay(date("Y-m-d H:i:s"), 'm/d/y');
            $wildcards["%%__date_mdY__%%"] = $ts->convertToDisplay(date("Y-m-d H:i:s"), 'm/d/Y');
            $wildcards["%%__date_year__%%"] = $ts->convertToDisplay(date("Y-m-d H:i:s"), 'Y');
            $wildcards["%%__date_y__%%"] = $ts->convertToDisplay(date("Y-m-d H:i:s"), 'y');
            $wildcards["%%__date_month__%%"] = $ts->convertToDisplay(date("Y-m-d H:i:s"), 'm');
            $wildcards["%%__date_m0__%%"] = $ts->convertToDisplay(date("Y-m-d H:i:s"), 'n');
            $wildcards["%%__date_M__%%"] = $ts->convertToDisplay(date("Y-m-d H:i:s"), 'F');
            $wildcards["%%__date_m__%%"] = $ts->convertToDisplay(date("Y-m-d H:i:s"), 'M');
            $wildcards["%%__date_d00__%%"] = $ts->convertToDisplay(date("Y-m-d H:i:s"), 'd');
            $wildcards["%%__date_d0__%%"] = $ts->convertToDisplay(date("Y-m-d H:i:s"), 'j');
            $wildcards["%%__date_dth__%%"] = $ts->convertToDisplay(date("Y-m-d H:i:s"), 'jS');
            $wildcards["%%__date_day__%%"] = $ts->convertToDisplay(date("Y-m-d H:i:s"), 'l');
            $wildcards["%%__date_d__%%"] = $ts->convertToDisplay(date("Y-m-d H:i:s"), 'D');
        }

        if(in_array('content_holder', $wildcardTypes))
        {
            // get content holders
            $ch = new Snippet($this->getState());
            $contentHolders = $ch->getList(1, [], [], 'and', 1000);
            if(isset($contentHolders['rows']))
            {
                foreach($contentHolders['rows'] as $contentHolder)
                {
                    $wildcards["%%__content_snippet_{$contentHolder['id']}__%%"] = $contentHolder['content'];
                }
            }
        }

        if(in_array('setting', $wildcardTypes))
        {
            if(isset($settings['rows']))
            {
                foreach($this->getState()->getSettings()->getAll() as $s)
                {
                    $value = $this->getState()->getSettings()->get($s['reference']);
                    $wildcards["%%__setting_{$s['reference']}__%%"] = $value;
                }
            }
        }

        if(in_array('account', $wildcardTypes))
        {
            $columns = [
                'domain',
                //'website', handled below
                'name',
                'trading_name',
                'business_number',
                'business_number_type',
                'timezone',
                'phone',
                'email',
                'contact_first_name',
                'contact_last_name',
                'street',
                'city',
                'postcode',
                'state',
                'country',
                'fax',
                'mobile'
            ];

            if($this->getState()->getMasterDb()) {
                $account = new Account($this->getState());
                $account->loadByField('reference', $this->getState()->getSettings()->get('system.reference'));

                if (!empty($account->name)) {
                    foreach ($columns as $property) {
                        $wildcards["%%__account_{$property}__%%"] = $account->$property;
                    }

                    // Create website wildcard variants
                    foreach (['website', 'website_secondary'] as $websiteProp) {
                        $host = '';
                        $url = '';
                        if (!empty($account->{$websiteProp})) {
                            $urlParts = parse_url($account->{$websiteProp});
                            if (isset($urlParts['host'])) {
                                $host = $urlParts['host'];
                                $url = $urlParts['scheme'] . "://" . $host;
                            } elseif (isset($urlParts['path'])) {
                                $host = explode('/', $urlParts['path'])[0];
                                $url = "http://" . $host;
                            }
                        }
                        $wildcards["%%__account_{$websiteProp}__%%"] = $url;
                        $wildcards["%%__account_{$websiteProp}_html_link__%%"] = "<a href='$url'>$host</a>";
                        $wildcards["%%__account_{$websiteProp}_display__%%"] = $host;
                    }
                }
            }
        }

        // These are added OUTSIDE of this function
/*        $wildcards["%%__view_in_browser__%%"] = 'View in Browser Link';
        $wildcards["%%__unsubscribe_link__%%"] = 'Unsubscribe Link';
        $wildcards["%%__manage_profile_link__%%"] = 'Manage Profile Link';*/

        $result = [];
        foreach($wildcards as $k=>$v) {
            if(is_scalar($k) && is_scalar($v)) {
                $result[$k] = $v;
            }
        }
        return $result;
    }
}
