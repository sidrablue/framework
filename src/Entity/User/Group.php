<?php
/**
 * Copyright (c) BlueSky Digital Labs Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Entity\User;

use BlueSky\Framework\Object\Entity\Base;

/**
 * Class UserGroup
 * @package BlueSky\Framework\Entity\User
 * @dbEntity true
 */
class Group extends Base
{
    public const SUBSCRIPTION_STATUS_PERMANENTLY_UNSUBSCRIBED = -1;
    public const SUBSCRIPTION_STATUS_UNSUBSCRIBED = 0;
    public const SUBSCRIPTION_STATUS_SUBSCRIBED = 1;
    public const SUBSCRIPTION_STATUS_SUBSCRIBED_DAILY = 2;
    public const SUBSCRIPTION_STATUS_SUBSCRIBED_WEEKLY = 3;
    public const SUBSCRIPTION_STATUS_SUBSCRIBED_MONTHLY = 4;

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__user_group';
    public const TABLE_NAME = 'sb__user_group';

    /**
     * @dbColumn user_id
     * @fieldType integer
     * @dbOptions notnull = true
     * @dbIndex true
     * @var int $user_id - The user ID of this group membership
     */
    public $user_id;

    /**
     * @dbColumn group_id
     * @fieldType integer
     * @dbOptions notnull = true
     * @dbIndex true
     * @var int $group_id - The group ID of this group membership
     */
    public $group_id;

    /**
     * @dbColumn is_approved
     * @fieldType boolean
     * @dbOptions notnull = false, default=true
     * @dbIndex true
     * @var boolean $is_approved - true if this membership status is approved
     */
    public $is_approved;

    /**
     * @dbColumn approved_by_user_id
     * @fieldType integer
     * @dbOptions notnull = true, default = 0
     * @var int $approved_by_user_id - User id of whom the entity was approved
     */
    public $approved_by_user_id;

    /**
     * @dbColumn subscription_status
     * @fieldType integer
     * @dbOptions notnull = true, default = 1
     * @var int $subscription_status - Subscription status
     */
    public $subscription_status;

}
