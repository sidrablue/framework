<?php
namespace BlueSky\Framework\Entity\User;

use BlueSky\Framework\Model\CfField as CfFieldModel;
use BlueSky\Framework\Model\CfFieldOption as CfFieldOptionModel;
use BlueSky\Framework\Object\Data\Field\Factory;
use BlueSky\Framework\Object\Data\Field\Type\Base as BaseFieldType;
use BlueSky\Framework\Object\Data\Field\Type\BaseOption;
use BlueSky\Framework\Object\Entity\Base;
use BlueSky\Framework\Model\CfField as FieldModel;
use BlueSky\Framework\Util\Strings;

/**
 * Class CfField
 * @package BlueSky\Framework\Entity\User
 * @deprecated to be deleted once EP 2.0 is stable
 */
class CfField extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__cf_field';
    public const TABLE_NAME = 'sb__cf_field';



    /**
     * @dbColumn reference
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @var string $reference - the reference of th group
     */
    public $reference;

    /**
     * @dbColumn db_field
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @var string $db_field - the db field to which this input matches
     */
    public $db_field;

    /**
     * @dbColumn class
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @var string $class - the class of the input field
     */
    public $class;

    /**
     * @dbColumn group_id
     * @fieldType integer
     * @dbOptions notnull = true, default = 0
     * @var int $group_id - the ID of the group that the field belongs to
     */
    public $group_id;

    /**
     * @dbColumn name
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @var string $name - the name of the field
     */
    public $name;

    /**
     * @dbColumn description
     * @fieldType text
     * @dbOptions notnull = false
     * @var string $description - the description of the field
     */
    public $description;

    /**
     * @dbColumn help_text
     * @fieldType text
     * @dbOptions notnull = false
     * @var string $help_text - The help text of the field
     */
    public $help_text;

    /**
     * @dbColumn default_value
     * @fieldType text
     * @dbOptions notnull = false
     * @var string $default_value - The help text of the field
     */
    public $default_value;

    /**
     * @dbColumn field_type
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @var string $field_type - the field type, e.g. 'string', 'text', 'int.array', etc...
     * This field should be overwritten in the child class
     */
    public $field_type;

    /**
     * @dbColumn is_assessable
     * @fieldType boolean
     * @dbOptions notnull = false, length = 1
     * @var boolean $is_assessable - true if it is an assessable field
     */
    public $is_assessable;

    /**
     * @dbColumn read_only
     * @fieldType boolean
     * @dbOptions notnull = false, default = false
     * @var boolean $read_only - defines if a field is read only or not
     */
    public $read_only;

    /**
     * @dbColumn is_unique
     * @fieldType boolean
     * @dbOptions notnull = false, default = false
     * @var boolean $is_unique - defines if a field is unique or not
     */
    public $is_unique;

    /**
     * @dbColumn mandatory
     * @fieldType boolean
     * @dbOptions notnull = false, default = false
     * @var boolean $mandatory - true if the field is a mandatory one
     */
    public $mandatory;

    /**
     * @dbColumn hide_label
     * @fieldType boolean
     * @dbOptions notnull = false, default = false
     * @var boolean $hide_label - true if the field label is hidden
     */
    public $hide_label;

    /**
     * @dbColumn active
     * @fieldType boolean
     * @dbOptions notnull = false, default = true
     * @var boolean $active - true if this field is active
     */
    public $active;

    /**
     * @dbColumn sort_order
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $sort_order - the sort order of this group for its object
     */
    public $sort_order;

    /**
     * @dbColumn config_data
     * @fieldType text
     * @dbOptions notnull = false
     * @param array $config_data
     */
    public $config_data;

    /**
     * @dbColumn hidden
     * @fieldType boolean
     * @dbOptions notnull = false, default = false
     * @var boolean $hidden - true if this field is hidden
     * @OA\Property(description="Hidden", title="hidden")
     */
    public $hidden;

    /**
     * @dbColumn source
     * @fieldType text
     * @dbOptions length=255, notnull = false
     * @var string $source - Script to apply to the field
     */
    public $source;

    /**
     * @dbColumn storage_type
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @var string $storage_type - storage type
     */
    public $storage_type;

    /**
     * @var CfValue $valueObject
     */
    protected $valueObject;

    /**
     * @var BaseFieldType $dataDefinition
     */
    protected $dataDefinition;

    /**
     * @access protected
     * @return void
     * */
   /* protected function beforeCreate()
    {
        $this->privateProperties[] = 'valueObject';
        $this->privateProperties[] = 'dataDefinition';
    }*/

    /**
     * Set the name of this field
     * @access public
     * @param string $column - the column name
     * @throws \Exception
     * @return void
     */
    public function setName($column)
    {
        if (is_string($column) && preg_match("/^[\w]+$/", $column) > 0) {
            $this->_column = $column;
        } else {
            throw new \Exception('Column name must be alpha numeric');
        }

    }

    /**
     * Check if this field has a default value
     * @access public
     * @return boolean
     * */
    public function hasDefault()
    {
        return isset($this->default_value);
    }

    public function setValueObject(CfValue $obj = null)
    {
        $this->valueObject = $obj;
        $this->setupDataDefinition();
        $this->dataDefinition->setConfigData($this->config_data);
    }

    /**
     * Setup the data definition
     * @access private
     * @return void
     * */
    private function setupDataDefinition()
    {
        $this->dataDefinition = Factory::createInstance($this->getState(), $this->field_type);
        $this->dataDefinition->setValueObject($this->valueObject);
        $this->dataDefinition->setValueFromObject($this->valueObject);
        $this->dataDefinition->setConfigData($this->config_data);
        $this->dataDefinition->definition = $this;
    }

    /**
     * Get the export columns for this cfField
     * */
    public function getExportColumns()
    {
        return $this->dataDefinition->getExportColumns($this);
    }

    /**
     * Get the export cells for this cfField
     * */
    public function getExportCells($data)
    {
        return $this->dataDefinition->getExportCells($this, $data);
    }


    /**
     * @param boolean $autoLoad
     * @return BaseFieldType
     * */
    public function getDataDefinition($autoLoad = false)
    {
        if ($autoLoad && !$this->dataDefinition) {
            $this->setupDataDefinition();
        }
        return $this->dataDefinition;
    }

    /**
     * @return mixed
     * */
    public function getDataValue()
    {
        $result = null;
        if ($this->dataDefinition) {
            $result = $this->dataDefinition->getValue();
        }
        return $result;
    }

    /**
     * @return CfValue
     * */
    public function getValueObject()
    {
        return $this->valueObject;
    }

    /**
     * Function to call before inserting data so that data can be manipulated if required
     * @param array $data
     * @return array
     * */
    protected function beforeInsert($data)
    {
        $m = new FieldModel($this->getState());
        if (!isset($data['sort_order']) || !is_numeric($data['sort_order'])) {
            $data['sort_order'] = $m->getNextSortOrder($this->group_id);
        }
        return $data;
    }

    /**
     * Get the options for this field
     * @access public
     * @return array|false
     * */
    public function getOptions()
    {
        $m = new CfFieldOptionModel($this->getState());

        $sortOrder = BaseOption::SORT_OPTIONS_DEFAULT;
        if ($sortOptionsOrder = $this->getConfigOption("sort_options_order", false)) {
            $m = new CfFieldOptionModel($this->getState());
            $sortOrder = $sortOptionsOrder;
            // TODO this if block needs to be deleted after all display_alphabetically references are removed from all projects
            if ($displayAlphabetically = $this->getConfigOption('display_alphabetically', false)) {
                if (boolval($displayAlphabetically)) {
                    $sortOrder = BaseOption::SORT_OPTIONS_ALPHA_ASC;
                }
            }
        } elseif ($displayAlphabetically = $this->getConfigOption('display_alphabetically', false)) {
            if (boolval($displayAlphabetically)) {
                $sortOrder = BaseOption::SORT_OPTIONS_ALPHA_ASC;
            }
        }
        return $m->getFieldOptions($this->id, $sortOrder);
    }

    /**
     * Check if this field is an option type field
     * @access public
     * @return bool
     * */
    public function isOptionTypeField()
    {
        $m = new CfFieldModel($this->getState());
        return array_search($this->field_type, $m->getOptionFieldTypes()) !== false;
    }

    /**
     * Get a config option as specified for this field
     * @access public
     * @param string $optionKey - the option
     * @param mixed|null $default - the default return value
     * @return mixed
     * */
    public function getConfigOption($optionKey, $default = null)
    {
        $result = $default;
        if ($configData = $this->config_data) {
            if (Strings::isJson($configData)) {
                $configData = json_decode($configData, true);
            }
            if (is_array($configData) && isset($configData[$optionKey])) {
                $result = $configData[$optionKey];
            }
        }
        return $result;
    }

}
