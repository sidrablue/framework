<?php
namespace BlueSky\Framework\Entity\User;

use BlueSky\Framework\Object\Entity\Base;

/**
 * Password class for users
 * @package BlueSky\Framework\Entity\User
 * @dbEntity true
 */
class TrustedDevices extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__user_devices';
    public const TABLE_NAME = 'sb__user_devices';

    /**
     * @dbColumn user_id
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $user_id - The user ID to which this password belongs
     */
    public $user_id;

    /**
     * @dbColumn token
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var string $token - The token for this device
     */
    public $token;
}
