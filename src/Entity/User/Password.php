<?php
namespace BlueSky\Framework\Entity\User;

use BlueSky\Framework\Object\Entity\Base;

/**
 * Password class for users
 * @package BlueSky\Framework\Entity\User
 * @dbEntity true
 */
class Password extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__user_password';
    public const TABLE_NAME = 'sb__user_password';

    /**
     * @dbColumn user_id
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $user_id - The user ID to which this password belongs
     */
    public $user_id;

    /**
     * @dbColumn password
     * @fieldType string
     * @dbOptions length=64, notnull = false
     * @var string $password - The encoded password
     */
    public $password;

    /**
     * @dbColumn sequence
     * @fieldType integer
     * @dbOptions notnull = false
     * @var string $sequence - The encoded password
     */
    public $sequence;

    /**
     * @dbColumn token
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var string $token - The token for this password
     */
    public $token;

    /**
     * @dbColumn is_current
     * @fieldType boolean
     * @dbOptions notnull = false, default=false
     * @dbIndex true
     * @var boolean $is_current - The token for this password
     */
    public $is_current;

    /**
     * @var boolean $isAudited
     * True if this entity is audited
     * */
    protected $isAudited = true;

    /**
     * Function to call before inserting data so that data can be manipulated if required
     * @param array $data
     * @return array
     * */
    protected function beforeInsert($data)
    {
        if($data['password'] && !empty($data['password'])) {
            if(!( strlen($data['password'])==60 && strpos($data['password'], '$2y$10$')===0)) {
                $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
            }
        }
        return $data;
    }

    /**
     * Function to call before updating data so that data can be manipulated if required
     * @param array $data
     * @return array
     * */
    protected function beforeUpdate($data)
    {
        if($data['password'] && !empty($data['password'])) {
            if(!( strlen($data['password'])==60 && strpos($data['password'], '$2y$10$')===0)) {
                $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
            }
        }
        return $data;
    }
}
