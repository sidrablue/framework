<?php
namespace BlueSky\Framework\Entity\User;

use BlueSky\Framework\Object\Entity\Base;

/**
 * Class CfValueOptionSelected
 * @package BlueSky\Framework\Entity
 * @deprecated to be deleted once EP 2.0 is stable
 */
class CfValueOptionSelected extends Base
{

    protected $tableName = 'sb__user__cf_value_option_selected';
    public const TABLE_NAME = 'sb__user__cf_value_option_selected';

    /**
     * @dbColumn value_id
     * @fieldType integer
     * @dbOptions length = 11, notnull = false
     * @dbIndex true
     * @var int $value_id - the ID of the group that the field belongs to
     */
    public $value_id;

    /**
     * @dbColumn option_id
     * @fieldType integer
     * @dbOptions length = 11, notnull = false
     * @dbIndex true
     * @var string $option_id - the id of the option that was selected
     */
    public $option_id;

    /**
     * @dbColumn option_id_col
     * @fieldType integer
     * @dbOptions length = 11, notnull = false
     * @dbIndex true
     * @var integer $option_id_col - the id of the option column that was selected for a 2D selection
     */
    public $option_id_col;

    /**
     * @dbColumn value_string
     * @fieldType string
     * @dbOptions length = 1500, notnull = false
     * @var string $description - The description of the field
     */
    public $value_string;

    /**
     * @dbColumn value_string_col
     * @fieldType string
     * @dbOptions length = 1500, notnull = false
     * @var string $value_string_col - The value of the field
     */
    public $value_string_col;

    /**
     * @dbColumn value_number
     * @fieldType decimal
     * @dbOptions precision = 10, scale = 0, notnull = false
     * @var double $value_number - Number value of field
     */
    public $value_number;

    /**
     * @dbColumn value_date
     * @fieldType datetime
     * @dbOptions notnull = false
     * @var \DateTime $value_date - Date value of field
     */
    public $value_date;

    /**
     * @dbColumn value_text
     * @fieldType text
     * @dbOptions notnull = false
     * @var string $value_text - Text value of the field
     */
    public $value_text;

    /**
     * @dbColumn value_boolean
     * @fieldType boolean
     * @dbOptions length = 1, notnull = false
     * @var string $value_boolean - Boolean value of field
     */
    public $value_boolean;

}
