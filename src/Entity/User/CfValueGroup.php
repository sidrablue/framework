<?php
namespace BlueSky\Framework\Entity\User;

use BlueSky\Framework\Object\Entity\Base;

/**
 * Class CfValueGroup
 * @package BlueSky\Framework\Entity
 * @deprecated to be deleted once EP 2.0 is stable
 */
class CfValueGroup extends Base
{

    protected $tableName = 'sb__user__cf_value_group';
    public const TABLE_NAME = 'sb__user__cf_value_group';

    /**
     * @dbColumn object_id
     * @fieldType integer
     * @dbOptions length = 11, notnull = false
     * @dbIndex true
     * @var int $object_id - the id of the object that the value belongs to
     */
    public $object_id;

    /**
     * @dbColumn group_id
     * @fieldType integer
     * @dbOptions length = 11, notnull = false
     * @dbIndex true
     * @var int $group_id - the ID of the group that this object is linked to
     */
    public $group_id;

    /**
     * @dbColumn active
     * @fieldType boolean
     * @dbOptions default = 1, length = 1, notnull = false
     * @var boolean $active - true if this field is active
     */
    public $active;

    /**
     * @dbColumn hidden
     * @fieldType boolean
     * @dbOptions default = 0, length = 1, notnull = false
     * @var boolean $hidden - true if this field is hidden
     */
    public $hidden;

    /**
     * Deletes this value group entry and all custom values that belong to it.
     *
     * @param bool|false $strongDelete
     * @return bool
     */
    public function delete($strongDelete = false)
    {
        // Delete field values belonging to this group
        $fieldTable = str_replace("__cf_value_group", "__cf_value", $this->tableName);
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('tv.id')
            ->from($fieldTable, 'tv')
            ->leftJoin('tv', 'sb__cf_field', 'f', 'tv.field_id = f.id')
            ->leftJoin('f', 'sb__cf_group', 'g', 'f.group_id = g.id')
            ->leftJoin('g', $this->getTableName(), 'tg', 'tg.group_id = g.id')
            ->where('tg.id = :id')
            ->setParameter('id', $this->id)
            ->andWhere('tg.object_id = :obj_id')
            ->andWhere('tv.object_id = :obj_id')
            ->setParameter('obj_id', $this->object_id)
            ->andWhere('tv.lote_deleted is null')
            ->andWhere('f.lote_deleted is null')
            ->andWhere('g.lote_deleted is null')
            ->andWhere('tg.lote_deleted is null');

        $data = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
        $e = new CfValue($this->getState());
        $e->setTableName($fieldTable);
        foreach($data as $cfv) {
            $e->load($cfv['id']);
            $e->delete($strongDelete);
        }

        // Delete self
        return parent::delete($strongDelete);
    }

    /**
     * Load an entity by its reference
     * @access public
     * @param string $objectRef
     * @param int $objectId
     * @param string $objectKey
     * @return void
     */
    public function loadByGroupAndObject($objectRef, $objectId, $objectKey = '')
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('*')
            ->from($this->getTableName(), 't')
            ->where('t.object_ref = :object_ref')
            ->setParameter('object_ref', $objectRef)
            ->where('t.object_id = :object_id')
            ->setParameter('object_id', $objectId);
        if($objectKey) {
            $q->where('t.object_key = :object_key')
                ->setParameter('object_key', $objectKey);
        }
        $s = $q->execute();
        if($row = $s->fetch(\PDO::FETCH_ASSOC)) {
            $this->setData($row);
        }
    }

}
