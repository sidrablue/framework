<?php
namespace BlueSky\Framework\Entity;
use BlueSky\Framework\Object\Entity\Base;

/**
 * Setting class
 * @package BlueSky\Framework\Entity
 * @dbEntity true
 */
class Setting extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__setting';
    public const TABLE_NAME = 'sb__setting';

    /**
     * @dbColumn object_ref
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var string $object_ref
     */
    public $object_ref;

    /**
     * @dbColumn object_id
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var string $object_id
     */
    public $object_id;

    /**
     * @dbColumn object_key
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var string $object_key
     */
    public $object_key;

    /**
     * @dbColumn parent_id
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $parent_id - The ID of the parent setting
     */
    public $parent_id;

    /**
     * @dbColumn app_ref
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var string $app_ref
     */
    public $app_ref;

    /**
     * @dbColumn setting_type
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @var string $setting_type - The setting type
     */
    public $setting_type;

    /**
     * @dbColumn reference
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var string $reference - The setting reference
     */
    public $reference;

    /**
     * @dbColumn title
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @var string $title - The setting title
     */
    public $title;

    /**
     * @dbColumn description
     * @fieldType text
     * @dbOptions notnull = false
     * @var string $description - The setting description
     */
    public $description;

    /**
     * @dbColumn value_config
     * @fieldType text
     * @dbOptions notnull = false
     * @var string $value_config - Configuration data for this field
     */
    public $value_config;

    /**
     * @dbColumn value_default
     * @fieldType text
     * @dbOptions notnull = false
     * @var string $value_default - The default value
     */
    public $value_default;

    /**
     * @dbColumn value_custom
     * @fieldType text
     * @dbOptions notnull = false
     * @var string $value_custom - The custom value of the setting
     */
    public $value_custom;

    /**
     * @dbColumn is_active
     * @fieldType boolean
     * @dbOptions notnull = false, default=true
     * @dbIndex true
     * @var boolean $is_active - Whether or not the setting is active
     */
    public $is_active;

    /**
     * @dbColumn is_hidden
     * @fieldType boolean
     * @dbOptions notnull = false, default=true
     * @dbIndex true
     * @var boolean $hidden - Whether or not the setting is hidden from the user
     */
    public $is_hidden;

    /**
     * @dbColumn is_locked
     * @fieldType boolean
     * @dbOptions notnull = false, default=true
     * @dbIndex true
     * @var boolean $locked - Whether or not the setting is locked from editing
     */
    public $is_locked;

    /**
     * @dbColumn is_content_accessible
     * @fieldType boolean
     * @dbOptions notnull = false, default = false
     * @dbIndex true
     * @var boolean $is_content_accessible - Whether or not the setting is available in the WYSIWYG editor
     */
    public $is_content_accessible;

    /**
     * Overwrite the save function
     * @todo - implement duplicate check
     * @return int
     * */
    public function save()
    {
        //check if the setting exists, and if it doesn't then save it
        return parent::save();
    }

    /**
     * @return boolean
     * */
    public function isWritable()
    {
        $result = $this->getState()->getUser()->isAdmin() || $this->lote_access == 1;
        if(!$result) {
            $result = $this->getState()->getUser()->id && ($this->lote_access === '0' || $this->lote_access === 0);
            if(!$result && $this->lote_access == -1 && $this->id) {
                $q = $this->getReadDb()->createQueryBuilder();
                $q->select('count(*)')
                    ->from('sb__auth_access_entity', 'sbe')
                    ->leftJoin('sbe', 'sb__role', 'sbe_r', 'sbe_r.id = sbe.access_type_id')
                    ->leftJoin('sbe_r', 'sb__user_role', 'sbe_u', 'sbe_u.role_id = sbe_r.id')
                    ->andWhere('sbe.object_ref = :table_name')
                    ->andWhere('sbe_u.user_id = :user_id')
                    ->andWhere('sbe.object_id = :entity_id')
                    ->andWhere('sbe.access_level = "write"');
                $q->setParameter('table_name', $this->getTableName());
                $q->setParameter('entity_id', $this->id);
                $q->setParameter('user_id', $this->getState()->getUser()->id);
                $result = $q->execute()->fetchColumn();
            }
        }
        return $result;
    }

}
