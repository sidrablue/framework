<?php

namespace BlueSky\Framework\Entity\Enqueue;

use BlueSky\Framework\Object\Entity\Base;

/**
 * Enqueue
 * @package BlueSky\Framework\Entity\Enqueue
 * @dbEntity true
 * @OA\Schema(
 *     description="Enqueue entries",
 *     title="Enqueue history log",
 * )
 */
class Enqueue extends Base
{
    public const TABLE_NAME = 'sb__enqueue';

    /**
     * @dbColumn id
     * @fieldType string
     * @dbOptions notnull = false, length=255
     * @dbIndex true
     */
    public $id;

    /**
     * @dbColumn published_at
     * @fieldType bigint
     * @dbOptions notnull = false
     * @dbIndex true
     * @OA\Property(description="published_at", title="published_at")
     */
    public $published_at;

    /**
     * @dbColumn body
     * @fieldType text
     * @dbOptions notnull = false
     * @OA\Property(description="body", title="body")
     */
    public $body = '';

    /**
     * @dbColumn properties
     * @fieldType text
     * @dbOptions notnull = false
     * @OA\Property(description="properties", title="properties")
     */
    public $properties = '';

    /**
     * @dbColumn headers
     * @fieldType text
     * @dbOptions notnull = false
     * @OA\Property(description="headers", title="headers")
     */
    public $headers = '';

    /**
     * @dbColumn redelivered
     * @fieldType boolean
     * @dbOptions notnull = false
     * @dbIndex true
     * @OA\Property(description="redelivered", title="redelivered")
     */
    public $redelivered;

    /**
     * @dbColumn queue
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @OA\Property(description="queue", title="queue")
     */
    public $queue;

    /**
     * @dbColumn priority
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @OA\Property(description="priority", title="priority")
     */
    public $priority;

    /**
     * @dbColumn delayed_until
     * @fieldType bigint
     * @dbOptions notnull = false
     * @dbIndex true
     * @OA\Property(description="delayed_until", title="delayed_until")
     */
    public $delayed_until;

    /**
     * @dbColumn time_to_live
     * @fieldType bigint
     * @dbOptions notnull = false
     * @dbIndex true
     * @OA\Property(description="time_to_live", title="time_to_live")
     */
    public $time_to_live;

    /**
     * @dbColumn delivery_id
     * @fieldType string
     * @dbOptions notnull = false, length=255
     * @dbIndex true
     * @OA\Property(description="delivery_id", title="delivery_id")
     */
    public $delivery_id;

    /**
     * @dbColumn redeliver_after
     * @fieldType bigint
     * @dbOptions notnull = false
     * @dbIndex true
     * @OA\Property(description="redeliver_after", title="redeliver_after")
     */
    public $redeliver_after;
}
