<?php

namespace BlueSky\Framework\Entity\Enqueue;

/**
 * Enqueue
 * @package BlueSky\Framework\Entity\Enqueue
 * @dbEntity true
 * @OA\Schema(
 *     description="Enqueue entries",
 *     title="Enqueue history log",
 * )
 */
class EnqueueHistory extends Enqueue
{
    public const TABLE_NAME = 'sb__enqueue_history';

    /**
     * The id of the table row
     * @fieldType primary_key_int
     * @flags FLAG_EXPORTABLE
     * @dbOptions autoincrement = true
     * @var int $id
     * */
    public $id;

    /**
     * @dbColumn original_id
     * @fieldType string
     * @dbOptions notnull = false, length=255
     * @dbIndex true
     * @OA\Property(description="original_id", title="original_id")
     */
    public $original_id;

    /**
     * @dbColumn processed_at
     * @fieldType datetime
     * @dbOptions notnull = false
     * @dbIndex true
     * @OA\Property(description="processed_at", title="processed_at")
     */
    public $processed_at;

    /**
     * @dbColumn status
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @OA\Property(description="status", title="status")
     */
    public $status;

    /**
     * @dbColumn status_message
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @OA\Property(description="status_message", title="status_message")
     */
    public $status_message;
}
