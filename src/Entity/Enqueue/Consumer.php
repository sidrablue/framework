<?php

namespace BlueSky\Framework\Entity\Enqueue;

use BlueSky\Framework\Object\Entity\Base;

/**
 * Enqueue
 * @package BlueSky\Framework\Entity\Enqueue
 * @dbEntity true
 */
class Consumer extends Base
{
    public const TABLE_NAME = 'sb__enqueue_consumer';

    /**
     * @dbColumn hostname
     * @fieldType string
     * @dbOptions notnull = false, length=255
     */
    public $hostname = '';

    /**
     * @dbColumn pid
     * @fieldType integer
     * @dbOptions notnull = false
     */
    public $pid = '';

    /**
     * @dbColumn name
     * @fieldType string
     * @dbOptions notnull = false, length=255
     */
    public $name = '';

    /**
     * @dbColumn status
     * @fieldType string
     * @dbOptions notnull = false, length=255
     */
    public $status = '';

    /**
     * @dbColumn created_at
     * @fieldType datetime
     * @dbOptions notnull = false
     * @dbIndex true
     */
    public $created_at;

    /**
     * @dbColumn active_at
     * @fieldType datetime
     * @dbOptions notnull = false
     * @dbIndex true
     */
    public $active_at;

    /**
     * @dbColumn is_terminated
     * @fieldType boolean
     * @dbOptions notnull = false, default=true
     * @var boolean $is_terminated
     */
    public $is_terminated;

    /**
     * @dbColumn terminated_at
     * @fieldType datetime
     * @dbOptions notnull = false
     * @dbIndex true
     */
    public $terminated_at;
}
