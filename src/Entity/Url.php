<?php
namespace BlueSky\Framework\Entity;

use BlueSky\Framework\Object\Entity\Base;

/**
 * Url class
 * @package BlueSky\Framework\Entity
 * @dbEntity true
 */
class Url extends Base
{

    /**
     * @var string $tableName
     * */
    protected $tableName = 'sb__url';
    public const TABLE_NAME = 'sb__url';

    /**
     * @dbColumn namespace
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var string $namespace
     */
    public $namespace;

    /**
     * @dbColumn object_ref
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var string $object_ref
     */
    public $object_ref;

    /**
     * @dbColumn object_id
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var string $object_id
     */
    public $object_id;

    /**
     * @dbColumn object_key
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var string $object_key
     */
    public $object_key;

    /**
     * @dbColumn uri
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var string $uri - The uri value of the URL
     */
    public $uri;

    /**
     * @dbColumn title
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @var string $title - The title of URL
     */
    public $title;

    /**
     * @dbColumn keywords
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @var string $keywords - The keywords of URL
     */
    public $keywords;

    /**
     * @dbColumn is_active
     * @fieldType boolean
     * @dbOptions notnull = false, default=true
     * @var bool $is_active - Whether or not the URL is active
     */
    public $is_active;

    /**
     * @dbColumn is_published
     * @fieldType boolean
     * @dbOptions notnull = false, default=true
     * @var boolean $is_published - When the URL is published
     */
    public $is_published;

    /**
     * @dbColumn datetime_from
     * @fieldType datetime
     * @dbOptions length=255, notnull = false
     * @var string $date_from - When the URL becomes active
     */
    public $datetime_from;

    /**
     * @dbColumn datetime_to
     * @fieldType datetime
     * @dbOptions length=255, notnull = false
     * @var string $date_to - When the URL expires
     */
    public $datetime_to;

    /**
     * @dbColumn data
     * @fieldType text
     * @dbOptions notnull = false
     * @var string $data - Additional Data
     */
    public $data;

    /**
     * @dbColumn site_id
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var string $site_id
     */
    public $site_id;

    /**
     * Load the entity by its object details
     * @param $objectRef
     * @param $objectId
     * @param int $siteId
     * @param bool $includeDeleted
     * @return boolean
     */
    public function loadByObject($objectId, $objectRef, $siteId = 0, $includeDeleted=false)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('*')
            ->from($this->getTableName(), 't')
            ->andWhere('t.object_ref = :object_ref')
            ->setParameter('object_ref', $objectRef)
            ->andWhere('t.object_id = :object_id')
            ->setParameter('object_id', $objectId);
        if (!$includeDeleted) {
            $q->andWhere('lote_deleted is null');
        }
        if($siteId) {
            $q->where('t.site_id = :site_id')
                ->setParameter('site_id', $siteId);
        }
        $s = $q->execute();
        if($row = $s->fetch(\PDO::FETCH_ASSOC)) {
            $this->setData($row);
        }
        return $this->id;
    }

}
