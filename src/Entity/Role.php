<?php
namespace BlueSky\Framework\Entity;

use BlueSky\Framework\Object\Entity\Base;

/**
 * Role class
 * @package BlueSky\Framework\Entity
 * @dbEntity true
 *
 * @SWG\Definition(required={"tableName", "name", "description", "is_active"}, type="object", @SWG\Xml(name="Role"))
 */

class Role extends Base
{
    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__role';
    public const TABLE_NAME = 'sb__role';
    /**
     * @fieldType string
     * @dbIndex true
     * @dbOptions length=255, notnull = false
     * @var string $name - the role name
     * @SWG\Property()
     * @var string
     */
    public $name;

    /**
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var string $external_id - This is the id on the external (3rd party system) api integration
     * @OA\Property(description="external_id", title="external_id")
     */
    public $external_id;

    /**
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @var string $description - description of the role
     * @SWG\Property()
     * @var string
     */
    public $description;

    /**
     * @fieldType boolean
     * @dbOptions notnull = false, default=true
     * @dbIndex true
     * @var boolean $is_active - True if the role is active
     * @SWG\Property()
     * @var bool
     */
    public $is_active;
}
