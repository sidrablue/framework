<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 */

declare(strict_types=1);

namespace BlueSky\Framework\Entity;

use BlueSky\Framework\Model\Import as ImportModel;
use BlueSky\Framework\Object\Entity\Base;

/**
 * Base class for all entities
 * @dbEntity true
 */
class Import extends Base implements QueueJobInterface
{

    /**
     * @var string $tableName
     * */
    protected $tableName = 'sb__import';
    public const TABLE_NAME = 'sb__import';

    /**
     * @dbColumn object_id
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var string $object_id - the reference of the field
     */
    public $object_id;

    /**
     * @dbColumn file_id
     * @fieldType fkey
     * @dbIndex true
     * @dbOptions notnull = false
     * @var int $file_id - the ID of the file that was uploaded for the import
     */
    public $file_id;

    /**
     * @dbColumn status
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @dbIndex true
     * @var string $status - The name of the field
     */
    public $status;

    /**
     * @dbColumn subscribed_status_email
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @dbIndex true
     * @var string $subscribed_status_email - The subscription email status of the import
     */
    public $subscribed_status_email;
    /**
     * @dbColumn subscribed_status_mobile
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @dbIndex true
     * @var string $subscribed_status_mobile - The subscription mobile status of the import
     */
    public $subscribed_status_mobile;

    /**
     * @dbColumn skip_first_row
     * @fieldType boolean
     * @dbOptions notnull = false
     * @var boolean $skip_first_row - true to skip the first row of an import file
     */
    public $skip_first_row;

    /**
     * @dbColumn field_map
     * @fieldType json
     * @dbOptions notnull = false
     * @var array $field_map - The field map for this import
     */
    public $field_map;

    /**
     * @dbColumn mode
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @dbIndex true
     * @var string $mode - The mode of the import, such as 'update', 'ignore', 'replace'
     */
    public $mode;

    /**
     * @dbColumn groups
     * @fieldType json
     * @dbOptions notnull = false
     * @var string $groups - The list of groups to import into
     */
    public $groups;

    /**
     * @dbColumn total_processed
     * @fieldType integer
     * @dbOptions notnull = false, defaults = 0
     * @var int $total_processed - the total number of rows processed for the file
     */
    public $total_processed;

    /**
     * @dbColumn total_inserted
     * @fieldType integer
     * @dbOptions default = 0, notnull = false
     * @var int $total_inserted - the total number of rows inserted for the file
     */
    public $total_inserted;

    /**
     * @dbColumn total_updated
     * @fieldType integer
     * @dbOptions default = 0, notnull = false
     * @var int $total_updated - the total number of rows updated for the file
     */
    public $total_updated;

    /**
     * @dbColumn total_skipped
     * @fieldType integer
     * @dbOptions default = 0, notnull = false
     * @var int $total_skipped - the total number of rows that were skipped, generally due to being empty
     */
    public $total_skipped;

    /**
     * @dbColumn total_error
     * @fieldType integer
     * @dbOptions default = 0, notnull = false
     * @var int $total_error - the total number of rows that resulted in errors
     */
    public $total_error;

    /**
     * @dbColumn total_ignored
     * @fieldType integer
     * @dbOptions default = 0, notnull = false
     * @var int $total_ignored - the total number of rows ignored for the file
     */
    public $total_ignored;

    /**
     * @dbColumn total_lines
     * @fieldType integer
     * @dbOptions default = 0, notnull = false
     * @var int $total_lines - the total number of lines for the file
     */
    public $total_lines;

    /**
     * Check if this import is still incomplete
     * @access public
     * @return boolean
     * */
    public function isIncomplete()
    {
        return $this->status === 'new' || $this->status === 'processing' || $this->status === 'suspended';
    }

    /**
     * Add to the inserted count for this import
     * @access public
     * @param boolean $clearError - true if this is a resubmission to the import and thus the removal of an error
     * @return void
     * */
    public function addInsertedCount($clearError = false)
    {
        $this->total_inserted++;
        if($clearError) {
            $this->total_error = max(0, $this->total_error - 1);
        }
    }

    /**
     * Add to the updated count for this import
     * @access public
     * @param boolean $clearError - true if this is a resubmission to the import and thus the removal of an error
     * @return void
     * */
    public function addUpdatedCount($clearError = false)
    {
        $this->total_updated++;
        if($clearError) {
            $this->total_error = max(0, $this->total_error - 1);
        }
    }

    /**
     * Add to the ignored count for this import
     * @access public
     * @param boolean $clearError - true if this is a resubmission to the import and thus the removal of an error
     * @return void
     * */
    public function addIgnoredCount($clearError = false)
    {
        $this->total_ignored++;
        if($clearError) {
            $this->total_error = max(0, $this->total_error - 1);
        }
    }

    /**
     * Add to the skipped count for this import
     * @access public
     * @param boolean $clearError - true if this is a resubmission to the import and thus the removal of an error
     * @return void
     * */
    public function addSkippedCount($clearError = false)
    {
        $this->total_skipped++;
        if($clearError) {
            $this->total_error = max(0, $this->total_error - 1);
        }
    }

    /**
     * QueueJobInterface Implementation
     */

    public function startJob()
    {
        // Set status using Queue status options
        $this->status = Queue::STATUS_PROCESSING;
        $this->save();
        // Enqueue job
        $m = new ImportModel($this->getState());
        $m->queueJob($this->id, $this->mode);
    }

    public function stopJob()
    {
        // Set status using Queue status options
        $this->status = Queue::STATUS_SUSPENDED;
        $this->save();
    }

    public function cancelJob()
    {
        // Set status using Queue status options
        $this->status = Queue::STATUS_CANCELLED;
        $this->save();
    }

}