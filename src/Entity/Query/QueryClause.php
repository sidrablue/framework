<?php
namespace BlueSky\Framework\Entity\Query;

use BlueSky\Framework\Object\Entity\Base;

/**
 * Base class for QueryClause
 * @package BlueSky\Framework\Entity\User
 * @dbEntity true
 *
 * @OA\Schema(
 *     description="QueryClause",
 *     title="QueryClause",
 * )
 */
class QueryClause extends Base
{
    /**
     * @var string $tableName
     * */
    protected $tableName = 'sb__query_clause';

    public const TABLE_NAME = 'sb__query_clause';

    /**
     * @var boolean $isAudited
     * True if this entity is audited
     * @OA\Property(description="isAudited", title="is_audited")
     * */
    protected $isAudited = true;

    /**
     * @dbColumn query_id
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $query_id - query id
     * @OA\Property(description="query id", title="query_id")
     */
    public $query_id;

    /**
     * @dbColumn parent_id
     * @fieldType integer
     * @dbOptions notnull = true, default = 0
     * @var int $parent_id - parent id
     * @OA\Property(description="parent id", title="parent_id")
     */
    public $parent_id;

    /**
     * @dbColumn field_reference
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @var string $field_reference - the query field
     * @OA\Property(description="field reference", title="field_reference")
     */
    public $field_reference;

    /**
     * @dbColumn field_type
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @var string $field_type - the query field type
     * @OA\Property(description="field type", title="field_type")
     */
    public $field_type;

    /**
     * @dbColumn entity_reference
     * @fieldType string
     * @dbOptions length = 100, notnull = false
     * @var string $entity_reference - the query entity reference
     * @OA\Property(description="entity reference", title="entity_reference")
     */
    public $entity_reference;

    /**
     * @dbColumn constraint_type
     * @fieldType string
     * @dbOptions length = 100, notnull = false
     * @var string $constraint_type - the query constraint type
     * @OA\Property(description="constraint type", title="constraint_type")
     */
    public $constraint_type;

    /**
     * @dbColumn constraint_value
     * @fieldType text
     * @dbOptions notnull = false
     * @var string $constraint_value - the query constraint type
     * @OA\Property(description="constraint value", title="constraint_value")
     */
    public $constraint_value;

    /**
     * @dbColumn constraint_value_old
     * @fieldType text
     * @dbOptions notnull = false
     * @var string $constraint_value - the query constraint type
     * @OA\Property(description="constraint value", title="constraint_value")
     */
    public $constraint_value_old;

    /**
     * @dbColumn constraint_value_old4
     * @fieldType text
     * @dbOptions notnull = false
     * @var string $constraint_value - the query constraint type
     * @OA\Property(description="constraint value", title="constraint_value_old4")
     */
    public $constraint_value_old4;

    /**
     * @dbColumn sort_order
     * @fieldType integer
     * @dbOptions notnull = false
     * @var int $sort_order - the query constraint sort order
     * @OA\Property(description="sort order", title="sort_order")
     */
    public $sort_order;

}
