<?php
namespace BlueSky\Framework\Entity\Query;

use BlueSky\Framework\Object\Entity\Base;

/**
 * Base class for Query
 * @package BlueSky\Framework\Entity\User
 * @dbEntity true
 *
 * @OA\Schema(
 *     description="Query",
 *     title="Query",
 * )
 */
class Query extends Base
{
    /**
     * @var string $tableName
     * */
    protected $tableName = 'sb__query';
    
    public const TABLE_NAME = 'sb__query';

    /**
     * @dbColumn name
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @var string $name - the query name
     * @OA\Property(description="name", title="name")
     */
    public $name;

    /**
     * @dbColumn description
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @var string $description - the query description
     * @OA\Property(description="description", title="description")
     */
    public $description;

    /**
     * @dbColumn object_ref
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @var string $object_ref - the query object_ref
     * @OA\Property(description="object ref", title="object_ref")
     */
    public $object_ref;

    /**
     * @dbColumn object_id
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $object_id - object id
     * @OA\Property(description="object id", title="object_id")
     */
    public $object_id;

    /**
     * @dbColumn operator
     * @fieldType string
     * @dbOptions length = 100, notnull = false
     * @var string $operator - the query constraint operator
     * @OA\Property(description="constraint operator", title="operator")
     */
    public $operator;

}
