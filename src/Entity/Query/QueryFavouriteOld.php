<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 */



namespace BlueSky\Framework\Entity\Query;

use BlueSky\Framework\Object\Entity\Base;

/**
 * Base class for QueryFavourite
 * @package BlueSky\Framework\Entity\User
 * @dbEntity true
 *
 * @OA\Schema(
 *     description="QueryFavourite",
 *     title="QueryFavourite",
 * )
 */
class QueryFavouriteOld extends Base
{
    /**
     * @var string $tableName
     * */
    protected $tableName = 'sb__query_favorite_old';

    public const TABLE_NAME = 'sb__query_favorite_old';

    /**
     * @dbColumn query_id
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $query_id - query id
     * @OA\Property(description="query id", title="query_id")
     */
    public $query_id;

    /**
     * @dbColumn user_id
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $user_id - query user id
     * @OA\Property(description="query user id", title="user_id")
     */
    public $user_id;

}
