<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 */



namespace BlueSky\Framework\Entity\Query;

use BlueSky\Framework\Object\Entity\Base;

/**
 * Base class for QueryClause
 * @package BlueSky\Framework\Entity\User
 * @dbEntity true
 *
 * @OA\Schema(
 *     description="QueryClause",
 *     title="QueryClause",
 * )
 */
class QueryClauseOld extends Base
{
    /**
     * @var string $tableName
     * */
    protected $tableName = 'sb__query_clause_old';

    public const TABLE_NAME = 'sb__query_clause_old';

    /**
     * @dbColumn query_id
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $query_id - query id
     * @OA\Property(description="query id", title="query_id")
     */
    public $query_id;

    /**
     * @dbColumn parent_id
     * @fieldType integer
     * @dbOptions notnull = false
     * @var int $parent_id - parent id
     * @OA\Property(description="parent id", title="parent_id")
     */
    public $parent_id;

    /**
     * @dbColumn field
     * @fieldType string
     * @dbOptions length = 100, notnull = false
     * @var string $field_reference - the query field
     * @OA\Property(description="field reference", title="field_reference")
     */
    public $field;

    /**
     * @dbColumn type
     * @fieldType string
     * @dbOptions length = 100, notnull = false
     * @var string $field_type - the query field type
     * @OA\Property(description="field type", title="field_type")
     */
    public $type;

    /**
     * @dbColumn operator
     * @fieldType string
     * @dbOptions length = 100, notnull = false
     * @var string $field_type - the query field type
     * @OA\Property(description="field type", title="field_type")
     */
    public $operator;

    /**
     * @dbColumn value
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @var string $field_type - the query field type
     * @OA\Property(description="field type", title="field_type")
     */
    public $value;

    /**
     * @dbColumn sort_order
     * @fieldType integer
     * @dbOptions notnull = false
     * @var int $sort_order - the query constraint sort order
     * @OA\Property(description="sort order", title="sort_order")
     */
    public $sort_order;

    /**
     * @dbColumn field_type
     * @fieldType string
     * @dbOptions length = 32, notnull = false
     * @var string $field_type - the query field type
     * @OA\Property(description="field type", title="field_type")
     */
    public $field_type;

    /**
     * @dbColumn custom_id
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @var string $custom_id - the query field type
     * @OA\Property(description="field type", title="field_type")
     */
    public $custom_id;

    /**
     * @dbColumn data_type
     * @fieldType string
     * @dbOptions length = 32, notnull = false
     * @var string $data_type - the query field type
     * @OA\Property(description="field type", title="field_type")
     */
    public $data_type;

}
