<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 */

declare(strict_types=1);

namespace BlueSky\Framework\Entity;

class QueueJobFactory
{

    /**
     * @var array $referenceMap - Map of references to QueueJobInterface Implementations
     */
    private static $referenceMap = [
        'sb__import' => '\BlueSky\Framework\Entity\Import'
    ];

    /**
     * Instantiate a QueueJobInterface implementation based on object ref and ID
     * @param $state
     * @param $objectRef
     * @param $objectId
     * @return QueueJobInterface|null
     */
    public static function createInstance($state, $objectRef, $objectId)
    {
        $jobObj = null;
        if (isset($objectRef) && self::$referenceMap[$objectRef] && isset($objectId)) {
            $className = self::$referenceMap[$objectRef];
            $jobObj = new $className($state, $objectId);
            // Only allow return of QueueJobInterface Implementation
            if (!($jobObj instanceof QueueJobInterface)) {
                unset($jobObj);
                $jobObj = null;
            }
        }
        return $jobObj;
    }

}