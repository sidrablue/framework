<?php
namespace BlueSky\Framework\Entity;

use BlueSky\Framework\Object\Entity\Base;

/**
 * UserRole class
 * @package BlueSky\Framework\Entity
 * @dbEntity true
 *
 * @SWG\Definition(required={"tableName", "role_id", "user_id"}, type="object", @SWG\Xml(name="UserRole"))
 */
class UserRole extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__user_role';
    public const TABLE_NAME = 'sb__user_role';

    /**
     * @fieldType integer
     * @dbIndex true
     * @var string $role_id - foreign key for role
     * @SWG\Property()
     * @var integer
     */
    public $role_id;

    /**
     * @fieldType integer
     * @dbIndex true
     * @var string $user_id - foreign key for user
     * @SWG\Property()
     * @var integer
     */
    public $user_id;

    /**
     * @fieldType boolean
     * @dbIndex true
     * @dbOptions length = 1, notnull = true, default = false
     * @var boolean $is_primary - True if the role is primary
     * @SWG\Property()
     */
    public $is_primary;

}
