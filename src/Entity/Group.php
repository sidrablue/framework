<?php
/**
 * Copyright (c) BlueSky Digital Labs Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Entity;

use BlueSky\Framework\Object\Entity\Base;

/**
 * Group class
 * @package BlueSky\Framework\Entity
 * @dbEntity true
 * @relation from_entity_reference = User, from_field_reference = id, to_field_reference = id, type = MANY_TO_MANY, mtm_connection_entity_reference = UserGroup, mtm_self_connection_field_reference = user_id, mtm_foreign_connection_field_reference = group_id
 * @OA\Schema(
 *     description="Group",
 *     title="Group",
 * )
 */

class Group extends Base
{
    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__group';
    public const TABLE_NAME = 'sb__group';

    public const GROUP_TYPE_STATIC = 'static';
    public const GROUP_TYPE_DYNAMIC = 'dynamic';

    /**
     * @var int $id
     * @fieldType primary_key_int
     * @dbOptions autoincrement = true
     * */
    public $id;

    /**
     * @fieldType datetime_immutable
     * @dbOptions notnull = false
     * @var \DateTimeImmutable $lote_created - the time that this row was created. This is a \DateTime compatible 'c' format value
     */
    public $lote_created;

    /**
     * @fieldType datetime
     * @dbOptions notnull = false
     * @var \DateTimeImmutable $lote_updated - the last time that this row was updated. This is a \DateTime compatible 'c' format value
     */
    public $lote_updated;

    /**
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var string $object_ref
     * @OA\Property(description="object_ref", title="object_ref")
     * @var string
     */
    public $object_ref;

    /**
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var string $object_id
     * @OA\Property(description="object_id", title="object_id")
     * @var int
     */

    public $object_id;

    /**
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var string $object_key
     * @OA\Property(description="object_key", title="object_key")
     * @var string
     */
    public $object_key;

    /**
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var string $external_code - This is used to differentiate between different 3rd party systems
     * @OA\Property(description="external_code", title="external_code")
     */
    public $external_code;

    /**
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var string $external_id - This is the id on the external (3rd party system) api integration
     * @OA\Property(description="external_id", title="external_id")
     */
    public $external_id;

    /**
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @var string $name - the group name
     * @dbIndex true
     * @flags [ FLAG_QUERYABLE ]
     * @OA\Property(description="name", title="name")
     * @var string
     */
    public $name;

    /**
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @var string $description - The group description
     * @OA\Property(description="description", title="description")
     * @var string
     */
    public $description;

    /**
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $priority - The priority of the group
     * @OA\Property(description="priority", title="priority")
     * @var int
     */
    public $priority;

    /**
     * @fieldType integer
     * @dbOptions notnull = false, default = 0
     * @dbIndex true
     * @var int $parent_id - The parent ID of the group
     * @OA\Property(description="parent_id", title="parent_id")
     * @var int
     */
    public $parent_id;

    /**
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var string $sort_order - The visual sort order
     * @OA\Property(description="sort_order", title="sort_order")
     * @var int
     */
    public $sort_order;

    /**
     * @fieldType boolean
     * @dbOptions notnull = false, default=true
     * @dbIndex true
     * @var boolean $is_active - True if the group is active
     * @OA\Property(description="is_active", title="is_active")
     * @var bool
     */
    public $is_active;

    /**
     * @fieldType string
     * @dbOptions length=32, notnull = true, default=static
     * @var string $group_type - 'static' or 'query', either fixed members or membership based on the query_id
     * @OA\Property(description="static", title="static")
     * @var string
     */
    public $group_type = 'static';

    /**
     * @fieldType integer
     * @dbOptions notnull=false
     * @var string $weighting - Group weighting (affects sort order)
     * @OA\Property(description="weighting", title="weighting")
     * @var int
     */
    public $weighting;

    /**
     * @fieldType boolean
     * @dbOptions notnull = false, default=false
     * @dbIndex true
     * @var boolean $is_public True if the group is publicly visible
     * @OA\Property(description="is_public", title="is_public")
     * @var bool
     */
    public $is_public = 0;

    /**
     * @fieldType boolean
     * @dbOptions notnull = false, default=false
     * @dbIndex true
     * @var boolean $is_parent_account_accessible True if the group is publicly visible
     * @OA\Property(description="is_parent_account_accessible", title="is_parent_account_accessible")
     * @var bool
     */
    public $is_parent_account_accessible = 0;

    /**
     * @fieldType boolean
     * @dbOptions notnull = false, default=false
     * @dbIndex true
     * @var boolean $is_protected - True if this entity is deletion protected
     * @OA\Property(description="is_protected", title="is_protected")
     * @var bool
     */
    public $is_protected;

    /**
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $query_id - The query ID to get the group members from, if the group type is 'query'
     * @OA\Property(description="query_id", title="query_id")
     * @var int
     */
    public $query_id;

    /**
     * @fieldType boolean
     * @dbOptions notnull = false, default=false
     * @dbIndex true
     * @var int $validate_members - To check if the members need to be validated
     * @OA\Property(description="validate_members", title="validate_members")
     * @var bool
     */
    public $validate_members = 0;

    /**
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @var int $lote_path - lote path
     * @OA\Property(description="lote_path", title="lote_path")
     * @var string
     */
    public $lote_path;

    /**
     * @var boolean $isAudited
     * True if this entity is audited
     * @OA\Property(description="is_audited", title="is_audited")
     * @var bool
     */
    protected $isAudited = true;

    /**
     * Function to call before inserting data so that data can be manipulated if required
     * @param array $data
     * @return array
     * */
    protected function beforeInsert($data)
    {
        if(!isset($data['parent_id']) || !$data['parent_id'] > 0 ) {
            $data['parent_id'] = 0;
        }
        if(!isset($data['is_active']) || $data['is_active'] !='0') {
            $data['is_active'] = 1;
        }
        return $data;
    }

    public function delete(bool $strongDelete = false)
    {
        if ($this->group_type === self::GROUP_TYPE_STATIC) {
            if ($strongDelete) {
                $this->getWriteDb()->delete(UserGroupEntity::TABLE_NAME, ['group_id' => $this->id]) > 0;
            } else {
                $data = ['lote_deleted' => Time::getUtcNow(), 'lote_deleted_by' => $this->getState()->getUser()->id];
                $this->getWriteDb()->update(UserGroupEntity::TABLE_NAME, $data, ['group_id' => $this->id]) > 0;
            }
        }
        parent::delete($strongDelete);
    }

}
