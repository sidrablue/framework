<?php
namespace BlueSky\Framework\Entity;

use BlueSky\Framework\Object\Entity\Base;

/**
 * RoleType class
 * @package BlueSky\Framework\Entity
 * @dbEntity true
 *
 * @SWG\Definition(required={"tableName", "type_id", "role_id"}, type="object", @SWG\Xml(name="RoleType"))
 */
class RoleType extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__role_type';
    public const TABLE_NAME = 'sb__role_type';

    /**
     * @dbColumn type_id
     * @fieldType integer
     * @var string $type_id - foreign key for type
     * @SWG\Property()
     * @var integer
     */
    public $type_id;

    /**
     * @dbColumn role_id
     * @fieldType integer
     * @var string $role_id - foreign key for role
     * @SWG\Property()
     * @var integer
     */
    public $role_id;

}
