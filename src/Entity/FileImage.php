<?php
namespace BlueSky\Framework\Entity;

use BlueSky\Framework\Object\Entity\Base;

/**
 * FileImage class
 * @package BlueSky\Framework\Entity
 * @dbEntity true
 */
class FileImage extends Base
{
    /**
     * @var string $tableName
     * */
    protected $tableName = 'sb__file_image';
    public const TABLE_NAME = 'sb__file_image';

    /**
     * @dbColumn file_id
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $file_id - the ID of the file
     */
    public $file_id;

    /**
     * @dbColumn width
     * @fieldType integer
     * @dbOptions notnull = false
     * @var string $width - the width of the file
     */
    public $width;

    /**
     * @dbColumn height
     * @fieldType integer
     * @dbOptions notnull = false
     * @var string $height - the height of the file
     */
    public $height;
}
