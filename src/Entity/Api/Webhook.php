<?php
namespace BlueSky\Framework\Entity\Api;

use BlueSky\Framework\Object\Entity\Base;

/**
 * Base class for API Webhook
 * @package BlueSky\Framework\Entity\Api
 * @dbEntity true
 */
class  Webhook extends Base
{
    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__api_webhook';
    public const TABLE_NAME = 'sb__api_webhook';

    /**
     * @dbColumn name
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @var string $name
     */
    public $name;

    /**
     * @dbColumn description
     * @fieldType text
     * @dbOptions notnull = false
     * @var string $description
     */

    public $description;

    /**
     * @dbColumn is_active
     * @fieldType boolean
     * @dbOptions notnull = false, default=true
     * @var boolean $is_active
     */
    public $is_active;

    /**
     * @dbColumn auth_data
     * @fieldType text
     * @dbOptions notnull = false
     * @var string $auth_data
     */

    public $auth_data;

    /**
     * @dbColumn target_url
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @var string $target_url
     */
    public $target_url;

    /**
     * @dbColumn auth_type
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @var string $auth_type
     */
    public $auth_type;

    /**
     * @dbColumn is_all_events
     * @fieldType boolean
     * @dbOptions notnull = false, default=false
     * @var boolean $is_all_events
     */
    public $is_all_events;

}
