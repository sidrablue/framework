<?php
namespace BlueSky\Framework\Entity\Api;

use BlueSky\Framework\Object\Entity\Base;

/**
 * Base class for API Keys
 * @package BlueSky\Framework\Entity\Api
 * @dbEntity true
 */
class Key extends Base
{
    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__api_key';
    public const TABLE_NAME = 'sb__api_key';

    /**
     * @dbColumn name
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @var string $name
     */
    public $name;

    /**
     * @dbColumn public_key
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @var string $public_key
     */
    public $public_key;

    /**
     * @dbColumn private_key
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @var string $private_key
     */
    public $private_key;

    /**
     * @dbColumn version
     * @fieldType string
     * @dbOptions length=32, notnull = false
     * @var string $version
     */
    public $version;

    /**
     * @dbColumn is_admin
     * @fieldType boolean
     * @dbOptions notnull = false, default=false
     * @var boolean $is_admin
     */
    public $is_admin;

    /**
     * @dbColumn is_active
     * @fieldType boolean
     * @dbOptions notnull = false, default=true
     * @var boolean $is_active
     */
    public $is_active;

    /**
     * @dbColumn datetime_from
     * @fieldType datetime
     * @dbOptions notnull = false
     * @var string $valid_from
     */
    public $datetime_from;

    /**
     * @dbColumn datetime_to
     * @fieldType datetime
     * @dbOptions notnull = false
     * @var string $datetime_to
     */
    public $datetime_to;

}
