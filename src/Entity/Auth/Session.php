<?php
namespace BlueSky\Framework\Entity\Auth;

use BlueSky\Framework\Object\Entity\Base;
use BlueSky\Framework\Util\Time;

/**
 * Entity class for the user login session
 * Class Session
 * @package BlueSky\Framework\Entity\Auth
 * @dbEntity true
 */
class Session extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__auth_session';
    public const TABLE_NAME = 'sb__auth_session';

    /**
     * @dbColumn object_ref
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var string $object_ref
     */
    public $object_ref;

    /**
     * @dbColumn object_id
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var string $object_id
     */
    public $object_id;

    /**
     * @dbColumn object_key
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var string $object_key
     */
    public $object_key;

    /**
     * @dbColumn source
     * @fieldType string
     * @dbOptions length = 32, notnull = false
     * @dbIndex true
     * @var string $source - the source of this user, such as 'local', 'ldap', 'google', 'facebook', etc.
     */
    public $source = 'local';

    /**
     * @dbColumn token
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @dbIndex true
     * @var string $token - the encryption token for this login
     */
    public $token;

    /**
     * @dbColumn hash
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @var string $hash - The encrypted login hash
     */
    public $hash;

    /**
     * @dbColumn session_id
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @dbIndex true
     * @var string $session_id - The web server session id of the current login
     */
    public $session_id;

    /**
     * @dbColumn ip_address
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @var string $ip_address - The IP Address of the current login session
     */
    public $ip_address;

    /**
     * @dbColumn is_active
     * @fieldType boolean
     * @dbOptions notnull = false, default=true
     * @dbIndex true
     * @var boolean $is_active - true if this session is still active
     */
    public $is_active = '1';

    /**
     * @dbColumn datetime_expires
     * @fieldType datetime
     * @dbOptions notnull = false
     * @var \DateTime $expires - The time that this session expires
     */
    public $datetime_expires;

    /**
     * @dbColumn datetime_last_activity
     * @fieldType datetime
     * @dbOptions notnull = false
     * @var \DateTime $last_activity - The last time that activity occurred for this login session
     */
    public $datetime_last_activity;

    /**
     * @dbColumn masquerade_user_id
     * @fieldType integer
     * @dbOptions notnull = false
     * @var int $masquerade_user_id - a user ID to fall back into and login as afterwards
     */
    public $masquerade_user_id;

    /**
     * @dbColumn is_two_factor_verified
     * @fieldType boolean
     * @dbOptions notnull = false, default=false
     * @var bool $is_two_factor_verified
     */
    public $is_two_factor_verified;

    /**
     * Determine if the details of this login session imply that a user is logged in
     * @access public
     * @return bool
     * @todo - validate this, to see if its still working
     * */
    public function isValidLogin()
    {
        return $this->id && $this->datetime_expires > Time::getUtcNow() && $this->is_active;
    }

    /**
     * Function to call before inserting data so that data can be manipulated if required
     * @param array $data
     * @return array
     * */
    protected function beforeInsert($data)
    {
        $data = $this->beforeUpdate($data);
        $data['token'] = uniqid();
        return $data;
    }

    /**
     * Function to call before inserting data so that data can be manipulated if required
     * @param array $data
     * @return array
     * */
    protected function beforeUpdate($data)
    {
        $time = new \DateTime('now', new \DateTimeZone('UTC'));
        $data['datetime_last_activity'] = $time->format("Y-m-d H:i:s");
        $time->add(new \DateInterval('PT5M'));
        $data['datetime_expires'] = $time->format("Y-m-d H:i:s");
        return $data;
    }

}
