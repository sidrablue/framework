<?php
namespace BlueSky\Framework\Entity\Auth;

use BlueSky\Framework\Object\Entity\Base;

/**
 * Group class
 * @package BlueSky\Framework\Entity
 * @dbEntity true
 *
 * @SWG\Definition(required={"tableName", "object_id", "object_ref","secret", @SWG\Xml(name="Authenticator")})
 */

class Authenticator extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__auth_authenticator';
    public const TABLE_NAME = 'sb__auth_authenticator';

    /**
     * @dbColumn object_id
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int
     */
    public $object_id;

    /**
     * @dbColumn object_ref
     * @fieldType string
     * @dbOptions notnull = false
     * @var string $object_ref
     * @SWG\Property()
     * @var string
     */
    public $object_ref;

    /**
     * @dbColumn secret
     * @fieldType string
     * @dbOptions notnull = false
     * @var string $secret - The secret code for the account
     * @SWG\Property()
     * @var string
     */
    public $secret;

}
