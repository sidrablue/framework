<?php
namespace BlueSky\Framework\Entity\Auth;

use BlueSky\Framework\Object\Entity\Base;

/**
 * Base class for a task
 * @package Lote\System\Crm\Entity
 * @dbEntity true
 */
class Token extends Base
{
    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__auth_token';
    public const TABLE_NAME = 'sb__auth_token';

    /**
     * @dbColumn object_ref
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var string $object_ref
     */
    public $object_ref;

    /**
     * @dbColumn object_id
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var string $object_id
     */
    public $object_id;

    /**
     * @dbColumn object_key
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var string $object_key
     */
    public $object_key;

    /**
     * @dbColumn public_key
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var string $public_key
     */
    public $public_key;

    /**
     * @dbColumn private_key
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var string $private_key
     */
    public $private_key;

    /**
     * @dbColumn usage_limit
     * @fieldType integer
     * @dbOptions notnull = false
     * @var int $usage_limit
     */
    public $usage_limit;

    /**
     * @dbColumn usage_count
     * @fieldType integer
     * @dbOptions notnull = false
     * @var int $usage_count
     */
    public $usage_count;

    /**
     * @dbColumn datetime_expires
     * @fieldType datetime
     * @dbOptions notnull = false
     * @var string $datetime_expires
     */
    public $datetime_expires;

    /**
     * Check if the given token is still valid
     * @access public
     * @return boolean
     * */
    public function isValid()
    {
        return $this->datetime_expires == null || $this->datetime_expires < gmdate("Y-m-d H:i:s", time());
    }

}