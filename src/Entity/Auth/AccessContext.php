<?php
namespace BlueSky\Framework\Entity\Auth;

use BlueSky\Framework\Object\Entity\Base;

/**
 * Field link log audit
 * @package BlueSky\Framework\Entity\Auth
 * @dbEntity true
 * @OA\Schema(
 *     description="AccessContext",
 *     title="AccessContext",
 * )
 * @SWG\Definition(required={"tableName", "isAudited", "account_ref", "object_ref", "object_id", "context", "access_level", "active"}, type="object", @SWG\Xml(name="AccessContext"))
 *
 */
class AccessContext extends Base
{
    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__auth_access_context';
    public const TABLE_NAME = 'sb__auth_access_context';

    /**
     * @var boolean $isAudited
     * True if this entity is audited
     * @OA\Property(description="isAudited", title="is_audited")
     * */
    protected $isAudited = true;

    /**
     * @dbColumn account_ref
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var string $account_ref
     * @OA\Property(description="account_ref", title="account_ref")
     */
    public $account_ref;

    /**
     * @dbColumn object_ref
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var string $object_ref
     * @OA\Property(description="object_ref", title="object_ref")
     */
    public $object_ref;

    /**
     * @dbColumn object_id
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var string $object_id
     * @OA\Property(description="object_id", title="object_id")
     */
    public $object_id;

    /**
     * @dbColumn context
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @dbIndex true
     * @var string $context
     * @OA\Property(description="context", title="context")
     */
    public $context;

    /**
     * @dbColumn access_level
     * @fieldType string
     * @dbOptions length = 32, notnull = false
     * @var string $access_level
     * @OA\Property(description="access_level", title="access_level")
     */
    public $access_level;

    /**
     * @dbColumn active
     * @fieldType integer
     * @dbOptions length = 10, notnull = false
     * @var int $active - active
     * @OA\Property(description="active", title="active")
     */
    public $active;

}
