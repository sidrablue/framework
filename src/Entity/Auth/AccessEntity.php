<?php
namespace BlueSky\Framework\Entity\Auth;

use BlueSky\Framework\Object\Entity\Base;

/**
 * Class Country
 * @package BlueSky\Framework\Entity\Auth
 * @dbEntity true
 *
 * @SWG\Definition(required={"tableName", "isAudited", "object_ref", "object_id", "field_ref", "access_type", "access_type_id", "access_level"}, type="object", @SWG\Xml(name="AccessEntity"))
 */
class AccessEntity extends Base
{
    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__auth_access_entity';
    public const TABLE_NAME = 'sb__auth_access_entity';

    /**
     * @var boolean $isAudited
     * True if this entity is audited
     * */
    protected $isAudited = true;

    /**
     * @dbColumn object_ref
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var string $object_ref
     */
    public $object_ref;

    /**
     * @dbColumn object_id
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var string $object_id
     */
    public $object_id;

    /**
     * @dbColumn field_ref
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var string $object_ref
     */
    public $field_ref;

    /**
     * @dbColumn access_type
     * @fieldType string
     * @dbOptions length=5, notnull = false
     * @dbIndex true
     * @dbComment role or user
     * @var string $access_type
     */
    public $access_type;

    /**
     * @dbColumn access_type_id
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var string $access_type_id
     */
    public $access_type_id;

    /**
     * @dbColumn access_level
     * @fieldType string
     * @dbOptions length=12, notnull = false
     * @dbIndex true
     * @dbComment read or write
     * @var string $access_level
     */
    public $access_level = 'write';

}
