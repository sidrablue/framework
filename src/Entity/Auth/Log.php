<?php
namespace BlueSky\Framework\Entity\Auth;

use BlueSky\Framework\Object\Entity\Base;

/**
 * Class Country
 * @package BlueSky\Framework\Entity\Region
 * @dbEntity true
 */
class Log extends Base
{
    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__auth_log';
    public const TABLE_NAME = 'sb__auth_log';

    /**
     * @var boolean $isAudited
     * True if this entity is audited
     * */
    protected $isAudited = true;

    /**
     * @dbColumn code
     * @fieldType string
     * @dbOptions length = 32, notnull = false
     * @var string $code - code
     */
    public $code;

    /**
     * @dbColumn name
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @var string $name - name
     */
    public $name;

    /**
     * @dbColumn active
     * @fieldType integer
     * @dbOptions length = 10, notnull = false
     * @var int $active - active
     */
    public $active;


}