<?php
namespace BlueSky\Framework\Entity\Group;

use BlueSky\Framework\Object\Entity\Base;

/**
 * Group User Totals class
 * @package BlueSky\Framework\Entity\Group
 * @dbEntity true
 * @OA\Schema(
 *     description="Group User Totals",
 *     title="Group User Totals",
 * )
 */
class UserTotal extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__group_user_total';
    public const TABLE_NAME = 'sb__group_user_total';

    /**
     * @dbColumn group_id
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $group_id
     * @OA\Property(description="group_id", title="group_id")
     */
    public $group_id;

    /**
     * @dbColumn total
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $total
     * @OA\Property(description="total", title="total")
     */
    public $total = 0;

    /**
     * @dbColumn total_active
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $total_active
     * @OA\Property(description="total_active", title="total_active")
     */
    public $total_active = 0;

    /**
     * @dbColumn total_subscribed
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $total_subscribed
     * @OA\Property(description="total_subscribed", title="total_subscribed")
     */
    public $total_subscribed = 0;

    /**
     * @dbColumn total_active_subscribed
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $total_active_subscribed
     * @OA\Property(description="total_active_subscribed", title="total_active_subscribed")
     */
    public $total_active_subscribed = 0;

    /**
     * @dbColumn total_subscribed_edm
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $total_subscribed_edm
     * @OA\Property(description="total_subscribed_edm", title="total_subscribed_edm")
     */
    public $total_subscribed_edm = 0;

    /**
     * @dbColumn total_subscribed_sms
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $total_subscribed_sms
     * @OA\Property(description="total_subscribed_sms", title="total_subscribed_sms")
     */
    public $total_subscribed_sms = 0;

    /**
     * @dbColumn total_active_subscribed_edm
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $total_active_subscribed_edm
     * @OA\Property(description="total_active_subscribed_edm", title="total_active_subscribed_edm")
     */
    public $total_active_subscribed_edm = 0;

    /**
     * @dbColumn total_active_subscribed_sms
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $total_active_subscribed_sms
     * @OA\Property(description="total_active_subscribed_sms", title="total_active_subscribed_sms")
     */
    public $total_active_subscribed_sms = 0;

    /**
     * @dbColumn total_unsubscribed_temporary
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $total_unsubscribed_temporary
     * @OA\Property(description="total_unsubscribed_temporary", title="total_unsubscribed_temporary")
     */
    public $total_unsubscribed_temporary = 0;

    /**
     * @dbColumn total_active_unsubscribed_temporary
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $total_active_unsubscribed_temporary
     * @OA\Property(description="total_active_unsubscribed_temporary", title="total_active_unsubscribed_temporary")
     */
    public $total_active_unsubscribed_temporary = 0;

    /**
     * @dbColumn total_unsubscribed_temporary_edm
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $total_unsubscribed_temporary_edm
     * @OA\Property(description="total_unsubscribed_temporary_edm", title="total_unsubscribed_temporary_edm")
     */
    public $total_unsubscribed_temporary_edm = 0;

    /**
     * @dbColumn total_unsubscribed_temporary_sms
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $total_unsubscribed_temporary_sms
     * @OA\Property(description="total_unsubscribed_temporary_sms", title="total_unsubscribed_temporary_sms")
     */
    public $total_unsubscribed_temporary_sms = 0;

    /**
     * @dbColumn total_active_unsubscribed_temporary_edm
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $total_active_unsubscribed_temporary_edm
     * @OA\Property(description="total_active_unsubscribed_temporary_edm", title="total_active_unsubscribed_temporary_edm")
     */
    public $total_active_unsubscribed_temporary_edm = 0;

    /**
     * @dbColumn total_active_unsubscribed_temporary_sms
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $total_active_unsubscribed_temporary_sms
     * @OA\Property(description="total_active_unsubscribed_temporary_sms", title="total_active_unsubscribed_temporary_sms")
     */
    public $total_active_unsubscribed_temporary_sms = 0;

    /**
     * @dbColumn total_unsubscribed_permanent
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $total_unsubscribed_permanent
     * @OA\Property(description="total_unsubscribed_permanent", title="total_unsubscribed_permanent")
     */
    public $total_unsubscribed_permanent = 0;

    /**
     * @dbColumn total_active_unsubscribed_permanent
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $total_active_unsubscribed_permanent
     * @OA\Property(description="total_active_unsubscribed_permanent", title="total_active_unsubscribed_permanent")
     */
    public $total_active_unsubscribed_permanent = 0;

    /**
     * @dbColumn total_unsubscribed_permanent_edm
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $total_unsubscribed_permanent_edm
     * @OA\Property(description="total_unsubscribed_permanent_edm", title="total_unsubscribed_permanent_edm")
     */
    public $total_unsubscribed_permanent_edm = 0;

    /**
     * @dbColumn total_unsubscribed_permanent_sms
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $total_unsubscribed_permanent_sms
     * @OA\Property(description="total_unsubscribed_permanent_sms", title="total_unsubscribed_permanent_sms")
     */
    public $total_unsubscribed_permanent_sms = 0;

    /**
     * @dbColumn total_active_unsubscribed_permanent_edm
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $total_active_unsubscribed_permanent_edm
     * @OA\Property(description="total_active_unsubscribed_permanent_edm", title="total_active_unsubscribed_permanent_edm")
     */
    public $total_active_unsubscribed_permanent_edm = 0;

    /**
     * @dbColumn total_active_unsubscribed_permanent_sms
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $total_active_unsubscribed_permanent_sms
     * @OA\Property(description="total_active_unsubscribed_permanent_sms", title="total_active_unsubscribed_permanent_sms")
     */
    public $total_active_unsubscribed_permanent_sms = 0;

    /**
     * Set the group_id of this entity
     * @access public
     * @param int $groupId
     * @return int
     * */
    public function setGroupId($groupId)
    {
        $this->group_id = $groupId;
        return $this->loadByField("group_id", $groupId);
    }

    /**
     * Set the group_id of this entity
     * @access public
     * @param array $data
     * @return array
     * */
    public function beforeInsert($data)
    {
        $this->getWriteDb()->delete($this->getTableName(), ['group_id' => $this->group_id]);
        $data['total'] = $data['total'] ?? 0;
        $data['total'] = $data['total'] ?? 0;
        $data['total_active'] = $data['total_active'] ?? 0;
        $data['total_subscribed'] = $data['total_subscribed'] ?? 0;
        $data['total_active_subscribed'] = $data['total_active_subscribed'] ?? 0;
        $data['total_subscribed_edm'] = $data['total_subscribed_edm'] ?? 0;
        $data['total_subscribed_sms'] = $data['total_subscribed_sms'] ?? 0;
        $data['total_active_subscribed_edm'] = $data['total_active_subscribed_edm'] ?? 0;
        $data['total_active_subscribed_sms'] = $data['total_active_subscribed_sms'] ?? 0;
        $data['total_unsubscribed_temporary'] = $data['total_unsubscribed_temporary'] ?? 0;
        $data['total_active_unsubscribed_temporary'] = $data['total_active_unsubscribed_temporary'] ?? 0;
        $data['total_unsubscribed_temporary_edm'] = $data['total_unsubscribed_temporary_edm'] ?? 0;
        $data['total_unsubscribed_temporary_sms'] = $data['total_unsubscribed_temporary_sms'] ?? 0;
        $data['total_active_unsubscribed_temporary_edm'] = $data['total_active_unsubscribed_temporary_edm'] ?? 0;
        $data['total_active_unsubscribed_temporary_sms'] = $data['total_active_unsubscribed_temporary_sms'] ?? 0;
        $data['total_unsubscribed_permanent'] = $data['total_unsubscribed_permanent'] ?? 0;
        $data['total_active_unsubscribed_permanent'] = $data['total_active_unsubscribed_permanent'] ?? 0;
        $data['total_unsubscribed_permanent_edm'] = $data['total_unsubscribed_permanent_edm'] ?? 0;
        $data['total_unsubscribed_permanent_sms'] = $data['total_unsubscribed_permanent_sms'] ?? 0;
        $data['total_active_unsubscribed_permanent_edm'] = $data['total_active_unsubscribed_permanent_edm'] ?? 0;
        $data['total_active_unsubscribed_permanent_sms'] = $data['total_active_unsubscribed_permanent_sms'] ?? 0;
        return $data;
    }

}
