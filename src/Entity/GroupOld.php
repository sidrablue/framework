<?php
namespace BlueSky\Framework\Entity;

use BlueSky\Framework\Object\Entity\Base;

/**
 * Group class
 * @package BlueSky\Framework\Entity
 * @dbEntity true
 * @OA\Schema(
 *     description="Group",
 *     title="Group",
 * )
 */

class GroupOld extends Base
{
    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__group_old';
    public const TABLE_NAME = 'sb__group_old';

    /**
     * @dbColumn object_ref
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var string $object_ref
     * @OA\Property(description="object_ref", title="object_ref")
     * @var string
     */
    public $object_ref;

    /**
     * @dbColumn object_id
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var string $object_id
     * @OA\Property(description="object_id", title="object_id")
     * @var int
     */

    public $object_id;

    /**
     * @dbColumn object_key
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var string $object_key
     * @OA\Property(description="object_key", title="object_key")
     * @var string
     */
    public $object_key;

    /**
     * @dbColumn external_id
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var string $external_id
     * @OA\Property(description="external_id", title="external_id")
     */
    public $external_id;

    /**
     * @dbColumn name
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @var string $name - the group name
     * @dbIndex true
     * @flags [ FLAG_QUERYABLE ]
     * @OA\Property(description="name", title="name")
     * @var string
     */
    public $name;

    /**
     * @dbColumn description
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @var string $description - The group description
     * @OA\Property(description="description", title="description")
     * @var string
     */
    public $description;

    /**
     * @dbColumn priority
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $priority - The priority of the group
     * @OA\Property(description="priority", title="priority")
     * @var int
     */
    public $priority;

    /**
     * @dbColumn parent_id
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $parent_id - The parent ID of the group
     * @OA\Property(description="parent_id", title="parent_id")
     * @var int
     */
    public $parent_id;

    /**
     * @dbColumn sort_order
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var string $sort_order - The visual sort order
     * @OA\Property(description="sort_order", title="sort_order")
     * @var int
     */
    public $sort_order;

    /**
     * @dbColumn is_active
     * @fieldType boolean
     * @dbOptions notnull = false, default=true
     * @dbIndex true
     * @var boolean $is_active - True if the group is active
     * @OA\Property(description="is_active", title="is_active")
     * @var bool
     */
    public $is_active;

    /**
     * @dbColumn group_type
     * @fieldType string
     * @dbOptions length=32, notnull = true, default=static
     * @var string $group_type - 'static' or 'query', either fixed members or membership based on the query_id
     * @OA\Property(description="static", title="static")
     * @var string
     */
    public $group_type = 'static';

    /**
     * @dbColumn weighting
     * @fieldType integer
     * @dbOptions notnull=false
     * @var string $weighting - Group weighting (affects sort order)
     * @OA\Property(description="weighting", title="weighting")
     * @var int
     */
    public $weighting;

    /**
     * @dbColumn is_public
     * @fieldType boolean
     * @dbOptions notnull = false, default=false
     * @dbIndex true
     * @var boolean $is_public True if the group is publicly visible
     * @OA\Property(description="is_public", title="is_public")
     * @var bool
     */
    public $is_public = 0;

    /**
     * @dbColumn is_parent_account_accessible
     * @fieldType boolean
     * @dbOptions notnull = false, default=false
     * @dbIndex true
     * @var boolean $is_parent_account_accessible True if the group is publicly visible
     * @OA\Property(description="is_parent_account_accessible", title="is_parent_account_accessible")
     * @var bool
     */
    public $is_parent_account_accessible = 0;

    /**
     * @dbColumn is_protected
     * @fieldType boolean
     * @dbOptions notnull = false, default=false
     * @dbIndex true
     * @var boolean $is_protected - True if this entity is deletion protected
     * @OA\Property(description="is_protected", title="is_protected")
     * @var bool
     */
    public $is_protected;

    /**
     * @dbColumn query_id
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $query_id - The query ID to get the group members from, if the group type is 'query'
     * @OA\Property(description="query_id", title="query_id")
     * @var int
     */
    public $query_id;

    /**
     * @dbColumn validate_members
     * @fieldType boolean
     * @dbOptions notnull = false, default=false
     * @dbIndex true
     * @var int $validate_members - To check if the members need to be validated
     * @OA\Property(description="validate_members", title="validate_members")
     * @var bool
     */
    public $validate_members = 0;

    /**
     * @dbColumn lote_path
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @var int $lote_path - lote path
     * @OA\Property(description="lote_path", title="lote_path")
     * @var string
     */
    public $lote_path;

    /**
     * @var boolean $isAudited
     * True if this entity is audited
     * @OA\Property(description="is_audited", title="is_audited")
     * @var bool
     */
    protected $isAudited = true;

    /**
     * Function to call before inserting data so that data can be manipulated if required
     * @param array $data
     * @return array
     * */
    protected function beforeInsert($data)
    {
        if(!isset($data['parent_id']) || !$data['parent_id'] > 0 ) {
            $data['parent_id'] = 0;
        }
        if(!isset($data['is_active']) || $data['is_active'] !='0') {
            $data['is_active'] = 1;
        }
        return $data;
    }

}
