<?php
namespace BlueSky\Framework\Entity\Identifier;

use BlueSky\Framework\Object\Entity\Base;

/**
 * Base class for Identifier
 * @package BlueSky\Framework\Entity\Identifier
 * @dbEntity true
 */
class Identifier extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__identifier';
    public const TABLE_NAME = 'sb__identifier';

    /**
     * @dbColumn object_id
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var string $object_id - Object Id
     */
    public $object_id;

    /**
     * @dbColumn object_ref
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @var int $object_ref - Object ref
     */
    public $object_ref;

    /**
     * @dbColumn preview_token
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @var string $preview_token - Preview token
     */
    public $preview_token;

}
