<?php
namespace BlueSky\Framework\Entity\Site;

use BlueSky\Framework\Entity\Domain as DomainEntity;
use BlueSky\Framework\Object\Entity\Base;
/**
 * Site Domain Class
 * @package Lote\App\Website\Entity
 * @dbEntity true
 */
class SiteDomain extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__site_domain';
    public const TABLE_NAME = 'sb__site_domain';

    /**
     * @dbColumn site_id
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $site_id - The Website Site ID of the domain
     */
    public $site_id;

    /**
     * @dbColumn skin_id
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $skin_id - The Website domain skin
     */
    public $skin_id;

    /**
     * @dbColumn domain_id
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $domain_id - Domain id
     */
    public $domain_id;

    /**
     * @dbColumn is_active
     * @fieldType string
     * @dbOptions notnull = false, default = true
     * @dbIndex true
     * @var boolean $enabled - True if enabled
     */
    public $is_active;

    /**
     * @dbColumn is_default
     * @fieldType string
     * @dbOptions notnull = false, default = true
     * @dbIndex true
     * @var boolean $is_default - Is default
     */
    public $is_default;

    /**
     * @dbColumn ssl_is_enabled
     * @fieldType string
     * @dbOptions notnull = false, default = true
     * @dbIndex true
     * @var boolean $ssl_is_enabled - Has the support of SSL on this domain
     */
    public $ssl_is_enabled;

    /**
     * @dbColumn ssl_is_forced
     * @fieldType string
     * @dbOptions notnull = false, default = true
     * @dbIndex true
     * @var boolean $ssl_is_forced - Force the use of SSL on this domain
     */
    public $ssl_is_forced;


    /**
     * Get the scheme and HTTP host for this domain, based on its ssl_force and ssl_enabled variables
     *
     * @access public
     * @param boolean $preferSsl
     * @return string
     * */
    public function getHttpSchemeAndHost($preferSsl = false)
    {
        $scheme = "http://";
        if ($this->ssl_force || ($preferSsl && $this->ssl_enabled)) {
            $scheme = 'https://';
        }
        $de = new DomainEntity($this->getState());
        $de->load($this->domain_id);
        return $scheme . trim($de->url, '/') . '/';
    }

}
