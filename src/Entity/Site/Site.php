<?php
namespace BlueSky\Framework\Entity\Site;

use BlueSky\Framework\Object\Entity\Base;

/**
 * Base class for Contractor
 * @package Lote\App\Website\Entity
 * @dbEntity true
 */
class Site extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__site';
    public const TABLE_NAME = 'sb__site';

    /**
     * @dbColumn reference
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var string $reference - The reference of the site
     */
    public $reference;

    /**
     * @dbColumn skin_id
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $skin_id - The Website site skin
     */
    public $skin_id;

    /**
     * @dbColumn name
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @var string $name - The name of the site
     */
    public $name;

    /**
     * @dbColumn description
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @var string $description - The description of the site
     */
    public $description;

    /**
     * @dbColumn theme
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @var string $theme - The theme of the site
     */
    public $theme;

    /**
     * @dbColumn is_active
     * @fieldType string
     * @dbOptions notnull = false, default = true
     * @dbIndex true
     * @var boolean $is_active - True if enabled/active
     */
    public $is_active;

    /**
     * @dbColumn is_default
     * @fieldType string
     * @dbOptions notnull = false, default = true
     * @dbIndex true
     * @var boolean $is_default - Is default
     */
    public $is_default;

}
