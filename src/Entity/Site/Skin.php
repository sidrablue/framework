<?php
namespace BlueSky\Framework\Entity\Site;

use BlueSky\Framework\Object\Entity\Base;

/**
 * Base class for Contractor
 * @package Lote\App\Website\Entity
 * @dbEntity true
 */
class Skin extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__site_skin';
    public const TABLE_NAME = 'sb__site_skin';

    /**
     * @dbColumn name
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @var string $name - The name of the site
     */
    public $name;

    /**
     * @dbColumn skin
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var string $skin - The skin reference
     */
    public $skin;

    /**
     * @dbColumn description
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @var string $description - The description of the site
     */
    public $description;

    /**
     * @dbColumn sort_order
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $sort_order - sort order
     */
    public $sort_order;

    /**
     * @dbColumn is_active
     * @fieldType string
     * @dbOptions notnull = false, default = true
     * @dbIndex true
     * @var boolean $is_active - True if enabled/active
     */
    public $is_active;

    /**
     * @dbColumn is_default
     * @fieldType string
     * @dbOptions notnull = false, default = true
     * @dbIndex true
     * @var boolean $is_default - True if default
     */
    public $is_default;

}
