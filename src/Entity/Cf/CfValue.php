<?php
namespace BlueSky\Framework\Entity\Cf;

use BlueSky\Framework\Object\Entity\Base;

/**
 * Class CfValue
 * @package BlueSky\Framework\Entity
 * @deprecated to be deleted once EP 2.0 is stable
 */
class CfValue extends Base
{
    /**
     * @dbColumn object_id
     * @fieldType integer
     * @dbOptions notnull = false
     * @var int $object_id - the id of the object that the value belongs to
     */
    public $object_id;

    /**
     * @dbColumn cf_field_id
     * @fieldType integer
     * @dbOptions notnull = false
     * @var int $field_id - Field ID that the value belongs to
     */
    public $cf_field_id;

    /**
     * @dbColumn value_string
     * @fieldType string
     * @dbOptions length = 1500, notnull = false
     * @var string $value_string - The string value of the field
     */
    public $value_string;

    /**
     * @dbColumn value_number
     * @fieldType decimal
     * @dbOptions precision = 10, scale = 0, notnull = false
     * @var double $value_number - Number value of the fields
     */
    public $value_number;

    /**
     * @dbColumn value_date
     * @fieldType datetime
     * @dbOptions notnull = false
     * @var \DateTime $value_date - Date value of the field
     */
    public $value_date;

    /**
     * @dbColumn value_text
     * @fieldType text
     * @dbOptions notnull = false
     * @var string $value_text - The text value of the field
     */
    public $value_text;

    /**
     * @dbColumn value_boolean
     * @fieldType boolean
     * @dbOptions length = 1, notnull = false
     * @var boolean $value_boolean - The boolean value of the field
     */
    public $value_boolean;

    /**
     *
     * @param int $objectId
     * @param int $fieldId
     * @return boolean
     * */
    public function loadByObjectAndField($objectId, $fieldId)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('*')
            ->from($this->getTableName(), 't')
            ->where('t.object_id = :object_id')
            ->setParameter('object_id', $objectId)
            ->andWhere('t.field_id = :field_id')
            ->setParameter('field_id', $fieldId)
            ->andWhere('t.lote_deleted is null');
        $s = $q->execute();
        if($row = $s->fetch(\PDO::FETCH_ASSOC)) {
            $this->setData($row);
        }
        return $this->id;
    }

}
