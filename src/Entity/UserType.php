<?php
namespace BlueSky\Framework\Entity;

use BlueSky\Framework\Object\Entity\Base;

/**
 * UserType class
 * @package BlueSky\Framework\Entity
 * @dbEntity true
 *
 * @SWG\Definition(required={"tableName", "type_id", "user_id"}, type="object", @SWG\Xml(name="UserType"))
 */
class UserType extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__user_type';
    public const TABLE_NAME = 'sb__user_type';

    /**
     * @dbColumn type_id
     * @fieldType integer
     * @var string $type_id - foreign key for type
     * @SWG\Property()
     * @var integer
     */
    public $type_id;

    /**
     * @dbColumn user_id
     * @fieldType integer
     * @var string $user_id - foreign key for user
     * @SWG\Property()
     * @var integer
     */
    public $user_id;

}
