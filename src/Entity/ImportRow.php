<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 */

declare(strict_types=1);

namespace BlueSky\Framework\Entity;

use BlueSky\Framework\Object\Entity\Base;

/**
 * Base class for all entities
 * @dbEntity true
 */
class ImportRow extends Base
{

    /**
     * @var string $tableName
     * */
    protected $tableName = 'sb__import_row';
    public const TABLE_NAME = 'sb__import_row';

    /**
     * @dbColumn import_id
     * @fieldType fkey
     * @dbOptions notnull = false
     * @dbIndex true
     * @var string $import_id - the import ID
     */
    public $import_id;

    /**
     * @dbColumn status
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @dbIndex true
     * @var string $status - The name of the field
     */
    public $status;

    /**
     * @dbColumn note
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @var string $note - The note
     */
    public $note;

    /**
     * @dbColumn data
     * @fieldType json
     * @dbOptions notnull = false
     * @var string $data - The data of the row
     */
    public $data;

}