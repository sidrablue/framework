<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 */

declare(strict_types=1);

namespace BlueSky\Framework\Entity;

use BlueSky\Framework\Object\Entity\Base;

/**
 * Notification base class
 * @package BlueSky\Framework\Entity
 * @dbEntity true
 */
class Notification extends Base
{

    protected $tableName = 'sb__notification';
    public const TABLE_NAME = 'sb__notification';

    /**
     * @dbColumn parent_id
     * @fieldType fkey
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $parent_id - The ID of the parent notification
     */
    public $parent_id;

    /**
     * @dbColumn object_ref
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @var string $object_ref - The reference object
     */
    public $object_ref;

    /**
     * @dbColumn object_id
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $object_id - The ID of the reference object
     */
    public $object_id;

    /**
     * @dbColumn user_id
     * @fieldType fkey
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $user_id - the ID of the user that this notification is for
     */
    public $user_id;

    /**
     * @dbColumn subject
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @var string $subject - The subject of the notification
     */
    public $subject;

    /**
     * @dbColumn description
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @var string $description - The $description of the notification
     */
    public $description;

    /**
     * @dbColumn level
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @var string $level - level of the notification, e.g. info, error, warning, alert, critical, etc
     */
    public $level;

    /**
     * @dbColumn view_url
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @var string $view_url - The URL at which to find more information
     */
    public $view_url;

    /**
     * @dbColumn download_url
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @var string $download_url - The download URL for the attached file
     */
    public $download_url;

    /**
     * @dbColumn date_read
     * @fieldType datetime
     * @dbOptions notnull = false
     * @var \DateTime $date - date that the notification was read
     */
    public $date_read;

    /**
     * @dbColumn confirmation_required
     * @fieldType boolean
     * @dbOptions notnull = false
     * @var bool $confirmation_required - true if a confirmation is required for this notifcation
     */
    public $confirmation_required;

    /**
     * @dbColumn date_confirmed
     * @fieldType datetime
     * @dbOptions notnull = false
     * @var \DateTime $date_confirmed - date that the notification was confirmed
     */
    public $date_confirmed;

    /**
     * @dbColumn data
     * @fieldType string
     * @dbOptions length = 1024, notnull = false
     * @var string $data - Any data for this notification
     */
    public $data;

    private function validate(array $data): bool
    {
        if (!$this->subject && (isset($data['subject']) && !$data['subject']) && (!$data || !isset($data['subject']))) {
            throw new \InvalidArgumentException("Notifications require a subject");
        }
        return true;
    }
    protected function beforeInsert($data)
    {
        $this->validate($data);
        return parent::beforeInsert($data);
    }
    protected function beforeUpdate($data)
    {
        $this->validate($data);
        return parent::beforeUpdate($data);
    }

}