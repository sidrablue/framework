<?php
namespace BlueSky\Framework\Entity\Master;

use BlueSky\Framework\Object\Entity\Base;

/**
 * Class Tag
 * @package BlueSky\Framework\Entity\Tag
 * @dbEntity false
 * @deprecated this file is being removed. Use the Tag entity in MasterBase App
 */
class TagMaster extends Base
{

    /**
     * @var string $tableName
     * */
    protected $tableName = 'sb__tag';
    public const TABLE_NAME = 'sb__tag';

    /**
     * @dbColumn value
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var string $value - The string value of the tag
     */
    public $value;

    /**
     * @dbColumn account_ref
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var string $account_ref - The string value of the tag
     */
    public $account_ref;

    /**
     * @dbColumn display_value
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var string $display_value - The string value of the tag
     */
    public $display_value;

    /**
     * @dbColumn colour_code
     * @fieldType string
     * @dbOptions length=32, notnull = false
     * @var string $colour_code - colour code
     */
    public $colour_code;

    /**
     * @dbColumn weighting
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var string $weighting - weighting
     */
    public $weighting;

    /**
     * @dbColumn is_active
     * @fieldType string
     * @dbOptions notnull = false, default = true
     * @dbIndex true
     * @var boolean $active - is active
     */
    public $is_active;

    /**
     * @dbColumn is_restricted
     * @fieldType string
     * @dbOptions notnull = false, default = false
     * @dbIndex true
     * @var boolean $is_restricted - is restricted
     */
    public $is_restricted;

    /**
     * Define the read DB function so that it can be overwritten
     * @access protected
     * @return \Doctrine\DBAL\Connection
     * */
    protected function getReadDb()
    {
        return $this->getState()->getMasterDb();
    }

    /**
     * Define the write DB function so that it can be overwritten
     * @access protected
     * @return \Doctrine\DBAL\Connection
     * */
    protected function getWriteDb()
    {
        return $this->getState()->getMasterDb();
    }

}