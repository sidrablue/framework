<?php
namespace BlueSky\Framework\Entity\Schema;

use Doctrine\DBAL\Query\QueryBuilder;
use BlueSky\Framework\Object\Entity\Base as BaseEntity;

/**
 * @dbEntity true
 */
class Relation extends BaseEntity
{

    public const ALL_RELATIONS = 'all'; /// CODE USE ONLY, NOT TO BE SAVED IN DB
    public const NO_RELATIONS = 'none'; /// CODE USE ONLY, NOT TO BE SAVED IN DB

    public const ONE_TO_ONE = 'one_to_one';
    public const ONE_TO_MANY = 'one_to_many';
    public const MANY_TO_ONE = 'many_to_one';
    public const MANY_TO_MANY = 'many_to_many';
    public const OBJECT_REF_ID_OO = 'object_ref_id_oo'; /// One To One Object ref/id. Object key value must be set for this one

    public const CONNECTION_TYPE_CORE = 'core';
    public const CONNECTION_TYPE_INTEGRATION = 'integration';
    public const CONNECTION_TYPE_VIRTUAL = 'virtual';

    public const APP_REFERENCE_SYSTEM = 'system';

    public const KEY_MAP_TYPE = [
        'ONE_TO_ONE' => self::ONE_TO_ONE,
        'ONE_TO_MANY' => self::ONE_TO_MANY,
        'MANY_TO_ONE' => self::MANY_TO_ONE,
        'MANY_TO_MANY' => self::MANY_TO_MANY,
        'OBJECT_REF_ID_OO' => self::OBJECT_REF_ID_OO,
    ];

    public const KEY_MAP_CONNECTION_TYPE = [
        'CONNECTION_TYPE_CORE' => self::CONNECTION_TYPE_CORE,
        'CONNECTION_TYPE_INTEGRATION' => self::CONNECTION_TYPE_INTEGRATION,
        'CONNECTION_TYPE_VIRTUAL' => self::CONNECTION_TYPE_VIRTUAL,
    ];

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__schema_relation';
    public const TABLE_NAME = 'sb__schema_relation';

    /**
     * @fieldType string
     * @dbOptions length = 500, notnull = true
     * @dbIndex true
     * @var string $from_entity_reference
     */
    public $from_entity_reference;

    /**
     * @fieldType string
     * @dbOptions length = 500, notnull = true
     * @dbIndex true
     * @var string $to_entity_reference
     */
    public $to_entity_reference;

    /**
     * @fieldType string
     * @dbOptions length = 255, notnull = true
     * @var string $from_field_reference
     */
    public $from_field_reference;

    /**
     * @fieldType string
     * @dbOptions length = 255, notnull = true
     * @var string $to_field_reference
     */
    public $to_field_reference;

    /**
     * @fieldType string
     * @dbOptions length = 25, notnull = true, default = one_to_one
     * @var string $type
     */
    public $type = self::ONE_TO_ONE;

    /**
     * @fieldType string
     * @dbOptions length = 25, notnull = true, default = core
     * @var string $connection_type
     */
    public $connection_type;

    /**
     * @fieldType string
     * @dbOptions length = 500, notnull = false
     * @var string $to_entity_reference
     */
    public $mtm_connection_entity_reference;

    /**
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @var string $mtm_self_connection_field_reference
     */
    public $mtm_self_connection_field_reference;

    /**
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @var string $mtm_foreign_connection_field_reference
     */
    public $mtm_foreign_connection_field_reference;

    /**
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @var string $foreign_object_key_value
     */
    public $foreign_object_key_value;

    /**
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @var string $name - The name of the field
     */
    public $name;

    /**
     * @fieldType text
     * @dbOptions notnull = false
     * @var string $description - The description of the field
     */
    public $description;

    /**
     * @fieldType boolean
     * @dbOptions notnull = true, default = true
     * @dbIndex true
     * @var boolean $is_active - true if this object is still active
     */
    public $is_active = true;

    /**
     * Assume that there will not be any more than 9999 unique sort_order options. Eg sort_order must be less than 10000
     * @fieldType integer
     * @dbOptions notnull = true, default = 10000
     * @dbIndex true
     * @var integer $sort_order - sort order
     */
    public $sort_order;

    /**
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @var string $app_reference - The app where this relation originated from
     */
    public $app_reference;

    public function reverse(): Relation
    {
        $output = new Relation($this->getState());
        $output->to_entity_reference = $this->from_entity_reference;
        $output->from_entity_reference = $this->to_entity_reference;
        $output->to_field_reference = $this->from_field_reference;
        $output->from_field_reference = $this->to_field_reference;
        $output->mtm_self_connection_field_reference = $this->mtm_foreign_connection_field_reference;
        $output->mtm_foreign_connection_field_reference = $this->mtm_self_connection_field_reference;
        $output->mtm_connection_entity_reference = $this->mtm_connection_entity_reference;
        $output->type = $this->type;
        $output->is_active = $this->is_active;
        $output->connection_type = $this->connection_type;
        return $output;
    }

    public function leftJoin(QueryBuilder $q, string $toAlias = '', ?string $fromAlias = null): string
    {
        if (empty($fromAlias)) {
            $entity = $this->getState()->getSchema()->getEntityByReference($this->from_entity_reference);
            $fromTableName = $entity->getEntityTableName();
            foreach ($q->getQueryPart('from') as $from) {
                if (!empty($from) && !empty($from['table']) && $from['table'] == $fromTableName) {
                    $alias = $from['table'];
                    if (!empty($from['alias'])) {
                        $alias = $from['alias'];
                    }
                    $fromAlias = $alias;
                    break;
                }
            }
        }

        $entity = $this->getState()->getSchema()->getEntityByReference($this->to_entity_reference);
        $toTableName = $entity->getEntityTableName();

        $toAlias = $toAlias ?? uniqid('bd_');

        $fromFieldReference = $this->from_field_reference;
        $toFieldReference = $this->to_field_reference;

        switch ($this->type) {
            case self::ONE_TO_ONE:
            case self::ONE_TO_MANY:
                $q->leftJoin($fromAlias, $toTableName, $toAlias, "{$fromAlias}.{$fromFieldReference} = {$toAlias}.{$toFieldReference} and {$toAlias}.lote_deleted is null");
                break;
            case self::MANY_TO_MANY:
                $joinEntity = $this->getState()->getSchema()->getEntityByReference($this->mtm_connection_entity_reference);
                $joinTableName = $joinEntity->getEntityTableName();
                $joinAlias = uniqid('bd_');
                $joinAliasFromFieldReference = $this->mtm_self_connection_field_reference;
                $joinAliasToFieldReference = $this->mtm_self_connection_field_reference;
                $q->leftJoin($fromAlias, $joinTableName, $joinAlias, "{$fromAlias}.{$fromFieldReference} = {$joinAlias}.{$joinAliasFromFieldReference} and {$joinAlias}.lote_deleted is null")
                    ->leftJoin($joinAlias, $toTableName, $toAlias, "{$joinAlias}.{$joinAliasToFieldReference} = {$toAlias}.{$toFieldReference} and {$toAlias}.lote_deleted is null");
                break;
            case self::OBJECT_REF_ID_OO:
                $paramRef = uniqid('bd_param_');
                $paramValue = uniqid('bd_value_');
                $q->leftJoin($fromAlias, $toTableName, $toAlias, "{$toAlias}.object_ref = :{$paramRef} and {$toAlias}.object_key = :{$paramValue} and {$fromAlias}.{$fromFieldReference} = {$toAlias}.{$toFieldReference} and {$toAlias}.lote_deleted is null")
                    ->setParameter($paramRef, $this->from_entity_reference)
                    ->setParameter($paramValue, $this->foreign_object_key_value);
                break;
        }

        return $toAlias;
    }

    public function containsValidJoin($input): bool
    {
        if (is_array($input)) {
            $input = (object) $input;
        }
        $toReference = $this->to_field_reference;
        return !empty($input->{$toReference});
    }

    /**
     * @param BaseEntity|VirtualEntity $fromEntity
     * @return BaseEntity|VirtualEntity|null
     * @throws Exception
     */
    public function createInstance($fromEntity)
    {
        $toEntity = $this->getState()->getSchema()->getEntityByReference($this->to_entity_reference);
        $foreignKey = $this->to_field_reference;
        $selfKey = $this->from_field_reference;
        if ($toEntity->is_virtual) {
            $output = $this->getState()->getEntityManager()->createInstance($toEntity);
        } else {
            $toClassName = $toEntity->class_name;
            $output = new $toClassName($this->getState());
        }
        if ($output !== null) {
            if ($this->type === self::ONE_TO_ONE || $this->type === self::ONE_TO_MANY) {
                $output->{$foreignKey} = $fromEntity->{$selfKey};
            } elseif ($this->type === self::OBJECT_REF_ID_OO) {
                /// In the current system, only Base entity items can be object ref/key/id type
                $output->object_ref = $fromEntity::getObjectRef();
                $output->object_key = $this->foreign_object_key_value;
                $output->object_id = $fromEntity->{$selfKey};
            } else {
                throw new Exception('Relation type not supported yet');
            }
        }
        return $output;
    }

    /**
     * @param BaseEntity|VirtualEntity $fromEntity
     * @return BaseEntity|VirtualEntity|null
     * @throws Exception
     */
    public function loadInstance($fromEntity)
    {
        $output = null;
        $toEntity = $this->getState()->getSchema()->getEntityByReference($this->to_entity_reference);
        $foreignKey = $this->to_field_reference;
        $selfKey = $this->from_field_reference;
        $selfValue = $fromEntity->{$selfKey};
        if ($toEntity->is_virtual) {
            $output = $this->getState()->getEntityManager()->getByFilter($this->to_entity_reference, [
                $foreignKey => $selfValue
            ], false);
        } else {
            $toClassName = $toEntity->class_name;
            /** @var BaseEntity $output */
            $output = new $toClassName($this->getState());
            if ($this->type === self::ONE_TO_ONE || $this->type === self::ONE_TO_MANY) {
                if (!$output->loadByField($foreignKey, $selfValue)) {
                    $output = null;
                }
            } elseif ($this->type === self::OBJECT_REF_ID_OO) {
                if (!$output->loadByFields([
                    'object_ref' => $fromEntity::getObjectRef(),
                    'object_key' => $this->foreign_object_key_value,
                    'object_id' => $selfValue
                ])) {
                    $output = null;
                }
            } else {
                throw new Exception('Relation type not supported yet');
            }
        }
        return $output;
    }

    public function __destruct()
    {
        
    }

}
