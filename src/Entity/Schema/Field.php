<?php
/**
 * Copyright (c) BlueSky Digital Labs Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Entity\Schema;

use BlueSky\Framework\Entity\Schema\Entity as SchemaEntity;
use BlueSky\Framework\Object\Entity\Base;
use BlueSky\Framework\Object\Entity\Validation\Validators\ValidatorBase;
use BlueSky\Framework\Object\Entity\Validation\Validators\Nil as NilValidator;
use BlueSky\Framework\Object\Entity\Validation\ValidatorSet;
use BlueSky\Framework\Schema\FieldOptions;
use BlueSky\Framework\Util\Strings;

/**
 * Base class for Item
 * @package Lote\App\Staff\Entity
 * @dbEntity true
 */
final class Field extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__schema_field';
    public const TABLE_NAME = 'sb__schema_field';

    public const FLAG_QUERYABLE_KEY = 'FLAG_QUERYABLE';
    public const FLAG_IMPORTABLE_KEY = 'FLAG_IMPORTABLE';
    public const FLAG_EXPORTABLE_KEY = 'FLAG_EXPORTABLE';
    public const FLAG_LISTABLE_KEY = 'FLAG_LISTABLE';

    public const FLAG_NONE = 0;
    public const FLAG_ALL = 0xFFFFFFFF; /// 2^32 - 1 (32 bit max unsigned number)
    public const FLAG_PASSTHROUGH = -1;

    public const FLAG_QUERYABLE = 1;
    public const FLAG_IMPORTABLE = 2;
    public const FLAG_EXPORTABLE = 4;
    public const FLAG_LISTABLE = 8;

    public const FIELD_TYPE_PRIMARY_KEY_INT = 'primary_key_int';
    public const FIELD_TYPE_DATETIME = 'datetime';
    public const FIELD_TYPE_DATETIME_IMMUTABLE = 'datetime_immutable';
    public const FIELD_TYPE_DATE = 'date';
    public const FIELD_TYPE_DATE_IMMUTABLE = 'date_immutable';
    public const FIELD_TYPE_TIME = 'time';
    public const FIELD_TYPE_ENUM = 'enum';
    public const FIELD_TYPE_STRING = 'string';
    public const FIELD_TYPE_TEXT = 'text';
    public const FIELD_TYPE_LONGTEXT = 'longtext';
    public const FIELD_TYPE_EMAIL = 'email';
    public const FIELD_TYPE_FILE = 'file';
    public const FIELD_TYPE_URL = 'url';
    public const FIELD_TYPE_PASSWORD = 'password';
    public const FIELD_TYPE_COUNTRY = 'country';
    public const FIELD_TYPE_BOOLEAN = 'boolean';
    public const FIELD_TYPE_INTEGER = 'integer';
    public const FIELD_TYPE_BIGINTEGER = 'bigint';
    public const FIELD_TYPE_ENUM_INTEGER = 'enum_integer';
    public const FIELD_TYPE_JSON = 'json';
    public const FIELD_TYPE_SINGLE_SELECT = 'single_select';
    public const FIELD_TYPE_MULTI_SELECT = 'multi_select';
    public const FIELD_TYPE_CHECKBOX = 'checkbox';
    public const FIELD_TYPE_RADIOBUTTON = 'radiobutton';
    public const FIELD_TYPE_FIXED_COLUMN_MATRIX = 'fixed_column_matrix';
    public const FIELD_TYPE_MATRIX = 'matrix';
    public const FIELD_TYPE_PUBLIC_GROUP = 'public_group';
    public const FIELD_TYPE_DECIMAL = 'decimal';
    public const FIELD_TYPE_CURRENCY = 'currency';
    public const FIELD_TYPE_FKEY = 'fkey';
    public const FIELD_TYPE_TEXTAREA = 'textarea';
    public const FIELD_TYPE_HEADING = 'heading';
    public const FIELD_TYPE_HEADER = 'header';
    public const FIELD_TYPE_MESSAGE = 'message';
    public const FIELD_TYPE_RECAPTCHA = 'recaptcha';
    public const FIELD_TYPE_TELEPHONE = 'telephone';
    public const FIELD_TYPE_IMAGE = 'image';
    public const FIELD_TYPE_FOOTING = 'footing';
    public const FIELD_TYPE_FOOTER = 'footer';
    public const FIELD_TYPE_CANVAS = 'canvas';

    private const FLAG_KEY_TO_MASK_MAP = [
        self::FLAG_QUERYABLE_KEY => self::FLAG_QUERYABLE,
        self::FLAG_IMPORTABLE_KEY => self::FLAG_IMPORTABLE,
        self::FLAG_EXPORTABLE_KEY => self::FLAG_EXPORTABLE,
        self::FLAG_LISTABLE_KEY => self::FLAG_LISTABLE,
    ];

    /**
     * @dbColumn reference
     * @fieldType string
     * @dbOptions notnull = true, length=500
     * @dbIndex true
     * @var string $reference - the reference of the field in the form of <ENTITY_REF>_DOT_<FIELD_REF>
     */
    public $reference;

    public const FIELD_TO_DB_MAP = [
        self::FIELD_TYPE_PRIMARY_KEY_INT  => 'integer',

        self::FIELD_TYPE_DATETIME  => 'datetime',
        self::FIELD_TYPE_DATETIME_IMMUTABLE  => 'datetime',
        self::FIELD_TYPE_DATE  => 'date',
        self::FIELD_TYPE_DATE_IMMUTABLE  => 'date',
        self::FIELD_TYPE_TIME  => 'time',

        self::FIELD_TYPE_ENUM  => 'string',
        self::FIELD_TYPE_STRING  => 'string',
        self::FIELD_TYPE_TEXT  => 'text',
        self::FIELD_TYPE_LONGTEXT  => 'text',
        self::FIELD_TYPE_EMAIL  => 'string',
        self::FIELD_TYPE_FILE  => 'string',
        self::FIELD_TYPE_URL  => 'string',
        self::FIELD_TYPE_PASSWORD  => 'string',
        self::FIELD_TYPE_COUNTRY  => 'string',

        self::FIELD_TYPE_BOOLEAN  => 'boolean',

        self::FIELD_TYPE_INTEGER  => 'integer',
        self::FIELD_TYPE_BIGINTEGER  => 'bigint',
        self::FIELD_TYPE_ENUM_INTEGER  => 'integer',

        self::FIELD_TYPE_JSON  => 'json',
        self::FIELD_TYPE_SINGLE_SELECT  => 'json',
        self::FIELD_TYPE_MULTI_SELECT  => 'json',
        self::FIELD_TYPE_CHECKBOX  => 'json',
        self::FIELD_TYPE_RADIOBUTTON  => 'json',
        self::FIELD_TYPE_MATRIX  => 'json',
        self::FIELD_TYPE_PUBLIC_GROUP  => 'json',
        'radio_button' => 'json',
        self::FIELD_TYPE_FIXED_COLUMN_MATRIX => 'json',

        self::FIELD_TYPE_DECIMAL  => 'decimal',
        self::FIELD_TYPE_CURRENCY  => 'decimal',

        self::FIELD_TYPE_FKEY  => 'integer',

        self::FIELD_TYPE_TEXTAREA  => 'text',
        self::FIELD_TYPE_HEADING  => 'text',
        self::FIELD_TYPE_HEADER  => 'text',
        self::FIELD_TYPE_MESSAGE  => 'text',
        self::FIELD_TYPE_RECAPTCHA  => 'json',
        self::FIELD_TYPE_TELEPHONE  => 'string',
        self::FIELD_TYPE_IMAGE  => 'string',
        self::FIELD_TYPE_FOOTING  => 'string',
        self::FIELD_TYPE_FOOTER  => 'text',

        self::FIELD_TYPE_CANVAS => 'text',
    ];

    /**
     * @dbColumn schema_entity_ref
     * @fieldType string
     * @dbOptions notnull = true, length=500
     * @dbIndex true
     * @var string $schema_entity_ref - the entity that the field applies to
     */
    public $schema_entity_ref;

    /**
     * @dbColumn field_type
     * @fieldType string
     * @dbOptions length = 255, notnull = true
     * @var string $field_type - the field type, e.g. 'single_select', 'wysiwyg' etc...
     */
    public $field_type;

    /**
     * @dbColumn db_type
     * @fieldType string
     * @dbOptions length = 255, notnull = true
     * @var string $db_type - the database field type, e.g. 'string', 'text', 'int.array', etc...
     */
    public $db_type;

    /**
     * @dbColumn db_options
     * @fieldType json
     * @dbOptions notnull = false
     * @var string $db_options - database options for the field
     */
    public $db_options;

    /**
     * @dbColumn db_index
     * @fieldType boolean
     * @dbOptions notnull = true, default = false
     * @var boolean $db_index - true to indicate an index on this field
     */
    public $db_index;

    /**
     * @dbColumn name
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @var string $name - The name of the field
     */
    public $name;

    /**
     * @dbColumn description
     * @fieldType text
     * @dbOptions notnull = false
     * @var string $description - The description of the field
     */
    public $description;

    /**
     * @dbColumn default_value
     * @fieldType text
     * @dbOptions notnull = false
     * @var mixed $default_value - the default value of the field
     */
    public $default_value;

    /**
     * @dbColumn sort_order
     * @fieldType integer
     * @dbOptions notnull = true, default = 0
     * @dbIndex true
     * @var int $sort_order - the sort order of this object
     */
    public $sort_order = 0;

    /**
     * @dbColumn is_active
     * @fieldType boolean
     * @dbOptions notnull = true, default = true
     * @dbIndex true
     * @var boolean $is_active - true if this object is still active
     */
    public $is_active;

    /**
     * @dbColumn is_hidden
     * @fieldType boolean
     * @dbOptions notnull = true, default = false
     * @dbIndex true
     * @var boolean $is_hidden - true if this object is hidden
     */
    public $is_hidden;

    /**
     * @dbColumn is_system_hidden
     * @fieldType boolean
     * @dbOptions notnull = true, default = false
     * @var boolean $is_system_hidden - true if this object is hidden form a system point of view
     */
    public $is_system_hidden;

    /**
     * @dbColumn is_read_only
     * @fieldType boolean
     * @dbOptions notnull = true, default = false
     * @var boolean $is_read_only - defines if a field is read only or not
     */
    public $is_read_only;

    /**
     * @dbColumn is_unique
     * @fieldType boolean
     * @dbOptions notnull = true, default = false
     * @var boolean $is_unique - defines if a field is unique or not
     */
    public $is_unique;

    /**
     * @dbColumn is_mandatory
     * @fieldType boolean
     * @dbOptions notnull = true, default = false
     * @var boolean $is_mandatory - true if the field is a mandatory one
     */
    public $is_mandatory;


    /**
     * @dbColumn config_data
     * @fieldType json
     * @dbOptions notnull = false
     * @param array $config_data
     */
    public $config_data;

    /**
     * @dbColumn source
     * @fieldType text
     * @dbOptions notnull = false
     * @var string $source - Script to apply to the field
     */
    public $source;

    /**
     * @dbColumn mirror_object
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @var string $mirror_object - storage type
     */
    public $mirror_object;

    /**
     * @dbColumn mirror_field
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @var string $mirror_field - the db field to which this input matches
     */
    public $mirror_field;

    /**
     * @dbColumn fk_object
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @var string $fk_object - the class reference to a php entity file
     */
    public $fk_object;

    /**
     * @dbColumn options_structure
     * @fieldType json
     * @dbOptions notnull = false
     * @var array $options_structure - The structure of the field if its an option
     */
    public $options_structure;

    /**
     * @dbColumn is_virtual
     * @fieldType boolean
     * @dbOptions notnull = true, default = true
     * @var boolean $is_virtual
     */
    public $is_virtual;

    /**
     * @dbColumn flags
     * @fieldType integer
     * @dbOptions notnull = true, default = 0
     * @var int $flags
     */
    public $flags;

    /** @var SchemaEntity $internalSchema  */
    private static $internalSchema = null; /// This is used to speed up getSchema

    /** @var FieldOptions $fieldOptions - Holds reference to the options entries */
    private $fieldOptions = null;

    /** @var Entity $parentEntity */
    private $parentEntity = null;

    public function __clone() { return new Field($this->getState(), $this->getData()); }

    protected function afterLoad()
    {
        if ($this->id > 0) {
            $this->loadFieldOptions();
        }

        if ($this->config_data && Strings::isJson($this->config_data)) {
            $this->config_data = json_decode($this->config_data, true);
        }

        if (($this->default_value === null || $this->default_value === '') && (!empty($this->db_options))) {
            $opts = $this->db_options ?? [];
            $this->default_value = $opts['default'] ?? null;
        }
    }

    protected function afterCreate()
    {
        if ($this->id > 0) {
            $this->loadFieldOptions();
        }

        if ($this->config_data && Strings::isJson($this->config_data)) {
            $this->config_data = json_decode($this->config_data, true);
        }
    }

    private function loadFieldOptions(): void
    {
        if (!empty($this->options_structure)) {
            $this->fieldOptions = new FieldOptions($this->options_structure);
        }
    }

    public function getFieldOptions(): ?FieldOptions
    {
        return $this->fieldOptions;
    }

    public function isExtension(): bool
    {
        /// A field can only an extension when it is virtual but its parent entity is not virtual
        /// Basically any field added to a core entity eg User
        return !is_null($this->parentEntity) && $this->is_virtual && !$this->parentEntity->is_virtual;
    }

    public function isForeignKey(): bool { return !empty($this->fk_object); }

    public function getValidator(): ValidatorBase
    {
        if (is_string($this->config_data)) {
            $this->config_data = json_decode($this->config_data, true);
        }
        if (is_array($this->config_data) && !empty($this->config_data['validation'])) {
            $output = ValidatorSet::createValidatorSet($this->reference, $this->config_data['validation']);
        } elseif ($validator = ValidatorSet::createValidatorSet($this->reference, [
            'validatorSet' => [
                'type' => $this->field_type,
            ]
        ])) {
            $output = $validator;
        } else {
            $output = new NilValidator($this->reference);
        }
        return $output;
    }

    public function setParentEntity(Entity $entity): void { $this->parentEntity = $entity; }

    public function getParentEntity(): ?Entity { return $this->parentEntity; }

    public function setData($data)
    {
        parent::setData($data);
        $this->loadFieldOptions();
    }

    public static function createFlag(int...$flags): int { return array_reduce($flags, function ($carry, $item) { return $carry | $item; }, 0); }

    public function hasFlag(int $flag): bool {
        return $flag === self::FLAG_PASSTHROUGH || ($this->flags & $flag) > 0;
    }

    public function hasFlags(int...$flags): bool {
        $count = count($flags);
        if ($count === 0) {
            $flags = 0;
        } elseif ($count === 1) {
            $flags = $flags[0];
        } else {
            $flags = self::createFlag($flags);
        }
        return $this->hasFlag($flags);
    }

    public static function getKeyValue(string $key): ?int { return self::FLAG_KEY_TO_MASK_MAP[$key] ?? null; }

    /**
     * @param int $flags - Masked Flags. You can OR the integers together to get multiple flags enabled
     */
    public function setFlags(int $flags): void { $this->flags = $flags; }

    public function getFlags(): array
    {
        $output = [];
        foreach (self::FLAG_KEY_TO_MASK_MAP as $key => $value) {
            $output[$key] = $this->hasFlag($key);
        }
        return $output;
    }

    public function enableFlag(int $flag): void { $this->flags |= $flag; }

    public function disableFlag(int $flag): void { $this->flags &= ~$flag; }

    public function __destruct()
    {

    }

    protected function onClearCachedData()
    {
        parent::onClearCachedData();
        if ($this->parentEntity) {
            $this->parentEntity->clearCachedData();
        }
    }

    public function getAnnotatedColumns()
    {
        static $dataSet = null;
        if ($dataSet === null) {
            $dataSet = parent::getAnnotatedColumns();
        }
        return $dataSet;
    }

    protected function getDefaultFieldValue($fieldName)
    {
        switch ($fieldName) {
            case 'db_index':
            case 'is_hidden':
            case 'is_read_only':
            case 'is_unique':
            case 'is_mandatory':
            case 'is_system_hidden':
                $output = false; break;
            case 'sort_order':
            case 'flags':
                $output = 0; break;
            case 'is_active':
            case 'is_virtual':
                $output = true; break;
            default: $output = null; break;
        }
        return $output;
    }

    protected function setupEntityProperties()
    {
        $this->entityProperties = [
            'id' => 'id',
            'lote_access' => 'lote_access',
            'lote_author_id' => 'lote_author_id',
            'lote_created' => 'lote_created',
            'lote_deleted' => 'lote_deleted',
            'lote_deleted_by' => 'lote_deleted_by',
            'lote_updated' => 'lote_updated',
            'reference' => 'reference',
            'schema_entity_ref' => 'schema_entity_ref',
            'field_type' => 'field_type',
            'db_type' => 'db_type',
            'db_options' => 'db_options',
            'db_index' => 'db_index',
            'name' => 'name',
            'description' => 'description',
            'default_value' => 'default_value',
            'sort_order' => 'sort_order',
            'is_active' => 'is_active',
            'is_hidden' => 'is_hidden',
            'is_read_only' => 'is_read_only',
            'is_unique' => 'is_unique',
            'is_mandatory' => 'is_mandatory',
            'config_data' => 'config_data',
            'source' => 'source',
            'mirror_object' => 'mirror_object',
            'mirror_field' => 'mirror_field',
            'fk_object' => 'fk_object',
            'options_structure' => 'options_structure',
            'is_virtual' => 'is_virtual',
            'flags' => 'flags',
            'is_system_hidden' => 'is_system_hidden',
        ];
        unset(
            $this->id, $this->lote_access, $this->lote_author_id, $this->lote_created, $this->lote_deleted, $this->lote_deleted_by, $this->lote_updated,
            $this->reference, $this->schema_entity_ref, $this->field_type, $this->db_type, $this->db_options, $this->db_index,
            $this->name, $this->description, $this->default_value, $this->sort_order, $this->is_active, $this->is_hidden,
            $this->is_read_only, $this->is_unique, $this->is_mandatory, $this->config_data, $this->source, $this->mirror_object,
            $this->mirror_field, $this->fk_object, $this->options_structure, $this->is_virtual, $this->flags, $this->is_system_hidden
        );
    }

    private function createCoreFieldEntity(string $reference, string $entityRef, string $fieldType, string $dbType): Field
    {
        $fe = new Field($this->getState());
        $fe->reference = $reference;
        $fe->schema_entity_ref = $entityRef;
        $fe->field_type = $fieldType;
        $fe->db_type = $dbType;
        $fe->is_virtual = false;
        return $fe;
    }

    public function getSchema(): SchemaEntity {
        if (is_null($this->schema)) {
            if (is_null(self::$internalSchema)) {
                $e = new Entity($this->getState());
                $e->reference = Entity::getObjectRef();
                $e->name = 'Field';
                $e->description = 'Core Field Schema';
                $e->sort_order = 0;
                $e->is_active = true;
                $e->is_hidden = true;
                $e->class_name = get_class($e);
                $e->is_virtual = false;

                $fReference = $this->createCoreFieldEntity('reference', $e->reference, 'string', 'string');
                $e->addEntityField($fReference);

                $fSchemaEntityRef = $this->createCoreFieldEntity('schema_entity_ref', $e->reference, 'string', 'string');
                $e->addEntityField($fSchemaEntityRef);

                $fFieldType = $this->createCoreFieldEntity('field_type', $e->reference, 'string', 'string');
                $e->addEntityField($fFieldType);

                $fName = $this->createCoreFieldEntity('name', $e->reference, 'string', 'string');
                $e->addEntityField($fName);

                $fDescription = $this->createCoreFieldEntity('description', $e->reference, 'string', 'string');
                $e->addEntityField($fDescription);

                $fSortOrder = $this->createCoreFieldEntity('sort_order', $e->reference, 'integer', 'integer');;
                $e->addEntityField($fSortOrder);

                $fIsActive = $this->createCoreFieldEntity('is_active', $e->reference, 'boolean', 'boolean');
                $e->addEntityField($fIsActive);

                $fIsHidden = $this->createCoreFieldEntity('is_hidden', $e->reference, 'boolean', 'boolean');
                $e->addEntityField($fIsHidden);

                $fDbType = $this->createCoreFieldEntity('db_type', $e->reference, 'string', 'string');
                $e->addEntityField($fDbType);

                $fDbOptions = $this->createCoreFieldEntity('db_options', $e->reference, 'json', 'json');
                $e->addEntityField($fDbOptions);

                $fDbIndex = $this->createCoreFieldEntity('db_index', $e->reference, 'boolean', 'boolean');
                $e->addEntityField($fDbIndex);

                $fDefaultValue = $this->createCoreFieldEntity('default_value', $e->reference, 'string', 'string');
                $e->addEntityField($fDefaultValue);

                $fIsReadOnly = $this->createCoreFieldEntity('is_read_only', $e->reference, 'boolean', 'boolean');
                $e->addEntityField($fIsReadOnly);

                $fIsUnique = $this->createCoreFieldEntity('is_unique', $e->reference, 'boolean', 'boolean');
                $e->addEntityField($fIsUnique);

                $fIsMandatory = $this->createCoreFieldEntity('is_mandatory', $e->reference, 'boolean', 'boolean');
                $e->addEntityField($fIsMandatory);

                $fConfigData = $this->createCoreFieldEntity('config_data', $e->reference, 'json', 'json');
                $e->addEntityField($fConfigData);

                $fSource = $this->createCoreFieldEntity('source', $e->reference, 'string', 'string');
                $e->addEntityField($fSource);

                $fMirrorObject = $this->createCoreFieldEntity('mirror_object', $e->reference, 'string', 'string');
                $e->addEntityField($fMirrorObject);

                $fMirrorField = $this->createCoreFieldEntity('mirror_field', $e->reference, 'string', 'string');
                $e->addEntityField($fMirrorField);

                $fFkObject = $this->createCoreFieldEntity('fk_object', $e->reference, 'string', 'string');
                $e->addEntityField($fFkObject);

                $fOptionsStructure = $this->createCoreFieldEntity('options_structure', $e->reference, 'json', 'json');
                $e->addEntityField($fOptionsStructure);

                $fIsVirtual = $this->createCoreFieldEntity('is_virtual', $e->reference, 'boolean', 'boolean');
                $e->addEntityField($fIsVirtual);

                $fFlags = $this->createCoreFieldEntity('flags', $e->reference, 'integer', 'integer');
                $e->addEntityField($fFlags);

                $fIsSystemHidden = $this->createCoreFieldEntity('is_system_hidden', $e->reference, 'boolean', 'boolean');
                $e->addEntityField($fIsSystemHidden);

                self::$internalSchema = $e;

                /// The internalSchema needs to be set first, then this loop can be read.
                /// Otherwise, The getLoteFields function will cause a recursive loop
                foreach (SchemaEntity::getLoteFields($this->getState()) as $loteField) {
                    $e->addEntityField($loteField);
                }

                self::$internalSchema = $e;
            }
            $this->schema = self::$internalSchema;
        }

        return $this->schema;
    }

}
