<?php
/**
 * Copyright (c) BlueSky Digital Labs Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Entity\Schema;

use BlueSky\Framework\Object\Entity\Validation\ValidatorGroup;
use BlueSky\Framework\Object\Entity\Validation\Validators\ValidatorBase;
use BlueSky\Framework\Model\Schema\Field as FieldModel;
use BlueSky\Framework\Entity\Schema\Field as FieldEntity;
use BlueSky\Framework\Object\Entity\Base;
use BlueSky\Framework\Model\Schema\Entity as EntityModel;
use BlueSky\Framework\Model\Schema\Relation as RelationModel;
use BlueSky\Framework\Entity\Schema\Relation as RelationEntity;
use BlueSky\Framework\Object\Entity\Validation\Validators\Entity as EntityValidator;
use BlueSky\Framework\Schema\RelationManager;
use BlueSky\Framework\State\Base as BaseState;

/**
 * Base class for Item
 * @package BlueSky\Framework\Entity\Schema
 * @dbEntity true
 */
final class Entity extends Base
{

    public const CHILD_ENTITIES = 'childEntities';
    public const FIELDS = 'fields';
    public const RELATIONS = 'relations';

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__schema_entity';
    public const TABLE_NAME = 'sb__schema_entity';

    /**
     * @dbColumn reference
     * @fieldType string
     * @dbOptions notnull = true, length=500
     * @dbUniqueIndex true
     * @dbIndex true
     * @var string $reference - the unique reference of the object
     */
    public $reference;

    /**
     * @dbColumn name
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @var string $name
     */
    public $name;

    /**
     * @dbColumn description
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @var string $description
     */
    public $description;

    /**
     * @dbColumn sort_order
     * @fieldType integer
     * @dbOptions notnull = true, default = 0
     * @dbIndex true
     * @var int $sort_order
     */
    public $sort_order;

    /**
     * @dbColumn is_active
     * @fieldType boolean
     * @dbOptions notnull = true, default = true
     * @dbIndex true
     * @var boolean $is_active
     */
    public $is_active;

    /**
     * @dbColumn is_hidden
     * @fieldType boolean
     * @dbOptions notnull = true, default = false
     * @dbIndex true
     * @var boolean $is_hidden
     */
    public $is_hidden;

    /**
     * @dbColumn class_name
     * @fieldType string
     * @dbOptions length = 511, notnull = true
     * @dbIndex false
     * @var string $class_name
     */
    public $class_name;

    /**
     * @dbColumn is_virtual
     * @fieldType boolean
     * @dbOptions notnull = true, default = true
     * @var boolean $is_virtual
     */
    public $is_virtual;

    /** @var Entity $internalSchema */
    private static $internalSchema = null;

    /**
     * @var Entity[] $childEntities - The child entities linking to this entity through parent_id
     * */
    private $childEntities = [];

    /**
     * @var Entity|null $parentEntity
     */
    private $parentEntity = null;

    /**
     * @var FieldEntity[] $fields - The fields of this group. This list contains the extension fields as well
     * */
    private $fields = [];

    /**
     * @var FieldEntity[] $extensionFields
     * */
    private $extensionFields = [];

    /** @var RelationManager|null $relations */
    private $relations = null;

    private static $loteFields = null;

    public function getData(): array
    {
        $output = parent::getData();

        $output[self::CHILD_ENTITIES] = [];
        $output[self::FIELDS] = [];
        $output[self::RELATIONS] = [];
        foreach ($this->childEntities as $entity) {
            /** @var Entity $entity */
            $output[self::CHILD_ENTITIES][] = $entity->getData();
        }

        foreach ($this->fields as $field) {
            /** @var FieldEntity $field */
            $output[self::FIELDS][] = $field->getData();
        }

        if ($this->relations) {
            foreach ($this->relations->getRelations() as $relation) {
                $output[self::RELATIONS][] = $relation->getData();
            }
        }

        return $output;
    }

    public function setData($data)
    {
        parent::setData($data);
        $childEntitiesData = $data[self::CHILD_ENTITIES] ?? [];
        foreach ($childEntitiesData as $childEntityData) {
            $se = new Entity($this->getState(), $childEntityData);
            $this->childEntities[$se->reference] = $se;
            $se->setParentEntity($this);
        }

        $childFieldsData = $data[self::FIELDS] ?? [];
        foreach ($childFieldsData as $childFieldData) {
            $sfe = new FieldEntity($this->getState(), $childFieldData);
            $this->fields[$sfe->reference] = $sfe;
        }

        $relations = $data[self::RELATIONS] ?? [];
        if (!empty($relations)) {
            $this->relations = new RelationManager($relations);
        }
    }

    /**
     * Get this object as an array
     * @access public
     * @return array
     * */
    public function toArray()
    {
        $result = parent::toArray();
        $result[self::CHILD_ENTITIES] = [];
        if (!empty($this->childEntities)) {
            foreach ($this->childEntities as $entity) {
                $result[self::CHILD_ENTITIES][] = $entity->toArray();
            }
        }
        return $result;
    }

    /**
     * Get a child entity by its reference
     * @access public
     * @param string $childRef
     * @return Entity|null
     */
    public function getChildEntityByReference(string $childRef): ?Entity
    {
        $result = null;
        foreach($this->childEntities as $entity) {
            if($entity->reference === $childRef) {
                $result = $entity;
                break;
            }
        }
        return $result;
    }

    /** @param Entity $entity */
    public function addChildEntity(Entity $entity): void { $this->childEntities[$entity->reference] = $entity; }

    /** @return Entity[] */
    public function getChildEntities(): array { return $this->childEntities; }

    /**
     * @param string ...$relationTypes
     * @return Entity[]
     */
    public function getChildEntitiesWithRelation(string ...$relationTypes): array
    {
        $entities = $this->getAllChildEntitiesFlat();
        foreach ($relationTypes as $relationType) {
            $relations = $this->relations->getRelationsByType($relationType);
            foreach ($relations as $relation) {
                if (!isset($entities[$relation->to_entity_reference])) {
                    $e = $this->getState()->getSchema()->getEntityByReference($relation->to_entity_reference);
                    $entities[$e->reference] = $e;
                }
            }
        }
        return $entities;
    }

    /** @return Entity[] */
    public function getAllChildEntitiesFlat(): array
    {
        $entities = $this->getAllChildEntitiesFlatRecursive($this);
        return $entities;
    }

    private function getAllChildEntitiesFlatRecursive(Entity $entity): array
    {
        $output = $entity->getChildEntities();
        foreach ($entity->getChildEntities() as $childEntity) {
            array_merge($output, $this->getAllChildEntitiesFlatRecursive($childEntity));
        }
        return $output;
    }

    /**
     * @param FieldEntity[] $entityFields
     */
    public function setEntityFields(array $entityFields): void
    {
        foreach ($entityFields as $field) {
            $this->addEntityField($field);
        }
    }

    /**
     * @param FieldEntity $field
     */
    public function addEntityField(FieldEntity $field): void
    {
        $field->setParentEntity($this);
        $this->fields[$field->reference] = $field;
        if ($field->isExtension()) {
            $this->extensionFields[$field->reference] = $field;
        }
    }

    public function removeEntityField(FieldEntity $field): void { unset($this->fields[$field->reference]); }

    /**
     * @return FieldEntity[]
     */
    public function getEntityFields(): array { return $this->fields; }

    public function getEntityExtensionFields(): array { return $this->extensionFields; }

    public function setParentEntity(Entity $parent): void { $this->parentEntity = $parent; }

    public function getParentEntity(): ?Entity { return $this->parentEntity; }

    public function getParentEntityReference(): ?string
    {
        $output = null;
        $pos = strrpos($this->reference, '\\');
        if ($pos !== false) {
            $output = substr($this->reference, 0, $pos);
        }
        return $output;
    }

    /**
     * Get the name of the column that points to the parent entity
     * @access public
     * @return string
     * */
    public function getParentForeignKeyColumn(): ?string
    {
        $result = null;
        if ($this->parentEntity) {
            foreach ($this->fields as $field) {
                /** @var \BlueSky\Framework\Entity\Schema\Field $field */
                if ($field->fk_object) {
                    $result = $field->reference;
                    break;
                }
            }
        }
        return $result;
    }

    public function getEntityTableName(bool $forceDynamicGeneratedName = false): string
    {
        $output = null;
        if ($this->class_name && !$forceDynamicGeneratedName) {
            if ($this->is_virtual) {
                if (!defined('LOTE_DB_OPERATION')) {
                    $instance = $this->getState()->getEntityManager()->createInstance($this);
                    if ($instance !== null) {
                        $output = $instance::getTableName();
                    }
                }
            } elseif (class_exists($this->class_name)) {
                /** @var Base $className */
                $className = $this->class_name;
                $output = ($className)::getTableName();
            }
        }

        if ($output === null) {
            $m = new EntityModel($this->getState());
            $output = 'sb_' . $m->createTableNameFromReference($this->reference);

            if ($this->is_virtual) {
                $output .= '__c';
            }
        }

        return $output;
    }

    public function getEntityField(string $fieldReference): ?FieldEntity { return $this->getEntityFields()[$fieldReference] ?? null; }

    public function __clone()
    {
        $output = new Entity($this->getState(), $this->getData());
        /** @var Entity $childEntity */
        foreach ($this->childEntities as $childEntity) {
            $output->addChildEntity(clone $childEntity);
        }
        $output->setParentEntity(clone $this->parentEntity);

        $oFields = [];
        /** @var FieldEntity $field */
        foreach ($this->fields as $key => $field) {
            $oFields[$key] = clone $field;
        }
        $output->setEntityFields($oFields);

        return $output;
    }

    protected function afterLoad()
    {
        if (!empty($this->reference)) {
            $this->coldLoadEntityFields();
            $this->coldLoadEntityRelations();

            $entitiesRaw = $this->getState()->getReadDb()->createQueryBuilder()
                ->select('*')
                ->addSelect('LENGTH(e.reference) - LENGTH(REPLACE(e.reference, "\\\", "")) as _parent_order')
                ->from(self::TABLE_NAME, 'e')
                ->andWhere('e.reference like :reference')
                ->setParameter('reference', "{$this->reference}\\\%")
                ->addOrderBy('_parent_order', 'asc')
                ->addOrderBy('e.sort_order', 'asc')
                ->andWhere('e.lote_deleted is null')
                ->execute()->fetchAll(\PDO::FETCH_ASSOC);

            foreach ($entitiesRaw as $entityRaw) {
                $entity = new Entity($this->getState());
                $entity->ignoreModifications = true;
                $entity->setData($entityRaw);
                $entity->ignoreModifications = false;
                $entity->setParentEntity($this);
                $this->addChildEntity($entity);
                $entity->coldLoadEntityFields();
                $entity->coldLoadEntityRelations();
            }
        }
    }

    private function coldLoadEntityRelations(): void
    {
        $rmm = new RelationManager();
        if (!empty($this->reference)) {
            $rm = new RelationModel($this->getState());
            $relationsRaw = $rm->listByFields([
                [
                    'from_entity_reference' => $this->reference,
                    'to_entity_reference' => $this->reference,
                ] /// We want an or field, thats why this is a double array
            ], false, 'sort_order', 'asc');
            foreach ($relationsRaw as $relationData) {
                $re = new RelationEntity($this->getState());
                $re->ignoreModifications = true;
                $re->setData($relationData);
                $re->ignoreModifications = false;
                $forwardRelation = $re->from_entity_reference === $this->reference;
                $rmm->addRelation($re, $forwardRelation);
            }
        }
        $this->relations = $rmm;
    }

    private function coldLoadEntityFields(): void
    {
        if (!empty($this->reference)) {
            $fm = new FieldModel($this->getState());
            $fieldsRaw = $fm->listByField('schema_entity_ref', $this->reference, false, 'sort_order', 'asc');

            foreach ($fieldsRaw as $fieldRaw) {
                $field = new FieldEntity($this->getState());
                $field->ignoreModifications = true;
                $field->setData($fieldRaw);
                $field->ignoreModifications = false;
                $field->setParentEntity($this);
                $this->addEntityField($field);
            }
        }
    }

    public function save(): int
    {
        $id = parent::save();

        /** @var FieldEntity $field */
        foreach ($this->fields as $field) {
            if (empty($field->schema_entity_ref)) {
                $field->schema_entity_ref = $this->reference;
            }
            $field->save();
        }

        foreach ($this->childEntities as $childEntity) {
            if (empty($childEntity->parentEntity)) {
                $childEntity->parentEntity = $this;
            }
            $childEntity->save();
        }

        return (int)$id;
    }

    public function delete($strongDelete = false)
    {
        foreach ($this->getEntityFields() as $f) {
            $f->delete($strongDelete);
        }
        foreach ($this->getRelations()->getRelations() as $r) {
            $r->delete($strongDelete);
        }
        foreach ($this->getRelations()->getAllReverseRelations() as $r) {
            $r->delete($strongDelete);
        }
        foreach ($this->getChildEntities() as $e) {
            $e->delete($strongDelete);
        }
        return parent::delete($strongDelete);
    }

    public function getValidatorGroup(): ValidatorGroup
    {
        $validators = [];
        foreach ($this->getEntityFields() as $field) {
            $validators[$field->reference] = $field->getValidator();
        }
        return new ValidatorGroup($validators);
    }

    public function getSelfValidator(): ValidatorBase { return new EntityValidator([$this->getEntityFields()]); }

    public function getRelations(): ?RelationManager { return $this->relations; }

    public function setRelation(RelationManager $relationManager): void { $this->relations = $relationManager; }

    public static function getLoteFields(?BaseState $state = null): array
    {
        if (is_null(self::$loteFields)) {
            self::$loteFields = [
                'id' => new FieldEntity($state, ['reference' => 'id', 'field_type' => 'primary_key_int', 'db_type' => 'integer', 'db_options' => '{"autoincrement":true}']),
                'lote_created' => new FieldEntity($state, ['reference' => 'lote_created', 'field_type' => 'datetime_immutable', 'db_type' => 'datetime', 'db_options' => '{"notnull":false}']),
                'lote_updated' => new FieldEntity($state, ['reference' => 'lote_updated', 'field_type' => 'datetime', 'db_type' => 'datetime', 'db_options' => '{"notnull":false}']),
                'lote_deleted' => new FieldEntity($state, ['reference' => 'lote_deleted', 'field_type' => 'datetime_immutable', 'db_type' => 'datetime', 'db_options' => '{"notnull":false}', 'db_index' => true]),
                'lote_deleted_by' => new FieldEntity($state, ['reference' => 'lote_deleted_by', 'field_type' => 'fkey', 'db_type' => 'integer', 'db_options' => '{"notnull":false}']),
                'lote_author_id' => new FieldEntity($state, ['reference' => 'lote_author_id', 'field_type' => 'fkey', 'db_type' => 'integer', 'db_options' => '{"notnull":false}']),
                'lote_access' => new FieldEntity($state, ['reference' => 'lote_access', 'field_type' => 'integer', 'db_type' => 'integer', 'db_options' => '{"notnull":false,"default":0,"length":4}']),
            ];
        }
        return self::$loteFields;
    }

    public function __destruct()
    {

    }

    public function getAnnotatedColumns()
    {
        static $dataSet = null;
        if ($dataSet === null) {
            $dataSet = parent::getAnnotatedColumns();
        }
        return $dataSet;
    }

    protected function setupEntityProperties()
    {
        $this->entityProperties = [
            'id' => 'id',
            'lote_access' => 'lote_access',
            'lote_author_id' => 'lote_author_id',
            'lote_created' => 'lote_created',
            'lote_deleted' => 'lote_deleted',
            'lote_deleted_by' => 'lote_deleted_by',
            'lote_updated' => 'lote_updated',
            'reference' => 'reference',
            'name' => 'name',
            'description' => 'description',
            'sort_order' => 'sort_order',
            'is_active' => 'is_active',
            'is_hidden' => 'is_hidden',
            'class_name' => 'class_name',
            'is_virtual' => 'is_virtual',
        ];
        unset(
            $this->id, $this->lote_access, $this->lote_author_id, $this->lote_created, $this->lote_deleted, $this->lote_deleted_by, $this->lote_updated,
            $this->reference, $this->name, $this->description, $this->sort_order, $this->is_active, $this->is_hidden, $this->class_name, $this->is_virtual
        );
    }

    protected function getDefaultFieldValue($fieldName)
    {
        $output = null;
        switch ($fieldName) {
            case 'id':
            case 'lote_created':
            case 'lote_updated':
            case 'lote_deleted':
            case 'lote_author_id':
            case 'lote_access':
            case 'lote_deleted_by':
            case 'reference':
            case 'name':
            case 'description':
            case 'class_name':
            case 'is_virtual':
                $output = null; break;
            case 'sort_order': $output = 0; break;
            case 'is_active': $output = true; break;
            case 'is_hidden': $output = false; break;
        }
        return $output;
    }

    public function getSchema(): Entity {
        if (is_null($this->schema)) {
            if (is_null(self::$internalSchema)) {
                $e = new Entity($this->getState());

                $e->reference = Entity::getObjectRef();
                $e->name = 'Entity';
                $e->description = 'Core Entity Schema';
                $e->sort_order = 0;
                $e->is_active = true;
                $e->is_hidden = true;
                $e->class_name = get_class($e);
                $e->is_virtual = false;

                $fReference = new FieldEntity($this->getState());
                $fReference->reference = 'reference';
                $fReference->schema_entity_ref = $e->reference;
                $fReference->field_type = 'string';
                $fReference->db_type = 'string';
                $fReference->is_virtual = false;
                $e->addEntityField($fReference);

                $fName = new FieldEntity($this->getState());
                $fName->reference = 'name';
                $fName->schema_entity_ref = $e->reference;
                $fName->field_type = 'string';
                $fName->db_type = 'string';
                $fName->is_virtual = false;
                $e->addEntityField($fName);

                $fDescription = new FieldEntity($this->getState());
                $fDescription->reference = 'description';
                $fDescription->schema_entity_ref = $e->reference;
                $fDescription->field_type = 'string';
                $fDescription->db_type = 'string';
                $fDescription->is_virtual = false;
                $e->addEntityField($fDescription);

                $fSortOrder = new FieldEntity($this->getState());
                $fSortOrder->reference = 'sort_order';
                $fSortOrder->schema_entity_ref = $e->reference;
                $fSortOrder->field_type = 'string';
                $fSortOrder->db_type = 'string';
                $fSortOrder->is_virtual = false;
                $e->addEntityField($fSortOrder);

                $fIsActive = new FieldEntity($this->getState());
                $fIsActive->reference = 'is_active';
                $fIsActive->schema_entity_ref = $e->reference;
                $fIsActive->field_type = 'boolean';
                $fIsActive->db_type = 'boolean';
                $fIsActive->is_virtual = false;
                $e->addEntityField($fIsActive);

                $fIsHidden = new FieldEntity($this->getState());
                $fIsHidden->reference = 'is_hidden';
                $fIsHidden->schema_entity_ref = $e->reference;
                $fIsHidden->field_type = 'boolean';
                $fIsHidden->db_type = 'boolean';
                $fIsHidden->is_virtual = false;
                $e->addEntityField($fIsHidden);

                $fClassName = new FieldEntity($this->getState());
                $fClassName->reference = 'class_name';
                $fClassName->schema_entity_ref = $e->reference;
                $fClassName->field_type = 'string';
                $fClassName->db_type = 'string';
                $fClassName->is_virtual = false;
                $e->addEntityField($fClassName);

                $fIsVirtual = new FieldEntity($this->getState());
                $fIsVirtual->reference = 'is_virtual';
                $fIsVirtual->schema_entity_ref = $e->reference;
                $fIsVirtual->field_type = 'boolean';
                $fIsVirtual->db_type = 'boolean';
                $fIsVirtual->is_virtual = false;
                $e->addEntityField($fIsVirtual);

                foreach (self::getLoteFields() as $loteField) {
                    $e->addEntityField($loteField);
                }

                self::$internalSchema = $e;
            }
            $this->schema = self::$internalSchema;
        }
        return $this->schema;
    }

}
