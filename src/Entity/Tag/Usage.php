<?php
namespace BlueSky\Framework\Entity\Tag;

use BlueSky\Framework\Object\Entity\Base;

/**
 * Class Usage
 * @package BlueSky\Framework\Entity\Tag
 * @dbEntity true
 */
class Usage extends Base
{

    /**
     * @var string $tableName
     * */
    protected $tableName = 'sb__tag_usage';
    public const TABLE_NAME = 'sb__tag_usage';

    /**
     * @dbColumn tag_id
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $tag_id - The id of the sb__tag that this usage references
     */
    public $tag_id;

    /**
     * @dbColumn object_ref
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var string $object_ref
     */
    public $object_ref;

    /**
     * @dbColumn object_id
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var string $object_id
     */
    public $object_id;

    /**
     * @dbColumn field_ref
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var string $field_ref
     */
    public $field_ref;

}
