<?php
namespace BlueSky\Framework\Entity\Tag;

use BlueSky\Framework\Object\Entity\Base;

/**
 * Class Tag
 * @package BlueSky\Framework\Entity\Tag
 * @dbEntity true
 */
class Tag extends Base
{

    /**
     * @var string $tableName
     * */
    protected $tableName = 'sb__tag';
    public const TABLE_NAME = 'sb__tag';

    /**
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @dbIndex true
     * @var string $value - The string value of the tag
     */
    public $value;

    /**
     * @fieldType string
     * @dbOptions length = 255, notnull = false
     * @dbIndex true
     * @var string $display_value - The string value of the tag
     */
    public $display_value;

    /**
     * @fieldType string
     * @dbOptions length = 32, notnull = false
     * @var string $colour_code - colour code
     */
    public $colour_code;

    /**
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var string $weighting - weighting
     */
    public $weighting;

    /**
     * @fieldType boolean
     * @dbOptions notnull = true, default = true
     * @dbIndex true
     * @var boolean $active - is active
     */
    public $is_active;

    /**
     * @fieldType boolean
     * @dbOptions notnull = true, default = false
     * @dbIndex true
     * @var boolean $is_restricted - is restricted
     */
    public $is_restricted;

}
