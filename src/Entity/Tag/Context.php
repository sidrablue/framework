<?php
namespace BlueSky\Framework\Entity\Tag;

use BlueSky\Framework\Object\Entity\Base;

/**
 * Class Context
 * @package BlueSky\Framework\Entity\Tag
 * @dbEntity true
 */
class Context extends Base
{

    /**
     * @var string $tableName
     * */
    protected $tableName = 'sb__tag_context';
    public const TABLE_NAME = 'sb__tag_context';

    /**
     * @dbColumn tag_id
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $tag_id - The id of the sb__tag that this context references
     */
    public $tag_id;

    /**
     * @dbColumn object_ref
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var string $object_ref
     */
    public $object_ref;

}
