<?php
namespace BlueSky\Framework\Entity;

use BlueSky\Framework\Object\Entity\Base;

/**
 * File class
 * @package BlueSky\Framework\Entity
 * @dbEntity true
 */
class File extends Base
{

    /**
     * @var string $tableName
     * */
    protected $tableName = 'sb__file';
    public const TABLE_NAME = 'sb__file';

    /**
     * @dbColumn store
     * @fieldType string
     * @dbOptions length=32, notnull = true
     * @dbIndex true
     * @var string $store - the reference of the adapter used to store the file
     */
    public $store;

    /**
     * @dbColumn status
     * @fieldType string
     * @dbOptions length=32, notnull = false
     * @dbIndex true
     * @var string $status - the status of the file
     */
    public $status;

    /**
     * @dbColumn object_ref
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var string $object - the reference of the object that this file is for
     */
    public $object_ref;

    /**
     * @dbColumn object_id
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $object_id - the ID of the object that this file is for
     */
    public $object_id;

    /**
     * @dbColumn object_key
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var int $object_key - the key of the object if one is required, such as a field name
     */
    public $object_key;

    /**
     * @dbColumn sub_path
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var int $sub_path - the sub path of the file if one is required
     */
    public $sub_path;

    /**
     * @dbColumn reference
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var string $reference - the reference of the file
     */
    public $reference;

    /**
     * @dbColumn location_reference
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var string $location_reference - the location reference of the file
     */
    public $location_reference;

    /**
     * @dbColumn size
     * @fieldType integer
     * @dbOptions notnull = false
     * @var string $size - the size of the file
     */
    public $size;

    /**
     * @dbColumn title
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var string $title - the display name of hte file
     */
    public $title;

    /**
     * @dbColumn filename
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var int $filename - the ID of the group that the field belongs to
     */
    public $filename;

    /**
     * @dbColumn extension
     * @fieldType string
     * @dbOptions length=32, notnull = false
     * @var int $extension - the extension of the file as it was uploaded as
     */
    public $extension;

    /**
     * @dbColumn mime_type
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @var int $mime_type - the mime type of the file
     */
    public $mime_type;

    /**
     * @dbColumn file_type
     * @fieldType string
     * @dbOptions length=64, notnull = false
     * @var int $file_type - the file type of the file
     */
    public $file_type;

    /**
     * @dbColumn is_public
     * @fieldType boolean
     * @dbIndex true
     * @dbOptions notnull = false
     * @var int $is_public - true if this file is publicly accessible
     */
    public $is_public;

    /**
     * @dbColumn is_saved
     * @fieldType boolean
     * @dbIndex true
     * @dbOptions notnull = false
     * @var int $is_saved - true if this file is fully saved
     */
    public $is_saved;

    /**
     * @dbColumn checksum
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var int $checksum - the checksum of the file
     */
    public $checksum;

    /**
     * @var boolean $isAudited
     * True if this entity is audited
     * */
    protected $isAudited = true;


    /**
     * Function to call before inserting data so that data can be manipulated if required
     * @param array $data
     * @return array
     * */
    protected function beforeInsert($data)
    {
        if (!isset($data['sub_path']) || $data['sub_path'] == '') {
            $data['sub_path'] = '/';
        }
        elseif(isset($data['sub_path'])) {
            $data['sub_path'] = strtolower($data['sub_path']);
        }
        return $data;
    }

    /**
     * Function to call before updating data so that data can be manipulated if required
     * @param array $data
     * @return array
     * */
    protected function beforeUpdate($data)
    {
        if (!isset($data['sub_path']) || $data['sub_path'] == '') {
            $data['sub_path'] = '/';
        }
        elseif(isset($data['sub_path'])) {
            $data['sub_path'] = strtolower($data['sub_path']);
        }
        return $data;
    }

    /**
     * Load an entity by its reference
     * @access public
     * @param string $reference - the reference of the object
     * @return bool
     * */
    public function loadByReference($reference)
    {
        $reference = strtolower($reference);
        if ($this->getState()->getData()->hasValue("entity:sb__file", $reference)) {
            $this->setData($this->getState()->getData()->getValue("entity:sb__file", $reference));
            $result = $this->id;
        } elseif ($this->getState()->getDbCache()->has('entity.sb__file.' . $this->makeRedisKey($reference))) {
            $data = $this->getState()->getDbCache()->get('entity.sb__file.' . $this->makeRedisKey($reference));
            $this->setData($data);
            $this->updateDataCache();
            $result = $this->id;
        } else if ($result = $this->loadByField('reference', $reference)) {
            $this->getState()->getDbCache()->set('entity.sb__file.' . $this->makeRedisKey($reference), $this->getData());
            $this->updateDataCache();
        }
        return $result;
    }

    /**
     * Load an entity by its reference
     * @access public
     * @param string $locationReference - the location reference of the object
     * @return bool
     * */
    public function loadByLocationReference($locationReference)
    {
        $reference = strtolower($locationReference);
        if ($this->getState()->getData()->hasValue("entity:sb__file:location_reference", $reference)) {
            $this->setData($this->getState()->getData()->getValue("entity:sb__file", $reference));
            $result = $this->id;
        } elseif ($this->getState()->getDbCache()->has('entity.sb__file.location_reference.' . $this->makeRedisKey($reference))) {
            $data = $this->getState()->getDbCache()->get('entity.sb__file.location_reference.' . $this->makeRedisKey($reference));
            $this->setData($data);
            $this->updateDataCache();
            $result = $this->id;
        } else {
            if ($result = $this->loadByField('location_reference', $reference)) {
                $this->getState()->getDbCache()->set('entity.sb__file.location_reference.' . $this->makeRedisKey($reference), $this->getData());
                $this->updateDataCache();
            }
        }
        return $result;
    }

    /**
     * Load the latest non archived version of the entity by its ID by first retrieving its reference
     * @access public
     * @param string $id - the reference of the object
     * @return bool
     * */
    public function loadLatestById($id)
    {
        $result = false;
        $tempFile = new File($this->getState());
        if ($tempFile->load($id)) {
            if (!$tempFile->lote_deleted) {
                $this->setData($tempFile->getData());
                $result = $this->id;
            } else {
                $result = $this->loadByReference($tempFile->reference);
            }
        }
        return $result;
    }

    /**
     * Load an entity by its reference
     * @access public
     * @param string $objectRef
     * @param int $objectId
     * @param string $objectKey
     * @param string $fileTitle
     * @param string $subPath
     * @return void
     */
    public function loadByObjectParams($objectRef, $objectId, $objectKey = '', $fileTitle = '', $subPath = '')
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('*')
            ->from($this->getTableName(), 't')
            ->where('t.object_ref = :object_ref')
            ->setParameter('object_ref', $objectRef)
            ->andWhere('t.object_id = :object_id')
            ->setParameter('object_id', $objectId);
        if ($objectKey) {
            $q->andWhere('t.object_key = :object_key')
                ->setParameter('object_key', $objectKey);
        }
        if ($fileTitle) {
            $q->andWhere('t.title = :title')
                ->setParameter('title', $fileTitle);
        }
        if ($subPath && $subPath != '/') {
            $q->andWhere('t.sub_path = :sub_path')
                ->setParameter('sub_path', strtolower(trim($subPath, '\\')));
        }
        $s = $q->execute();
        if ($row = $s->fetch(\PDO::FETCH_ASSOC)) {
            $this->setData($row);
            $this->updateDataCache();
        }
    }

    /**
     * Load an entity by its filename and reference
     * @access public
     * @param string $filename
     * @param string $objectRef
     * @param string $subPath
     * @return void
     */
    public function loadByFilename($filename, $objectRef, $subPath = '/')
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('*')
            ->from($this->getTableName(), 't')
            ->where('t.object_ref = :object_ref')
            ->setParameter('object_ref', $objectRef);
        if ($filename) {
            $q->andWhere('t.filename = :filename')
                ->setParameter('filename', $filename);
        }
        if ($subPath && $subPath != '/') {
            $q->andWhere('t.sub_path = :sub_path')
                ->setParameter('sub_path', strtolower(trim('\\', $subPath)));
        }
        $q->orderBy('t.object_id', 'desc');
        $s = $q->execute();
        if ($row = $s->fetch(\PDO::FETCH_ASSOC)) {
            $this->setData($row);
            $this->updateDataCache();
        }
    }

    /**
     * Archive this file
     * @access public
     * @return void
     * @todo - put this in a sub folder instead
     * */
    public function archive()
    {
        if (true || $this->id && $this->status != 'archived') {
            $this->status = 'archived';
            $this->location_reference = $this->makeArchiveReference();
            $this->save();
        }
    }

    /**
     * Make a reference to use for archiving this current file
     * @access public
     * @return string
     * */
    public function makeArchiveReference()
    {
        $baseName = explode('.', basename($this->location_reference));
        count($baseName) ==1?$offset=0:$offset=count($baseName)-2;
        $baseName[$offset] .= uniqid("_");
        $baseName = implode(".", $baseName);
        return dirname($this->location_reference) . '/_archive/' . $baseName;
    }

    /**
     * Delete this object
     * @param bool $strongDelete - Whether or not to delete the table object completely
     * @return boolean
     */
    public function delete($strongDelete = false)
    {
        $fileId = $this->id;
        $fileReference = $this->reference;
        $result = parent::delete($strongDelete);
        if($fileId && $fileReference) {
            $vars = ['reference' => $this->getState()->accountReference];
            $vars['file_id'] = $fileId;
            $vars['file_reference'] = $fileReference;
            $this->getState()->getQueue()->add("media-uncache", '\Lote\Module\Media\Queue\Worker\UnCache', $vars);
        }
        return $result;
    }
}
