<?php
namespace BlueSky\Framework\Entity;

use BlueSky\Framework\Object\Entity\Base;

/**
 * Base class for ProcessEmails
 * @package BlueSky\Framework\Entity
 * @dbEntity true
 */
class ProcessEmails extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb_process_emails';
    public const TABLE_NAME = 'sb_process_emails';
    /**
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @var string $email_type
     */
    public $email_type;

    /**
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @var string $object_ref
     */
    public $object_ref;


    /**
     * @fieldType integer
     * @dbOptions notnull = false
     * @var int $object_id - object id
     */
    public $object_id;

    /**
     * @fieldType text
     * @dbOptions notnull = false
     * @var string $data - data
     */
    public $data;

    /**
     * @fieldType datetime
     * @dbOptions notnull = false
     * @var string $processed - processed
     */
    public $processed;

    /**
     * @fieldType datetime
     * @dbOptions notnull = false
     * @var string $scheduled - scheduled
     */
    public $scheduled;

    /**
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @var string $external_id
     */
    public $external_id;

}
