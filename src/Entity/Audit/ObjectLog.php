<?php
namespace BlueSky\Framework\Entity\Audit;

use BlueSky\Framework\Object\Entity\Base;

/**
 * Field link log audit
 * @package BlueSky\Framework\Entity\Audit
 * @dbEntity true
 */
class ObjectLog extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__audit_object_log';
    public const TABLE_NAME = 'sb__audit_object_log';

    /**
     * @dbColumn object_ref
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var string $object_ref
     */
    public $object_ref;

    /**
     * @dbColumn object_id
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var string $object_id
     */
    public $object_id;

    /**
     * @dbColumn log_id
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var string $log_id
     */
    public $log_id;

    /**
     * @dbColumn operation
     * @fieldType string
     * @dbOptions length=32, notnull = false
     * @dbIndex true
     * @var string $operation
     */
    public $operation;

    /**
     * @dbColumn description
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @var string $description
     */
    public $description;

}
