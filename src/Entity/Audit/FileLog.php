<?php
namespace BlueSky\Framework\Entity\Audit;

use BlueSky\Framework\Object\Entity\Base;

/**
 * File log audit
 * @package BlueSky\Framework\Entity\Audit
 * @dbEntity true
 */
class FileLog extends Base
{
    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__audit_file_log';
    public const TABLE_NAME = 'sb__audit_file_log';

    /**
     * @dbColumn log_id
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var string $log_id
     */
    public $log_id;

    /**
     * @dbColumn object_log_id
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var string $object_log_id
     */
    public $object_log_id;

    /**
     * @dbColumn object_ref
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var string $object_ref
     */
    public $object_ref;

    /**
     * @dbColumn object_id
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var string $object_id
     */
    public $object_id;

    /**
     * @dbColumn field_reference
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var string $field_reference
     */
    public $field_reference;

    /**
     * @dbColumn store
     * @fieldType string
     * @dbOptions length=64, notnull = false
     * @dbIndex true
     * @var string $store
     */
    public $store;

    /**
     * @dbColumn location_reference
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var string $location_reference
     */
    public $location_reference;

    /**
     * @dbColumn archive_reference
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var string $archive_reference
     */
    public $archive_reference;
}
