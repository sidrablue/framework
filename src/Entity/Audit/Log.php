<?php
namespace BlueSky\Framework\Entity\Audit;

use BlueSky\Framework\Object\Entity\Base;

/**
 * Field link log audit
 * @package BlueSky\Framework\Entity\Audit
 * @dbEntity true
 */
class Log extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__audit_log';
    public const TABLE_NAME = 'sb__audit_log';

    /**
     * @dbColumn parent_id
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var integer $parent_id
     */
    public $parent_id;

    /**
     * @dbColumn controller
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var string $controller
     */
    public $controller;

    /**
     * @dbColumn method
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var string $method
     */
    public $method;

    /**
     * @dbColumn status
     * @fieldType string
     * @dbOptions length=32, notnull = false
     * @dbIndex true
     * @var string $status
     */
    public $status;

    /**
     * @dbColumn reference
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @var string $reference
     */
    public $reference;

    /**
     * @dbColumn description
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @var string $description
     */
    public $description;

    /**
     * @dbColumn data
     * @fieldType text
     * @dbOptions notnull = false
     * @var string $data
     */
    public $data;

}
