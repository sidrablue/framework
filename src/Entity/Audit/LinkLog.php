<?php
namespace BlueSky\Framework\Entity\Audit;

use BlueSky\Framework\Object\Entity\Base;

/**
 * Field link log audit
 * @package BlueSky\Framework\Entity\Audit
 * @dbEntity true
 */
class LinkLog extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__audit_link_log';
    public const TABLE_NAME = 'sb__audit_link_log';

    /**
     * @dbColumn log_id
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var string $log_id
     */
    public $log_id;

    /**
     * @dbColumn object_log_id
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var string $object_log_id
     */
    public $object_log_id;

    /**
     * @dbColumn object_ref_from
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var string $object_ref_from
     */
    public $object_ref_from;

    /**
     * @dbColumn object_ref_to
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var string $object_ref_to
     */
    public $object_ref_to;

    /**
     * @dbColumn object_id_from
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var string $object_id_from
     */
    public $object_id_from;

    /**
     * @dbColumn object_id_to
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var string $object_id_to
     */
    public $object_id_to;

    /**
     * @dbColumn operation
     * @fieldType string
     * @dbOptions length=32, notnull = false
     * @dbIndex true
     * @var string $operation
     */
    public $operation;

}
