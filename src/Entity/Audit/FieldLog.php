<?php
namespace BlueSky\Framework\Entity\Audit;

use BlueSky\Framework\Object\Entity\Base;

/**
 * Field log audit
 * @package BlueSky\Framework\Entity\Audit
 * @dbEntity true
 */
class FieldLog extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__audit_field_log';
    public const TABLE_NAME = 'sb__audit_field_log';

    /**
     * @dbColumn object_id
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var string $object_id
     */
    public $object_id;

    /**
     * @dbColumn field_name
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @var string $field_name
     */
    public $field_name;

    /**
     * @dbColumn field_type
     * @fieldType string
     * @dbOptions length=32, notnull = false
     * @var string $field_type
     */
    public $field_type;

    /**
     * @dbColumn value_old
     * @fieldType text
     * @dbOptions notnull = false
     * @var string $value_old
     */
    public $value_old;

    /**
     * @dbColumn value_new
     * @fieldType text
     * @dbOptions notnull = false
     * @var string $value_new
     */
    public $value_new;

}
