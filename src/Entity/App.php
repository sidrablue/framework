<?php
namespace BlueSky\Framework\Entity;

use BlueSky\Framework\Object\Entity\Base;

/**
 * App class
 * @package BlueSky\Framework\Entity
 * @dbEntity true
 */
class App extends Base
{

    /**
     * @var string $tableName
     * */
    protected $tableName = 'sb__app';

    public const TABLE_NAME = 'sb__app';

    /**
     * @dbColumn reference
     * @fieldType string
     * @dbOptions length=255, notnull = true
     * @dbIndex true
     * @var string $reference - the reference of the app
     */
    public $reference;

    /**
     * @dbColumn version
     * @fieldType string
     * @dbOptions length=64, notnull = true
     * @var int $version - the version of the app
     */
    public $version;

    /**
     * @dbColumn name
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @var string $name - the name of the app
     */
    public $name;

    /**
     * @dbColumn description
     * @fieldType text
     * @dbOptions notnull = false
     * @var string $description - The description of the app
     */
    public $description;

    /**
     * @dbColumn priority
     * @fieldType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var integer $priority - The priority of the app
     */
    public $priority;

    /**
     * @dbColumn is_active
     * @fieldType boolean
     * @dbOptions notnull = false, default=true
     * @dbIndex true
     * @var boolean $is_active - The enabled status of the app
     */
    public $is_active;

    /**
     * @dbColumn is_hidden
     * @fieldType boolean
     * @dbOptions notnull = false, default=false
     * @dbIndex true
     * @var boolean $is_hidden - True if this app is hidden from the user
     */
    public $is_hidden;

    /**
     * @dbColumn license
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @var string $license - The type of license that this app has
     */
    public $license;

}
