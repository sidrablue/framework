<?php
namespace BlueSky\Framework\Entity;

use BlueSky\Framework\Object\Entity\Base;

/**
 * Domain class
 * @package BlueSky\Framework\Entity
 * @dbEntity true
 */
class Domain extends Base
{

    /**
     * @var string $tableName
     * */
    protected $tableName = 'sb__domain';
    public const TABLE_NAME = 'sb__domain';

    /**
     * @dbColumn name
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @var string $name - The name of URL
     */
    public $name;

    /**
     * @dbColumn url
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @var string $url - The name of URL
     */
    public $url;

    /**
     * @dbColumn is_active
     * @fieldType boolean
     * @dbOptions notnull = false, default=true
     * @var bool $is_active - Whether or not the URL is active
     */
    public $is_active;

    /**
     * @dbColumn datetime_from
     * @fieldType datetime
     * @dbOptions length=255, notnull = false
     * @var string $date_from - When the URL becomes active
     */
    public $datetime_from;

    /**
     * @dbColumn datetime_to
     * @fieldType datetime
     * @dbOptions length=255, notnull = false
     * @var string $date_to - When the URL expires
     */
    public $datetime_to;

}
