<?php
namespace BlueSky\Framework\Entity;

use BlueSky\Framework\Object\Entity\Base;

/**
 * Type class
 * @package BlueSky\Framework\Entity
 * @dbEntity true
 * @OA\Schema(
 *     description="Type",
 *     title="Type",
 * )
 * @SWG\Definition(required={"tableName", "name", "description", "user_type", "role_type"}, type="object", @SWG\Xml(name="Type"))
 *
 */
class Type extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__type';
    public const TABLE_NAME = 'sb__type';

    /**
     * @dbColumn name
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @var string $name - the role name
     * @OA\Property(description="name", title="name")
     * @var string
     */
    public $name;

    /**
     * @dbColumn description
     * @fieldType string
     * @dbOptions length=255, notnull = false
     * @var string $description - description of the role
     * @OA\Property(description="description", title="description")
     * @var string
     */
    public $description;
}
