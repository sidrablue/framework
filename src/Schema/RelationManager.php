<?php
/**
 * Copyright (c) BlueSky Digital Labs Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Schema;

use BlueSky\Framework\Entity\Schema\Relation as RelationEntity;

class RelationManager
{
    /** @var RelationEntity[] $oneToOne */
    private $oneToOne = [];

    /** @var RelationEntity[] $manyToOne */
    private $manyToOne = [];

    /** @var RelationEntity[] $oneToMany */
    private $oneToMany = [];

    /** @var RelationEntity[] $manyToMany */
    private $manyToMany = [];

    /** @var RelationEntity[] $objectRefIdOO */
    private $objectRefIdOO = [];

    /** @var RelationEntity[] $allRelations */
    private $allRelations = [];

    /** @var RelationEntity[] $reverseOneToOne */
    private $reverseOneToOne = [];

    /** @var RelationEntity[] $reverseManyToOne */
    private $reverseManyToOne = [];

    /** @var RelationEntity[] $reverseOneToMany */
    private $reverseOneToMany = [];

    /** @var RelationEntity[] $reverseManyToMany */
    private $reverseManyToMany = [];

    /** @var RelationEntity[] $reverseObjectRefIdOO */
    private $reverseObjectRefIdOO = [];

    /** @var RelationEntity[] $reverseAllRelations */
    private $reverseAllRelations = [];

    public function __construct(array $relations = [], bool $isDirectionForward = true)
    {
        foreach ($relations as $relation) {
            $this->addRelation($relation, $isDirectionForward);
        }
    }

    public function addRelation(RelationEntity $relation, bool $isDirectionForward = true): void
    {
        if ($isDirectionForward) { $this->allRelations[] = $relation; } else { $this->reverseAllRelations[] = $relation; }
        switch ($relation->type) {
            case RelationEntity::ONE_TO_ONE:
                if ($isDirectionForward) { $this->oneToOne[] = $relation; } else { $this->reverseOneToOne[] = $relation; }
                break;
            case RelationEntity::MANY_TO_ONE:
                if ($isDirectionForward) { $this->manyToOne[] = $relation; } else { $this->reverseManyToOne[] = $relation; }
                break;
            case RelationEntity::ONE_TO_MANY:
                if ($isDirectionForward) { $this->oneToMany[] = $relation; } else { $this->reverseOneToMany[] = $relation; }
                break;
            case RelationEntity::MANY_TO_MANY:
                if ($isDirectionForward) { $this->manyToMany[] = $relation; } else { $this->reverseManyToMany[] = $relation; }
                break;
            case RelationEntity::OBJECT_REF_ID_OO:
                if ($isDirectionForward) { $this->objectRefIdOO[] = $relation; } else { $this->reverseObjectRefIdOO[] = $relation; }
                break;
        }
    }

    public function getRelationByToReference(string $toEntityReference): ?RelationEntity
    {
        $output = null;
        foreach ($this->getRelations() as $relation) {
            if ($relation->to_entity_reference === $toEntityReference) {
                $output = $relation;
                break;
            }
        }
        return $output;
    }

    /**
     * @param string $appReference
     * @return RelationEntity[]
     */
    public function getRelationsByAppReference(string $appReference): array
    {
        return array_filter($this->getRelations(), function ($item) use ($appReference) {
            /** @var RelationEntity $item */
            return $item->app_reference === $appReference;
        });
    }

    public function getRelationsByType(string $type, bool $isDirectionForward = true): array
    {
        $output = [];
        switch ($type) {
            case RelationEntity::ALL_RELATIONS:
                if ($isDirectionForward) { $output = $this->allRelations; } else { $output = $this->reverseAllRelations; }
                break;
            case RelationEntity::ONE_TO_ONE:
                if ($isDirectionForward) { $output = $this->oneToOne; } else { $output = $this->reverseOneToOne; }
                break;
            case RelationEntity::MANY_TO_ONE:
                if ($isDirectionForward) { $output = $this->manyToOne; } else { $output = $this->reverseManyToOne; }
                break;
            case RelationEntity::ONE_TO_MANY:
                if ($isDirectionForward) { $output = $this->oneToMany; } else { $output = $this->reverseOneToMany; }
                break;
            case RelationEntity::MANY_TO_MANY:
                if ($isDirectionForward) { $output = $this->manyToMany; } else { $output = $this->reverseManyToMany; }
                break;
            case RelationEntity::OBJECT_REF_ID_OO:
                if ($isDirectionForward) { $output = $this->objectRefIdOO; } else { $output = $this->reverseObjectRefIdOO; }
                break;
        }
        return $output;
    }

    /** @return RelationEntity[] */
    public function getRelations(): array { return $this->allRelations; }

    /** @return RelationEntity[] */
    public function getOneToOneRelations(): array { return $this->oneToOne; }

    /** @return RelationEntity[] */
    public function getOneToManyRelations(): array { return $this->oneToMany; }

    /** @return RelationEntity[] */
    public function getManyToOneRelations(): array { return $this->manyToOne; }

    /** @return RelationEntity[] */
    public function getManyToManyRelations(): array { return $this->manyToMany; }

    /** @return RelationEntity[] */
    public function getObjectRefIdOORelations(): array { return $this->objectRefIdOO; }

    /** @return RelationEntity[] */
    public function getAllReverseRelations(): array { return $this->reverseAllRelations; }

    /** @return RelationEntity[] */
    public function getReverseOneToOneRelations(): array { return $this->reverseOneToOne; }

    /** @return RelationEntity[] */
    public function getReverseOneToManyRelations(): array { return $this->reverseOneToMany; }

    /** @return RelationEntity[] */
    public function getReverseManyToOneRelations(): array { return $this->reverseManyToOne; }

    /** @return RelationEntity[] */
    public function getReverseManyToManyRelations(): array { return $this->reverseManyToMany; }

    /** @return RelationEntity[] */
    public function getReverseObjectRefIdOORelations(): array { return $this->reverseObjectRefIdOO; }

}