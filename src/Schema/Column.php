<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 */

declare(strict_types=1);

namespace BlueSky\Framework\Schema;


class Column
{
    /** @var string $id */
    private $id;

    /** @var string|null $text */
    private $text;

    /** @var int $index */
    private $index;

    /** @var FieldOption $data */
    private $data;

    /**
     * Column constructor.
     * @param string $id
     * @param int $index
     * @param string|null $text
     * @param FieldOption $data
     */
    public function __construct(string $id, int $index, FieldOption $data, ?string $text = null)
    {
        $this->id = $id;
        $this->index = $index;
        $this->text = $text;
        $this->data = $data;
    }

    /** @return string */
    public function getId(): string { return $this->id; }

    /** @return int */
    public function getIndex(): int { return $this->index; }

    /** @return string|null */
    public function getText(): ?string { return $this->text; }

    /** @return FieldOption */
    public function getData(): FieldOption { return $this->data; }

}