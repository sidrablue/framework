<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Schema;

use BlueSky\Framework\Util\Strings;

class FieldOptions
{

    public const DIRECTION_DOWN = 'down';
    public const DIRECTION_RIGHT = 'right';

    private $direction = self::DIRECTION_DOWN;
    private $sortOption = 0;
    private $rows = [];
    private $allowCustomOption = false;

    /**
     * FieldOption constructor.
     * @param string|array $data - this is option_structure in json encoded form
     */
    public function __construct($data)
    {
        if (is_string($data) && Strings::isJson($data)) {
            $optionsData = json_decode($data, true);
        } else {
            $optionsData = $data;
        }
        $this->direction = $optionsData['direction'];
        $this->sortOption = $optionsData['sort_option'];
        $this->allowCustomOption = $optionsData['custom_user_options']?? false;
        if (!empty($optionsData['rows'])) {
            foreach ($optionsData['rows'] as $row) {
                $rowText = $row['text'];
                if (!is_null($rowText)) { $rowText = (string) $rowText; }
                $rowId = $row['id'];
                if (!empty($row['columns'])) {
                    $columns = [];
                    foreach ($row['columns'] as $column) {
                        $columnText = $column['text'];
                        if (!is_null($columnText)) { $columnText = (string) $columnText; }
                        $columnId = $column['id'];
                        if (!empty($column['data'])) {
                            $dataText = $column['data']['text'];
                            $dataValue = $column['data']['value'];
                            if (!is_null($dataText)) { $dataText = (string) $dataText; }
                            if (!is_null($dataValue)) { $dataValue = (string) $dataValue; }
                            $data = new FieldOption($dataText, $dataValue, $column['data']['id'], $rowId, $columnId, $column['sort_weight'] ?? 0);
                            $columns[] = new Column($columnId, $column['index'], $data, $columnText);
                        }
                    }
                    $this->rows[] = new Row($rowId, $row['index'], $columns, $rowText);
                }
            }
        }
    }

    /** @return Row[] */
    public function getRows(): array { return $this->rows; }

    /** @return string */
    public function getDirection(): string { return $this->direction; }

    /** @return bool */
    public function allowCustomOption(): bool { return $this->allowCustomOption; }

}
