<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Schema;

class FieldOption
{

    private $text;
    private $value;
    private $optionId;
    private $rowKey;
    private $columnKey;
    private $sortWeight;

    /**
     * FieldOption constructor.
     * @param string $text
     * @param string $value
     * @param string $optionId
     * @param string $rowKey
     * @param string $columnKey
     * @param int $sortWeight
     */
    public function __construct(?string $text, ?string $value, string $optionId, string $rowKey, string $columnKey, int $sortWeight)
    {
        $this->text = $text;
        $this->value = $value;
        $this->optionId = $optionId;
        $this->rowKey = $rowKey;
        $this->columnKey = $columnKey;
        $this->sortWeight = $sortWeight;
    }

    /** @return string */
    public function getText(): string { return $this->text; }

    /** @return string */
    public function getId(): string { return $this->optionId; }

    /** @return int|string */
    public function getValue() { return $this->value; }

    /** @return string */
    public function getRowKey(): string { return $this->rowKey; }

    /** @return string */
    public function getColumnKey(): string { return $this->columnKey; }

    /** @return int */
    public function getSortWeight(): int { return $this->sortWeight; }

}