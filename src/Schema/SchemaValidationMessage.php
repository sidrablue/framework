<?php
/**
 * Copyright (c) BlueSky Digital Labs Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Schema;


class SchemaValidationMessage
{
    private $validName = false;
    private $unique = false;
    private $errorCode = SchemaValidation::ERROR_FREE;
    private $errorMessage = null;

    /**
     * SchemaValidationMessage constructor.
     * @param bool $isValidName
     * @param bool $isUnique
     * @param int $errorCode
     * @param string|null $errorMessage
     */
    public function __construct(bool $isValidName, bool $isUnique, int $errorCode = SchemaValidation::ERROR_FREE, ?string $errorMessage = null)
    {
        $this->validName = $isValidName;
        $this->unique = $isUnique;
        $this->errorCode = $errorCode;
        $this->errorMessage = $errorMessage;
    }

    public function isValid(): bool
    {
        return $this->isValidName() &&
            $this->isUnique();
    }

    /** @return bool */
    public function isValidName(): bool { return $this->validName; }

    /** @return bool */
    public function isUnique(): bool { return $this->unique; }

    /** @return int */
    public function getErrorCode(): int { return $this->errorCode; }

    /** @return string|null */
    public function getErrorMessage(): ?string { return $this->errorMessage; }


}