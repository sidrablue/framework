<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 */

declare(strict_types=1);

namespace BlueSky\Framework\Schema;


class Row
{
    /** @var string $id */
    private $id;

    /** @var string $text */
    private $text;

    /** @var Column[] $columns */
    private $columns;

    /** @var int $index */
    private $index;

    /**
     * Row constructor.
     * @param string $id
     * @param int $index
     * @param Column[] $columns
     * @param string|null $text
     */
    public function __construct(string $id, int $index, array $columns, ?string $text = null)
    {
        $this->id = $id;
        $this->index = $index;
        $this->text = $text;
        $this->columns = $columns;
    }

    /** @return string */
    public function getId(): string { return $this->id; }

    /** @return int */
    public function getIndex(): int { return $this->index; }

    /** @return string */
    public function getText(): string { return $this->text; }

    /** @return Column[] */
    public function getColumns(): array { return $this->columns; }

}