<?php
/**
 * Copyright (c) BlueSky Digital Labs Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Schema;

use BlueSky\Framework\Object\State as BaseState;
use BlueSky\Framework\Model\Schema\Entity as EntityModel;
use BlueSky\Framework\Model\Schema\Field as FieldModel;

class SchemaValidation extends BaseState
{

    public const ERROR_FREE = 0;
    public const ERROR_EMPTY_NAME = 1;
    public const ERROR_NOT_ALPHANUM = 2;
    public const ERROR_NUM_PREFIX = 3;
    public const ERROR_RESERVED_PHP = 4;
    public const ERROR_RESERVED_MYSQL = 5;
    public const ERROR_RESERVED_JS = 6;
    public const ERROR_NOT_UNIQUE = 7;

    /**
     * @var string[] - PHP standard reserved keywords more info at http://php.net/manual/en/reserved.keywords.php
     */
    public const reservedPhpKeywords = [
        '__class__', '__dir__', '__file__', '__function__', '__line__', '__method__', '__namespace__', '__trait__', /// compile time constants lowercased for logic here
        '__halt_compiler', 'abstract', 'and', 'array', 'as', 'break', 'callable', 'case', 'catch',
        'class', 'clone', 'const', 'continue', 'declare', 'default', 'die', 'do', 'echo', 'else', 'elseif', 'empty',
        'enddeclare', 'endfor', 'endforeach', 'endif', 'endswitch', 'endwhile', 'eval', 'exit', 'extends', 'final', 'for',
        'foreach', 'function', 'global', 'goto', 'if', 'implements', 'include', 'include_once', 'instanceof', 'insteadof',
        'interface', 'isset', 'list', 'namespace', 'new', 'or', 'print', 'private', 'protected', 'public', 'require',
        'require_once', 'return', 'static', 'switch', 'throw', 'trait', 'try', 'unset', 'use', 'var', 'while', 'xor'
    ];

    public const reservedMySqlKeywords = [
        'accessible', 'add', 'all','alter','analyze','and','as','asc','asensitive','before','between','bigint','binary','blob', 'both', 'by', 'call', 'cascade',
        'case', 'change', 'char', 'character', 'check', 'collate', 'column', 'condition', 'constraint', 'continue', 'convert', 'create', 'cross', 'current_date', 'current_time',
        'current_timestamp', 'current_user', 'cursor', 'database', 'databases', 'day_hour', 'day_microsecond', 'day_minute',
        'day_second', 'dec', 'decimal', 'declare', 'default', 'delayed', 'delete', 'desc', 'describe', 'deterministic', 'distinct', 'distinctrow', 'div', 'double', 'drop', 'dual',
        'each', 'else', 'elseif', 'enclosed', 'escaped', 'except', 'added', 'in', 'mariadb', '10.3.0', 'exists', 'exit', 'explain', 'false', 'fetch', 'float', 'float4', 'float8',
        'for', 'force', 'foreign', 'from', 'fulltext', 'general', 'grant', 'group', 'having', 'high_priority', 'hour_microsecond', 'hour_minute', 'hour_second', 'if', 'ignore', 'ignore_server_ids',
        'in', 'index', 'infile', 'inner', 'inout', 'insensitive', 'insert', 'int','int1', 'int2', 'int3', 'int4', 'int8', 'integer', 'intersect', 'interval', 'into', 'is',
        'iterat', 'join', 'key', 'keys', 'kill', 'leading', 'leave', 'left', 'like', 'limit', 'linear', 'lines', 'load', 'localtime', 'localtimestamp', 'lock', 'long', 'longblob', 'longtext', 'loop',
        'low_priority', 'master_heartbeat_period', 'added', 'in', 'mariadb', '5.5', 'master_ssl_verify_server_cert', 'match', 'maxvalue	added', 'in', 'mariadb', '5.5', 'mediumblob', 'mediumint',
        'mediumtext', 'middleint', 'minute_microsecond', 'minute_second', 'mod', 'modifies', 'natural', 'not','no_write_to_binlog', 'null', 'numeric', 'on', 'optimize', 'option', 'optionally', 'or',
        'order', 'out', 'outer', 'outfile', 'over', 'partition', 'precision', 'primary', 'procedure', 'purge', 'range', 'read', 'reads', 'read_write', 'real', 'recursive', 'references', 'regexp',
        'release', 'rename', 'repeat', 'replace', 'require', 'resignal', 'restrict', 'return', 'returning', 'revoke','right', 'rlike', 'rows', 'schema', 'schemas', 'second_microsecond', 'select',
        'sensitive', 'separator', 'set', 'show', 'signal', 'slow', 'smallint', 'spatial', 'specific', 'sql', 'sqlexception', 'sqlstate', 'sqlwarning', 'sql_big_result', 'sql_calc_found_rows',
        'sql_small_result', 'ssl', 'starting', 'straight_join', 'table', 'terminated', 'then', 'tinyblob', 'tinyint', 'tinytext', 'to', 'trailing', 'trigger', 'true', 'undo', 'union', 'unique',
        'unlock', 'unsigned', 'update', 'usage', 'use', 'using', 'utc_date', 'utc_time', 'utc_timestamp', 'values', 'varbinary', 'varchar', 'varcharacter', 'varying', 'when', 'where', 'while',
        'window', 'with', 'write', 'xor', 'year_month', 'zerofill'
    ];

    public const reservedJavascriptKeywords = [
        'abstract','arguments','await','boolean','break','byte','case','catch','char','class','const','continue',
        'debugger','default','delete','do','double','else','enum','eval','export','extends','false','final',
        'finally','float','for','function','goto','if','implements','import','in','instanceof','int','interface',
        'let','long','native','new','null','package','private','protected','public','return','short','static',
        'super','switch','synchronized','this','throw','throws','transient','true','try','typeof','var',
        'void','volatile','while','with','yield'
    ];

    /**
     * @param string $name - the name entered by the user. This will be converted to a reference
     * @param string|null $parentReference - the name entered by the user. This will be converted to a reference
     * @param bool $sanitiseName - clean up any non alphanum characters from the generated reference
     * @return SchemaValidationMessage
     */
    public function validateEntityReference(string $name, ?string $parentReference = null, bool $sanitiseName = false): SchemaValidationMessage
    {
        $isValid = false;
        $isUnique = false;
        $errorMessage = null;
        $errorCode = self::ERROR_FREE;
        $name = trim($name);
        if (empty($name)) {
            $errorCode = self::ERROR_EMPTY_NAME;
            $errorMessage = 'Please enter a name';
        } else {
            $em = new EntityModel($this->getState());
            $reference = strtolower($em->createReferenceFromName($name, null, $sanitiseName));
            $className = strtolower($em->createClassNameFromReference($reference));
            if (!ctype_alnum(str_replace(' ', '', $reference))) {
                $errorCode = self::ERROR_NOT_ALPHANUM;
                $errorMessage = 'Please enter only alphanumeric characters';
            } elseif (ctype_digit($reference[0])) {
                $errorCode = self::ERROR_NUM_PREFIX;
                $errorMessage = 'Name cannot start with a number';
            } elseif (in_array($reference, self::reservedPhpKeywords) && !in_array($className, self::reservedPhpKeywords)) {
                $errorCode = self::ERROR_RESERVED_PHP;
                $errorMessage = 'This name cannot be used';
            } elseif (in_array($reference, self::reservedMySqlKeywords) && !in_array($className, self::reservedMySqlKeywords)) {
                $errorCode = self::ERROR_RESERVED_MYSQL;
                $errorMessage = 'This name cannot be used';
            } elseif (in_array($reference, self::reservedJavascriptKeywords) && !in_array($className, self::reservedJavascriptKeywords)) {
                $errorCode = self::ERROR_RESERVED_JS;
                $errorMessage = 'This name cannot be used';
            } elseif (!preg_match('/^[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*$/', $className)) {
                $errorCode = self::ERROR_NOT_ALPHANUM;
                $errorMessage = 'Name contains invalid characters';
            } else {
                $isValid = true;
                $fullReference = $em->createReferenceFromName($name, $parentReference, $sanitiseName);
                $isUnique = $this->getState()->getSchema()->getEntityByReference($fullReference) === null;
                if (!$isUnique) {
                    $errorCode = self::ERROR_NOT_UNIQUE;
                    $errorMessage = 'This name already exists';
                }
            }
        }
        return new SchemaValidationMessage($isValid, $isUnique, $errorCode, $errorMessage);
    }

    /**
     * @param string $entityReference
     * @param string $fieldName - the name entered by the user. This will be converted to a reference
     * @param bool $sanitiseName - clean up any non alphanum characters from the generated reference
     * @return SchemaValidationMessage
     */
    public function validateFieldReference(string $entityReference, string $fieldName, bool $sanitiseName = false): SchemaValidationMessage
    {
        $isValid = false;
        $isUnique = false;
        $errorMessage = null;
        $errorCode = self::ERROR_FREE;
        $fieldName = trim($fieldName);
        if (empty($fieldName)) {
            $errorCode = self::ERROR_EMPTY_NAME;
            $errorMessage = 'Please enter a name';
        } else {
            $em = new FieldModel($this->getState());
            $reference = strtolower($em->createReferenceFromName($fieldName, $sanitiseName));
            if (!ctype_alnum(str_replace('_', '', $reference))) {
                $errorCode = self::ERROR_NOT_ALPHANUM;
                $errorMessage = 'Please enter only alphanumeric characters';
            } elseif (ctype_digit($reference[0])) {
                $errorCode = self::ERROR_NUM_PREFIX;
                $errorMessage = 'Name cannot start with a number';
            } elseif (in_array($reference, self::reservedPhpKeywords)) {
                $errorCode = self::ERROR_RESERVED_PHP;
                $errorMessage = 'This name cannot be used';
            } elseif (in_array($reference, self::reservedMySqlKeywords)) {
                $errorCode = self::ERROR_RESERVED_MYSQL;
                $errorMessage = 'This name cannot be used';
            } elseif (in_array($reference, self::reservedJavascriptKeywords)) {
                $errorCode = self::ERROR_RESERVED_JS;
                $errorMessage = 'This name cannot be used';
            } elseif (!preg_match('/^[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*$/', $reference)) {
                $errorCode = self::ERROR_NOT_ALPHANUM;
                $errorMessage = 'Name contains invalid characters';
            } else {
                $entity = $this->getState()->getSchema()->getEntityByReference($entityReference);
                if (!is_null($entity)) {
                    $isValid = true;
                    $isUnique = $entity->getEntityField($reference) === null;
                    if (!$isUnique) {
                        $errorCode = self::ERROR_NOT_UNIQUE;
                        $errorMessage = 'This name already exists';
                    }
                }
            }
        }
        return new SchemaValidationMessage($isValid, $isUnique, $errorCode, $errorMessage);
    }

}
