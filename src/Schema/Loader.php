<?php
/**
 * Copyright (c) BlueSky Digital Labs Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Schema;

use Psr\SimpleCache\InvalidArgumentException;
use BlueSky\Framework\Controller\Install\Update;
use BlueSky\Framework\Entity\Schema\Entity as SchemaEntity;
use BlueSky\Framework\Model\Schema\Entity as EntityModel;
use BlueSky\Framework\Model\Schema\Field as FieldModel;
use BlueSky\Framework\Entity\Schema\Field as SchemaField;
use BlueSky\Framework\Object\State as StateBase;
use BlueSky\Framework\Entity\Schema\Relation as RelationEntity;
use BlueSky\Framework\Util\Strings;

/**
 * Schema service
 * @package Lote\App\CustomFields\Service
 */
class Loader extends StateBase implements \JsonSerializable
{

    private const CACHE_ENTITY_NAME = 'core-loader-entities';

    /**
     * This variable contains the entities with both flat and hierarchy design
     * For example, if item 3 has a child entity (item 4), then item 4 will both be in the childEntities of item 3 and it is key (reference) of the entities map
     * This is used for finding the entity in an O(1) manner
     *
     * @var SchemaEntity[] $entities
     */
    private $entities = [];

    public function cleanLoad(): void
    {
        $this->entities = $this->buildEntitiesMap();
        $this->rebuildEntityCache();
    }

    public function _forceAddEntity(SchemaEntity $e): void { $this->entities[$e->reference] = $e; }

    private function getCacheName(string $entityReference): string { return strtolower(str_replace('\\', '-', $entityReference)); }

    /**
     * This function will update the entities and update the entity cache
     * @param SchemaEntity[] $data
     */
    public function rebuildEntityCache(array $data = []): void
    {
        if (empty($data)) {
            $data = $this->getEntitiesHierarchical();
        }

        foreach ($data as $entity) {
            $entitiesSerialised = json_encode($entity);
            try {
                $this->getState()->getDbCache()->set(self::CACHE_ENTITY_NAME . '-' . $this->getCacheName($entity->reference), $entitiesSerialised);
            } catch (InvalidArgumentException $e) {
                $this->getState()->getLoggers()->getMasterLogger()->error('Could not save schema cache to redis');
            }
        }
    }

    /**
     * This will update the entities map directly to append the entity children
     * The appending will ensure that the flat structure of the map is defined
     *
     * @NOTE: this function will recursively get all children. This means that it can support indefinite levels of parent-children classes
     *
     * @param SchemaEntity[] $entities
     * @param SchemaEntity $entity
     */
    private function appendFlattenedEntityChildren(array &$entities, SchemaEntity $entity): void
    {
        $childEntities = $entity->getChildEntities();
        foreach ($childEntities as $childEntity) {
            /** @var SchemaEntity $childEntity */
            $entityReference = $childEntity->reference;
            $entities[$entityReference] = $childEntity;
            foreach ($childEntity->getChildEntities() as $childChildEntity) {
                /** @var SchemaEntity $childChildEntity */
                $childEntityReference = $childChildEntity->reference;
                $entities[$entityReference] = $childEntityReference;
                $this->appendFlattenedEntityChildren($entities, $childChildEntity);
            }
        }
    }

    /**
     * This function will generate a fresh map of the virtual entities
     * The map will contain both the flat and hierarchy structures as well as the fields for the entities
     *
     * @return SchemaEntity[]
     */
    private function buildEntitiesMap(): array
    {
        /// With the reference including the full hierarchy, we can sort by the most root entities to most child entities
        /// This way, the entire entities map can be generated with a single loop pass
        $entitiesData = $this->getState()->getReadDb()->createQueryBuilder()
            ->select('e.*')
            ->addSelect('LENGTH(e.reference) - LENGTH(REPLACE(e.reference, "\\\", "")) as _parent_order')
            ->from('sb__schema_entity', 'e')
            ->andWhere('e.lote_deleted is null')
            ->addOrderBy('_parent_order', 'asc')
            ->addOrderBy('e.sort_order', 'asc')
            ->execute()->fetchAll(\PDO::FETCH_ASSOC);

        $entityFields = $this->getEntityFields();
        $entityRelations = $this->getEntityRelations();

        /** @var SchemaEntity[] $entities */
        $entities = [];

        foreach ($entitiesData as $entity) {
            $entity = new SchemaEntity($this->getState(), $entity);

            if ($fields = $entityFields[$entity->reference] ?? null) {
                $entity->setEntityFields($fields);
            }

            if ($relations = $entityRelations[$entity->reference] ?? null) {
                $entity->setRelation(new RelationManager($relations));
            }

            /// We cant use getParentEntity here because it hasn't been setup yet
            $parentEntityRef = $entity->getParentEntityReference();
            if (!empty($parentEntityRef)) {
                /// We can be certain that a parent entity will be found
                if ($parentEntity = $entities[$parentEntityRef] ?? null) {
                    $parentEntity->addChildEntity($entity);
                    $entity->setParentEntity($parentEntity);
                }
            }
            $entities[$entity->reference] = $entity;
        }

        return $entities;
    }

    private function getEntityRelations(): array
    {
        $result = [];
        $q = $this->getState()->getReadDb()->createQueryBuilder();
        $q->select('r.*')
            ->from('sb__schema_relation', 'r')
            ->andWhere('r.lote_deleted is null');

        if ($data = $q->execute()->fetchAll(\PDO::FETCH_ASSOC)) {
            foreach ($data as $d) {
                $re = new RelationEntity($this->getState(), $d);
                $entityRef = $re->from_entity_reference;

                $relationsMap = $result[$entityRef] ?? [];
                $relationsMap[] = $re;

                $result[$entityRef] = $relationsMap;
            }
        }
        return $result;
    }

    /**
     * This function will generate a map of vectors from the fields. The vector is to group the fields for the same entity
     * The map has the key of the entity id and the value is the vector group of fields
     *
     * @return SchemaField[]
     */
    private function getEntityFields(): array
    {
        $result = [];
        $q = $this->getState()->getReadDb()->createQueryBuilder();
        $q->select('f.*')
            ->from('sb__schema_field', 'f')
            ->andWhere('f.lote_deleted is null')
            ->addOrderBy('f.sort_order', 'asc');

        if ($data = $q->execute()->fetchAll(\PDO::FETCH_ASSOC)) {
            foreach ($data as $d) {
                $fe = new SchemaField($this->getState(), $d);
                $entityRef = $fe->schema_entity_ref;

                $fieldsMap = $result[$entityRef] ?? [];
                $fieldsMap[$fe->reference] = $fe;

                $result[$entityRef] = $fieldsMap;
            }
        }

        return $result;
    }

    /**
     * Get a virtual entity by its reference (tablename)
     *
     * @fixme This function will return the same object that is being used in the core internals
            Any modifications outside of this class to the entity, will be reflected here as well
            This means that the loader can then be out of sync with the database
     *
     * @fixme due to some inconsistent outputs, this function will no longer check the redis cache to load the entity data
     *
     * @access private
     * @param string $reference
     * @return null|SchemaEntity
     */
    public function getEntityByReference(string $reference): ?SchemaEntity
    {
        $output = null;
//        try {
            if (isset($this->entities[$reference])) {
                $output = $this->entities[$reference];
//            } elseif ($cacheOutput = $this->getState()->getDbCache()->get(self::CACHE_ENTITY_NAME . '-' . $this->getCacheName($reference))) {
//                if ($cacheData = json_decode($cacheOutput, true)) {
//                    $se = new SchemaEntity($this->getState(), $cacheData);
//                    $entityReference = $se->reference;
//                    $this->entities[$entityReference] = $se;
//                    $this->appendFlattenedEntityChildren($this->entities, $se);
//                    $output = $se;
//                }
            } else {
                /// Worst case scenario when cache is not loaded. Do a direct db lookup
                $item = new SchemaEntity($this->getState());
                if ($item->loadByField('reference', $reference)) {
                    $this->entities[$reference] = $item;
                    $this->appendFlattenedEntityChildren($this->entities, $item);
                    $output = $item;
                }
            }
//        } catch (InvalidArgumentException $e) {
//            $this->getState()->getLoggers()->getMasterLogger('schema-loader')->critical("Error occurred in the schema loader: {$e->getMessage()}");
//        }
        return $output;
    }

    /**
     * Get the definition of the schema as an array
     *
     * @access public
     * @return array
     */
    public function toArray(): array
    {
        $result = [];
        foreach ($this->entities as $entity) {
            /** @var SchemaEntity $entity */
            $result[] = $entity->toArray();
        }
        return $result;
    }

    public function jsonSerialize()
    {
        return json_encode($this->toArray());
    }

    /**
     * This function will create a new map based on the one already loaded in memory
     * This map however will only contain a hierarchical structure instead of the flat + hierarchical structure of the map object in memory
     *
     * @Note: this function will have a complexity of O(n) to generate the new map
     *
     * @return SchemaEntity[]
     */
    public function getEntitiesHierarchical(): array
    {
        $data = [];
        foreach ($this->entities as $entity) {
            if (is_null($entity->getParentEntity())) { /// Don't duplicate the information by saving the reference objects and flat structures
                $data[$entity->reference] = $entity;
            }
        }
        return $data;
    }

    /**
     * This function will return a clone of the entities currently in memory
     * @return SchemaEntity[]
     */
    public function getEntitiesFlat(): array
    {
        $data = $this->entities;
        return $data;
    }

    /**
     * @param string|SchemaEntity $entity - either the object itself or its reference
     * @return bool
     */
    public function deleteEntity($entity): bool
    {
        $output = false;
        $entityRef = is_string($entity) ? $entity : null;
        if ($entity instanceof SchemaEntity) {
            $entityRef = $entity->reference;
        }

        if (!empty($entityRef)) {
            $e = $this->getEntityByReference($entityRef);
            $output = $e->delete(false);
        }
        return $output;
    }

    /**
     * @param string|SchemaEntity $entity - either the object itself or its reference
     * @param string|SchemaField $field - either the object itself or its reference
     * @return bool
     */
    public function deleteField($entity, $field): bool
    {
        $output = false;
        $entityRef = is_string($entity) ? $entity : null;
        if ($entity instanceof SchemaEntity) {
            $entityRef = $entity->reference;
        }

        $fieldRef = is_string($field) ? $field : null;
        if ($field instanceof SchemaField) {
            $fieldRef = $field->reference;
        }

        if (!empty($entityRef) && !empty($fieldRef)) {
            $e = $this->getEntityByReference($entityRef);
            if ($f = $e->getEntityField($fieldRef)) {
                if ($f->is_virtual) {
                    $e->removeEntityField($f);
                    $output = $f->delete();
                    $this->saveToDb($e);
                }
            }
        }
        return $output;
    }

    private function saveToDb(SchemaEntity $entity)
    {
        $u = new Update($this->getState());
        $u->updateByEntity($entity);
    }

    /**
     * @todo FIXME Do some testing around this function to ensure the internal state maintains the correct entity structure
     * @param SchemaEntity $entity - can be the array representation (from forms) or the entity itself
     * @param SchemaEntity|string $parentEntity - the object itself or the reference to it
     *                                            Parent entity is only used for new entity creation
     *                                            Existing entities will already have the correct reference
     * @param bool $forceGenerateUniqueReference - true if a unique reference is to be generated for this entity upon creation
     * @return null|SchemaEntity
     * @throws \Exception
     */
    public function saveEntity(SchemaEntity $entity, $parentEntity = null, $forceGenerateUniqueReference = false): ?SchemaEntity
    {
        $output = null;
        if (!is_null($entity)) {
            if (!empty($entity->reference)) {
                $match = $this->getEntityByReference($entity->reference);
                if ($match !== null && $match->is_virtual) {
                    $match->name = $entity->name;
                    $match->description = $entity->description;
                    $match->sort_order = $entity->sort_order;
                    $match->is_active = $entity->is_active;
                    $output = $match;
                }
            } else {
                $output = $this->createEntityData($entity, $parentEntity, $forceGenerateUniqueReference);
            }

            if (!is_null($output)) {
                $output->save();

                $this->entities[$output->reference] = $output;
                $this->saveToDb($output);
                $this->rebuildEntityCache([$output]);
            }
        }

        return $output;
    }

    /**
     * THIS FUNCTION WILL NOT ACTUALLY SAVE THE DATA TO THE DB
     *
     * @param SchemaEntity $entity
     * @param null $parentEntity
     * @param bool $forceGenerateUniqueReference - true if a unique reference is to be generated for this entity upon creation
     * @return SchemaEntity
     */
    private function createEntityData(SchemaEntity $entity, $parentEntity = null, bool $forceGenerateUniqueReference = false): SchemaEntity
    {
        $parentEntityRef = is_string($parentEntity) ? $parentEntity : null;
        if ($parentEntity instanceof SchemaEntity) {
            $parentEntityRef = $parentEntity->reference;
        }

        $em = new EntityModel($this->getState());
        $sv = new SchemaValidation($this->getState());
        $name = $entity->name;
        $validationMessage = $sv->validateEntityReference($name, $parentEntityRef, $forceGenerateUniqueReference);
        if ($forceGenerateUniqueReference) {
            while (!$validationMessage->isValid() && !$validationMessage->isUnique()) {
                $name = $entity->name . Strings::generateRandomString(4);
                $validationMessage = $sv->validateEntityReference($name, $parentEntity !== null ? $parentEntity->reference : null, true);
            }
        }

        if (!$validationMessage->isValid()) {
            throw new \InvalidArgumentException($validationMessage->getErrorMessage());
        }

        $entity->reference = $em->createReferenceFromName($name, $parentEntityRef, $forceGenerateUniqueReference);
        $entity->class_name = $em->getVirtualAccountEntityNamespace() . $em->createClassNameFromReference($entity->reference);
        $entity->is_virtual = true;

        if (!empty($parentEntityRef)) {
            if ($parentEntityInternal = $this->getEntityByReference($parentEntityRef)) {
                $entity->setParentEntity($parentEntityInternal);
                $parentEntityInternal->addChildEntity($entity);
            }
        }
        return $entity;
    }

    /**
     * FIXME: The fields need to be updated for matched and input so that the correct fields are updated and the reference generation for the input creation
     * @param string $entityReference
     * @param SchemaField $fieldEntity
     * @param bool $forceGenerateUniqueReference - true if a unique reference is to be generated for this entity upon creation
     * @return null|SchemaField
     * @throws \Exception
     */
    public function saveField(string $entityReference, SchemaField $fieldEntity, bool $forceGenerateUniqueReference = false): ?SchemaField
    {
        $output = null;
        if (!is_null($fieldEntity)) {
            if ($entity = $this->getEntityByReference($entityReference)) {
                if (!empty($fieldEntity->reference)) {
                    if ($fieldEntity->is_virtual) {
                        $match = $entity->getEntityField($fieldEntity->reference);
                        if ($match !== null && $match->is_virtual) {
                            $match->field_type = $fieldEntity->field_type;
                            $match->name = $fieldEntity->name;
                            $match->description = $fieldEntity->description;
                            $match->default_value = $fieldEntity->default_value;
                            $match->sort_order = $fieldEntity->sort_order;
                            $match->is_active = $fieldEntity->is_active;
                            $match->is_read_only = $fieldEntity->is_read_only;
                            $match->config_data = $fieldEntity->config_data;
                            $match->options_structure = $fieldEntity->options_structure;
                            $match->is_mandatory = $fieldEntity->is_mandatory;
                            $match->is_unique = $fieldEntity->is_unique;
                            $match->is_hidden = $fieldEntity->is_hidden;
                            $output = $match;
                        }
                    }
                } else {
                    $output = $this->createFieldData($entityReference, $fieldEntity, $forceGenerateUniqueReference);
                    $output->setParentEntity($entity);
                    $entity->addEntityField($fieldEntity);
                }

                if (!is_null($output)) {
                    $output->save();

                    $this->saveToDb($output->getParentEntity());
                    /// Don't need to update the internal entities array since the object references automatically cause it to update
                    $this->rebuildEntityCache([$entity]);
                }
            }
        }

        return $output;
    }

    private function createFieldData(string $entityReference, SchemaField $fieldEntity, bool $forceGenerateUniqueReference = false): SchemaField
    {
        $sv = new SchemaValidation($this->getState());

        $name = $fieldEntity->name;

        $validationMessage = $sv->validateFieldReference($entityReference, $name, $forceGenerateUniqueReference);

        if ($forceGenerateUniqueReference) {
            while (!$validationMessage->isValid() && !$validationMessage->isUnique()) {
                $name = $fieldEntity->name . Strings::generateRandomString(4);
                $validationMessage = $sv->validateFieldReference($entityReference, $name, true);
            }
        }
        if (!$validationMessage->isValid()) {
            throw new \InvalidArgumentException($validationMessage->getErrorMessage());
        }

        $fm = new FieldModel($this->getState());
        $reference = $fm->createReferenceFromName($name, $forceGenerateUniqueReference);

        $fieldEntity->reference = $reference;
        $fieldEntity->schema_entity_ref = $entityReference;
        $fieldEntity->is_virtual = true;

        return $fieldEntity;
    }

}
