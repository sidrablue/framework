<?php

declare(strict_types=1);

namespace BlueSky\Framework\Cache\Database\Adapter;

use Psr\SimpleCache\CacheInterface;
use BlueSky\Framework\Cache\Database\Base;
use BlueSky\Framework\Registry\Config;
use Symfony\Component\Cache\Simple\FilesystemCache;

/**
 * Base controller for Core Controllers
 * @package BlueSky\Framework\Controller\Core
 * */
class Filesystem extends Base
{

    private const CACHE_DEFAULT_TTL = 60;

    /**
     * @param string $namespace
     * @param Config $config
     * @return CacheInterface
     */
    function getAdapter(string $namespace, Config $config): CacheInterface
    {
        $directory = LOTE_TEMP_PATH.$namespace.'cache/db';
        $ttl = $config->get('cache.db.filesystem.ttl', self::CACHE_DEFAULT_TTL);
        return new FilesystemCache($namespace, $ttl, $directory);
    }

}
