<?php

declare(strict_types=1);

namespace BlueSky\Framework\Cache\Database\Adapter;

use Psr\SimpleCache\CacheInterface;
use BlueSky\Framework\Cache\Database\Base;
use BlueSky\Framework\Registry\Config;

/**
 * Base controller for Core Controllers
 * @package BlueSky\Framework\Controller\Core
 * */
class NullCache extends Base
{

    /**
     * @param string $namespace
     * @param Config $config
     * @return CacheInterface
     */
    function getAdapter(string $namespace, Config $config): CacheInterface
    {
        return new \Symfony\Component\Cache\Simple\NullCache();
    }

}
