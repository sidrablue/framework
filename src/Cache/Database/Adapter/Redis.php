<?php

declare(strict_types=1);

namespace BlueSky\Framework\Cache\Database\Adapter;

use Predis\Client;
use Psr\SimpleCache\CacheInterface;
use BlueSky\Framework\Cache\Database\Base;
use BlueSky\Framework\Registry\Config;
use Symfony\Component\Cache\Simple\RedisCache;

/**
 * Base controller for Core Controllers
 * @package BlueSky\Framework\Controller\Core
 * */
class Redis extends Base
{

    /**
     * @param string $namespace
     * @param Config $config
     * @return CacheInterface
     */
    function getAdapter(string $namespace, Config $config): CacheInterface
    {
        $result = new NullCache();
        if ($config) {
            $parameters = [];
            $parameters['host'] = $config->get('cache.db.redis.host');
            $parameters['port'] = $config->get('cache.db.redis.port');
            $parameters['scheme'] = $config->get('cache.db.redis.scheme') ?? 'redis';
            $parameters['database'] = $config->get('cache.db.redis.database');
            $result = new RedisCache(new Client($parameters, ['prefix' => "{$namespace}:lote:db:"]));
        }
        return $result;
    }

}
