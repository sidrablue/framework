<?php

declare(strict_types=1);

namespace BlueSky\Framework\Cache\Database;

use Psr\SimpleCache\CacheInterface;
use BlueSky\Framework\Registry\Config;

/**
 * Base controller for Core Controllers
 * @package BlueSky\Framework\Controller\Core
 * */
abstract class Base
{

    abstract function getAdapter(string $namespace, Config $config) : CacheInterface;

}
