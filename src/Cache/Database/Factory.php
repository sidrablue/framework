<?php

declare(strict_types=1);

namespace BlueSky\Framework\Cache\Database;

use Psr\SimpleCache\CacheInterface;

/**
 * Base controller for Core Controllers
 * @package BlueSky\Framework\Controller\Core
 * */
class Factory
{

    /**
     * Create an instance of field data
     * @param \BlueSky\Framework\State\Base $state
     * @return CacheInterface
     * */
    public static function createInstance(\BlueSky\Framework\State\Base $state) : CacheInterface
    {
        $reference = $state->getSettings()->getConfigOrSetting("cache.db.adapter", 'NullCache');
        $referenceClass = __NAMESPACE__ . '\Adapter\\'.ucfirst($reference);
        if (class_exists($referenceClass)) {
            $className = $referenceClass;
        }
        else {
            $className = __NAMESPACE__ . '\Adapter\NullCache';
        }
        /**
         * @var \BlueSky\Framework\Cache\Database\Base $adapterClass
         * */
        $adapterClass = new $className($state);
        return $adapterClass->getAdapter($state->accountReference, $state->getConfig());
    }

}
