<?php
namespace BlueSky\Framework\Cache\File\Adapter;

use League\Flysystem\AwsS3v3\AwsS3Adapter;
use BlueSky\Framework\Cache\File\Base;

/**
 * Base controller for Core Controllers
 * @package BlueSky\Framework\Controller\Core
 * */
class Aws extends Base
{

    /**
     * @return \League\Flysystem\Adapter\AbstractAdapter
     * */
    protected function getAdapter()
    {
        $client = $this->getState()->getS3Client();
        return new AwsS3Adapter($client, $this->getState()->getSettings()->get('media.cache.aws.bucket'),
            $this->getState()->getSettings()->get('media.cache.aws.prefix'));
    }

    /**
     * Check if an item is cached
     * @access public
     * @param string|array $key
     * @param string $content
     * @param null|array $metadata
     * @return boolean
     */
    public function put($key, $content, $metadata = null)
    {
        $f = finfo_open();
        $contentType = finfo_buffer($f, $content, FILEINFO_MIME_TYPE);
        finfo_close($f);
        $config = ['visibility' => 'public', 'CacheControl' => "private, max-age=604800", 'ContentType' => $contentType];
        return $this->fileSystem->put($this->getCacheKey($key), $content, $config);
    }


    /**
     * Invalidate a file or folder if used in a CDN
     * @param string $location
     * @return boolean
     * */
    protected function invalidatePath($location)
    {
        //      if($cloudFrontId = $this->getState()->getSettings()->get("media.cloudfront.id")) {
        /*            $s3 = new AmazonS3();
                    $cdn = new AmazonCloudFront();

        // Get the information about the distribution.
                    $distribution_info = $cdn->get_distribution_info('E2L6A3OZHQT5W4');

        // Fetch the fully-qualified domain name for the origin bucket.
                    $origin_bucket_fqdn = (string) $distribution_info->body->DistributionConfig->S3Origin->DNSName;

        // Strip the hostname, and only leave the bucket name.
                    $origin_bucket = substr($origin_bucket_fqdn, 0, -17); // Remove `.s3.amazonaws.com`

        // Get the list of files from the S3 bucket.
                    $file_paths = $s3->get_object_list($origin_bucket);

        // Create a new invalidation.
                    $response = $cdn->create_invalidation('E2L6A3OZHQT5W4', 'aws-php-sdk-test' . time(), $file_paths);

        // Success?
                    var_dump($response->isOK());*/
        //       }
    }

}
