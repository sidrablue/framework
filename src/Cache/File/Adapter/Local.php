<?php
namespace BlueSky\Framework\Cache\File\Adapter;

use BlueSky\Framework\Cache\File\Base;
use League\Flysystem\Filesystem;
use League\Flysystem\Adapter\Local as LocalAdapter;
use BlueSky\Framework\Util\Dir;

/**
 * Base controller for Core Controllers
 * @package BlueSky\Framework\Controller\Core
 * */
class Local extends Base
{

    /**
     * @return \League\Flysystem\Adapter\AbstractAdapter
     * */
    protected function getAdapter()
    {
        return new LocalAdapter(LOTE_ASSET_PATH . 'cache/' . $this->getState()->accountReference . '/media/thumb/');
    }

}
