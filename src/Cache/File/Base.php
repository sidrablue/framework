<?php
namespace BlueSky\Framework\Cache\File;

use League\Flysystem\Filesystem;
use BlueSky\Framework\Event\Data;
use BlueSky\Framework\Model\File;
use BlueSky\Framework\Object\State as StateBase;
use BlueSky\Framework\Util\Image\Resize;
use BlueSky\Framework\Util\Strings;
use BlueSky\Framework\Util\Time;

/**
 * Base controller for Core Controllers
 * @package BlueSky\Framework\Controller\Core
 * */
class Base extends StateBase
{

    /**
     * @const int GLOBAL_AGE - the age in seconds after which images are considered expired by default
     * */
    const MAX_CACHE_SECONDS = 28800; // 8 hours

    /**
     * @var \League\Flysystem\Filesystem $fileSystem
     * */
    protected $fileSystem;

    /**
     * Check if an image has already been cached
     * @access public
     * @param string $reference
     * @param boolean $autoCache
     * @param int $width
     * @param int $height
     * @param int $size
     * @param int $timestamp
     * @param string $mode
     * @return boolean
     * */
    public function imageIsCached($reference, $autoCache = false, $width = 0, $height = 0, $size = 0, $timestamp = 0, $mode = '')
    {
        $result = false;
        $cacheKey = $this->getCacheKey($this->getKeyArray($reference, $width, $height, $size, $timestamp, $mode));

        if ($this->hasByReference($cacheKey)) {
            $result = $cacheKey;
        } elseif ($autoCache) {
            $f = new File($this->getState());
            if ($content = $f->getContentByReference($reference)) {
                if ($width > 0 || $height > 0) {
                    $r = new Resize(null, $content);
                    $r->resizeImage($width, $height, $mode);
                    if ($this->putByReference($cacheKey, $r->getContent())) {
                        $result = $cacheKey;
                    }
                } else if ($this->putByReference($cacheKey, $content)) {
                    $result = $cacheKey;
                }
            }
        }
        return $result;
    }

    /**
     * On create handler to be implemented in child
     * @access public
     * @return void
     * */
    protected function onCreate()
    {
        $this->fileSystem = new Filesystem($this->getAdapter());
    }

    /**
     * @return \League\Flysystem\Adapter\AbstractAdapter
     * */
    protected function getAdapter()
    {
        //implement in child
    }

    /**
     * @return \League\Flysystem\Filesystem
     */
    protected function getFileSystem()
    {
        return $this->fileSystem;
    }

    /**
     * Delete a cached directory
     * @access public
     * @param string $directory
     * @return boolean
     */
    public function deleteDir($directory)
    {
        return $this->fileSystem->deleteDir($directory) && $this->invalidatePath($directory . '/*');
    }

    /**
     * Invalidate a file or folder if used in a CDN
     * @param string $location
     * @return boolean
     * */
    protected function invalidatePath($location)
    {

    }


    /**
     * Get an item from the cache
     * @access public
     * @param string|array $key
     * @return boolean
     */
    public function get($key)
    {
        if($result = $this->fileSystem->read($this->getCacheKey($key))) {
            if($this->isExpiredOrNotReleased($this->getCacheKey($key))) {
                $result = false;
            }
        }
        return  $result;
    }

    /**
     * Check if an item is cached
     * @access public
     * @param string|array $key
     * @param string $content
     * @param null|array $metadata
     * @return boolean
     */
    public function put($key, $content, $metadata = null)
    {
        $reference = $this->getCacheKey($key);
        if ($result = $this->fileSystem->put($reference, $content)) {
            $this->createMetadataFile($reference, $metadata);
        }
        return $result;
    }

    /**
     * Check if an item is cached
     * @access public
     * @param string $reference
     * @param string $content
     * @param null|array $metadata
     * @return boolean
     */
    public function putByReference($reference, $content, $metadata = null)
    {
        if ($result = $this->fileSystem->put($reference, $content)) {
            $this->createMetadataFile($reference, $metadata);
        }
        return $result;
    }

    /**
     * Get the metadata for a file
     * @access public
     * @param string $reference
     * @return null|string
     */
    private function getMetadata($reference)
    {
        $result = null;
        $reference = $this->createMetadataFileReference($reference);
        if($this->fileSystem->has($reference)) {
            $result = $this->fileSystem->read($reference);
        }
        return $result;
    }

    /**
     * Check if a file in the cache has expired
     * @access private
     * @param string $reference
     * @return boolean
     * @todo - update the $this->getState()->getAccess()->hasAccess("cms.media", "view") to be a callback/event
     * */
    private function isExpiredOrNotReleased($reference) {
        $result = false;
        if($metadata = $this->getMetadata($reference)) {
            $data = json_decode($metadata, true);
            if( (isset($data['expires']) && $data['expires'] < Time::getUtcNow()) || (isset($data['release']) && $data['release'] > Time::getUtcNow()) )  {
                if(!$this->getState()->getUser()->is_admin && !$this->getState()->getAccess()->hasAccess("cms.media", "view")) {
                    $result = true;
                }
            }
        }
        return $result;
    }

    /**
     * Create a metadata file if an item is cached
     * @access public
     * @param string $reference
     * @param null|array $metadata
     * @return void
     */
    private function createMetadataFile($reference, $metadata = null)
    {
        if ($metadata && is_array($metadata)) {
            $fileContents = json_encode($metadata);
            $this->fileSystem->put($this->createMetadataFileReference($reference), $fileContents);
        }
    }

    /**
     * Create a metadata file if an item is cached
     * @access public
     * @param string $reference
     * @return string
     */
    private function createMetadataFileReference($reference)
    {
        return $reference . '._metadata.txt';
    }

    /**
     * Check if an item is cached
     * @access public
     * @param string|array $key
     * @return boolean
     */
    public function delete($key)
    {
        $result = false;
        $cacheKey = $this->getCacheKey($key);
        if ($this->fileSystem->has($cacheKey)) {
            $result = $this->fileSystem->delete($cacheKey) && $this->invalidatePath(dirname($cacheKey . '/*'));
        }
        return $result;
    }

    /**
     * Check if an item is cached
     * @access public
     * @param string|array $key
     * @return boolean
     */
    public function has($key)
    {
        return $this->hasByReference($this->getCacheKey($key));
    }

    /**
     * Check if a file is cached by a reference
     * @access private
     * @param string $reference
     * @return boolean
     * */
    private function hasByReference($reference)
    {
        $result = false;
        if ($this->fileSystem->has($reference)) {
            if ($this->fileSystem->getTimestamp($reference) > (time() - self::MAX_CACHE_SECONDS) && !$this->isExpiredOrNotReleased($reference)) {
                $result = true;
            } else {
                $this->fileSystem->delete($reference);
            }
        }
        return $result;
    }

    /**
     * Get the cache key
     * @param string|array $key
     * @return string
     * */
    public function getCacheKey($key)
    {
        $pathItems = [];
        if (is_array($key)) {
            foreach ($key as $v) {
                $parts = explode("/", $v);
                foreach ($parts as $p) {
                    $pathItems[] = Strings::generatePathId($p);
                }
            }
            $result = implode('/', $pathItems);
        } else {
            $parts = explode("/", $key);
            foreach ($parts as $p) {
                $pathItems[] = Strings::generatePathId($p);
            }
            $result = implode('/', $pathItems);
        }
        return $result;
    }

    public function getKeyArray($reference, $width = 0, $height = 0, $size = 0, $timestamp = 0, $mode = false)
    {
        $result = [dirname($reference)];
        if ($timestamp) {
            $result[] = '_' . $timestamp;
        }
        if ($size > 0) {
            $result[] = $size;
        } elseif ($height || $width) {
            $result[] = $width ? $width : '0';
            $result[] = $height ? $height : '0';
        }
        if ($mode) {
            $result[] = $mode . '_' . basename($reference);
        } else {
            $result[] = basename($reference);
        }
        return $result;
    }

}
