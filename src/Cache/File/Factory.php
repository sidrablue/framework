<?php
namespace BlueSky\Framework\Cache\File;

/**
 * Base controller for Core Controllers
 * @package BlueSky\Framework\Controller\Core
 * */
class Factory
{

    /**
     * Create an instance of field data
     * @param $state \BlueSky\Framework\State\Base
     * @param string $reference
     * @return \League\Flysystem\Filesystem
     * */
    public static function createInstance($state, $reference = "")
    {
        if(!$reference) {
            $reference = $state->getSettings()->getConfigOrSetting("media.cache.adapter", 'local');
        }
        $referenceClass = __NAMESPACE__ . '\Adapter\\'.ucfirst($reference);
        if (class_exists($referenceClass)) {
            $className = $referenceClass;
        }
        else {
            $className = __NAMESPACE__ . '\Adapter\Local';
        }
        /**
         * @var \BlueSky\Framework\Cache\File\Base $adapterClass
         * */
        return new $className($state);
    }

}
