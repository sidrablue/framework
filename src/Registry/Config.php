<?php
namespace BlueSky\Framework\Registry;

use Pixel418\Iniliq\IniParser;

class Config
{

    /**
     * The account reference to use for loading of INI details
     * @param string $accountRef
     * */
    private $accountRef = false;

    /**
     * The config variables
     * @param array $config
     * */
    private $config = false;

    /**
     * Default constructor with the ability to specify an account reference
     * @param bool|string $accountRef
     */
    public function __construct($accountRef = false)
    {
       $this->accountRef = $accountRef;
    }

    /**
     * Get a config value
     * @param string $key
     * @param mixed $default
     * @return mixed
     */
    public function get($key = null, $default = null)
    {
        if(!$this->config){
            $this->load();
        }
        if($key) {
            if(isset($this->config[$key])) {
                $result = $this->config[$key];
            }
            elseif($default) {
                $result = $default;
            }
            else {
                $result = null;
            }
        }
        else {
            $result = $this->config;
        }
        return $result;
    }

    private function getAccountFolder()
    {
        return defined('LOTE_TEST_MODE')?"account_test":"account";
    }

    public function isServerUrl($url): bool
    {
        $result = $host = false;
        $urlData = parse_url($url);
        if (isset($urlData['host'])) {
            $host = $urlData['host'];
        }
        if ($host && is_array($this->get("server.urls", false))) {
            foreach ($this->get("server.urls") as $serverUrl) {
                //@todo - this can be improved
                $baseHost = "*.".substr($host, strpos($host, '.') + 1);
                if ($baseHost == $serverUrl) {
                    $result = true;
                    break;
                }
            }
        }
        return $result;
    }

    private function load()
    {
        $p = new IniParser();
        $accountFolder = $this->getAccountFolder();
        $this->config = $p->parse(array(LOTE_CONF_PATH . "base.ini"));
        if(isset($this->config['lote.account'])) {
            if($this->accountRef) {
                $this->config = $p->parse(array(
                    LOTE_CONF_PATH . ''.$accountFolder.'/' . $this->accountRef . '.ini',
                    LOTE_CONF_PATH . "base.ini"
                ));
            }
            elseif(defined('LOTE_ACCOUNT_REF')) {
                if(file_exists(LOTE_CONF_PATH . ''.$accountFolder.'/' . LOTE_ACCOUNT_REF . '.ini')) {
                    $this->config = $p->parse(array(
                        LOTE_CONF_PATH . ''.$accountFolder.'/' . LOTE_ACCOUNT_REF . '.ini',
                        LOTE_CONF_PATH . "base.ini"
                    ));
                }
                elseif(file_exists(LOTE_CONF_PATH . '' . LOTE_ACCOUNT_REF . '.ini')) {
                    $this->config = $p->parse(array(
                        LOTE_CONF_PATH . '' . LOTE_CONF_PATH . '.ini',
                        LOTE_CONF_PATH . "base.ini"
                    ));
                }
            }
            elseif(isset($this->config['lote.account'])) {
                $this->config = $p->parse(array(
                    LOTE_CONF_PATH . ''.$accountFolder.'/' . $this->config['lote.account'] . '.ini',
                    LOTE_CONF_PATH . "base.ini"
                ));
            }
            else {
                $this->config = $p->parse([LOTE_CONF_PATH . "base.ini"]);
            }
        }
    }

}
