<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 */
declare(strict_types=1);

namespace BlueSky\Framework\Query;

/**
 * Schema service
 * @package Lote\App\CustomFields\Service
 */
class Paging
{

    /** @var int $total - the total number of results */
    public $total = 0;

    /** @var int $pageCurrent - the total number of results */
    public $pageCurrent = 0;

    /** @var int $pageTotal - the total number of results */
    public $pageTotal = 0;

    /** @var int $resultFrom - the total number of results */
    public $resultFrom = 0;

    /** @var int $resultTo - the total number of results */
    public $resultTo = 0;

    /** @var int $pageSize - the total number of results */
    public $pageSize = 0;

    /**
     * Setup the paging data based off of the three required variables
     *
     * @param int|string $offset
     * @param int|string $pageSize
     * @param int|string|bool $total
     * @return void
     */
    public function setupPaging($offset, $pageSize, $total = false): void
    {
        if($total) {
            $this->total = intval($total);
        }
        $this->pageSize = intval($pageSize);
        $this->pageCurrent = intval($offset == 0 ? 1 : floor($offset / $pageSize) + 1);
        $this->pageTotal = intval($this->total == 0 ? 1 : ceil($this->total / $pageSize));
        $this->resultFrom = intval($this->total == 0 ? $offset : $offset + 1);
        $this->resultTo = min($offset + $pageSize, $this->total);
    }

    /**
     * Convert the current Paging object into an array
     *
     * @access public
     * @return array
     * */
    public function toArray(): array
    {
        $result = [];
        $result['total'] = $this->total;
        $result['pageCurrent'] = $this->pageCurrent;
        $result['pageTotal'] = $this->pageTotal;
        $result['pageTotal'] = $this->pageTotal;
        $result['resultTo'] = $this->resultTo;
        $result['pageSize'] = $this->total;
        return $result;
    }

}
