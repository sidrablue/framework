<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Query;

use Doctrine\DBAL\Query\QueryBuilder;
use BlueSky\Framework\Entity\Schema\Entity;
use BlueSky\Framework\Entity\Schema\Relation as RelationEntity;
use BlueSky\Framework\Entity\User\User;
use BlueSky\Framework\Object\State as StateBase;
use \BlueSky\Framework\Object\Entity\VirtualTypes\Factory as VirtualDataFactory;
use BlueSky\Framework\Query\DataTypes\BaseType;
use BlueSky\Framework\Query\DataTypes\Factory;
use BlueSky\Framework\Util\Strings;

/**
 * Schema service
 * @package Lote\App\CustomFields\Service
 */
class DynamicQuery extends StateBase
{
    
    /**
     * @var string[] $joinedTables - tables joined in a one_to_one relation
     */
    private $joinedTables = [];

    private $tableNameAliases = [];

    public function getTableNameAlias($table): string
    {
        return $this->tableNameAliases[$table] ?? '';
    }

    public function generateQueryBuilder(array $queryData)
    {
        try {
            $clauses = $queryData['clauses'];
            $message = $this->buildConstraintQueryRecursive($clauses);
            $q = $this->buildDynamicQuery($message, $queryData['clauses']['root_reference']);
            $this->tableNameAliases = $message->getTableNameAliases();
            if (!empty($queryData['sort_options'])) {
                $q = $this->addQuerySortOrder($q, $queryData['sort_options'], $this->tableNameAliases);
            }
            return $q;
        } catch (\Exception $e) {
            $this->getState()->getLoggers()->getMasterLogger('dynamic-query')->error($e->getMessage(), $queryData);
            throw $e;
        }
    }

    /**
     * Creates the query builder object based on the constraints returned from @see buildConstraintQueryRecursive
     * This function will also try and link any required tables together. Eg it will be able to link User <-> Group
     *      if there is a constraint on the group as well. All links must be defined in @see RelationEntity
     * Due to graph theory logic, we only look at direct relations eg. One-One, One-Many, Many-Many. Indirect relations will not be traversed
     *      This means that User <-> Group and User <-> UserSubscription will both have direct relations
     *      However, User <-> Newsletter will not have a direct relation  due to its relation structure of User <-> Group <-> Newsletter
     *      and therefore will not be picked up
     *
     * @param DynamicQueryMessage $constraintsMessage
     * @param string|null $rootReference
     * @return QueryBuilder
     */
    private function buildDynamicQuery(DynamicQueryMessage $constraintsMessage, string $rootReference): QueryBuilder
    {
        $relationReferences = [$rootReference];

        $rootEntity = $this->getState()->getSchema()->getEntityByReference($rootReference);
        $q = $this->getState()->getReadDb()->createQueryBuilder();
        $rootTableName = ($rootEntity->class_name)::TABLE_NAME;
        if (empty($constraintsMessage->getTableNameAliases()[$rootTableName])) {
            $rootTableNameAlias = uniqid('bd_');
            $constraintsMessage->addTableNameAlias($rootTableName, $rootTableNameAlias);
        } else {
            $rootTableNameAlias = $constraintsMessage->getTableNameAliases()[$rootTableName];
        }
        $q->select("distinct {$rootTableNameAlias}.*")
            ->from($rootTableName, $rootTableNameAlias)
            ->andWhere("{$rootTableNameAlias}.lote_deleted is null");
        foreach ($constraintsMessage->getRelationReferences() as $reference) {
            $reference = str_replace('___', '\\', $reference);
            if (!in_array($reference, $relationReferences)) {
                if ($e = $this->getState()->getSchema()->getEntityByReference($reference)) {
                    if ($relation = $this->findDirectRelation($e, $rootEntity)) {
                        $relationTableName = ($e->class_name)::TABLE_NAME;
                        $relationTableNameAlias = $constraintsMessage->getTableNameAliases()[$relationTableName] ?? uniqid('bd_');
                        if ($relation->type === RelationEntity::MANY_TO_MANY) {
                            if ($mtmSchema = $this->getState()->getSchema()->getEntityByReference($relation->mtm_connection_entity_reference)) {
                                $joiningTableName = ($mtmSchema->class_name)::TABLE_NAME;
                                $joiningTableNameAlias = $constraintsMessage->getTableNameAliases()[$joiningTableName] ?? uniqid('bd_');
                                $q->leftJoin($rootTableNameAlias, $joiningTableName, $joiningTableNameAlias,
                                    "{$joiningTableNameAlias}.{$relation->mtm_foreign_connection_field_reference} = {$rootTableNameAlias}.{$relation->to_field_reference} and {$joiningTableNameAlias}.lote_deleted is null")
                                    ->leftJoin($joiningTableNameAlias, $relationTableName, $relationTableNameAlias,
                                        "{$joiningTableNameAlias}.{$relation->mtm_self_connection_field_reference} = {$relationTableNameAlias}.{$relation->from_field_reference} and {$relationTableNameAlias}.lote_deleted is null");                            }
                        } else {
                            if (!in_array($relationTableName, $this->joinedTables)) {
                                $this->joinedTables[] = $relationTableName; //Add table names to avoid repeated joins
                                $q->leftJoin($rootTableNameAlias, $relationTableName, $relationTableNameAlias,
                                    "{$rootTableNameAlias}.{$relation->to_field_reference} = {$relationTableNameAlias}.{$relation->from_field_reference} and {$relationTableNameAlias}.lote_deleted is null");
                            }
                        }
                    }
                }
            }
        }
        $q->andWhere($constraintsMessage->getSql())
            ->setParameters($constraintsMessage->getParameters());
        return $q;
    }

    /**
     * This finds the relation from the linking entity. If the link exists then it is returned.
     * As a fail safe, if the linking entity does not contain the relation, then the root entity is searched
     * If the root entity has the relation required, then the relation is reversed so that the code sees it coming from
     * the linking entity. This makes the coding a lot easier since it wont require if blocks anymore
     *
     * @param Entity $linkingEntity
     * @param Entity $rootEntity
     * @return RelationEntity|null
     */
    private function findDirectRelation(Entity $linkingEntity, Entity $rootEntity): ?RelationEntity
    {
        $output = null;
        if ($entityRelations = $linkingEntity->getRelations()) {
            foreach ($entityRelations->getRelations() as $relation) {
                if ($relation->to_entity_reference === $rootEntity->reference) {
                    $output = $relation;
                    break;
                }
            }
        }

        if ($output === null) {
            if ($entityRelations = $rootEntity->getRelations()) {
                foreach ($entityRelations->getRelations() as $relation) {
                    if ($relation->to_entity_reference === $linkingEntity->reference) {
                        $output = $relation->reverse();
                        break;
                    }
                }
            }
        }

        return $output;
    }

    /**
     * This creates the constraints within the query.
     * Conditions are processed first, followed by the nested conditions
     * Conditions require an operator (AND | OR)
     * Nested Conditions should have the same structure as its parent condition
     *
     * @NOTE: This function is recursive
     *
     * @param array $clauses
     * @param string[] $tableNameAliases
     * @return DynamicQueryMessage
     * @throws \Exception
     */
    private function buildConstraintQueryRecursive(array $clauses, array $tableNameAliases = []): DynamicQueryMessage
    {
        $operator = $clauses['operator'] ?? null;
        $conditionsRaw = $clauses['conditions'] ?? [];
        $subConditionsRaw = $clauses['groups'] ?? [];
        $rootReferenceRaw = $clauses['root_reference'] ?? User::getObjectRef();

        if (empty($conditionsRaw) && empty($subConditionsRaw)) {
            $operator = 'AND';
            $conditionsRaw = [
                [
                    'type' => 'date_time',
                    'reference' => $rootReferenceRaw,
                    'field' => 'lote_deleted',
                    'constraint_type' => 'is_null'
                ]
            ];
        }

        if ($operator !== 'OR' && $operator !== 'AND') {
            throw new \Exception('Invalid operator');
        }

        $relationReferences = [];

        $sql = '';
        $parameters = [];
        foreach ($conditionsRaw as $index => $condition) {
            $c = $this->buildQueryCondition($condition, $tableNameAliases);
            $relationReferences[] = $condition['reference'];
            if ($c !== null) {

                foreach ($c->getOutput()->getParameters() as $key => $value) {
                    $parameters[$key] = $value;
                }

                $tableNameAliases[$c->getOutput()->getTableName()] = $c->getOutput()->getTableNameAlias();

                if ($index !== 0) { $sql .= " $operator "; }
                $sql .= $c->getOutput()->getSql();
            }
        }

        if (!empty($sql)) { $sql = "({$sql})"; }

        foreach ($subConditionsRaw as $subCondition) {
            $message = $this->buildConstraintQueryRecursive($subCondition, $tableNameAliases);
            if (!empty($sql)) { $sql .= " $operator "; }
            $sql .= $message->getSql();
            $parameters = array_merge($parameters, $message->getParameters());
            $relationReferences = array_merge($relationReferences, $message->getRelationReferences());
            $tableNameAliases = array_merge($tableNameAliases, $message->getTableNameAliases());
        }

        return new DynamicQueryMessage($sql, $parameters, $relationReferences, $tableNameAliases);
    }

    /**
     * This creates an instance of an SQL generator that processes the constraint condition and outputs an sql where fragment
     *
     * @param array $condition
     * @param string[] $tableNameAliases
     * @return BaseType|null
     */
    private function buildQueryCondition(array $condition, array $tableNameAliases): ?BaseType {
        $reference = $condition['reference'];
        $fieldReference = $condition['field'];

        $reference = str_replace('___', '\\', $reference);

        $type = null;
        $e = $this->getState()->getSchema()->getEntityByReference($reference);
        if ($e !== null) {
            $f = $e->getEntityField($fieldReference);
            if ($f !== null) {
                $type = $f->field_type;
            }
        }

        $constraintType = $condition['constraint_type'];
        $constraint = $condition['constraint_value'] ?? null;

        if (is_string($constraint) && Strings::isJson($constraint)) {
            $constraint = json_decode($constraint, true);
        }

        $conditionType = null;
        if ($entity = $this->getState()->getSchema()->getEntityByReference($reference)) {
            if ($field = $entity->getEntityField($fieldReference)) {
                $conditionType = Factory::getInstance($type, $field, $constraintType, $constraint, $tableNameAliases);
            }
        }

        return $conditionType;
    }

    private function addQuerySortOrder(QueryBuilder $q, array $sortOptions, array $tableNameAliases = []): QueryBuilder
    {
        if (!empty($sortOptions)) {
            foreach ($sortOptions as $sortOption) {
                $sortOrder = $sortOption['order'];
                if (!empty($sortOrder)) {
                    $sortOrder = strtolower($sortOrder);
                    if ($sortOrder === 'asc' || $sortOrder === 'desc') {
                        $reference = $sortOption['reference'];
                        if ($e = $this->getState()->getSchema()->getEntityByReference($reference)) {
                            $fieldReference = $sortOption['field'];
                            if ($f = $e->getEntityField($fieldReference)) {
                                /// fixme change the sort options to handle number fields better + multi select fields
                                $class = $e->class_name;
                                if ($e->is_virtual) {
                                    $class = $this->getState()->getEntityManager()->createInstance($e);
                                }
                                $tableName = $class::getTableName();
                                $tableNameAlias = $tableNameAliases[$tableName] ?? $tableName;

                                if ($sortOrder === 'asc') {
                                    $q->addOrderBy("isnull({$tableNameAlias}.{$f->reference})", 'asc');
                                }
                                $q->addOrderBy("{$tableNameAlias}.{$f->reference}", $sortOrder);
                            }
                        }
                    }
                }
            }
        }

        return $q;
    }
}