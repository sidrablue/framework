<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 */

declare(strict_types=1);

namespace BlueSky\Framework\Query;


final class DynamicQueryMessage
{
    private $sql = '';
    private $parameters = [];
    private $relationReferences = [];
    private $tableNameAliases = [];

    /**
     * DynamicQueryMessage constructor.
     * @param string $sql
     * @param array $parameters
     * @param string[] $relationReferences
     * @param string[] $tableNameAliases
     */
    public function __construct(string $sql, array $parameters, array $relationReferences = [], array $tableNameAliases = [])
    {
        $this->sql = $sql;
        $this->parameters = $parameters;
        $this->relationReferences = $relationReferences;
        $this->tableNameAliases = $tableNameAliases;
    }

    /** @return string */
    public function getSql(): string { return $this->sql; }

    /** @return array */
    public function getParameters(): array { return $this->parameters; }

    /** @return array */
    public function getRelationReferences(): array { return $this->relationReferences; }

    /** @return array */
    public function getTableNameAliases(): array { return $this->tableNameAliases; }

    public function addTableNameAlias(string $tableName, string $alias): void { $this->tableNameAliases[$tableName] = $alias; }
}