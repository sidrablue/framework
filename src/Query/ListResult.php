<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 */
declare(strict_types=1);

namespace BlueSky\Framework\Query;

use ArrayAccess;

/**
 * Schema service
 * @package Lote\App\CustomFields\Service
 */
class ListResult implements ArrayAccess
{

    use ArrayAccessTrait;

    /** @var Paging $paging - the paging data */
    public $paging;

    /** @var array $rows - the array of row results */
    public $rows = [];

    /**
     * Base constructor, which instantiates the paging class
     *
     * @access public
     * @return void
     * */
    public function __construct()
    {
        $this->paging = new Paging();
    }

    /**
     * Setup the paging for this query result set
     *
     * @access public
     * @param int|string $offset
     * @param int|string $pageSize
     * @param bool|int|string $total
     * @return void
     */
    public function setupPaging($offset, $pageSize, $total = false)
    {
        if($this->paging->total > 0 || $total) {
            $this->paging->setupPaging($offset, $pageSize, $total);
        }
    }

    /**
     * Convert the current ListResult object into an array
     *
     * @access public
     * @return array
     * */
    public function toArray(): array
    {
        $result = [];
        $result['paging'] = $this->paging->toArray();
        $result['rows'] = $this->rows;
        return $result;
    }

}
