<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 */

declare(strict_types=1);

namespace BlueSky\Framework\Query\DS;


class DynamicQueryConditionDS
{

    /** @var string $dataType */
    private $dataType;

    /** @var string $entityReference */
    private $entityReference;

    /** @var string $fieldReference */
    private $fieldReference;

    /** @var string $constraintType */
    private $constraintType;

    /** @var string|array|null - the value can either be empty, a string or a json (value_lower, value_upper) */
    private $constraintValue;

    public function __construct(string $dataType, string $entityReference, string $fieldReference, string $constraintType, $constraintValue = null)
    {
        $this->dataType = $dataType;
        $this->entityReference = $entityReference;
        $this->fieldReference = $fieldReference;
        $this->constraintType = $constraintType;
        $this->constraintValue = $constraintValue;
    }

    public function toArray(): array
    {
        return [
            'constraint_type' => $this->constraintType,
            'constraint_value' => $this->constraintValue,
            'field' => $this->fieldReference,
            'reference' => $this->entityReference,
            'type' => $this->dataType,
        ];
    }

    public static function fromArray(array $input): DynamicQueryConditionDS
    {
        return new DynamicQueryConditionDS(
            $input['type'],
            $input['reference'],
            $input['field'],
            $input['constraint_type'],
            $input['constraint_value']
        );
    }

    /** @return string */
    public function getDataType(): string { return $this->dataType; }

    /** @return string */
    public function getEntityReference(): string { return $this->entityReference; }

    /** @return string */
    public function getFieldReference(): string { return $this->fieldReference; }

    /** @return string */
    public function getConstraintType(): string { return $this->constraintType; }

    /** @return array|string|null */
    public function getConstraintValue() { return $this->constraintValue; }

}