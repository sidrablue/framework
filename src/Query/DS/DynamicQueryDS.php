<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 */

declare(strict_types=1);

namespace BlueSky\Framework\Query\DS;


class DynamicQueryDS
{

    /** @var string $operator - AND/OR (case sensitive) */
    private $operator;

    /** @var DynamicQueryConditionDS[] $conditions - the base conditions  */
    private $conditions = [];

    /** @var string $rootEntityReference - The starting graph node entity reference  */
    private $rootEntityReference;

    /** @var DynamicQueryDS[] $groupQueries - These are sub queries. Eg condition A OR B OR group C (C is D AND E) */
    private $groupQueries = [];

    public function __construct(string $operator, string $rootEntityReference, array $condition = [], array $groupQueries = [])
    {
        $this->operator = $operator;
        $this->rootEntityReference = $rootEntityReference;
        $this->conditions = $condition;
        $this->groupQueries = $groupQueries;
    }

    public function addCondition(DynamicQueryConditionDS $condition): void { $this->conditions[] = $condition; }

    public function addGroup(DynamicQueryDS $group): void { $this->groupQueries[] = $group; }

    public function toArray(): array
    {
        $output = [
            'root_reference' => $this->rootEntityReference,
            'operator' => $this->operator,
            'conditions' => [],
            'groups' => [],
        ];

        foreach ($this->conditions as $condition) { $output['conditions'][] = $condition->toArray(); }
        foreach ($this->groupQueries as $groupQuery) { $output['groups'][] = $groupQuery->toArray(); }

        return $output;
    }

    public static function fromArray(array $input): DynamicQueryDS
    {
        $output = new DynamicQueryDS(
            $input['operator'],
            $input['root_reference']
        );
        if (!empty($input['conditions'])) {
            foreach ($input['conditions'] as $conditionData) {
                $output->addCondition(DynamicQueryConditionDS::fromArray($conditionData));
            }
        }
        if (!empty($input['groups'])) {
            foreach ($input['groups'] as $conditionData) {
                $output->addGroup(DynamicQueryDS::fromArray($conditionData));
            }
        }
        return $output;
    }

    /** @return string */
    public function getOperator(): string { return $this->operator; }

    /** @return DynamicQueryConditionDS[]  */
    public function getConditions(): array { return $this->conditions; }

    /** @return string */
    public function getRootEntityReference(): string { return $this->rootEntityReference; }

    /** @return DynamicQueryDS[] */
    public function getGroupQueries(): array { return $this->groupQueries; }

}