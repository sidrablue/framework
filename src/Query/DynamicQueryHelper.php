<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 */

declare(strict_types=1);

namespace BlueSky\Framework\Query;


use BlueSky\Framework\Object\State;
use BlueSky\Framework\Query\DS\DynamicQueryConditionDS;
use BlueSky\Framework\Query\DS\DynamicQueryDS;
use BlueSky\Framework\Entity\Schema\Field as SchemaField;

class DynamicQueryHelper
{

    private function __construct() { }

    private function __clone() { }

    private const FIELD_TYPE_TO_QUERY_TYPE_MAPPING = [
        'primary_key_int' => 'integer',

        'datetime' => 'date_time',
        'datetime_immutable' => 'date_time',
        'date' => 'date',
        'date_immutable' => 'date_time',
        'time' => 'date_time',

        'enum' => 'string',
        'string' => 'string',
        'text' => 'string',
        'longtext' => 'string',
        'email' => 'string',
        'file' => 'string',
        'url' => 'string',
        'password' => 'string',

        'boolean' => 'boolean',

        'integer' => 'integer',
        'enum_integer' => 'enum',

        'json' => 'enum',
        'single_select' => 'single_select',
        'multi_select' => 'multi_select',
        'checkbox' => 'multi_select',
        'radiobutton' => 'single_select',
        'matrix' => 'multi_select',
        'country' => 'enum',
        'public_group' => 'enum',

        'decimal' => 'integer',

        'fkey' => 'integer',

        'textarea' => 'string',
        'heading' => 'string',
        'header' => 'string',
        'message' => 'string',
        'radio_button' => 'json',
        'recaptcha' => 'string',
        'telephone' => 'string',
        'image' => 'string',
        'footing' => 'string',
        'footer' => 'string',
    ];

    public static function getAsNestedQuery(DynamicQueryDS $input, ?string $operator = null, ?string $rootEntityReference = null): DynamicQueryDS
    {
        return new DynamicQueryDS(
            $operator ?? $input->getOperator(),
            $rootEntityReference ?? $input->getRootEntityReference(),
            [],
            [$input]
        );
    }

    public static function generateQueryCondition(SchemaField $field, string $constraintType, $constraintValue = null): DynamicQueryConditionDS
    {
        $entityReference = $field->getParentEntity()->reference;
        $dataType = self::FIELD_TYPE_TO_QUERY_TYPE_MAPPING[$field->field_type] ?? 'string';
        return new DynamicQueryConditionDS(
            $dataType,
            $entityReference,
            $field->reference,
            $constraintType,
            $constraintValue
        );
    }

}