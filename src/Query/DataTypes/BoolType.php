<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 */

declare(strict_types=1);
namespace BlueSky\Framework\Query\DataTypes;

use BlueSky\Framework\Entity\Schema\Entity as SchemaEntity;
use BlueSky\Framework\Entity\Schema\Field as SchemaField;

class BoolType extends BaseType
{

    const CONSTRAINT_TYPE_EQUALS = 'equals';

    protected function processCondition(string $tableName, string $tableNameAlias, SchemaEntity $entity, SchemaField $field, string $constraintType, $constraint): BaseTypeReturn
    {
        $output = null;
        switch ($constraintType) {
            case self::CONSTRAINT_TYPE_EQUALS:
                $output = $this->constraintEquals($tableName, $tableNameAlias, $field->reference, $constraint);
                break;
        }
        if ($output === null) {
            $output = parent::processCondition($tableName, $tableNameAlias, $entity, $field, $constraintType, $constraint);
        }
        return $output;
    }

    private function constraintEquals(string $tableName, string $tableNameAlias, string $fieldReference, $constraint): BaseTypeReturn
    {
        $isTrue = filter_var($constraint, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
        if ($isTrue === null) {
            throw new \Exception('Constraint must be of type boolean');
        }
        $isTrue = $isTrue ? 1 : 0;
        $sql = "{$tableNameAlias}.{$fieldReference} = {$isTrue}";
        return new BaseTypeReturn($sql, [], $tableName, $tableNameAlias);
    }

}