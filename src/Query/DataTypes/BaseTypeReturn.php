<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 */

declare(strict_types=1);
namespace BlueSky\Framework\Query\DataTypes;

final class BaseTypeReturn
{
    private $sql = '';
    private $parameters = [];
    private $tableName = '';
    private $tableNameAlias = '';

    /**
     * BaseTypeReturn constructor.
     * @param string $sql
     * @param array $parameters
     * @param string $tableName
     * @param string $tableNameAlias
     */
    public function __construct(string $sql, array $parameters, string $tableName, string $tableNameAlias)
    {
        $this->sql = $sql;
        $this->parameters = $parameters;
        $this->tableName = $tableName;
        $this->tableNameAlias = $tableNameAlias;
    }

    /** @return string */
    public function getSql(): string { return $this->sql; }

    /** @return array */
    public function getParameters(): array { return $this->parameters; }

    /** @return string */
    public function getTableName(): string { return $this->tableName; }

    /** @return string */
    public function getTableNameAlias(): string { return $this->tableNameAlias; }

}