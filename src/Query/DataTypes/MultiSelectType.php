<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Query\DataTypes;

use BlueSky\Framework\Entity\Schema\Entity as SchemaEntity;
use BlueSky\Framework\Entity\Schema\Field as SchemaField;

class MultiSelectType extends BaseType
{
    const CONSTRAINT_TYPE_BEGINS_WITH = 'begins_with';
    const CONSTRAINT_TYPE_ENDS_WITH = 'ends_with';
    const CONSTRAINT_TYPE_IN = 'in';
    const CONSTRAINT_TYPE_NOT_IN = 'not_in';
    const CONSTRAINT_TYPE_EQUALS = 'equals';
    const CONSTRAINT_TYPE_NOT_EQUALS = 'not_equals';

    protected function processCondition(string $tableName, string $tableNameAlias, SchemaEntity $entity, SchemaField $field, string $constraintType, $constraint): BaseTypeReturn
    {
        $output = null;
        switch ($constraintType) {
            case self::CONSTRAINT_TYPE_BEGINS_WITH:
                $output = $this->constraintBeginsWith($tableName, $tableNameAlias, $field, $constraint);
                break;
            case self::CONSTRAINT_TYPE_ENDS_WITH:
                $output = $this->constraintEndsWith($tableName, $tableNameAlias, $field, $constraint);
                break;
            case self::CONSTRAINT_TYPE_IN:
                $output = $this->constraintIn($tableName, $tableNameAlias, $field, $constraint);
                break;
            case self::CONSTRAINT_TYPE_NOT_IN:
                $output = $this->constraintNotIn($tableName, $tableNameAlias, $field, $constraint);
                break;
            case self::CONSTRAINT_TYPE_EQUALS:
                $output = $this->constraintEquals($tableName, $tableNameAlias, $field, $constraint);
                break;
            case self::CONSTRAINT_TYPE_NOT_EQUALS:
                $output = $this->constraintNotEquals($tableName, $tableNameAlias, $field, $constraint);
                break;
        }
        if ($output === null) {
            $output = parent::processCondition($tableName, $tableNameAlias, $entity, $field, $constraintType, $constraint);
        }
        return $output;
    }

    private function findKey(SchemaField $field, string $constraint): ?string
    {
        $output = null;
        foreach ($field->getFieldOptions()->getRows() as $row) {
            foreach ($row->getColumns() as $column) {
                $data = $column->getData();
                if (strpos($data->getId(), $constraint) === 0) { /// FIXME: the getText() check needs to be removed. its added in for now until migrations are complete
                    $output = $column->getData()->getId();
                    break;
                }
            }
            if ($output !== null) {
                break;
            }
        }
        return $output;
    }

    private function constraintBeginsWith(string $tableName, string $tableNameAlias, SchemaField $field, $constraint): BaseTypeReturn
    {
        $sql = "json_search({$tableNameAlias}.{$field->reference}, 'one', '{$constraint}%', null, '$.selections') is not null";
        return new BaseTypeReturn($sql, [], $tableName, $tableNameAlias);
    }

    private function constraintEndsWith(string $tableName, string $tableNameAlias, SchemaField $field, $constraint): BaseTypeReturn
    {
        $sql = "json_search({$tableNameAlias}.{$field->reference}, 'one', '%{$constraint}', null, '$.selections') is not null";
        return new BaseTypeReturn($sql, [], $tableName, $tableNameAlias);
    }

    private function constraintIn(string $tableName, string $tableNameAlias, SchemaField $field, array $constraint): BaseTypeReturn
    {
        $sqls = [];
        foreach ($constraint as $item) {
            $key = $this->findKey($field, $item);
            $sqls[] = "json_exists({$tableNameAlias}.{$field->reference}, '$.selections.{$key}')";
        }
        $sql = implode(' or ', $sqls);
        return new BaseTypeReturn($sql, [], $tableName, $tableNameAlias);
    }

    private function constraintNotIn(string $tableName, string $tableNameAlias, SchemaField $field, array $constraint): BaseTypeReturn
    {
        $sqls = [];
        foreach ($constraint as $item) {
            $key = $this->findKey($field, $item);
            $sqls[] = "not json_exists({$tableNameAlias}.{$field->reference}, '$.selections.{$key}')";
        }
        $sql = implode(' and ', $sqls);
        return new BaseTypeReturn($sql, [], $tableName, $tableNameAlias);
    }

    private function constraintEquals(string $tableName, string $tableNameAlias, SchemaField $field, string $constraint): BaseTypeReturn
    {
        return $this->constraintIn($tableName, $tableNameAlias, $field, [$constraint]);
    }

    private function constraintNotEquals(string $tableName, string $tableNameAlias, SchemaField $field, string $constraint): BaseTypeReturn
    {
        return $this->constraintNotIn($tableName, $tableNameAlias, $field, [$constraint]);
    }

}