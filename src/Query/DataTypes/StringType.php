<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 */

declare(strict_types=1);
namespace BlueSky\Framework\Query\DataTypes;

use BlueSky\Framework\Entity\Schema\Entity as SchemaEntity;
use BlueSky\Framework\Entity\Schema\Field as SchemaField;

class StringType extends BaseType
{

    const CONSTRAINT_TYPE_BEGINS_WITH = 'begins_with';
    const CONSTRAINT_TYPE_ENDS_WITH = 'ends_with';
    const CONSTRAINT_TYPE_LIKE = 'like';
    const CONSTRAINT_TYPE_NOT_LIKE = 'not_like';
    const CONSTRAINT_TYPE_IN = 'in';
    const CONSTRAINT_TYPE_NOT_IN = 'not_in';
    const CONSTRAINT_TYPE_EQUALS = 'equals';
    const CONSTRAINT_TYPE_NOT_EQUALS = 'not_equals';

    const MAPPING = [
        'equals_string' => 'equals',
        'equals_option' => 'equals',
        'not_equals_string' => 'not_equals',
        'not_equals_option' => 'not_equals',
    ];

    protected function processCondition(string $tableName, string $tableNameAlias, SchemaEntity $entity, SchemaField $field, string $constraintType, $constraint): BaseTypeReturn
    {
        $output = null;
        $constraintType = self::MAPPING[$constraintType] ?? $constraintType;
        switch ($constraintType) {
            case self::CONSTRAINT_TYPE_BEGINS_WITH:
                $output = $this->constraintBeginsWith($tableName, $tableNameAlias, $field, $constraint);
                break;
            case self::CONSTRAINT_TYPE_ENDS_WITH:
                $output = $this->constraintEndsWith($tableName, $tableNameAlias, $field, $constraint);
                break;
            case self::CONSTRAINT_TYPE_LIKE:
                $output = $this->constraintLike($tableName, $tableNameAlias, $field, $constraint);
                break;
            case self::CONSTRAINT_TYPE_NOT_LIKE:
                $output = $this->constraintNotLike($tableName, $tableNameAlias, $field, $constraint);
                break;
            case self::CONSTRAINT_TYPE_EQUALS:
                $output = $this->constraintEquals($tableName, $tableNameAlias, $field, $constraint);
                break;
            case self::CONSTRAINT_TYPE_NOT_EQUALS:
                $output = $this->constraintNotEquals($tableName, $tableNameAlias, $field, $constraint);
                break;
            case self::CONSTRAINT_TYPE_IN:
                $output = $this->constraintIn($tableName, $tableNameAlias, $field, $constraint);
                break;
            case self::CONSTRAINT_TYPE_NOT_IN:
                $output = $this->constraintNotIn($tableName, $tableNameAlias, $field, $constraint);
                break;
        }
        if ($output === null) {
            $output = parent::processCondition($tableName, $tableNameAlias, $entity, $field, $constraintType, $constraint);
        }
        return $output;
    }

    private function constraintBeginsWith(string $tableName, string $tableNameAlias, SchemaField $field, string $constraint): BaseTypeReturn
    {
        $uid = uniqid("bd_");
        $sql = "{$tableNameAlias}.{$field->reference} like :{$uid}";
        $parameters = [ $uid => "{$constraint}%" ];
        return new BaseTypeReturn($sql, $parameters, $tableName, $tableNameAlias);
    }

    private function constraintEndsWith(string $tableName, string $tableNameAlias, SchemaField $field, string $constraint): BaseTypeReturn
    {
        $uid = uniqid("bd_");
        $sql = "{$tableNameAlias}.{$field->reference} like :{$uid}";
        $parameters = [ $uid => "%{$constraint}" ];
        return new BaseTypeReturn($sql, $parameters, $tableName, $tableNameAlias);
    }

    private function constraintLike(string $tableName, string $tableNameAlias, SchemaField $field, string $constraint): BaseTypeReturn
    {
        $uid = uniqid("bd_");
        $sql = "{$tableNameAlias}.{$field->reference} like :{$uid}";
        $parameters = [ $uid => "%{$constraint}%" ];
        return new BaseTypeReturn($sql, $parameters, $tableName, $tableNameAlias);
    }

    private function constraintNotLike(string $tableName, string $tableNameAlias, SchemaField $field, string $constraint): BaseTypeReturn
    {
        $uid = uniqid("bd_");
        $sql = "{$tableNameAlias}.{$field->reference} not like :{$uid}";
        $parameters = [ $uid => "%{$constraint}%" ];
        return new BaseTypeReturn($sql, $parameters, $tableName, $tableNameAlias);
    }

    private function constraintEquals(string $tableName, string $tableNameAlias, SchemaField $field, string $constraint): BaseTypeReturn
    {
        return $this->constraintIn($tableName, $tableNameAlias, $field, [$constraint]);
    }

    private function constraintNotEquals(string $tableName, string $tableNameAlias, SchemaField $field, string $constraint): BaseTypeReturn
    {
        return $this->constraintNotIn($tableName, $tableNameAlias, $field, [$constraint]);
    }

    private function constraintIn(string $tableName, string $tableNameAlias, SchemaField $field, array $constraint): BaseTypeReturn
    {
        $sqls = [];
        $parameters = [];
        foreach ($constraint as $item) {
            $uid = uniqid('bd_');
            $sqls[] = "{$tableNameAlias}.{$field->reference} = :{$uid}";
            $parameters[$uid] = $item;
        }
        $sql = implode(' or ', $sqls);
        return new BaseTypeReturn($sql, $parameters, $tableName, $tableNameAlias);
    }

    private function constraintNotIn(string $tableName, string $tableNameAlias, SchemaField $field, array $constraint): BaseTypeReturn
    {
        $sqls = [];
        $parameters = [];
        foreach ($constraint as $item) {
            $uid = uniqid('bd_');
            $sqls[] = "{$tableNameAlias}.{$field->reference} != :{$uid}";
            $parameters[$uid] = $item;
        }
        $sql = implode(' and ', $sqls);
        return new BaseTypeReturn($sql, [], $tableName, $tableNameAlias);
    }

}