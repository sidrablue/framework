<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 */

declare(strict_types=1);
namespace BlueSky\Framework\Query\DataTypes;

use BlueSky\Framework\Entity\Schema\Entity as SchemaEntity;
use BlueSky\Framework\Entity\Schema\Field as SchemaField;

class DateType extends BaseType
{

    const CONSTRAINT_TYPE_EQUALS = 'equals';
    const CONSTRAINT_TYPE_NOT_EQUALS = 'not_equals';
    const CONSTRAINT_TYPE_GREATER_THAN = 'greater_than';
    const CONSTRAINT_TYPE_LESS_THAN = 'less_than';
    const CONSTRAINT_TYPE_BETWEEN = 'between';
    const CONSTRAINT_TYPE_NOT_BETWEEN = 'not_between';

    protected function processCondition(string $tableName, string $tableNameAlias, SchemaEntity $entity, SchemaField $field, string $constraintType, $constraint): BaseTypeReturn
    {
        $output = null;
        switch ($constraintType) {
            case self::CONSTRAINT_TYPE_EQUALS:
                $output = $this->constraintEquals($tableName, $tableNameAlias, $field->reference, $constraint);
                break;
            case self::CONSTRAINT_TYPE_NOT_EQUALS:
                $output = $this->constraintNotEquals($tableName, $tableNameAlias, $field->reference, $constraint);
                break;
            case self::CONSTRAINT_TYPE_GREATER_THAN:
                $output = $this->constraintGreaterThan($tableName, $tableNameAlias, $field->reference, $constraint);
                break;
            case self::CONSTRAINT_TYPE_LESS_THAN:
                $output = $this->constraintLessThan($tableName, $tableNameAlias, $field->reference, $constraint);
                break;
            case self::CONSTRAINT_TYPE_BETWEEN:
                $output = $this->constraintBetween($tableName, $tableNameAlias, $field->reference, $constraint);
                break;
            case self::CONSTRAINT_TYPE_NOT_BETWEEN:
                $output = $this->constraintNotBetween($tableName, $tableNameAlias, $field->reference, $constraint);
                break;
        }
        if ($output === null) {
            $output = parent::processCondition($tableName, $tableNameAlias, $entity, $field, $constraintType, $constraint);
        }
        return $output;
    }

    private function constraintEquals(string $tableName, string $tableNameAlias, string $fieldReference, string $constraint): BaseTypeReturn
    {
        $uid = uniqid("bd_");
        $sql = "{$tableNameAlias}.{$fieldReference} = :{$uid}";
        $parameters = [ $uid => $constraint ];
        return new BaseTypeReturn($sql, $parameters, $tableName, $tableNameAlias);
    }

    private function constraintNotEquals(string $tableName, string $tableNameAlias, string $fieldReference, string $constraint): BaseTypeReturn
    {
        $uid = uniqid("bd_");
        $sql = "{$tableNameAlias}.{$fieldReference} != :{$uid}";
        $parameters = [ $uid => $constraint ];
        return new BaseTypeReturn($sql, $parameters, $tableName, $tableNameAlias);
    }

    private function constraintGreaterThan(string $tableName, string $tableNameAlias, string $fieldReference, string $constraint): BaseTypeReturn
    {
        $uid = uniqid("bd_");
        $sql = "{$tableNameAlias}.{$fieldReference} > :{$uid}";
        $parameters = [ $uid => $constraint ];
        return new BaseTypeReturn($sql, $parameters, $tableName, $tableNameAlias);
    }

    private function constraintLessThan(string $tableName, string $tableNameAlias, string $fieldReference, string $constraint): BaseTypeReturn
    {
        $uid = uniqid("bd_");
        $sql = "{$tableNameAlias}.{$fieldReference} < :{$uid}";
        $parameters = [ $uid => $constraint ];
        return new BaseTypeReturn($sql, $parameters, $tableName, $tableNameAlias);
    }

    private function constraintBetween(string $tableName, string $tableNameAlias, string $fieldReference, array $constraint): BaseTypeReturn
    {
        $lower = $constraint['value_lower'];
        $upper = $constraint['value_upper'];
        $uid1 = uniqid("bd_");
        $uid2 = uniqid("bd_");
        $sql = "{$tableNameAlias}.{$fieldReference} >= :{$uid1} and {$tableNameAlias}.{$fieldReference} <= :{$uid2}";
        $parameters = [ $uid1 => $lower, $uid2 => $upper ];
        return new BaseTypeReturn($sql, $parameters, $tableName, $tableNameAlias);
    }

    private function constraintNotBetween(string $tableName, string $tableNameAlias, string $fieldReference, array  $constraint): BaseTypeReturn
    {
        $lower = $constraint['value_lower'];
        $upper = $constraint['value_upper'];
        $uid1 = uniqid("bd_");
        $uid2 = uniqid("bd_");
        $sql = "{$tableNameAlias}.{$fieldReference} < :{$uid1} and {$tableNameAlias}.{$fieldReference} > :{$uid2}";
        $parameters = [ $uid1 => $lower, $uid2 => $upper ];
        return new BaseTypeReturn($sql, $parameters, $tableName, $tableNameAlias);
    }

}