<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Query\DataTypes;

use BlueSky\Framework\Entity\Schema\Entity as SchemaEntity;
use BlueSky\Framework\Entity\Schema\Field as SchemaField;

class EnumType extends BaseType
{
    const CONSTRAINT_TYPE_IN = 'in';
    const CONSTRAINT_TYPE_NOT_IN = 'not_in';
    const CONSTRAINT_TYPE_EQUALS = 'equals';
    const CONSTRAINT_TYPE_NOT_EQUALS = 'not_equals';

    protected function processCondition(string $tableName, string $tableNameAlias, SchemaEntity $entity, SchemaField $field, string $constraintType, $constraint): BaseTypeReturn
    {
        $output = null;
        switch ($constraintType) {
            case self::CONSTRAINT_TYPE_IN:
                $output = $this->constraintIn($tableName, $tableNameAlias, $field->reference, $constraint);
                break;
            case self::CONSTRAINT_TYPE_NOT_IN:
                $output = $this->constraintNotIn($tableName, $tableNameAlias, $field->reference, $constraint);
                break;
            case self::CONSTRAINT_TYPE_EQUALS:
                $output = $this->constraintEquals($tableName, $tableNameAlias, $field->reference, $constraint);
                break;
            case self::CONSTRAINT_TYPE_NOT_EQUALS:
                $output = $this->constraintNotEquals($tableName, $tableNameAlias, $field->reference, $constraint);
                break;
        }
        if ($output === null) {
            $output = parent::processCondition($tableName, $tableNameAlias, $entity, $field, $constraintType, $constraint);
        }
        return $output;
    }

    private function constraintIn(string $tableName, string $tableNameAlias, string $fieldReference, $constraint): BaseTypeReturn
    {
        $sqls = [];
        $parameters = [];
        foreach ($constraint as $item) {
            $uid = uniqid("bd_");
            $sqls[] = "{$tableNameAlias}.{$fieldReference} = :{$uid}";
            $parameters[$uid] = $item;
        }
        $sql = implode(' or ', $sqls);
        return new BaseTypeReturn($sql, $parameters, $tableName, $tableNameAlias);
    }

    private function constraintNotIn(string $tableName, string $tableNameAlias, string $fieldReference, $constraint): BaseTypeReturn
    {
        $sqls = [];
        $parameters = [];
        foreach ($constraint as $item) {
            $uid = uniqid("bd_");
            $sqls[] = "{$tableNameAlias}.{$fieldReference} != :{$uid}";
            $parameters[$uid] = $item;
        }
        $sql = implode(' and ', $sqls);
        return new BaseTypeReturn($sql, $parameters, $tableName, $tableNameAlias);
    }

    private function constraintEquals(string $tableName, string $tableNameAlias, string $fieldReference, $constraint): BaseTypeReturn
    {
        if (!is_string($constraint) && !is_numeric($constraint)) {
            throw new \Exception('Constraint parameter must be of type string or integer');
        }
        $uid = uniqid("bd_");
        $sql = "{$tableNameAlias}.{$fieldReference} = :{$uid}";
        $parameters = [ $uid => $constraint ];
        return new BaseTypeReturn($sql, $parameters, $tableName, $tableNameAlias);
    }

    private function constraintNotEquals(string $tableName, string $tableNameAlias, string $fieldReference, $constraint): BaseTypeReturn
    {
        if (!is_string($constraint) && !is_numeric($constraint)) {
            throw new \Exception('Constraint parameter must be of type string or integer');
        }
        $uid = uniqid("bd_");
        $sql = "{$tableNameAlias}.{$fieldReference} != :{$uid}";
        $parameters = [ $uid => $constraint ];
        return new BaseTypeReturn($sql, $parameters, $tableName, $tableNameAlias);
    }

}