<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 */

declare(strict_types=1);
namespace BlueSky\Framework\Query\DataTypes;

use BlueSky\Framework\Entity\Schema\Field as SchemaField;
use BlueSky\Framework\Entity\Schema\Entity as SchemaEntity;

abstract class BaseType
{

    protected const CONSTRAINT_TYPE_IS_NULL = 'is_null';
    protected const CONSTRAINT_TYPE_IS_NOT_NULL = 'not_is_null';
    protected const CONSTRAINT_TYPE_IS_EMPTY = 'is_empty';
    protected const CONSTRAINT_TYPE_IS_NOT_EMPTY = 'is_not_empty';

    /** @var BaseTypeReturn|null $output */
    private $output = null;

    public function __construct(SchemaField $field, string $constraintType, $constraint, array $tableNameAliases = [])
    {
        $entity = $field->getParentEntity();
        $tableName = $entity->getEntityTableName();
        $tableNameAlias = $tableNameAliases[$tableName] ?? uniqid('bd_');
        $this->output = $this->processCondition($tableName, $tableNameAlias, $entity, $field, $constraintType, $constraint);
    }

    protected function processCondition(string $tableName, string $tableNameAlias, SchemaEntity $entity, SchemaField $field, string $constraintType, $constraint): BaseTypeReturn
    {
        $output = null;
        switch ($constraintType) {
            case self::CONSTRAINT_TYPE_IS_NULL:
                $output = $this->constraintIsNull($tableName, $tableNameAlias, $field->reference);
                break;
            case self::CONSTRAINT_TYPE_IS_NOT_NULL:
                $output = $this->constraintIsNotNull($tableName, $tableNameAlias, $field->reference);
                break;
            case self::CONSTRAINT_TYPE_IS_EMPTY:
                $output = $this->constraintIsEmpty($tableName, $tableNameAlias, $field->reference);
                break;
            case self::CONSTRAINT_TYPE_IS_NOT_EMPTY:
                $output = $this->constraintIsNotEmpty($tableName, $tableNameAlias, $field->reference);
                break;
        }
        return $output;
    }

    public final function getOutput(): BaseTypeReturn { return $this->output; }

    protected function constraintIsNull(string $tableName, string $tableNameAlias, string $fieldReference): BaseTypeReturn
    {
        $sql = "{$tableNameAlias}.{$fieldReference} is null";
        return new BaseTypeReturn($sql, [], $tableName, $tableNameAlias);
    }

    protected function constraintIsNotNull(string $tableName, string $tableNameAlias, string $fieldReference): BaseTypeReturn
    {
        $sql = "{$tableNameAlias}.{$fieldReference} is not null";
        return new BaseTypeReturn($sql, [], $tableName, $tableNameAlias);
    }

    protected function constraintIsEmpty(string $tableName, string $tableNameAlias, string $fieldReference): BaseTypeReturn
    {
        $sql = "{$tableNameAlias}.{$fieldReference} is null or {$tableNameAlias}.{$fieldReference} = ''";
        return new BaseTypeReturn($sql, [], $tableName, $tableNameAlias);
    }

    protected function constraintIsNotEmpty(string $tableName, string $tableNameAlias, string $fieldReference): BaseTypeReturn
    {
        $sql = "{$tableNameAlias}.{$fieldReference} is not null and {$tableNameAlias}.{$fieldReference} != ''";
        return new BaseTypeReturn($sql, [], $tableName, $tableNameAlias);
    }
}