<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 */

declare(strict_types=1);
namespace BlueSky\Framework\Query\DataTypes;

use BlueSky\Framework\Entity\Schema\Field as SchemaField;

final class Factory
{
    private function __construct() { }

    private function __clone() { }

    private const MAPPING = [
        'string' => 'BlueSky\Framework\Query\DataTypes\StringType',
        'url' => 'BlueSky\Framework\Query\DataTypes\StringType',
        'email' => 'BlueSky\Framework\Query\DataTypes\StringType',
        'telephone' => 'BlueSky\Framework\Query\DataTypes\StringType',
        'text' => 'BlueSky\Framework\Query\DataTypes\StringType',
        'textarea' => 'BlueSky\Framework\Query\DataTypes\StringType',
        'richtext' => 'BlueSky\Framework\Query\DataTypes\StringType',

        'bool' => 'BlueSky\Framework\Query\DataTypes\BoolType',
        'boolean' => 'BlueSky\Framework\Query\DataTypes\BoolType',

        'date' => 'BlueSky\Framework\Query\DataTypes\DateType',
        'date_immutable' => 'BlueSky\Framework\Query\DataTypes\DateType',

        'datetime' => 'BlueSky\Framework\Query\DataTypes\DateTimeType',
        'datetime_immutable' => 'BlueSky\Framework\Query\DataTypes\DateTimeType',

        'int' => 'BlueSky\Framework\Query\DataTypes\IntType',
        'integer' => 'BlueSky\Framework\Query\DataTypes\IntType',
        'decimal' => 'BlueSky\Framework\Query\DataTypes\IntType',
        'currency' => 'BlueSky\Framework\Query\DataTypes\IntType',
        'fkey' => 'BlueSky\Framework\Query\DataTypes\IntType',

        'primary_key_int' => 'BlueSky\Framework\Query\DataTypes\EnumType',
        'enum' => 'BlueSky\Framework\Query\DataTypes\EnumType',
        'enum_integer' => 'BlueSky\Framework\Query\DataTypes\EnumType',
        'enum_string' => 'BlueSky\Framework\Query\DataTypes\EnumType',

        'single_select' => 'BlueSky\Framework\Query\DataTypes\SingleSelectType',
        'radiobutton' => 'BlueSky\Framework\Query\DataTypes\SingleSelectType',

        'multi_select' => 'BlueSky\Framework\Query\DataTypes\MultiSelectType',
        'checkbox' => 'BlueSky\Framework\Query\DataTypes\MultiSelectType',
    ];

    public static function getInstance(string $type, SchemaField $field, string $constraintType, $constraint, $tableNameAliases = []): BaseType
    {
        /** @var BaseType $queryType */
        $queryType = self::MAPPING[$type];
        return new $queryType($field, $constraintType, $constraint, $tableNameAliases);
    }
}