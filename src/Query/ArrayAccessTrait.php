<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 */
declare(strict_types=1);

namespace BlueSky\Framework\Query;

/**
 * Schema service
 * @package Lote\App\CustomFields\Service
 */
trait ArrayAccessTrait
{

    /**
     * Check if an offset exists
     * @access public
     * @param string $offset
     * @return boolean true on success or false on failure.
     */
    public function offsetExists($offset)
    {
        return isset($this->{$offset});
    }


    /**
     * Get the value of an offset
     * @access public
     * @param string $offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return $this->{$offset};
    }

    /**
     * Set the value of an offset
     * @access public
     * @param string $offset
     * @param mixed $value
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        $this->{$offset} = $value;
    }

    /**
     * Unset the value of an offset
     * @access public
     * @param string $offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->{$offset});
    }

}
