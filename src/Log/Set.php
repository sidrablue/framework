<?php

namespace BlueSky\Framework\Log;

use BlueSky\Framework\Registry\Config;
use BlueSky\Framework\State\Base as BaseState;

/**
 * Primary logging class for the Lote Framework.
 *
 * @package BlueSky\Framework\Log
 * */
class Set
{
    private array $loggers = [];
    private Config $config;
    private BaseState $state;

    /**
     * Private constructor to create an instance of the Logger
     * @access public
     * @param Config $config
     * @param BaseState $state
     */
    public function __construct(Config $config, BaseState $state)
    {
        $this->config = $config;
        $this->state = $state;
        $this->loggers['account'] = [];
        $this->loggers['master'] = [];
    }

    /**
     * Get an account logger by its reference
     * @access public
     * @param string $reference
     * @return LoteLog
     * */
    public function getAccountLogger($reference = 'log') {
        if(!isset($this->loggers['account']) || !isset($this->loggers['account'][$reference])) {
            $log = new LoteLog($this->state->accountReference, $this->state->getConfig()->get('log.account'), dirname(LOTE_LOG_PATH).'/'.$this->state->accountReference.'/', $reference.'.txt', $reference);
            $this->loggers['account'][$reference] = $log;
        }
        return $this->loggers['account'][$reference];
    }

    /**
     * Get an account logger by its reference
     * @access public
     * @param string $reference
     * @return LoteLog
     * */
    public function getMasterLogger($reference = 'log') {
        if(!isset($this->loggers['master']) || !isset($this->loggers['master'][$reference])) {
            $log = new LoteLog($this->state->accountReference, $this->state->getConfig()->get('log.master'), LOTE_LOG_MASTER, $reference.'.txt', $reference);
            $this->loggers['master'][$reference] = $log;
        }
        return $this->loggers['master'][$reference];
    }
}
