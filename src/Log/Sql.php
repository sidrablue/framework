<?php
namespace BlueSky\Framework\Log;

use Doctrine\DBAL\Logging\SQLLogger;
use BlueSky\Framework\Object\State;
use BlueSky\Framework\State\Base;

class Sql extends State implements SQLLogger
{

    /**
     * @var int $queryCount - the counter of queries run so far
     * */
    private $queryCount = 0;

    /**
     * @var array $queries
     * The array of queries executed and their params, types and ms of execution
     * */
    private $queries = [];

    /**
     * @var string $logKey
     * The log key used for logging via the LoteLog, so in a single file instances they can be distinguished per request
     * */
    private $logKey;

    /**
     * @var int $startTime
     * The start time (micro time in float format) of the last query to be logged
     * */
    private $startTime;

    /**
     * The default constructor to define the request specific log key
     * @access public
     * @param Base $state
     */
    public function __construct(Base $state)
    {
        parent::__construct($state);
        $this->logKey = uniqid("sql_");
    }

    /**
     * Handler for starting an SQL query
     * @access public
     * @param string $sql - the SQL query
     * @param array $params - the params for the SQL query
     * @param array $types - the types of the SQL params
     * @return void
     * */
    public function startQuery($sql, array $params = null, array $types = null)
    {
        $this->startTime = microtime(true);
        $this->queries[++$this->queryCount] = ['sql' => $sql, 'params' => $params, 'types' => $types, 'ms' => 0];
        $this->getState()->getLoggers()->getAccountLogger("sql")->debug("{$this->logKey} : {$this->queryCount} : $sql", ['params' => $params, 'types' => $types]);
    }

    /**
     * Handler for on query completion, to log using the LoteLog and to add to the local query array
     * @access public
     * @return void
     */
    public function stopQuery()
    {
        $ms = microtime(true) - $this->startTime;
        $this->getState()->getLoggers()->getAccountLogger("sql")->debug("{$this->logKey} : {$this->queryCount} : STOP", ['ms' => $ms]);
        $this->queries[$this->queryCount]['ms'] = $ms;
    }

    /**
     * Get the SQL queries stored in this logger
     * @access public
     * @return array
     * */
    public function getQueries()
    {
        return $this->queries;
    }

}
