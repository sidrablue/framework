<?php
namespace BlueSky\Framework\Log;

use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

class Factory
{
    protected static $map = array();

    protected static $defaultLogger;

    private function __construct()
    {
    }

    /**
     * Must returns with the same object every time!
     *
     * @return LoteLog
     */
    protected static function getDefaultLogger()
    {
        if (self::$defaultLogger === null) {
            self::$defaultLogger = new NullLogger();
        }
        return self::$defaultLogger;
    }

    /**
     * @param string $name
     * @return LoggerInterface
     */
    protected static function findClosestAncestor($name)
    {
        $name = trim($name, '\\');
        $parts = explode('\\', $name);
        while (!array_key_exists($name, self::$map) && !empty($parts)) {
            array_pop($parts);
            $name = implode('\\', $parts);
        }
        return array_key_exists($name, self::$map)
            ? self::$map[$name]
            : self::getDefaultLogger();
    }

    public static function registerLogger($name, LoggerInterface $logger)
    {
        self::$map[(string) $name] = $logger;
    }

    /**
     * @return \Psr\Log\LoggerInterface
     */
    public static function getLogger($name)
    {
        if (!array_key_exists($name, self::$map)) {
            self::$map[$name] = self::findClosestAncestor($name);
        }
        return self::$map[$name];
    }
}