<?php
namespace BlueSky\Framework\Log;

use Monolog\Formatter\LineFormatter;
use Monolog\Handler\FirePHPHandler;
use Monolog\Handler\RavenHandler;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Monolog\Handler\NullHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Logger as MonologLogger;
use Aws\CloudWatchLogs\CloudWatchLogsClient;
use Maxbanton\Cwh\Handler\CloudWatch;
use BlueSky\Framework\State\Base as BaseState;
use BlueSky\Framework\State\Web as WebState;

/**
 * Primary logging class for the Lote Framework.
 *
 * @package BlueSky\Framework\Log
 * */
class LoteLog implements LoggerInterface
{
    /**
     * The logger
     * @var MonologLogger $logger
     * */
    private $logger;

    /**
     * @var array $config - the config variables
     *  */
    private $config;

    /**
     * @var String $logFile - the file log name
     *  */
    private $logFile;

    /**
     * @var String $logFileName - the file log name
     *  */
    private $logFileName = 'log.txt';

    /**
     * @var String $logDir - the file log path
     *  */
    private $logDir;

    /**
     * @var String $reference - the reference of the log
     *  */
    private $reference;

    /**
     * @var String $accountReference - the account reference of the log
     *  */
    private $accountReference;

    /**
     * @var array $suppressedPaths - routes for which errors are not to be logged
     * */
    private $suppressedPaths = [];

    /**
     * Private constructor to create an instance of the Logger
     * @access public
     * @param string $accountReference
     * @param array $config
     * @param string $logDir
     * @param string $logFilename
     * @param string $reference
     */
    public function __construct($accountReference, $config, $logDir = '', $logFilename = 'log.txt', $reference = '')
    {
        $this->accountReference = $accountReference;
        $this->config = $config;
        $this->logger = new MonologLogger('LOTE');
        $this->logDir = $logDir;
        $this->logFileName = $logFilename;
        $this->reference = $reference;
        if (isset($this->config['enabled']) && $this->config['enabled'] == "1") {
            $this->setupFileHandler();
            $this->setupFirePhpHandler();
            //$this->setupSentryHandler();
            $this->setupCloudWatchHandler();
        } else {
            $this->logger->pushHandler(new NullHandler());
        }
    }

    /**
     * Setup an AWS CloudWatch handler if one is defined
     * @access private
     * @return void
     * */
    private function setupCloudWatchHandler()
    {
        if (isset($this->config['cloudwatch']) && isset($this->config['cloudwatch']['enabled']) && $this->config['cloudwatch']['enabled'] == '1') {
            $logLevel = MonologLogger::ERROR;
            $value = constant('\Monolog\Logger::' . strtoupper($this->config['cloudwatch']['level']));
            if (is_numeric($value)) {
                $logLevel = $value;
            }
            $sdkParams = [
                'region' => $this->config['cloudwatch']['region'],
                'version' => 'latest',
                'credentials' => [
                    'key' => $this->config['cloudwatch']['key'],
                    'secret' => $this->config['cloudwatch']['secret']
                ]
            ];
            $client = new CloudWatchLogsClient($sdkParams);
            $groupName = $this->config['cloudwatch']['group'];
            $streamName = isset($this->config['cloudwatch']['stream']) ? $this->config['cloudwatch']['stream'] : $this->reference;
            $retentionDays = $this->config['cloudwatch']['retention'];
            $batchSize = $this->config['cloudwatch']['batch_size'];
            $handler = new CloudWatch($client, $groupName, $streamName, $retentionDays, $batchSize);
            $handler->setLevel($logLevel);
            $this->logger->pushHandler($handler);
        }
    }

    /**
     * Setup the Sentry handler for logging
     * @access private
     * @return void
     * */
    /*private function setupSentryHandler()
    {
        if (isset($this->config['sentry']) && isset($this->config['sentry']['enabled']) && $this->config['sentry']['enabled'] == '1') {
            $logLevel = MonologLogger::ERROR;
            $value = constant('\Monolog\Logger::' . strtoupper($this->config['sentry']['level']));
            if (is_numeric($value)) {
                $logLevel = $value;
            }
            $ravenClient = new Raven_Client($this->config['sentry']['key']);
            $handler = new RavenHandler($ravenClient, $logLevel);
            $handler->setFormatter(new LineFormatter("%message% %context% %extra%\n"));
            $this->logger->pushHandler(new RavenHandler($ravenClient, $logLevel));
        }
    }*/

    /**
     * Setup the FirePHP handler for logging
     * @access private
     * @return void
     * */
    private function setupFirePhpHandler()
    {
        if (isset($this->config['firephp.enabled']) && $this->config['firephp.enabled'] == '1' && isset($this->config['firephp.level']) && php_sapi_name() != 'cli'
        ) {
            $logLevel = MonologLogger::ERROR;
            $value = constant('\Monolog\Logger::' . strtoupper($this->config['firephp.level']));
            if (is_numeric($value)) {
                $logLevel = $value;
            }
            $this->logger->pushHandler(new FirePHPHandler($logLevel));
        }
    }

    /**
     * Setup the File logging handler
     * @access private
     * @return void
     * */
    private function setupFileHandler()
    {
        $logLevel = MonologLogger::ERROR;
        if (isset($this->config['level']) && defined('\Monolog\Logger::' . strtoupper($this->config['level']))) {
            $value = constant('\Monolog\Logger::' . strtoupper($this->config['level']));
            if (is_numeric($logLevel)) {
                $logLevel = $value;
            }
        }
        $this->logger->pushHandler(new StreamHandler($this->getLogFile(), $logLevel));
    }

    /**
     * Get the log file to use for logging
     * @access private
     * @return string
     * */
    private function getLogFile()
    {
        if (!$this->logFile) {
            if ($this->logDir) {
                $logDir = $this->logDir;
            } else {
                $logDir = LOTE_LOG_PATH;
            }
            $this->logFile = $logDir . $this->getLogFileName();
            if (isset($this->config['log_type']) && $this->config['log_type'] == "individual") {
                $time = date('Y-m-d_H-m-s', time());
                $this->logFile = $logDir . 'log_' . $time . '.txt';
                while (file_exists($this->logFile)) {
                    $this->logFile = $logDir . 'log_' . $time . '_' . uniqid() . '.txt';
                }
            }
        }
        return $this->logFile;
    }

    /**
     * System is unusable.
     *
     * @param string $message
     * @param array $context
     * @return boolean
     */
    public function emergency($message, array $context = array())
    {
        return $this->log(LogLevel::EMERGENCY, $message, $context);
    }

    /**
     * Action must be taken immediately.
     *
     * Example: Entire website down, database unavailable, etc. This should
     * trigger the SMS alerts and wake you up.
     *
     * @param string $message
     * @param array $context
     * @return boolean
     */
    public function alert($message, array $context = array())
    {
        return $this->log(LogLevel::ALERT, $message, $context);
    }

    /**
     * Critical conditions.
     *
     * Example: Application component unavailable, unexpected exception.
     *
     * @param string $message
     * @param array $context
     * @return boolean
     */
    public function critical($message, array $context = array())
    {
        return $this->log(LogLevel::CRITICAL, $message, $context);
    }

    /**
     * Runtime errors that do not require immediate action but should typically
     * be logged and monitored.
     *
     * @param string $message
     * @param array $context
     * @return boolean
     */
    public function error($message, array $context = array())
    {
        return $this->log(LogLevel::ERROR, $message, $context);
    }

    /**
     * Exceptional occurrences that are not errors.
     *
     * Example: Use of deprecated APIs, poor use of an API, undesirable things
     * that are not necessarily wrong.
     *
     * @param string $message
     * @param array $context
     * @return boolean
     */
    public function warning($message, array $context = array())
    {
        return $this->log(LogLevel::WARNING, $message, $context);
    }

    /**
     * Exceptional occurrences that are not errors.
     *
     * Example: Use of deprecated APIs, poor use of an API, undesirable things
     * that are not necessarily wrong.
     *
     * @param string $message
     * @param array $context
     * @return boolean
     */
    public function warn($message, array $context = array())
    {
        return $this->log(LogLevel::WARNING, $message, $context);
    }

    /**
     * Normal but significant events.
     *
     * @param string $message
     * @param array $context
     * @return boolean
     */
    public function notice($message, array $context = array())
    {
        return $this->log(LogLevel::NOTICE, $message, $context);
    }

    /**
     * Interesting events.
     *
     * Example: User logs in, SQL logs.
     *
     * @param string $message
     * @param array $context
     * @return boolean
     */
    public function info($message, array $context = array())
    {
        return $this->log(LogLevel::INFO, $message, $context);
    }

    /**
     * Detailed debug information.
     *
     * @param string $message
     * @param array $context
     * @return boolean
     */
    public function debug($message, array $context = array())
    {
        return $this->log(LogLevel::DEBUG, $message, $context);
    }

    /**
     * Format the message for logging, which will append the account reference to it if one exists
     * @access private
     * @param string $message - the original message
     * @return string - the formatted message
     * */
    private function formatMessage($message)
    {
        $result = $message;
        if ($this->accountReference) {
            $result = $this->accountReference . '-' . $message;
        }
        return $result;
    }

    /**
     * Logs with an arbitrary level.
     *
     * @param mixed $level
     * @param string $message
     * @param array $context
     * @return boolean
     */
    public function log($level, $message, array $context = array())
    {
        $result = true;
        try {
            $message = $this->formatMessage($message);
            if ($this->logger instanceof CloudWatch) {
                if(strlen($message) < 262144) {
                    $result = $this->logger->log($this->parseLevel($level), $message, json_encode($context));
                }
            } else {
                $result = $this->logger->log($this->parseLevel($level), $message, $context);
            }
        }
        catch(\Exception $e) {
            //do nothing, could not log so trying to log it again may cause a loop.
        }
        return $result;
    }

    /**
     * Parse the string level into a Monolog constant.
     * @access protected
     * @param string $level
     * @return int
     * @throws \InvalidArgumentException
     */
    protected function parseLevel($level)
    {
        switch ($level) {
            case 'debug':
                return MonologLogger::DEBUG;

            case 'info':
                return MonologLogger::INFO;

            case 'notice':
                return MonologLogger::NOTICE;

            case 'warning':
                return MonologLogger::WARNING;

            case 'error':
                return MonologLogger::ERROR;

            case 'critical':
                return MonologLogger::CRITICAL;

            case 'alert':
                return MonologLogger::ALERT;

            case 'emergency':
                return MonologLogger::EMERGENCY;

            default:
                throw new \InvalidArgumentException("Invalid log level.");
        }
    }

    /**
     * Get the log filename
     * @access public
     * @return String
     */
    public function getLogFileName()
    {
        return $this->logFileName;
    }

    /**
     * Set the log filename
     * @acess public
     * @param String $logFileName
     * @return void
     */
    public function setLogFileName($logFileName)
    {
        $this->logFileName = $logFileName;
    }

    /**
     * Get the logger interface
     * @access public
     * @return MonologLogger
     * */
    public function getLoggerInterface()
    {
        return $this->logger;
    }

    /**
     * Add a suppressed path, for which no errors are to be logged
     * @access public
     * @param $pathRegex
     * @return void
     */
    public function addSuppressedPath($pathRegex)
    {
        $this->suppressedPaths[] = $pathRegex;
    }
}
