<?php
namespace BlueSky\Framework\Event;

use BlueSky\Framework\Model\Group as GroupModel;

/**
 * Base class for event dispatch data
 */
class Data
{

    /**
     * @var mixed $data - the event data
     * */
    public $data = null;

    /**
     * @var mixed $params - the event params
     * */
    private $params = null;

    /**
     * @var array $result - the event result
     * */
    private $result = [];

    /**
     * Default constructor
     * @param null $data
     * @param null $params
     */
    public function __construct($data = null, $params = null)
    {
        $this->data = $data;
        $this->params = $params;
    }

    /**
     * Get the data for this event
     * @access public
     * @param null|string $key
     * @param null|mixed $default
     * @return mixed
     */
    public function getData($key = null, $default = null)
    {
        $result = $default;
        if($key && array_key_exists($key, $this->data)) {
            $result = $this->data[$key];
        }
        elseif(!$key) {
            $result = $this->data;
        }
        return $result;
    }

    /**
     * Set the data for this event
     * @access public
     * @param mixed $data
     * @return void
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * Get the params for this event
     * @access public
     * @return mixed
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * Set the params for this event
     * @access public
     * @param mixed $params
     * @return void
     */
    public function setParams($params)
    {
        $this->params = $params;
    }

    /**
     * Get the result for this event
     * @access public
     * @return array|mixed
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Set the result for this event
     * @access public
     * @param mixed $result
     * @return void
     */
    public function setResult($result)
    {
        $this->result = $result;
    }

    /**
     * Add a result for this event
     * @access public
     * @param mixed $result
     * @return void
     */
    public function addResult($result)
    {
        if(is_array($this->result)) {
            $this->result[] = $result;
        }
    }

}
