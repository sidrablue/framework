<?php
namespace BlueSky\Framework\Event;

use BlueSky\Framework\Object\State as BaseState;

/**
 * Base class for all models
 */
class Manager extends BaseState
{

    /**
     * @var array $listeners - the events to observes
     * */
    private $listeners = [];

    /**
     * Check if a user is in a specified group
     * @access public
     * @param string $eventRef
     * @param null|Data|\BlueSky\Framework\Object\Entity\Base $eventData
     * @return Data
     */

    public function dispatch($eventRef, $eventData = null)
    {
        if ($eventData == null) {
            $data = new Data($eventData);
            $eventData = $data;
        }
        if (isset($this->listeners[$eventRef]) && is_array($this->listeners[$eventRef])) {
            foreach ($this->listeners[$eventRef] as $listener) {
                $params = array_slice(func_get_args(), 1);
                if ($listener instanceof \Closure) {
                    $listener($eventData, $params);
                } else {
                    call_user_func_array($listener, array_merge([$eventData], $params));
                }
            }
        }
        return $eventData;
    }
        public function dispatchAny($eventRef, $eventData)
    {

    }

    /**
     * Check if event listeners exist for a given reference
     * @access public
     * @param $eventRef
     * @return bool
     */
    public function has($eventRef)
    {
        return array_key_exists($eventRef, $this->listeners);
    }

    /**
     * Register an interest in an event
     * @access public
     * @param $eventRef
     * @param $callback
     * @param int $priority
     */
    public function listen($eventRef, $callback, $priority = null)
    {
        $this->listeners[$eventRef][] = $callback;
    }

}
