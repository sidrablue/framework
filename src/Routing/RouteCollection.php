<?php

namespace BlueSky\Framework\Routing;

use Symfony\Component\Routing\RouteCollection as BaseRouteCollection;
use Symfony\Component\Routing\Route as BaseRoute;

/**
 * RouteCollection service class
 */
class RouteCollection extends BaseRouteCollection
{

    /**
     * Adds a route and throws an exception if the route already exists.
     *
     * @Note removing @throws tag to help the code highlighting in PhpStorm
     *
     * @param string $name The route name
     * @param BaseRoute $route A Route instance
     */
    public function add($name, BaseRoute $route): void
    {
        if (!$this->get($name)) {
            parent::add($name, $route);
        } else {
            throw new \Exception("Route '{$name}' is already defined");
        }
    }

    /**
     * Adds a route and throws an exception if the route already exists.
     *
     * @param $method - the method name that is to be called
     * @param $uriSubPath
     * @param $routeName
     * @param $controllerPath
     * @param array $requirements
     * @throws \Exception $e
     */
    public function addApiRoutes($uriSubPath, $method, $routeName, $controllerPath, $requirements = []): void
    {
        /** @see \BlueSky\Framework\Controller\Api */
        $defaults['_method'] = 'call';
        $defaults['controller'] = '\BlueSky\Framework\Controller\Api';
        $defaults['_api_controller'] = $controllerPath;
        $defaults['_api_method'] = $method;
        $defaults['_is_api'] = true;
        $defaults['format'] = 'json';
        $requirements['lote_api_version'] = "(\d+)";
        $route = new Route('_api/v{lote_api_version}/' . $uriSubPath, $defaults, $requirements);
        $this->add('api.versioned.' . $routeName, $route);
        $route = new Route('_api/' . $uriSubPath, $defaults, $requirements);
        $this->add('api.latest.' . $routeName, $route);
    }

    /**
     * Adds a route and overrides it if it already exists.
     *
     * @param string $name The route name
     * @param BaseRoute $route A Route instance
     * @throws \Exception $e
     */
    public function addOverride($name, BaseRoute $route): void
    {
        parent::add($name, $route);
    }

}
