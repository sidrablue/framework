<?php
namespace BlueSky\Framework\Service;

use BlueSky\Framework\Object\State;

class Url extends State
{

    /**
     * @param string $log - the scheme of this reply
     * */
    protected $scheme;

    /**
     * @param string $baseUrl
     * */
    protected $baseUrl;

    /**
     * @param string $httpHost
     * */
    protected $httpHost;

    /**
     * @param $baseUrl string
     * @return void
     * */
    public function setBaseUrl($baseUrl)
    {
        $this->baseUrl = $baseUrl;
    }

    /**
     * @param string $scheme
     * @return string
     * */
    public function getBaseUrl($scheme = "http://")
    {
        if ($this->baseUrl) {
            $result = $this->baseUrl;
        } elseif ($this->getState()->isCli()) {
            //$url = $this->getState()->getSites()->getCurrentSiteUrl();
            $url = $this->getHttpHost();
            if ($url && $url === '1') {
                $result = $scheme . $this->getState()->getAccount()->domain . '/';
            } else {
                if (!preg_match('|^http(s)?://.*$|i', $url)) {
                    $url = $this->getScheme($scheme) . $this->getHttpHost();
                    //$url = $this->getState()->getSites()->getCurrentSiteScheme($scheme) . $url . '/';
                }
                $result = preg_replace('#/+$#', '/', $url);
            }
        } else {
            $result = $this->getState()->getRequest()->getSchemeAndHttpHost() . $this->getState()->getRequest()->getBasePath() . '/';
        }
        return $result;
    }

    /**
     * Get the HTTP Host and scheme of the current state object
     * @access public
     * @return string
     * */
    public function getHttpSchemeAndHost()
    {
        if (php_sapi_name() == 'cli') {
            $result = $this->getBaseUrl("https://");
        }
        else {
            $result = $this->getScheme().'://'.$this->getHttpHost();
        }
        return rtrim($result, "/").'/';
    }

    /**
     * Get the HTTP Host of the current state object
     * @access public
     * @return string
     * */
    public function getHttpHost()
    {
        if ($this->getState()->isCli()) {
            $result = $this->getState()->getSettings()->get("system.url", $this->getState()->getAccount()->domain ?? false);
        } else {
            $result = $this->getState()->getRequest()->getHttpHost();
        }
        if ($subPath = $this->getState()->getSettings()->get("system.url.base_path") && !$this->getState()->isCli()) {
            if (!preg_match("#^/_admin.*#", $this->getState()->getRequest()->getRequestUri())) {
                $result .= '/' . $subPath;
            }
        }
        return rtrim($result, "/").'/';
    }

    /**
     * Get the HTTP Host of the current state object
     * @access public
     * @param string $default
     * @return string
     * */
    public function getScheme($default = "http://")
    {
        if ($this->getState()->isCli()) {
            $result = $default;
        }
        else {
            $result = $this->getState()->getRequest()->getScheme();
        }
        return $result;
    }

    public function getMasterBaseUrl()
    {
        return $this->getState()->getSettings()->getConfigOrSetting("master.url.base");
    }

}
