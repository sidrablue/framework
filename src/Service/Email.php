<?php
declare(strict_types=1);

namespace BlueSky\Framework\Service;

use BlueSky\Framework\State\Base;
use BlueSky\Framework\Service\Email\EmailFactory as EmailFactory;

class Email
{

    private $methodBulk = '';
    private $apiKeyBulk = '';
    private $methodTransactional = '';
    private $apiKeyTransactional = '';
    private $accountReference = '';
    private $logger;

    public function __construct(Base $state)
    {
        $this->methodBulk = $state->getSettings()->getSettingOrConfig("email.method");
        $this->apiKeyBulk = $state->getSettings()->getSettingOrConfig("{$this->methodBulk}.key");
        $this->methodTransactional = $state->getSettings()->getSettingOrConfig("email.method.transactional") ?? $this->methodBulk;
        $this->apiKeyTransactional = $state->getSettings()->getSettingOrConfig("{$this->methodTransactional}.key.transactional") ?? $this->apiKeyBulk;
        $this->accountReference = $state->accountReference;
        $this->logger = $state->getLoggers()->getMasterLogger($this->methodBulk)->getLoggerInterface();
    }

    public function sendEmail(string $subject, string $content, $to, string $from, string $fromName, array $cc = [], array $bcc = [], array $attachments = []): ?string
    {
        try {
            $obj = EmailFactory::createInstance($this->methodBulk, $this->apiKeyBulk, $this->accountReference, $this->logger);
            $result = $obj->sendEmail($subject, $content, $to, $from, $fromName, $cc, $bcc, $attachments);
        } catch (\Exception $e) {
            $result = null;
        }
        return $result;
    }

    public function sendTransactionalEmail(string $subject, string $content, $to, string $from, string $fromName, array $cc = [], array $bcc = [], array $attachments = []): ?string
    {
        try {
            $obj = EmailFactory::createInstance($this->methodTransactional, $this->apiKeyTransactional, $this->accountReference, $this->logger);
            $result = $obj->sendEmail($subject, $content, $to, $from, $fromName, $cc, $bcc, $attachments);
        } catch (\Exception $e) {
            $result = null;
        }
        return $result;
    }

}
