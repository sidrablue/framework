<?php
namespace BlueSky\Framework\Service;

use BlueSky\Framework\Model\Group as GroupModel;
use BlueSky\Framework\Object\State as BaseState;

/**
 * Base class for all models
 */
class Group extends BaseState
{

    /**
     * @var array|false $userGroups - the groups for this user
     * */
    private $userGroups = false;

    /**
     * Check if a user is in a specified group
     * @access public
     * @param int $groupId - the group to check against
     * @return boolean
     * */
    public function isInGroup($groupId)
    {
        $result = false;
        $groups = $this->getGroupIds();
        if (is_array($groups) && array_search($groupId, $groups) !== false) {
            $result = true;
        }
        return $result;
    }

    /**
     * Get the group Ids for this user
     * @access public
     * @param string|bool $kind - the group kind
     * @return array
     * */
    public function getGroupIds($kind = false)
    {
        $result = [];
        foreach ($this->getUserGroups() as $v) {
            if($kind) {
                if($v['kind']==$kind) {
                    $result[] = $v['id'];
                }
            }
            else {
                $result[] = $v['id'];
            }
        }
        return $result;
    }

    /**
     * Get the groups for this user
     * @access public
     * @return array
     * */
    public function getUserGroups()
    {
        if ($this->userGroups === false) {
            $m = new GroupModel($this->getState());
            if (!$this->userGroups = $m->getUserGroups($this->getState()->getUser()->id)) {
                $this->userGroups = [];
            }
        }
        return $this->userGroups;
    }

    /**
     * Get the groups for this user
     * @access public
     * @return array
     * */
    public function isInAdminGroup()
    {
        $result = false;
        $userGroups = $this->getUserGroups();
        foreach($userGroups as $g) {
            if($g['kind'] == 'admin') {
                $result = true;
                break;
            }
        }
        return $result || $this->getState()->getUser()->is_admin;
    }

}
