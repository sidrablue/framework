<?php
namespace BlueSky\Framework\Service;

use BlueSky\Framework\Entity\Cf\CfObject;
use BlueSky\Framework\Object\State as BaseState;
use BlueSky\Framework\Entity\Cf\CfGroup;
use BlueSky\Framework\Util\Arrays;

/**
 * Service for custom field management, such as loading, validating, saving.
 */
class CustomField extends BaseState
{

    /**
     * Load the custom data for this object
     * @param integer $id
     * @param string $baseTable - the base table of the custom data
     * @param bool $loadEmptyGroups - true to load empty groups
     * @param bool $includeDeleted - Whether or not to include objects that have been deleted
     * @return array
     */
    private function loadCustomData($id, $baseTable, $loadEmptyGroups = false, $includeDeleted = false)
    {
        $result = [];
        if ($id) {
            $q = $this->getState()->getReadDb()->createQueryBuilder();
            $q->select('v.*, f.*, v.id as value_id, g.reference as group_reference, g.name as group_name, g.id as group_id, g.sort_order as sort_order')
                ->from($baseTable, 'v')
                ->leftJoin('v', 'sb__cf_field', 'f', 'f.id = v.cf_field_id')
                ->leftJoin('f', 'sb__cf_group', 'g', 'g.id = f.cf_group_id')
                ->where('v.object_id = :id')
                ->setParameter('id', $id)
                ->andWhere('g.is_active is true')
                ->andWhere('g.is_hidden is not true')
                ->addOrderBy('g.name', 'asc')
                ->addOrderBy('f.sort_order', 'asc');

                if(!$includeDeleted) {
                    $q->andWhere('v.lote_deleted is null')
                    ->andWhere('f.lote_deleted is null');
                }
            $s = $q->execute();
            $fieldData = $s->fetchAll(\PDO::FETCH_ASSOC);
            $groupData = $this->fieldsToGroups($fieldData);
            foreach ($groupData as $v) {
                $c = new CfGroup($this->getState());
                $c->setTableName($baseTable.'_group');
                $c->id = $v[0]['group_id'];
                $c->name = $v[0]['group_name'];
                $c->reference = $v[0]['group_reference'];
                $c->sort_order = $v[0]['sort_order'];
                $c->setData($v);
                $c->setFieldData($v, true);
                $result[] = $c;
            }
            if($loadEmptyGroups) {
                $q = $this->getState()->getReadDb()->createQueryBuilder();
                $q->select('fg.reference as group_reference, fg.name as group_name, fg.id as group_id, fg.sort_order as sort_order')
                    ->from($baseTable.'_group', 'ug')
                    ->leftJoin('ug', 'sb__cf_group', 'fg', 'fg.id = ug.group_id')
                    ->where('ug.object_id = :id')
                    ->setParameter('id', $id)
                    ->andWhere('ug.is_active is true')
                    ->andWhere('fg.is_active is true')
                    ->andWhere('fg.is_hidden is not true')
                    ->addOrderBy('sort_order', 'asc');
                $s = $q->execute();
                $groupData = $s->fetchAll(\PDO::FETCH_ASSOC);
                foreach ($groupData as $v) {
                    $groupExists = false;
                    $cnt = count($result);
                    for($i=0; $i<$cnt; $i++) {
                        if($result[$i]->id==$v['group_id']) {
                            $groupExists = true;
                            break;
                        }
                    }
                    if(!$groupExists) {
                        $c = new CfGroup($this->getState());
                        $c->id = $v['group_id'];
                        $c->name = $v['group_name'];
                        $c->reference = $v['group_reference'];
                        $c->sort_order = $v['sort_order'];
                        $c->loadAllFields();
                        $result[] = $c;
                    }
                }
                Arrays::order2dClassArray($result, 'sort_order');
            }
        }
        return $result;
    }

    /**
     * Convert fields to groups
     * @param array $fieldList
     * @return array
     * */
    private function fieldsToGroups($fieldList)
    {
        $result = [];
        foreach ($fieldList as $v) {
            $result[$v['group_reference']][] = $v;
        }
        return $result;
    }

    /**
     * Get the base table name for where data is stored for a particular object
     * @access private
     * @param string $baseTable - the base table if one exists
     * @return string
     * */
    private function getBaseTableName($baseTable = '')
    {
        $result = 'sb__cf_value';
        if($baseTable) {
            $result = $baseTable.'__cf_value';
        }
        return $result;
    }

    /**
     * Get the custom data for an object
     * @access public
     * @param int $id - the ID of the object
     * @param string $baseTable
     * @param bool $loadEmptyGroups
     * @return array of CfGroup objects
     * */
    public function getCustomViewData($id, $baseTable = '', $loadEmptyGroups = false)
    {
        return $this->loadCustomData($id, $this->getBaseTableName($baseTable), $loadEmptyGroups);
    }

    /**
     * Get the custom data required to edit an object, ensuring that all fields and groups are present
     * @access public
     * @param int $id - the ID of the object
     * @param string $baseTable
     * @param string $reference
     * @return array of CfGroup objects
     * */
    public function getCustomEditData($id, $baseTable = '', $reference = 'user')
    {
        $e = new CfObject($this->getState());
        $e->load($reference, true);
        $allCustomGroups = $e->getActiveGroups();
        $customData = $this->loadCustomData($id, $this->getBaseTableName($baseTable), true);
        $allCount = count($allCustomGroups);
        for($i=0; $i<$allCount; $i++) {
            $dataCount = count($customData);
            for($j=0; $j<$dataCount; $j++) {
                if($customData[$j]->reference==$allCustomGroups[$i]->reference) {
                    //iterate through the fields
                    $allFields = $allCustomGroups[$i]->getChildFields();
                    $allFieldsCount = count($allFields);
                    for($x=0; $x<$allFieldsCount; $x++) {
                        $dataFields = $customData[$j]->getChildFields();
                        $dataFieldsCount = count($dataFields);
                        for($y=0; $y<$dataFieldsCount; $y++) {
                           if($dataFields[$y]->reference==$allFields[$x]->reference){
                               $allFields[$x]->setValueObject($dataFields[$y]->getValueObject());
                               break;
                           }
                        }
                    }
                    break;
                }
            }
        }
        return $allCustomGroups;
    }

    /**
     * Get the custom data for an object
     * @access public
     * @param int $id - the ID of the object
     * @param string $baseTable
     * @return array of data
     * */
    public function getCustomViewDataArray($id, $baseTable = '')
    {
        $data = $this->getCustomViewData($id, $this->getBaseTableName($baseTable));
        die('todo');
    }

}
