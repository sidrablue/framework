<?php
namespace BlueSky\Framework\Service;

use Aws\Ec2\Ec2Client;
use Aws\ElasticLoadBalancing\ElasticLoadBalancingClient;
use Aws\Route53\Route53Client;
use Aws\Sdk;
use BlueSky\Framework\Object\State;

/**
 * Service class for generic AWS functionality
 * @package BlueSky\Framework\Service
 * */
class Aws extends State
{

    /**
     * Get all EC2 instances that belong to a load balancer
     * @access public
     * @param string $key - the AWS credentials key
     * @param string $secret - the AWS credentials secret
     * @param string $region - the AWS region, e.g. us-west-1, ap-southeast-2
     * @param string $name - the name of hte load balancer
     * @return array
     * */
    public function getEc2InstancesByLoadBalancer($key, $secret, $region, $name)
    {
        $result = [];
        $ec2 = new ElasticLoadBalancingClient([
            'credentials' => ['key' => $key, 'secret' => $secret],
            'region'  => $region,
            'version' => 'latest',
            'scheme' => 'http'
        ]);
        $command = $ec2->getCommand('describeInstanceHealth', ['LoadBalancerName' => $name]);
        $a = $ec2->execute($command);
        $data = $a->toArray();
        if(is_array($data['InstanceStates'])) {
            $allInstances = $this->getAllEc2Instances($key, $secret, $region);
            foreach ($data['InstanceStates'] as $v) {
                $result[$v['InstanceId']] = $allInstances[$v['InstanceId']];
            }
        }
        return $result;
    }

    /**
     * Get all EC2 isntances in a given region
     * @access public
     * @param string $key - the AWS credentials key
     * @param string $secret - the AWS credentials secret
     * @param string $region - the AWS region, e.g. us-west-1, ap-southeast-2
     * @return array
     * */
    public function getAllEc2Instances($key, $secret, $region)
    {
        $result = [];
        $ec2 = new Ec2Client([
            'credentials' => ['key' => $key, 'secret' => $secret],
            'region'  => $region,
            'version' => 'latest',
            'scheme' => 'http'
        ]);

        $command = $ec2->getCommand('DescribeInstances');
        $a = $ec2->execute($command);
        $data = $a->toArray();
        if(isset($data['Reservations'])) {
            $reservations = $data['Reservations'];
            foreach($reservations as $reservation) {
                if(isset($reservation['Instances'])) {
                    $instances = $reservation['Instances'];
                    foreach($instances as $instance) {
                        $result[$instance['InstanceId']] = $instance;
                    }
                }
            }
        }
        return $result;
    }

    /**
     * Add or update an AWS Route53 Record
     *
     * @access public
     * @param string $parentZoneId - AWS ID of parent hosted zone
     * @param string $name - Name of record
     * @param string $type - Record type
     * @param string $value - Record value
     * @return array
     */
    public function addRoute53Record($parentZoneId, $name, $type, $value)
    {
        $key = $this->getState()->getSettings()->getSettingOrConfig('aws.route53.key');
        $secret = $this->getState()->getSettings()->getSettingOrConfig('aws.route53.secret');
        $region = $this->getState()->getSettings()->getSettingOrConfig('aws.route53.region');

        $shareConfig = [
            'credentials' => ['key' => $key, 'secret' => $secret],
            'region' => $region,
            'version' => 'latest',
        ];

        $result = [
            'success' => false
        ];
        try {
            $route53 = new Route53Client($shareConfig);
            $route53->changeResourceRecordSets([
                'ChangeBatch' => [
                    'Changes' => [[
                        'Action' => 'UPSERT',
                        'ResourceRecordSet' => [
                            'Name' => $name,
                            'ResourceRecords' => [[
                                'Value' => $value
                            ]],
                            'TTL' => 300,
                            'Type' => $type,
                        ]
                    ]],
                    'Comment' => "Updating route 53 {$type} record",
                ],
                'HostedZoneId' => $parentZoneId,
            ]);

            $result['success'] = true;
        } catch(\Exception $ex) {
            $result['success'] = false;
            $result['error'] = $ex->getMessage();
        }

        return $result;
    }

}
