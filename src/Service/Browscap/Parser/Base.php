<?php
declare(strict_types=1);

namespace BlueSky\Framework\Service\Browscap\Parser;

use BlueSky\Framework\Service\Browscap\Browscap as SbBrowscap;

abstract class Base
{

    public function parse(string $userAgent, SbBrowscap $sbc): void
    {

    }

    abstract function update(bool $forceUpdate = false, bool $useHardcodedFallback = false): bool;

}