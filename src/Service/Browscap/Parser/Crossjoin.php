<?php
namespace BlueSky\Framework\Service\Browscap\Parser;

use Crossjoin\Browscap\Browscap as CrossjoinBrowscap;
use Crossjoin\Browscap\Parser\Sqlite\Parser as CrossjoinSqliteParser;
use \Crossjoin\Browscap\Formatter\Optimized as OptimizedFormatter;
use BlueSky\Framework\Service\Browscap\Browscap as SbBrowscap;
use BlueSky\Framework\State\Cli;
use BlueSky\Framework\Util\Dir;

class Crossjoin extends Base
{

    /**
     * @var bool $isHardcodedMode - download the file for updates from a hardcoded path
     */
    private $staticBucketName = 'sz-deploy';

    /**
     * Get the cache path
     * @access private
     * @return string
     * */
    private function getCachePath()
    {
        return LOTE_ASSET_PATH . 'res/meta/crossjoin';
    }

    public function parse(string $userAgent, SbBrowscap $sbc): void
    {
        $browscap = new CrossjoinBrowscap();
        $parser = new CrossjoinSqliteParser($this->getCachePath());
        $browscap->setParser($parser);
        $optimizedFormatter = new OptimizedFormatter();
        $browscap->setFormatter($optimizedFormatter);
        $details = $browscap->getBrowser($userAgent);

        if (isset($details->Browser)) {
            $sbc->browser = $details->Browser;
        }
        if (isset($details->Version)) {
            $sbc->browserVersion = $details->Version;
        }
        if (isset($details->Device_Type)) {
            $sbc->deviceType = $details->Device_Type;
        }
        if (isset($details->Platform)) {
            $sbc->platform = $details->Platform;
        }
        if (isset($details->platform_version)) {
            $sbc->platformVersion = $details->platform_version;
        }
        if (isset($details->platform_description)) {
            $sbc->platformDescription = $details->platform_description;
        }
        if (isset($details->isMobileDevice)) {
            $sbc->isMobileDevice = $details->isMobileDevice;
        }
        if (isset($details->isTablet)) {
            $sbc->isTablet = $details->isTablet;
        }
    }

    public function update(bool $forceUpdate = false, bool $useHardcodedFallback = false): bool
    {
        if($useHardcodedFallback) {
            $result = $this->updateFromHardcoded($forceUpdate);
        }
        else {
            $result = $this->updateThroughCrossjoin($forceUpdate);
        }
        return $result;
    }

    /**
     * Run and update of the data for the crossjoin browscap by downloading harcoded files
     * @access public
     * @param bool $forceUpdate
     * @return bool
     * */
    public function updateFromHardcoded($forceUpdate) {
        $s = new Cli(BROWSCAP_FALLBACK_HARDCODED_ACCOUNT_REF);
        if ($s->getSettings()->get('media.adapter.aws_key', false)) {
            $browcapDir = $this->getCachePath().'/browscap/sqlite/';
            Dir::make($browcapDir);
            $s3Client = new \Aws\S3\S3Client([
                'credentials' => [
                    'key' => $s->getSettings()->get('media.adapter.aws_key'),
                    'secret' => $s->getSettings()->get('media.adapter.aws_secret')
                ],
                'region' => $s->getSettings()->getSettingOrConfig("media.adapter.aws_region", 'ap-southeast-2'),
                'version' => 'latest',
                'scheme' => 'http'
            ]);

            $adapter = new \League\Flysystem\AwsS3v3\AwsS3Adapter($s3Client, $this->staticBucketName);
            $fileSystem = new \League\Flysystem\Filesystem($adapter);
            $contents = $fileSystem->listContents('crossjoin/browscap/sqlite');
            foreach($contents as $file) {
                $data = $fileSystem->read($file['path']);
                file_put_contents($browcapDir.$file['basename'], $data);
            }
        }
    }

    /**
     * Run and update of the data for the crossjoin browscap through crossjoin itslef
     * @access public
     * @param bool $forceUpdate
     * @return bool
     * */
    public function updateThroughCrossjoin($forceUpdate) {
        ini_set('memory_limit', '2056M');
        ini_set('max_execution_time', '1200');
        Dir::make($this->getCachePath());
        $browscap = new CrossjoinBrowscap();
        $parser = new CrossjoinSqliteParser($this->getCachePath());
        $browscap->setParser($parser);
        return $browscap->update($forceUpdate);
    }

}
