<?php
namespace BlueSky\Framework\Service\Browscap;

use BlueSky\Framework\Service\Browscap\Parser\Crossjoin;
use BlueSky\Framework\Service\Browscap\Parser\Base as BaseParser;

class Browscap
{

    /**
     * @var string $parser
     */
    private $parser;

    /**
     * @var string $browser
     */
    public $browser;

    /**
     * @var string $version
     */
    public $browserVersion;

    /**
     * @var string $deviceType
     */
    public $deviceType;

    /**
     * @var string $platform
     */
    public $platform;

    /**
     * @var string $platformVersion
     */
    public $platformVersion;

    /**
     * @var string $platformDescription
     */
    public $platformDescription;

    /**
     * @var boolean $isMobileDevice
     */
    public $isMobileDevice;

    /**
     * @var boolean $isTablet
     */
    public $isTablet;

    public function __construct(bool $parser = false)
    {
        $this->parser = $parser;
        if (is_array($_SERVER) && isset($_SERVER['HTTP_USER_AGENT'])) {
            $userAgent = $_SERVER['HTTP_USER_AGENT'];
            $this->getParser()->parse($userAgent, $this);
        }
    }

    public function parse(string $userAgent): void
    {
        $this->getParser()->parse($userAgent, $this);
    }

    /**
     * Run and update of the data for the crossjoin browscap
     * @access public
     * @param bool $forceUpdate
     * @param bool $useHardcodedFallback
     * @return bool
     * */
    public function runUpdate($forceUpdate = false, $useHardcodedFallback = false)
    {
        $this->getParser()->update($forceUpdate, $useHardcodedFallback);
    }

    /**
     * Get the parser to use in this instance
     * @access private
     * @return BaseParser
     * */
    private function getParser()
    {
        return new Crossjoin();
    }

}
