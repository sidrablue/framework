<?php
declare(strict_types=1);

namespace BlueSky\Framework\Service\Email;

interface EmailInterface
{

    public function sendEmail(string $subject, string $htmlContent, $to, string $from, string $fromName, array $cc = [], array $bcc = [], array $attachments = []);

}
