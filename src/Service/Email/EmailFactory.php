<?php
declare(strict_types=1);

namespace BlueSky\Framework\Service\Email;

use Monolog\Logger;

class EmailFactory
{

    public static function createInstance(string $method, string $apiKey, string $accountReference, Logger $logger = null): ?EmailInterface
    {
        if ($method == 'sendgrid') {
            $class = new SendGrid($apiKey, $accountReference, $logger);
        } elseif ($method == 'sparkpost') {
            $class = new SparkPost($apiKey, $accountReference, $logger);
        } else {
            throw new \Exception("Email method not found");
        }
        return $class;
    }

}
