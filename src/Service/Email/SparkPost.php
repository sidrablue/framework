<?php
declare(strict_types=1);

namespace BlueSky\Framework\Service\Email;

use Http\Adapter\Guzzle6\Client as GuzzleAdapter;
use GuzzleHttp\Client;
use SparkPost\SparkPost as SparkPostBase;

class SparkPost extends BaseSendEmail implements EmailInterface
{

    public function sendEmail(string $subject, string $htmlContent, $to, string $from, string $fromName, array $cc = [], array $bcc = [], array $attachments = []): ?string
    {
        $result = null;
        if ($this->apiKey) {
            $sparkData = [];
            $sparkData['metadata'] = [
                'reference' => $this->accountReference,
                'campaign_id' => 0
            ];

            $options = [];
            $options['start_time'] = 'now';
            $options['open_tracking'] = false;
            $options['click_tracking'] = false;
            $options['sandbox'] = false;
            $options['inline_css'] = false;
            $options['transactional'] = true;
            $sparkData['options'] = $options;

            $content = [];
            $content['from']['name'] = $fromName;
            $content['from']['email'] = $from;
            $content['subject'] = $subject;

            $content['html'] = $this->escapeBrackets($htmlContent);

            //Create and add attachments
            if (!empty($attachments)) {
                foreach ($attachments as $attachment) {
                    if (is_array($attachment)) {
                        if (isset($attachment['name']) && isset($attachment['type']) && isset($attachment['content'])) {
                            $content['attachments'][] = [
                                'type' => $attachment['type'],
                                'name' => $attachment['name'],
                                'data' => base64_encode($attachment['content'])
                            ];

                        }
                    }
                }
            }

            $sparkData['content'] = $content;

            $recipients = [];
            if (is_string($to)) {
                $to = $this->formatEmailToAddress($to);

                if (!empty($to)) {
                    foreach ($to as $toEmail) {
                        if (filter_var($toEmail, FILTER_VALIDATE_EMAIL)) {
                            $recipient = ['address'];
                            $recipient['address'] = ['email' => $toEmail];
                            $recipients[] = $recipient;
                        }
                    }
                }

            } elseif (is_array($to)) {
                foreach ($to as $k => $v) {
                    if(is_string($k)) {
                        $k = trim($k);
                    }
                    $v = trim($v);
                    $recipient = ['address'];
                    if (filter_var($k, FILTER_VALIDATE_EMAIL)) {
                        $recipient['address']['email'] = $k;
                        $recipient['address']['name'] = $v;
                    } elseif (filter_var($v, FILTER_VALIDATE_EMAIL)) {
                        $recipient['address']['email'] = $v;
                    } else {
                        continue;
                    }
                    $recipients[] = $recipient;
                }
            }
            $sparkData['recipients'] = $recipients;

            $httpAdapter = new GuzzleAdapter(new Client(['verify' => false]));
            $sparky = new SparkPostBase($httpAdapter, ['key' => $this->apiKey]);
            $sparky->setOptions(['async' => false]);
            try {
                $transmission = $sparky->transmissions->post($sparkData);
                $responseCode = $transmission->getStatusCode();
                $responseData = $transmission->getBody();
                if ($responseCode == '200' && isset($responseData['results']) && isset($responseData['results']['id'])) {
                    $result = (string) $responseData['results']['id'];
                }
            } catch (\Exception $e) {
                $this->logger->warn("Message not sent", ['code' => $e->getCode(), 'message' => $e->getMessage(), 'sparkpost_data' => $sparkData]);
            }
        }
        return $result;
    }

    public function escapeBrackets($content)
    {
        $key = uniqid("###");
        $content = str_replace('{{', $key . ' opening_double_curly() ' . $key, $content);
        $content = str_replace('}}', $key . ' closing_double_curly() ' . $key, $content);
        $content = str_replace('{{{', $key . ' opening_triple_curly() ' . $key, $content);
        $content = str_replace('}}}', $key . ' closing_triple_curly() ' . $key, $content);

        $content = str_replace($key . ' opening_double_curly() ' . $key, '{{ opening_double_curly() }}', $content);
        $content = str_replace($key . ' closing_double_curly() ' . $key, '{{ closing_double_curly() }}', $content);
        $content = str_replace($key . ' opening_triple_curly() ' . $key, '{{ opening_triple_curly() }}', $content);
        $content = str_replace($key . ' closing_triple_curly() ' . $key, '{{ closing_triple_curly() }}', $content);

        return $content;
    }

}
