<?php
declare(strict_types=1);

namespace BlueSky\Framework\Service\Email;

use SendGrid\Mail\Mail;

class SendGrid extends BaseSendEmail implements EmailInterface
{

    public function sendEmail(string $subject, string $content, $to, string $from, string $fromName, array $cc = [], array $bcc = [], array $attachments = []): ?string
    {
        $result = null;
        if ($this->apiKey) {
            if (is_string($to)) {
                $to = $this->formatEmailToAddress($to);
            }
            $to = $this->formatToAssociativeArr($to);
            $email = new Mail();
            $email->setFrom($from, $fromName);
            $email->setSubject($subject);
            $email->addTos($to);
            $email->addContent('text/html', $content);
            if (!empty($attachments)) {
                $email->addAttachment($attachments);
            }
            if (!empty($bcc)) {
                $bcc = $this->formatToAssociativeArr($bcc);
                $email->addBccs($bcc);
            }
            if (!empty($cc)) {
                $cc = $this->formatToAssociativeArr($cc);
                $email->addBccs($cc);
            }
            $sendGrid = new \SendGrid($this->apiKey);
            try {
                if ($response = $sendGrid->send($email)) {
                    if ($response->statusCode() === 200) {
                        //TODO: TEST 
                        $result = (string) $response->headers()->get('X-Message-Id');
                    }
                }
            } catch (\Exception $e) {
                $this->logger->warn("Message not sent", ['code' => $e->getCode(), 'message' => $e->getMessage(), 'sendgrid_data' => $response]);
            }
        }
        return $result;
    }

}
