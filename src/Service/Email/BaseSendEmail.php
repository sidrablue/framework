<?php
declare(strict_types=1);

namespace BlueSky\Framework\Service\Email;

use Monolog\Logger;

class BaseSendEmail
{

    protected $apiKey = '';
    protected $accountReference = '';
    protected $logger;

    public function __construct(string $apiKey, string $accountReference, Logger $logger)
    {
        $this->apiKey = $apiKey;
        $this->accountReference = $accountReference;
        $this->logger = $logger ?? new Logger("LOTE");
    }

    public function setLogger(Logger $log) {
        $this->logger = $log;
    }

    public function formatEmailToAddress(string $data): array
    {
        $to = [];
        if (is_string($data)) {
            $to = str_replace(' ', '', $data);
            if (strpos($to, ';') > 0) {
                $to = explode(';', $to);
            } elseif (strpos($to, ',') > 0) {
                $to = explode(',', $to);
            } else {
                $to = [$to];
            }
        }
        return $to;
    }

    public function formatToAssociativeArr(array $data): array
    {
        $formattedArr = [];
        foreach ($data as $email) {
            $formattedArr["$email"] = $email;
        }

        return $formattedArr;
    }

}
