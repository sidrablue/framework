<?php
namespace BlueSky\Framework\Service;

use Lote\App\Content\Entity\Snippet;
use Lote\Module\Form\Entity\Form;
use Lote\App\Form\Entity\RecordValue;
use Lote\Module\Page\Model\Item as PageModel;
use Lote\Module\Szschool\Model\Szschool as NewsletterModel;
use Lote\App\Master\Entity\Master\Account;
use Lote\App\Edm\Entity\Campaign\Channel\CampaignEdm;
use Lote\System\Schoolzine\Model\Newsletter as News;
use Lote\System\Schoolzine\Model\Newsletter;
use BlueSky\Framework\Entity\Cf\CfField;
use BlueSky\Framework\Entity\File;
use BlueSky\Framework\Entity\User\User;
use BlueSky\Framework\Model\Url as UrlModel;
use BlueSky\Framework\Object\Data\Field\Factory as FieldFactory;
use BlueSky\Framework\Object\State as BaseState;
use BlueSky\Framework\View\Transform\Html\Service as ContentService;
use BlueSky\Framework\Util\Time;
use BlueSky\Framework\Util\Url as UrlUtil;
use BlueSky\Framework\Util\Xml;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Base class for state debug
 */
class Debug extends BaseState
{

    public function getLogArray($level)
    {
        $result = [];
        $result['type'] = $this->getState()->isCli()?"cli":"web";
        if(!$this->getState()->isCli()) {
            $result['url'] = $this->getState()->getUrl()->getHttpSchemeAndHost().ltrim($this->getState()->getRequest()->getRequestUri(), '/');
        }
        $result['reference'] = $this->getState()->accountReference;
        $result['view'] = $this->getState()->getView()->getDebugData($level);
        return $result;
    }

}
