<?php
namespace BlueSky\Framework\Service\Import;

/**
 * Class Csv
 * @package BlueSky\Framework\Service\Import
 */
class Csv
{
    /**
     * @var array $fieldIndexes
     */
    private $fieldIndexes = [];

    /**
     * @var resource $csvFile
     */
    private $csvFile;

    /**
     * Import csv data from a file, returning an array with data matching the given association
     *
     * @access public
     * @param string $filePath - File location
     * @param array $association - Array describing the header->field relationship of the csv file in the format:
     * array([
     *      {{ HeaderName1 }} => {{ FieldName1 }},
     *      {{ HeaderName2 }} => {{ FieldName2 }},
     *      .
     *      .
     *      {{ HeaderNameX }} => {{ FieldNameX }}
     * ])
     * @param callable $saveFunction - Save function used to save the row value. Function must have "row" as it's first
     * parameter and no other mandatory
     * @param string $error - Pass by reference error message
     * @param array $optionalParams - Optional parameters to pass through to save function
     * @return array|bool
     */
    public function importCSVData($filePath, $association, $saveFunction, &$error, $optionalParams = [])
    {
        if($result = $this->openFile($filePath, $error)) {
            if($result = $this->setHeaderToFieldAssociation($association, $error)) {
                $result = $this->readCSVRows($saveFunction, $error, $optionalParams);
            }
        }
        return $result;
    }

    /**
     * Open the selected csv file by file path
     *
     * @access private
     * @param string $filePath - File location
     * @param string $error - Error message
     * @return boolean
     */
    private function openFile($filePath, &$error)
    {
        $result = false;
        if(preg_match('^.*\.(csv)$^', $filePath)) {
            if (!file_exists($filePath)) {
                $error = 'File not found.\n';
            } else {
                $file = fopen($filePath, 'r');
                if (!$file) {
                    $error = 'Error opening file.\n';
                } else {
                    $this->csvFile = $file;
                    $result = true;
                }
            }
        } else {
            $error = 'File is not a .csv file.\n';
        }
        return $result;
    }

    /**
     * Read and store the header line of the csv file
     *
     * @access private
     * @return boolean
     */
    private function readCSVHeader()
    {
        rewind($this->csvFile);

        $headerRow = fgetcsv($this->csvFile);
        $columnCount = 0;
        $result = false;
        foreach($headerRow as $headerName) {
            $result[$headerName] = $columnCount;
            $columnCount++;
        }
        return $result;
    }

    /**
     * Create and store the header name to field name association.
     *
     * @access private
     * @param array $association - Array describing the header->field relationship of the csv file in the format:
     * array([
     *      {{ HeaderName1 }} => {{ FieldName1 }},
     *      {{ HeaderName2 }} => {{ FieldName2 }},
     *      .
     *      .
     *      {{ HeaderNameX }} => {{ FieldNameX }}
     * ])
     * @param string $error - Error message
     * @return bool
     */
    private function setHeaderToFieldAssociation($association, &$error)
    {
        $columnIndexes = $this->readCSVHeader();
        $result = false;
        if($columnIndexes) {
            $error = '';
            if(is_array($association)) {
                $result = true;
                foreach($association as $headerName => $field) {
                    if(isset($columnIndexes[$headerName])) {
                        $this->fieldIndexes[$columnIndexes[$headerName]] = $field;
                    } else {
                        $result = false;
                        if($error == '') {
                            $error = 'The following errors were found:\n';
                        }
                        $error .= '"' . $headerName . '" not found in CSV header\n';
                    }
                }
            } else {
                $result = false;
                $error = 'Association is empty.\n';
            }
        }
        return $result;
    }

    /**
     * Read and save each row of data from the csv file
     *
     * @access private
     * @param callable $saveFunction - Save function used to save the row values
     * @param string $errorMessage - Pass by reference error message
     * @param array $optionalParams - Optional parameters to pass through to save function
     * @return bool
     */
    private function readCSVRows($saveFunction, &$errorMessage, $optionalParams = [])
    {
        if($result = $this->checkSaveFunction($saveFunction, $errorMessage)) {
            while($csvContent = fgetcsv($this->csvFile)) {
                $row = [];
                foreach($this->fieldIndexes as $index=>$field) {
                    if(isset($csvContent[$index])){
                        $row[$field] = $csvContent[$index];
                    }
                }

                if($optionalParams && count($optionalParams) < $result) {
//                    //Use when converted to php5.6+
//                    $saveFunction($row, ...$optionalParams);

                    $params = array_merge([0 => $row], $optionalParams);
                    call_user_func_array($saveFunction, $params);
                } else {
                    $saveFunction($row);
                }
            }
        }
        return $result;
    }

    /**
     * Inspect given save function to assert whether or not it has been provided in the correct format
     *
     * @access private
     * @param callable $saveFunction - Function having it's validity checked
     * @param string $errorMessage - Pass by reference error message
     * @return int
     */
    private function checkSaveFunction($saveFunction, &$errorMessage)
    {
        $result = 1;
        $rf = new \ReflectionFunction($saveFunction);
        $params = $rf->getParameters();
        if(isset($params[0])) {
            if($params[0]->getName() == 'row') {
                foreach ($params as $index => $param) {
                    if($param->getName() != 'row' && $index == 0) {
                        $result = 0;
                    } elseif($index > 0) {
                        if(!$param->isOptional()) {
                            $result = 0;
                            $errorMessage = 'Illegal arguments to save function:"'.$param->getName().'" cannot be mandatory.';
                        }
                    }
                    $result = $result ? $index + 1 : $result;
                }
            } else {
                $result = 0;
                $errorMessage = 'Illegal arguments to save function: "$row" must be the first argument.';
            }
        } else {
            $result = 0;
            $errorMessage = 'Illegal arguments to save function: Function must have at least 1 argument ($row)';
        }
        return $result;
    }
}