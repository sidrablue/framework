<?php
namespace BlueSky\Framework\Service;

use BlueSky\Framework\Entity\File;
use BlueSky\Framework\Object\State;
use BlueSky\Framework\Util\Time;

class ImageServer extends State
{

    /**
     * Check if the image server is enabled
     * @access public
     * @return boolean
     */
    public function isEnabled()
    {
        return $this->getState()->getSettings()->getSettingOrConfig("media.cache.imageserver.enabled", false) == '1';
    }

    /**
     * Get the image server host
     * @access public
     * @param string|null $mod - A string to use as a mod
     * @return string
     * */
    public function getHost($mod = null)
    {
        if(!$result = $this->getServersValue($mod, 'host')) {
            $result = $this->getState()->getSettings()->getConfigOrSetting("media.cache.imageserver.host", $this->getState()->getRequest()->getHttpHost());
        }
        return str_replace("%%reference%%", $this->getState()->accountReference, $result);
    }

    /**
     * Get a value from a servers array, if multiple image servers are defined
     * @param string|null $mod - A string to use as a mod
     * @param string $key - the key of the servers value that we are after, e.g. 'host' or 'scheme'
     * @return string|false
     * */
    private function getServersValue($mod, $key)
    {
        $result = false;
        if($servers = $this->getState()->getSettings()->getConfigOrSetting("media.cache.imageserver.servers", false)) {
            if($mod) {
                $offset = abs(crc32($mod) % count($servers));
                $result = $servers[$offset][$key];
            }
            else {
                $result = $servers[0][$key];
            }
        }
        return $result;
    }

    /**
     * Get the image server scheme
     * @access public
     * @param string|null $mod - A string to use as a mod
     * @return string
     * */
    public function getScheme($mod = null)
    {
        if (!$result = $this->getServersValue($mod, 'scheme')) {
            $result = $this->getState()->getSettings()->getConfigOrSetting("media.cache.imageserver.scheme",
                $this->getState()->getRequest()->getScheme());
        }
        return $result;
    }

    /**
     * Get the image server parts of a url, containing an array of http parts such as host, scheme, path and query
     * @param $reference
     * @param $width
     * @param $height
     * @param $size
     * @param $timestamp
     * @param $createPreviewKey
     * @return array
     */
    public function getImageUrlParts($reference, $width = false, $height = false, $size = false, $timestamp = false, $createPreviewKey = false) {
        $result = [];
        $result['host'] = $this->getHost($reference);
        $result['scheme'] = $this->getScheme($reference);
        $f = new File($this->getState());
        $f->loadByReference($reference);
        $result['path'] = '_image/' . $f->location_reference;
        if ($width || $height || $timestamp || $size || $createPreviewKey) {
            $paramVars = [];
            if ($width) {
                $paramVars['width'] = $width;
            }
            if ($height) {
                $paramVars['height'] = $height;
            }
            if ($size) {
                $paramVars['size'] = $size;
            }
            if ($timestamp) {
                $paramVars['ts'] = $timestamp;
            }
           /* if($createPreviewKey && $this->getState()->getRedisDbClient()->isEnabled()) {
                $uniqueId = uniqid();
                $this->getState()->getRedisDbClient()->set("key:sb__file:preview:".$uniqueId, Time::getUtcNow(), '7200');
                $paramVars['filePreviewKey'] = $uniqueId;
            }*/
            $result['query'] = http_build_query($paramVars);
        }
        return $result;
    }

    /**
     * Get the image server url of an image
     * @param $reference
     * @param $width
     * @param $height
     * @param $size
     * @param $timestamp
     * @return array
     */
    public function getImageUrl($reference, $width = false, $height = false, $size = false, $timestamp = false) {
        return http_build_url($this->getImageUrlParts($reference, $width, $height, $size, $timestamp));
    }

}
