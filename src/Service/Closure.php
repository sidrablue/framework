<?php
namespace BlueSky\Framework\Service;

/**
 * Class for generic data
 */
class Closure
{

    /**
     * @var array $dataClosures
     * */
    private $dataClosures;

    /**
     * @var array $eventClosures
     * */
    private $eventClosures;

    /**
     * Set a specific value for an item of data within a given scope
     * @access public
     * @param array $keys - the array of app keys such as ['admin', 'content'] ['form', 'data'] etc
     * @param \Closure $closure = the value of the object
     * */
    public function setDataClosure(array $keys, \Closure $closure)
    {
        $this->dataClosures[$this->makeKey($keys)] = $closure;
    }

    /**
     * Make a key from an array of references
     * @access private
     * @param array $keys
     * @return string
     * */
    private function makeKey($keys)
    {
        return implode("__", $keys);
    }

    /**
     * Set a specific value for an item of data within a given scope
     * @access public
     * @param array $keys - the array of app keys such as ['admin', 'content'] ['form', 'data'] etc
     * @param \Closure $closure = the value of the object
     * */
    public function setEventClosure(array $keys, \Closure $closure)
    {
        $this->eventClosures[$this->makeKey($keys)][] = $closure;
    }

    /**
     * Set a specific value for an item of data within a given scope
     * @access public
     * @param array $keys - the array of app keys such as ['admin', 'content'] ['form', 'data'] etc
     * @return \Closure|null
     */
    public function getDataClosure(array $keys)
    {
        $result = null;
        if (isset($this->dataClosures[$this->makeKey($keys)])) {
            $result = $this->dataClosures[$this->makeKey($keys)];
        }
        return $result;
    }

    /**
     * Set a specific value for an item of data within a given scope
     * @access public
     * @param array $keys - the array of app keys such as ['admin', 'content'] ['form', 'data'] etc
     * @return array|null - array of of \Closure calls or null
     */
    public function getEventClosure(array $keys)
    {
        $result = null;
        if (isset($this->eventClosures[$this->makeKey($keys)]) && count($this->eventClosures[$this->makeKey($keys)]) > 0) {
            $result = $this->eventClosures[$this->makeKey($keys)];
        }
        return $result;
    }

    /**
     * Check if a specified data value exists
     * @param array $keys - the array of app keys such as ['admin', 'content'] ['form', 'data'] etc
     * @return boolean
     * */
    public function hasDataClosure(array $keys)
    {
        return isset($this->dataClosures[$this->makeKey($keys)]) && $this->dataClosures[$this->makeKey($keys)] instanceof \Closure;
    }

    /**
     * Check if a specified data value exists
     * @param array $keys - the array of app keys such as ['admin', 'content'] ['form', 'data'] etc
     * @return boolean
     * */
    public function hasEventClosures(array $keys)
    {
        return isset($this->eventClosures[$this->makeKey($keys)]) && count($this->eventClosures[$this->makeKey($keys)]) > 0;
    }

}
