<?php
namespace BlueSky\Framework\Service;

use BlueSky\Framework\Entity\Audit\FieldLog;
use BlueSky\Framework\Entity\Audit\LinkLog;
use BlueSky\Framework\Entity\Audit\Log;
use BlueSky\Framework\Entity\Audit\ObjectLog;
use BlueSky\Framework\Object\State;
use BlueSky\Framework\Object\Entity\Base as BaseEntity;

class LotePath extends State
{

    /**
     * Generate a path string made up of of the path ID's indicating the objects location
     *
     * @access public
     * @param string $tableName
     * @param string $parentFieldName
     * @param int $id - the id
     * @param String $path - the path
     * @return string
     * */
    public function getLotePath($tableName, $parentFieldName, $id, $path)
    {
        $q = $this->getState()->getWriteDb()->createQueryBuilder();
        $q->select("*")->from($tableName, 't')->where("id = :id")->setParameter("id", $id);
        $data = $q->execute()->fetch(\PDO::FETCH_ASSOC);

        if (isset($data[$parentFieldName]) && $data[$parentFieldName] != 0) {
            $parentId = $data[$parentFieldName];
            return $this->getLotePath($tableName, $parentFieldName, $parentId, '/' . $parentId . $path);
        } else {
            return $path;
        }
    }

    /**
     * Fix the lote path for a table in tehe system
     * @access public
     * @param $tableName
     * @param string $parentFieldName
     */
    public function fixLotePath($tableName, $parentFieldName = 'parent_id')
    {
        $q = $this->getState()->getReadDb()->createQueryBuilder();
        $q->select('f.id')
            ->from($tableName, 'f')
            ->where('f.lote_deleted is null');
        $folders = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);

        // For each, getFolderPath, fix
        foreach($folders as $folder) {
            $lotePath = $this->getLotePath($tableName, $parentFieldName, $folder['id'], '/');
            $this->getState()->getWriteDb()->update($tableName, ['lote_path' => $lotePath], ['id' => $folder['id']]);
        }
    }

}
