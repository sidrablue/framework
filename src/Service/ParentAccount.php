<?php
namespace BlueSky\Framework\Service;

use BlueSky\Framework\Object\State as BaseState;

/**
 * Base class for all models
 */
class ParentAccount extends BaseState
{

    private $parentAccountData = false;

    /**
     * Get the parent account's URL
     *
     * @param string $scheme
     * @return string
     */
    public function getUrl($scheme = "http")
    {
        $result = '';
        $this->loadAccount();
        if($this->parentAccountData) {
            $result = $scheme.'://' . $this->parentAccountData['domain'] . '/';
        }
        return $result;
    }

    public function getReference()
    {
        $result = '';
        $this->loadAccount();
        if($this->parentAccountData) {
            $result = $this->parentAccountData['reference'];
        }
        return $result;
    }

    public function loadAccount()
    {
        if(!$this->parentAccountData) {
            $a = new \Lote\App\MasterBase\Model\Master\Account($this->getState());
            $this->parentAccountData = $a->getParentAccount($this->getState()->getSettings()->get('system.reference'));
        }
    }

    public function loadSuperParentAccount()
    {
        if(!$this->parentAccountData) {
            $a = new \Lote\App\MasterBase\Model\Master\Account($this->getState());
            $this->parentAccountData = $a->getSuperParentAccount($this->getState()->getSettings()->get('system.reference'));
        }
    }

}
