<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace BlueSky\Framework\Service;

use Contao\ImagineSvg\Imagine;

/**
 * Thumbnail generation class.
 *
 *  */
class Image
{

    /**
     * Get the image dimensions for an image that is specified by its base keys
     * @access private
     * @param string $imageContent
     * @param string $extension
     * @return array
     * */
    public static function getImageDimensionsByContent($imageContent, $extension = "")
    {
        if ($extension == 'svg' || strpos($imageContent, trim("<?xml version=\"1.0\" encoding=\"utf-8\"")) !== false) {
            $imagine = new Imagine();
            $size = $imagine->load($imageContent)->getSize();
            $result = ['width' => $size->getWidth(), 'height' => $size->getHeight()];
        } else {
            if ($result = getimagesizefromstring($imageContent)) {
                $result = ['width' => $result[0], 'height' => $result[1]];
            }
        }
        return $result;
    }

}
