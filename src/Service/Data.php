<?php
namespace BlueSky\Framework\Service;

/**
 * Class for generic data
 */
class Data
{

    /**
     * @var array $data
     * */
    private $data;

    /**
     * @var array $objects
     * */
    private $objects;

    /**
     * @var array $tagTypes
     * */
    private $tagTypes;

    /**
     * @var array $masterTagTypes
     * */
    private $masterTagTypes;

    /**
     * Set a specific value for an item of data within a given scope
     * @access public
     * @param string $scope - the scope of the data, such as 'tag'
     * @param string $key - the key of the object
     * @param mixed $value = the value of the object
     * */
    public function setValue($scope, $key, $value)
    {
        $this->data[$scope][$key] = $value;
    }

    /**
     * delete a specific value for an item of data within a given scope
     * @access public
     * @param string $scope - the scope of the data, such as 'tag'
     * @param string $key - the key of the object
     * @return void
     * */
    public function deleteValue($scope, $key = '')
    {
        if(isset($this->data[$scope])) {
            if(isset($key)) {
                if(isset($this->data[$scope][$key])) {
                    unset($this->data[$scope][$key]);
                }
            }
            else {
                unset($this->data[$scope]);
            }
        }
    }

    /**
     * Set a specific value for an item of data within a given scope
     * @access public
     * @param string $scope - the scope of the data, such as 'sb__tag'
     * @param bool|string $key - the key of the object
     * @param null|mixed $defaultValue
     * @return mixed
     */
    public function getValue($scope, $key = false, $defaultValue = null)
    {
        $result = $defaultValue;
        if($key) {
            if(isset($this->data[$scope][$key])) {
                $result = $this->data[$scope][$key];
            }
        }
        else {
           if(isset($this->data[$scope])) {
               $result = $this->data[$scope];
           }
        }
        return $result;
    }

    /**
     * Check if a specified data value exists
     * @param string $scope - the scope of the data, such as 'tag'
     * @param bool|string $key - the key of the object
     * @return boolean
     * */
    public function hasValue($scope, $key = false)
    {
        return is_array($this->data) && array_key_exists($scope, $this->data) && ($key == false || array_key_exists($key, $this->data[$scope]));
    }

    /**
     * Add/Append a data element to a given scope
     * @access public
     * @param string $scope - the scope of the data, such as 'tag'
     * @param mixed $value = the value of the object
     * */
    public function addData($scope, $value)
    {
        $this->data[$scope][] = $value;
    }

    /**
     * Overwrite the data for a given scope
     * @access public
     * @param string $scope - the scope of the data, such as 'tag'
     * @param array $value = the value of the object
     * */
    public function resetData($scope, Array $value)
    {
        $this->data[$scope] = $value;
    }

    /**
     * Set an object name for use in the system in areas where object selection is required
     * @access public
     * @param string $reference - the reference of the object
     * @param string $name = the name of the object
     * */
    public function setObjectName($reference, $name)
    {
        $this->objects[$reference] = $name;
    }

    /**
     * Get an object name for use in the system
     * @access public
     * @param string $reference - the reference of the object
     * @param string $default = the value to return if no match is found
     * @return string
     * */
    public function getObjectName($reference, $default)
    {
        $result = $default;
        if(isset($this->objects[$reference])) {
            $result = $this->objects[$reference];
        }
        return $result;
    }

    /**
     * Set an object name for use in the system in areas where object selection is required
     * @access public
     * @param string $reference - the reference of the object
     * @param string $name = the name of the object
     * */
    public function setTagType($reference, $name)
    {
        $this->tagTypes[$reference] = $name;
        asort($this->tagTypes);
    }

    /**
     * Set an object name for use in the system in areas where object selection is required
     * @access public
     * @param string $reference - the reference of the object
     * @param string $name = the name of the object
     * */
    public function setMasterTagType($reference, $name)
    {
        $this->masterTagTypes[$reference] = $name;
        asort($this->masterTagTypes);
    }

    /**
     * Get a tag type name for use in the system
     * @access public
     * @param string $reference - the reference of the object
     * @param string $default = the value to return if no match is found
     * @return string
     * */
    public function getTagTypeName($reference, $default)
    {
        $result = $default;
        if(isset($this->tagTypes[$reference])) {
            $result = $this->tagTypes[$reference];
        }
        return $result;
    }

    /**
     * Get all tag types  name for use in the system
     * @access public
     * @return array
     * */
    public function getAllTagTypes()
    {
        $sitewideData = ['sitewide' => 'Sitewide'];
        $feedeData = ['feed' => 'Feed'];
        $tagTypes = !empty($this->tagTypes) ? $this->tagTypes : [];
        $return = array_merge($sitewideData, $tagTypes);
        $return = array_merge($return, $feedeData);
        return $return;
    }

    /**
     * Get all tag types  name for use in the system
     * @access public
     * @return array
     * */
    public function getAllMasterTagTypes()
    {
        return $this->masterTagTypes;
    }

}
