<?php

namespace BlueSky\Framework\Service;

use BlueSky\Framework\Object\State;
use BlueSky\Framework\State\Cli as CliState;
use BlueSky\Framework\State\Base as BaseState;

class CloudFront extends State
{
    /** @var array $statesList */
    private $statesList = [];

    protected function onCreate()
    {
        parent::onCreate();
        $this->statesList[$this->getState()->accountReference] = $this->getState();
    }

    private function getStateByReference(string $reference): ?BaseState
    {
        if (!array_key_exists($reference, $this->statesList)) {
            try {
                $state = new CliState($reference);
                $this->statesList[$reference] = $state;
            } catch (\Exception $e) {
                $this->statesList[$reference] = null;
            }
        }
        return $this->statesList[$reference];
    }

    public function buildCloudfrontImageUrl(string $url): string
    {
        $result = $url;
        $urlData = parse_url($url);
        if (isset($urlData['path']) && strpos($urlData['path'], '/_image/') === 0) {
            $fileReference = $urlData['path'];
            $fileReference = preg_replace("#^/_image/#", "", $fileReference);
            $hostParts = explode('.', $urlData['host']);
            $accountReference = array_shift($hostParts);
            try {
                $accountState = $this->getStateByReference($accountReference);
                $contentService = new Content($accountState);
                parse_str($urlData['query'] ?? "", $vars);
                $width = (int)($vars['width'] ?? $vars['w'] ?? 0);
                $height = (int)($vars['height'] ?? $vars['h'] ?? 0);
                $size = (int)($vars['size'] ?? $vars['s'] ?? 0);
                $result = $contentService->getImageCachedUrl($fileReference, $width, $height, $size);
            } catch (\Exception $e) {
                //do nothing
            }
        }
        return $result;
    }
}
