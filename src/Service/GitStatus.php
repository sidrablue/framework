<?php
declare(strict_types=1);

namespace BlueSky\Framework\Service;

use Cz\Git\GitException;

/**
 * Git Status wrapper
 */
class GitStatus
{

    private $path = "";

    private $type = "";

    private $branch = "";

    private $changes = false;

    private $valid = false;

    public function __construct($gitPath, $type = "")
    {
        $this->path = $gitPath;
        $this->type = $type;
        $this->setup();
    }

    private function setup(): void
    {
        try {
            $repo = new \Cz\Git\GitRepository($this->path);
            $this->changes = $repo->hasChanges();
            $this->branch = $repo->getCurrentBranchName();
            $this->valid = true;
        } catch (GitException $e) {

        }
    }

    public function getName(): string
    {
        $result = basename($this->type) . '';
        if (strtolower($this->type) == "app") {
            $result .= ": " . basename($this->path);
        }
        return $result;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function isValid(): bool
    {
        return $this->valid;
    }

    public function getBranch(): string
    {
        return $this->branch;
    }

    public function hasChanges(): bool
    {
        return $this->changes;
    }

    public function asArray()
    {
        return [$this->getName(), $this->getBranch(), !$this->hasChanges() ? "" : "Commit Required"];
    }

}
