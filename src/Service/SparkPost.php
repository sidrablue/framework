<?php
namespace BlueSky\Framework\Service;

use BlueSky\Framework\Object\State as BaseState;

class SparkPost extends BaseState
{

    public function escapeBrackets($content) {

        $key = uniqid("###");
        $content = str_replace('{{', $key.' opening_double_curly() '.$key, $content);
        $content = str_replace('}}', $key.' closing_double_curly() '.$key, $content);
        $content = str_replace('{{{', $key.' opening_triple_curly() '.$key, $content);
        $content = str_replace('}}}', $key.' closing_triple_curly() '.$key, $content);

        $content = str_replace($key.' opening_double_curly() '.$key, '{{ opening_double_curly() }}', $content);
        $content = str_replace($key.' closing_double_curly() '.$key, '{{ closing_double_curly() }}', $content);
        $content = str_replace($key.' opening_triple_curly() '.$key, '{{ opening_triple_curly() }}', $content);
        $content = str_replace($key.' closing_triple_curly() '.$key, '{{ closing_triple_curly() }}', $content);

        return $content;
    }

}
