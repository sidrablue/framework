<?php
namespace BlueSky\Framework\Service;

use BlueSky\Framework\Object\State as BaseState;

/**
 * Timer service
 */
class Timer extends BaseState
{

    /**
     * @var array $times
     * */
    private $times = [];

    /**
     * Log an event as occurring
     * @access public
     * @param string $message - the message which to attribute to this time entry
     * @return void
     */
    public function addTime($message = '')
    {
        $this->times[] = ['microtime' => microtime(true), 'message' => $message];
    }

    /**
     * Reset the timer
     * @access public
     * @return void
     * */
    public function reset()
    {
        $this->times = [];
    }

    /**
     * Print the times of this timer log
     * @access public
     * @param bool $return - true to return the times in an array instead of printing them
     * @return array
     * */
    public function printTimes($return = false)
    {
        $result = [];
        if (count($this->times) > 0) {
            $globalStart = $startTime = $this->times[0]['microtime'];
            php_sapi_name() == 'cli' ? $nlStr = "\n" : $nlStr = "<br />";

            foreach ($this->times as $t) {
                $diff = $t['microtime'] - $startTime;
                $globalDiff = $t['microtime'] - $globalStart;
                $str = sprintf('%.3f', $globalDiff).' - '.sprintf('%.3f', $diff);
                if ($t['message']) {
                    $str .= " - {$t['message']}";
                }
                if ($return) {
                    $result[] = $str;
                } else {
                    echo($str . $nlStr);
                }
                $startTime = $t['microtime'];
            }
        }
        return $result;
    }

}
