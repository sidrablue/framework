<?php
declare(strict_types=1);

namespace BlueSky\Framework\Service\User;

use BlueSky\Framework\Entity\User\User as UserEntity;
use BlueSky\Framework\Object\State;
use BlueSky\Framework\Query\DS\DynamicQueryConditionDS;
use BlueSky\Framework\Query\DS\DynamicQueryDS;
use BlueSky\Framework\Query\DynamicQuery;
use BlueSky\Framework\Query\DynamicQueryHelper;

class QueryTotal extends State
{




    public function getTotal(array $dynamicQueryData): int
    {
        return $this->getTotalBase($dynamicQueryData);
    }

    public function getTotalActive(array $dynamicQueryData): int
    {
        return $this->getTotalBase($dynamicQueryData, [$this->getActiveCondition()]);
    }

    public function getSubscribedEmail(array $dynamicQueryData): int
    {
        return $this->getTotalBase($dynamicQueryData, [$this->getEmailSubscribedCondition()]);
    }

    public function getSubscribedActiveEmail(array $dynamicQueryData): int
    {
        return $this->getTotalBase($dynamicQueryData, [$this->getEmailSubscribedCondition(), $this->getActiveCondition()]);
    }

    public function getSubscribedMobile(array $dynamicQueryData): int
    {
        return $this->getTotalBase($dynamicQueryData, [$this->getMobileSubscribedCondition()]);
    }

    public function getSubscribedActiveMobile(array $dynamicQueryData): int
    {
        return $this->getTotalBase($dynamicQueryData, [$this->getMobileSubscribedCondition(), $this->getActiveCondition()]);
    }

    private function getTotalBase(array $dynamicQueryData, array $additionalQueries = []): int
    {
        $dQds = DynamicQueryDS::fromArray($dynamicQueryData);
        $newQueryDS = DynamicQueryHelper::getAsNestedQuery($dQds, 'AND', UserEntity::getObjectRef());
        foreach ($additionalQueries as $q) {
            if ($q instanceof DynamicQueryConditionDS) {
                $newQueryDS->addCondition($q);
            }
        }
        $dq = new DynamicQuery($this->getState());
        $q = $dq->generateQueryBuilder(['clauses' => $newQueryDS->toArray()]);

        $userTableAlias = $dq->getTableNameAlias(UserEntity::TABLE_NAME);

        $q->select("count(*) as cnt")
            ->addGroupBy("{$userTableAlias}.id");
        return (int)$q->execute()->rowCount();
    }

    public function getActiveCondition(): DynamicQueryConditionDS
    {
        $field = $this->getState()->getSchema()->getEntityByReference('User')->getEntityField('is_active');
        return DynamicQueryHelper::generateQueryCondition($field, 'equals', '1');
    }

    public function getEmailSubscribedCondition(): DynamicQueryConditionDS
    {
        $field = $this->getState()->getSchema()->getEntityByReference('UserSubscription')->getEntityField('email');
        return DynamicQueryHelper::generateQueryCondition($field, 'equals', '1');
    }

    public function getEmailNotEmptyCondition(): DynamicQueryConditionDS
    {
        $field = $this->getState()->getSchema()->getEntityByReference('User')->getEntityField('email');
        return DynamicQueryHelper::generateQueryCondition($field, 'is_not_empty');
    }

    public function getMobileSubscribedCondition(): DynamicQueryConditionDS
    {
        $field = $this->getState()->getSchema()->getEntityByReference('UserSubscription')->getEntityField('mobile');
        return DynamicQueryHelper::generateQueryCondition($field, 'equals', '1');
    }

}
