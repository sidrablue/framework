<?php
declare(strict_types=1);

namespace BlueSky\Framework\Service\User;

use BlueSky\Framework\Model\Query\QueryUser;
use BlueSky\Framework\Object\State;
use BlueSky\Framework\Query\DS\DynamicQueryConditionDS;

class QueryUsers extends State
{

    public function getAllSubscribedActiveEdmUserIds(int $queryId): array
    {
        try {
            $queryTotalService = new QueryTotal($this->getState());
            $qum = new QueryUser($this->getState());
            $data = $qum->getQuery($queryId);
            //return $this->getAllBase($data['clauses'], [$queryTotalService->getEmailSubscribedCondition(), $queryTotalService->getActiveCondition(), $queryTotalService->getEmailNotEmptyCondition()]);
            $additionalQueries = [$queryTotalService->getEmailSubscribedCondition(), $queryTotalService->getActiveCondition()];
            $result = $this->getAllBase($data['clauses'], $additionalQueries);
        } catch (\Exception $e) {
            $result = [];
        }
        return $result;
    }


    private function getAllBase(array $dynamicQueryData, array $additionalQueries = []): array
    {
        $result = [];
        $dQds = \BlueSky\Framework\Query\DS\DynamicQueryDS::fromArray($dynamicQueryData);
        $newQueryDS = \BlueSky\Framework\Query\DynamicQueryHelper::getAsNestedQuery($dQds, 'AND',
            \BlueSky\Framework\Entity\User\User::getObjectRef());
        foreach ($additionalQueries as $q) {
            if ($q instanceof DynamicQueryConditionDS) {
                $newQueryDS->addCondition($q);
            }
        }
        $dq = new \BlueSky\Framework\Query\DynamicQuery($this->getState());
        $clauses = ['clauses' => $newQueryDS->toArray()];
        $q = $dq->generateQueryBuilder($clauses);
        $q->select("{$dq->getTableNameAlias("sb__user")}.*");
        $s = $q->execute();
        foreach ($s->fetchAll() as $row) {
            $result[] = $row;
        }
        return $result;
    }

}
