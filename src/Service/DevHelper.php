<?php
namespace BlueSky\Framework\Service;

use Doctrine\DBAL\Query\QueryBuilder;
use BlueSky\Framework\Object\State as BaseState;

class DevHelper extends BaseState
{

    public function getFullSQL(QueryBuilder $q): string
    {
        $sql = $q->getSQL();
        foreach ($q->getParameters() as $key => $value) {
            if (is_numeric($value) || filter_var($value, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) !== null) {
                $sql = str_replace(":$key", $value, $sql);
            } elseif (is_array($value)) {
                if (is_numeric($value[0])) {
                    $param = implode(', ', $value);
                } else {
                    $param = "'" . implode("', '", $value) . "'";
                }
                $sql = str_replace(":$key", $param, $sql);
            } else {
                $sql = str_replace(":$key", "'$value'", $sql);
            }
        }
        return $sql;
    }

}
