<?php
namespace BlueSky\Framework\Service;

use Lote\App\Content\Entity\Snippet;
use Lote\Module\Form\Entity\Form;
use Lote\App\Form\Entity\RecordValue;
use Lote\App\MasterBase\Entity\Master\Account;
use Lote\App\Edm\Entity\Campaign\Channel\CampaignEdm;
use BlueSky\Framework\Entity\File;
use BlueSky\Framework\Entity\User\User;
use BlueSky\Framework\Model\Url as UrlModel;
use BlueSky\Framework\Object\Data\Field\Factory as FieldFactory;
use BlueSky\Framework\Object\State as BaseState;
use BlueSky\Framework\View\Transform\Html\Service as ContentService;
use BlueSky\Framework\Util\Time;
use BlueSky\Framework\Util\Url as UrlUtil;
use BlueSky\Framework\Util\Xml;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Base class for content services
 */
class Content extends BaseState
{

    /**
     * Convert content stored in the DB to HTML, renaming any
     * @access public
     * @param string $content - the content to render
     * @param User $userEntity - the user Entity
     * @param bool $renderRelative - true to render relative URLs
     * @return string
     * */
    public function renderContent($content, $userEntity = null, $renderRelative = false)
    {
        $baseUrl = $this->getBaseUrl();
        $content = $this->replaceUrlWildcard($content, $baseUrl);
        $content = $this->renderPlugins($content, $baseUrl);
        $content = $this->replaceSettingWildcards($content, $baseUrl, $userEntity);
        $content = $this->replaceLoteLinks($content, $baseUrl);
        $content = $this->cacheLocalImages($content, $baseUrl);
        $content = $this->replaceUserWildcards($content, $userEntity);
//      $content = $this->replaceUserCfWildcards($content, $userEntity);
        $content = $this->replaceDateWildcards($content, $userEntity);
        $content = $this->replaceContentWildcards($content, $baseUrl, $userEntity);
        $content = $this->replaceCoreWildcards($content, $baseUrl);
        if ($renderRelative) {
            $content = $this->replaceRelativeUrls($content, $baseUrl);
        }
        return $content;
    }

    /**
     * Convert content stored in the DB to HTML
     * @access public
     * @param string $content - the content to render
     * @param bool $renderRelative - true to render relative URLs
     * @return string
     * */
    public function renderImageContent($content, $renderRelative = false)
    {
        $baseUrl = $this->getBaseUrl();
        $content = $this->replaceLoteLinks($content, $baseUrl);
        if ($renderRelative) {
            $content = $this->replaceRelativeUrls($content, $baseUrl);
        }
        return $content;
    }

    /**
     * Convert content stored in the DB to HTML, renaming any
     * @access public
     * @param string $content - the content to render
     * @param bool $renderRelative - true to convert relative links to full links
     * @param bool $renderCloudfront - true to convert image urls to cloudfront urls
     * @return string
     * */
    public function renderBulkEmailContent($content, $renderRelative = true, $renderCloudfront = true)
    {
        $baseUrl = $this->getBaseUrl();
        $content = $this->replaceUrlWildcard($content, $baseUrl);
        $content = $this->renderPlugins($content, $baseUrl);
        $content = $this->replaceSettingWildcards($content, $baseUrl);
        $content = $this->replaceLoteLinks($content, $baseUrl);
        $content = $this->replaceContentWildcards($content, $baseUrl);
        $content = $this->replaceCoreWildcards($content, $baseUrl);
        $content = $this->replaceDateWildcards($content);
        $content = $this->replaceAccountWildcards($content);
        if ($renderRelative) {
            $content = $this->replaceRelativeUrls($content, $baseUrl);
        }
        if ($renderCloudfront) {
            $content = $this->buildCloudfrontUrls($content, $baseUrl);
        }
        return $content;
    }

    /**
     * Convert notification content
     * @access public
     * @param $content - the content to render
     * @param null $userEntity - the user Entity
     * @param bool $renderRelative - true to render relative URLs
     * @param bool $textOnly
     * @return mixed
     */
    public function renderNotificationContent($content, $userEntity = null, $renderRelative = false, $textOnly = true)
    {
        $baseUrl = $this->getBaseUrl();
        $content = $this->replaceUrlWildcard($content, $baseUrl);
        $content = $this->replaceUserWildcards($content, $userEntity);
        $content = $this->replaceContentWildcards($content, $baseUrl, $userEntity);
        $content = $this->replaceSettingWildcards($content, $baseUrl, $userEntity, $textOnly);
        $content = $this->replaceLoteLinks($content, $baseUrl);

        if (!$renderRelative) {
            $content = $this->replaceRelativeUrls($content, $baseUrl);
        }

        return $content;
    }

    public function renderEmailTemplateContent($content, $userEntity = null, $colorsOnly = false)
    {
        $baseUrl = $this->getBaseUrl();
        if (!$colorsOnly) {
            $content = $this->replaceUrlWildcard($content, $baseUrl);
        }
        $content = $this->replaceSettingWildcards($content, $baseUrl, $userEntity, $colorsOnly);
        return $content;
    }

    /**
     * Render plugins
     * @access private
     * @param string $content - the content to render
     * @param User $userEntity - the user Entity
     * @return string
     * */
    private function renderPlugins($content, $userEntity = null)
    {
        //{plugin,content_tagged,abcd}
        while (preg_match("/\{\!(\s+)?([a-zA-Z0-9\_]+)(\s+)?;(\s+)?([a-zA-Z0-9\_\.]+)(\s+)?(;([a-zA-Z0-9\_\,\.]+)?(\s+)?)?\!\}/",
            $content, $match)) {
            if ($match[2] == 'setting') {
                $default = '';
                if (isset($match[8])) {
                    $default = $match[8];
                }
                $content = str_replace($match[0], $this->getState()->getSettings()->get($match[5], $default), $content);
            } elseif ($match[2] == 'setting_exists') {
                $result = '';

                $default = false;
                if (isset($match[8])) {
                    $default = explode(',', $match[8]);
                }
                $value = $this->getState()->getSettings()->get($match[5]);
                $exists = $value != false && $value !== false;
                if (is_array($default)) {
                    if ($exists && isset($default[0])) {
                        $result = $default[0];
                    } elseif (!$exists && isset($default[1])) {
                        $result = $default[1];
                    }
                }
                $content = str_replace($match[0], $result, $content);
            } else {
                $params = $match[5];
                if (isset($match[8])) {
                    $cnt = count($match);
                    $params = [];
                    for ($i = 5; $i < $cnt; $i += 3) {
                        $params[] = $match[$i];
                    }
                }

                $tempContent = $this->getState()->getPlugins()->renderPlugin($match[2], $params);
                $content = str_replace($match[0], $tempContent, $content);
            }
        }
        return $content;
    }

    /**
     * Replaces the href attributes in <a> tags and adds utm tags as parameters.
     * Should be called after other renders
     * @param string $content
     * @param string $source
     * @param string $channel
     * @param string $campaignId
     * @return mixed
     */
    public function addUTMTags($content, $source, $channel, $campaignId)
    {
        $content = preg_replace_callback("/<a[^>]*>/", function($match) use ($source, $channel, $campaignId) {
            if (preg_match('/href="[^"]*/', $match[0], $m1)) {
                if (strpos($m1[0], '{{{') && strpos($m1[0], '}}}')) {
                    return $match[0];
                }
                $smartName = '';
                preg_match('/title="[^"]*/', $match[0], $smartName);
                $newHref = $this->setupUTMTags($m1[0], $source, $channel, $campaignId, $smartName);

                return preg_replace('/href="[^"]*/', $newHref, $match[0]);
            }
            return $match[0];
        }, $content);

        return $content;
    }

    /**
     * Adds the utm tag to the href attribute.
     * @param string $originalHref
     * @param string $source
     * @param string $channel
     * @param string $campaignId
     * @param string $smartName
     * @return string
     */
    private function setupUTMTags($originalHref, $source, $channel, $campaignId, $smartName)
    {
        $utm = "utm_source=" . $source . "&utm_medium=" . $channel . "&utm_campaign=" . $campaignId;
        if (!empty($smartName)) {
            $utm .= "&utm_content=" . $smartName;
        }

        $paramsSection = preg_replace('/[^?]*/', '', $originalHref);
        $sourceSection = preg_replace('/\?(.*)/', '', $originalHref);

        $paramsLength = strlen($paramsSection);
        if ($paramsLength == 0 || $paramsLength == 1) {
            $paramsSection = "?" . $utm;
        } else {
            $paramsSection .= "&" . $utm;
        }

        return $sourceSection . $paramsSection;
    }

    /**
     * Convert content stored in the DB to HTML, renaming any
     * @access public
     * @param string $content - the content to render
     * @param string $baseUrl - the base URL
     * @param User $userEntity - the user Entity
     * @return string
     * */
    private function replaceContentWildcards($content, $baseUrl, $userEntity = null)
    {
        if (preg_match_all("/%%__content_snippet_(\d+)__%%/", $content, $matches)) {

            foreach ($matches[0] as $k => $v) {
                $id = $matches[1][$k];
                $c = new Snippet($this->getState());
                $c->load($id);
                $skin = $c->skin_id;
                $show = 0;
                $today = date("Y-m-d");
                $today_time = strtotime($today);
                $valid = 1;
                if(!empty($c->date_from)){
                    $start = $c->date_from;
                    $start_time = strtotime($start);
                    if($today_time < $start_time){
                        $valid = 0;
                    }
                }
                if(!empty($c->date_to)){
                    $end = $c->date_to;
                    $end_time = strtotime($end);
                    if($today_time >= $end_time && $c->never_expires != 1 && $c->never_expires != null){
                        $valid = 0;
                    }
                }
                if ($skin != '0' && $skin != null && $valid == 1) {
                    $base = $this->getBaseUrl();
                    $current = $this->getCurrentUrl();
                    if (strpos($current, '/newsletter/') !== false) {
                        $newslettertemp = str_replace($base, '', $current);
                        $newsletterId = str_replace('newsletter/', '', $newslettertemp);
                        $n = new News($this->getState());
                        $n->setDb($this->getState()->getSuperParentDb());
                        $newsletter = $n->getNewsletter($newsletterId);
                        $newsletterSkin = $newsletter['skin_id'];
                        if ($newsletterSkin != 0 && $skin != $newsletterSkin) {
                            $show = 1;
                        }
                    } else {
                        $uri = str_replace($base, '', $current);
                        $u = new UrlModel($this->getState());
                        $urlId = $u->match($uri)['object_id'];
                        $p = new PageModel($this->getState());
                        $pageSkin = $p->get($urlId)['skin_id'];
                        if ($skin != $pageSkin) {
                            $show = 1;
                        }
                    }
                }
                if($valid == 0){
                    $show = 1;
                }
                if ($show == 1) {
                    $content = str_replace($v, "", $content);
                } else {
                    $tempContent = "";
                    if ($c->id && $c->active) {
                        $tempContent = $this->renderContent($c->content, $userEntity);
                    }
                    $content = str_replace($v, $tempContent, $content);
                }
            }
        }
        return $content;
    }

    /**
     * Convert content stored in the DB to HTML, renaming any
     * @access public
     * @param string $content - the content to render
     * @param string $baseUrl - the base URL
     * @param User $userEntity - the user Entity
     * @return string
     * */
    private function replaceSettingWildcards($content, $baseUrl, $userEntity = null, $colorSettingsOnly = false)
    {
        $pattern = "/%%__setting_([a-zA-Z0-9\.\_]+)__%%/";
        if ($colorSettingsOnly) {
            $pattern = "/%%__setting_szschool.color([a-zA-Z0-9\.\_]+)__%%/";
        }
        if (preg_match_all($pattern, $content, $matches)) {
            foreach ($matches[0] as $k => $v) {
                $id = $matches[1][$k];
                if ($colorSettingsOnly) {
                    $id = 'szschool.color' . $matches[1][$k];
                }
                $settingValue = $this->getState()->getSettings()->get($id);
                $content = str_replace($v, $settingValue, $content);
            }
        }
        return $content;
    }

    /**
     * Form data
     * @access public
     * @param string $content - the content to render
     * @return string
     * */
    public function replaceFormDataWildcards($content, $formData)
    {
        if (preg_match_all("/%%__form_data__%%/", $content, $matches)) {
            foreach ($matches[0] as $k => $v) {
                //$id = $matches[1][$k];

                $dataContent = ContentService::renderView(
                    $this->getState(),
                    'form/responder_data',
                    [
                        'formData' => $formData
                    ]
                );
                $content = str_replace("%%__form_data__%%", $dataContent, $content);
            }
        }
        return $content;
    }

    /**
     * Form data
     * @access public
     * @param string $content - the content to render
     * @return string
     * */
    public function replaceAssessmentDataWildcards($content, $formData, $answers)
    {
        if (preg_match_all("/%%__form_assessment_data__%%/", $content, $matches)) {
            foreach ($matches[0] as $k => $v) {
                //$id = $matches[1][$k];

                $dataContent = ContentService::renderView(
                    $this->getState(),
                    'form/assessment_responder_data',
                    [
                        'formData' => $formData,
                        'answers' => $answers
                    ]
                );
                $content = str_replace("%%__form_assessment_data__%%", $dataContent, $content);
            }
        }
        return $content;
    }

    /**
     * Form fields
     * @access public
     * @param string $content - the content to render
     * @return string
     * */
    public function replaceFormFieldsWildcards($content, $recordId)
    {
//        if (preg_match_all("/%%__form_field_(\d+)_([a-zA-Z0-9\.\_]+)__%%/", $content, $matches)) {
        //if (preg_match_all("/%%__form_field_(\d+)_(\w+)__%%/", $content, $matches)) {
        if (preg_match_all("/%%__form_field_(\d+)_([^%]+%?)__%%/", $content, $matches)) {
            foreach ($matches[0] as $k => $v) {
                $fieldId = $matches[1][$k];
                /* @todo Correctly load Custom Field Data */
                $rv = new RecordValue($this->getState());
                $rv->loadByFields([
                    'object_id' => $recordId,
                    'field_id' => $fieldId
                ]);
                $displayValue = '';
                if ($rv->field_id) {
                    $f = new CfField($this->getState());
                    if ($f->load($fieldId)) {
                        $field = FieldFactory::createInstance($this->getState(), $f->field_type);
                        $displayValue = $field->getDisplayValue($rv);
                    }
                }
                $content = str_replace($v, $displayValue, $content);
            }
        }
        return $content;
    }

    /**
     * Convert content stored in the DB to HTML, renaming any
     * @access public
     * @param string $content - the content to render
     * @param string $baseUrl - the base URL
     * @return string
     * */
    private function replaceCoreWildcards($content, $baseUrl)
    {
        if (preg_match_all("/%%__core_([a-zA-Z0-9\.\_]+)__%%/", $content, $matches)) {
            foreach ($matches[0] as $k => $v) {
                $id = $matches[1][$k];
                if ($id == 'baseUrl') {
                    $content = str_replace($v, $baseUrl, $content);
                }
            }
        }
        return $content;
    }

    public function replaceUserWildcards(string $content, ?User $userEntity): string
    {
        if ($userEntity && $userEntity->id) {
            $replacementWildcards = $userEntity->getWildcardValues(['user', 'custom_field']);
            $content = str_ireplace(array_keys($replacementWildcards), array_values($replacementWildcards), $content);
        }
        return $content;
    }

    /**
     * Convert content stored in the DB to HTML, renaming any
     * @access public
     * @param string $content - the content to render
     * @param User $userEntity - the user Entity
     * @return string
     * */
    public function replaceUserCfWildcards($content, $userEntity)
    {
        if ($userEntity && $userEntity->id) {
            /*$customFields = $userEntity->getCustomFieldValues();
            foreach($customFields as $k => $v) {
                $wildcards["%%__cf_$k%%"] = $v;
                $content = str_ireplace("%%__cf_$k%%", $v, $content);
            }*/
        }
        return $content;
    }

    /**
     * Convert content stored in the DB to HTML, renaming any
     * @access public
     * @param string $content - the content to render
     * @param User $userEntity - the user Entity
     * @return string
     * */
    public function replaceDateWildcards($content, $userEntity = null)
    {
        if (!$userEntity) {
            $userEntity = new User($this->getState());
        }
        $replacementWildcards = $userEntity->getWildcardValues(['date']);
        $content = str_replace(array_keys($replacementWildcards), array_values($replacementWildcards), $content);
        return $content;
    }

    /**
     * Convert content stored in the DB to HTML, renaming any
     * @access public
     * @param string $content - the content to render
     * @return string
     * */
    public function replaceAccountWildcards($content)
    {
        $wildcard = new Wildcard($this->getState());
        $accountWildcards = $wildcard->getAccountWildcards();
        $a = new Account($this->getState());
        if ($a->loadByField('reference', $this->getState()->getSettings()->get('system.reference'))) {
            // Load Account data
            $data = $a->getData();
            // Create website wildcard variants, including a replacement for the originals (e.g. website, website_secondary)
            foreach (['website', 'website_secondary'] as $websiteProp) {
                $host = '';
                $url = '';
                if (!empty($data[$websiteProp])) {
                    $urlParts = parse_url($data[$websiteProp]);
                    if (isset($urlParts['host'])) {
                        $host = $urlParts['host'];
                        $url = $urlParts['scheme'] . "://" . $host;
                        if(isset($urlParts['path'])) {
                            $url .= $urlParts['path'];
                        }
                    } elseif (isset($urlParts['path'])) {
                        //$host = explode('/', $urlParts['path'])[0];
                        $url = "http://" . $urlParts['path'];
                    }
                }
                $data["{$websiteProp}"] = $url;
                $data["{$websiteProp}_html_link"] = "<a href='$url'>$host</a>";
                $data["{$websiteProp}_display"] = $host;
            }

            // Map wildcards to values
            foreach ($accountWildcards as $k => $v) {
                $k2 = str_replace("%%__account_", '', $k);
                $k2 = str_replace("__%%", '', $k2);
                if (isset($data[$k2])) {
                    $accountWildcards[$k] = $data[$k2];
                } else {
                    $accountWildcards[$k] = '';
                }
            }
            return str_ireplace(array_keys($accountWildcards), array_values($accountWildcards), $content);
        }
        return $content;
    }

    private function replaceLoteLinks($content, $baseUrl)
    {
        $dom = new \DOMDocument('1.0', 'UTF-8');
        @$dom->loadHTML($content);
        $loteLinks = $dom->getElementsByTagName('lotelink');
        while ($loteLinks->length > 0) {
            /** @var \DomElement $v */
            $v = $loteLinks->item(0);
            if ($v->hasAttribute('src')) {
                $url = $v->getAttribute('src');
                $urlData = parse_url($url);
                if (isset($urlData['path'])) {
                    while (strpos($urlData['path'], '../') === 0) {
                        $urlData['path'] = substr($urlData['path'], 3);
                    }
                }
                $v->setAttribute('src', http_build_url($urlData));
            } elseif ($v->hasAttribute('href')) {
                $url = $v->getAttribute('href');
                $urlData = parse_url($url);
                if (isset($urlData['path'])) {
                    while (strpos($urlData['path'], '../') === 0) {
                        $urlData['path'] = substr($urlData['path'], 3);
                    }
                }
                $v->setAttribute('href', http_build_url($urlData));
            } elseif ($v->hasAttribute('type')) {
                $type = $v->getAttribute('type');
                if ($type == 'img') {
                    $v->setAttribute('src', '_img/' . $v->getAttribute('lote_id'));
                }
            }
            $this->convertAndCleanLoteLinkElement($v);
            $content = $v->ownerDocument->saveHTML();
            $loteLinks = $dom->getElementsByTagName('lotelink');
        }
        return $content;
    }


    /**
     * @return string
     * */
    private function getImageParameterOrAttribute(\DomElement $imageElement, $key, $defaultValue = null)
    {
        $result = $defaultValue;
        if ($imageElement->hasAttribute($key)) {
            $result = $imageElement->getAttribute($key);
        } elseif ($imageElement->hasAttribute("src")) {
            $imgUrl = parse_url($imageElement->getAttribute("src"));
            if (isset($imgUrl['query'])) {
                parse_str($imgUrl['query'], $vars);
                if (isset($vars[$key])) {
                    $result = $vars[$key];
                }
            }
        }
        return $result;
    }

    public function cacheLocalImages($content, $baseUrl = '', $tagName = 'img', $tagAttr = 'src')
    {
        $lightboxSupportRequired = false;
        if ($this->getState()->getSettings()->get("media.cache.enabled", false)) {
            if(!$baseUrl) {
                $baseUrl = $this->getBaseUrl();
            }
            $dom = new \DOMDocument('1.0', 'UTF-8');
            $content = mb_convert_encoding($content, 'HTML-ENTITIES', 'UTF-8');
            @$dom->loadHTML($content);
            $imageLinks = $dom->getElementsByTagName($tagName);

            $lightboxSupportRequired = true;

            foreach ($imageLinks as $v) {
                /** @var \DomElement $v */
                if ($v->getAttribute($tagAttr)) {
                    $url = $v->getAttribute($tagAttr);
                    if ($this->isLocalUrl($url, $baseUrl)) {

                        if (strpos($url, '/_image/') === 0) {
                            $url = $this->getState()->getRequest()->getSchemeAndHttpHost() . $url;
                        } elseif ($match = preg_match("#/_img/(\d+)(.*)#", $url, $result)) {
                            $f = new File($this->getState());
                            if ($f->load($result[1])) {
                                $url = $this->getState()->getRequest()->getSchemeAndHttpHost() . '/_image/' . $f->reference . $result[2];
                            }
                        }
                        $imgUrlData = parse_url($url);
                        $baseUrlData = parse_url($baseUrl);
                        if (isset($imgUrlData['host']) && isset($baseUrlData['host']) && strtolower($imgUrlData['host']) == strtolower($baseUrlData['host'])) {
                            $reference = str_replace("/_image/", '', $imgUrlData['path']);
                            $reference = urldecode($reference);
                            $width = $this->getImageParameterOrAttribute($v, 'width', 0);
                            if (!is_numeric($width) || $width < 0) {
                                $width = 0;
                            }
                            $height = $this->getImageParameterOrAttribute($v, 'height', 0);
                            if (!is_numeric($height) || $height < 0) {
                                $height = 0;
                            }
                            if ($newSrc = $this->getImageCachedUrl($reference, $width, $height, null, null, true)) {
                                $v->setAttribute($tagAttr, $newSrc);
                                $content = $v->ownerDocument->saveHTML();
                            }

                        }
                    }
                }
            }
        }

/*        if ($lightboxSupportRequired) {
            if (strpos('<head>', $content) > 0) {
                $content = str_replace_first('<head>', '<head><link rel="stylesheet" type="text/css" href="' . $this->getState()->getRequest()->getSchemeAndHttpHost() . '/theme/website/szschool/css/lightbox.css">', $content);
            } else {
                $content = str_replace_first('<html>', '<html><head><link rel="stylesheet" type="text/css" href="' . $this->getState()->getRequest()->getSchemeAndHttpHost() . '/theme/website/szschool/css/lightbox.css"></head>' . "\n", $content);
            }
            $content = str_replace_first('</body>', '<script src="' . $this->getState()->getRequest()->getSchemeAndHttpHost() . '/theme/website/szschool/js/lightbox.js"></script></body>', $content);
        }*/

        return $content;
    }

    /**
     * Determine if a specified URL is local or if it is referencing a remote site.
     *
     *
     * @access private
     * @param string $urlToCheck
     * @param string $baseUrl
     * @return boolean
     * */
    private function isLocalUrl($urlToCheck, $baseUrl)
    {
        $urlDataToCheck = parse_url($urlToCheck);
        $baseUrlData = parse_url($baseUrl);
        if($this->getState()->isCli()) {
            $currentHttpHost = $this->getState()->getUrl()->getBaseUrl();
        }
        else {
            $currentHttpHost = $this->getState()->getRequest()->getHttpHost();
        }

        $isLocalHost = !isset($urlDataToCheck['host']) || $urlDataToCheck['host']==$currentHttpHost || $urlDataToCheck['host'] == $baseUrlData['host'];
        $isSystemImage = strpos($urlDataToCheck['path'],'/_image/')===0 || strpos($urlDataToCheck['path'],'/_img/') ===0;

        return $isLocalHost && $isSystemImage;
    }

    public function getImageCachedUrl($reference, $width = 0, $height = 0, $size = 0, $mode = 'auto', $appendTs = false, $createPreviewKey = false)
    {
        $timestamp = false;
        $e = new File($this->getState());
        if (is_numeric($reference) || $appendTs) {
            if ($this->getState()->getData()->hasValue("entity:sb__file", $reference)) {
                $e->setData($this->getState()->getData()->getValue("entity:sb__file", $reference));
            }
            if (!$e->id) {
                if (is_numeric($reference)) {
                    $e->load($reference);
                } else {
                    if (!$e->loadByReference($reference)) {
                        $e->loadByLocationReference($reference);
                    }
                }
            }
            if ($appendTs) {
                if ($e->lote_updated instanceof \DateTime) {
                    $timestamp = $e->lote_updated->getTimestamp();
                } else {
                    $timestamp = Time::dateToTimestamp($e->lote_updated);
                }
            }
            $reference = $e->reference;
        } elseif ($this->getState()->getSettings()->get("media.cache.adapter") == 'cloudfront-lambda') {
            if (!$e->loadByReference($reference)) {
                $e->loadByLocationReference($reference);
            }
        }

        if ($e->id && $e->is_public && $this->getState()->getSettings()->get("media.cache.adapter") == 'cloudfront-lambda') {
            $result = $this->getCloudFrontLambdaUrl($e, $width, $height, $size);
        } elseif ($this->getState()->getImageServer()->isEnabled() && $this->getState()->getSettings()->getSettingOrConfig("media.adapter") == 'aws') {
            $result = $this->getState()->getImageServer()->getImageUrlParts($reference, $width, $height, $size, //@todo - delete this option
                $timestamp, $createPreviewKey);
        } else {
            $result = $this->getLocalCacheUrl($reference, $width, $height, $size, $timestamp, $createPreviewKey);
        }

        if (is_array($result)) {
            $result = http_build_url($result);
        }
        return $result;
    }

    private function getCloudFrontLambdaUrl(File $e, $width, $height, $size)
    {
        $result = "https://" . $this->getState()->getSettings()->getSettingOrConfig("media.cache.cloudfront.host");

        $fileImageEntity = new FileImage($this->getState());
        $fileImageEntity->loadByField("file_id", $e->id);
        if (!$fileImageEntity->width && $width) {
            $fileImageEntity->width = $width;
        }
        if (!$fileImageEntity->height && $height) {
            $fileImageEntity->height = $height;
        }

        if ($size && !$width & !$height) {
            if ($fileImageEntity->width > $fileImageEntity->height) {
                $height = $size;
            } else {
                $width = $size;
            }
        } elseif ($width) {
            $height = intval(($width / $fileImageEntity->width) * $fileImageEntity->height);
        } elseif ($height) {
            $width = intval(($height / $fileImageEntity->height) * $fileImageEntity->width);
        } else {
            $width = $fileImageEntity->width;
            $height = $fileImageEntity->height;
        }
        $locationDir = dirname($e->location_reference);
        $locationFile = basename($e->location_reference);
        if ($prefix = $this->getState()->accountReference) {
            $locationDir = "{$prefix}/$locationDir";
        }
        return rtrim($result, "/") . "/$locationDir/{$width}x{$height}/".rawurlencode($locationFile);
    }

    private function getLocalCacheUrl($reference, $width, $height, $size, $timestamp, $createPreviewKey = false) {
        $result = [];
        $result['host'] = $this->getState()->getUrl()->getHttpHost();
        $result['scheme'] = $this->getState()->getUrl()->getScheme('http');
        $result['path'] = '_image/' . $reference;
        if ($width || $height || $timestamp || $size || $createPreviewKey) {
            $paramVars = [];
            if ($width) {
                $paramVars['width'] = $width;
            }
            if ($height) {
                $paramVars['height'] = $height;
            }
            if ($size) {
                $paramVars['size'] = $size;
            }
            if ($timestamp) {
                $paramVars['ts'] = $timestamp;
            }
            if($createPreviewKey && $this->getState()->getDbCache()->isEnabled()) {
                $uniqueId = uniqid();
                $this->getState()->getDbCache()->set("key.sb__file.preview.".$uniqueId, Time::getUtcNow(), '7200');
                $paramVars['filePreviewKey'] = $uniqueId;
            }
            $result['query'] = http_build_query($paramVars);
        }
        return $result;
    }

    public function addContentType($content, $contentType = 'text/html;charset=UTF-8')
    {
        return $this->addMetaTag($content, ['http-equiv' => "Content-Type", 'content' => $contentType]);
    }

    public function addViewPort($content, $viewPort = 'width=device-width, initial-scale=1')
    {
        return $this->addMetaTag($content, ['name' => "viewport", 'content' => $viewPort]);
    }

    public function addXuaCompatible($content, $compatible = 'IE=edge')
    {
        return $this->addMetaTag($content, ['X-UA-Compatible' => "Content-Type", 'content' => $compatible]);
    }

    public function addMetaTag($content, $attributes)
    {
        $result = $content;
        $dom = new \DOMDocument();
        @$dom->loadHTML($content);
        if ($dom->textContent) {
            $head = $dom->getElementsByTagName('head');
            if ($head->length == 0) {
                $head = $dom->createElement("head");
                $dom->documentElement->insertBefore($head, $dom->documentElement->firstChild);
            }
            $head = $dom->getElementsByTagName('head');
            $metaNode = $dom->createElement("meta");
            foreach($attributes as $k=>$v) {
                $metaNode->setAttribute($k, $v);
            }
            $head->item(0)->insertBefore($metaNode, $head->item(0)->firstChild);
            $dom->removeChild($dom->firstChild);
            $result = $dom->saveHTML();
        }
        return $result;
    }

    public function addTitleTag($content, $title)
    {
        $result = $content;
        $dom = new \DOMDocument();
        @$dom->loadHTML($content);
        if ($dom->textContent) {
            $head = $dom->getElementsByTagName('head');
            if ($head->length == 0) {
                $head = $dom->createElement("head");
                $dom->documentElement->insertBefore($head, $dom->documentElement->firstChild);
            }
            $head = $dom->getElementsByTagName('head');
            $metaNode = $dom->createElement("title", $title);
            $head->item(0)->insertBefore($metaNode, $head->item(0)->firstChild);
            $dom->removeChild($dom->firstChild);
            $result = $dom->saveHTML();
        }
        return $result;
    }

    public function addLinkTag($content, $attributes)
    {
        $result = $content;
        $dom = new \DOMDocument();
        @$dom->loadHTML($content);
        if ($dom->textContent) {
            $head = $dom->getElementsByTagName('head');
            if ($head->length == 0) {
                $head = $dom->createElement("head");
                $dom->documentElement->insertBefore($head, $dom->documentElement->firstChild);
            }
            $head = $dom->getElementsByTagName('head');
            $linkNode = $dom->createElement("link");
            foreach($attributes as $k=>$v) {
                $linkNode->setAttribute($k, $v);
            }
            $head->item(0)->insertBefore($linkNode, $head->item(0)->firstChild);
            $dom->removeChild($dom->firstChild);
            $result = $dom->saveHTML();
        }
        return $result;
    }

    /**
     * @todo - determine if the lote_id and type attributes should be removed
     * */
    private function convertAndCleanLoteLinkElement(\DOMElement $e)
    {
        //$e->removeAttribute("lote_id");
        if ($e->getAttribute("type") == "img") {
            //$e->removeAttribute("type");
            Xml::renameTag($e, 'img');
        } else {
            if ($e->getAttribute("type") == "file" || $e->getAttribute("type") == "page" || true) {
                //$e->removeAttribute("type");
                Xml::renameTag($e, 'a');
            }
        }
    }

    public function innerHTML($node)
    {
        $doc = new \DOMDocument('1.0', 'UTF-8');
        foreach ($node->childNodes as $child) {
            $doc->appendChild($doc->importNode($child, true));
        }

        return $doc->saveHTML();
    }

    private function replaceUrlWildcard($content, $baseUrl)
    {
        $currentUrl = $this->getCurrentUrl();
        $content = str_ireplace('%%__url__%%', $baseUrl, $content);
        $content = str_ireplace('%%__current_url__%%', $currentUrl, $content);
        return $content;
    }

    public function buildCloudfrontUrls($content, $baseUrl = false)
    {
        $cloudFrontService = new CloudFront($this->getState());
        $crawler = new Crawler($content);
        $anchorTags = $crawler->filter('img');
        foreach ($anchorTags as $a) {
            /** @var \DOMElement $a */
            $url = $a->getAttribute('src');
            if (is_string($url) && UrlUtil::isAbsolute($url) && $this->getState()->getConfig()->isServerUrl($url)) {
                //build a cloudfront url from the server url
                $cfUrl = $cloudFrontService->buildCloudfrontImageUrl($url);
                $a->setAttribute("src", $cfUrl);
                $a->ownerDocument->saveHTML();
            }
        }
        return $this->getCrawlerHtml($crawler);
    }

    public function replaceRelativeUrls($content, $baseUrl = false)
    {
        $content = $this->replaceRelativeAnchorTags($content, $baseUrl);
        $content = $this->replaceRelativeImageTags($content, $baseUrl);
        $content = $this->replaceRelativeTableBackgrounds($content, $baseUrl);
        return $content;
    }

    public function replaceRelativeTableBackgrounds($content, $baseUrl = false)
    {
        if (!$baseUrl) {
            $baseUrl = $this->getBaseUrl();
        }
        $crawler = new Crawler($content);
        $tables = $crawler->filter('table');
        foreach ($tables as $a) {
            if ($a->hasAttribute('background')) {
                $url = $a->getAttribute('background');
                if (!UrlUtil::isAbsolute($url)) {
                    if (strpos($url, '/') === 0) {
                        $url = substr($url, 1);
                    }
                    $a->setAttribute("background", $baseUrl . $url);
                    $a->ownerDocument->saveHTML();
                }
            }
            if ($a->hasAttribute('data-thumb')) {
                $url = $a->getAttribute('data-thumb');
                if (!UrlUtil::isAbsolute($url)) {
                    if (strpos($url, '/') === 0) {
                        $url = substr($url, 1);
                    }
                    $a->setAttribute("data-thumb", $baseUrl . $url);
                    $a->ownerDocument->saveHTML();
                }
            }
        }
        return $this->getCrawlerHtml($crawler);
    }

    public function replaceRelativeAnchorTags($content, $baseUrl = false)
    {
        if (!$baseUrl) {
            $baseUrl = $this->getBaseUrl();
        }
        $crawler = new Crawler($content);

        $anchorTags = $crawler->filter('a');
        foreach ($anchorTags as $a) {
            $url = $a->getAttribute('href');
            if (!UrlUtil::isAbsolute($url) && strpos($url, "%%") !== 0) {
                if (strpos($url, '/') === 0) {
                    $url = substr($url, 1);
                }
                $a->setAttribute("href", $baseUrl . $url);
                $a->ownerDocument->saveHTML();
            }
        }
        return $this->getCrawlerHtml($crawler);
    }


    public function replaceRelativeImageTags($content, $baseUrl = false)
    {
        if (!$baseUrl) {
            $baseUrl = $this->getBaseUrl();
        }
        $crawler = new Crawler($content);

        $anchorTags = $crawler->filter('img');
        foreach ($anchorTags as $a) {
            $url = $a->getAttribute('src');
            if (!UrlUtil::isAbsolute($url) && strpos($url, "%%") !== 0) {
                if (strpos($url, '/') === 0) {
                    $url = substr($url, 1);
                }
                $a->setAttribute("src", $baseUrl . $url);
                $a->ownerDocument->saveHTML();
            }
        }
        return $this->getCrawlerHtml($crawler);
    }

    /**
     * Get the HTML from the crawler.
     * This is a workaround as there seems to be UTF-8 encoding issues related to the html function on the crawler
     * acting differently for php versions >= 5.3.6
     * @param Crawler $crawler
     * @return string
     * */
    public function getCrawlerHtml($crawler)
    {
        $html = '';
        if (isset($crawler->getNode(0)->childNodes)) {
            foreach ($crawler->getNode(0)->childNodes as $child) {
                $document = new \DOMDocument('1.0', 'UTF-8');
                $importedNode = @$document->importNode($child, true);
                $document->appendChild($importedNode);
                $html .= rtrim($document->saveHTML());
            }
        }
        return $html;
    }

    private function getBaseUrl($scheme = "http://")
    {
        if(!$url = $this->getState()->getUrl()->getBaseUrl()) {
            if (php_sapi_name() == 'cli') {
                $url = $this->getState()->getSites()->getCurrentSiteUrl();
                if ($url && $url === '1') {
                    $url = $scheme . $this->getState()->getAccount()->domain . '/';
                } else {
                    if (!preg_match('|^http(s)?://.*$|i', $url)) {
                        $url = $scheme . $url . '/';
                    }
                    $url = preg_replace('#/+$#', '/', $url);
                }
            } else {
                $url = $this->getState()->getRequest()->getSchemeAndHttpHost() .
                    $this->getState()->getRequest()->getBasePath() . '/';
            }
        }
        return $url;
    }

    private function getCurrentUrl($scheme = "http://")
    {
        if (php_sapi_name() == 'cli') {
            $url = $this->getState()->getUrl()->getHttpHost();
            if($url && $url ==='1') {
                $url = $scheme.$this->getState()->getAccount()->domain. '/';
            }
        } else {
            $url = $this->getState()->getRequest()->getSchemeAndHttpHost() . $this->getState()->getRequest()->getRequestUri();
        }
        return $url;
    }

    public function getContentPreviewFromWildCards($reference, $textDisplay, $content)
    {
        $wildcards = [];
        preg_match_all('/' . $reference . '.*?%%/is', $content, $browseMatches);
        if (isset($browseMatches[0]) && !empty($browseMatches[0])) {
            $browseMatches = $browseMatches[0];
            foreach ($browseMatches as $value) {
                if ($value == $reference . "_url__%%") {
                    $wildcards[$reference . "_url__%%"] = $textDisplay;
                } elseif ($value == $reference . "__%%") {
                    $link = '<a href="">' . $textDisplay . '</a>';
                    if ($reference == "%%__unsubscribe_link") {
                        $link = '<p style="font-size: 10px; text-align:center; font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif;"><a style="font-size: 10px; color:#555555;" href="">' . $textDisplay . '</a></p>';
                    }
                    $wildcards[$reference . "__%%"] = '<a href="">' . $link . '</a>';
                } else {
                    $temp = array_values(array_filter(explode($reference . "_", $value)));
                    $color = str_replace("__%%", "", $temp[0]);
                    $link = '<a style="color:#' . $color . ';" href="">' . $textDisplay . '</a>';
                    if ($reference == "%%__unsubscribe_link") {
                        $link = '<p style="font-size: 10px; text-align:center; font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif;"><a style="font-size: 10px; color:#' . $color . ';" href="">' . $textDisplay . '</a></p>';
                    }
                    $wildcards[$reference . "_" . $color . "__%%"] = $link;
                }
            }
        }
        return $wildcards;
    }

    public function convertUpdateFormsToPreview($content)
    {
        preg_match_all('/%%__form_update_url_(\d+)__%%/is', $content, $formMatches);
        if (isset($formMatches[0]) && !empty($formMatches[0])) {
            $formIds = $formMatches[1];
            $formMatches = $formMatches[0];
            foreach ($formMatches as $k=>$value) {
                $content = str_replace($value, $this->getState()->getRequest()->getSchemeAndHttpHost()."/form/".$formIds[$k], $content);
            }
        }
        preg_match_all('/%%__form_update_(\d+)__%%/is', $content, $formMatches);
        if (isset($formMatches[0]) && !empty($formMatches[0])) {
            $formIds = $formMatches[1];
            $formMatches = $formMatches[0];
            foreach ($formMatches as $k=>$value) {
                $form = new Form($this->getState());
                $form->load($formIds[$k]);
                $formUrl = $this->getState()->getRequest()->getSchemeAndHttpHost()."/form/".$formIds[$k];
                $link = '<a style="font-size: 10px; color:#555555;" href="'.$formUrl.'">' . $form->name . '</a></p>';
                $content = str_replace($value, $link, $content);
            }
        }
        return $content;
    }

    public function getContentFromWildCards($reference, $textDisplay, $url, $content)
    {
        $wildcards = [];
        preg_match_all('/' . $reference . '.*?%%/is', $content, $browseMatches);
        if (isset($browseMatches[0]) && !empty($browseMatches[0])) {
            $browseMatches = $browseMatches[0];
            foreach ($browseMatches as $value) {
                if ($value == $reference . "_url__%%") {
                    $wildcards[$value] = $url;
                } elseif ($value == $reference . "__%%") {
                    $link = '<a href="' . $url . '">' . $textDisplay . '</a>';
                    if ($reference == "%%__unsubscribe_link") {
                        $link = '<p style="font-size: 10px; text-align:center; font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif;"><a style="font-size: 10px; color:#555555;" href="">' . $textDisplay . '</a></p>';
                    }
                    $wildcards[$reference . "__%%"] = '<a href="' . $url . '">' . $link . '</a>';
                } else {
                    $temp = array_values(array_filter(explode($reference . "_", $value)));
                    $color = str_replace("__%%", "", $temp[0]);
                    $link = '<a style="color:#' . $color . ';" href="' . $url . '">' . $textDisplay . '</a>';
                    if ($reference == "%%__unsubscribe_link") {
                        $link = '<p style="font-size: 10px; text-align:center; font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif;"><a style="font-size: 10px; color:#' . $color . ';" href="' . $url . '">' . $textDisplay . '</a></p>';
                    }
                    $wildcards[$reference . "_" . $color . "__%%"] = $link;
                }
            }
        }
        return $wildcards;
    }

    public function replaceCampaignWildcards($content, $campaign_id)
    {
        $c = new CampaignEdm($this->getState());
        if($c->loadByField("campaign_id", $campaign_id)){
            $content = str_replace("%%__email_subject__%%",$c->subject, $content);
        }
        return $content;
    }

    /**
     * Remove all remaining wildcards in text
     *
     * @access public
     * @param string $content - Text to search
     * @return string
     */
    public function cleanRemainingWildcards($content)
    {
        $content = preg_replace("/%%[a-zA-z0-9\-\_\.]*%%/", '', $content);
        return $content;
    }
}
