<?php
namespace BlueSky\Framework\Service;

use BlueSky\Framework\Entity\Audit\FieldLog;
use BlueSky\Framework\Entity\Audit\FileLog;
use BlueSky\Framework\Entity\Audit\LinkLog;
use BlueSky\Framework\Entity\Audit\Log;
use BlueSky\Framework\Entity\Audit\ObjectLog;
use BlueSky\Framework\Object\State;
use BlueSky\Framework\Object\Entity\Base as BaseEntity;
use BlueSky\Framework\State\Web;

class Audit extends State
{

    /**
     * @param Log $log - the ID of this log
     * */
    protected $log;

    /**
     * Setup the log
     * @access private
     * @return void
     * */
    private function initLog()
    {
        if (!$this->log) {
            $this->log = new Log($this->getState());
            $this->log->parent_id = 0;
            if($this->getState() instanceof Web) {
                $r = $this->getState()->getRoute();
                if (isset($r['controller'])) {
                    $this->log->controller = $r['controller'];
                }
                if (isset($r['_method'])) {
                    $this->log->method = $r['_method'];
                }
            }
            $this->log->save();
        }
    }

    /**
     * @param BaseEntity $objectOld
     * @param BaseEntity $objectNew
     * @return ObjectLog|null
     * */
    public function addObjectLog($objectNew, $objectOld = null)
    {
        $result = null;
        if ($this->isModified($objectNew, $objectOld)) {
            $result = $this->initObjectLog($objectNew, $objectOld);
            $modifiedProperties = $objectNew->getModifiedProperties();
            foreach ($modifiedProperties as $k => $v) {
                if (strpos($k, 'lote_') !== 0) {
                    $newValue = $objectNew->{$k};
                    if ($k == "password") {
                        if (!(strlen($newValue) == 60 && strpos($newValue, '$2y$10$') === 0)) {
                            $newValue = password_hash($newValue, PASSWORD_DEFAULT);
                        }
                    }
                    if ($objectOld) {
                        if ($newValue != $objectOld->{$k}) {
                            $this->addFieldLog('core', $result, $k, $newValue, $objectOld->{$k});
                        }
                    } else {
                        $this->addFieldLog('core', $result, $k, $newValue, null);
                    }
                }
            }
        }
        return $result;
    }

    /**
     * @param BaseEntity $objectOld
     * @param BaseEntity $objectNew
     * @return ObjectLog|null
     * */
    public function addObjectCfLog($objectNew, $objectOld = null)
    {
        $result = null;
        if ($this->isModified($objectNew, $objectOld)) {
            $result = $this->initObjectLog($objectNew, $objectOld);
            $modifiedProperties = $objectNew->getModifiedProperties();
            foreach ($modifiedProperties as $k => $v) {
                if(strpos($k, 'lote_')!==0) {
                    if ($objectOld) {
                        if($objectNew->{$k} != $objectOld->{$k}) {
                            $this->addFieldLog('core', $result, $k, $objectNew->{$k}, $objectOld->{$k});
                        }
                    } else {
                        $this->addFieldLog('core', $result, $k, $objectNew->{$k}, null);
                    }
                }
            }
        }
        return $result;
    }

    /**
     * @param String $tableName
     * @param array $dataNew
     * @param array $dataOld
     * @return ObjectLog|null
     * */
    public function addObjectDataLog($tableName, $dataNew, $dataOld = null)
    {
        $result = null;
        if(!$dataOld && isset($dataNew['id']) && $dataNew['id'] > 0)
        {
            $q = $this->getState()->getReadDb()->createQueryBuilder();
            $q->select('*')->from($tableName, 't')->where("id = :id")->setParameter("id", $dataNew['id']);
            $dataOld = $q->execute()->fetch(\PDO::FETCH_ASSOC);
        }
        if ($this->isModifiedArray($dataNew, $dataOld)) {
            $result = $this->initObjectArrayLog($tableName, $dataNew, $dataOld);
            foreach ($dataNew as $k => $v) {
                if($dataOld && isset($dataOld[$k])) {
                    $this->addFieldLog('core', $result, $k, $dataNew[$k], $dataOld[$k]);
                }
                else {
                    $this->addFieldLog('core', $result, $k, $dataNew[$k], null);
                }
            }
        }
        return $result;
    }

    /**
     * @param string $tableName
     * @param array $dataNew
     * @param array $dataOld
     * @return ObjectLog
     * */
    public function initObjectArrayLog($tableName, $dataNew, $dataOld = null)
    {
        $this->initLog();
        $o = new ObjectLog($this->getState());
        $o->log_id = $this->log->id;
        $o->object_ref = $tableName;
        if (!$dataOld) {
            $o->operation = 'add';
            $o->object_id = $dataNew['id'];
        } else {
            $o->object_id = $dataOld['id'];
            if (isset($dataNew['lote_deleted']) && $dataNew['lote_deleted']) {
                $o->operation = 'delete';
            } else {
                $o->operation = 'update';
            }
        }
        $o->save();
        return $o;
    }

    /**
     * Add a link creation
     * @param $objectRefFrom
     * @param $objectIdFrom
     * @param $objectRefTo
     * @param $objectIdTo
     * @param null|ObjectLog $objectLog
     * */
    public function addLinkLog($objectRefFrom, $objectIdFrom, $objectRefTo, $objectIdTo, ObjectLog $objectLog = null)
    {
        $this->addLinkOperationLog('link', $objectRefFrom, $objectIdFrom, $objectRefTo, $objectIdTo, $objectLog);
    }

    /**
     * Add a link removal
     * @param $objectRefFrom
     * @param $objectIdFrom
     * @param $objectRefTo
     * @param $objectIdTo
     * @param null|ObjectLog $objectLog
     * */
    public function addUnlinkLog($objectRefFrom, $objectIdFrom, $objectRefTo, $objectIdTo, ObjectLog $objectLog = null)
    {
        $this->addLinkOperationLog('unlink', $objectRefFrom, $objectIdFrom, $objectRefTo, $objectIdTo, $objectLog);
    }

    /**
     * Add a link operation
     * @param $operation - the operation type
     * @param $objectRefFrom
     * @param $objectIdFrom
     * @param $objectRefTo
     * @param $objectIdTo
     * @param ObjectLog $objectLog
     */
    private function addLinkOperationLog($operation, $objectRefFrom, $objectIdFrom, $objectRefTo, $objectIdTo, ObjectLog $objectLog = null)
    {
        $this->initLog();
        $l = new LinkLog($this->getState());
        $l->log_id = $this->log->id;
        $l->operation = $operation;
        $l->object_id_from = $objectIdFrom;
        $l->object_id_to = $objectIdTo;
        $l->object_ref_from = $objectRefFrom;
        $l->object_ref_to = $objectRefTo;
        if($objectLog) {
            $l->object_log_id = $objectLog->id;
        }
        $l->save();
    }

    /**
     * Check if an object has been modified
     * @param array $dataNew
     * @param array $dataOld
     * @return bool
     * */
    protected function isModifiedArray($dataNew, $dataOld = null)
    {
        $result = !$dataOld;
        if(!$result) {
            foreach ($dataNew as $k => $v) {
                if($dataNew[$k] != $dataOld[$k]) {
                    $result = true;
                    break;
                }
            }
        }
        return $result;
    }

    /**
     * Check if an object has been modified
     * @param BaseEntity $objectOld
     * @param BaseEntity $objectNew
     * @return bool
     * */
    protected function isModified($objectNew, $objectOld = null)
    {
        $result = !$objectOld;
        if(!$result) {
            $modifiedProperties = $objectNew->getModifiedProperties();
            foreach ($modifiedProperties as $k => $v) {
                if($objectNew->{$k} != $objectOld->{$k}) {
                    $result = true;
                    break;
                }
            }
        }
        return $result;
    }

    /**
     * @param BaseEntity $objectOld
     * @param BaseEntity $objectNew
     * @return ObjectLog
     * */
    public function initObjectLog($objectNew, $objectOld = null)
    {
        $this->initLog();
        $o = new ObjectLog($this->getState());
        $o->log_id = $this->log->id;
        $o->object_ref = $objectNew->getTableName();
        if (!$objectOld) {
            $o->operation = 'add';
            $o->object_id = $objectNew->id;
        } else {
            $o->object_id = $objectOld->id;
            if ($objectNew->lote_deleted) {
                $o->operation = 'delete';
            } else {
                $o->operation = 'update';
            }
        }
        $o->save();
        return $o;
    }

    /**
     * @param string $fieldType
     * @param ObjectLog $logObject
     * @param string $fieldName
     * @param string $newValue
     * @param null $oldValue
     * @return void
     */
    public function addFieldLog($fieldType, $logObject, $fieldName, $newValue, $oldValue = null)
    {
        if (is_array($newValue)) {
            $newValue = json_encode($newValue);
        } elseif ($newValue instanceof \DateTimeInterface) {
            $newValue = $newValue->format('Y-m-d H:i:s');
        }
        if (is_array($oldValue)) {
            $oldValue = json_encode($oldValue);
        } elseif ($oldValue instanceof \DateTimeInterface) {
            $oldValue = $oldValue->format('Y-m-d H:i:s');
        }
        if ($newValue != $oldValue) {
            $f = new FieldLog($this->getState());
            $f->object_id = $logObject->id;
            $f->field_type = $fieldType;
            $f->field_name = $fieldName;
            $f->value_new = $newValue;
            $f->value_old = $oldValue;
            $f->save();
        }
    }

    /**
     * @param string $store
     * @param string $storeReference
     * @param string $archiveReference
     * @param string $objectRef
     * @param int $objectId
     * @param string $fieldRef
     * @param int $objectLogId
     * @return void
     * */
    public function addFileLog($store, $storeReference, $archiveReference, $objectRef, $objectId, $fieldRef = '', $objectLogId = 0)
    {
        $this->initLog();
        $f = new FileLog($this->getState());
        $f->log_id = $this->log->id;
        $f->store = $store;
        $f->location_reference = $storeReference;
        $f->archive_reference = $archiveReference;
        $f->object_log_id  = $objectLogId;
        $f->object_ref  = $objectRef;
        $f->object_id  = $objectId;
        $f->field_reference = $fieldRef;
        $f->save();
    }

}
