<?php
namespace BlueSky\Framework\Service;

use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\ValidationData;

/**
 * Class for JSON web tokens
 */
class Jwt
{

    /**
     * Generate a token
     * @param $userId
     * @param $issuer - the issue of the token
     * @param $expires - when this token expires
     * @param string|bool $key - the optional signing key
     * @param string|bool $jtiId - the ID of the token
     * @return string
     */
    function generateTokenString($userId, $issuer, $expires = 3600, $key = false, $jtiId = false)
    {
        $b = new Builder();
        $b->setIssuer($issuer) // Configures the issuer (iss claim)
            ->setAudience($issuer) // Configures the audience (aud claim)
            ->setIssuedAt(time()) // Configures the time that the token was issue (iat claim)
            ->setNotBefore(time() + 0) // Configures the time that the token can be used (nbf claim)
            ->setExpiration(time() + $expires) // Configures the expiration time of the token (nbf claim)
            ->set('user_id', $userId);

        if ($jtiId) {
            $b->setId($jtiId, true);
        }

        if ($key) {
            $signer = new Sha256();
            $b->sign($signer, $key);
        }

        return (string)$b->getToken(); // Retrieves the generated token
    }

    /**
     * @param $token - the full token as a string
     * @param $issuer - the issue of the token
     * @param string|bool $key - the optional signing key
     * @param string|bool $jtiId - the ID of the token
     * @return bool
     */
    function validateTokenString($token, $issuer, $key = false, $jtiId = false)
    {
        $result = false;
        $token = (new Parser())->parse((string) $token); // Parses from a string
        $token->getHeaders(); // Retrieves the token header
        $token->getClaims(); // Retrieves the token claims

        $data = new ValidationData(); // It will use the current time to validate (iat, nbf and exp)
        $data->setIssuer($issuer);
        $data->setAudience($issuer);
        if($jtiId) {
            $data->setId($jtiId);
        }
        if($result = $token->validate($data) && $key) {

            $signer = new Sha256();
            $result = $token->verify($signer, $key);
        }
        return $result;
    }

    /**
     * Get the "user_id" claim of the token
     * @param $token - the full token as a string
     * @return string
     */
    function getTokenUserId($token)
    {
        try {
            $token = (new Parser())->parse((string)$token);
            $result = $token->getClaim("user_id");
        }
        catch(\OutOfBoundsException $e) {
            $result = null;
        }
        return $result;
    }


    /**
     * Get the token ID (jti) if one exists
     * @param $token - the full token as a string
     * @return string
     */
    function getTokenId($token)
    {
        try {
            $token = (new Parser())->parse((string)$token);
            $result = $token->getHeader('jti');
        }
        catch(\OutOfBoundsException $e) {
            $result = null;
        }
        return $result;
    }

}
