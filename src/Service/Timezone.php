<?php
namespace BlueSky\Framework\Service;

use BlueSky\Framework\Object\State as BaseState;
use BlueSky\Framework\Util\Time;

/**
 * Timezone service
 */
class Timezone extends BaseState
{

    /**
     * Get the groups for this user
     * @access public
     * @return array
     * @todo - look at the commented part in this function
     * */
    public function getDisplayTimezone()
    {
        $result = date_default_timezone_get();

        $systemTimezone = $this->getState()->getSettings()->get('system.timezone', false);
        if($systemTimezone && !Time::isValidTimezone($systemTimezone)) {
            $systemTimezone = false;
        }
/*        $userTimezone = false;
        if($this->getState()->getUser()->timezone_id) {
            $userTimezone = $this->getState()->getUser()->timezone_id;
        }
        if($userTimezone && !Time::isValidTimezone($userTimezone)) {
            $userTimezone = false;
        }

        if($userTimezone) {
            $result = $userTimezone;
        }
        elseif($systemTimezone) {
            $result = $systemTimezone;
        }*/
        if($systemTimezone) {
            $result = $systemTimezone;
        }
        return $result;
    }

    /**
     * @return string
     * */
    public function getDisplayOffset() {
        $result = '+0:00';
        if($data = Time::getTimezone($this->getDisplayTimezone())) {
            $result = $data['offset'];
        }
        return $result;
    }

    /**
     * @return string
     * */
    public function getOffset() {
        return $this->getDisplayOffset();
    }

    public function convertToDisplay($time, $format = 'Y-m-d H:i:s') {
        $result = '';
        if ($object = Time::getUtcObject($time)) {
            if($object->format('Y') > 0) {
                $object->setTimezone(new \DateTimeZone($this->getDisplayTimezone()));
                $result = $object->format($format);
            }
        }
        return $result;
    }


    public function convertToUtc($time, $timezone, $format) {
        $result = "";
        if($timezone){
            try {
                $d = new \DateTime($time, new \DateTimeZone($timezone));
                $d->setTimezone(new \DateTimeZone('UTC'));
                $result = $d->format($format);
            }
            catch(\Exception $e) {
                //do nothing
            }
        }
        return $result;
    }

}
