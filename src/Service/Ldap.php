<?php
namespace BlueSky\Framework\Service;

use Adldap\Adldap;
use Adldap\Connections\Provider;
use Adldap\Models\Entry;
use Adldap\Models\Group;
use Adldap\Models\OrganizationalUnit;
use Adldap\Models\RootDse;
use Adldap\Models\User;
use BlueSky\Framework\Object\State;
use BlueSky\Framework\Util\Time;


class Ldap extends State
{

    const LOGIN_LDAP_SERVER_CREDENTIALS = 10;

    const LOGIN_LDAP_SERVER_UNAVAILABLE = 11;

    /**
     * @var Adldap $connection
     */
    private $connection = false;
    private $providerName = 'default';

    public function ldapConnect(&$loginError = '')
    {
        $options = [];
        $options['domain_controllers'] = [$this->getState()->getSettings()->get('login.ldap.dc')];
        $options['account_suffix'] = $this->getState()->getSettings()->get('login.ldap.suffix');
        $options['base_dn'] = $this->getState()->getSettings()->get('login.ldap.basedn');
        $options['admin_username'] = $this->getState()->getSettings()->get('login.ldap.username');
        $options['admin_password'] = $this->getState()->getSettings()->get('login.ldap.password');
        if($this->getState()->getSettings()->get('login.ldap.use_ssl')) {
            $options['use_ssl'] = true;
        }

        $adLdap = new Adldap();
        $provider = new Provider($options);
        $adLdap->addProvider($this->providerName, $provider);
        if ($adLdap->connect($this->providerName)) {
            $adLdap->getProvider($this->providerName)->auth()->bindAsAdministrator();
            $this->connection = $adLdap;
        } else {
            $loginError = self::LOGIN_LDAP_SERVER_UNAVAILABLE;
        }
    }

    /**
     * Determine if a user in AD requires their password to be reset, as sometimes a login may fail due to this reason
     * @param string $username
     * @access private
     * @return boolean
     * */
    public function requiresPasswordReset($username)
    {
        return $this->getPasswordLastSet($username) == '0';
    }

    /**
     * Determine a user's password expiry data
     *
     * @access public
     * @param string $username The username to query
     * @return array
     */
    public function passwordExpiry($username) {
        $expiry = [];

        $user = $this->connection->getDefaultProvider()->search()->users()->whereEquals('samaccountname', $username)->firstOrFail();
        if($user instanceof \Adldap\Models\User) {
            $expiry['is_expired'] = $user->passwordExpired();
            if(!$expiry['is_expired']) {
                $time = new \DateTime(Time::ldapTimestampToDateString($user->getPasswordLastSet()));
                $expiryDate = date('Y-m-d H:i:s', strtotime('+365 day', $time->getTimestamp()));
                $expiry['expiryformat'] = $expiryDate;
            }
        }
        return $expiry;
    }

    /**
     * Get the last time that a password was set for a user
     * @param string $username
     * @access private
     * @return boolean
     * */
    public function getPasswordLastSet($username)
    {
        $result = false;
        if($username != null) {
            $user = $this->getConnection()->getDefaultProvider()->search()->users()->where('samAccountName', '=', $username)->first(['*']);
            if($user instanceof User) {
                $result = $user->getPasswordLastSet();
            }
        }
        return $result;
    }

    public function updatePassword($username, $password, &$errorMessage)
    {
        $result = false;
        if ($this->connection) {
            $user = $this->getConnection()->getDefaultProvider()->search()->users()->where('samAccountName', '=', $username)->first(['*']);
            if($user instanceof User) {
                $user->setPassword($password);
                $result = $user->save();
            }
        }
        return $result;
    }

    /**
     * @return adLDAP
     *  */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * Remove the requirement for a user to set their password on next login, as this prevents authentication
     * @param string $username
     * @access private
     * @return boolean
     * */
    public function unsetPasswordResetRequirement($username)
    {
        return $this->setUserVariable($username, 'pwdlastset', '-1');
    }

    /**
     * Set the requirement for a user to set their password on next login
     * @param string $username
     * @access private
     * @return boolean
     * */
    public function setPasswordResetRequirement($username)
    {
        return $this->setUserVariable($username, 'pwdlastset', '0');
    }

    /**
     * Update a user variable
     * @param string $username
     * @param string $variableName
     * @param string $value
     * @access private
     * @return boolean
     * */
    private function setUserVariable($username, $variableName, $value)
    {
        $result = false;
        $attributes = [$variableName => $value];
        $userInfo = $this->getConnection()->getDefaultProvider()->search()->users()->where('samAccountName', '=', $username)->firstOrFail(['*'])['attributes'];
        $userDn = $userInfo['dn'];
        if ($userDn) {
            $result = ldap_modify($this->connection->getDefaultProvider()->getConnection()->getConnection(), $userDn, $attributes);
        }
        return $result;
    }
}
