<?php
namespace BlueSky\Framework\State\Connection;

use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\DriverManager;
use BlueSky\Framework\Log\Sql;
use BlueSky\Framework\Object\State;

/**
 * Class for creating database connections based on provided configuration variables
 * @todo - implement proper error checking for valid configuration data
 * */
class Manager extends State
{

    /**
     * Create a database connection to the provided DB configuration
     * @param array $config - the configuration data for the connection
     * @access public
     * @throws \Exception - for invalid (or not yet implemented) database types
     * @return \Doctrine\DBAL\Connection
     */
    public function createConnection($config)
    {
        if(isset($config['type']) && $config['type']=='pdo_mysql') {
            $connection = $this->createMysqlConnection($config);
        }
        elseif(isset($config['type']) && $config['type']=='pdo_sqlite') {
            $connection = $this->createSqliteConnection($config);
        }
        else {
            throw new \Exception("Database connection not implemented");
        }
        return $connection;
    }

    /**
     * Create the base connection parameters to use with any database
     * @param string $driver - the driver type
     * @param array $config
     * @access private
     * @return array
     * @todo - uncommenting the two lines in the function causes an issue with Doctrine on delete of a node. Why??
     * */
    private function createBaseParams($driver, $config = [])
    {
        $params = [];
        $params['driver'] = $driver;
        $params['charset'] = $config['charset'] ?? 'utf8';
        $params['fetch_case'] = \PDO::CASE_LOWER;
        //$params['wrapperClass'] = 'Doctrine\DBAL\Portability\Connection';
        //$params['portability'] = Connection::PORTABILITY_ALL;
        return $params;
    }

    /**
     * Create a MySQL database connection using the provided configuration
     * @param array $config - the database configuration
     * @access private
     * @return \Doctrine\DBAL\Connection
     * */
    private function createMysqlConnection($config)
    {
        $params = $this->createBaseParams('pdo_mysql', $config);
        $params['host'] = $config['host'];
        $params['dbname'] = $config['name'];
        $params['user'] = $config['user'];
        $params['password'] = $config['pass'];
        if (!empty($config['server_version'])) {
            $params['server_version'] = $config['server_version'];
        }
        $result = DriverManager::getConnection($params);
        $result->getConfiguration()->setSQLLogger(new Sql($this->getState()));
        return $result;
    }

    /**
     * Create an SQLite database connection using the provided configuration
     * @param array $config - the database configuration
     * @access private
     * @return \Doctrine\DBAL\Connection
     * */
    private function createSqliteConnection($config)
    {
        $params = $this->createBaseParams('pdo_sqlite');
        $params['path'] = str_replace("\\",'/',$config['path']);
        if(isset($config['user'])) {
            $params['user'] = $config['user'];
        }
        if(isset($config['pass'])) {
            $params['password'] = $config['pass'];
        }
        return DriverManager::getConnection($params, new Configuration());
    }

}
