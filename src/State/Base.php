<?php

namespace BlueSky\Framework\State;

use Aura\Di;
use Aura\Signal;
use Aws\S3\S3Client;
use BlueSky\Framework\Application\App\Schema as AppSchema;
use BlueSky\Framework\Application\Composition;
use BlueSky\Framework\Auth\Acl\Access;
use BlueSky\Framework\Auth\Acl\Context;
use BlueSky\Framework\Auth\Login\Variables\Factory as VariablesFactory;
use BlueSky\Framework\Auth\Login\Variables\VariableInterface;
use BlueSky\Framework\Auth\Session as LoginSession;
use BlueSky\Framework\Cache\File\Factory as CacheFactory;
use BlueSky\Framework\Entity\App as AppEntity;
use BlueSky\Framework\Entity\Auth\Session as AuthSession;
use BlueSky\Framework\Entity\Schema\Entity as SchemaEntityEntity;
use BlueSky\Framework\Entity\Schema\Field as SchemaFieldEntity;
use BlueSky\Framework\Entity\Schema\Relation as SchemaRelationEntity;
use BlueSky\Framework\Entity\User\User;
use BlueSky\Framework\Event\Manager;
use BlueSky\Framework\Log\Set as LoggerSet;
use BlueSky\Framework\Model\Setting;
use BlueSky\Framework\Model\Site\Site;
use BlueSky\Framework\Object\Entity\Manager\EntityManager;
use BlueSky\Framework\Queue\Producer;
use BlueSky\Framework\Registry\Config;
use BlueSky\Framework\Routing\Route;
use BlueSky\Framework\Routing\RouteCollection;
use BlueSky\Framework\Schema\Loader as Schema;
use BlueSky\Framework\Service\Audit as AuditLog;
use BlueSky\Framework\Service\Browscap\Browscap;
use BlueSky\Framework\Service\Closure;
use BlueSky\Framework\Service\Data;
use BlueSky\Framework\Service\Debug;
use BlueSky\Framework\Service\DevHelper;
use BlueSky\Framework\Service\Group as GroupService;
use BlueSky\Framework\Service\ImageServer;
use BlueSky\Framework\Service\ParentAccount;
use BlueSky\Framework\Service\Plugin;
use BlueSky\Framework\Service\Timer;
use BlueSky\Framework\Service\Timezone;
use BlueSky\Framework\Service\Url;
use BlueSky\Framework\Util\Strings;
use BlueSky\Framework\View\Factory as ViewFactory;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Schema\Schema as DBSchema;
use Doctrine\DBAL\Schema\Table;
use League\Flysystem\AwsS3v3\AwsS3Adapter;
use League\Flysystem\Filesystem;
use Lote\App\MasterBase\Entity\Master\Account as AccountEntity;
use Lote\App\MasterBase\Model\Master\Account;
use MicrosoftAzure\Storage\Blob\BlobRestProxy;
use Psr\SimpleCache\CacheInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Translation\Loader\PhpFileLoader;
use Symfony\Component\Translation\Translator;

//@todo - move this back to the Web state


class Base
{

    /**
     * @var \Aura\Di\Container $di
     */
    protected $di;

    /**
     * @var boolean $isCli
     * */
    protected $isCli;

    /**
     * @var Connection\Manager
     * */
    protected $connectionManager;

    /**
     * @var array
     * */
    protected $route;

    /**
     * @var array $data ;
     * */
    public $data = [];

    /**
     * @var string $accountReference ;
     * */
    public $accountReference = "";

    /** @var string $accountReferenceFormatted - This is a php safe account reference ucworded */
    public $accountReferenceFormatted = "";

    /**
     * @var string $configReference ;
     * */
    public $configReference = "";

    /**
     * @var string $masterOnly ;
     * */
    public $masterOnly = false;


    /**
     * Default constructor for Base State
     *
     * @access public
     * @param null $parentReference
     * @param bool $masterOnly
     */
    public function __construct($parentReference = null, $masterOnly = false)
    {
        $this->di = new Di\Container(new Di\Forge(new Di\Config));
        $this->connectionManager = new Connection\Manager($this);
        $this->isCli = php_sapi_name() == 'cli';
        if ($masterOnly) {
            $this->accountReference = $this->configReference = 'master';
        } elseif ($parentReference) {
            $this->accountReference = $this->configReference = $parentReference;
        } else {
            $this->accountReference = LOTE_ACCOUNT_REF;
        }
        $this->accountReferenceFormatted = str_replace('_', '', ucwords($this->accountReference, '_ '));
        if (ctype_digit($this->accountReferenceFormatted[0])) {
            $this->accountReferenceFormatted = APP_NAMESPACE . $this->accountReferenceFormatted;
        }
        $this->masterOnly = $masterOnly;
        $this->setup();
    }

    /**
     * Default setup function for initialising the state variables
     * @access protected
     * @return void
     * @throws Di\Exception\ContainerLocked
     * @throws Di\Exception\ServiceNotObject
     */
    protected function setup()
    {
        $this->di->set("config", function () {
            return $this->setupConfig();
        });
        $this->di->set("database.master", function () {
            return $this->setupMasterDatabase();
        });
        if (!$this->masterOnly) {
            $this->di->set("database.parent", function () {
                return $this->setupParentDatabase();
            });
            $this->di->set("database.superparent", function () {
                return $this->setupSuperParentDatabase();
            });
            $this->di->set("database.read", function () {
                return $this->setupReadDatabase();
            });
            $this->di->set("database.write", function () {
                return $this->setupWriteDatabase();
            });
        } else {
            $this->di->set("database.read", function () {
                return $this->di->get("database.master");
            });
            $this->di->set("database.write", function () {
                return $this->di->get("database.master");
            });
        }
        $this->di->set("logger.set", function () {
            return $this->setupLoggerSet();
        });
        $this->di->set("request", function () {
            return $this->setupRequest();
        });
        $this->di->set("response", function () {
            return $this->setupResponse();
        });
        $this->di->set("site", function () {
            return $this->setupSites();
        });
        $this->di->set("signal", function () {
            return $this->setupSignal();
        });
        $this->di->set("apps", function () {
            return $this->setupApps();
        });
        $this->di->set("user", function () {
            return $this->setupUser();
        });
        $this->di->set("group.service", function () {
            return $this->setupGroupService();
        });
        $this->di->set("queue", function () {
            return $this->setupQueue();
        });
        $this->di->set("auth.session", function () {
            return $this->setupAuthSession();
        });
        $this->di->set("view", function () {
            return $this->setupView();
        });
        $this->di->set("context", function () {
            return $this->setupContext();
        });
        $this->di->set("access", function () {
            return $this->setupAccess();
        });
        $this->di->set("translator", function () {
            return $this->setupTranslator();
        });
        $this->di->set("response", function () {
            return new Response();
        });
        $this->di->set("login.session", function () {
            return $this->setupLoginSession();
        });
        $this->di->set("settings", function () {
            return $this->setupSettings();
        });
        $this->di->set("plugins", function () {
            return $this->setupPlugins();
        });
        $this->di->set("audit", function () {
            return $this->setupAudit();
        });
        $this->di->set("ldap", function () {
            return $this->setupLdap();
        });
        $this->di->set("azureclient", function () {
            return $this->setupAzureClient();
        });
        $this->di->set("s3client", function () {
            return $this->setupS3Client();
        });
        $this->di->set("s3client.cloudfront", function () {
            return $this->setupS3CloudfrontClient();
        });
        $this->di->set("account", function () {
            return $this->setupAccount();
        });
        $this->di->set("data", function () {
            return $this->setupSystemData();
        });
        $this->di->set("parent.account", function () {
            return $this->setupParentAccount();
        });
        $this->di->set("parent.super.account", function () {
            return $this->setupSuperParentAccount();
        });
        $this->di->set("timezone", function () {
            return $this->setupTimezone();
        });
        $this->di->set("filecache", function () {
            return $this->setupFileCache();
        });
        $this->di->set("cache.db", function () {
            return $this->setupDbCache();
        });
        $this->di->set("client.account.config.file_system", function () {
            return $this->setupAccountConfigFileSystem();
        });
        $this->di->set("parent.state", function () {
            return $this->setupParentState();
        });
        $this->di->set("parent.super.state", function () {
            return $this->setupSuperParentState();
        });
        $this->di->set("master.state", function () {
            return $this->setupMasterState();
        });
        $this->di->set("url", function () {
            return $this->setupUrl();
        });
        $this->di->set("imageserver", function () {
            return $this->setupImageServer();
        });
        $this->di->set("closures", function () {
            return $this->setupClosures();
        });
        $this->di->set("timer", function () {
            return $this->setupTimer();
        });
        $this->di->set("timer", function () {
            return $this->setupTimer();
        });
        $this->di->set("event.manager", function () {
            return $this->setupEventManager();
        });
        $this->di->set("debug", function () {
            return $this->setupDebug();
        });
        $this->di->set("schema", function () {
            return $this->setupSchema();
        });
        $this->di->set("entity.manager", function () {
            return $this->setupEntityManager();
        });
        $this->di->set("login.variables", function () {
            return $this->setupLoginVariables();
        });
        $this->di->set("dev.helper", function () {
            return $this->setupDevHelper();
        });
        $this->di->set(
            "browscap",
            function () {
                return $this->setupBrowscap();
            }
        );
    }

    public function tearDown()
    {
        if (in_array("database.master", $this->di->getServices()) && $this->getMasterDb()) {
            $this->getMasterDb()->close();
        }
        if (in_array("database.parent", $this->di->getServices()) && $this->getParentAccount()) {
            $this->getParentDb()->close();
        }
        if (in_array("database.read", $this->di->getServices()) && $this->getReadDb()) {
            $this->getReadDb()->close();
        }
        if (in_array("database.write", $this->di->getServices()) && $this->getWriteDb()) {
            $this->getWriteDb()->close();
        }
    }

    /**
     * Setup the redis service for DB caching
     * @return CacheInterface
     * */
    protected function setupDbCache()
    {
        return \BlueSky\Framework\Cache\Database\Factory::createInstance($this);
    }

    /**
     * Get the DB cache in use
     * @return CacheInterface
     * */
    public function getDbCache(): CacheInterface
    {
        return $this->di->get("cache.db");
    }

    /**
     * Setup the file cache object
     * @return \BlueSky\Framework\Cache\File\Base
     */
    public function setupFileCache()
    {
        return CacheFactory::createInstance($this);
    }

    /**
     * Get the file cache
     * @return \BlueSky\Framework\Cache\File\Base
     */
    public function getFileCache()
    {
        return $this->di->get("filecache");
    }

    public function setupQueue(): Producer
    {
        return new Producer($this->getConfig()->get('queue.enqueue', null) ?? []);
    }

    public function getQueue(): ?Producer
    {
        $result = null;
        try {
            $producer = $this->di->get("queue");
            if ($producer instanceof Producer) {
                $result = $producer;
            }
        } catch (Di\Exception\ServiceNotFound $e) {
        }
        return $result;
    }

    /**
     * Setup the logger set object
     * @return LoggerSet
     */
    public function setupLoggerSet()
    {
        return new LoggerSet($this->getConfig(), $this);
    }

    /**
     * Get the logger set object
     * @return LoggerSet
     */
    public function getLoggers()
    {
        return $this->di->get("logger.set");
    }

    /**
     * setup the timezone object
     * @return Timezone
     */
    public function setupTimezone()
    {
        return new Timezone($this);
    }

    /**
     * Get the timezone object
     * @return Timezone
     */
    public function getTimezone()
    {
        return $this->di->get("timezone");
    }

    /**
     * Get the request object
     * @return \Lote\App\MasterBase\Entity\Master\Account
     */
    public function getAccount()
    {
        return $this->di->get("account");
    }

    /**
     * Get the request object
     * @return Data
     */
    public function getData()
    {
        return $this->di->get("data");
    }

    /**
     * Get the request object
     * @return \Symfony\Component\HttpFoundation\Response
     * @todo - move this back to the web state
     */
    public function getResponse()
    {
        return $this->di->get("response");
    }

    /**
     * Get the request object
     * @return LoginSession
     */
    public function getLogin()
    {
        return $this->di->get("login.session");
    }

    /**
     * Get the request object
     * @return Ldap
     */
    public function getLdap()
    {
        return $this->di->get("ldap");
    }

    /**
     * Get the request object
     * @return Plugin
     */
    public function getPlugins()
    {
        return $this->di->get("plugins");
    }

    /**
     * Get the settings object
     * @return Setting
     */
    public function getSettings()
    {
        return $this->di->get("settings");
    }

    /**
     * Get the settings object
     * @return S3Client
     */
    public function getS3Client()
    {
        return $this->di->get("s3client");
    }

    public function getAzureClient()
    {
        return $this->di->get("azureclient");
    }

    /**
     * Setup the view object
     * @return  \BlueSky\Framework\View\Base
     */
    protected function setupView()
    {
        return ViewFactory::createInstance($this, "html");
    }

    /**
     * Setup the view object
     * @return  \BlueSky\Framework\View\Base
     */
    protected function setupLdap()
    {
        return new Ldap($this);
    }

    /**
     * Setup the view object
     * @return  \BlueSky\Framework\View\Base
     */
    protected function setupAzureClient()
    {
        $result = false;
        $key = $this->getSettings()->get('media.adapter.azure_account_key', false);
        $accountName = $this->getSettings()->get('media.adapter.azure_account_name', false);
        $protocol = $this->getSettings()->get('media.adapter.azure_protocol', 'https');
        if ($key && $accountName && $protocol) {
            $conString = sprintf('DefaultEndpointsProtocol=%s;AccountName=%s;AccountKey=%s;', $protocol, $accountName, $key);
            $result = BlobRestProxy::createBlobService($conString);
        }
        return $result;
    }

    /**
     * Setup the view object
     * @return  \BlueSky\Framework\View\Base
     */
    protected function setupS3Client()
    {
        $result = false;
        if ($this->getSettings()->get('media.adapter.aws_key', false)) {
            $result = new S3Client([
                'credentials' => [
                    'key' => $this->getSettings()->get('media.adapter.aws_key'),
                    'secret' => $this->getSettings()->get('media.adapter.aws_secret')
                ],
                'region' => $this->getSettings()->getSettingOrConfig("media.adapter.aws_region", 'ap-southeast-2'),
                'version' => 'latest',
                //'ssl.certificate_authority' => LOTE_LIB_PATH.'res/ca-bundle.crt',
                'scheme' => 'http'
            ]);
        }
        return $result;
    }

    /**
     * Get the CDN cache client
     * @return  S3Client
     */
    public function getS3CloudfrontClient()
    {
        return $this->di->get("s3client.cloudfront");
    }

    /**
     * Setup the CDN cache client
     * @return  S3Client
     */
    protected function setupS3CloudfrontClient()
    {
        return $this->getS3ClientByScope('.cloudfront');
    }

    /**
     * Setup the context object
     * @return  Context
     */
    protected function setupContext()
    {
        return new Context();
    }

    /**
     * Setup the access object
     * @return  Context
     */
    protected function setupAccess()
    {
        return new Access($this);
    }

    /**
     * Setup the account object
     * @return \Lote\App\MasterBase\Entity\Master\Account|false
     * @throws \Exception
     */
    protected function setupAccount()
    {
        $result = false;
        if ($db = $this->getMasterDb()) {
            $e = new AccountEntity($this);
            if ($e->loadByField('reference', $this->getSettings()->get('system.reference'))) {
                $result = $e;
            } else {
                throw new \Exception("Unable to locate account:" . $this->getSettings()->get('system.reference'));
            }
        }
        return $result;
    }

    /**
     * Setup the data object
     * @return Data
     */
    protected function setupSystemData()
    {
        $d = new Data();
        return $d;
    }

    /**
     * Setup the identifier object
     * @return LoginSession
     */
    protected function setupLoginSession()
    {
        return new LoginSession($this);
    }

    /**
     * Setup the identifier object
     * @return Setting
     */
    protected function setupSettings()
    {
        return new Setting($this);
    }

    /**
     * Setup the content plugin
     * @return Setting
     */
    protected function setupPlugins()
    {
        return new Plugin($this);
    }

    /**
     * Setup the audit object
     * @return AuditLog
     */
    protected function setupAudit()
    {
        return new AuditLog($this);
    }

    /**
     * Setup the user object
     * @return User
     */
    protected function setupUser()
    {
        $u = new User($this);

        $u->setAuditing(false);
        return $u;
    }

    /**
     * Setup the groups model
     * @return GroupService
     */
    protected function setupGroupService()
    {
        return new GroupService($this);
    }

    /**
     * Setup the user login session
     * @return AuthSession
     */
    protected function setupAuthSession()
    {
        return new AuthSession($this);
    }

    /**
     * Setup the view object
     * @return array
     */
    protected function overrideRoute()
    {
        return $this->route;
    }

    /**
     * Assign
     * @param array $newRouteData
     */
    public function setRouteData($newRouteData)
    {
        $this->route = $newRouteData;
        $this->di->set("route", function () {
            return $this->overrideRoute();
        });
    }

    /**
     * Create the request object
     */
    protected function setupRequest()
    {
        //implement in child class
        return false;
    }

    /**
     * Create the response object
     */
    protected function setupResponse()
    {
        //implement in child class
        return false;
    }

    protected function setupParentAccount()
    {
        return new ParentAccount($this);
    }

    protected function setupSuperParentAccount()
    {
        $pa = new ParentAccount($this);
        $pa->loadSuperParentAccount();
        return $pa;
    }

    /**
     * Get the DI container for this state
     * @return \Aura\Di\Container
     * */
    public function getDi()
    {
        return $this->di;
    }

    /**
     * Setup the signal manager
     * @access private
     * @return \Aura\Signal\Manager
     * */
    private function setupSignal()
    {
        $signalManager = new Signal\Manager(new Signal\HandlerFactory, new Signal\ResultFactory,
            new Signal\ResultCollection);
        $this->getApps()->setupSignals($signalManager);
        return $signalManager;
    }

    /**
     * Get the signal manager
     * @access private
     * @return \Aura\Signal\Manager
     * */
    public function getSignal()
    {
        return $this->di->get('signal');
    }

    /**
     * Get the audit object
     * @return AuditLog
     */
    public function getAudit()
    {
        return $this->di->get('audit');
    }

    /**
     * Setup the sites object
     * @return  Context
     */
    protected function setupSites()
    {
        return new Site($this);
    }

    /**
     * Get the site setup for this install
     * @return Site
     * */
    public function getSites()
    {
        return $this->di->get("site");
    }

    /**
     * Get the user
     * @access private
     * @return User
     * */
    public function getUser()
    {
        return $this->di->get('user');
    }

    /**
     * Get the group service object
     * @access private
     * @return GroupService
     * */
    public function getGroupService()
    {
        return $this->di->get('group.service');
    }

    /**
     * Get the user
     * @access private
     * @return AuthSession
     * */
    public function getAuthSession()
    {
        return $this->di->get('auth.session');
    }

    /**
     * Get the locale for this request. This function returns 'en' as a default, with the Web and Cli child classes
     * overriding this with more specific functionality
     * @return string
     * */
    protected function getLocale()
    {
        return 'en';
    }

    /**
     * Setup the translator, loading the base translation files plus the system and module specific ones
     * @access private
     * @return Translator
     * */
    private function setupTranslator()
    {
        $translator = new Translator($this->getLocale());
        $translator->setFallbackLocales(["en"]);
        $translator->addLoader('php', new PhpFileLoader());

        foreach ($this->getApps()->getAll() as $v) {
            /** @var $v \BlueSky\Framework\Application\Config */
            $this->addTranslationFilesByDirectory($translator, $v->getDirectory() . 'res/language/');
        }
        $this->addTranslationFilesByDirectory($translator, LOTE_APP_PATH . 'res/language/');
        return $translator;
    }

    /**
     * Add translation files from a specified directory
     * @access private
     * @param Translator $translator
     * @param $directory
     * @return void
     */
    private function addTranslationFilesByDirectory(Translator $translator, $directory)
    {
        $translationFiles = $this->getTranslationFiles($directory);
        foreach ($translationFiles as $file) {
            /**
             * @var $file \SplFileInfo
             * */
            $fileName = $file->getBasename('.php');
            $fileParts = explode('.', $fileName);
            if (count($fileParts) == 2) {
                $translator->addResource('php', $file->getPathname(), $fileParts[1]);
            }
        }
    }

    /**
     * Get the translation files from a specified directory
     * @param string $directory
     * @access private
     * @return array
     * */
    protected function getTranslationFiles($directory)
    {
        $result = [];
        if (is_dir($directory)) {
            $fileIterator = new \FilesystemIterator($directory);
            $regexIterator = new \RegexIterator($fileIterator, '#.(php)$#');
            /* @var $file \SplFileInfo */
            foreach ($regexIterator as $file) {
                $result[] = $file;
            }
        }
        return $result;
    }

    /**
     * Get the translation object
     * @return  \Symfony\Component\Translation\Translator
     */
    public function getTranslator()
    {
        return $this->di->get("translator");
    }

    /**
     * Create a database connection for use in writing
     * @access private
     * @return \Doctrine\DBAL\Connection - the database connection
     */
    private function setupWriteDatabase()
    {
        return $this->setupDatabase('write');
    }

    /**
     * Create a database connection for use for reading
     * @access private
     * @return \Doctrine\DBAL\Connection - the database connection
     * @todo - implement multiple database read connection support
     */
    private function setupReadDatabase()
    {
        return $this->setupDatabase('read');
    }

    /**
     * Initialise the configuration variables
     * @access private
     * @return Config - the config object
     */
    private function setupConfig()
    {
        return new Config($this->configReference);
    }

    /**
     * Get the configuration variables
     * @access private
     * @return Config
     */
    public function getConfig()
    {
        return $this->di->get("config");
    }

    /**
     * Create a database connection to the master database
     * @access private
     * @return \Doctrine\DBAL\Connection - the database connection
     */
    private function setupMasterDatabase()
    {
        $result = false;
        $res = $this->getConfig()->get();
        if (isset($res['database.master']) && isset($res['database.master']['type'])) {
            return $this->connectionManager->createConnection($res['database.master']);
        }
        return $result;
    }

    /**
     * Create a database connection to the parent database
     * @access private
     * @return \Doctrine\DBAL\Connection - the database connection
     */
    private function setupParentDatabase()
    {
        $result = false;
        $a = new Account($this);
        if ($parent = $a->getParentAccount($this->getSettings()->get('system.reference'))) {
            $res = $this->getConfig()->get();
            if (isset($res['database.master'])) {
                $config = $res['database.master'];
                $params = [];
                $params['type'] = 'pdo_mysql';
                $params['host'] = $config['host'];
                $params['name'] = $parent['db_name'];
                $params['user'] = $config['user'];
                $params['pass'] = $config['pass'];
                if (!empty($config['server_version'])) {
                    $params['server_version'] = $config['server_version'];
                }
                $result = $this->connectionManager->createConnection($params);
            }
        }
        return $result;
    }

    /**
     * Create a database connection to the superparent database
     * @access private
     * @return \Doctrine\DBAL\Connection - the database connection
     */
    private function setupSuperParentDatabase()
    {
        $result = false;
        $a = new Account($this);
        if ($parent = $a->getSuperParentAccount($this->getSettings()->get('system.reference'))) {
            $res = $this->getConfig()->get();
            if (isset($res['database.master'])) {
                $config = $res['database.master'];
                $params = [];
                $params['type'] = 'pdo_mysql';
                $params['host'] = $config['host'];
                $params['name'] = $parent['db_name'];
                $params['user'] = $config['user'];
                $params['pass'] = $config['pass'];
                $result = $this->connectionManager->createConnection($params);
            }
        }
        return $result;
    }

    /**
     * Create a database connection based on the state (live, dev, test) and the type (read or write)
     * @access private
     * @param string $type
     * @return \Doctrine\DBAL\Connection - the database connection
     */
    private function setupDatabase($type)
    {
        $res = $this->getConfig()->get();
        if (isset($res['database.replicated']) && $res['database.replicated'] == "1") {
            $database = $this->connectionManager->createConnection($res['database'][$type]);
        } else {
            $database = $this->connectionManager->createConnection($res['database']);
        }
        return $database;
    }

    /**
     * Get the database connection used for reading
     * @access public
     * @return \Doctrine\DBAL\Connection - the database connection
     */
    public function getReadDb()
    {
        return $this->di->get("database.read");
    }

    /**
     * Get the database connection used for writing
     * @access public
     * @return \Doctrine\DBAL\Connection - the database connection
     */
    public function getWriteDb()
    {
        return $this->di->get("database.write");
    }

    /**
     * Get the master database connection
     * @access public
     * @return \Doctrine\DBAL\Connection - the database connection
     */
    public function getMasterDb()
    {
        return $this->di->get("database.master");
    }

    /**
     * Get the parent database connection
     * @access public
     * @return \Doctrine\DBAL\Connection - the database connection
     */
    public function getParentDb()
    {
        return $this->di->get("database.parent");
    }

    /**
     * Get the superparent database connection
     * @access public
     * @return \Doctrine\DBAL\Connection - the database connection
     */
    public function getSuperParentDb()
    {
        return $this->di->get("database.superparent");
    }

    /**
     * Get child database connection
     * @access public
     * @return \Doctrine\DBAL\Connection - the database connection
     */
    public function getChildren()
    {
        $a = new Account($this);
        return $a->getAllPushDownAccounts($this->getSettings()->get('system.reference'));
    }

    public function getChildDb($system = '')
    {
        $result = false;
        $childrenArr = $this->getChildren();
        if (is_array($childrenArr)) {
            foreach ($childrenArr as $child) {
                $reference = $child['reference'];

                if ($system == $reference) {
                    /*
                     * @TODO Need to get database setting by reading the ini file corresponding to the selected client. This is not needed if the db settings are same for both parent and client.
                     */
                    $childDb = $child['db_name'];
                    if (!empty($childDb)) {
                        $sm = $this->getMasterDb()->getSchemaManager();
                        $databases = $sm->listDatabases();
                        if (array_search($childDb, $databases)) {
                            $res = $this->getConfig()->get();
                            $config = $res['database.master'];
                            $params = [];
                            $params['type'] = 'pdo_mysql';
                            $params['host'] = $config['host'];
                            $params['name'] = $childDb;
                            $params['user'] = $config['user'];
                            $params['pass'] = $config['pass'];
                            $result = $this->connectionManager->createConnection($params);
                        }
                    }
                }
            }
        }
        return $result;
    }

    /**
     * @param string $reference
     * @return false|\Doctrine\DBAL\Connection
     * */
    public function getDescendantDb($reference)
    {
        $result = false;
        $m = new Account($this);
        if ($m->isDescendantOf($this->accountReference, $reference)) {
            if ($descendant = $m->getAccountByReference($reference)) {
                $result = $this->getDbConnectionByName($descendant['db_name']);
            }
        }
        return $result;
    }

    private function getDbConnectionByName($dbName)
    {
        $result = false;
        if (!empty($dbName)) {
            $sm = $this->getMasterDb()->getSchemaManager();
            $databases = $sm->listDatabases();
            if (in_array($dbName, $databases)) {
                $res = $this->getConfig()->get();
                $config = $res['database.master'];
                $params = [];
                $params['type'] = $config['type'] ? $config['type'] : 'pdo_mysql';
                $params['host'] = $config['host'];
                $params['name'] = $dbName;
                $params['user'] = $config['user'];
                $params['pass'] = $config['pass'];
                try {
                    $result = $this->connectionManager->createConnection($params);
                } catch (\Exception $e) {

                }
            }
        }
        return $result;
    }

    /**
     * Get the site Application configuration
     * @access public
     * @return Composition
     */
    public function getApps()
    {
        return $this->di->get("apps");
    }

    /**
     * Check if the system is installed in the current database
     *
     * @access public
     * @return boolean
     * */
    public function isInstalled(): bool
    {
        return
            $this->getWriteDb()->getSchemaManager()->tablesExist(['sb__app', 'sb__schema_entity', 'sb__schema_field', 'sb__schema_relation']) &&
            $this->getWriteDb()->getSchemaManager()->listTableDetails('sb__app')->hasColumn('id') &&
            $this->getWriteDb()->getSchemaManager()->listTableDetails('sb__schema_entity')->hasColumn('id') &&
            $this->getWriteDb()->getSchemaManager()->listTableDetails('sb__schema_field')->hasColumn('id') &&
            $this->getWriteDb()->getSchemaManager()->listTableDetails('sb__schema_relation')->hasColumn('id');
    }

    /**
     * This checks that the system installed has the latest updates
     * This function assumes that isInstalled has already resolved true
     * @return bool
     * @see isInstalled
     */
    public function isValidInstalled(): bool
    {
        $output = $this->getCoreSchemaDiff($this->getApps()->has("master"));
        return empty($output);
    }

    /**
     * This function should be called if isInstalled resolves false
     * @param bool $sync - whether to sync the changes to the db or not
     * @see isInstalled
     */
    public function installCoreSchema(SymfonyStyle $io, bool $sync = true): void
    {
        $changes = $this->getCoreSchemaDiff($this->getMasterState()->accountReference === $this->accountReference);
        $this->executeCoreSchemaChanges($io, $changes, $sync);
    }

    /**
     * This function should be called if isValidInstalled resolves false
     * @param bool $sync - whether to sync the changes to the db or not
     * @see isValidInstalled
     */
    public function updateCoreSchema(SymfonyStyle $io, bool $sync = true): void
    {
        $changes = $this->getCoreSchemaDiff($this->getApps()->has("master"));
        $this->executeCoreSchemaChanges($io, $changes, $sync);
    }

    /**
     * This function should only be used to migrate the changes of the core tables that the rest of the system is dependent on
     *
     * @param string[] $changes - the migration changes to the new structure
     * @param bool $sync - whether the changes should be made to the db or not
     */
    private function executeCoreSchemaChanges(SymfonyStyle $io, array $changes, bool $sync): void
    {
        foreach ($changes as $change) {
            if ($sync) {
                try {
                    $this->getWriteDb()->exec($change);
                } catch (DBALException $e) {
                    echo($e->getMessage() . PHP_EOL);
                }
            } else {
                if (Strings::startsWith("drop", strtolower($change))) {
                    $io->writeln("<fg=red>{$change}</>");
                } elseif (Strings::startsWith("alter", strtolower($change))) {
                    $io->writeln("<comment>{$change}</comment>");
                } else {
                    $io->writeln("<info>{$change}</info>");
                }
            }
        }
    }

    /**
     * This function is a wrapper for the migration function call to check whether the code and the db are in track
     *
     * @param bool $isMaster
     * @return string[]
     */
    private function getCoreSchemaDiff(bool $isMaster): array
    {
        $output = [];
        try {
            $output = $this->getExistingCoreSchema()->getMigrateToSql($this->createCoreCodeSchema(), $this->getWriteDb()->getDatabasePlatform());
        } catch (DBALException $e) {
            echo($e->getMessage() . PHP_EOL);
        }

        return $output;
    }

    /**
     * This function creates the schema based on the current code .php files
     *
     * @param bool $isMaster
     * @return DBSchema
     */
    private function createCoreCodeSchema(): DBSchema
    {
        $newSchema = new DBSchema();
        $appSchema = new AppSchema();

        $appSchema->getSchema($newSchema, AppEntity::class, false);
        $appSchema->getSchema($newSchema, SchemaEntityEntity::class, false);
        $appSchema->getSchema($newSchema, SchemaFieldEntity::class, false);
        $appSchema->getSchema($newSchema, SchemaRelationEntity::class, false);

//        ddd($newSchema->toSql($this->getWriteDb()->getDatabasePlatform()));

        return $newSchema;
    }

    /**
     * This function creates the schema based on the current db setup tables
     *
     * @return DBSchema
     */
    private function getExistingCoreSchema(): DBSchema
    {
        $coreTables = [
            $this->getWriteDb()->getSchemaManager()->listTableDetails('sb__app'),
            $this->getWriteDb()->getSchemaManager()->listTableDetails('sb__schema_entity'),
            $this->getWriteDb()->getSchemaManager()->listTableDetails('sb__schema_field'),
            $this->getWriteDb()->getSchemaManager()->listTableDetails('sb__schema_relation')
        ];

        /** @var Table[] $coreTables */
        for ($i = 0; $i < count($coreTables); $i++) {
            if (!$coreTables[$i]->hasColumn('id')) {
                unset($coreTables[$i]);
            }
        }

        $existingSchema = new DBSchema($coreTables);

        return $existingSchema;
    }

    /**
     * Setup the installed site systems and plugins
     * @return Composition
     * */
    private function setupApps()
    {
        $isInstalled = $this->isInstalled();
        $c = new Composition($this, $isInstalled);
        if ($isInstalled) {
            $c->setupAccess($this->getContext());
            $c->setupPlugins();
            //$c->setupData(); //infinite loop with translations
            $c->setupClosures();
        }
        return $c;
    }

    /**
     * Get the view object
     * @return  \BlueSky\Framework\View\Base
     */
    public function getView()
    {
        return $this->di->get("view");
    }

    /**
     * Get the context object
     * @return  Context
     */
    public function getContext()
    {
        return $this->di->get("context");
    }

    /**
     * Get the access object
     * @return  Access
     */
    public function getAccess()
    {
        return $this->di->get("access");
    }

    /**
     * Get the route for this request, specifying the system, controller, action and parameters
     * @return array
     * */
    public function getRoute()
    {
        return $this->di->get("route");
    }

    /**
     * Get parent account.
     * @return ParentAccount
     */
    public function getParentAccount()
    {
        return $this->di->get('parent.account');
    }

    /**
     * Get parent account.
     * @return ParentAccount
     */
    public function getSuperParentAccount()
    {
        return $this->di->get('parent.super.account');
    }

    /**
     * setup the account config file system
     * @return Filesystem
     * */
    public function setupAccountConfigFileSystem()
    {
        $s3Client = new S3Client([
            'credentials' => [
                'key' => $this->getConfig()->get("client.aws.config.key"),
                'secret' => $this->getConfig()->get("client.aws.config.secret")
            ],
            'region' => $this->getConfig()->get("client.aws.config.region", 'ap-southeast-2'),
            'version' => 'latest',
            'scheme' => 'http'
        ]);
        $adapter = new AwsS3Adapter($s3Client, $this->getConfig()->get("client.aws.config.bucket"),
            $this->getConfig()->get("client.aws.config.prefix"));
        return new Filesystem($adapter);
    }

    /**
     * Get the account config file system
     * @return Filesystem
     * */
    public function getAccountConfigFileSystem()
    {
        return $this->di->get("client.account.config.file_system");
    }

    /**
     * Get the parent state
     * @return Base
     * */
    public function setupParentState()
    {
        $className = get_class($this);
        return new $className($this->getParentAccount()->getReference());
    }

    /**
     * Get the parent state
     * @return Base
     * */
    public function setupSuperParentState()
    {
        $className = get_class($this);
        return new $className($this->getSuperParentAccount()->getReference());
    }

    /**
     * Get the master state
     * @return Base
     * */
    public function setupMasterState()
    {
        $className = get_class($this);
        return new $className(null, true);
    }

    /**
     * Get the parent state
     * @return Base
     * */
    public function getParentState()
    {
        return $this->di->get("parent.state");
    }

    /**
     * Get the parent state
     * @return Base
     * */
    public function getSuperParentState()
    {
        return $this->di->get("parent.super.state");
    }

    /**
     * Get the parent state
     * @return Base
     * */
    public function getMasterState()
    {
        return $this->di->get("master.state");
    }

    /**
     * setup the URL Service
     * @return Url
     * */
    public function setupUrl()
    {
        return new Url($this);
    }

    /**
     * Get the parent state
     * @return Url
     * */
    public function getUrl()
    {
        return $this->di->get("url");
    }

    /**
     * setup the ImageServer Service
     * @return ImageServer
     * */
    public function setupImageServer()
    {
        return new ImageServer($this);
    }

    /**
     * Get the parent state
     * @return ImageServer
     * */
    public function getImageServer()
    {
        return $this->di->get("imageserver");
    }

    /**
     * Setup the Closure service
     * @return Closure
     * */
    public function setupClosures()
    {
        return new Closure($this);
    }

    /**
     * Get the Closure service
     * @return Closure
     * */
    public function getClosures()
    {
        return $this->di->get("closures");
    }

    /**
     * Setup the Timer service
     * @return Timer
     * */
    public function setupTimer()
    {
        return new Timer($this);
    }

    /**
     * Get the Closure service
     * @return Timer
     * */
    public function getTimer()
    {
        return $this->di->get("timer");
    }

    /**
     * Setup the Event Manager Service
     * @return Manager
     * */
    public function setupEventManager()
    {
        $result = new Manager($this);
        $this->getApps()->registerEvents($result);
        return $result;
    }

    /**
     * Get the Closure service
     * @return Manager
     * */
    public function getEventManager()
    {
        return $this->di->get("event.manager");
    }

    /**
     * Setup the Debug Service
     * @return Debug
     * */
    public function setupDebug()
    {
        return new Debug($this);
    }

    /**
     * Get the Closure service
     * @return Debug
     * */
    public function getDebug()
    {
        return $this->di->get("debug");
    }

    /**
     * Setup the Debug Service
     * @return Schema
     * */
    public function setupSchema()
    {
        $schema = new Schema($this);
        return $schema;
    }

    /**
     * Get the schema service
     * @return Schema
     * */
    public function getSchema()
    {
        return $this->di->get("schema");
    }

    /**
     * Setup the Entity Manager Service
     * @return EntityManager
     * */
    public function setupEntityManager()
    {
        return new EntityManager($this);
    }

    /**
     * Get the entity manager
     * @return EntityManager
     * */
    public function getEntityManager()
    {
        return $this->di->get("entity.manager");
    }

    /**
     * Check if the current state is CLI based
     * */
    public function isCli()
    {
        return php_sapi_name() == 'cli';
    }

    /**
     * Get the url router
     * @return \Symfony\Component\Routing\RouteCollection
     *
     * @throws \Aura\Di\Exception\ServiceNotFound
     */
    public function getRouter()
    {
        return $this->di->get("router");
    }

    private function setupDevHelper()
    {
        return new DevHelper($this);
    }

    public function getDevHelper(): DevHelper
    {
        return $this->di->get('dev.helper');
    }

    /**
     * Setup the core routes of the system
     * */
    public function setupCoreRoutes(RouteCollection $r)
    {
        $defaults = ['_handler' => ['system' => 'core']];
        $defaults['controller'] = 'BlueSky\Framework\Controller\Core\Image';
        $defaults['_method'] = 'renderImage';
        $defaults['_static'] = true;
        $route = new Route('/_img/{id}', $defaults, ['id' => '([0-9]+)']);
        $r->add("Image View and Thumbnail", $route);
        /** @Depracated - _thumb was not released due to potentially breaking other projects */
        $route = new Route('_thumb/{id}', $defaults);
        $r->add('core.thumnail.by_id', $route);

        /** @see \BlueSky\Framework\Controller\Core\Image::renderImageByReference */
        $defaults['_method'] = 'renderImageByReference';
        $route = new Route('/_image/{reference}', $defaults, ['reference' => '.*']);
        $r->add("Image View and Thumbnail By Reference", $route);


        $defaults = ['_handler' => ['system' => 'website']];

        $defaults['controller'] = 'BlueSky\Framework\Controller\Core\File';

        /** @see \BlueSky\Framework\Controller\Core\File::download */
        $defaults['_method'] = 'download';
        $route = new Route('_d/{id}', $defaults);
        $r->add('core.download.by_id', $route);

        /** @see \BlueSky\Framework\Controller\Core\File::preview() */
        $defaults['_method'] = 'preview';
        $route = new Route('_p/{id}', $defaults);
        $r->add('core.preview.by_id', $route);

        /** @see \BlueSky\Framework\Controller\Core\File::downloadByReference */
        $defaults['_method'] = 'downloadByReference';
        $route = new Route('_file/{reference}', $defaults, ['reference' => '.*']);
        $r->add('core.download.by_reference', $route);

        /** @see \BlueSky\Framework\Controller\Core\File::attachmentDownloadByReference */
        $defaults['_method'] = 'attachmentDownloadByReference';
        $route = new Route('_download/{reference}', $defaults, ['reference' => '.*']);
        $r->add('core.download.by_reference_attachment', $route);

        unset($defaults['_static']);

        /** @see \BlueSky\Framework\Controller\Core\Util::ping() */
        $requirements = ['format' => '(\.[a-z]+)?'];
        $defaults = ['controller' => 'BlueSky\Framework\Controller\Core\Util', '_method' => 'ping'];
        $route = new Route('/_util/keep-alive{format}', $defaults, $requirements);
        $r->add("core.keep_alive", $route);

        $defaults['controller'] = 'BlueSky\Framework\Controller\Login';
        $requirements = ['login_hash' => '([a-zA-Z0-9]+)', 'user_hash' => '([a-zA-Z0-9]+)'];
        $defaults['_method'] = 'autoLogin';
        $route = new Route('/_l/{login_hash}/{user_hash}', $defaults, $requirements);
        $r->add(uniqid('Auto Login Redirect'), $route);

        /** @see \BlueSky\Framework\Controller\Access\Url\Manage::urlAccess() */
        $defaults['controller'] = 'BlueSky\Framework\Controller\Access\Url\Manage';
        $requirements = ['encoded_url' => '(.+)'];
        $defaults['_method'] = 'urlAccess';
        $route = new Route('/_access/password-restricted/{encoded_url}', $defaults, $requirements);
        $r->add(uniqid('Url Access Verification'), $route);

        /** @see \BlueSky\Framework\Controller\Access\Url\Form\Password */
        $defaults['_method'] = 'validate';
        $defaults['controller'] = 'BlueSky\Framework\Controller\Access\Url\Form\Password';
        $route = new Route('/_access/password-validate{format}', $defaults, ['format' => '(\.[a-z]+)?']);
        $r->add(uniqid('Url Access Verification Password Check'), $route);
    }

    public function setupLoginVariables(): \BlueSky\Framework\Auth\Login\Variables\Variable
    {
        return VariablesFactory::createInstance($this);
    }

    public function getLoginVariables(): VariableInterface
    {
        return $this->di->get("login.variables");
    }

    public function getBrowscap(): Browscap
    {
        return $this->di->get("browscap");
    }

    protected function setupBrowscap(): Browscap
    {
        return new Browscap();
    }


}
