<?php
namespace BlueSky\Framework\State;

//use Lote\Module\Analytics\Service\Analytics;
use Lote\App\Page\Website\Model\Page as PageModel;
use BlueSky\Framework\Event\Data;
use BlueSky\Framework\Model\Url;
//use BlueSky\Framework\Model\UrlPassword;
use BlueSky\Framework\Service\Browscap\Browscap;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\RequestContext;
use BlueSky\Framework\Routing\RouteCollection;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use BlueSky\Framework\View\Factory as ViewFactory;

class Web extends Base
{

    /**
     *  */
    public function setup()
    {
        parent::setup();
        $this->di->set(
            "request",
            function () {
                return $this->setupRequest();
            }
        );
        $this->di->set(
            "router",
            function () {
                return $this->setupRouter();
            }
        );
        $this->di->set(
            "route",
            function () {
                return $this->setupRoute();
            }
        );
        /*$this->di->set(
            "analytics",
            function () {
                return $this->setupAnalytics();
            }
        );*/
    }

    /**
     * Create the request object
     * @return Request
     */
    protected function setupRequest()
    {
        $r = Request::createFromGlobals();
        if (isset($_COOKIE['sessionid'])) {
            session_id($_COOKIE['sessionid']);
        }
        $s = new Session();
        if (isset($_COOKIE['sessionid'])) {
            session_id($_COOKIE['sessionid']);
            $s->setId($_COOKIE['sessionid']);
        }
        if (!$s->getId()) {
            $s->start();
        }
        $r->setSession($s);
        return $r;
    }

    /**
     * Setup the analytics object
     * @return Analytics
     */
    /*protected function setupAnalytics()
    {
        return new Analytics($this);
    }*/

    /**
     * Setup the view object
     * @return  \BlueSky\Framework\View\Base
     */
    protected function setupView()
    {
        $route = $this->getRoute();
        $reference = "html";
        if (isset($route['format'])) {
            $reference = preg_replace('/^(\.)(.*)/', '\2', $route['format']);
        } else {
            $acceptType = $this->getRequest()->getAcceptableContentTypes();
            if (isset($acceptType[0]) && $acceptType[0]) {
                $reference = $acceptType[0];
            }
        }
        return ViewFactory::createInstance($this, $reference);
    }

    /**
     * @return RouteCollection
     * */
    private function setupRouter()
    {
        $router = new RouteCollection();
        return $router;
    }

    public function hasStaticMatch($url)
    {
        $result = false;
        $context = new RequestContext();
        $context->fromRequest($this->getRequest());
        $matcher = new UrlMatcher($this->getRouter(), $context);
        try {
            $result = $matcher->match($url);
        } catch (\Exception $e) {
            //do nothing
        }
        return $result;
    }

    /**
     * Get a dynamic match for a URL
     * */
    public function getDynamicMatch($url)
    {
        $controllers = [];
        $controllers['sb_page_item'] = 'page.website.view';
        $controllers['media.file'] = 'media.website.download';
        $controllers['sb_cms_news_article'] = 'Website News Article View';
        $controllers['sb_news_article'] = 'Website News Article View';
        $controllers['sb_blog_post'] = 'Website Blog Item View';
        $controllers['blog'] = 'Website Blog Items';
        $controllers['sb_seotools_file'] = 'seotools.website.file.view';
        $controllers['sb_cms_product_folder'] = 'Website Product Folder View';
        $controllers['sb_cms_product_item'] = 'Website Product Item View';
        $controllers['sb_cms_staff_item'] = 'Website Staff Member View';
        $controllers['sb_newsletter_publication'] = 'newsletter.publication.website.view';
        $controllers['sz_newsletter'] = 'Website SZ Newsletter View';
        $controllers['sb_newsletter'] = 'newsletter.publication.website.view';
        $controllers['sb_directory_item'] = 'Website Directory Item View';
        $controllers['sb_directory_folder'] = 'Website Directory Folder View';
        $controllers['sb_cms_gallery_album'] = 'Website Gallery View';
        $controllers['sb_gallery_album'] = 'Website Gallery View';

        $controllers['sb_mullins_services_item'] = 'Website Mullins Services View';
        $controllers['sb_mullins_services_category'] = 'Website Mullins Services Category View';

        $controllers['sb_nw_apartment'] = 'website.nwapartment.apartments.apartment.view';
        $controllers['sb_nw_apartment_building'] = 'website.nwapartment.apartments.building.view';
        $controllers['sb_nw_apartment_floors'] = 'website.nwapartment.apartments.floor.view';

        $controllers['sb_realestate_release'] = 'property.website.release.view';
        $controllers['sb_realestate_property'] = 'property.website.view';

        $controllers['sb_business_key_directory'] = 'website.businesskey.directory.view';

        $result = [];
        $urlModel = new Url($this);
        if ($match = $urlModel->match($url, $this->getSites()->getCurrentSite()->id)) {
            if (isset($controllers[$match['object_ref']]) && $route = $this->getRouter()->get($controllers[$match['object_ref']])) {
                if (!empty($match['data'])) {
                    $result = $route->getDefaults();
                    $result['_dynamic'] = true;
                    $data = json_decode($match['data'], true);
                    foreach ($data as $k => $v) {
                        $result[$k] = $v;
                    }
                } else {
                    $result = $route->getDefaults();
                    $result['_dynamic'] = true;
                    $result['id'] = $match['object_id'];
                    if($match['object_ref'] == 'blog'){
                        $result['blog_id'] = $match['object_id'];
                    }
                }
            }
        }
        return $result;
    }


    public function getDefaultMatch()
    {
        $result = false;
        $m = new PageModel($this);
        if ($route = $m->getDefaultRoute()) {
            $result = $route;
        }
        return $result;
    }

    public function noDefaultRouteFound()
    {
        $eventData = new Data([], []);
        $this->getEventManager()->dispatch('route.no_default_found', $eventData);
        $route = $eventData->getData("route");
        if (!$route) {
            $route = [];
            $route['controller'] = 'Lote\Landing';
            $route['_method'] = 'noDefaultRouteFound';
        }
        return $route;
    }

    public function getRequestUrl($removeParams = true)
    {
        $requestUri = $this->getRequest()->getRequestUri();
        if ($removeParams) {
            $requestUri = preg_replace('/\?.*/', '', $requestUri);
        }
        $basePath = $this->getRequest()->getBasePath();
        $result = str_replace($basePath, "", $requestUri);
        if ($result != '/') {
            $result = rtrim($result, '/');
        }
        $result = preg_replace('#//(.*)#', '/\1', $result);
        return $result;
    }

    protected function tryStaticMatch($url)
    {
        $context = new RequestContext();
        $context->fromRequest($this->getRequest());
        $matcher = new UrlMatcher($this->getRouter(), $context);
        try {
            $result = $matcher->match($url);
            if ($result) {
                if ($customMethod = $this->getRequest()->get("_method", false)) {
                    $result['_method'] = $customMethod;
                }
            }
        } catch (\Exception $e) {
            //do a phpcr service route check
            $result = false;
        }
        return $result;
    }

    protected function getStaticMatch()
    {
        if (!$result = $this->tryStaticMatch($this->getRequestUrl(false))) {
            $result = $this->tryStaticMatch($this->getRequestUrl(true));
        }
        return $result;
    }

    /**
     * Setup the view object
     * @return  Array
     */
    protected function setupRoute()
    {
        $url = $this->getRequestUrl();
        if (!$result = $this->getStaticMatch()) {
            $result = $this->getDynamicMatch($url);
        }
        if (!$result) {
            if ($url == '/' && $this->getApps()->has('cms') && $this->getApps()->has('page')) {
                $result = $this->getDefaultMatch();
                if (!$result) {
                    $result = $this->noDefaultRouteFound();
                }
            }
        } else {
            if ($url == '/') {
                $result['url'] = $this->getRequest()->getSchemeAndHttpHost() . '/';
            } else {
                $result['url'] = $this->getRequest()->getSchemeAndHttpHost() . '/' . $url;
            }
        }
        return $result;
    }

    /**
     * Get the url router
     * @return \Symfony\Component\Routing\RouteCollection
     * */
    public function getRouter()
    {
        return $this->di->get("router");
    }

    /**
     * Get the request object
     * @return \Symfony\Component\HttpFoundation\Request
     */
    public function getRequest()
    {
        return $this->di->get("request");
    }

    /**
     * Get the analytics object
     * @return Analytics
     */
    public function getAnalytics()
    {
        return $this->di->get("analytics");
    }

    /**
     * Get the locale for this request
     * @return String
     */
    protected function getLocale()
    {
        return $this->getRequest()->getLocale();
    }

    public function getAppByUrl($url, $default = 'website')
    {
        $result = $default;
        do {
            if ($match = $this->hasStaticMatch($url)) {
                if (isset($match['_handler']['app'])) {
                    $result = $match['_handler'];
                    if (is_array($result) && isset($result['app'])) {
                        $result = $result['app'];
                    }
                    break;
                }
            }
            $url = substr($url, 0, strrpos($url, '/'));
        } while ($url);
        return $result;
    }

}
