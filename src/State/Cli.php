<?php
namespace BlueSky\Framework\State;

use Aura\Cli\CliFactory;
use Aura\Cli\Context as CliContext;
use BlueSky\Framework\Console\Application;
use BlueSky\Framework\Routing\RouteCollection;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;


class Cli extends Base
{

    /**
     * @throws \Exception
     */
    public function setup()
    {
        parent::setup();
        $this->di->set(
            "request",
            function () {
                return $this->setupRequest();
            }
        );
        //$this->di->set("response", function(){return new Response();});
        //$this->di->set("router", function(){return $this->setupRouter();});
        $this->di->set(
            "route",
            function () {
                return $this->setupRoute();
            }
        );
        $this->di->set(
            "console.application",
            function () {
                return $this->setupConsoleApplication();
            }
        );
        $this->di->set(
            "router",
            function () {
                return $this->setupRouter();
            }
        );
    }

    /**
     * @return RouteCollection
     * */
    private function setupRouter()
    {
        $router = new RouteCollection();
        $this->setupCoreRoutes($router);
        $this->getApps()->setupRoutes($router);
        return $router;
    }

    public function hasStaticMatch($url)
    {
        $result = false;
        $context = new RequestContext($url);
        $matcher = new UrlMatcher($this->getRouter(), $context);
        try {
            $result = $matcher->match($url);
        } catch (\Exception $e) {
            //do nothing
        }
        return $result;
    }

    /**
     * Get the url router
     * @return \Symfony\Component\Routing\RouteCollection
     *
     * @throws \Aura\Di\Exception\ServiceNotFound
     */
    public function getRouter()
    {
        return $this->di->get("router");
    }

    /**
     * Create the request object
     * @return Application
     * @todo - Implement a proper check in the state to determine if a db connection has been defined
     */
    protected function setupConsoleApplication()
    {
        $c = new Application();

        $dispatcher = new EventDispatcher();
        $c->setDispatcher($dispatcher);

        $c->getDefinition()->addOptions([
            new InputOption(
                'reference',
                'r',
                InputOption::VALUE_OPTIONAL,
                'The account reference to use',
                ''
            )
        ]);

        $c->setState($this);
        $frameworkCommandDir = realpath(str_replace("\\", "/",__DIR__).'/../Console/Command');
        $c->addCommandsFromPath($frameworkCommandDir, 'BlueSky\Framework\Console\Command');
        $projectCommandDir = realpath(str_replace("\\", "/",LOTE_APP_PATH) . 'src/Command');
        $c->addCommandsFromPath($projectCommandDir, APP_NAMESPACE . 'Command');
        try {
            $this->getApps()->setupConsole($c);
        }
        catch(\Throwable $t) {
            //dump($t);
            //die;
        }
        return $c;
    }

    public function isCli()
    {
        return true;
    }

    /**
     * Create the request object
     * @return Application
     */
    public function getConsoleApplication()
    {
        return $this->di->get("console.application");
    }

    /**
     * Create the request object
     * @return CliContext
     */
    protected function setupRequest()
    {
        $c = new CliFactory();
        return $c->newContext($GLOBALS);
    }

    /**
     * Get the request object
     * @return CliContext
     */
    public function getRequest()
    {
        return $this->di->get("request");
    }

    /**
     * Setup the view object
     * @return array
     */
    protected function setupRoute()
    {
        return [];
    }

}
