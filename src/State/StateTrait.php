<?php
declare(strict_types=1);


namespace BlueSky\Framework\State;

trait StateTrait
{

    /**
     * @var Web $requestState
     * @access private
     * The model state
     * */
    private $requestState;

    /**
     * Base constructor to define the state object
     * @access public
     * @param Base $state
     * */
    public function __construct(Base $state = null)
    {
        $this->requestState = $state;
        $this->onCreate();
    }

    /**
     * Get the state object
     * @access public
     * @return Web
     * */
    public function getState()
    {
        return $this->requestState;
    }

    /**
     * On create handler to be implemented in child
     * @access public
     * @return void
     * */
    protected function onCreate()
    {

    }

}
