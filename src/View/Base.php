<?php
namespace BlueSky\Framework\View;

use Psr\Log\LogLevel;

class Base
{

    protected $_status;
    protected $statusCode;
    protected $_error;
    protected $_errorLines = [];
    protected $_renderData = [];
    protected $_configData = [];
    protected $_renderFile;
    protected $customInnerContent = false;
    protected $isRedirect = false;
    protected $customContent = false;

    /**
     * @var array $path - the breadcrumb path
     */
    protected $path;

    /**
     * @param \BlueSky\Framework\State\Web $state
     */
    public function __construct($state)
    {
        $this->_state = $state;
        $this->onCreate();
    }

    /**
     * Get the view debug data
     * @param string $level
     * @return array
     * */
    public function getDebugData($level = LogLevel::DEBUG)
    {
        $result = [];
        $result['is_redirect'] = $this->isRedirect;
        $result['render_file'] = $this->_renderFile;
        if($level > LogLevel::INFO) {
            $result['data'] = $this->_renderData;
        }
        return $result;
    }

    public function setData($data)
    {
        $this->_renderData = $data;
    }

    /**
     * Set the HTTP status code for this response
     * @access public
     * @param int $code
     * @return void
     * */
    public function setStatusCode($code)
    {
        if (is_numeric($code)) {
            $this->statusCode = $code;
        }
    }

    public function setContent($content)
    {
        $this->customContent = $content;
    }

    public function setInnerContent($content)
    {
        $this->customInnerContent = $content;
    }

    protected function onCreate()
    {

    }

    public function isRedirect()
    {
        return $this->isRedirect;
    }

    /**
     * @param array $configData
     */
    public function setConfigData($configData)
    {
        $this->_configData = $configData;
    }

    /**
     * Event handler for when a login is required for a particular resource
     * @return void
     * */
    public function loginRequired()
    {

    }

    /**
     * Event handler for when a two step or two factor login is required for a particular resource
     * @return void
     * */
    public function twoFactorRequired()
    {

    }

    /**
     * Event handler for when a access is denied for a particular resource
     * @return void
     * */
    public function accessDenied()
    {

    }

    /**
     * @param string $key
     * @param mixed|null $value
     */
    public function setConfigVar($key, $value = null)
    {
        if ($value == null) {
            unset($this->_configData[$key]);
        } else {
            $this->_configData[$key] = $value;
        }
    }

    /**
     * @return array
     */
    public function getConfigData()
    {
        return $this->_configData;
    }

    /**
     * @var \BlueSky\Framework\State\Web $_state
     * */
    protected $_state;

    public function getData()
    {

    }

    /**
     * @return array
     * */
    public function getRenderData()
    {
        return $this->_renderData;
    }

    public function setRenderFile($file)
    {
        $this->_renderFile = $file;
    }

    public function __set($key, $value)
    {
        $this->_renderData[$key] = $value;
    }

    public function __isset($key)
    {
        array_key_exists($key, $this->_renderData[$key]);
    }

    public function __get($key)
    {
        $result = null;
        if (isset($this->_renderData[$key])) {
            $result = $this->_renderData[$key];
        }
        return $result;
    }

    public function render($viewRef = "")
    {
        return '';
    }

    public function redirect($path, $headerCode = 301)
    {
        $this->isRedirect = true;
        return false;
    }

    public function getBaseUrl()
    {
        if (!$result = $this->_state->getUrl()->getBaseUrl()) {
            if (php_sapi_name() == 'cli') {
                $result = $this->_state->getSites()->getCurrentSiteUrl();
            } else {
                $result = $this->_state->getRequest()->getSchemeAndHttpHost() . $this->_state->getRequest()->getBasePath() . '/';
            }
        }
        return $result;
    }

    public function getAppRef()
    {
        $route = $this->_state->getRoute();
        $app = 'website';
        if (isset($route['_handler']) && isset($route['_handler']['app'])) {
            $app = $route['_handler']['app'];
        } elseif (isset($this->_configData['_route_handler']) && isset($this->_configData['_route_handler']['app'])) {
            $app = $this->_configData['_route_handler']['app'];
        }
        return $app;
    }

    public function setPath(Array $path)
    {
        $this->path = $path;
    }

}