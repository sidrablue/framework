<?php
namespace BlueSky\Framework\View\Transform;

use Aura\Cli\Stdio;
use Aura\Cli\StdioResource;
use Aura\Cli\Vt100;
use BlueSky\Framework\View\Base as Base;

class Cli extends Base
{

    /**
     * @var Stdio $stdio
     * */
    private $stdio;

    protected function onCreate()
    {
        return;
        $this->stdio = new Stdio(new StdioResource('php://stdin', 'r'),
            new StdioResource('php://stdout', 'w+'),
            new StdioResource('php://stderr', 'w+'),
            new Vt100);
    }

    public function render($viewRef = "")
    {
        if(isset($this->renderData['success']) && $this->renderData['success']==true) {
            $this->stdio->err('%2%k');
        }
        else {
            $this->stdio->err('%1%k');
        }
        if(is_array($this->renderData)) {
            foreach ($this->renderData as $k => $v) {
                $this->stdio->errln($k . ' => ' . var_export($v, true));
            }
        }
        $this->stdio->err('%0%n');
    }

}
