<?php
namespace BlueSky\Framework\View\Transform;

use BlueSky\Framework\View\Base as Base;

class Jsonp extends Base
{

    public function render($viewRef = "")
    {
        $this->getState()->getResponse()->headers->set('Content-Type','application/json; charset=utf-8');
        $callback = $this->getState()->getRequest()->get('callback');
        if($callback) {
            $result = $callback.'('.json_encode($this->renderData).');';
        }
        else {
            $result = "alert('No callback parameter specified');";
        }
        return $result;
    }

    public function loginRequired()
    {
        $this->_state->getResponse()->setStatusCode('401', 'Login Required');
        $this->_renderData = ['success' => false, 'error' => 'Login Required'];
        $this->_state->getResponse()->setContent($this->render());
        $this->_state->getResponse()->send();
        exit;
    }

    public function accessDenied()
    {
        if($this->_state->getUser()->id) {
            $this->_state->getResponse()->setStatusCode('403', 'Forbidden');
            $this->_renderData = ['success' => false, 'error' => 'Forbidden'];
            $this->_state->getResponse()->setContent($this->render());
            $this->_state->getResponse()->send();
            exit;
        }
        else {
            $this->loginRequired();
        }
    }

}
