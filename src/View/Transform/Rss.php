<?php
namespace BlueSky\Framework\View\Transform;

use BlueSky\Framework\View\Base as Base;

class Rss extends Base
{

    public function render($viewRef = "")
    {
        $this->getState()->getResponse()->headers->set('Content-Type','application/rss+xml');
        return $this->customContent;
    }

}