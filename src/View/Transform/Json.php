<?php
namespace BlueSky\Framework\View\Transform;

use BlueSky\Framework\View\Base as Base;

class Json extends Base
{

    public function render($viewRef = "")
    {
        if($this->_state->getRequest()->get('_lote_form_module', false) &&
            !$this->_state->getResponse()->headers->set('Content-Type','application/json')) {
            $this->_state->getResponse()->headers->set('Content-Type','text/plain');
        }
        else {
            $this->_state->getResponse()->headers->set('Content-Type','application/json');
        }

        return json_encode($this->_renderData);
    }

    public function loginRequired()
    {
        $this->_state->getResponse()->setStatusCode(401, 'Login Required');
        $this->success = false;
        $this->error = 'Login Required';
        $this->login = false;
        $this->_state->getResponse()->setContent($this->render());
        $this->_state->getResponse()->send();

        exit;
    }



    public function accessDenied()
    {
        if($this->_state->getUser()->id) {
            $this->_state->getResponse()->setStatusCode('403', 'Forbidden');
            $this->success = false;
            $this->error = 'Access Denied';
            $this->_state->getResponse()->setContent($this->render());
            $this->_state->getResponse()->send();
            exit;
        }
        else {
            $this->loginRequired();
        }
    }

    public function redirect($path, $headerCode = 301) {
        $this->_renderData['_redirect'] = $path;
        $this->_renderData['_httpCode'] = $headerCode;
    }


}