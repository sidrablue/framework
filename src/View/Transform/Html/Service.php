<?php
namespace BlueSky\Framework\View\Transform\Html;

use BlueSky\Framework\State\Base as BaseState;

class Service
{

    public static function renderView(BaseState $state, $viewFile, $viewData, $customPaths = [])
    {
        $view = new Single($state);
        if( $state->getView() instanceof Base) {
            $view->paths = $state->getView()->paths;
        } else {
            $view->getPaths();
        }
        if ($state->getView()->customPaths && is_array($state->getView()->customPaths)) {
            foreach ($state->getView()->customPaths as $p) {
                $view->addCustomPath($p);
            }
        }
        foreach ($customPaths as $p) {
            $view->addCustomPath($p);
        }
        $view->setRenderFile($viewFile);
        $view->addWebPath();
        $view->setData($viewData);
        return $view->render();
    }

}
