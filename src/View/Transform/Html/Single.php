<?php
namespace BlueSky\Framework\View\Transform\Html;

use Psr\Log\LogLevel;
use BlueSky\Framework\Service\Content;

class Single extends Base
{

    public function render($viewRef = "")
    {
        if($viewRef) {
            $this->setRenderFile($viewRef);
        }
        if($this->customContent !== false) {
            $result = $this->customContent;
        }
        else {
            $this->renderSetup();
            $result = '';
            if(isset($this->_renderFile)){
                try {
                    $result = $this->_twig->render($this->_renderFile.'.twig', $this->_renderData);
                } catch (\Twig_Error_Loader $e) {
                    echo($e->getMessage());
                    $result = "Unable to render view - {$this->_renderFile} - ".$e->getMessage();
                    $this->_state->getLoggers()->getMasterLogger("lote")->error($this->_state->accountReference . "-error trying to load view-" . $this->_renderFile, $this->_state->getDebug()->getLogArray(LogLevel::ERROR));
                }
                if($this->fullRenderRequired()) {
                    $c = new Content($this->_state);
                    $result = $c->renderContent($result);
                }
            }
        }
        return $result;
    }

}
