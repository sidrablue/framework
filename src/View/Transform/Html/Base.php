<?php
namespace BlueSky\Framework\View\Transform\Html;

use BlueSky\Framework\Object\Entity\VirtualTypes\Type\StdDateTime;
use Psr\Log\LogLevel;
use BlueSky\Framework\Event\Data;
use BlueSky\Framework\Model\Token;
use Lote\App\MasterBase\Entity\Master\Account as AccountEntity;
use BlueSky\Framework\Service\Content as ContentService;
use BlueSky\Framework\Service\Timezone;
use BlueSky\Framework\State\Web;
use BlueSky\Framework\View\Base as ViewBase;
use BlueSky\Framework\Controller\Base as BaseController;
use BlueSky\Framework\Util\Country;
use BlueSky\Framework\Util\Currency;
use BlueSky\Framework\Util\Date;
use BlueSky\Framework\Util\Industry;
use BlueSky\Framework\Util\Strings;
use BlueSky\Framework\Util\Time;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Twig\TwigFilter;
use Twig_Extensions_Extension_Text;
use Urodoz\Truncate\TruncateService;

class Base extends ViewBase implements \Twig\Loader\LoaderInterface
{

    /**
     * @var \Twig\Environment $_twig
     */
    protected $_twig;

    /**
     * @var array $paths
     * */
    public $paths = [];

    /**
     * @var array $customPaths
     * */
    public $customPaths = [];

    /**
     * @var array $viewExtensions
     */
    private $viewExtensions = [];

    /**
     * @var array $minifyPaths
     */
    private $minifyPaths = [];

    /**
     * Get the view extensions, generally for a specific key
     * @access public
     * @param string $key - the key for the view extensions to get
     * @return array
     */
    public function getViewExtension($key = "")
    {
        $result = [];
        if (!$key) {
            $result = $this->viewExtensions;
        } elseif ($key && array_key_exists($key, $this->viewExtensions)) {
            $result = $this->viewExtensions[$key];
        }
        return $result;
    }

    /**
     * Override the view extensions
     * @access public
     * @param array $viewExtensions
     */
    public function setViewExtension($viewExtensions)
    {
        $this->viewExtensions = $viewExtensions;
    }

    /**
     * Add a view extensions for a specific key
     * @access public
     * @param string $key
     * @param string $viewExtension
     */
    public function addViewExtension($key, $viewExtension)
    {
        $this->viewExtensions[$key][] = $viewExtension;
    }

    /**
     *  */
    protected function onCreate()
    {
//        Twig_Extensions_Autoloader::register();

        $cache = false;
        if ($this->_state->getConfig()->get('cache.twig', '1') != '0') {
            $cacheDir = LOTE_ASSET_PATH . 'cache/' . LOTE_ACCOUNT_REF . '/twig';
            if (is_dir($cacheDir) && is_writable($cacheDir)) {
                $cache = $cacheDir;
            } elseif (!is_dir($cacheDir)) {
                if (mkdir($cacheDir, 0777, true)) {
                    $cache = $cacheDir;
                }
            }
        }

        $environmentVariables = ['cache' => $cache];
        
        $this->_twig = new \Twig\Environment($this, $environmentVariables);
        $this->_twig->addGlobal("state", $this->_state);
        $this->_twig->addGlobal("user", $this->_state->getUser());
        $this->_state->getApps()->setupView($this, 'twig');
        $filter = new \Twig\TwigFilter('dump', function ($var) {
            dump($var);
        });
        $this->_twig->addFilter($filter);
        $filter = new \Twig_SimpleFilter('var_dump', function ($var) {
            var_dump($var);
        });
        $this->_twig->addFilter($filter);

        $filter = new \Twig\TwigFilter('preg_match', function ($string, $pattern) {
            return preg_match($pattern, $string);
        });
        $this->_twig->addFilter($filter);

        $filter = new \Twig\TwigFilter('generateHtmlId', function ($string) {
            return Strings::generateHtmlId($string);
        });
        $this->_twig->addFilter($filter);

        $filter = new \Twig\TwigFilter('bool2str', function ($boolean) {
            $result = 'No';
            if ($boolean && $boolean != -1) {
                $result = 'Yes';
            }
            return $result;
        });
        $this->_twig->addFilter($filter);

        $this->_twig->addFilter(new TwigFilter('postcodeFormat', function ($input) {
            if (is_numeric($input)) {
                if ($input < 1000) { $input = '0' . $input; }
                elseif ($input < 100) { $input = '00' . $input; }
                elseif ($input < 10) { $input = '000' . $input; }
            }
            return $input;
        }));

        $function = new \Twig\TwigFunction('addMinifyJs', function ($src, $group = 'default') {
            $this->minifyPaths['js'][$group][] = $src;
        });
        $this->_twig->addFunction($function);

        $function = new \Twig\TwigFunction('addMinifyCss', function ($src, $group = 'default') {
            $this->minifyPaths['css'][$group][] = $src;
        });
        $this->_twig->addFunction($function);

        $function = new \Twig\TwigFunction('getMinifyJs', function ($group = 'default') {
            $result = '';
            if(isset($this->minifyPaths['js']) && isset($this->minifyPaths['js'][$group])) {
                if ($this->_state->getSettings()->getSettingOrConfig('minify.enabled', false) == true) {
                    require_once LOTE_WEB_PATH . 'min/lib.php';
                    $query = 'f=' . implode(",", $this->minifyPaths['js'][$group]);
                    $result = '<script src="' . \Minify\StaticService\build_uri('/min/', $query, 'js') . '"></script>';
                } else {
                    foreach ($this->minifyPaths['js'][$group] as $minifyPath) {
                        $result .= '<script src="' . $this->getBaseUrl() . $minifyPath . '"></script>';
                    }
                }
            }
            return $result;
        });
        $this->_twig->addFunction($function);

        $function = new \Twig\TwigFunction('getMinifyCss', function ($group = 'default') {
            $result = '';
            if(isset($this->minifyPaths['css']) && isset($this->minifyPaths['css'][$group]) ) {
                if ($this->_state->getSettings()->getSettingOrConfig('minify.enabled', false) == true) {
                    require_once LOTE_WEB_PATH . 'min/lib.php';
                    $query = 'f=' . implode(",", $this->minifyPaths['css'][$group]);
                    $result = '<link rel="stylesheet" href="' . \Minify\StaticService\build_uri('/min/', $query, 'css') . '">';
                } else {
                    foreach ($this->minifyPaths['css'][$group] as $minifyPath) {
                        $result .= '<link rel="stylesheet" href="' . $this->getBaseUrl() . $minifyPath . '">';
                    }
                }
            }
            return $result;
        });
        $this->_twig->addFunction($function);

        $function = new \Twig\TwigFunction('isRoute', function ($route) {
            $result = false;
            $currentRoute = $this->_state->getRoute();
            if (is_array($currentRoute) && isset($currentRoute['_route'])) {
                if (is_string($route)) {
                    $result = $route == $currentRoute['_route'];
                } elseif (is_array($route)) {
                    $result = in_array($currentRoute['_route'], $route);
                }
            }
            return $result;
        });
        $this->_twig->addFunction($function);


        $function = new \Twig\TwigFunction('imgUrl',
            function ($idOrReference, $width = 0, $height = 0, $size = 0, $mode = 'auto') {
                $c = new ContentService($this->_state);
                return $c->getImageCachedUrl($idOrReference, $width, $height, $size, $mode);
            });
        $this->_twig->addFunction($function);

        $function = new \Twig\TwigFunction('imgUrlPreview',
            function ($idOrReference, $width = 0, $height = 0, $size = 0, $mode = 'auto') {
                $c = new ContentService($this->_state);
                return $c->getImageCachedUrl($idOrReference, $width, $height, $size, $mode, false, true);
            });
        $this->_twig->addFunction($function);

        $function = new \Twig\TwigFunction('imgTsUrl',
            function ($idOrReference, $size = 0, $width = 0, $height = 0, $mode = 'auto') {
                $c = new ContentService($this->_state);
                $url = $c->getImageCachedUrl($idOrReference, $width, $height, $size, $mode, true);
                $url = str_replace(' ', '%20', $url);
                return $url;
            });
        $this->_twig->addFunction($function);

        $function = new \Twig\TwigFunction('imgParentUrl',
            function ($idOrReference, $width = 0, $height = 0, $size = 0, $mode = 'auto') {
                $c = new ContentService($this->_state->getParentState());
                $url = $c->getImageCachedUrl($idOrReference, $width, $height, $size, $mode);
                $url = str_replace($this->_state->getRequest()->getSchemeAndHttpHost(),
                    $this->_state->getParentAccount()->getUrl(), $url);
                return $url;
            });
        $this->_twig->addFunction($function);

        $function = new \Twig\TwigFunction('getSchemaEntity',
            function ($reference) {
                return $this->_state->getSchema()->getEntityByReference($reference);
            });
        $this->_twig->addFunction($function);

        $function = new \Twig\TwigFunction('getSchemaField',
            function ($fieldReference, $entityReference) {
                if (is_string($entityReference)) {
                    $entityReference = $this->_state->getSchema()->getEntityByReference($entityReference);
                }
                return $entityReference->getEntityField($fieldReference);
            });
        $this->_twig->addFunction($function);


        /*$function = new \Twig\TwigFunction('getLatestArticlesInPublication',
            function ($publication_id, $limit = false) {
                $p = new Publication($this->_state);
                $items = $p->getLatestArticlesInPublication($publication_id, $limit, false,
                    $this->_state->getSettings()->get("szschool.website.swiper.tag", false));
                return $items;
            });
        $this->_twig->addFunction($function);

        $function = new \Twig\TwigFunction('getLatestEventsInPublication', function ($limit = '') {

            $start = date("Y-m-d H:i:s");
            $cal_id = $this->_state->getSettings()->get("szschool.website.stream.box.calendar_id");
            $modelEventObj = new CalMod($this->_state);
            $data = $modelEventObj->getCalendarEvents($cal_id, $start, '', $limit, '', false, true);
            return $data;
        });
        $this->_twig->addFunction($function);*/

        $function = new \Twig\TwigFunction('imgTsParentUrl',
            function ($idOrReference, $size = 0, $width = 0, $height = 0, $mode = 'auto') {
                $parentReference = $this->_state->getParentAccount()->getReference();
                $s = new Web($parentReference);
                $c = new ContentService($s);
                $url = $c->getImageCachedUrl($idOrReference, $width, $height, $size, $mode, true);
                $url = str_replace($this->_state->getRequest()->getSchemeAndHttpHost(),
                    $this->_state->getParentAccount()->getUrl(), $url);
                $url = str_replace(' ', '%20', $url);
                return $url;
            });
        $this->_twig->addFunction($function);

        $function = new \Twig\TwigFunction('imgTsMasterUrl',
            function ($idOrReference, $size = 0, $width = 0, $height = 0, $mode = 'auto') {
                $c = new ContentService($this->_state->getMasterState());
                $url = $c->getImageCachedUrl($idOrReference, $width, $height, $size, $mode, true);
                $url = str_replace(' ', '%20', $url);
                return $url;
            });
        $this->_twig->addFunction($function);

        $function = new \Twig\TwigFunction('truncateHtml', function ($html, $length = 240) {
            $truncateService = new TruncateService();
            return $truncateService->truncate($html, $length);
        });
        $this->_twig->addFunction($function);


        /*$function = new \Twig\TwigFunction('getPositionsByTag', function ($limit) {
            $pm = new PositionModel($this->_state);
            $positionList = $pm->getMostRecentAvailablePositions($limit);
            return $positionList;
        });
        $this->_twig->addFunction($function);*/

        /*$function = new \Twig\TwigFunction('getParentAndChildren', function ($child) {
            $m = new PageItemModel($this->getState());
            return $m->getParentAndChildren($child);
        });
        $this->_twig->addFunction($function);*/

        /*$function = new \Twig\TwigFunction('masterContentByTag', function ($tag) {
            $m = new Holder($this->_state->getParentState());
            return $m->getHolderWithTag($tag, true);
        });
        $this->_twig->addFunction($function);*/

        /*$function = new \Twig\TwigFunction('getLatestArticle', function ($tag) {
            $m = new NewsletterArticle($this->_state);
            return $m->getArticlesWithTag($tag);
        });
        $this->_twig->addFunction($function);

        $function = new \Twig\TwigFunction('getSideItems', function ($pageID, $location) {
            $m = new ContentExtra($this->_state);
            return $m->getContentByPageLocationData($pageID, $location);
        });
        $this->_twig->addFunction($function);

        $function = new \Twig\TwigFunction('getWebsiteFeature', function () {
            $m = new NewsletterArticle($this->_state);
            return $m->getWebsiteFeature();
        });
        $this->_twig->addFunction($function);

        $function = new \Twig\TwigFunction('getWebsiteArticleSlider', function () {
            $m = new NewsletterArticle($this->_state);
            return $m->getWebsiteArticleSlider();
        });
        $this->_twig->addFunction($function);

        $function = new \Twig\TwigFunction('getNoticeboardEvents', function ($id) {
            $c = new CalendarM($this->_state);
            if ($id) {
                return ['items' => $c->eventNoticeView($id)];
            } else {
                return null;
            }
        });
        $this->_twig->addFunction($function);

        $function = new \Twig\TwigFunction('getEventUsage', function ($id) {
            $obj = new CalendarEvent($this->_state);
            return $obj->getLinkedCalendarDetails($id);
        });
        $this->_twig->addFunction($function);

        $function = new \Twig\TwigFunction('getNotificationListing', function () {
            $m = new CampaignNotificationModel($this->_state);
            return $m->getSentNotifications(1);
        });
        $this->_twig->addFunction($function);

        $function = new \Twig\TwigFunction('getFeedListing', function () {
            $e = new CampaignNotificationModel($this->_state);
            $feed = $e->getPublishedFeeds(1, 10);
            if (isset($feed['rows'])) {
                foreach ($feed['rows'] as $key => $value) {
                    $c = new ContentService($this->_state);
                    $message = $c->renderNotificationContent($value['description']);
                    $mPubSet = new PublicationSettings($this->_state);
                    $message = $mPubSet->replaceDateScheduledWildcards($message, $value['lote_created']);
                    $feed['rows'][$key]['description'] = $message;
                }
            }
            return $feed;
        });
        $this->_twig->addFunction($function);
        $function = new \Twig\TwigFunction('getFeedListingByTag', function ($value) {
            $e = new CampaignNotificationModel($this->_state);
            $data = $e->getPublishedFeedsByTag(1, 7, $value);
            return $data;
        });
        $this->_twig->addFunction($function);

        $function = new \Twig\TwigFunction('getFeedListingByTagValue', function ($value) {
            $e = new CampaignNotificationModel($this->_state);
            $data = $e->getPublishedFeedsByTagValue(1, 7, $value);
            return $data;
        });
        $this->_twig->addFunction($function);

        //Get the data for an object from its identifier
        $function = new \Twig\TwigFunction('getIdentifier', function ($id) {
//            return $this->_state->getIdentifier()->getIdentifierObject($id);
            return 0;
        });
        $this->_twig->addFunction($function);*/

        $function = new \Twig\TwigFunction('DateTimeValue', function ($defaultTime, $format) {
            $d = new \DateTime($defaultTime);
            return $d->format($format);
        });
        $this->_twig->addFunction($function);

        $function = new \Twig\TwigFunction('constructUrl', function ($parent, $module = '') {

            if (is_array($parent)) {
                if (isset($parent['url'])) {
                    $return = $parent['properties']['url'];
                    if ($parent['properties']['url'] == 'javascript:;') {
                        $string = strtolower(preg_replace('/\s+/', '-', $parent['properties']['name']));
                        $return = '/' . $string;
                    }
                    return $return;
                } else {
                    return null;
                }
            } else {
                $path = $parent;
                $array = explode('/', $path);

                $segments = 3;
                if ($module == 'page') {
                    $segments = 5;
                }
                array_splice($array, 0, $segments);

                $string = '';
                if (!empty($array)) {
                    $string = '/' . implode($array, '/');
                    $string = strtolower(preg_replace('/\s+/', '-', $string));
                }
                return $string;
            }

        });
        $this->_twig->addFunction($function);

        $function = new \Twig\TwigFunction('getUniqueId', function () {
            $id = mt_rand(3, 5);
            $id = $id . time();
            return $id;
        });
        $this->_twig->addFunction($function);

        /*$function = new \Twig\TwigFunction('getUserId', function () {
            $id = $this->_state->getUser()->id;
            return $id;
        });
        $this->_twig->addFunction($function);

        $function = new \Twig\TwigFunction('getUserDetails', function () {
            $details = $this->_state->getUser()->getViewData();
            return $details;
        });
        $this->_twig->addFunction($function);*/

        $function = new \Twig\TwigFunction('getCurrencyList', function () {
            /// Move this twig function to the correct app + use the updated util functionality in BlueSky Framework
            return [
                'AUD' => [
                    'name' => 'AU Dollar',
                    'code' => 'AUD',
                    'symbol' => '$',
                    'prefix' => null,
                    'suffix' => null,
                    'precision' => 2,
                ],
                [
                    'name' => 'NZ Dollar',
                    'code' => 'NZD',
                    'symbol' => '$',
                    'prefix' => null,
                    'suffix' => null,
                    'precision' => 2,
                ],
                [
                    'name' => 'US Dollar',
                    'code' => 'USD',
                    'symbol' => '$',
                    'prefix' => null,
                    'suffix' => null,
                    'precision' => 2,
                ],
                [
                    'name' => 'British Pound',
                    'code' => 'GBP',
                    'symbol' => '£',
                    'prefix' => null,
                    'suffix' => null,
                    'precision' => 2,
                ],
                [
                    'name' => 'Euro',
                    'code' => 'EUR',
                    'symbol' => '£',
                    'prefix' => null,
                    'suffix' => null,
                    'precision' => 2,
                ],
            ];
        });
        $this->_twig->addFunction($function);

        $function = new \Twig\TwigFunction('getCountryList', function () {
            return Country::getList();
        });
        $this->_twig->addFunction($function);

        $function = new \Twig\TwigFunction('getIndustryList', function () {
            return Industry::getList();
        });
        $this->_twig->addFunction($function);

        $function = new \Twig\TwigFunction('base64_decode', function ($var) {
            return base64_decode($var);
        });
        $this->_twig->addFunction($function);

        $filter = new \Twig\TwigFilter('base64_decode', function ($var) {
            echo base64_decode($var);
        });
        $this->_twig->addFilter($filter);

        $function = new \Twig\TwigFunction('generateCsrf', function () {
            $m = new Token($this->_state);
            $csrfToken = $m->addToken();
            return $csrfToken['public_key'];
        });
        $this->_twig->addFunction($function);

        $function = new \Twig\TwigFunction('base64_encode', function ($var) {
            return base64_encode($var);
        });
        $this->_twig->addFunction($function);

        $filter = new \Twig\TwigFilter('base64_encode', function ($var) {
            echo base64_encode($var);
        });
        $this->_twig->addFilter($filter);

        $function = new \Twig\TwigFunction('validDateTime', function ($dateTime) {
            $d = new Date();
            return $d->validateDateTime($dateTime);
        });
        $this->_twig->addFunction($function);

        $function = new \Twig\TwigFunction('validDate', function ($date) {
            $valid = false;
            if ($date !== null && $date !== '') {
                $d = new Date();
                $valid = $d->validateDate($date) && $date >= StdDateTime::MINIMUM_DATE_STRING;
            }
            return  $valid;
        });
        $this->_twig->addFunction($function);

        $function = new \Twig\TwigFunction('hasAccess', function ($context, $right = 'view', $ignoreAdmin = false) {
            if (is_array($context)) {
                $result = false;
                foreach ($context as $v) {
                    if ($this->_state->getAccess()->hasAccess($v, $right, $ignoreAdmin)) {
                        $result = true;
                        break;
                    }
                }
            } else {
                $result = $this->_state->getAccess()->hasAccess($context, $right, $ignoreAdmin);
            }
            return $result;
        });
        $this->_twig->addFunction($function);

        $function = new \Twig\TwigFunction('hasRole', function ($roleReference) {
            return $this->_state->getAccess()->hasRole($roleReference);
        });
        $this->_twig->addFunction($function);

        $function = new \Twig\TwigFunction('getPrimaryUserRole', function () {
            return $this->_state->getAccess()->getPrimaryUserRole();
        });
        $this->_twig->addFunction($function);

        /*$function = new \Twig\TwigFunction('getEventsList', function ($id) {
            $e = new CalendarEvent($this->_state);
            $date = date("Y-m-d");
            return $e->getCalendarEvents($id, $date, '', 10);
        });
        $this->_twig->addFunction($function);*/

        $filter = new \Twig\TwigFilter('json_decode', function ($string, $asArray = true) {
            $result = $string;
            if (!is_array($string) && $asArray) {
                $result = json_decode($string, $asArray);
            }
            if (!is_object($string) && !$asArray) {
                $result = json_decode($string, $asArray);
            }
            return $result;
        });
        $this->_twig->addFilter($filter);

        $filter = new \Twig\TwigFilter('format_bytes', function ($bytes, $si = true) {
            $unit = $si ? 1000 : 1024;
            if ($bytes <= $unit) {
                return $bytes . " B";
            }
            $exp = intval((log($bytes) / log($unit)));
            $pre = ($si ? "kMGTPE" : "KMGTPE");
            $pre = $pre[$exp - 1] . ($si ? "" : "i");
            return sprintf("%.1f %sB", $bytes / pow($unit, $exp), $pre);
        });
        $this->_twig->addFilter($filter);

        $filter = new \Twig\TwigFilter('format_phone', function ($phone) {
            if (preg_match('/^(61|\+61|0)?([0-9])?([0-9]{8})$/', str_replace(' ', '', $phone), $matches)) {
                $formatCode = $matches[1];
                $areaCode = $matches[2];
                $number = $matches[3];
                if ($areaCode == 4) {
                    $phone = substr($number, 0, 2) . ' ' . substr($number, 2, 3) . ' ' . substr($number, 5, 3);
                } else {
                    $phone = substr($number, 0, 4) . ' ' . substr($number, 4, 4);
                }
                $pre = $formatCode . $areaCode;
                if (!($formatCode == 0 && $areaCode == 4) && !empty($pre)) {
                    $pre = "($pre) ";
                }
                $phone = $pre . $phone;
            }
            return $phone;
        });
        $this->_twig->addFilter($filter);

        $filter = new \Twig\TwigFilter('intOrdinal', function ($number) {
            $ends = array('th', 'st', 'nd', 'rd', 'th', 'th', 'th', 'th', 'th', 'th');
            if ((($number % 100) >= 11) && (($number % 100) <= 13)) {
                return $number . 'th';
            } else {
                return $number . $ends[$number % 10];
            }
        });
        $this->_twig->addFilter($filter);

        $filter = new \Twig\TwigFilter('format_abn', function ($abn) {
            if (preg_match('/^([0-9]{2})([0-9]{3})([0-9]{3})([0-9]{3})$/', str_replace(' ', '', $abn), $matches)) {
                $abn = implode(' ', array_slice($matches, 1));
            }
            return $abn;
        });
        $this->_twig->addFilter($filter);

        $filter = new \Twig\TwigFilter('format_acn', function ($acn) {
            if (preg_match('/^([0-9]{3})([0-9]{3})([0-9]{3})$/', str_replace(' ', '', $acn), $matches)) {
                $acn = implode(' ', array_slice($matches, 1));
            }
            return $acn;
        });
        $this->_twig->addFilter($filter);

        $filter = new \Twig\TwigFilter('json_encode', function ($string) {
            return json_encode($string);
        });
        $this->_twig->addFilter($filter);

        $filter = new \Twig\TwigFilter('isJson', function ($string) {
            return Strings::isJson($string);
        });
        $this->_twig->addFilter($filter);

        $function = new \Twig\TwigFunction('isJson', function ($string) {
            return Strings::isJson($string);
        });
        $this->_twig->addFunction($function);


        $function = new \Twig\TwigFunction('getAppData', function ($key) {
            $result = $this->_state->getApps()->get($key);
            return $result;
        });
        $this->_twig->addFunction($function);

        $function = new \Twig\TwigFunction('getTitle', function ($routeData) {
            $result = '';
            if (isset($routeData['title'])) {
                $result = $routeData['title'];
            }
            if (isset($routeData['_route'])) {
                $result = $routeData['_route'];
            }
            return $result;
        });
        $this->_twig->addFunction($function);

        $function = new \Twig\TwigFunction('mimeIsImage', function ($mimeType) {
            return strpos($mimeType, 'image') === 0;
        });
        $this->_twig->addFunction($function);

        $function = new \Twig\TwigFunction('mimeGetExtension', function ($mimeType, $fileName = '') {
            $result = '';
            if (preg_match('#^image.*#', $mimeType)) {
                $result = 'jpg';
                if (preg_match('#^image/png.*$#', $mimeType)) {
                    $result = 'png';
                } elseif (preg_match('#^image/tif.*$#', $mimeType)) {
                    $result = 'tif';
                } elseif (preg_match('#^image/gif.*$#', $mimeType)) {
                    $result = 'gif';
                }
            } elseif (preg_match('#^application.*xls.*$#', $mimeType)) {
                $result = 'xls';
            } elseif (preg_match('#^application.*rtf.*$#', $mimeType)) {
                $result = 'rtf';
            } elseif (preg_match('#^application.*doc.*$#', $mimeType)) {
                $result = 'doc';
            } elseif (preg_match('#^application/msword.*$#', $mimeType)) {
                $result = 'doc';
            } elseif (preg_match('#^application.*pdf.*$#', $mimeType)) {
                $result = 'pdf';
            } else {
                if ($fileName) {
                    $result = pathinfo($fileName, PATHINFO_EXTENSION);
                }
            }
            return $result;
        });
        $this->_twig->addFunction($function);

        $function = new \Twig\TwigFunction('getSetting', function ($name, $default = null) {
            return $this->_state->getSettings()->get($name, $default);
        });
        $this->_twig->addFunction($function);

        $function = new \Twig\TwigFunction('getSessionVar', function ($namespace, $name, $default = null) {
            return $this->_state->getRequest()->getSession()->get($namespace . $name, $default);
        });
        $this->_twig->addFunction($function);

        $function = new \Twig\TwigFunction('r', function ($var, $default = null) {
            $route = $this->_state->getRoute();
            if (isset($route[$var])) {
                $result = $route[$var];
            } else {
                $result = $this->_state->getRequest()->get($var, $default);
            }
            return $result;
        });
        $this->_twig->addFunction($function);

        $function = new \Twig\TwigFunction('loteEntityData', function ($entityId, $table, $includeDeleted = false) {
            $result = false;
            if ($entityId) {
                $q = $this->_state->getReadDb()->createQueryBuilder();
                $q->select("*")->from($table, "t")->where("t.id=:entity_id")->setParameter("entity_id", $entityId);
                if (!$includeDeleted) {
                    $q->andWhere("t.lote_deleted is null");
                }
                $result = $q->execute()->fetch(\PDO::FETCH_ASSOC);
            }
            return $result;
        });
        $this->_twig->addFunction($function);

        $function = new \Twig\TwigFunction('s', function ($name, $default = null) {
            return $this->_state->getSettings()->getSettingOrConfig($name, $default);
        });
        $this->_twig->addFunction($function);

        $function = new \Twig\TwigFunction('trans', function ($key, $params = []) {
            $route = $this->_state->getRoute();
            if (preg_match_all("/%%([a-zA-Z0-9\_]+)%%/", $key, $matches)) {
                foreach ($matches[0] as $k => $v) {
                    if (array_key_exists($matches[1][$k], $route)) {
                        $key = str_replace($matches[0][$k], $route[$matches[1][$k]], $key);
                    } else {
                        $key = str_replace($matches[0][$k], '', $key);
                    }
                }
            }
            return $this->_state->getTranslator()->trans($key, $params);
        });
        $this->_twig->addFunction($function);

        $function = new \Twig\TwigFunction('renderContent', function ($content) {
            $contentService = new ContentService($this->_state);
            return $contentService->renderContent($content);
        });
        $this->_twig->addFunction($function);

        $function = new \Twig\TwigFunction('renderNotificationContent', function ($content) {
            $contentService = new ContentService($this->_state);
            return $contentService->renderNotificationContent($content, $this->_state->getUser(), null, false);
        });
        $this->_twig->addFunction($function);

        /*$function = new \Twig\TwigFunction('renderContentWithDatePlanned',
            function ($content, $datePlanned, $convertToDisplay = false) {
                $contentService = new PublicationSettings($this->_state);
                return $contentService->renderContentWithDatePlanned($content, $datePlanned, $convertToDisplay);
            });
        $this->_twig->addFunction($function);*/

        $function = new \Twig\TwigFunction('isDev', function () {
            return $this->_state->getConfig()->get('lote.dev', false);
        });
        $this->_twig->addFunction($function);

        $function = new \Twig\TwigFunction('uniqid', function ($prefix = '') {
            return uniqid($prefix);
        });
        $this->_twig->addFunction($function);

        $function = new \Twig\TwigFunction('tzGetZone', function () {
            return $this->_state->getTimezone()->getDisplayTimezone();
        });
        $this->_twig->addFunction($function);

        $function = new \Twig\TwigFunction('tzGetList', function () {
            return Time::TIMEZONE_LIST;
        });
        $this->_twig->addFunction($function);

        $function = new \Twig\TwigFunction('tzGetOffset', function () {
            return $this->_state->getTimezone()->getDisplayOffset();
        });
        $this->_twig->addFunction($function);

        $filter = new \Twig\TwigFilter('tzUtcToDisplay', function ($time, $format = false) {
            $ts = new Timezone($this->_state);
            if ($format == false) {
                $format = $this->_state->getSettings()->get('system.datetime_format', 'jS F Y g:ia');
            }
            return $ts->convertToDisplay($time, $format);
        });
        $this->_twig->addFilter($filter);

        $filter = new \Twig\TwigFilter('tzUtcDateToDisplay', function ($time, $format = false) {
            $ts = new Timezone($this->_state);
            if ($format == false) {
                $format = 'jS F Y';
            }
            return $ts->convertToDisplay($time, $format);
        });
        $this->_twig->addFilter($filter);

        $filter = new \Twig\TwigFilter('tzDateToUtc', function ($time, $format = false, $timezone = false) {
            $ts = new Timezone($this->_state);
            if ($timezone == false) {
                $timezone = $ts->getDisplayTimezone();
            }
            if ($format == false) {
                $format = 'jS F Y g:ia';
            }
            return $ts->convertToUtc($time, $timezone, $format);
        });
        $this->_twig->addFilter($filter);

        $filter = new \Twig\TwigFilter('tzDateToTs', function ($time, $format = false, $timezone = false) {
            $ts = new Timezone($this->_state);
            if ($timezone == false) {
                $timezone = $ts->getDisplayTimezone();
            }
            if ($format == false) {
                $format = 'jS F Y g:ia';
            }
            return $ts->convertToTimestamp($time, $timezone, $format);
        });
        $this->_twig->addFilter($filter);

        $filter = new \Twig\TwigFilter('loteCurrencyFormat', function ($number, $precision = 2) {
            $currencyCode = $this->_state->getSettings()->get('system.currency', 'AUD');

            $prefix = '$';
            if ($currencyCode == "GBP") {
                $prefix = '�';
            }

            return $prefix . number_format($number, $precision, '.', ',');
        });
        $this->_twig->addFilter($filter);

        $function = new \Twig\TwigFilter('loteCurrencyFormatLabel', function () {
            $currencyCode = $this->_state->getSettings()->get('system.currency', 'AUD');

            $prefix = '$';
            if ($currencyCode == "GBP") {
                $prefix = '\u00A3';//'�';
            }

            return $prefix;
        });
        $this->_twig->addFilter($function);

        $filter = new \Twig\TwigFilter('loteDateFormat', function ($date, $format = 'Y-m-d g:ia   ') {
            return Date::dateFormat($date ?? '', $format);
        });
        $this->_twig->addFilter($filter);

        $filter = new \Twig\TwigFilter('safeDate', function ($date, $format = 'Y-m-d g:ia   ') {
            try {
                $date = Date::dateFormat($date, $format);
            } catch (\Exception $e) {
            }
            return $date;
        });
        $this->_twig->addFilter($filter);

        $filter = new \Twig\TwigFilter('dateFormatDay', function ($displayDateTime) {
            $tz = new Timezone($this->_state);
            $dtz = new \DateTimeZone($tz->getDisplayTimezone());
            $now = new \DateTime('now', $dtz);
            $now->setTime(0, 0, 0);
            $then = new \DateTime($displayDateTime);
            $then->setTimezone($dtz);
            $diff = $now->diff($then);
            $diffStr = $diff->format('%R%a');
            switch ($diffStr) {
                case '+1':
                    $day = 'Tomorrow';
                    break;
                case "0":
                    $day = $diffStr === '-0' ? 'Yesterday' : 'Today'; // necessary because switch equates -0 and +0
                    break;
                case '-1':
                case '-2':
                case '-3':
                case '-4':
                case '-5':
                    $day = $then->format('D');
                    break;
                default:
                    $day = $then->format('j M');
            }
            return $day;
        });
        $this->_twig->addFilter($filter);


        $function = new \Twig\TwigFunction('getHumanTiming', function ($date) {

            $time = strtotime($date);
            $time = time() - $time; // to get the time since that moment

            $tokens = array(
                31536000 => 'year',
                2592000 => 'month',
                604800 => 'week',
                86400 => 'day',
                3600 => 'hour',
                60 => 'minute',
                1 => 'second'
            );

            foreach ($tokens as $unit => $text) {
                if ($time < $unit) {
                    continue;
                }
                $numberOfUnits = floor($time / $unit);
                return $numberOfUnits . ' ' . $text . (($numberOfUnits > 1) ? 's' : '') . ' ago';
            }
        });
        $this->_twig->addFunction($function);

        /*$function = new \Twig\TwigFunction('getProjectsAndPages', function () {
            $m = new ModelProject($this->_state);
            $projects = $m->getAll();

            if (!empty($projects)) {
                foreach ($projects as $key => $value) {
                    $pageObj = new PageModel($this->_state);
                    $pageList = $pageObj->getProjectPages($projects[$key]['id']);
                    $projects[$key]['pages'] = $pageList;
                }
            }

            return $projects;
        });
        $this->_twig->addFunction($function);

        $function = new \Twig\TwigFunction('getAllDisplaySuits', function () {
            $m = new ModelDisplay($this->_state);
            $displays = $m->getAllDisplays();
            $groups = $m->getGroupNames();

            $id = isset($_SESSION['control_session_id']) ? $_SESSION['control_session_id'] : 0;
            $obj = new SessionEntity($this->_state);
            $obj->load($id);
            $sessionDetails = $obj->getData();

            $data = [];
            $data['displays'] = $displays;
            $data['groups'] = $groups;
            $data['session_data'] = $sessionDetails;

            return $data;
        });
        $this->_twig->addFunction($function);

        $function = new \Twig\TwigFunction('getCountryName', function ($id) {
            $name = '';
            $obj = new CountryEntity($this->_state);
            if ($obj->load($id)) {
                $data = $obj->getViewData();
                $name = $data['name'];
            }
            return $name;
        });
        $this->_twig->addFunction($function);*/

        $function = new \Twig\TwigFunction('getAccountField', function ($key) {
            $field = '';
            $a = new AccountEntity($this->_state);
            if ($a->loadByField('reference', $this->_state->getSettings()->get('system.reference'))) {
                if (isset($a->getData()[$key])) {
                    $field = $a->getData()[$key];
                }
            }
            return $field;
        });
        $this->_twig->addFunction($function);

        $function = new \Twig\TwigFunction('frequencyToDisplay', function ($frequency) {
            $result = "Manual Sync Only";
            if ($frequency == "hour") {
                $result = "Hourly";
            } elseif ($frequency == "day") {
                $result = "Daily";
            } elseif ($frequency == "week") {
                $result = "Weekly";
            } elseif ($frequency == "month") {
                $result = "Monthly";
            }
            return $result;
        });
        $this->_twig->addFunction($function);

        $function = new \Twig\TwigFunction('isFramework', function ($site) {
            return $site == $this->_state->getConfig()->getFramework();
        });
        $this->_twig->addFunction($function);

        $this->_twig->addGlobal('_route', $this->_state->getRoute());

        $this->_twig->addExtension(new Twig_Extensions_Extension_Text());
//        $this->_twig->addExtension(new TruncateExtension(new TruncateService()));

    }

    /**
     * Get the view debug data
     * @param string $level
     * @return array
     * */
    public function getDebugData($level = LogLevel::DEBUG)
    {
        $result = parent::getDebugData($level);
        $paths = $this->getPaths();
        foreach ($paths as $k => $v) {
            $paths[$k] = str_replace(LOTE_BASE_PATH, "", $v);
        }
        $result['paths'] = $paths;
        return $result;
    }

    /**
     * @return \Twig\Environment
     * */
    public function getTwig(): \Twig\Environment
    {
        return $this->_twig;
    }

    private function getSkin($systemRef, $checkParent = true)
    {
        $route = $this->_state->getRoute();
        $skin = 'default';
        if (isset($this->_configData['_skin'])) {
            $skin = $this->_configData['_skin'];
        } else {
            if (isset($route['_skin'])) {
                $skin = $route['_skin'];
            } elseif (empty($route) && isset($this->_configData['_route_handler']) && isset($this->_configData['_route_handler']['skin'])) {
                $skin = $this->_configData['_route_handler']['skin'];
            } elseif ($systemRef == 'website') {
                /** @todo - Implement proper site based skin determination */
                $skin = $this->_state->getSites()->getSkin($this->_state->getSettings()->get('website.skin', 'default'));
            } else {
                if ($checkParent && $parentRef = $this->getParentSystem($this->getAppRef())) {
                    if ($this->_state->getSettings()->get($parentRef . '.skin', false)) {
                        $skin = $this->_state->getSettings()->get($parentRef . '.skin', 'default');
                    }
                } elseif ($systemSkin = $this->_state->getSettings()->get($this->getAppRef() . '.skin', false)) {
                    $skin = $systemSkin;
                } else {
                    $parentRef = $this->getParentSystem($this->getAppRef());
                    if ($parentSkin = $this->_state->getSettings()->get($parentRef . '.skin', false)) {
                        $skin = $this->_state->getSettings()->get($parentRef . '.skin', 'default');
                    }

                }
            }
        }
        $eventData = new Data(['route' => $route, 'skin' => $skin]);
        $this->_state->getEventManager()->dispatch("core.get_skin", $eventData);
        return $eventData->getData("skin", false) ? $eventData->getData("skin") : $skin;
    }

    private function hasCustomSkin()
    {
        $route = $this->_state->getRoute();
        return isset($route['_skin']) && !empty($route['_skin']);
    }

    private function getTheme($systemRef)
    {
        $route = $this->_state->getRoute();
        $theme = 'default';
        if (isset($route['_theme'])) {
            $theme = $route['_theme'];
        } elseif (empty($route) && isset($this->_configData['_route_handler']) && isset($this->_configData['_route_handler']['theme'])) {
            $theme = $this->_configData['_route_handler']['theme'];
        } elseif ($systemRef == 'website') {
            $theme = $this->_state->getSites()->getTheme();
        } elseif ($appTheme = $this->_state->getSettings()->get("$systemRef.theme")) {
            $theme = $appTheme;
        }
        return $theme;
    }

    private function hasCustomTheme()
    {
        $route = $this->_state->getRoute();
        return isset($route['_theme']) && !empty($route['_theme']);
    }

    public function getPaths()
    {
        //$this->paths = [];
        $app = $this->getAppRef();
        $skin = $this->getSkin($app, false);
        $parentSkin = $this->getSkin($app);

        $this->paths[] = LOTE_APP_PATH . 'view/' . $app . "/{$skin}/";
        $this->paths[] = LOTE_APP_PATH . 'view/_base/default/';
        $this->paths[] = LOTE_LIB_PATH . 'view/' . $app . "/{$skin}/";
        $this->_state->getApps()->setupViewPath($this, $app, $skin);
        if ($systemConfig = $this->_state->getApps()->get($app)) {
            if ($parent = $this->getParentSystem($app)) {
                $this->paths[] = LOTE_LIB_PATH . 'view/' . $this->getParentSystem($app) . "/{$parentSkin}/";
            }
            $systemBaseFolder = LOTE_LIB_PATH . 'view/' . $app . '/_base/';
            if (is_dir($systemBaseFolder)) {
                $this->paths[] = $systemBaseFolder;
            }
        }
        //$this->paths[] = LOTE_LIB_PATH . 'view/_base/default/';
        $this->addCustomPaths();
        $this->paths = array_unique($this->paths);
        return $this->paths;
    }

    private function getParentSystem($system = '')
    {
        $result = false;
        $route = $this->_state->getRoute();
        if (isset($route['_handler']['parent'])) {
            $result = $route['_handler']['parent'];
        }
        if (!$result && $system) {
            if ($this->statusCode == 404) {
                //set up the parent view
                if ($system == 'crm') {
                    $result = 'admin';
                }
            }
        }
        return $result;
    }

    /**
     * @todo - define default system for the route through configuration
     * */
    protected function renderSetup()
    {
        $route = $this->_state->getRoute();
        $app = $this->getAppRef();
        $paths = $this->getPaths();
        $paths = $this->removeInvalidPaths($paths);

        //$this->_state->getApps()->setupSystem($this->_state, $system);
        $loader = new \Twig\Loader\FilesystemLoader($paths);

        $this->_renderData['_route'] = $route;
        //$this->_renderData['_skinDir'] = $this->getSkinDir(); // $this->_renderData['_basePath'] . "theme/{$system}/{$theme}";
        if (php_sapi_name() != 'cli' && $this->_state->getRequest()->headers->get("js-global-hide", false) == true) {
            $this->_renderData['_hideGlobalScripts'] = true;
        }

        $urlData = [];
        $urlData['scheme'] = $this->_state->getUrl()->getScheme();
        $urlData['host'] = $this->_state->getUrl()->getHttpHost();
        if(!$this->_state->isCli()) {
            $urlData['port'] = $this->_state->getRequest()->getPort();
            $urlData['user'] = $this->_state->getRequest()->getUser();
            $urlData['pass'] = $this->_state->getRequest()->getPassword();
            $urlData['path'] = $this->_state->getRequest()->getRequestUri();
            $urlData['query'] = $this->_state->getRequest()->getQueryString();
        } else {
            $urlData['query'] = '';
            $urlData['path'] = '';
        }
        $urlData['base'] = $this->_state->getUrl()->getHttpSchemeAndHost();
        $urlData['page'] = $urlData['base'] . ltrim(str_replace('?' . $urlData['query'], "", $urlData['path']), '/');
        $urlData['full'] = $urlData['base'] . ltrim($urlData['path'], '/');
        $this->_renderData['_url'] = $urlData;
        $this->_renderData['_site'] = $this->_state->getSites()->getCurrentSite()->getData();
        $this->_twig->setLoader($loader);
    }

    private function getSkinDir()
    {
        $app = $this->getAppRef();
        $skin = $this->getSkin($app);
        $theme = $this->getTheme($app);
        $appThemeRef = $app;


        if ($appThemeRef == 'admin' || $this->getParentSystem() == 'admin') {
            $appThemeRef = '_admin';
        } elseif ($appThemeRef == 'crm' && $this->statusCode == 404) {
            $appThemeRef = '_admin';
        }
        //@todo - find an elegant way to do the above, not hard coding...

        $themeBaseDir = $skinDir = $this->_state->getUrl()->getHttpSchemeAndHost() . 'theme/';

        if ($this->hasCustomTheme() && $this->hasCustomSkin()) {
            $skinDir = $themeBaseDir . "{$theme}/{$skin}/";
        } elseif (is_dir(LOTE_WEB_PATH . "theme/{$appThemeRef}/" . $skin)) {
            $skinDir = $themeBaseDir . "{$appThemeRef}/" . $skin . '/';
        } elseif (isset($this->_configData['system_theme'][$app])) {
            $skinDir = $themeBaseDir . "{$appThemeRef}/" . $this->_configData['system_theme'][$app] . '/';
        } elseif ($theme != "default" && $appThemeRef) {
            $skinDir = $themeBaseDir . "{$appThemeRef}/{$theme}/";
        } else {
            $skinDir = $themeBaseDir . "{$appThemeRef}/{$skin}/";
        }

        return $skinDir;
    }

    private function removeInvalidPaths($paths)
    {
        $result = [];
        foreach ($paths as $v) {
            if (is_dir($v)) {
                $result[] = $v;
            }
        }
        return $result;
    }

    public function loginRequired()
    {
        if ($this->_state->getRequest()->isXmlHttpRequest()) {
            $this->_state->getResponse()->setStatusCode('401', 'Login Required');
            $this->_state->getResponse()->send();
        } else {
            $requestUri = $this->_state->getRequest()->getRequestUri();
            $url = $this->getSystemLoginRoute($requestUri);
            if ($requestUri != '' && $requestUri != '/') {
                $url .= '/redirect/' . base64_encode($requestUri);
            }
            if (strpos($url, '/') !== 0) {
                $url = '/' . $url;
            }
            $response = new RedirectResponse($this->_state->getRequest()->getSchemeAndHttpHost() . $url);
            $response->send();
        }
    }

    public function twoFactorRequired()
    {
        if ($this->_state->getRequest()->isXmlHttpRequest()) {
            $this->_state->getResponse()->setStatusCode('401', 'Two step login required for access to this resource.');
            $this->_state->getResponse()->send();
        } else {
            $requestUri = $this->_state->getRequest()->getRequestUri();
            $url = $this->getSystemTwoFactorLoginRoute($requestUri);
            if ($requestUri != '' && $requestUri != '/') {
                $url .= '/' . base64_encode($requestUri);
            }
            if (strpos($url, '/') !== 0) {
                $url = '/' . $url;
            }
            $response = new RedirectResponse($this->_state->getRequest()->getSchemeAndHttpHost() . $url);
            $response->send();
        }
    }

    public function accessDenied()
    {
        if (!$this->_state->getUser()->id) {
            $this->loginRequired();
        } else {
            if ($this->_state->getRequest()->isXmlHttpRequest()) {
                $this->success = false;
                $this->_state->getResponse()->setStatusCode('403', 'Access Denied');
                $this->_state->getResponse()->send();
            } else {
                $requestUri = $this->_state->getRequest()->getRequestUri();
                $url = $this->getSystemUnauthorisedRoute($requestUri);
                if (strpos($url, '/') !== 0) {
                    $url = '/' . $url;
                }
                $response = new RedirectResponse($this->_state->getRequest()->getSchemeAndHttpHost() . $url);
                $response->send();
            }
        }
    }

    private function getSystemUnauthorisedRoute($requestUri)
    {
        $c = new BaseController($this->_state);
        $appRef = $this->_state->getAppByUrl($requestUri);
        if (!$this->_state->getAccess()->hasAccess($appRef, "view")) {
            $appRef = "website";
        }
        if ($route = $this->_state->getRouter()->get($appRef . '.access.denied')) {
            $result = $route->compile()->getStaticPrefix();
        } else {
            $result = $c->getRouteAppPrefix($this->_state->getAppByUrl($requestUri), true) . '/access-denied';
        }

        $eventData = new Data(["url" => $result]);
        $this->_state->getEventManager()->dispatch('access-denied', $eventData);

        return $eventData->getData("url");
    }

    private function getSystemLoginRoute($requestUri)
    {
        $c = new BaseController($this->_state);
        $appRef = $this->_state->getAppByUrl($requestUri);
        if ($route = $this->_state->getRouter()->get($appRef . '.login')) {
            $result = $route->compile()->getStaticPrefix();
        } else {
            $result = $c->getRouteAppPrefix($this->_state->getAppByUrl($requestUri), true) . '/login';
        }
        return $result;
    }

    private function getSystemTwoFactorLoginRoute($requestUri)
    {
        $c = new BaseController($this->_state);
        $appRef = $this->_state->getAppByUrl($requestUri);
        if ($route = $this->_state->getRouter()->get($appRef . '.login.two_factor')) {
            $result = $route->compile()->getStaticPrefix();
        } else {
            $result = $c->getRouteAppPrefix($this->_state->getAppByUrl($requestUri), true) . '/login/two-factor';
        }
        return $result;
    }

    public function redirect($path, $headerCode = 301)
    {
        parent::redirect($path);
        $headers = $this->_state->getResponse()->headers;
        $headers->add(['Location' => $path]);
        $this->_state->getResponse()->setStatusCode($headerCode);
    }

    private function addCustomPaths()
    {
        foreach ($this->customPaths as $v) {
            $this->paths[] = $v;
        }
    }

    public function addCustomPath($path)
    {
        $this->customPaths[] = $path;
        $this->renderSetup();
    }

    public function addChildAppPath($parentApp, $app)
    {
        $this->customPaths[] = LOTE_CORE_PATH . 'view/' . $app . '/' . $this->_state->getSettings()->get($parentApp . '.skin',
                'default') . '/';
    }

    public function addWebPath()
    {
        $this->customPaths[] = $this->paths[] = LOTE_APP_PATH . 'view/website/' . $this->_state->getSettings()->get('website.skin',
                'default') . '/';
    }

    protected function fullRenderRequired()
    {
        $result = false;
        $route = $this->_state->getRoute();
        if ($this->getAppRef() == 'website' || (isset($route['_render']) && $route['_render'])) {
            $result = true;
        }
        return $result;
    }

    public function getSourceContext($name)
    {
        // TODO: Implement getSourceContext() method.
    }

    public function getCacheKey($name)
    {
        // TODO: Implement getCacheKey() method.
    }

    public function isFresh($name, $time)
    {
        // TODO: Implement isFresh() method.
    }

    public function exists($name)
    {
        // TODO: Implement exists() method.
    }

    public function getSource($name)
    {
        // TODO: Implement getSource() method.
    }

}
