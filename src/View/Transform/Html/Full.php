<?php
namespace BlueSky\Framework\View\Transform\Html;

use Detection\MobileDetect;
use Psr\Log\LogLevel;
use BlueSky\Framework\Service\Content;

class Full extends Base
{

    protected function getIndexFileName()
    {
        $result = 'index.twig';
        $m = new MobileDetect();
        if ($m->isMobile() && !$m->isTablet()) {
            $this->_renderData['is_mobile'] = true;
            if ($this->hasTemplate('index_mobile.twig')) {
                $result = 'index_mobile.twig';
            }
        }
        if ($m->isTablet()) {
            $this->_renderData['is_tablet'] = true;
        }
        return $result;
    }

    protected function hasTemplate($name)
    {
        $result = false;
        try {
            $this->_twig->loadTemplate($name);
            $result = true;
        } catch (\Exception $e) {
        }
        return $result;
    }

    public function render($viewRef = "")
    {
        if ($this->customContent !== false) {
            $result = $this->customContent;
        } else {
            $this->renderSetup();
            if ($this->customInnerContent) {
                $this->_renderData['_content_'] = $this->customInnerContent;
            } else if (isset($this->_renderFile)) {
                try {
                    $this->_renderData['_content_'] = $this->_twig->render($this->_renderFile . '.twig', $this->_renderData);
                } catch (\Twig_Error_Loader $e) {
                    echo($e->getMessage());
                    $this->_renderData['_content_'] = "Unable to render view - {$this->_renderFile} ".$e->getMessage();
                    $this->_state->getLoggers()->getMasterLogger("lote")->error($this->_state->accountReference . "-error trying to load view-" . $this->_renderFile, $this->_state->getDebug()->getLogArray(LogLevel::ERROR));
                }
            }
            try {
                $result = $this->_twig->render($this->getIndexFileName(), $this->_renderData);
            } catch (\Twig_Error_Loader $e) {
                $result = "";
                echo($e->getMessage());
                $this->_renderData['_content_'] = "Unable to render index file - {$this->getIndexFileName()}";
                $this->_state->getLoggers()->getMasterLogger("lote")->error($this->_state->accountReference . "-error trying to load index-" . $this->getIndexFileName(), $this->_state->getDebug()->getLogArray(LogLevel::ERROR));
            }
            if ($this->fullRenderRequired()) {
                $c = new Content($this->_state);
                $result = $c->renderContent($result);
            }

        }
        return $result;
    }

}