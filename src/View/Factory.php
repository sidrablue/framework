<?php
namespace BlueSky\Framework\View;

use BlueSky\Framework\Util\Strings;

class Factory
{

    private static $referenceMap = [
        'html' => 'BlueSky\Framework\View\Transform\Html\Full',
        'cli' => 'BlueSky\Framework\View\Transform\Html\Full',
        'lote/shtml' => 'BlueSky\Framework\View\Transform\Html\Single',
        'shtml' => 'BlueSky\Framework\View\Transform\Html\Single',
        'embed' => 'BlueSky\Framework\View\Transform\Html\Embed',
        'json' => 'BlueSky\Framework\View\Transform\Json',
        'jsonp' => 'BlueSky\Framework\View\Transform\Jsonp',
        'rss' => 'BlueSky\Framework\View\Transform\Rss',
        //'cli'        => 'BlueSky\Framework\View\Transform\Cli',
        'wkpdf' => 'BlueSky\Framework\View\Transform\WkhtmlToPdf'
    ];

    private static $acceptMap = [
        'application/json' => 'BlueSky\Framework\View\Transform\Json',
        'application/rss+xml' => 'BlueSky\Framework\View\Transform\Json'
    ];

    public static function createInstance(\BlueSky\Framework\State\Base $state, string $reference = "html"): Base
    {
        $className = false;
        if (php_sapi_name() === 'cli') {
            $className = self::$referenceMap['cli'];
        } else {
            $contentTypes = $state->getRequest()->getAcceptableContentTypes();
            foreach ($contentTypes as $type) {
                if (array_key_exists($type, self::$acceptMap)) {
                    $className = self::$acceptMap[$type];
                    break;
                }
            }
            if(!$className) {
                $route = $state->getRoute();
                if(isset($route['_view']) && array_key_exists($route['_view'], self::$referenceMap)) {
                    $className = self::$referenceMap[$route['_view']];
                }
            }
        }
        if (!$className) {
            if (Strings::startsWith("/_api/", $state->getRequest()->getRequestUri())) {
                $className = self::$referenceMap["json"];
            } elseif ($route = $state->getRoute() && isset($route['format']) && array_key_exists($route['format'], self::$referenceMap)) {
                $className = self::$referenceMap[$route['format']];
            } else {
                $className = self::$referenceMap["html"];
            }
            if (array_key_exists($reference, self::$referenceMap)) {
                $className = self::$referenceMap[$reference];
            }
        }
        return new $className($state);
    }

}
