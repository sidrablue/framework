<?php
namespace BlueSky\Framework\Application\App;

use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use BlueSky\Framework\Entity\Schema\Entity as SchemaEntity;

class Finder
{

    public function getLibEntityClasses($reference)
    {
        $result = [];
        foreach ($this->getEntityFiles(LOTE_LIB_PATH . "app/{$reference}/src/Entity/") as $path) {
            if ($entityClass = $this->getEntityClass(LOTE_LIB_PATH . "app/{$reference}/src/Entity", "Lote\\App\\{$reference}\\Entity\\", $path)) {
                $result[] = $entityClass;
            }
        }
        return $result;
    }

    public function getCoreEntityClasses()
    {
        $result = [];
        foreach ($this->getEntityFiles(LOTE_LIB_PATH . "vendor/bluesky/framework/src/Entity/") as $path) {
            if ($entityClass = $this->getEntityClass(LOTE_LIB_PATH . "vendor/bluesky/framework/src/Entity", 'BlueSky\\Framework\\Entity\\', $path)) {
                $result[] = $entityClass;
            }
        }
        return $result;
    }

    public function getProjectEntityClasses()
    {
        $result = [];
        foreach ($this->getEntityFiles(LOTE_APP_PATH . "/src/Entity/") as $path) {
            if ($entityClass = $this->getEntityClass(LOTE_APP_PATH . "src/Entity", APP_NAMESPACE.'Entity\\', $path)) {
                $result[] = $entityClass;
            }
        }
        return $result;
    }

    function getEntityFiles($folder)
    {
        $result = [];
        $path = realpath($folder);
        if($path) {
            foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path)) as $filename) {
                if (preg_match("#\.php$#", $filename->getPathname(), $matches)) {
                    $result[] = $filename->getPathname();
                }
            }
        }
        return $result;
    }

    private function getEntityClass($baseClassPath, $classPath, $path)
    {
        $result = false;
        if (file_exists($path)) {
            require_once $path;
            $info = pathinfo($path);
            $dirname = str_replace("\\", '/', $info['dirname']);
            $subPath = str_replace($baseClassPath, '', $dirname);
            if($subPath) {
                $subPath = substr($subPath, 1);
                $subPath = str_replace("/", '\\', $subPath).'\\';
            }
            $classPath = $classPath . $subPath. $info['filename'];
            $class = new \ReflectionClass($classPath);
            if (!$class->isAbstract()) {
//                if ($classPath !== SchemaEntity::class) {
//                    $object = new $classPath();
//                } else {
//                    $object = SchemaEntity::getInstance();
//                }
                $className = $class->getName();
                if (is_subclass_of($classPath, 'BlueSky\Framework\Object\Entity\Base', true)) {
                    $result = $className;
                }
            }
        }
        return $result;
    }

}
