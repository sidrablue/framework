<?php


declare(strict_types = 1);
namespace BlueSky\Framework\Application\App;

use Doctrine\DBAL\Schema\Schema as DbalSchema;
use Doctrine\DBAL\Schema\SchemaException;
use Doctrine\DBAL\Schema\Table;
use ReflectionClass;
use BlueSky\Framework\Entity\Schema\Entity;
use BlueSky\Framework\Entity\Schema\Field as FieldEntity;
use zpt\anno\Annotations;
use zpt\anno\ReflectorNotCommentedException;
use BlueSky\Framework\Object\Entity\Base as BaseEntity;

class Schema
{

    public function getSchema(DbalSchema $toSchema, string $className, bool $isMaster): bool
    {
        $output = false;
        if (class_exists($className)) {
            try {
                $classAnno = new Annotations(new ReflectionClass($className));
                if ($classAnno->hasAnnotation("dbEntity") && $classAnno->offsetGet("dbEntity") === true) {
                    $classReflector = new ReflectionClass($className);
                    if ((!$isMaster && $this->isEntityClass($classReflector) && !$this->isEntityMasterClass($classReflector)) || ($isMaster && $this->isEntityMasterClass($classReflector, $classAnno))) {
                        try {
                            $table = $this->getClassTable($toSchema, $className);
                            $this->addAnnotatedColumns($classReflector, $table);
                            $this->setDefaultTablePrimaryKey($table);
                            $this->addClassIndexes($table, $classAnno);
                            $this->addClassUniqueIndexes($table, $classAnno);
                            $output = true;
                        } catch (SchemaException $e) {

                        }
                    }
                }
            } catch (\ReflectionException $e) {
            } catch (ReflectorNotCommentedException $e) {
            }
        }
        return $output;
    }

    /**
     * @param DbalSchema $toSchema
     * @param string $className
     * @return Table $table
     * @throws \Doctrine\DBAL\Schema\SchemaException
     */
    private function getClassTable(DbalSchema $toSchema, string $className): Table
    {
//        $classObject = new $className();
        /** @var \BlueSky\Framework\Object\Entity\Base $classObject */
//        $tableName = $classObject->getTableName();
        /** @var BaseEntity $className */
        $tableName = $className::TABLE_NAME;
        try {
            if ($toSchema->hasTable($tableName)) {
                $table = $toSchema->getTable($tableName);
            } else {
                $table = $toSchema->createTable($tableName);
            }
        } catch (\Exception $e) {
            throw new \Exception("Invalid table name specified for class {$className}. Table is '{$tableName}'");
        }
        return $table;
    }

    /**
     * @param $class
     * @param Table $table
     * @param array $columnFilter
     * @throws \ReflectionException
     */
    public function addColumnsByClass($class, Table $table, array $columnFilter = []): void
    {
        if (is_object($class) || (is_string($class) && class_exists($class))) {
            $classReflector = new ReflectionClass($class);
            $this->addAnnotatedColumns($classReflector, $table, $columnFilter);
        }
    }

    /**
     * @param DbalSchema $toSchema
     * @param Entity $entity
     * @throws SchemaException
     */
    public function getSchemaByEntity(DbalSchema $toSchema, Entity $entity)
    {

        $tableName = $entity->getEntityTableName();
        if ($toSchema->hasTable($tableName)) {
            $table = $toSchema->getTable($tableName);
        } else {
            $table = $toSchema->createTable($tableName);
        }

        $loteFields = $entity->getLoteFields();
        $idField = $loteFields['id'];
        $this->addColumnToSchema($table, $idField);
        $this->setDefaultTablePrimaryKey($table);
        unset($loteFields['id']);

        foreach ($entity->getEntityFields() as $fieldEntity) {
            $this->addColumnToSchema($table, $fieldEntity);
        }

        foreach ($loteFields as $loteField) {
            $this->addColumnToSchema($table, $loteField);
        }

        foreach ($entity->getChildEntities() as $entity) {
            $this->getSchemaByEntity($toSchema, $entity);
        }

    }

    private function addColumnToSchema(Table $table, FieldEntity $fieldEntity)
    {
        $column = $fieldEntity->reference;
        if (!$table->hasColumn($column)) {
            $type = $fieldEntity->db_type;
            /// FIXME: this needs to update to check the field_type not db_type
            if ($type === 'fkey') {
                if (class_exists($fieldEntity->fk_object)) {
                    $fTable = $fieldEntity->fk_object::TABLE_NAME;
                    $fColumn = 'id';
                    $table->addColumn($column, 'integer');
                    $table->addForeignKeyConstraint($fTable, [$column], [$fColumn]);
                } else {
                    echo "fkey is wrong in " . $fieldEntity->reference . PHP_EOL;
                }
            } else {
                $options = $fieldEntity->db_options ?? [];
                /// FIXME: remove the fkey check after the FIXME in the parent if block has been fixed
                if ($fieldEntity->is_virtual && $fieldEntity->field_type !== 'fkey') {
                    $options['notnull'] = false;
                }
                $table->addColumn($column, $type, $options);
            }

            // TODO this needs to be re-enabled after EP 2.0 migration
            /*if ($fieldEntity->is_unique) {
                $table->addUniqueIndex([$column]);
            } else*/if ($fieldEntity->db_index) {
                /// TODO: remove this once add in full text index for text type columns
                if ($fieldEntity->db_type !== 'text' && $fieldEntity->db_type !== 'json') {
                    $table->addIndex([$column]);
                }
            }
        }
    }

    private function addAnnotatedColumns(ReflectionClass $classReflector, Table $table, $columnFilter = [])
    {
        foreach ($classReflector->getProperties() as $methodReflector) {
            if ($this->propertyBelongsToEntityOrEntityBaseClasses($methodReflector, $classReflector)) {
                $anno = new Annotations($methodReflector);
                if ($anno->hasAnnotation('fieldType')) {
                    $column = $anno->hasAnnotation('dbColumn') ? $anno->offsetGet("dbcolumn") : $methodReflector->getName();
                    if (!$table->hasColumn($column) && (empty($columnFilter) || in_array($column, $columnFilter))) {
                        $fieldType = $anno->offsetGet("fieldtype");
                        $type = FieldEntity::FIELD_TO_DB_MAP[$fieldType];
                        /** @fixme remove this if block when foreign keys are implemented */
                        if ($type === 'fkey') {
                            $type = 'integer';
                        }
                        $options = [];
                        if ($anno->hasAnnotation("dboptions")) {
                            $options = $anno->offsetGet("dboptions");
                        }
                        $table->addColumn($column, $type, $options);
                        if ($anno->hasAnnotation("dbindex") && $anno->offsetGet("dbindex") === true) {
                            $table->addIndex([$column]);
                        }
                        if ($anno->hasAnnotation("dbFk") && $anno->offsetGet("dbFk") == true) {
                            $fk = $anno->offsetGet('dbFk');
                            if (isset($fk['self']) && $fk['self']) {
                                $fk['table'] = $table->getName();
                                $fk['column'] = 'id';
                            }
                            $table->addForeignKeyConstraint($fk['table'], [$column], [$fk['column']]);
                        }
                    }

                }
            }
        }
    }

    private function setDefaultTablePrimaryKey(Table $table)
    {
        if (!$table->getPrimaryKey() && $table->hasColumn("id")) {
            $table->setPrimaryKey(['id']);
        }
    }

    private function addClassIndexes(Table $table, Annotations $classAnnotation)
    {
        $this->addClassIndexBase($table, $classAnnotation, 'dbIndex', 'addIndex');
    }

    private function addClassUniqueIndexes(Table $table, Annotations $classAnnotation)
    {
        $this->addClassIndexBase($table, $classAnnotation, 'dbUniqueIndex', 'addUniqueIndex');
    }

    private function addClassIndexBase(
        Table $table,
        Annotations $classAnnotation,
        $annotationName = 'dbIndex',
        $functionName = 'addIndex'
    )
    {
        if ($classAnnotation->hasAnnotation($annotationName)) {
            $dbIndex = $classAnnotation->offsetGet($annotationName);
            if (is_array($dbIndex) && count($dbIndex) > 0) {
                $table->{$functionName}($dbIndex);
            }
        }
    }

    private function propertyBelongsToEntityOrEntityBaseClasses(
        \ReflectionProperty $property,
        ReflectionClass $classReflector
    )
    {
        return is_subclass_of($classReflector->getName(),
                '\BlueSky\Framework\Object\Entity\Base') || $classReflector->getParentClass()->getName() == 'BlueSky\Framework\Object\Entity\Base' || $classReflector->getName() == 'BlueSky\Framework\Object\Entity\Base';
    }

    private function isEntityClass(ReflectionClass $reflectionClass, $directChild = false)
    {
        return $reflectionClass->isSubclassOf('BlueSky\Framework\Object\Entity\Base') && !$reflectionClass->isSubclassOf('BlueSky\Framework\Object\Entity\BaseMaster');
        //return $reflectionClass->getParentClass()->getName() == 'BlueSky\Framework\Object\Entity\Base' || is_subclass_of($reflectionClass->getParentClass()->getName(), '\BlueSky\Framework\Object\Entity\Base');
    }

    private function isEntityMasterClass(ReflectionClass $reflectionClass, Annotations $classAnno = null)
    {
        return $reflectionClass->isSubclassOf('BlueSky\Framework\Object\Entity\BaseMaster') || ($classAnno && $classAnno->hasAnnotation("dbMasterEntity"));
    }

}
