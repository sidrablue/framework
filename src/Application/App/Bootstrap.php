<?php
namespace BlueSky\Framework\Application\App;

use BlueSky\Framework\Application\Bootstrap\Component;
use BlueSky\Framework\Application\Config as AppConfig;
use BlueSky\Framework\View\Transform\Html\Base as BaseHtmlView;

/**
 *  */
class Bootstrap extends Component
{

    /**
     * @var AppConfig $config
     * */
    private $config;


    /**
     * Setup the access namespaces for this system
     * @return void
     * */
    public function setupViewPath()
    {
        $params = func_get_args();
        if (isset($params[0]) && $params[0] instanceof BaseHtmlView) {
            /** @var BaseHtmlView $view */
            $view = $params[0];

            if (!empty($params[1]) && !empty($params[2])) {
                $systemRef = $params[1];
                $skinDir = $params[2];
                $path = $this->config->getDirectory() . "view/" . strtolower($systemRef) . "/{$skinDir}/";
                if (file_exists($path)) {
                    $view->paths[] = $path;
                } else {
                    $path = $this->config->getDirectory() . "/view/{$systemRef}/default/";
                    if (file_exists($path)) {
                        $view->paths[] = $path;
                    }
                }
            }
            $appPath = LOTE_APP_PATH . $this->config->getReference() . "/view/_base/default/";
            $libAppPath = LOTE_LIB_APP_PATH . $this->config->getReference() . "/view/_base/default/";
            if (file_exists($appPath)) {
                $view->paths[] = $appPath;
            } elseif (file_exists($libAppPath)) {
                $view->paths[] = $libAppPath;
            }
        }
    }

    public function setConfig($config)
    {
        $this->config = $config;
    }

    public function getConfig()
    {
        return $this->config;
    }

}
