<?php
namespace BlueSky\Framework\Application\Build;

use DirectoryIterator;
use League\Flysystem\Filesystem;
use League\Flysystem\Adapter\Local as LocalAdapter;
//use Falc\Flysystem\Plugin\Symlink\Local as LocalSymlinkPlugin;
use BlueSky\Framework\Util\Dir;
use Symfony\Component\Console\Style\SymfonyStyle;

class Builder
{

    /** @var Filesystem $fs */
    private $fs;
    /** @var SymfonyStyle $io */
    private $io;

    public function __construct(SymfonyStyle $io = null)
    {
        $this->io = $io;
        //$this->fs = new Filesystem(new LocalAdapter('/'));
        //$this->fs->addPlugin(new LocalSymlinkPlugin\Symlink());
        //$this->fs->addPlugin(new LocalSymlinkPlugin\IsSymlink());
    }

    public function build()
    {
        Dir::make(LOTE_BASE_PATH . "/lib/app");
        $config = json_decode(file_get_contents(LOTE_BASE_PATH . "/lote.json"), true);
        $this->updateOrInstallApps($config);
        //$this->uninstallRemovedApps($config);

    }

    private function updateOrInstallApps($config)
    {
        foreach ($config['apps'] as $k => $v) {
            chdir(LOTE_LIB_APP_PATH);
            if (file_exists("$k")) {
                $this->updateApp($k, $v);
            } else {
                $this->installApp($k, $v);
            }
        }
        $this->addWebSymlinkIfMissing();
    }

    private function uninstallRemovedApps($config)
    {
        $expectedApps = [];// array_keys($config['apps']);
        $currentApps = $this->getCurrentInstalledApps();
        $appsToRemove = array_diff($currentApps, $expectedApps);
        foreach ($appsToRemove as $app) {
            $this->uninstallApp($app, $config['apps'][$app]);
        }
    }

    private function getCurrentInstalledApps()
    {
        $dirs = [];
        foreach (new DirectoryIterator(LOTE_LIB_APP_PATH) as $file) {
            if ($file->isDir() && !$file->isDot()) {
                $dirs[] = $file->getFilename();
            }
        }
        return $dirs;
    }

    public function addWebSymlinkIfMissing()
    {
        foreach (glob(LOTE_LIB_APP_PATH . '**/web') as $path) {
            $appReference = str_replace(LOTE_LIB_APP_PATH, '', substr($path, 0, strlen($path) - 4));
            if (is_dir(LOTE_LIB_APP_PATH . $appReference . '/web')) {
                $projectSymlinkLocation = LOTE_BASE_PATH . 'web/lib/' . $appReference;
                if (!file_exists($projectSymlinkLocation)) {
                    Dir::make(LOTE_BASE_PATH . 'web/lib');
                    if (!@symlink(LOTE_LIB_APP_PATH . $appReference . '/web', $projectSymlinkLocation)) {
                        $this->io->error("Could not create symlink probably due to permission settings for $appReference");
                    }
                } elseif (!is_link($projectSymlinkLocation)) {
                    $this->io->error("Could not create symlink due to the file location being a directory for $appReference");
                }
            }
        }
    }

    private function installApp($appReference, $appData)
    {
        echo("Installing $appReference\n");
        if (isset($appData['source-url']) && isset($appData['source-branch'])) {
            shell_exec("git clone {$appData['source-url']} {$appReference} --branch={$appData['source-branch']}");
        }

    }

    private function updateApp($appReference, $appData)
    {
        echo("Updating $appReference\n");
        chdir(LOTE_LIB_APP_PATH . $appReference);
        shell_exec("git pull");
        echo("Updated $appReference\n");
    }

    private function uninstallApp($appReference, $appData)
    {
        Dir::delete(LOTE_LIB_APP_PATH . '/' . $appReference);
        die("Removed one...{$appReference}\n");
    }

}
