<?php
namespace BlueSky\Framework\Application\Bootstrap;

use ReflectionClass;
use BlueSky\Framework\Console\Application;
use BlueSky\Framework\Console\Command;
use BlueSky\Framework\Routing\Route;
use BlueSky\Framework\Routing\RouteCollection;
use BlueSky\Framework\Auth\Acl\Context;
use BlueSky\Framework\State\Base as StateBase;

/**
 *  */
class Component extends Base
{

    /**
     * Setup the access namespaces for this system
     * @param Context $context
     * @return array
     * */
    public function setupAccess(Context $context)
    {
        return [];
    }

    /**
     * Setup the hooks for this system
     * @return void
     * */
    public function setupClosures()
    {

    }

    public function setupUser()
    {

    }

    /**
     * Setup any new state DI objects
     * @param StateBase $state
     * @return array
     * */
    public function setupState(StateBase $state)
    {
        //do nothing
    }

    /**
     * Setup the login routes for this system
     * @param \BlueSky\Framework\Routing\RouteCollection $router
     * @param string $systemReference - the reference of the system
     * @param string $controller - the name of the login controller
     * @param string $urlPrefix - the URL prefix for login routes
     * @param array $baseDefaults - Base defaults to use
     * @internal Route $route
     */
    public function setupSystemLoginRoutes($router, $systemReference, $controller, $urlPrefix = '', $baseDefaults = [])
    {
        if($system = $this->getState()->getApps()->getSystem($systemReference)) {
            $defaults = ['_handler' => ['system' => $systemReference]];
            $defaults['controller'] = $controller;
            $defaults['_method'] = 'login';

            $defaults = array_merge($defaults, $baseDefaults);

            $route = new Route('/'.$urlPrefix.'/login', $defaults);
            $router->add($system->getName().' Login', $route);
            $route = new Route('/'.$urlPrefix.'/login/redirect/{redirect}', $defaults, ['redirect' => '.+']);
            $router->add($system->getName().' Login Redirect', $route);

            $requirements = ['format' => '(\.[a-z]+)?'];
            $defaults['_method'] = 'validate';
            $route = new Route('/'.$urlPrefix.'/login/validate{format}', $defaults, $requirements);
            $router->add($system->getName().' Login Validate', $route);

            $requirements = ['format' => '(\.[a-z]+)?'];
            $defaults['_method'] = 'refreshToken';
            $route = new Route('/'.$urlPrefix.'/login/refresh-token{format}', $defaults, $requirements);
            $router->add($system->getName().' Login Refresh Token', $route);

            $defaults['_method'] = 'logout';
            $route = new Route('/'.$urlPrefix.'/logout', $defaults);
            $router->add($system->getName().' Logout', $route);

            $defaults['_method'] = 'retrieve';
            $route = new Route('/'.$urlPrefix.'/retrieve', $defaults);
            $router->add($system->getName().' Login retrieve', $route);

            $defaults['_method'] = 'resetPassword';
            $route = new Route('/'.$urlPrefix.'/reset-password', $defaults);
            $router->add($system->getName().' Reset Password Page', $route);

            $defaults['_method'] = 'updatePassword';
            $route = new Route('/'.$urlPrefix.'/updatePassword{format}', $defaults, $requirements);
            $router->add($system->getName().' Update Password', $route);
        }
    }

    /**
     * Get the name of a Twig skin for a system by checking the settings and determining if an override exists
     * @access public
     * @param string $system
     * @param string $suffix
     * @param string $default
     * @return string
     * */
    protected function getSkinName($system, $suffix = '', $default = 'default') {
        $result = $default;
        if($this->getState()->getSettings()->get("$system.skin", false)) {
            $result = $this->getState()->getSettings()->get("$system.skin", false);
            if($suffix) {
                $result .= $suffix;
            }
        }
        return $result;
    }

    /**
     * Setup the console commands
     * the installed systems and modules
     * @param Application $consoleApplication
     * @return void
     */
    public function setupConsole(Application $consoleApplication)
    {
        $reflector = new ReflectionClass(get_class($this));
        $path = dirname($reflector->getFileName());
        $path = str_replace("\\","/", $path).'/Command';
        if(is_dir($path)) {
            $consoleApplication->addCommandsFromPath($path, $reflector->getNamespaceName().'\Command');
        }
    }

}
