<?php
namespace BlueSky\Framework\Application\Bootstrap;

/**
 *  */
class Admin extends Component
{

    protected function getParentSystem()
    {
        $result = false;
        $route = $this->getState()->getRoute();
        if (isset($route['_handler']['parent'])) {
            $result = $route['_handler']['parent'];
        }
        return $result;
    }

}

