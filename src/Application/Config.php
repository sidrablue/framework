<?php
namespace BlueSky\Framework\Application;

/**
 * Class to define a systems base parameters
 *
 * @package BlueSky\Framework\Application\Module
 * */
class Config
{

    /**
     * The version of this system
     * @var string $_version
     * */
    protected $_version;

    /**
     * The reference of this system
     * @var string $_reference
     * */
    protected $_reference;

    /**
     * The directory base of this system
     * @var string $_directory
     * */
    protected $_directory;

    /**
     * The namespace base of this app
     * @var string $namespace
     * */
    protected $namespace;

    /**
     * Get the app namespace
     * @access public
     * @return string
     */
    public function getNamespace()
    {
        return $this->namespace;
    }

    /**
     * Set the app namespace
     * @access public
     * @param string $namespace
     * @return void
     */
    public function setNamespace($namespace)
    {
        $this->namespace = $namespace;
    }

    /**
     * The database data base of this system
     * @var string $data
     * */
    public $data;

    /**
     * @param string $directory
     * @return void
     */
    public function setDirectory($directory)
    {
        $this->_directory = $directory;
    }

    /**
     * @param array $data
     * @return void
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return string
     */
    public function getDirectory()
    {
        return $this->_directory;
    }

    /**
     * Get the source code directory for this app
     *
     * @access public
     * @return string
     */
    public function getSrcDirectory()
    {
        return $this->_directory.'src/';
    }

    /**
     * @return string
     */
    public function getBootstrap()
    {
        return $this->getDirectory().'Bootstrap.php';
    }

    /**
     * @param string $reference
     */
    public function setReference($reference)
    {
        $this->_reference = $reference;
    }

    /**
     * @return string
     */
    public function getReference()
    {
        return $this->_reference;
    }

    /**
     * @param mixed $version
     */
    public function setVersion($version)
    {
        $this->_version = $version;
    }

    /**
     * @return mixed
     */
    public function getVersion()
    {
        return $this->_version;
    }

}
