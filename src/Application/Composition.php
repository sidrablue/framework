<?php
namespace BlueSky\Framework\Application;

use BlueSky\Framework\Auth\Acl\Context;
use BlueSky\Framework\Console\Application;
use BlueSky\Framework\Model\App;
use BlueSky\Framework\Object\State as StateBase;
use BlueSky\Framework\State\Base as State;

class Composition extends StateBase
{

    /**
     * @var Config[] $apps
     * */
    private $apps = [];

    /**
     * Default constructor for the site Configuration
     * @param \BlueSky\Framework\State\Base $state
     * @param boolean $loadApps - true if the apps should be auto loaded
     * @access public
     * */
    public function __construct(State $state, $loadApps = true)
    {
        parent::__construct($state);
        if ($loadApps) {
            $this->loadApps();
        }
    }

    /**
     * Force a reload of the apps
     * @access public
     * @return void
     * */
    public function reloadApps()
    {
        $this->apps = [];
        $this->loadApps();
    }

    /**
     * Load all of the apps in this install
     * @access private
     * @return void
     * */
    private function loadApps()
    {
        $this->addBaseApp();
        $m = new App($this->getState());
        foreach ($m->getAll() as $v) {
            $this->addAppDetails($v);
        }
    }

    private function addBaseApp()
    {
        global $loader;
        if(defined('APP_NAMESPACE') && file_exists(LOTE_APP_PATH.'/src/Bootstrap.php')) {
            $s = new Config();
            $s->setReference('base');
            $s->setDirectory(LOTE_APP_PATH);
            $s->setNamespace(APP_NAMESPACE);
            $this->apps[strtolower('base')] = $s;
            $loader->addPsr4(APP_NAMESPACE, LOTE_APP_PATH.'src/');
        }
    }

    /**
     * Add app details to the local app array
     * @access private
     * @param array $app
     * @return void
     */
    private function addAppDetails(array $app)
    {
        global $loader;
        $dir = LOTE_LIB_APP_PATH . $app['reference'] . '/';
        if (is_dir($dir)) {
            $s = new Config();
            $s->setReference($app['reference']);
            $s->setDirectory($dir);
            $s->setNamespace($namespace = '\Lote\App\\' . $app['reference'].'\\');
            $s->setData($app);
            $this->apps[strtolower($app['reference'])] = $s;
            $loader->addPsr4("Lote\\App\\" . $app['reference'] . "\\", LOTE_LIB_APP_PATH . $app['reference'] . '/src');
        }
    }

    /**
     * Get all of the apps in the system.
     *
     * @access public
     * @return Config[]
     * */
    public function getAll()
    {
        return $this->apps;
    }

    /**
     * Check if this system has a specified app
     * @access public
     * @param string $reference
     * @return boolean
     * */
    public function has($reference)
    {
        return $this->get($reference) instanceof Config;
    }

    /**
     * Get an app by its reference
     * @access public
     * @param string $reference
     * @return boolean|Config
     * */
    public function get($reference)
    {
        $result = false;
        if (isset($this->apps[strtolower($reference)])) {
            $result = $this->apps[strtolower($reference)];
        }
        return $result;
    }

    /**
     * Setup the routes for the system, by calling all of the setupRoutes functions in the bootstraps of
     * the installed systems and modules
     * @param
     * @return void
     */
    public function setupRoutes($router)
    {
        $this->callAllApps("setupRoutes", [$router]);
    }

    /**
     * Setup the routes for the system, by calling all of the setupRoutes functions in the bootstraps of
     * the installed systems and modules
     * @param
     * @return void
     */
    public function setupData()
    {
        $this->callAllApps("setupData", null, true);
    }

    /**
     * Setup the route view paths for the system, by calling all of the setupRoutes functions in the bootstraps of
     * the installed systems and modules
     * @param
     * @return void
     */
    public function setupViewPath()
    {
        $this->callAllApps("setupViewPath", func_get_args(), false, true);
    }

    /**
     * Setup the signals for the system, by calling all of the setupSignals functions in the bootstraps of
     * the installed systems and modules
     * @param \BlueSky\Framework\Event\Manager $eventManager
     * @return void
     */
    public function registerEvents($eventManager)
    {
        $this->callAllApps("registerEvents", [$eventManager]);
    }

    /**
     * Setup the access namespaces for the system, by calling all of the setupAccess functions in the bootstraps of
     * the installed systems and modules
     * @return void
     */
    public function setupClosures()
    {
        $this->callAllApps("setupClosures", [$this->getState()]);
    }

    /**
     * Setup the console commands
     * the installed systems and modules
     * @param Application $consoleApplication
     * @return void
     */
    public function setupConsole(Application $consoleApplication)
    {
        $this->callAllApps("setupConsole", [$consoleApplication]);
    }

    /**
     * Setup the access namespaces for the system, by calling all of the setupAccess functions in the bootstraps of
     * the installed systems and modules
     * @param Context $access - an array of accesses
     * @return void
     */
    public function setupAccess(Context $access)
    {
        $this->callAllApps("setupAccess", [$access]);
    }

    /**
     * Setup the access namespaces for the system, by calling all of the setupAccess functions in the bootstraps of
     * the installed systems and modules
     * @return void
     */
    public function setupState()
    {
        $this->callAllApps("setupState", [$this->getState()]);
    }

    /**
     * Setup the view data for rendering
     * @param \BlueSky\Framework\View\Base $view - the view Reference
     * @param string $reference - the view Reference
     * @access public
     * @return void
     * */
    public function setupView($view, $reference)
    {
        $this->callAllApps("setupView", [$view, $reference]);
    }

    /**
     * Setup the plugins
     * @access public
     * @return void
     */
    public function setupPlugins()
    {
        $this->callAllApps("setupPlugins");
    }

    private function callAllApps($function, $params = [], $useAppData = false, $userViewOrder = false)
    {
        if ($userViewOrder) {
            $route = $this->getState()->getRoute();
            if (isset($route['_handler']) && isset($route['_handler']['order']) && is_array($route['_handler']['order'])) {
                $apps = [];
                $initialApps = array_values($this->getAll());
                $order = $route['_handler']['order'];
                foreach ($initialApps as $counter => $app) {
                    /** @var \BlueSky\Framework\Application\Config $app */
                    if ($app->getReference() == 'base') {
                        $apps[-1] = $app;
                    } elseif (in_array($app->getReference(), $order)) {
                        $apps[intval(array_search($app->getReference(), $order))] = $app;
                    } else {
                        $apps[intval(count($order) + $counter)] = $app;
                    }
                }
                ksort($apps);
                $apps = array_values($apps);
            } else {
                $apps = $this->getAll();
            }
        } else {
            $apps = $this->getAll();
        }

        foreach ($apps as $app) {
            /** @var \BlueSky\Framework\Application\Config $app */

            if (file_exists($app->getDirectory() . 'src/Bootstrap.php')) {
                if ($useAppData) {
                    $params = $app->data;
                }
                $this->callAppBootstrapFunction($app, $function, $params);
            }
        }
    }

    private function callAppBootstrapFunction(\BlueSky\Framework\Application\Config $app, $function, $params = [])
    {
        $namespace = $app->getNamespace().'Bootstrap';
        /** @var $class \BlueSky\Framework\Application\App\Bootstrap */
        if(class_exists($namespace)) {
            $class = new $namespace($this->getState());
            $class->setConfig($app);
            if (method_exists($class, $function)) {
                call_user_func_array([$class, $function], $params);
            }
        }
    }

    /**
     * Setup the signals for the system, by calling all of the setupSignals functions in the bootstraps of
     * the installed apps
     * @param \Aura\Signal\Manager $signalManager
     * @return void
     */
    public function setupSignals($signalManager)
    {
        $this->callAllApps("setupSignals", [$signalManager]);
    }

}
