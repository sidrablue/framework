<?php
namespace BlueSky\Framework\Model\Api;

use BlueSky\Framework\Object\Model\Base;

/**
 * Model class for a file
 *
 * @package Lote\System\Crm\Model
 */
class Key extends Base
{
    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__api_key';

    public function updateApi($detailsArr = [])
    {
        $id = isset($detailsArr['id']) ? intval($detailsArr['id']) : false;
        if (!empty($id)) {
            $this->getState()->getWriteDb()->update($this->tableName, $detailsArr, ['id' => $id]);
        }
        return $id;
    }

    public function getSearchQuery($phrase = '')
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("ak.*")
            ->from($this->getTableName(), 'ak')
            ->andWhere("ak.lote_deleted is null");

        if (!empty($phrase)) {
            $q = $this->getPhraseSplitQuery($q, $phrase, ['ak.name']);
        }

        return $q;
    }

} 