<?php
namespace BlueSky\Framework\Model\Api;

use BlueSky\Framework\Object\Model\Base;

/**
 * Model class for a Webhook
 *
 * @package Lote\System\Crm\Model
 */
class Webhook extends Base
{
    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__api_webhook';

    public function getSearchQuery($phrase = '')
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("aw.*")
            ->from($this->getTableName(), 'aw')
            ->andWhere("aw.lote_deleted is null");

        if (!empty($phrase)) {
            $q = $this->getPhraseSplitQuery($q, $phrase, ['aw.name']);
        }

        return $q;
    }

} 