<?php
namespace BlueSky\Framework\Model;

use BlueSky\Framework\Entity\Notification as NotificationEntity;

class NotificationBuilder
{
    /**
     * see BlueSky\Framework\Entity\Notification for field docs
     */
    private $id;
    private $parent_id;
    private $object_ref;
    private $object_id;
    private $user_id;
    private $subject;
    private $description;
    private $level;
    private $view_url;
    private $download_url;
    private $confirmation_required;
    private $data;

    /**
     * Create a new Builder
     * @return NotificationBuilder
     */
    public static function createInstance() { return new self(); }

    /**
     * Build the NoticationEntity based on fields set in this Builder
     * @param \BlueSky\Framework\Object\StateDb $state
     * @return NotificationEntity
     */
    public function build($state)
    {
        // Validate mandatory fields
        if (!isset($this->subject)) {
            throw new \DomainException("Subject field is required to build a Notification");
        }
        $obj = new NotificationEntity($state);
        $obj->setData($this->getFields());
        return $obj;
    }

    /**
     * Build the notification object then save it.
     * Saving submits the notification for viewing.
     * @param $state
     * @return NotificationEntity
     */
    public function buildAndSave($state)
    {
        $obj = $this->build($state);
        $obj->save();
        return $obj;
    }

    /**
     * Get associative array of this Builder's fields
     * @return mixed
     */
    public function getFields() { return get_object_vars($this); }

    /**
     * Set the Notification ID
     * @param $id
     * @return $this
     */
    public function id($id)
    {
        if (!ctype_digit(strval($id))) {
            throw new \InvalidArgumentException("ID must be an integer string or int-type");
        }
        $this->id = $id;
        return $this;
    }

    /**
     * Set the Parent ID
     * @param $id
     * @return $this
     */
    public function parentId($id)
    {
        if (!ctype_digit(strval($id))) {
            throw new \InvalidArgumentException("Parent ID must be an integer string or int-type");
        }
        $this->parent_id = $id;
        return $this;
    }

    /**
     * Reference an object
     * @param $objRef
     * @param $objId
     * @return $this
     */
    public function referenceObject($objRef, $objId)
    {
        if (empty($objRef) || !is_string(strval($objId))) {
            throw new \InvalidArgumentException("Object Reference must be a non-empty string");
        }
        if (!ctype_digit(strval($objId))) {
            throw new \InvalidArgumentException("Object ID must be an integer string or int-type");
        }
        $this->object_ref = $objRef;
        $this->object_id = $objId;
        return $this;
    }

    /**
     * Set the user ID
     * @param $id
     * @return $this
     */
    public function userId($id)
    {
        if (!ctype_digit(strval($id))) {
            throw new \InvalidArgumentException("User ID must be an integer string or int-type");
        }
        $this->user_id = $id;
        return $this;
    }

    /**
     * Set the subject line
     * @param $subject
     * @return $this
     */
    public function subject($subject)
    {
        if (empty($subject) || !is_string(strval($subject))) {
            throw new \InvalidArgumentException("Subject must be a non-empty string");
        }
        $this->subject = $subject;
        return $this;
    }

    /**
     * Set the description
     * @param $desc
     * @return $this
     */
    public function description($desc)
    {
        if (empty($desc) || !is_string(strval($desc))) {
            throw new \InvalidArgumentException("Description must be a non-empty string");
        }
        $this->description = $desc;
        return $this;
    }

    /**
     * Set the notification level
     */
    public function level($level)
    {
        // Validate level
        $this->level = $level;
        return $this;
    }

    /**
     * Set the URL to which the notification item will link
     * @param $url
     * @return $this
     */
    public function viewUrl($url)
    {
        if (empty($url) || !is_string($url)) {
            throw new \InvalidArgumentException("View URL must be a non-empty string");
        }
        $this->view_url = $url;
        return $this;
    }

    /**
     * Set the URL to which the download icon in the notification item will link
     * @param $url
     * @return $this
     */
    public function downloadUrl($url)
    {
        if (empty($url) || !is_string($url)) {
            throw new \InvalidArgumentException("Download URL must be a non-empty string");
        }
        $this->download_url = $url;
        return $this;
    }

    /**
     * Whether or not this notification requires confirmation
     * @param $isRequired
     * @return $this
     */
    public function confirmationRequired($isRequired)
    {
        if (!is_bool($isRequired)) {
            throw new \InvalidArgumentException("Confirmation Required field must be a boolean");
        }
        $this->confirmation_required = $isRequired;
        return $this;
    }

    /**
     * Additional data for this notification
     * @param $data
     * @return $this
     */
    public function data($data)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * Set all fields from an array
     * Requires array keys to match DB columns
     * @param $fieldsArr
     * @return $this
     */
    public function setFieldsFromArray($fieldsArr)
    {
        // Collect field names
        $fieldNames = array_keys(get_object_vars($this));

        // Check imported array for each member field.
        // Set member field whenever value is present in imported array.
        foreach ($fieldNames as $field) {
            if (isset($fieldsArr[$field])) {
                $this->{$field} = $fieldsArr[$field];
            }
        }
        return $this;
    }

    /**
     * Reset the values of all fields in the Builder
     */
    public function reset()
    {
        // Collect field names
        $fieldNames = array_keys(get_object_vars($this));
        // Unset all fields
        foreach ($fieldNames as $field) {
            $this->{$field} = null;
        }
        return $this;
    }
}