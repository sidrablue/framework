<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 */

declare(strict_types=1);

namespace BlueSky\Framework\Model;

use BlueSky\Framework\Object\Model\Base;
use BlueSky\Framework\Entity\ImportRow as ImportRowEntity;

/**
 * Model class for Importing
 */
class ImportRow extends Base
{

    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__import_row';

    /**
     * Get the latest import list
     * @param int $importId
     * @param String $note
     * @param array $data
     * @return array
     */
    public function saveError($importId, $note, $data)
    {
        return $this->saveRow($importId, $note, $data, 'error');
    }


    public function saveIgnore($importId, $note, $data)
    {
        return $this->saveRow($importId, $note, $data, 'ignored');
    }

    /**
     * Get the latest import list
     * @param int $importId
     * @param String $note
     * @param array $data
     * @param String $status
     * @return array
     */
    private function saveRow($importId, $note, $data, $status) {
        $m = new ImportRowEntity($this->getState());
        $m->import_id = $importId;
        $m->note = $note;
        $m->data = json_encode(array_map('utf8_encode', $data));
        $m->status = $status;
        $result = $m->save();
        $m = null;
        return $result;
    }



}