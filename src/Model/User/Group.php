<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace BlueSky\Framework\Model\User;

use BlueSky\Framework\Object\Model\Base;
/**
 * Model class for Users
 */
class Group extends Base
{

    protected $tableName = 'sb__user_group';

}
