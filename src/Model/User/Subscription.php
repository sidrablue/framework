<?php
declare(strict_types=1);

namespace BlueSky\Framework\Model\User;

use Doctrine\DBAL\Query\QueryBuilder;
use BlueSky\Framework\Entity\User\Subscription as SubscriptionEntity;
use BlueSky\Framework\Object\Model\Base as BaseModel;
use BlueSky\Framework\Entity\User\User as UserEntity;
use BlueSky\Framework\Entity\User\SubscriptionLogger as SubscriptionLoggerEntity;

class Subscription extends BaseModel
{
    protected $tableName = 'sb__user_subscription';

    public function addUserEmailSubscribedClause(QueryBuilder $q, int $emailStatus = 1, string $userTablePrefix = 'u', $subscriptionTablePrefix = ""): void
    {
        $subscriptionTablePrefix = $subscriptionTablePrefix ?: uniqid("usub_");
        $q->leftJoin($userTablePrefix, "sb__user_subscription", $subscriptionTablePrefix, "{$subscriptionTablePrefix}.user_id = {$userTablePrefix}.id")
            ->andWhere("{$subscriptionTablePrefix}.email = :email_status")
            ->setParameter('email_status', $emailStatus)
            ->andWhere("{$subscriptionTablePrefix}.lote_deleted is null");
    }

    public function addUserMobileSubscribedClause(QueryBuilder $q, int $mobileStatus = 1, string $userTablePrefix = 'u', $subscriptionTablePrefix = ""): void
    {
        $subscriptionTablePrefix = $subscriptionTablePrefix ?: uniqid("usub_");
        $q->leftJoin($userTablePrefix, "sb__user_subscription", $subscriptionTablePrefix, "{$subscriptionTablePrefix}.user_id = {$userTablePrefix}.id")
            ->andWhere("{$subscriptionTablePrefix}.mobile = :mobile_status")
            ->setParameter('mobile_status', $mobileStatus)
            ->andWhere("{$subscriptionTablePrefix}.lote_deleted is null");
    }

    public function addSubscriptionData(array &$users)
    {
        if ($data = $this->getListByValues($this->getEntityIds($users), "user_id", true)) {
            foreach ($users as $k=>$v) {
                $users[$k]->UserSubscription = $data[$v->id] ?? [];
            }
        }
    }


    /**
     * @param int|UserEntity $user
     * @param string $type - pick from Subscription Entity const
     * @param int $status - pick from Subscription Entity const
     * @param string|null $description - description to be saved in the logger
     * @param bool $forceAllowChange - To override the check to make sure the subscription can be changed or not
     * @return SubscriptionEntity - the item saved to the db
     */
    public function setSubscription($user, string $type, int $status, ?string $description = null, bool $forceAllowChange = false): SubscriptionEntity
    {
        if (!$this->validateSubscriptionType($type)) {
            throw new \InvalidArgumentException('Invalid Subscription Type');
        }
        if (!$this->validateSubscriptionStatus($type, $status)) {
            throw new \InvalidArgumentException('Invalid Subscription Status');
        }

        $se = new SubscriptionEntity($this->getState());
        $uid = $user;
        if ($user instanceof UserEntity) {
            $uid = $user->id;
        }
        $se->loadByField('user_id', $uid);
        if (empty($se->user_id)) {
            $se->user_id = $uid;
        }
        if ($se->{$type} != $status) {
            $allowedToSetStatus = true;
            if ($se->$type !== null && !$forceAllowChange) {
                /// this is only called for subscription that already exist.
                $allowedToSetStatus = $this->isStatusAllowedToChange($se, $type);
            }
            if ($allowedToSetStatus) {
                $se->$type = $status;
                $se->save();
                $this->getState()->getWriteDb()->createQueryBuilder()->update(SubscriptionLoggerEntity::TABLE_NAME)
                    ->set('is_current', 0)
                    ->andWhere('user_id = :uid')
                    ->setParameter('uid', $uid)
                    ->andWhere('channel = :channel')
                    ->setParameter('channel', $type)
                    ->execute();

                $sle = new SubscriptionLoggerEntity($this->getState());
                $sle->is_current = true;
                $sle->user_id = $uid;
                $sle->channel = $type;
                $sle->subscription_status = $status;
                $sle->description = $description;
                $sle->save();
            }
        }
        return $se;
    }

    private function isStatusAllowedToChange(SubscriptionEntity $e, string $type): bool
    {
        $output = true;
        switch ($type) {
            case SubscriptionEntity::TYPE_EMAIL:
                $output = $e->email !== SubscriptionEntity::ENUM_EMAIL_PERMANENT_UNSUBSCRIBED;
                break;
            case SubscriptionEntity::TYPE_MOBILE:
                $output = $e->mobile !== SubscriptionEntity::ENUM_MOBILE_PERMANENT_UNSUBSCRIBED;
                break;
        }
        return $output;
    }

    private function validateSubscriptionType(string $type): bool
    {
        return $type === SubscriptionEntity::TYPE_EMAIL ||
            $type === SubscriptionEntity::TYPE_MOBILE;
    }

    private function validateSubscriptionStatus(string $type, int $status): bool
    {
        return
            (
                $type === SubscriptionEntity::TYPE_EMAIL &&
                (
                    $status === SubscriptionEntity::ENUM_EMAIL_PERMANENT_UNSUBSCRIBED ||
                    $status === SubscriptionEntity::ENUM_EMAIL_SUBSCRIBED
                )
            ) ||
            (
                $type === SubscriptionEntity::TYPE_MOBILE &&
                (
                    $status === SubscriptionEntity::ENUM_MOBILE_PERMANENT_UNSUBSCRIBED ||
                    $status === SubscriptionEntity::ENUM_MOBILE_SUBSCRIBED
                )
            );
    }

    public function isCompletelyOptedOut(?string $email, ?string $mobile): bool
    {
        $output = false;
        $emailSet = false;
        $mobileSet = false;
        $whereStatement = '';
        if (!empty($email)) {
            $whereStatement = 'email = :email';
            $emailSet = true;
        }
        if (!empty($mobile)) {
            $mobileSet = true;
            if (!empty($whereStatement)) {
                $whereStatement .= ' or mobile = :mobile';
            } else {
                $whereStatement = 'mobile = :mobile';
            }
        }
        if (!empty($whereStatement)) {
            $whereStatement = '(' . $whereStatement . ')';
            $q = $this->getState()->getReadDb()->createQueryBuilder()
                ->select('*')
                ->from($this->getTableName())
                ->andWhere($whereStatement);

            if ($emailSet) {
                $q->setParameter('email', $email);
            }
            if ($mobileSet) {
                $q->setParameter('mobile', $mobile);
            }
            $q->andWhere('lote_deleted is null');

            $results = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
            foreach ($results as $result) {
                if (!empty($result['email']) && !empty($result['mobile'])) {
                    $output = $result['email'] == SubscriptionEntity::ENUM_EMAIL_PERMANENT_UNSUBSCRIBED && $result['mobile'] == SubscriptionEntity::ENUM_MOBILE_PERMANENT_UNSUBSCRIBED;
                    if ($output) {
                        break;
                    }
                }
            }
        }
        unset($emailSet, $mobileSet, $whereStatement, $q, $results, $result);
        return $output;
    }

    public function getCompletelyOptedOutMulti(array $input): array
    {
        $output = [];
        $whereStatement = '';
        $q = $this->getState()->getReadDb()->createQueryBuilder();
        foreach ($input as $index => $item) {
            if ($index !== 0) {
                $whereStatement .= ' and ';
            }
            $email = $item['email'] ?? null;
            $mobile = $item['mobile'] ?? null;
            $whereSubStatement = '';
            if (!empty($email)) {
                $er1 = uniqid('bd_');
                $whereSubStatement = "u.email = :{$er1}";
                $q->setParameter($er1, $email);
            }
            if (!empty($mobile)) {
                if (!empty($whereSubStatement)) {
                    $whereSubStatement .= ' and ';
                }
                $mr1 = uniqid('bd_');
                $whereSubStatement .= "u.mobile = :{$mr1}";
                $q->setParameter($mr1, $mobile);
            }
            if (!empty($whereSubStatement)) {
                $whereStatement .= "({$whereSubStatement})";
            }
        }

        if (!empty($whereStatement)) {
            $q->select('u.email, u.mobile, s.email as _subbed_email, s.mobile as _subbed_mobile')
                ->from('sb__user', 'u')
                ->andWhere('u.lote_deleted is null')
                ->andWhere($whereStatement)
                ->leftJoin('u', $this->getTableName(), 's', 's.user_id = u.id and s.lote_deleted is null');

            $results = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
            foreach ($results as $result) {
                if (!empty($result['email']) && !empty($result['mobile'])) {
                    if ($result['_subbed_email'] == SubscriptionEntity::ENUM_EMAIL_PERMANENT_UNSUBSCRIBED &&
                        $result['_subbed_mobile'] == SubscriptionEntity::ENUM_MOBILE_PERMANENT_UNSUBSCRIBED
                    ) {
                        $output[] = [
                            'email' => $result['email'],
                            'mobile' => $result['mobile']
                        ];
                    }
                }
            }
        }
        unset($emailSet, $mobileSet, $whereStatement, $whereSubStatement, $q, $results, $result);
        return $output;
    }

}
