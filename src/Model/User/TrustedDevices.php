<?php
namespace BlueSky\Framework\Model\User;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Driver\PDOConnection;
use Doctrine\DBAL\Query\QueryBuilder;
use function MongoDB\BSON\toJSON;
use BlueSky\Framework\Entity\Audit\ObjectLog;
use BlueSky\Framework\Entity\User\TrustedDevices as TrustedDevicesEntity;
use BlueSky\Framework\Object\Model\CustomField;
use BlueSky\Framework\Service\Email;
use BlueSky\Framework\View\Transform\Html\Service;
use BlueSky\Framework\Util\Arrays;
use BlueSky\Framework\Util\Number\Mobile;
use BlueSky\Framework\Util\Strings;

/**
 * Model class for Users
 */
class TrustedDevices extends CustomField
{

    protected $tableName = 'sb__user_devices';

    /**
     * Check the device is trusted
     * @param int $userId- the user to check
     * @param string $token - the key for the device
     * @return bool
     */
    public function isTrustedDevice($userId, $token)
    {
        $result = false;
        $trustedDevices = new TrustedDevicesEntity($this->getState());
        if ($trustedDevices->loadByFields(['user_id' => $userId, 'token' => $token])) {
            $result = true;
        }
        return $result;
    }
}
