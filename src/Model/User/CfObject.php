<?php
declare(strict_types=1);

namespace BlueSky\Framework\Model\User;

use Doctrine\DBAL\Query\QueryBuilder;
use BlueSky\Framework\Object\Model\Base as BaseModel;

class CfObject extends BaseModel
{
    protected $tableName = 'sb__cf_object';

}
