<?php

namespace BlueSky\Framework\Model\User;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Driver\PDOConnection;
use Doctrine\DBAL\Query\QueryBuilder;
use BlueSky\Framework\Entity\Audit\ObjectLog;
use BlueSky\Framework\Entity\User as UserEntity;
use BlueSky\Framework\Object\Model\CustomField;
use BlueSky\Framework\Service\Email;
use BlueSky\Framework\View\Transform\Html\Service;
use BlueSky\Framework\Util\Arrays;
use BlueSky\Framework\Util\Number\Mobile;
use BlueSky\Framework\Util\Strings;
use BlueSky\Framework\Event\Data;

class Password extends CustomField
{
    protected $tableName = 'sb__user_password';

    /**
     * Get all of a users groups
     * @param string $password - the password to validate
     * @param int $userId
     * @return bool
     */
    public function isValidPassword($password, $userId, $email = '')
    {
        $result = false;
        if ($passwords = $this->getValidPasswords($userId)) {
            foreach ($passwords as $p) {
                if ($result = password_verify($password, $p['password'])) {
                    break;
                }
            }
        }
        if (!$result) {
            $eventData = new Data(['password' => $password, 'id' => $userId, 'email' => $email]);
            $this->getState()->getEventManager()->dispatch('login.no_valid_password', $eventData);
            $result = $eventData->getResult();
        }
        return $result;
    }

    public function getValidPasswords($userId)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('p.*')
            ->from('sb__user_password', 'p')
            ->where("p.user_id = :user_id")
            ->setParameter("user_id", $userId)
            ->andWhere("p.lote_deleted is null")
            ->andWhere("is_current = true");
        return $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
    }
}
