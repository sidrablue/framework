<?php
namespace BlueSky\Framework\Model;

use Doctrine\DBAL\Connection;
use BlueSky\Framework\Object\Model\Base;
use BlueSky\Framework\Util\Arrays;
use BlueSky\Framework\Entity\Url as UrlEntity;

/**
 * Class for management of URLs in the system
 * */
class Url extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__url';

    /**
     * Attempt to match a URL in the database
     * @access public
     * @param string $url
     * @param int $siteId - defaults to 0 to imply using the current site ID.
     * @return false|array
     * */
    public function match($url, $siteId = 0)
    {
        $url = ltrim($url, '/');
        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select('*')
            ->from($this->getTableName(), 'u')
            ->where('(u.uri = :uri or u.uri = :uri2)')
            ->andWhere('u.lote_deleted is null')
            ->setParameter('uri', $url)
            ->setParameter('uri2', '/'.$url);
        /*if ($siteId == 0) {
            $siteId = $this->getState()->getSites()->getCurrentSiteId();
        }*/
        if ($siteId) {
            $q->andWhere('(u.site_id = :site_id or u.site_id is null or u.site_id = 0)')
                ->setParameter('site_id', $siteId)
                ->addOrderBy("u.site_id", "desc");
        }
        $query = $data->execute();
        $result = $query->fetch(\PDO::FETCH_ASSOC);
        return $result;
    }

    /**
     * Check if a URL is taken, or if it is not assigned to the specified object ID and reference
     * @access public
     * @param $url
     * @param int $siteId
     * @param string $objectRef
     * @param int $objectId
     * @param bool $includeDeleted
     * @param bool $checkStatic
     * @return boolean
     */
    public function urlIsTaken($url, $siteId, $objectRef, $objectId, $includeDeleted=false, $checkStatic = true)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select('count(*)')
            ->from('sb__url', 'u')
            ->where('u.uri = :uri')
            ->setParameter('uri', $url)
            ->andWhere('(object_ref <> :object_ref or object_id <> :object_id)')
            ->setParameter('object_ref', $objectRef)
            ->setParameter('object_id', $objectId);
        if ($siteId) {
            $data->andWhere('u.site_id = :site_id')->setParameter('site_id', $siteId);
        }
        if (!$includeDeleted) {
            $q->andWhere('lote_deleted is null');
        }
        $result = $q->execute()->fetchColumn();
        $hasStatic = $checkStatic && $this->getState()->hasStaticMatch($url);

        $taken = ($hasStatic || $result > 0);
        return $taken;
    }

    /**
     * Get the urls for a list of results based on an object reference
     * @param array $data
     * @param string $objectRef
     * @return array
     * */
    public function addUrlsToData($data, $objectRef)
    {
        $ids = Arrays::getFieldValuesFrom2dArray($data, 'id', true);
        if(count($ids) > 0) {
            $q = $this->getReadDb()->createQueryBuilder();
            $q->select("u.uri, u.object_id")
                ->from("sb__url", 'u')
                ->where("object_id in (:object_id)")
                ->setParameter("object_id", $ids, Connection::PARAM_INT_ARRAY)
                ->andWhere("object_ref = :object_ref")
                ->setParameter("object_ref", $objectRef)
                ->andWhere("lote_deleted is null");
            $matches = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
            $matches = Arrays::indexArrayByField($matches, 'object_id');
            foreach($data as $k=>$v) {
                if(isset($matches[$v['id']])) {
                    $data[$k]['_url'] = $matches[$v['id']]['uri'];
                }
            }
        }
        return $data;
    }

    /**
     * Get the URL for an object
     * @param int $objectId
     * @param string $objectRef
     * @return string|false
     * */
    public function getUrl($objectId, $objectRef)
    {
        $result = false;
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("u.uri")
            ->from("sb__url", 'u')
            ->where("object_id = :object_id")
            ->setParameter("object_id", $objectId)
            ->andWhere("object_ref = :object_ref")
            ->setParameter("object_ref", $objectRef)
            ->andWhere("lote_deleted is null");
        $data = $q->execute()->fetch(\PDO::FETCH_ASSOC);
        if($data) {
            $result = $data['uri'];
        }
        return $result;
    }

    /**
     * Create or update a URL entity by object id and ref
     * Delete if updating with empty URI.
     * @param $uri
     * @param $object_id
     * @param $object_ref
     * @param $siteId
     */
    public function updateUriByObject($uri, $object_id, $object_ref, $siteId = 0)
    {

        $result = false;
        $e = new UrlEntity($this->getState());
        $e->loadByObject($object_id, $object_ref);
        // If URI not empty, Create or update; else don't add to DB - delete existing entry
        if (!empty(trim($uri))) {
            //Don't create url if it exists statically already
            if(!$this->getState()->hasStaticMatch($uri) || $uri == $this->getState()->getSettings()->get("event.url.calendar_prefix", "/calendar")) {
                $e->object_id = $object_id;
                $e->object_ref = $object_ref;
                $e->uri = $uri;
                $e->site_id = $siteId;
                $result = $e->save();
            }

        } elseif ($e->id) {
            $e->delete();
        }
        return $result;
    }

    /**
     * Create or update a URL entity by object id, site id and ref.
     *
     * @access public
     * @param $uri
     * @param $objectId
     * @param $objectRef
     * @param $siteIds
     * @return void
     */
    public function updateUri($uri, $objectId, $objectRef, $siteIds = [])
    {
        if (!empty(trim($uri))) {
            if ($siteIds) {
                $siteUrlIds = [];
                $siteUrls = $this->getSiteUri($objectId, $objectRef);
                if ($siteUrls) {
                    $siteUrlIds = array_column($siteUrls, 'site_id');
                }
                $deletedSiteIds = array_diff($siteUrlIds, $siteIds);
                if ($deletedSiteIds) {
                    foreach ($deletedSiteIds as $ds) {
                        $ue = new UrlEntity($this->getState());
                        if ($ue->loadByFields([
                            'object_ref' => $objectRef,
                            'object_id' => $objectId,
                            'site_id' => $ds
                        ])
                        ) {
                            $ue->delete(true);
                        }
                    }

                }
                foreach ($siteIds as $siteId) {
                    $ue = new UrlEntity($this->getState());
                    $fields = [
                        'object_ref' => $objectRef,
                        'object_id' => $objectId,
                        'site_id' => $siteId
                    ];
                    if ($ue->loadByFields($fields)) {
                        $fields['id'] = $ue->id;
                    }
                    $fields['uri'] = $uri;
                    $ue = new UrlEntity($this->getState());
                    $ue->setData($fields);
                    $ue->save();
                }

            } else {
                $removeUrls = $this->getSiteUri($objectId, $objectRef, "", true);
                if ($removeUrls) {
                    foreach ($removeUrls as $sl) {
                        $ue = new UrlEntity($this->getState());
                        $ue->load($sl['id']);
                        $ue->delete(true);
                    }
                }
                $updateUrls = $this->getSiteUri($objectId, $objectRef);
                $fields = ['object_ref' => $objectRef, 'object_id' => $objectId, 'uri' => $uri];
                if ($updateUrls) {
                    $fields['id'] = $updateUrls[0]['id'];
                }
                $ue = new UrlEntity($this->getState());
                $ue->setData($fields);
                $ue->save();
            }
        } else {
            $siteUrls = $this->getSiteUri($objectId, $objectRef);
            if ($siteUrls) {
                foreach ($siteUrls as $sl) {
                    $ue = new UrlEntity($this->getState());
                    $ue->load($sl['id']);
                    $ue->delete(true);
                }
            }
        }
    }

    /**
     * Function used to fetch site uri.
     *
     * @access public
     * @param $uri
     * @param $objectId
     * @param $objectRef
     * @return array
     */
    public function getSiteUri($objectId, $objectRef, $uri = "", $siteIsNotEmpty = false)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("u.*")
            ->from($this->getTableName(), 'u')
            ->where("u.object_id = :object_id")
            ->setParameter("object_id", $objectId)
            ->andWhere("u.object_ref = :object_ref")
            ->setParameter("object_ref", $objectRef);
        if ($uri) {
            $q->andWhere("u.uri = :uri")
                ->setParameter("uri", $uri);
        }
        if ($siteIsNotEmpty) {
            $q->andWhere("u.site_id>0");
        }
        $q->andWhere("u.lote_deleted is null");
        return $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * Function used to remove urls
     *
     * @access public
     * @param $action - deleteUrl/inactiveUrl
     * @param $objectId
     * @param $objectRef
     * @return void
     */
    public function removeUrls($objectRef, $objectId, $action = "deleteUrl")
    {
        if (method_exists($this, $action) && in_array($action, ['deleteUrl', 'inactivateUrl'])) {
            $urls = $this->getSiteUri($objectId, $objectRef);
            if ($urls) {
                foreach ($urls as $url) {
                    $this->$action($url['id']);
                }
            }
        }
    }


    /**
     * Function used to delete url
     *
     * @access public
     * @param $id - Url Id
     * @return void
     */
    public function deleteUrl($id)
    {
        $ue = new UrlEntity($this->getState());
        $ue->load($id);
        $ue->delete();
    }

    /**
     * Function used to inactivate url
     *
     * @access public
     * @param $id - Url Id
     * @return void
     */
    public function inactivateUrl($id)
    {
        $ue = new UrlEntity($this->getState());
        $ue->load($id);
        $ue->is_active = 0;
        $ue->save();
    }

    /**
     * Function used to remove urls by site
     *
     * @access public
     * @param $siteId - Site Id
     * @param $strongDelete
     * @return void
     */
    public function removeSiteUrls($siteId, $strongDelete = false)
    {
        $urls = $this->getSiteUrls($siteId);
        if ($urls) {
            foreach ($urls as $url) {
                $ue = new UrlEntity($this->getState());
                $ue->load($url['id']);
                $ue->delete($strongDelete);
            }
        }
    }

    /**
     * Function used to get all urls by site
     *
     * @access public
     * @param $siteId - Site Id
     * @return array
     */
    public function getSiteUrls($siteId)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("u.*")
            ->from($this->getTableName(), 'u')
            ->where("u.site_id = :site_id")
            ->setParameter("site_id", $siteId)
            ->andWhere("u.lote_deleted is null");
        return $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
    }

}
