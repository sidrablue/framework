<?php
namespace BlueSky\Framework\Model;

use Aws\Result;
use Aws\S3\Exception\S3Exception;
use Doctrine\DBAL\Connection;
use League\Flysystem\Adapter\Local;
use League\Flysystem\AwsS3v3\AwsS3Adapter;
use League\Flysystem\AzureBlobStorage\AzureBlobStorageAdapter;
use League\Flysystem\Filesystem;
use BlueSky\Framework\Entity\FileImage as FileImageEntity;
use BlueSky\Framework\Event\Data;
use BlueSky\Framework\Object\Model\Base;
use BlueSky\Framework\Entity\File as FileEntity;
use Lote\App\Media\Entity\File as MediaFileEntity;
use BlueSky\Framework\Service\Image;
use BlueSky\Framework\Util\Arrays;
use BlueSky\Framework\Util\Dir;
use BlueSky\Framework\Util\Time;
use BlueSky\Framework\Util\File as FileUtil;

class File extends Base
{

    /** @param string $tableName */
    protected $tableName = 'sb__file';

    /** @param array $tableName */
    protected $storeConfig = [];

    /** The max filesize before streaming is enforced */
    const DOWNLOAD_FULL_DOWNLOAD_LIMIT = 1024 * 1024 * 50;

    /** The size of each part to retrieve when streaming downloads */
    const DOWNLOAD_STREAM_CHUNK_SIZE = 1024 * 1024;

    protected function setConfig($key, $value): void
    {
        $this->storeConfig[$key] = $value;
    }

    protected function getConfig($key, $default = null)
    {
        $result = $default;
        if (array_key_exists($key, $this->storeConfig)) {
            $result = $this->storeConfig[$key];
        }
        return $result;
    }

    public function getLineCount($fileId): int
    {
        ini_set('auto_detect_line_endings', true);
        $result = 0;
        if ($content = $this->getContent($fileId)) {
            $file = new \SplTempFileObject();
            $file->fwrite($content);
            foreach ($file as $lineNumber => $content) {
                $result = ($lineNumber + 1);
            }
        }
        return (int)$result;
    }

    private function checkDownloadAccess(FileEntity $fileEntity): bool
    {
        $result = true;
        $functions = $this->getDownloadAccessFunctionArray();
        if (isset($functions[$fileEntity->object_ref])) {
            $f = $functions[$fileEntity->object_ref];
            $result = (bool)call_user_func($f, $fileEntity);
        }
        $data = ['file_id' => $fileEntity->id];
        $eventData = new Data($data);
        $this->getState()->getEventManager()->dispatch('file.download.has_access', $eventData);
        $eventDataResult = $eventData->getData();
        if (isset($eventDataResult['has_access']) && $eventData->getData()['has_access'] === false) {
            $result = false;
        }
        return $result;
    }

    /**
     * Get the functions for determining access to download a file
     * @access private
     * @return array
     * @todo - remove this file and have it as a dispatch only
     * */
    private function getDownloadAccessFunctionArray()
    {
        $newsFileFunction = function (FileEntity $f) {
            $result = false;
            $m = new \Lote\Module\News\Entity\Article($this->getState());
            if ($m->load($f->object_id)) {
                $result = $this->getState()->getAccess()->hasEntityAccess($m);
            }
            return $result;
        };
        $mediaFileFunction = function (FileEntity $f) {
            $result = false;
            $m = new MediaFileEntity($this->getState());
            if ($m->load($f->object_id)) {
                $result = $this->getState()->getAccess()->hasEntityAccess($m);
            }
            return $result;
        };
        $fns = [
            "news" => $newsFileFunction,
            "media" => $mediaFileFunction,
            "media.file" => $mediaFileFunction,
            "form" => function (FileEntity $f) {
                return $this->getState()->getAccess()->hasAccess('cms.form', 'edit');
            },
            "import" => function (FileEntity $f) {
                return $this->getState()->getAccess()->hasAccess('admin.import', 'view');
            },
        ];
        return $fns;
    }

    //@todo - fix the isEnabled
    private function checkFileExpiry(FileEntity $e)
    {
        if ($e->object_ref == 'media' || $e->object_ref == 'sb_media_file_item') {
            if (!$this->getState()->getUser()->is_admin || !$this->getState()->getAccess()->hasAccess("cms.media", 'view')) {
                if (!$this->getState()->getRequest()->get("filePreviewKey", false) || /*|| !$this->getState()->getDbCache()->isEnabled() ||*/ !$this->getState()->getDbCache()->get("key:sb__file:preview:" . $this->getState()->getRequest()->get("filePreviewKey"))) {
                    $m = new MediaFileEntity($this->getState());
                    if ($m->load($e->object_id)) {
                        if (method_exists($m, 'isVisible')) {
                            if (!$m->isVisible()) {
                                die('File is unavailable');
                            }
                        } else {
                            if (!$m->isCurrentAndPublished()) {
                                die('File is unavailable');
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Download a file
     * @param int $id - the ID of the file to download
     * @param string $disposition - "inline" to view in browser, "attachment" to force download
     * */
    public function download($id, $disposition = "attachment", $accessOverride = false)
    {
        $fe = new FileEntity($this->getState());
        $fe->load($id);
        $this->downloadFileByEntity($fe, $disposition, $accessOverride);
    }

    /**
     * Download a file
     * @param int $reference - the reference of the file to download
     * @param string $disposition - "inline" to view in browser, "attachment" to force download
     * */
    public function downloadByReference($reference, $disposition = "inline")
    {
        $e = new FileEntity($this->getState());
        if (!$e->loadByField('reference', $reference)) {
            $e->loadByField('location_reference', $reference);
        }
        $this->downloadFileByEntity($e, $disposition);
    }

    private function downloadFileByEntity(FileEntity $fileEntity, $disposition, $accessOverride = false)
    {
        if ($fileEntity->id > 0 && ($accessOverride || $this->checkDownloadAccess($fileEntity))) {
            $this->checkFileExpiry($fileEntity);
            $fs = $this->getFileSystem('', $fileEntity->is_public);
            $size = $fs->getSize($fileEntity->location_reference);
            if ($size > self::DOWNLOAD_FULL_DOWNLOAD_LIMIT) {
                $this->streamDownload($fileEntity, $fs, $size);
            } else {
                $this->fullDownload($fileEntity, $fs, $size, $disposition);
            }
        }
    }

    private function fullDownload(FileEntity $fileEntity, Filesystem $fs, $fileSize, $disposition)
    {
        if($contents = $fs->read($fileEntity->location_reference)) {
            $this->sendDownloadHeaders($fileEntity, $fs, $fileSize, $disposition);
            if ($range = $this->getRange()) {
                $this->downloadRange($contents, $range);
            } else {
                echo($contents);
                header('Connection: close');
            }
        }
        die;
    }

    private function getRange(): ?string
    {
        $result = null;
        if ($this->getState()->getRequest()->headers->has("range")) {
            $range = $this->getState()->getRequest()->headers->get("range", false);
            if (strpos($range, '=') !== false) {
                $range = substr($range, strpos($range, '=')+1);
            }
            $result = $range;
        }
        return $result;
    }

    private function downloadRange(string $content, string $range): void
    {
        $fp = fopen('php://memory', 'r+');
        fwrite($fp, $content);
        rewind($fp);
        $size = (int)strlen($content);
        header("Accept-Ranges: bytes");
        list(, $rangeOld) = explode('-', $range, 2);
        if (strpos($rangeOld, ',') !== false) {
            header('HTTP/1.1 416 Requested Range Not Satisfiable');
            header("Content-Range: bytes 0-" . ($size - 1) . "/$size");
            exit;
        }
        if ($range == '-' || $range == "0-") {
            $rangeStart = 0;
            $rangeEnd = (int)($size - 1);
        } else {
            $range = explode('-', $range);
            $rangeStart = (int)$range[0];
            $rangeEnd = (isset($range[1]) && is_numeric($range[1])) ? (int)$range[1] : (int)($size - 1);
        }
        if ($rangeStart >= $rangeEnd || $rangeEnd >= $size) {
            header('HTTP/1.1 416 Requested Range Not Satisfiable');
            header("Content-Range: bytes 0-" . ($size - 1) . "/$size");
            exit;
        }

        $length = $rangeEnd - $rangeStart + 1;
        fseek($fp, $rangeStart);
        header('HTTP/1.1 206 Partial Content');
        header("Content-Range: bytes $rangeStart-$rangeEnd/$size");
        header("Content-Length: $length");

        $buffer = 1024 * 8;
        while (!feof($fp) && ($p = ftell($fp)) <= $rangeEnd) {
            if ($p + $buffer > $rangeEnd)
                $buffer = $rangeEnd - $p + 1;
            set_time_limit(0);
            echo fread($fp, $buffer);
            flush();
        }
        fclose($fp);
    }

    private function sendDownloadHeaders(FileEntity $fileEntity, FileSystem $fs, $fileSize, $disposition)
    {
        header('Pragma: public');    // required
        header('Expires: 0');        // no cache
        header('Cache-Control: private', false);
        if ($fileEntity->mime_type) {
            header('Content-Type: ' . $fileEntity->mime_type);
        } else {
            header('Content-Type: ' . $fs->getMimetype($fileEntity->location_reference));
        }
        header('Content-Disposition: ' . $disposition . '; filename="' . $fileEntity->title . '"');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: ' . $fileSize);
    }

    private function streamDownload(FileEntity $fileEntity, Filesystem $fs, $fileSize)
    {
        ini_set("max_execution_time", 6000);
        $adapter = $fs->getAdapter();
        if ($adapter instanceof AwsS3Adapter) {
            $partsCount = ceil($fileSize / self::DOWNLOAD_STREAM_CHUNK_SIZE);
            $this->sendDownloadHeaders($fileEntity, $fs, $fileSize, 'attachment');
            for ($i = 0; $i < $partsCount; $i++) {
                echo($this->readAwsFilePart($adapter, $fileEntity, $fileSize, $i + 1));
            }
            header('Connection: close');
        }
        die;
    }

    private function readAwsFilePart(AwsS3Adapter $adapter, FileEntity $fe, $fileSize, $partCount)
    {
        $rangeFrom = $partCount * self::DOWNLOAD_STREAM_CHUNK_SIZE - self::DOWNLOAD_STREAM_CHUNK_SIZE;
        $rangeTo = min($rangeFrom + self::DOWNLOAD_STREAM_CHUNK_SIZE, $fileSize);
        if ($partCount != 1) {
            $rangeFrom++;
        }
        $options = [
            'Bucket' => $adapter->getBucket(),
            'Key' => rtrim($adapter->getPathPrefix(), '/') . '/' . ltrim($fe->location_reference, '/'),
            'Range' => "bytes={$rangeFrom}-{$rangeTo}"
        ];
        $command = $adapter->getClient()->getCommand('getObject', $options);
        try {
            /** @var Result $response */
            $response = $adapter->getClient()->execute($command);
        } catch (S3Exception $e) {
            return false;
        }
        /** @var \GuzzleHttp\Psr7\Stream $stream */
        $stream = $response['Body'];
        return $stream->getContents();
    }


    /**
     * Get the content of a file
     * @param int $fileId - the ID of the file to get
     * @access public
     * @return string
     */
    public function getContent($fileId)
    {
        $result = "";
        $e = new FileEntity($this->getState());
        if ($e->load($fileId)) {
            $result = $this->getContentByEntity($e);
        }
        $e = null;
        return $result;
    }

    /**
     * Get the content of a file
     * @param int $fileReference - the reference of the file to get
     * @access public
     * @return string
     */
    public function getContentByReference($fileReference)
    {
        $e = new FileEntity($this->getState());
        $e->loadByReference($fileReference);
        return $this->getContentByEntity($e);
    }

    public function getContentByEntity(FileEntity $e)
    {
        $result = '';
        if ($e->id) {
            $fs = $this->getFileSystem($e->store, $e->is_public);
            if ($fs->has($e->location_reference)) {
                $result = $fs->read($e->location_reference);
            }
        }
        $fs = null;
        return $result;
    }

    public function getContentStreamByEntity(FileEntity $e)
    {
        $result = '';
        if ($e->id) {
            $fs = $this->getFileSystem($e->store, $e->is_public);
            if ($fs->has($e->location_reference)) {
                $result = $fs->readStream($e->location_reference);
            }
        }
        $fs = null;
        return $result;
    }

    /**
     * Get the content of a file
     * @param int $fileId - the ID of the file to get
     * @param string $content
     * @return boolean
     * @throws \League\Flysystem\FileExistsException
     * @throws \League\Flysystem\FileNotFoundException
     * @access public
     */
    public function setContent($fileId, $content)
    {
        $result = false;
        $e = new FileEntity($this->getState());
        $e->load($fileId);
        $fs = $this->getFileSystem($e->store, $e->is_public);
        if ($fs->has($e->location_reference)) {
            if ($fs->has($e->location_reference)) {
                $fs->delete($e->location_reference);
            }
            $result = $fs->write($e->location_reference, $content);
        }
        if ($e->id) {
            $this->removeFromCache($e->location_reference);
        }
        return $result;
    }

    /**
     * Get an array list of the data for one or more files by object id
     * @param string $objectRef - the reference of the object
     * @param int $objectId - the ID of the object
     * @param string $objectKey - the optional key of the file
     * @param string $subPath - the subpath of the file
     * @return array|false
     * */
    public function getListByObjectId($objectRef, $objectId, $objectKey = '', $subPath = '')
    {
        $result = false;
        $subPath = $this->cleanSubPath($subPath);
        if ($objectId > 0) {
            $q = $this->getReadDb()->createQueryBuilder();
            $q->select('o.*')
                ->from($this->getTableName(), 'o')
                ->where('o.object_ref = :ref')
                ->setParameter('ref', $objectRef)
                ->andWhere('o.object_id = :id')
                ->setParameter('id', $objectId);
            $q->andWhere("o.lote_deleted is null");
            if ($objectKey) {
                $q->andWhere("o.object_key = :key")
                    ->setParameter('key', $objectKey);
            }
            if ($subPath) {
                $q->andWhere("o.sub_path = :sub_path")
                    ->setParameter('sub_path', $subPath);
            }

            $q->orderBy('o.lote_created', 'desc');

            $s = $q->execute();
            $result = $s->fetchAll(\PDO::FETCH_ASSOC);
        }
        return $result;
    }

    public function deleteEntity(FileEntity $e, $strongDelete = false)
    {
        $this->removeFromCache($e->location_reference);
        $this->archiveFileContent($e);
        $e->delete();
        parent::delete($e->id, $strongDelete);
    }

    /**
     * On upload of a new file or of a delete of an existing file, remove any cached entries for it
     * @access public
     * @param string $reference - the reference of the file
     * return void
     * */
    public function removeFromCache($reference)
    {
        $this->getState()->getFileCache()->deleteDir(dirname($reference));
    }

    public function getFile($object, $objectId, $objectKey = '', $subPath = '', $fileName = ''): ?FileEntity
    {
        $result = null;
        $subPath = $this->cleanSubPath($subPath);
        $e = new FileEntity($this->getState());
        $e->loadByObjectParams($object, $objectId, $objectKey, $fileName, $subPath);
        if ($e->id) {
            $result = $e;
        }
        return $result;
    }

    private function archiveFileContent(FileEntity $item)
    {
        if ($item) {
            if ($destinationFileSystem = $this->getArchiveFilesystem()) {
                $existingFileSystem = $this->getFileSystem($item->store);
                if ($existingFileSystem->has($item->location_reference)) {
                    $archivePath = $item->makeArchiveReference();
                    $destinationFileSystem->put($archivePath, $existingFileSystem->read($item->location_reference));
                    $this->getState()->getAudit()->addFileLog($this->getStore(), $item->location_reference,
                        $archivePath, $this->getTableName(), $item->id, "");
                }
            }
        }
    }

    private function getArchiveFilesystem()
    {
        $result = false;
        if ($this->getState()->getSettings()->get("media.archive.is_enabled", true)) {
            $result = $this->getFileSystem($this->getStore());
        }
        return $result;
    }

    private function deleteExistingContent($item)
    {
        if ($item->id) {
            $existingFileSystem = $this->getFileSystem($item->store);
            if ($existingFileSystem->has($item->location_reference)) {
                $existingFileSystem->delete($item->location_reference);
            }
        }
    }

    private function cleanName($name, $cleanName = true)
    {
        $result = $name;
        if ($cleanName) {
            $name = trim(preg_replace("/[^a-zA-Z0-9\.\_]/", "_", $name));
            $result = preg_replace('!_+!', '_', $name);
        }
        return $result;
    }

    /**
     * Save a file, but ignore the new file if one already exists for the specified parameters
     *
     * @param $tempPath - the temporary path of the file
     * @param $filename - the filename
     * @param $object - the object that the file belongs to
     * @param $objectId - the object id that the file belongs to
     * @param string $objectKey - an optional key
     * @param string $subPath
     * @param string $store - a preferred store for the file
     * @return bool|FileEntity
     */
    public function saveIgnore($tempPath, $filename, $object, $objectId, $objectKey = '', $subPath = '', $store = '')
    {
        $subPath = $this->cleanSubPath($subPath);
        if ($item = $this->getFile($object, $objectId, $objectKey, $subPath, $filename)) {
            $result = $item;
        } else {
            $result = $this->save($tempPath, $filename, $object, $objectId, $objectKey, $subPath, $store);
        }
        return $result;
    }

    /**
     * Save a file, and if it exists archive the previous one and create a new entry for this one
     *
     * @param $tempPath - the temporary path of the file
     * @param $filename - the filename
     * @param $object - the object that the file belongs to
     * @param $objectId - the object id that the file belongs to
     * @param string $objectKey - an optional key
     * @param string $subPath - an optional sub path
     * @param string $store - a preferred store for the file
     * @return bool|FileEntity
     */
    public function saveUpdate($tempPath, $filename, $object, $objectId, $objectKey = '', $subPath = '', $store = '')
    {
        $subPath = $this->cleanSubPath($subPath);
        if ($item = $this->getFile($object, $objectId, $objectKey, $subPath, $filename)) {
            $currentPath = $item->location_reference;
            $item->archive();
            $fs = $this->getFileSystem($item->store);
            $fs->rename($currentPath, $item->location_reference);
        } else {
            $reference = $this->makeReference($object, $objectId, $objectKey, $subPath, $filename);
            $fs = $this->getFileSystem();
            if ($fs->has($reference)) {
                $e = new FileEntity($this->getState());
                $fs->rename($reference, $e->makeArchiveReference());
            }
        }
        return $this->save($tempPath, $filename, false, $object, $objectId, $objectKey, $subPath, $store);
    }

    /**
     * Save a file, and if it exists archive the previous one and create a new entry for this one
     *
     * @param $id - the ID of the path
     * @param $tempPath - the temporary path of the file
     * @param $filename - the filename
     * @param $object - the object that the file belongs to
     * @param $objectId - the object id that the file belongs to
     * @param string $objectKey - an optional key
     * @param string $subPath - an optional sub path
     * @param string $store - a preferred store for the file
     * @return bool|FileEntity
     */
    public function saveUpdateById($id, $tempPath, $filename, $object, $objectId, $objectKey = '', $subPath = '', $store = '')
    {
        $e = new FileEntity($this->getState());
        if ($e->load($id)) {
            $reference = $e->reference;
            $e->archive();
            $fs = $this->getFileSystem($e->store);
            $fs->rename($reference, $e->reference);
        } else {
            $reference = $this->makeReference($object, $objectId, $objectKey, $subPath, $filename);
            $fs = $this->getFileSystem();
            if ($fs->has($reference)) {
                $e = new FileEntity($this->getState());
                $fs->rename($reference, $e->makeArchiveReference());
            }
        }
        $result = $this->save($tempPath, $filename, $object, $objectId, $objectKey, $subPath, $store, $reference);
        return $result;
    }

    /**
     * Save a file
     *
     * @param string $newPath - the temporary path of the new file to save
     * @param string $fileName - the name of the new file
     * @param bool $isSingle - true if this file is intended to be a single file for the given object parameters, so
     *                         override any file that exists for the combination instead of creating an additional entry
     * @param string $object - the reference of the object that the file is for
     * @param int $objectId - the ID of the object in question
     * @param string $objectKey - the key for the object, if its for a specific field or part of an array
     * @param string $subPath - the sub path of the file
     * @param bool $saveByStream - True is file should be saved by file stream, false if file should be saved by content string
     * @param bool $cleanName - If true the file name will be cleaned, if false it will remain as is
     * @access public
     * @return FileEntity|false
     * */
    public function save(
        $newPath,
        $fileName,
        $isSingle,
        $object,
        $objectId,
        $objectKey = '',
        $subPath = '',
        $saveByStream = false,
        $cleanName = true

    ) {
        $fileSystemName = $this->cleanName($fileName, $cleanName);
        $enableRotate = $this->getState()->getSettings()->getSettingOrConfig("rotate.images", false);
        if ($enableRotate) {
            $this->prepareFile($newPath);
        }
        $subPath = $this->cleanSubPath($subPath);
        $itemByName = $item = $this->getFile($object, $objectId, $objectKey, $subPath, $fileName);
        $item ? null : $item = $this->getFile($object, $objectId, $objectKey, $subPath, '');

        if ($isSingle && ($item || $itemByName)) {
            $itemByName ? $tempItem = $itemByName : $tempItem = $item;
            $result = $this->saveByEntity($tempItem, $newPath, $fileName, $fileSystemName, true, $saveByStream);
        } elseif ($itemByName) {
            $result = $this->saveByEntity($itemByName, $newPath, $fileName, $fileSystemName, true, $saveByStream);
        } else {
            $itemByObjectRef = $this->getFile($object, '', $objectKey, $subPath, $fileName);
            if ($isSingle && $itemByObjectRef) {
                $result = $this->saveByEntity($itemByObjectRef, $newPath, $fileName, $fileSystemName, true, $saveByStream);
            } else {
                $result = $this->saveByParams($newPath, $fileName, $fileSystemName, $object, $objectId, $objectKey, $subPath, $saveByStream);
            }
        }
        return $result;
    }

    protected function cleanSubPath($subPath)
    {
        $result = $subPath;
        if ($subPath == '\\') {
            $result = '';
        }
        return strtolower($result);
    }

    /**
     * Save a file to a store
     *
     * @access public
     * @param FileEntity $e - the file entity in question
     * @param string $newPath - the temporary path of the new file to save
     * @param string $fileName - the name of the new file used for display purposes
     * @param string $fileSystemName - the name of the new file as stored in the file system
     * @param bool $isPublic - true if this file is private
     * @param bool $saveByStream - True is file should be saved by file stream, false if file should be saved by content string
     * @param bool $cleanName - If true the file name will be cleaned, if false it will remain as is
     * @access public
     * @return FileEntity
     * */
    public function saveByEntity(
        FileEntity $e,
        $newPath,
        $fileName,
        $fileSystemName,
        $isPublic = false,
        $saveByStream = false,
        $cleanName = true
    ) {
        $newName = $this->cleanName($fileSystemName, $cleanName);

        $this->archiveFileContent($e);
        $this->deleteExistingContent($e);

        $location_reference = $this->makeReferenceByEntity($e, $newName);
        if ($size = $this->saveToFileSystem($location_reference, $newPath, $saveByStream)) {
            $this->removeFromCache($e->location_reference);
            $mimeType = FileUtil::getMimeType($newPath);
            $extension = strtolower(pathinfo($newName, PATHINFO_EXTENSION));
            
            /**
             * @see https://bugs.php.net/bug.php?id=53035
             * finfo returns an incorect file type for css files
             * this sets the correct file type
             */
            if($extension === 'css'){
                $mimeType = 'text/css';
            }
            
            $e->location_reference = $location_reference;
            $e->title = $fileName;
            $e->filename = $newName;
            $e->extension = $extension;
            $e->mime_type = $mimeType;
            $e->size = $size;
            $e->file_type = FileUtil::getFileCategory($mimeType);
            $e->is_public = boolval($isPublic);
            if ($e->save()) {
                $this->updateImageSize($e, $newPath);
            }
        }
        return $e;
    }

    /**
     * Save a file to a store by its file ID
     *
     * @access public
     * @param int $fileId - the ID of the file in question
     * @param string $newPath - the temporary path of the new file to save
     * @param string $newName - the name of the new file
     * @param bool $isPublic - true if this file is public
     * @param bool $cleanName - If true the file name will be cleaned, if false it will remain as is
     * @access public
     * @return FileEntity|false
     * */
    public function saveById($fileId, $newPath, $newName, $isPublic, $cleanName = true)
    {
        $newName = $this->cleanName($newName, $cleanName);
        $result = false;
        $f = new FileEntity($this->getState());
        if ($f->load($fileId)) {
            $result = $this->saveByEntity($f, $newPath, $newName, $isPublic, false, false);
        }
        return $result;
    }

    /**
     * Save a file with a given set of parameters
     *
     * @param string $newPath - the temporary path of the new file to save
     * @param string $fileName - the name of the new file used for display purposes
     * @param string $fileSystemName - the name of the new file as stored in the file system
     * @param string $object - the reference of the object that the file is for
     * @param int $objectId - the ID of the object in question
     * @param string $objectKey - the key for the object, if its for a specific field or part of an array
     * @param string $subPath - the sub path of the file
     * @param bool $saveByStream - True is file should be saved by file stream, false if file should be saved by content string
     * @access public
     * @return FileEntity|false
     * */
    private function saveByParams(
        $newPath,
        $fileName,
        $fileSystemName,
        $object,
        $objectId,
        $objectKey = '',
        $subPath = '',
        $saveByStream = false
    ) {
        $result = false;
        $item = new FileEntity($this->getState());
        $item->reference = $this->makeReference($object, $objectId, $objectKey, $subPath, $fileSystemName);
        $item->location_reference = $this->makeReference($object, $objectId, $objectKey, $subPath, $fileSystemName, true);
        $item->store = $this->getStore();
        $size = $this->saveToFileSystem($item->location_reference, $newPath, $item->store, $saveByStream);
        if ($size > 0) {
            $mimeType = FileUtil::getMimeType($newPath);
            $item->object_ref = $object;
            $item->object_id = $objectId;
            $item->object_key = $objectKey;
            $item->sub_path = $this->cleanSubPath($subPath); //@todo - Move this function into the entity maybe?
            $item->title = $fileName;
            $item->filename = $fileSystemName;
            $item->extension = strtolower(pathinfo($fileSystemName, PATHINFO_EXTENSION));
            $item->mime_type = $mimeType;
            $item->is_public = true;
            $item->is_saved = true;
            $item->size = $size;
            $item->file_type = FileUtil::getFileCategory($mimeType);
            if ($item->save()) {
                $this->updateImageSize($item, $newPath);
                $result = $item;
            }
        }
        return $result;
    }

    public function savePlaceholderFile($newName, $isPublic, $object, $objectId, $objectKey = '', $subPath = '')
    {
        $newName = $this->cleanName($newName);

        $result = false;
        $item = new FileEntity($this->getState());
        $item->reference = $this->makeReference($object, $objectId, $objectKey, $subPath, $newName);
        $item->location_reference = $this->makeReference($object, $objectId, $objectKey, $subPath, $newName, true);
        $item->store = $this->getStore();
        $size = 0;

        $item->object_ref = $object;
        $item->object_id = $objectId;
        $item->object_key = $objectKey;
        $item->sub_path = $this->cleanSubPath($subPath); //@todo - Move this function into the entity maybe?
        $item->filename = $newName;
        $item->is_public = $isPublic;
        $item->extension = strtolower(pathinfo($newName, PATHINFO_EXTENSION));
        $item->size = $size;
        $item->is_saved = false;
        if ($item->save()) {
            $result = $item;
        }
        return $result;
    }

    private function updateImageSize(FileEntity $item, $tempPath)
    {
        $fileImageEntity = new FileImageEntity($this->getState());
        $fileImageEntity->loadByField("file_id", $item->id);
        if ($item->file_type == "image") {
            if ($size = Image::getImageDimensionsByContent(file_get_contents($tempPath))) {
                $fileImageEntity->file_id = $item->id;
                $fileImageEntity->width = $size && isset($size['width']) ? $size['width'] : 0;
                $fileImageEntity->height = $size && isset($size['height']) ? $size['height'] : 0;
                $fileImageEntity->save();
            } elseif ($fileImageEntity->id) {
                $fileImageEntity->delete();
            }
        } elseif ($fileImageEntity->id) {
            $fileImageEntity->delete();
        }
    }

    public function initImageSize(FileEntity $file)
    {
        $result = false;
        $fileImageEntity = new FileImageEntity($this->getState());
        $fileImageEntity->loadByField("file_id", $file->id);
        if ($file->file_type == "image") {
            if ($content = $this->getContentByEntity($file)) {
                if ($size = Image::getImageDimensionsByContent($content)) {
                    $fileImageEntity->file_id = $file->id;
                    $fileImageEntity->width = $size && isset($size['width']) ? $size['width'] : 0;
                    $fileImageEntity->height = $size && isset($size['height']) ? $size['height'] : 0;
                    $fileImageEntity->lote_deleted = null;
                    if ($fileImageEntity->save()) {
                        $result = $fileImageEntity;
                    }
                }
            }
        } elseif ($fileImageEntity->id) {
            $fileImageEntity->delete();
        }
        return $result;
    }

    /**
     * Save a file, and if it exists archive the previous one and create a new entry for this one
     *
     * @param $content - the content of the file
     * @param $filename - the filename
     * @param bool $isSingle - true if this file is intended to be a single file for the given object parameters, so
     *                         override any file that exists for the combination instead of creating an additional entry
     * @param $object - the object that the file belongs to
     * @param $objectId - the object id that the file belongs to
     * @param string $objectKey - an optional key
     * @param string $subPath - an optional sub path
     * @param string $store - a preferred store for the file
     * @return bool|FileEntity
     */
    public function saveByContent(
        $content,
        $filename,
        $isSingle,
        $object,
        $objectId,
        $objectKey = '',
        $subPath = '',
        $store = ''
    ) {
        $tempPath = LOTE_TEMP_PATH . 'file_model_temp_' . uniqid();
        Dir::make(LOTE_TEMP_PATH);
        file_put_contents($tempPath, $content);
        $file = $this->save($tempPath, $filename, $isSingle, $object, $objectId, $objectKey, $subPath,
            $store);
        if (file_exists($tempPath)) {
            unlink($tempPath);
        }
        return $file;
    }

    /**
     * Prepare the file for saving, by checking if its an image and then rotating the image if required
     * @access private
     * @param string $path - the path of the image
     * @return void
     * */
    protected function prepareFile($path)
    {
        if (@exif_imagetype($path)) { //Check if file is an image
            $exif = @exif_read_data($path);
            if ($exif && isset($exif['Orientation']) && $exif['Orientation'] != '1') {
                $this->rotateImage($path, $exif['Orientation']);
            }
        }
    }

    /**
     * Rotate an image based off of its exif data rotation value
     *
     * @access private
     * @param string $path - the current path of the image
     * @param int|string $rotationValue
     * @return void
     * */
    protected function rotateImage($path, $rotationValue)
    {
        $img = imagecreatefromjpeg($path);

        switch ($rotationValue) {
            case 3:
                $img = imagerotate($img, 180, 0);
                break;
            case 6:
                $img = imagerotate($img, -90, 0);
                break;
            case 8:
                $img = imagerotate($img, 90, 0);
                break;
        }

        switch (exif_imagetype($path)) {
            case 1:
                imagegif($img, $path);
                break;
            case 2:
                imagejpeg($img, $path);
                break;
            case 3:
                imagepng($img, $path);
                break;
            case 6:
                if (function_exists('imagebmp')) {
                    imagebmp($img, $path);
                } else {
                    imagejpeg($img, $path);
                }
                break;
            default:
                imagejpeg($img, $path);
                break;
        }

        imagedestroy($img);
    }

    /**
     * Save a file to a store
     * @param FileEntity $fileEntity - the temporary path of the file to save
     * @param string $path - the temp path of the file
     * @param string $name - the name of the file
     * @param string $store - the reference of the store to save the file in, if one is explicitly required
     * @access public
     * @return boolean|FileEntity
     * */
    protected function update($fileEntity, $path, $name, $store)
    {
        $result = false;
        $size = $this->saveToFileSystem($fileEntity->reference, $path, $store);
        if ($size > 0) {
            $fileEntity->store = $this->getStore();
            $fileEntity->filename = $name;
            $fileEntity->extension = strtolower(pathinfo($name, PATHINFO_EXTENSION));
            $fileEntity->mime_type = FileUtil::getMimeType($path);
            $fileEntity->size = $size;
            if ($fileEntity->save()) {
                $result = $fileEntity;
                $this->removeFromCache($fileEntity->reference);
            }
        }
        return $result;
    }

    /**
     * Get the store type for this system
     * @access private
     * @return string
     * */
    protected function getStore()
    {
        return $this->getState()->getSettings()->get('media.adapter', 'local');
    }

    /**
     * Make a reference for the file, to use in saving the file.
     * @param string $object - the reference of the object that this file is for
     * @param int $objectId - the ID of the object that the file is for
     * @param string $objectKey - the key of the object ID if one is required, such as a field name
     * @param string $subPath - sub path of the file
     * @param string $name - the name of the file if it is known
     * @param bool $useTime - true to use the current time in the reference
     * @access private
     * @return string;
     * @todo - add in validation for the object and object ID
     * */
    protected function makeReference($object, $objectId, $objectKey = '', $subPath = '', $name = '', $useTime = false)
    {
        $subPath = $this->cleanSubPath($subPath);
        $object = str_replace('.', '/', $object);
        $reference = "{$object}/{$objectId}";
        if ($objectKey) {
            $reference .= '/' . $objectKey;
        }
        if ($subPath != "" && $subPath != "/" && $subPath != "\\") {
            $reference .= '/' . ltrim($subPath, "/");
        }
        if ($useTime) {
            $reference .= '/' . Time::getUtcNow("ymdhis") . substr((string)microtime(), 2, 3);
        }
        if ($name) {
            $reference .= '/' . $name;
        }
        return strtolower($reference);
    }

    private function makeReferenceByEntity(FileEntity $e, $name)
    {
        return $this->makeReference($e->object_ref, $e->object_id, $e->object_key, $e->sub_path, $name, true);
    }

    /**
     * Save a file to the filesystem in the store specified or in the default store as defined in the settings
     * @param string $reference - the unique reference of the file
     * @param string $tempPath - the current temporary path of the file to save
     * @param string $store - the reference of the store to use, if an explicit one is to be used
     * @access private
     * @return int;
     * */
    protected function saveToFileSystem($reference, $tempPath, $store = false, $saveByStream = false)
    {
        $result = 0;
        $store ? null : $store = $this->getStore();
        $fs = $this->getFileSystem($store);
        if (file_exists($tempPath)) {
            if ($saveByStream) {
                $fileStream = fopen($tempPath, 'r');
                if ($fs->writeStream($reference, $fileStream)) {
                    $result = 1;
                    $fileStats = fstat($fileStream);
                    if ($fileStats && isset($fileStats['size'])) {
                        $result = max($result, fstat($fileStream)['size']);
                    }
                }
            } else {
                $fileData = file_get_contents($tempPath);
                if ($fs->write($reference, $fileData) > 0) {
                    $result = max(1, strlen($fileData));
                }
            }
        }
        return $result;
    }

    /**
     * Get the file location to store local files in
     * @access private
     * @param string $store
     * @return Filesystem
     * @todo - create a store factory
     * */
    protected function getFileSystem($store = '')
    {
        if ($store == '') {
            $store = $this->getStore();
        }
        switch ($store){
            case 'aws':{
                $client = $this->getState()->getS3Client();
                $adapter = new AwsS3Adapter($client, $this->getBucketName('aws'), $this->getBucketPrefix());
                break;
            }
            case 'azure': {
                $client = $this->getState()->getAzureClient();
                $adapter = new AzureBlobStorageAdapter($client, $this->getBucketName('azure'));
                break;
            }
            default :{
                $localPath = $this->getLocalPath();
                Dir::make($localPath);
                $adapter = new Local($localPath);
            }
        }
        $fs = new Filesystem($adapter);
        return $fs;
    }

    /**
     * Get the name of the bucket to use
     * @param string $client
     * @access private
     * @return string
     * */
    private function getBucketName($client = 'aws')
    {
        if (isset($this->storeConfig['bucket_name'])) {
            $result = $this->storeConfig['bucket_name'];
        } else {
            switch ($client){
                case 'aws':{
                    $result = $this->getState()->getSettings()->get('media.adapter.aws_bucket');
                    break;
                }
                case 'azure':{
                    $result =$this->getState()->getSettings()->get('media.adapter.azure_container_name');
                    break;
                }
            }

        }
        return $result;
    }

    /**
     * Get the prefix of the bucket to use
     * @param string $client
     * @access private
     * @return string
     * */
    private function getBucketPrefix($client = 'aws')
    {
        if (isset($this->storeConfig['bucket_name'])) {
            $result = $this->storeConfig['bucket_name'];
        } else {
            $result = '';
            switch ($client){
                case 'aws':{
                    $result = $this->getState()->getSettings()->get('media.adapter.aws_prefix');
                    break;
                }
                case 'azure':{
                    $result =$this->getState()->getSettings()->get('media.adapter.azure_prefix');
                    break;
                }
            }
        }
        return $result;
    }

    /**
     * Get the file location to store local files in
     * @access private
     * @return string
     * @todo - move this to the factory when it is created
     * */ 
    protected function getLocalPath()
    {
        if ($this->getState()->masterOnly) {
            $result = LOTE_ASSET_PATH . 'media/master';
        } else {
            $result = LOTE_ASSET_PATH . 'media/' . $this->getState()->accountReference;
        }
        return $result;
    }

    /**
     * Get a list of files by their IDs
     * @access public
     * @param array $fileIds - the ID's of the files that we want the data for
     * @param bool $indexById - true if the result is to be indexed by the file ID
     * @return array
     *  */
    public function getFilesById($fileIds, $indexById = false)
    {
        $result = [];
        if (is_array($fileIds) && count($fileIds) > 0) {
            $q = $this->getReadDb()->createQueryBuilder();
            $q->select('f.*')
                ->from('sb__file', 'f')
                ->where('f.id in (:file_ids)')
                ->setParameter('file_ids', $fileIds, Connection::PARAM_INT_ARRAY);
            $files = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
            foreach ($files as $f) {
                if ($indexById) {
                    $result[$f['id']] = $f;
                } else {
                    $result[] = $f;
                }
            }
        }
        return $result;
    }

    /**
     * Add information to an array of data about a file that it points to via a file_id field
     * @access public
     * @param array $data
     * @param string $dataFieldName - the field name that the file information should be appended to
     * @return array
     * */
    public function addFileInfo($data, $dataFieldName = '_file', $fileFieldName = 'file_id')
    {
        if (is_array($data) && count($data) > 0) {
            $ids = Arrays::getFieldValuesFrom2dArray($data, $fileFieldName, true);
            if (is_array($ids) && count($ids) > 0) {
                $fileList = $this->getFilesById($ids, true);
                $cnt = count($data);
                for ($i = 0; $i < $cnt; $i++) {
                    if (isset($fileList[$data[$i][$fileFieldName]])) {
                        $data[$i][$dataFieldName] = $fileList[$data[$i][$fileFieldName]];
                    }
                }
            }
        }
        return $data;
    }

    public function getTagItems($tag, $paging = [])
    {
        $result = [];
        $q = $this->getReadDb()->createQueryBuilder();

        $q->select('f.*')
            ->from($this->tableName, 'f')
            ->leftJoin('f', 'sb__tag_usage', 'u', 'u.object_id = f.id')
            ->leftJoin('u', 'sb__tag', 't', 't.id = u.tag_id')
            ->andWhere('t.value = :tag')
            ->andWhere('u.object_ref = :reference')
            ->setParameter('tag', $tag)
            ->setParameter('reference', 'media.file');

        if (!empty($paging)) {
            $q->setFirstResult($paging['start'] - 1 >= 0 ? $paging['start'] - 1 : $paging['start']);
            $q->setMaxResults($paging['limit']);
        }
        $q->addOrderBy('lote_created', 'desc');

        $query = $q->execute();
        while ($row = $query->fetchAll(\PDO::FETCH_ASSOC)) {
            $result = $row;
        }
        return $result;
    }

    /**
     * Get all imaages
     *
     * @param int $maxResults
     * @return array
     */
    public function getAllImages($maxResults = 1000)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $s = $q->select('*')
            ->from($this->getTableName(), 't')
            ->where("t.file_type = 'image'")
            ->andWhere("t.lote_deleted is null")
            ->setMaxResults($maxResults)->execute();
        return $s->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * Archive a file by its ID
     * @param int $fileId - the ID of the file to archive
     * @return bool
     * */
    public function archiveById($fileId)
    {
        $result = false;
        $f = new FileEntity($this->getState());
        if ($f->load($fileId)) {
            $fs = $this->getFileSystem($f->store);
            if ($fs->has($f->reference)) {
                $originalReference = $f->reference;
                $f->archive();
                $fs->rename($originalReference, $f->reference);
                $this->removeFromCache($f->reference);
            }
            $f->delete();
            $result = true;
        }
        return $result;
    }

    public function cloneFile($id, $folderId = 0)
    {
        $fe = null;
        $pmfe = new MediaFileEntity($this->getState()->getParentState());
        if ($pmfe->load($id)) {
            // Get parent's File Entry
            $mediaData = $pmfe->getViewData();
            $pfm = new File($this->getState()->getParentState());
            $fileData = $pfm->get($mediaData['file_id']);
            $fileContent = $pfm->getContent($mediaData['file_id']);

            // Create new CmsMediaFile entry in own db
            unset($mediaData['id']);
            unset($mediaData['file_id']);
            $mfe = new MediaFileEntity($this->getState());
            $mediaData['parent_id'] = $folderId;
            $mfe->setData($mediaData);
            $mfe->save();

            // Create new File entry in own db
            if ($mfe->id > 0) {
                $fe = $this->saveByContent($fileContent, $fileData['filename'], true, $fileData['object_ref'], $mfe->id, $fileData['object_key'], $fileData['sub_path']);
                $mfe->file_id = $fe->id;
                $mfe->save();
            }
        }
        $this->getState()->getView()->success = (!is_null($fe) && $fe->id > 0) ? true : false;
    }
}
