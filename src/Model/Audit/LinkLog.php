<?php
namespace BlueSky\Framework\Model\Audit;

use BlueSky\Framework\Object\Model\Base;

/**
 * Model class for audit log object fields
 *
 */
class LinkLog extends Base
{
    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__audit_link_log';

    /**
     * Retrieve Link logs via parent log id
     *
     * @param int $id - Parent log
     * @param int $page - Page number
     * @return array
     */
    public function getLinksByLogId($id, $page = 0)
    {
        $q = $this->getState()->getReadDb()->createQueryBuilder();
        $q->select('l.*')
            ->from($this->tableName, 'l')
            ->where('l.log_id = :logId')
            ->setParameter('logId', $id);

        if($page) {
            $list = $this->getListByQuery($q, $page, $this->getResultsPerPage());
        } else {
            $list = $this->getListByQuery($q);
        }
        return $list;
    }

    /**
     * Retrieve Link logs via parent object log id
     *
     * @param int $id - Parent object log
     * @param int $page - Page number
     * @return array
     */
    public function getLinksByObjectId($id, $page = 0)
    {
        $q = $this->getState()->getReadDb()->createQueryBuilder();
        $q->select('l.*')
            ->from($this->tableName, 'l')
            ->where('l.object_log_id = :objectLogId')
            ->setParameter('objectLogId', $id);

        if($page) {
            $list = $this->getListByQuery($q, $page, $this->getResultsPerPage());
        } else {
            $list = $this->getListByQuery($q);
        }
        return $list;
    }
}