<?php
namespace BlueSky\Framework\Model\Audit;

use BlueSky\Framework\Object\Model\Base;

/**
 * Model class for audit log object fields
 *
 */
class FieldLog extends Base
{
    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__audit_field_log';

    /**
     * Retrieve field logs via parent object id
     *
     * @param int $id - Parent object id
     * @param int $page - Page number
     * @return array
     */
    public function getObjectsByObjectId($id, $page = 0)
    {
        $q = $this->getState()->getReadDb()->createQueryBuilder();
        $q->select('f.*')
            ->from($this->tableName, 'f')
            ->where('f.object_id = :objectId')
            ->setParameter('objectId', $id);

        if($page) {
            $list = $this->getListByQuery($q, $page, $this->getResultsPerPage());
        } else {
            $list = $this->getListByQuery($q);
        }
        return $list;
    }
}