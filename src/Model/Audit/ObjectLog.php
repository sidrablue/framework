<?php
namespace BlueSky\Framework\Model\Audit;

use BlueSky\Framework\Object\Model\Base;

/**
 * Model class for audit log objects
 */
class ObjectLog extends Base
{
    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__audit_object_log';

    /**
     * Retrieve Object logs via parent log id
     *
     * @param int $id - Parent log
     * @param int $page - Page number
     * @return array
     */
    public function getObjectsByLogId($id, $page = 0)
    {
        $q = $this->getState()->getReadDb()->createQueryBuilder();
        $q->select('o.*')
            ->from($this->tableName, 'o')
            ->where('o.log_id = :logId')
            ->setParameter('logId', $id);

        if($page) {
            $list = $this->getListByQuery($q, $page, $this->getResultsPerPage());
        } else {
            $list = $this->getListByQuery($q);
        }
        return $list;
    }

}