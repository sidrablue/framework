<?php
namespace BlueSky\Framework\Model\Audit;

use BlueSky\Framework\Object\Model\Base;

/**
 * Model class for audit logs
 */
class Log extends Base
{
    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__audit_log';



}
