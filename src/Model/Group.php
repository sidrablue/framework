<?php
namespace BlueSky\Framework\Model;

use Doctrine\DBAL\Connection;
use Lote\System\Admin\Model\QueryUser as QueryUserModel;
use BlueSky\Framework\Model\User as UserModel;
use BlueSky\Framework\Object\Model\Base;
use BlueSky\Framework\Util\Arrays;
use BlueSky\Framework\Query\DynamicQuery;

/**
 * Model class for Users
 */
class Group extends Base
{

    /**
     * @var String $tableName - the name of the table for this model
     * */
    protected $tableName = 'sb__group';

    /**
     * Get selected groups
     * @param array $selectedGroupsArr
     * @param string $objectKey
     * @return array
     */
    public function getUserSelectedGroups($selectedGroupsArr, $objectKey = '')
    {

        $ids = join(',', $selectedGroupsArr);

        $result = [];
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('g.*')
            ->from('sb__group', 'g')
            ->where('g.id in (:ids)')
            ->setParameter('ids', $ids);;

        if(!empty($objectKey)) {
            $q->andWhere('g.object_key = :object_key')
                ->setParameter('object_key', $objectKey);
        }

        $query = $q->execute()->fetchAll(
            \PDO::FETCH_ASSOC
        );

        if (!empty($query)) {
            $result = $query;
        }
        return $result;
    }

    /**
     * Get all of the groups, and optionally of a particular tag
     * @param array|string $tag
     * @todo - Make these tags work
     * @param string $objectKey
     * @return array
     * */


    public function getGroups($tag = '', $objectKey = '')
    {
        $result = [];
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('t.*')->from($this->getTableName(),'t');
        if(is_string($tag) && $tag) {
            $q->leftJoin("t", "sb__tag_usage", "tu", "t.id = tu.object_id")
                ->leftJoin("tu", "sb__tag", "tag", "tag.id = tu.tag_id")
                ->andWhere("tag.lote_deleted is null")
                ->andWhere("tu.lote_deleted is null")
                ->andWhere("tag.value = :tagVal")
                ->setParameter("tagVal", $tag);
        }
        else if(is_array($tag) && count($tag) > 0) {

        }
        $q->andWhere("t.lote_deleted is null");

        if(!empty($objectKey)) {
            $q->andWhere('t.object_key = :object_key')
                ->setParameter('object_key', $objectKey);
        }

        $q->addOrderBy('t.sort_order', 'asc');
        $q->addOrderBy('t.name', 'asc');
        $q->addOrderBy('t.id', 'asc');

        $m = new Group($this->getState());
        $m->addLoteAccessClauses($q, 't');

        $s = $q->execute();
        while($row = $s->fetch(\PDO::FETCH_ASSOC)) {
            $result[] = $row;
        }
        return $result;
    }

    /**
     * Get all of the groups that a user is in
     * @param int $userId
     * @param string $objectKey
     * @return array|false
     * */
    public function getUserGroups($userId, $objectKey = '')
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('g.*')->from('sb__group', 'g')
            ->leftJoin('g', 'sb__user_group', 'ug', 'ug.group_id = g.id')
            ->where('ug.user_id = :user_id')->setParameter('user_id', $userId);

        if(!empty($objectKey)) {
            $q->andWhere('g.object_key = :object_key')
                ->setParameter('object_key', $objectKey);
        }
        $q->andWhere("g.lote_deleted is null")->andWhere("ug.lote_deleted is null");
        $s = $q->execute();
        return $s->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * Get the information for a set of groups as specified by an array of IDS
     * @access public
     * @param array $groupIds
     * @param bool $indexByIds
     * @return array
     * */

    public function getGroupList($groupIds, $indexByIds = false)
    {
        $result = [];
        if(is_array($groupIds)) {
            $q = $this->getReadDb()->createQueryBuilder();
            $q->select('g.*')
                ->from('sb__group', 'g')
                ->where('g.id in (:group_ids)')
                ->setParameter('group_ids', $groupIds, Connection::PARAM_INT_ARRAY)
                ->andWhere("g.lote_deleted is null");
            $rows = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
            if(is_array($rows)) {
                foreach ($rows as $v) {
                    if ($indexByIds) {
                        $result[$v['id']] = $v;
                    } else {
                        $result[] = $v;
                    }
                }
            }
        }
        return $result;
    }

    public function getAllGroups($parentId = 0, $kind = '', $includeDynamic = true, $is_parent_account_accessible = null)
    {
        $result = [];
        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select('*')
            ->from($this->getTableName(), 'sbc')
            ->andWhere('parent_id = :parent_id')
            ->setParameter('parent_id', $parentId);
        if(!$includeDynamic) {
            $q->andWhere('group_type != "dynamic"');
        }
        if($is_parent_account_accessible) {
            $q->andWhere('is_parent_account_accessible = 1');
        }
        $q->andWhere('lote_deleted is null')
            ->orderBy('sort_order')
            ->addOrderBy('id');

        if(!empty($kind)) {
            $q->andWhere('kind = :kind')
                ->setParameter('kind', $kind);
        }

        $query = $data->execute();
        while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
            $row['children'] = $this->getAllGroups($row['id'], $kind);
            $result[] = $row;
        }
        return $result;
    }

    public function getAllGroup(?string $type = ""): array
    {
        $result = [];
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('g.*')->from('sb__group', 'g')->andWhere("g.lote_deleted is null");
        if($type) {
            $q->andWhere("group_type = :type")->setParameter("type", $type);
        }
        if($rows = $q->execute()->fetchAll(\PDO::FETCH_ASSOC)) {
            $result = $rows;
        }
        return $result;
    }

    /**
     * Ensure that a specific user belongs to a set of provided groups
     * @param int $userId
     * @param array $groupIds
     * */
    public function addUserGroups($userId, $groupIds)
    {
        if(is_array($groupIds) && count($groupIds)>0) {
            $userGroups = Arrays::indexArrayByField($this->getUserGroups($userId), 'id');
            foreach ($groupIds as $gid) {
                if (!isset($userGroups[$gid])) {
                    //Assign new user group data
                    $data = [
                        'user_id' => $userId,
                        'group_id' => $gid,
                    ];
                    $this->getState()->getWriteDb()->insert('sb__user_group', $data);
                }
            }
        }
    }

    /**
     * Get the public groups
     * @param string $objectKey
     * @return array
     */
    public function getPublicGroupsList($objectKey = 'user')
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select('g.*')
            ->from($this->getTableName(), 'g')
            ->where('g.is_public = 1')
            ->andWhere('g.lote_deleted is null')
            ->andWhere('g.object_key = :object_key')
            ->setParameter('object_key', $objectKey)
            ->orderBy('weighting', 'desc')
            ->addOrderBy('name', 'asc');
        return $data->execute()->fetchAll(\PDO::FETCH_ASSOC);
    }


    /**
     * Get groups by tag
     * @param array $tag
     * @param array $paging
     * @param string $objectKey
     * @param array/string $orderBy
     * @param $groupType $objectKey
     * @return array
     */
    public function getByTag($tag, $paging = [], $objectKey = '', $orderBy = [], $groupType = false)
    {
        $result = [];
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('distinct g.*')
            ->from($this->tableName, 'g')
            ->leftJoin('g', 'sb__tag_usage', 'u', 'u.object_id = g.id')
            ->leftJoin('u', 'sb__tag', 't', 't.id = u.tag_id')
            ->andWhere('u.object_ref = :reference')
            ->setParameter('reference', 'user.group');

        if(is_string($tag)) {
            $q->andWhere('t.value = :tag')
                ->setParameter('tag', $tag);
        }
        elseif(is_array($tag)) {
            $q->andWhere('t.value in (:tags)')
                ->setParameter('tags', $tag, Connection::PARAM_STR_ARRAY);
        }

        if(!empty($objectKey)) {
            $q->andWhere('g.object_key = :object_key')
                ->setParameter('object_key', $objectKey);
        }
        if($groupType) {
            $q->andWhere('g.group_type = :group_type')
                ->setParameter('group_type', $groupType);
        }

        $q->andWhere('g.lote_deleted is null');
        $q->andWhere("t.lote_deleted is null");
        if(!empty($paging)){
            $q->setFirstResult($paging['start'] - 1 >= 0 ? $paging['start'] - 1 : $paging['start']);
            $q->setMaxResults($paging['limit']);
        }
        if(!empty($orderBy)){
            if(is_array($orderBy)){
                foreach($orderBy as $sort=>$order){
                    if(is_string($sort) && is_string($order) && !empty($sort) && !empty($order)){
                        $q->addOrderBy($sort, $order);
                    }
                }
            }
            else{
                if(is_string($orderBy) && !empty($orderBy)){
                    $q->addOrderBy($orderBy, "asc");
                }
            }
        }
        else{
            $q->addOrderBy('weighting', 'desc');
            $q->addOrderBy('name', 'asc');
        }


        $query = $q->execute();
        while ($row = $query->fetchAll(\PDO::FETCH_ASSOC)) {
            $result = $row;
        }
        return $result;
    }

    public function getIdsFromLdapNames($ldapNames)
    {
        $result = [];
        if (count($ldapNames) > 0) {
            $q = $this->getReadDb()->createQueryBuilder();
            $q->select("id")
                ->from("sb__group", "g")
                ->where("g.ldap_path in (:paths)")
                ->setParameter("paths", array_values($ldapNames), Connection::PARAM_STR_ARRAY);
            foreach($q->execute()->fetchAll(\PDO::FETCH_ASSOC) as $v) {
                $result[] = $v['id'];
            }
        }
        return $result;
    }

    public function updateGroups($userId, $userGroups, $approvedUserId = 1, $subscriptionStatus = 1)
    {
        $currentGroups = $this->getUserGroups($userId);
        $currentGroups = Arrays::getFieldValuesFrom2dArray($currentGroups, 'id');

        $groupsToRemove = array_diff($currentGroups, $userGroups);
        $groupsToAdd = array_diff($userGroups, $currentGroups);

        if (!empty($groupsToRemove)) {
            $this->removeUserGroups($userId, $groupsToRemove);
        }
        if (!empty($groupsToAdd)) {
            $this->insertUserGroups($userId, $groupsToAdd, $approvedUserId, $subscriptionStatus);
        }
    }

    /**
     * @todo - Michael centralise this - find other instances and remove
     * */
    public function insertUserGroups($userId, $userGroups, $approvedUserId = 1, $subscriptionStatus = 1)
    {
        if (is_numeric($userId) && count($userGroups) > 0) {
            foreach ($userGroups as $v) {
                $this->getWriteDb()->insert('sb__user_group', ['user_id' => $userId, 'group_id' => $v, 'approved_by_user_id'=>$approvedUserId, 'subscription_status' =>$subscriptionStatus]);
            }
        }
    }

    /**
     * @param $userId
     * @param $userGroups
     * @throws \Doctrine\DBAL\Exception\InvalidArgumentException
     */
    public function removeUserGroups($userId, $userGroups)
    {
        if (is_numeric($userId) && count($userGroups) > 0) {
            foreach($userGroups as $groupId) {
                $params = ["user_id" => $userId, "group_id" => $groupId];
                $types = ["user_id" => \PDO::PARAM_INT, "group_id" => \PDO::PARAM_INT];
                $this->getWriteDb()->delete("sb__user_group", $params, $types);
            }
        }
    }

    /**
     * Code exists
     * @access public
     * @param $code
     * @param int $id
     * @return bool
     */
    public function codeExists($code, $id = 0)
    {
        return $this->valueExists('code', $code, $id);
    }


    /**
     * Retrieve all children groups to a selected group
     *
     * @access public
     * @param int $groupId - Parent group ID
     * @return array
     */
    public function getChildGroups($groupId)
    {
        $q = $this->getState()->getReadDb()->createQueryBuilder();
        $q->select('g.*')
            ->from($this->getTableName(), 'g')
            ->where('g.parent_id = :parentId')
            ->setParameter('parentId', $groupId);
        $groups = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
        return $groups;
    }


    /**
     * Retrieve all users linked to a selected group
     *
     * @access public
     * @param int $groupId - Group ID
     * @return array
     */
    public function getUsersInGroup($groupId, $select = 'u.*')
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select($select)
            ->from('sb__user_group', 'ug')
            ->leftJoin('ug', 'sb__user', 'u', 'ug.user_id = u.id')
            ->leftJoin('ug', 'sb__group', 'g', 'ug.group_id = g.id')
            ->where('ug.group_id = :groupId')
            ->setParameter('groupId', $groupId)
            ->andWhere("ug.lote_deleted is null")
            ->andWhere("u.lote_deleted is null")
            ->andWhere("g.lote_deleted is null")
            ->andWhere("u.id > 0")
            ->addOrderBy('u.last_name')
            ->addOrderBy('u.first_name');
        $users = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
        return $users;
    }

    public function isDynamic($groupId)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('t.group_type')
            ->from($this->getTableName(), 't')
            ->andWhere('t.id = :id')
            ->setParameter('id', $groupId)
            ->andWhere('t.group_type = "dynamic"')
            ->andWhere('t.lote_deleted is null')
            ->setMaxResults(1);
        return $q->execute()->fetch(\PDO::FETCH_COLUMN) !== false;
    }

    public function getDynamicGroupMembers($groupId, $select = "t.*")
    {
        $e = new \BlueSky\Framework\Entity\Group($this->getState());
        $e->load($groupId);

        // Retrieve query clauses
        $qum = new QueryUserModel($this->getState());
        $data = $qum->getQuery($e->query_id);
        $dqb = new DynamicQuery($this->getState());
        $q = $dqb->generateQueryBuilder($data);
        $m = new UserModel($this->getState());
        $userList = $m->getListByQuery($q);
        return $userList['row'] ?? [];
    }

    public function getAllUsersInAdminGroups()
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("u.*")
            ->from('sb__user_group', 'ug')
            ->leftJoin('ug', 'sb__user', 'u', 'ug.user_id = u.id')
            ->leftJoin('ug', 'sb__group', 'g', 'ug.group_id = g.id')
            ->where('g.object_key = :object_key')
            ->setParameter('object_key', 'admin')
            ->andWhere("ug.lote_deleted is null")
            ->andWhere("u.lote_deleted is null")
            ->andWhere("g.lote_deleted is null")
            ->andWhere("u.id > 0");
        return $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * Get all the groups
     *
     * @access public
     * @param int $parentId
     * @param string $kind
     * @return array
     */
    public function getAllGroups2($parentId = 0, $kind = '', $includeDynamic = true, $isParentAccountAccessible = null, $includeInactive = true)
    {
        $result = [];
        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select('*')
            ->from($this->getTableName(), 'sbc')
            ->andWhere('parent_id = :parent_id')
            ->setParameter('parent_id', $parentId);
        if (!$includeDynamic) {
            $q->andWhere('group_type != "dynamic"');
        }
        if ($isParentAccountAccessible) {
            $q->andWhere('is_parent_account_accessible = 1');
        }
        if ($includeInactive) {
            $q->andWhere('is_active = 1');
        }
        $q->andWhere('lote_deleted is null')
            ->orderBy('sort_order')
            ->addOrderBy('id');

        if (!empty($kind)) {
            $q->andWhere('kind = :kind')
                ->setParameter('kind', $kind);
        }
        $this->addLoteAccessClauses($q, 'sbc', 'sb__group');

        $query = $data->execute();
        while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
            $row['children'] = $this->getAllGroups($row['id'], $kind);
            $result[] = $row;
        }
        return $result;
    }

    /**
     * Check if a user is in an admin group
     * @param int $userId
     * @access public
     * @return bool
     * */
    public function isInAdminGroup($userId)
    {
        $result = false;
        $userGroups = $this->getUserGroups($userId);
        if($userGroups && is_array($userGroups)) {
            foreach ($userGroups as $g) {
                if ($g['object_key'] == 'admin') {
                    $result = true;
                    break;
                }
            }
        }
        return $result;
    }


    /**
     * Check if a user is in a protected group
     * @param int $userId
     * @access public
     * @return array
     * */
    public function isInProtectedGroup($userId)
    {
        $result = false;
        $userGroups = $this->getUserGroups($userId);
        if($userGroups && is_array($userGroups)) {
            foreach ($userGroups as $g) {
                if ($g['is_protected'] == '1') {
                    $result = true;
                    break;
                }
            }
        }
        return $result;
    }

    /**
     * Retrieve all groups with a broadcast reference
     *
     * @access public
     * @return array
     */
    public function getBroadcastGroups()
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('g.*')
            ->from($this->getTableName(), 'g')
            ->where('g.broadcast_ref IS NOT NULL')
            ->andWhere('g.lote_deleted IS NULL');
        return $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getUserType($userId, $type = "groupId", $checkIsAdmin = true)
    {
        $return = [];
        if ($this->getState()->getUser()->isAdmin() && $checkIsAdmin) {
            $return[] = 1;
        } else {
            $q = $this->getReadDb()->createQueryBuilder();
            $data = $q->select("g.id,g.name")
                ->from('sb__user', 'u')
                ->leftJoin('u', 'sb__user_group', 'ug', 'ug.user_id = u.id')
                ->leftJoin('ug', 'sb__group', 'g', 'g.id = ug.group_id')
                ->where('u.id = :user_id')
                ->andWhere('u.lote_deleted is null')
                ->andWhere('ug.lote_deleted is null')
                ->andWhere('g.lote_deleted is null')
                ->setParameter("user_id", $userId);
            $query = $data->execute();
            $result = $query->fetchAll(\PDO::FETCH_ASSOC);
            if(!empty($result)){
                foreach ($result as $r) {
                    if ($type == "groupName") {
                        $return[] = $r['name'];
                    } else {
                        $return[] = $r['id'];
                    }
                }
            }
        }
        return $return;
    }

    /**
     * Retrieve Group search query
     *
     * @access public
     * @param array $phrase - Search parameters
     * @param array $orderBy - Search parameters
     *
     * @return \Doctrine\DBAL\Query\QueryBuilder
     */

    public function getSearchQuery($phrase = '' ,$orderBy = [])
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("g.*")
            ->from($this->getTableName(), 'g')
            ->andWhere("g.lote_deleted is null");
        if (!empty($phrase)) {
            $q = $this->getPhraseSplitQuery($q, $phrase, ['g.name']);
        }
        if ($orderBy) {
            $q->orderBy('g.' . key($orderBy), current($orderBy));
        }

        return $q;

    }
    
    public function updatePublicGroups (User $ue, ?array $selectedPublicGroups) {
        $currentLinks = $this->getState()->getReadDb()->createQueryBuilder()
            ->select('ug.id')
            ->from('sb__user', 'u')
            ->innerJoin('u', 'sb__user_group', 'ug', 'u.id = ug.user_id and ug.lote_deleted is null')
            ->innerJoin('ug', 'sb__group', 'g', 'g.id = ug.group_id and g.lote_deleted is null')
            ->andWhere('u.lote_deleted is null')
            ->andWhere('u.id = :id')
            ->andWhere('g.is_public = true')
            ->andWhere('g.object_key = "user"')
            ->setParameter('id', $ue->id)
            ->execute()->fetchAll(\PDO::FETCH_COLUMN);
            $this->getState()->getWriteDb()->createQueryBuilder()
                ->update(UserGroupEntity::getTableName())
                ->set('lote_deleted', 'NOW()')
                ->andWhere('id in (:ids)')
                ->setParameter('ids', $currentLinks, Connection::PARAM_INT_ARRAY)
                ->execute();
            if ($selectedPublicGroups !== null) {
                foreach ($selectedPublicGroups as $selectedPublicGroup) {
                    $ge = new GroupEntity($this->getState());
                    if ($ge->load($selectedPublicGroup)) {
                        if ($ge->is_public && $ge->object_key === 'user') {
                            $uge = new UserGroupEntity($this->getState());
                            if (!$uge->loadByFields([ 'user_id' => $ue->id, 'group_id' => $ge->id, ])) {
                                $uge->user_id = $ue->id;
                                $uge->group_id = $selectedPublicGroup;
                                $uge->save();
                            }
                        }
                    }
                }
            }
        }
}
