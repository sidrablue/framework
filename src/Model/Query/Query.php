<?php
namespace BlueSky\Framework\Model\Query;

use BlueSky\Framework\Object\Data\Field\Factory;
use BlueSky\Framework\Object\Model\Base;
use BlueSky\Framework\Entity\Query\Query as QueryEntity;
use BlueSky\Framework\Entity\Query\QueryClause as QueryClauseEntity;

/**
 * Model class for Query
 */
class Query extends Base
{

    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__query';

    protected $queryTable;
    protected $queryTableAlias;
    protected $queryType;

    /**
     * Function used to get query
     *
     * @access public
     * @param $id
     * @return array
     */
   public function getQuery($id)
    {
        $q = new QueryEntity($this->getState());
        $q->load($id);
        return $q->getData();
    }

    public function saveQuery($name, $objectRef, $operator, $conditions, $id = 0, $description = '', $groups = [])
    {
        $parentId = 0;
        $qe = new QueryEntity($this->getState());
        $qe->id = $id;
        $qe->name = $name;
        $qe->description = $description;
        $qe->object_ref = $objectRef;
        $qe->operator = $operator;
        $queryId = $qe->save();

        // Remove all clauses if ID is set
        if ($id > 0) {
            $qm = new QueryClause($this->getState());
            $qm->deleteQueryClauses($id);
        }

        // Save Query Fields
        if (!empty($conditions)) {
            foreach ($conditions as $key => $condition) {
                $qfe = new QueryClauseEntity($this->getState());
                $qfe->query_id = $queryId;
                $qfe->parent_id = $parentId;
                $qfe->field_reference = $condition['field'];
                $qfe->entity_reference = str_replace('___', '\\', $condition['reference']);
                if ($e = $this->getState()->getSchema()->getEntityByReference($qfe->entity_reference)) {
                    if ($f = $e->getEntityField($qfe->field_reference)) {
                        $qfe->field_type = $f->field_type;
                    }
                }
                $qfe->constraint_type = $condition['constraint_type'];
                if (is_array($condition['constraint_value'])) {
                    $qfe->constraint_value = json_encode($condition['constraint_value']);
                } else {
                    $qfe->constraint_value = $condition['constraint_value'];
                }
                $qfe->save();
            }
        }

        if (!empty($groups)) {
            $this->saveChildQuery($queryId, $groups);

        }

        return $queryId;
    }

    private function saveChildQuery($parentId, $groups)
    {
        foreach ($groups as $group) {
            $operator = $group['operator'];
            $qe = new QueryEntity($this->getState());
            $qe->operator = $operator;
            $childQueryId = $qe->save();
            if (isset($group['conditions'])) {
                foreach ($group['conditions'] as $key => $condition) {
                    $qfe = new QueryClauseEntity($this->getState());
                    $qfe->query_id = $childQueryId;
                    $qfe->parent_id = $parentId;
                    $qfe->field_reference = $condition['field'];
                    $qfe->field_type = $condition['type'];
                    $qfe->entity_reference = str_replace('___', '\\', $condition['reference']);
                    $qfe->constraint_type = $condition['constraint_type'];
                    if (is_array($condition['constraint_value'])) {
                        $qfe->constraint_value = json_encode($condition['constraint_value']);
                    } else {
                        $qfe->constraint_value = $condition['constraint_value'];
                    }
                    $qfe->save();
                }
            }
            if (isset($group['groups']) && !empty($group['groups'])) {
                $this->saveChildQuery($childQueryId, $group['groups']);
            }
        }
    }
}
