<?php
namespace BlueSky\Framework\Model\Query;

use BlueSky\Framework\Object\Model\Base;
use BlueSky\Framework\Entity\Query\QueryClause as QueryClauseEntity;

class QueryClause extends Base
{

    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__query_clause';

    public const FIELD_TYPE_TO_CLAUSE_TYPES_MAPPING = [ /// FIXME -> this will need to be deleted after clause_types is updated to automatically pick its type
        'string' => 'string',
        'text' => 'string',
        'longtext' => 'string',
        'email' => 'string',
        'file' => 'string',
        'url' => 'string',
        'password' => 'string',
        'textarea' => 'string',
        'heading' => 'string',
        'header' => 'string',
        'message' => 'string',
        'radio_button' => 'json',
        'recaptcha' => 'string',
        'telephone' => 'string',
        'image' => 'string',
        'footing' => 'string',
        'footer' => 'string',

        'bool' => 'boolean',
        'boolean' => 'boolean',

        'datetime' => 'date_time',
        'datetime_immutable' => 'date_time',
        'date' => 'date',
        'date_immutable' => 'date',
        'time' => 'date_time',

        'enum' => 'enum',
        'enum_integer' => 'enum',
        'primary_key_int' => 'enum',

        'integer' => 'integer',
        'int' => 'integer',
        'decimal' => 'integer',
        'fkey' => 'integer',
        'currency' => 'integer',

        'json' => 'base_option',
        'single_select' => 'base_option',
        'multi_select' => 'base_option',
        'checkbox' => 'base_option',
        'radiobutton' => 'base_option',
        'matrix' => 'base_option',
        'country' => 'base_option',
        'public_group' => 'base_option',
    ];


    /**
     * Get clauses
     * @access public
     * @param $id
     * @return mixed
     */
    public function getClauses($id)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("t.*")
            ->addSelect("JSON_VALID(t.constraint_value) as is_json_valid")
            ->from($this->getTableName(), 't')
            ->andWhere('t.lote_deleted is null')
            ->andWhere('t.query_id = :queryId')
            ->setParameter('queryId', $id);

        $s = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($s as $k => $v) {
            $s[$k] = $this->processValueField($v);
        }
        return $s;
    }

    private function processValueField($clauseEntry)
    {
        $clauseEntry['value'] = $clauseEntry['constraint_value'];
        if ($clauseEntry['is_json_valid']) {
            $clauseEntry['value'] = json_decode($clauseEntry['constraint_value'], true);
        }
        $clauseEntry['reference'] = $clauseEntry['entity_reference'];
        $clauseEntry['field'] = $clauseEntry['field_reference'];

//        if ($entity = $this->getState()->getSchema()->getEntityByReference($entityReference)) {
//            if ($field = $entity->getEntityField($fieldReference)) {
//                $type = $field->field_type;
//                if ($type === 'datetime' || $type === 'datetime_immutable') {
//                    $type = 'date_time';
//                }
//                $type = str_replace('_immutable', '', $type);
//
//            }
//        }
        $clauseEntry['type'] = QueryClause::FIELD_TYPE_TO_CLAUSE_TYPES_MAPPING[$clauseEntry['field_type']] ?? $clauseEntry['field_type'];
        return $clauseEntry;
    }

    public function deleteQueryClauses($queryId)
    {
        $queryEntities = $this->getClauses($queryId);
        foreach ($queryEntities as $entity) {
            $qe = new QueryClauseEntity($this->getState());
            $id = $entity['id']?? '';
            $qe->load($id);
            $qe->delete();
            //$this->getState()->getSchema()->getEntityByReference($entity['entity_reference'])->delete();
        }
    }
}
