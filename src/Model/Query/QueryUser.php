<?php
namespace BlueSky\Framework\Model\Query;

use BlueSky\Framework\Entity\Query\Query as QueryEntity;
use BlueSky\Framework\Entity\Query\QueryClause as QueryClauseEntity;
use BlueSky\Framework\Model\Query\QueryClause as QueryClauseModel;
use BlueSky\Framework\Object\Data\Field\Factory;

/**
 * Model class for Dynamic User Queries
 */
class QueryUser extends Query
{
    protected $queryTable = 'sb__user';
    protected $queryTableAlias = 'u';
    protected $queryType = 'user';

    protected $objectReference = '_user';

    //protected $resultsPerPage = 3;

    /**
     * Function used to get query
     *
     * @access public
     * @param $id
     * @return array
     */
    public function getQuery1($id)
    {
        $data = [];
        $qe = new QueryEntity($this->getState());
        $qe->load($id);
        $data['query'] = $qe->getData();

        $qce = new QueryClauseModel($this->getState());
        $data['clauses'] = $qce->getClauses($id);

        return $data;
    }

    public function getQuery($id)
    {
        $qe = new QueryEntity($this->getState());
        $qe->load($id);
        $data['query'] = $qe->getData();
        $qce = new QueryClauseModel($this->getState());
        $clause['root_reference'] = $qe->object_ref;
        $clause['operator'] = $qe->operator;
        $clause['conditions'] = $qce->getClauses($id);
        $qce = new QueryClauseEntity($this->getState());
        if ($qce->loadByField('parent_id', $qe->id)) {
            $clause['groups'] = $this->getChildQuery($qe->id);
        }
        $data['clauses'] = $clause;
        return $data;

    }

    private function getChildQuery($id, $result = [])
    {
        $qce = new QueryClauseEntity($this->getState());
        if ($qce->loadByField('parent_id', $id)) {
            $qe = new QueryEntity($this->getState());
            $qe->load($qce->query_id);
            $qe->getData();
            $result['operator'] = $qe->operator;
            $qcm = new QueryClauseModel($this->getState());
            $result['conditions'] = $qcm->getClauses($qe->id);
            if ($qce->loadByField('parent_id', $qe->id)) {
                $result['groups'] = $this->getChildQuery($qe->id, $result);
            }
        }

        return $result;
    }

    /**
     * Function used to soft delete a query
     *
     * @access public
     * @param $id
     * @return array
     */
    public function softDelete($id)
    {
        $qe = new QueryEntity($this->getState());
        $qe->load($id);
        $qe->delete();
        return true;
    }

    public function getQueries($objRef ='User')
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select('*')
            ->from($this->tableName, 'q')
            ->andWhere('lote_deleted is null')
            ->andWhere('q.name is not null')
            ->andWhere('object_ref = :value')
            ->setParameter('value', $objRef);
        return $q;
    }
}
