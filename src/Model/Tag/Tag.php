<?php
namespace BlueSky\Framework\Model\Tag;

use Doctrine\DBAL\Connection;
// Might want to remove AccountEntity dependency, as it belongs to an app
use Doctrine\DBAL\Query\QueryBuilder;
use Lote\App\MasterBase\Entity\Master\Account as AccountEntity;
use BlueSky\Framework\Entity\Tag\Tag as TagEntity;
use BlueSky\Framework\Object\Model\Base;
use BlueSky\Framework\Util\Arrays;
use BlueSky\Framework\Model\Tag\Usage as TagUsageModel;
use BlueSky\Framework\Model\Tag\Context as TagContextModel;

class Tag extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__tag';

    public function getTagsByReference($reference, $phrase = '')
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select('t.id, t.value')
            ->from($this->tableName, 't')
            ->leftJoin('t', 'sb__tag_usage', 'u', 't.id = u.tag_id')
            ->where('u.object_ref = :reference')
            ->andWhere('t.value LIKE :q')
            ->setParameter('reference', $reference)
            ->setParameter('q', "%$phrase%");

        $query = $data->execute();
        return $query->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getSearchQuery($phrase = '', $object = '', $masterTags = false, $accountRef = '', $sortField = '', $sortDirection = '')
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('t.*')
            ->from($this->tableName, 't')
            ->andWhere('t.lote_deleted is null');

        if (!empty($phrase)) {
            $q = $this->getPhraseSplitQuery($q, $phrase, ['t.value', 't.display_value', 't.colour_code']);
        }

        if ($masterTags && $accountRef) {
            $q->andWhere('t.account_ref = :account_ref')
                ->setParameter('account_ref', $accountRef);
        }

        if (!empty($object)) {
            $ids = [];
            $usageObj = new TagUsageModel($this->getState());
            $list = $usageObj->listByField('object_ref', $object);

            if (!empty($list)) {
                foreach ($list as $value) {
                    $ids[] = $value['tag_id'];
                }
            }

            $contextObj = new TagContextModel($this->getState());
            $contextList = $contextObj->listByField('object_ref', $object);

            if (!empty($contextList)) {
                foreach ($contextList as $value) {
                    $ids[] = $value['tag_id'];
                }
            }

            $q->andWhere('t.id in (:refs)')
                ->setParameter('refs', $ids, Connection::PARAM_STR_ARRAY);
        }

        if (!empty($sortField) && !empty($sortDirection)) {
            $q->orderBy($sortField, $sortDirection);
        } else {
            $q->orderBy('t.id');
        }
        return $q;
    }

    public function getTagsByReferenceDist($reference, $phrase = '')
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select('distinct t.id, t.*')
            ->from($this->tableName, 't')
            ->leftJoin('t', 'sb__tag_usage', 'u', 't.id = u.tag_id')
            ->where('u.object_ref = :reference')
            ->andWhere('t.value LIKE :q')
            ->andWhere('t.value != ""')
            ->andWhere('t.is_active = 1')
            ->andWhere('t.lote_deleted is null')
            ->setParameter('reference', $reference)
            ->setParameter('q', "%$phrase%");

        $query = $data->execute();
        return $query->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getAllTagsByReferenceDist($reference, $phrase = '')
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select('distinct t.id, t.*')
            ->from($this->tableName, 't')
            ->leftJoin('t', 'sb__tag_context', 'c', 't.id = c.tag_id')
            ->where('t.lote_deleted is null')
            ->andWhere('t.value LIKE :q')
            ->andWhere('t.value != ""')
            ->andWhere('t.is_active = 1')
            ->andWhere('(c.object_ref = "sb_event") OR (t.id IN (select tag_id from sb__tag_usage where object_ref = :reference ))')
            ->setParameter('q', "%$phrase%")
            ->setParameter('reference', $reference);

        $query = $data->execute();
        return $query->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * Get a list of tags by a context
     *
     * Note: method has dependency on MasterBase app for the account entity
     *
     * @FIXME don't fetch all tags by default
     * @access public
     * @param array|string $references
     * @param string $phrase - optional
     * @param string $accountRef
     * @return array|false
     * */
    public function getTagsByContext($references, $phrase = '', $accountRef = '', $distinct = false)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        if($distinct){
            $data = $q->select('distinct t.id, t.value, t.display_value, t.colour_code');
        }
        else{
            $data = $q->select('t.id, t.value, t.display_value, t.colour_code');
        }
        $data->from($this->getTableName(), 't')
            ->leftJoin('t', 'sb__tag_context', 'u', 't.id = u.tag_id')
            ->andWhere('t.lote_deleted is null')
            ->andWhere('u.lote_deleted is null');

        if (is_array($references)) {
            $q->andWhere('u.object_ref = "sitewide" or u.object_ref in (:refs)')
                ->setParameter('refs', $references, Connection::PARAM_STR_ARRAY);
        } else {
            $q->andWhere('u.object_ref = "sitewide" or u.object_ref = :refs')
                ->setParameter('refs', $references);
        }

        if ($phrase) {
            $q->andWhere('t.value like :phrase')->setParameter('phrase', "%$phrase%");
        }

        if (!empty($accountRef)) {
            $account = new AccountEntity($this->getState());
            $account->loadByField('reference', $accountRef);
            //If account is master, retrieve all tags
            if (!$account->is_master) {
                $q->andWhere('t.account_ref = :account_ref')->setParameter('account_ref', $account->reference);
            }
        }

        $q->addOrderBy("t.value", "asc");

        $query = $data->execute();

        return $query->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * Get tag hints based on the current user input
     * @param string $reference - the reference of the tag hints to look for
     * @param string $phrase - the currently typed in user phrase
     * @param string $fieldRef - the reference of the tag hints to look for
     * @return array|false
     * */
    public function getTagHints($reference, $phrase = '', $fieldRef = '')
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select('*')
            ->from($this->tableName, 't')
            ->leftJoin('t', 'sb__tag_usage', 'u', 'u.tag_id = t.id')
            ->where('u.object_ref = :reference')
            ->setParameter('reference', $reference);
        if (!empty($fieldRef)) {
            $q->where('u.field_ref = :field_ref')
                ->setParameter('field_ref', $fieldRef);
        }
        $q->andWhere('t.value like :q')
            ->setParameter('q', "%$phrase%")
            ->groupBy('t.value')
            ->setMaxResults(20);
        $query = $data->execute();
        return $query->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getTagById($id)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select('t.value')
            ->from($this->tableName, 't')
            ->where('t.id = :id')
            ->setParameter('id', $id);

        $query = $data->execute();
        return $query->fetch(\PDO::FETCH_ASSOC);
    }
    public function getTagDataById($id)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select('t.*')
            ->from($this->tableName, 't')
            ->where('t.id = :id')
            ->setParameter('id', $id);

        $query = $data->execute();
        return $query->fetch(\PDO::FETCH_ASSOC);
    }

    /**
     * @param $id
     * @param string $objectRef
     * @param string $fieldRef
     * @param array|boolean $showRestrictedTags - true for show restricted, false for hide or array of tag values to show
     * @return array
     */
    public function getTagsByObjectId($id, $objectRef = 'blog.post', $fieldRef = '', $showRestrictedTags = false)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select('*, t.id')
            ->from($this->getTableName(), 't')
            ->leftJoin('t', 'sb__tag_usage', 'u', 't.id = u.tag_id')
            ->andWhere('u.object_id = :id')
            ->andWhere('u.object_ref = :object_ref')
            ->orderBy('t.weighting', 'desc')
            ->setParameter('id', $id)
            ->setParameter('object_ref', $objectRef)
            ->andWhere('t.is_active = 1')
            ->andWhere('u.lote_deleted is null')
            ->andWhere('t.lote_deleted is null')
            ->groupBy("t.id");
        if (!empty($fieldRef)) {
            $q->andWhere('u.field_ref = :field_ref')
                ->setParameter('field_ref', $fieldRef);
        }
        $query = $data->execute();
        $result = $query->fetchAll(\PDO::FETCH_ASSOC);

        return $result;
    }

    public function getTagValues($objectId, $objectRef = '', $fieldRef = '')
    {
        $result = [];
        $tags = $this->getTagsByObjectId($objectId, $objectRef, $fieldRef);
        if (is_array($tags)) {
            foreach ($tags as $v) {
                $result[] = !empty($v['display_value']) ? $v['display_value'] : $v['value'];
            }
        }
        return $result;
    }
    public function getTagByValueLower($value)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("*")
            ->from('sb__tag', 'a')
            ->where("lower(a.value) = :value")
            ->setParameter("value" , strtolower($value))
            ->andWhere('a.lote_deleted is null');


        $query = $q->execute();
        $result = $query->fetch(\PDO::FETCH_ASSOC);
        return $result;
    }
    public function getTagByValue($value)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select('*')
            ->from($this->tableName, 't')
            ->where('t.value = :value')
            ->setParameter('value', $value);

        $query = $data->execute();
        $result = $query->fetch(\PDO::FETCH_ASSOC);


        return $result;
    }

    public function checkIfTagExistsAndAddIfNot($id, $tag, $reference)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select('count(*), t.id')
            ->from($this->tableName, 't')
            ->where('t.value = :tag')
            ->andWhere('t.reference = :reference')
            ->setParameter('tag', $tag)
            ->setParameter('reference', $reference);

        $query = $data->execute();
        $result = $query->fetch(\PDO::FETCH_ASSOC);
        $tag_id = $result['id'];


        if ($result['count(*)'] == 0) {
            $inserted = $this->insertTag($id, $tag, $reference);


        } else {
            //check if tag is already in use

            $q = $this->getReadDb()->createQueryBuilder();
            $data = $q->select('count(*) as cnt')
                ->from('sb__tag_usage', 'u')
                ->where('u.object_id = :object_id')
                ->andWhere('u.tag_id = :tag_id')
                ->setParameter('object_id', $id)
                ->setParameter('tag_id', $result['id']);

            $query = $data->execute();
            $result = $query->fetch(\PDO::FETCH_ASSOC);

            if ($result['cnt'] == 0) {

                $insert['tag_id'] = $tag_id;
                $insert['object_id'] = $id;
                $inserted = $this->getWriteDb()->insert('sb__tag_usage', $insert);
            }
        }
    }

    private function insertTag($id, $tag, $reference)
    {
        $insert['value'] = $tag;
        $insert['reference'] = $reference;
        $insert['created'] = date_format(new \DateTime('now'), 'c');
        $insert['user_id'] = $this->getState()->getUser()->id;
        $inserted = $this->getWriteDb()->insert($this->tableName, $insert);

        if ($inserted) {
            $insertedId = $this->getWriteDb()->lastInsertId();

            $usage['tag_id'] = $insertedId;
            $usage['object_id'] = $id;
            $usage['created'] = date_format(new \DateTime('now'), 'c');
            $this->getWriteDb()->insert('sb__tag_usage', $usage);
        }

        return $inserted;
    }


    /**
     * Get the tag data for a set of tags
     * @access public
     * @param array $tags - an array of string tag names
     * @return array
     * */
    public function getTagData(Array $tags)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('*')
            ->from('sb__tag', 't')
            ->where('value in (:values)')
            ->setParameter('values', $tags, Connection::PARAM_STR_ARRAY);
        return $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * Add a new tag
     * @access public
     * @param string $value - the value of the tag
     * @return TagEntity
     * */
    public function addTag($value)
    {
        $t = new TagEntity($this->getState());
        $t->setWriteDb($this->getWriteDb());
        $t->setReadDb($this->getReadDb());
        $t->value = $value;
        $t->save();
        return $t;
    }


    /**
     * Get the tag data for a set of tags, and if they don't exist then create them
     * @access public
     * @param array $tagValues - an array of string tag values
     * @param bool $caseSensitive
     * @return array
     * */
    public function getOrCreateTags(Array $tagValues, $caseSensitive = false)
    {
        $tags = $this->getTags($tagValues, $caseSensitive);
        $tags = Arrays::prefixByValue($tags, 'value');
        foreach ($tagValues as $v) {
            if (!isset($tags[$v])) {
                $t = $this->addTag($v);
                $tags[$v] = $t->getData();
            }
        }
        return $tags;
    }

    /**
     * Get the tag data for a set of tags
     * @access public
     * @param array $tags - an array of string tag names
     * @param bool $caseSensitive
     * @return array
     * */
    public function getTags(Array $tags, $caseSensitive = false)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('*')
            ->from('sb__tag', 't')
        ->where("t.lote_deleted is null");
        if ($caseSensitive) {
            $q->andWhere('binary value in (:values)');
        } else {
            $q->andWhere('lower(value) in (:values)');
        }
        $q->setParameter('values', $tags, Connection::PARAM_STR_ARRAY);
        return $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * Add clauses to a query to ensure that tags are also checked
     * @access public
     * @param \Doctrine\DBAL\Query\QueryBuilder $queryBuilder
     * @param string $reference
     * @param string|Array $tags - the tags to check for
     * @param bool $wholeWordMatch
     * @param string $paramSuffix
     * @param array|boolean $showRestrictedTags - true for show restricted, false for hide or array of tag values to show
     * @param array|boolean $tagsByValue - true for using tag values in queries or false to use tag ids
     * @deprecated This method is both inefficient and has specific caveats for specific projects
     * @see addTagsToQuery - method is the generalisation of this method
     */
    public function addTagsQuery($queryBuilder, $reference, $tags, $wholeWordMatch = true, $paramSuffix = '', $showRestrictedTags = false, $tagsByValue = true, $defaultUsed = false, $hideNotice = false)
    {

        if ($tagsByValue) {
            $column = 'value';
        } else {
            $column = 'id';
        }
        $queryBuilder->leftJoin('t', 'sb__tag_usage', 'u', 'u.object_id = t.id and u.object_ref = :reference' . $paramSuffix)
            ->setParameter('reference' . $paramSuffix, $reference)
            ->leftJoin('u', 'sb__tag', 'tag', 'tag.id = u.tag_id')
            ->andWhere("u.lote_deleted is null")
            ->andWhere("tag.lote_deleted is null");
        if ($hideNotice) {
            $queryBuilder->andWhere('tag.value != "notice_board" or tag.value is null');
        }
        if ($tags != false) {
            if (is_string($tags) || (is_array($tags) && count($tags) > 0)) {
                if (is_iterable($tags) && count($tags) == 1) {
                    if (is_array($tags)) {
                        $tags = array_pop($tags);
                    }
                    if ($wholeWordMatch) {
                        $queryBuilder->andWhere('tag.' . $column . ' = :tag' . $paramSuffix)
                            ->setParameter('tag' . $paramSuffix, $tags);
                    } else {
                        $queryBuilder->andWhere('tag.value LIKE :tag' . $paramSuffix)
                            ->setParameter('tag' . $paramSuffix, '%' . $tags . '%');
                    }
                } elseif (is_array($tags) && count($tags) > 0) {
                    $queryBuilder->andWhere('tag.' . $column . ' in (:tags' . $paramSuffix . ')')
                        ->setParameter('tags' . $paramSuffix, $tags, Connection::PARAM_STR_ARRAY);
                } elseif (is_string($tags)) {
                    $queryBuilder->andWhere('tag.' . $column . ' in (:tags' . $paramSuffix . ')')
                        ->setParameter('tags' . $paramSuffix, $tags);
                }
            }
            if ($defaultUsed) {
                //Include events with no tags if flagged to
                $queryBuilder->orwhere("t.id NOT IN (select object_id from sb__tag_usage where object_ref = 'sb_event' )");
            }
        }
        // Hide entities with inactive and restricted tags.  Keep entities that are not linked to any tags

        $queryBuilder->andWhere('tag.is_active = 1 or tag.is_active is null');
        if (is_bool($showRestrictedTags) && !$showRestrictedTags) {
            $queryBuilder->andWhere('tag.is_restricted = 0 or tag.is_restricted is null');
        } else {
            if (is_string($showRestrictedTags) && strlen($showRestrictedTags) > 1) {
                $showRestrictedTags = [$showRestrictedTags];

            }
            if (is_array($showRestrictedTags)) {
                $queryBuilder->andWhere('tag.is_restricted = 0 or tag.' . $column . ' in (:unrestricted_tags) or tag.is_restricted is null')
                    ->setParameter('unrestricted_tags', $showRestrictedTags, Connection::PARAM_STR_ARRAY);
            }
        }
    }

    public function addTagsToQuery(
        QueryBuilder $queryBuilder,
        string $incomingTableAlias,
        $reference,
        bool $tagsByValue = true,
        array $tags = [],
        bool $wholeWordMatch = true
    ): void
    {
        if ($tagsByValue) {
            $column = 'value';
        } else {
            $column = 'id';
        }
        $referenceParam = uniqid('bd_');
        $tagAlias = uniqid('tag_');
        $tagUsageAlias = uniqid('tag_usage_');
        $queryBuilder->leftJoin($incomingTableAlias, 'sb__tag_usage', $tagUsageAlias, "{$tagUsageAlias}.object_id = {$incomingTableAlias}.id and {$tagUsageAlias}.object_ref in (:{$referenceParam}) and {$tagUsageAlias}.lote_deleted is null")
            ->leftJoin($tagUsageAlias, 'sb__tag', $tagAlias, "{$tagAlias}.id = {$tagUsageAlias}.tag_id and {$tagAlias}.lote_deleted is null and {$tagAlias}.is_active = true");

        if (!is_array($reference)) {
            $reference = array_map('trim', explode(',', $reference));
        }
        if (is_array($reference)) {
            $queryBuilder->setParameter($referenceParam, $reference, Connection::PARAM_STR_ARRAY);
        }

        if (!empty($tags)) {
            $sql = '';
            $tagsMaxIndex = count($tags) - 1;
            $tagMatchConstraintType = $wholeWordMatch ? '=' : 'like';
            foreach ($tags as $index => $tag) {
                $trp = uniqid('bd_');
                $sql .= "{$tagAlias}.{$column} {$tagMatchConstraintType} :{$trp}";
                $queryBuilder->setParameter($trp, $wholeWordMatch ? $tag : "%{$tag}%");
                if ($index !== $tagsMaxIndex) {
                    $sql .= ' or ';
                }
            }
            $queryBuilder->andWhere($sql);
        }
    }

    public function update($detailsArr = [])
    {
        $id = isset($detailsArr['id']) ? intval($detailsArr['id']) : false;
        if (!empty($id)) {
            $this->getState()->getWriteDb()->update($this->tableName, $detailsArr, ['id' => $id]);
        }
        return $id;
    }

    public function tagExists($value, $id = 0)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("*")
            ->from('sb__tag', 'a')
            ->where("lower(a.value) = :value")
            ->setParameter("value" , strtolower($value))
            ->andWhere('a.lote_deleted is null');

        if($id) {
            $q->andWhere("id != :id")
                ->setParameter("id", $id);
        }
        return $q->execute()->fetchColumn() > 0;
    }

    /**
     * Find all the objects that use a tag, and return the names of the entities that they are for
     * @access public
     * @param int $tagId
     * @return array
     * */
    private function getUsageTagNameAndReferences($tagId)
    {
        $result = [];
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("distinct u.object_ref")
            ->from('sb__tag_usage', 'u')
            ->where("u.tag_id = :tag_id")
            ->setParameter("tag_id" ,$tagId);
        if($data = $q->execute()->fetchAll(\PDO::FETCH_ASSOC)) {
            foreach($data as $d) {
                $result[$d['object_ref']]  = $this->getState()->getData()->getTagTypeName($d['object_ref'], $d['object_ref']);
            }
        }
        asort($result);
        return $result;
    }

    /**
     * Find all the objects that use a tag, and return the names of the entities that they are for
     * @access public
     * @param int $tagId
     * @return array
     * */
    public function getUsageTagNames($tagId)
    {
        return array_values($this->getUsageTagNameAndReferences($tagId));
    }

    /**
     * Find all the objects that use a tag, and return the names of the entities that they are for
     * @access public
     * @param int $tagId
     * @return array
     * */
    public function getUsageTagReferences($tagId)
    {
        return array_keys($this->getUsageTagNameAndReferences($tagId));
    }

    /**
     *
     * tag options
     *
     * @return array
     */
    public function tagSearchOptions()
    {
        $options = [];
        $options['value'] = 'asc';
        return $options;
    }

    /**
     * Tag search clause
     *
     * @param string $objectRef
     * @return array
     */
    public function tagSearchClauses($objectRef)
    {
        $clauses = [];

        $ids = [];
        $usageObj = new TagUsageModel($this->getState());
        $list = $usageObj->listByField('object_ref', $objectRef);
        if (!empty($list)) {
            foreach ($list as $value) {
                $ids[] = $value['tag_id'];
            }
        }

        $contextObj = new TagContextModel($this->getState());
        $contextList = $contextObj->listByField('object_ref', $objectRef);
        if (!empty($contextList)) {
            foreach ($contextList as $value) {
                $ids[] = $value['tag_id'];
            }
        }

        $sitewideList = $contextObj->listByField('object_ref', 'sitewide');
        if (!empty($sitewideList)) {
            foreach ($sitewideList as $value) {
                $ids[] = $value['tag_id'];
            }
        }

        //if (!empty($ids)) {
            $clause['field'] = 'id';
            $clause['operator'] = 'in';
            $clause['value'] = array_unique($ids);
            $clause['data_type'] = \PDO::PARAM_STR;
            $clauses[] = $clause;
        //}

        $clause['field'] = 'is_active';
        $clause['operator'] = 'equals';
        $clause['value'] = 1;
        $clause['data_type'] = \PDO::PARAM_INT;
        $clauses[] = $clause;

        return $clauses;
    }

    public function getObjectIdsByTagId($tagId, $objectRef) {
        $q = $this->getReadDb()->createQueryBuilder();

        $q->select('t.tag_id, t.object_id, t.object_ref')
            ->from('sb__tag_usage', 't')
            ->where('t.object_ref = :object_ref');

            if ($tagId) {
                $q->andWhere('t.tag_id = :tag_id')
                ->setParameter('tag_id', $tagId);
            }

            $q->setParameter('object_ref', $objectRef);

        $result = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);

        return $result;
    }

    public function getMasterSharingTags($accountRef = null) {
        if (!$accountRef) {
            $accountRef = $this->getState()->accountReference;
        }
        $q = $this->getReadDb()->createQueryBuilder();

        $q->select('t.id, t.display_value')
            ->from($this->tableName, 't')
            ->leftJoin('t', 'sb__tag_context', 'u', 't.id = u.tag_id')
            ->where('t.lote_deleted is null')
            ->andWhere('t.is_active = 1')
            ->andWhere('t.account_ref = :accountRef')
            ->andWhere('u.lote_deleted is null')
            ->andWhere('u.object_ref = :tag_ref')
            ->setParameter('accountRef', $accountRef)
            ->setParameter('tag_ref', "sb_master_account.account")
        ->addOrderBy("t.display_value")
        ->addOrderBy("t.value");

        return $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
    }

}
