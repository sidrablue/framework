<?php
namespace BlueSky\Framework\Model\Tag;

use BlueSky\Framework\Object\Model\Base;
use Doctrine\DBAL\Connection;

/**
 *
 * */
class Context extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__tag_context';

    /**
     * @param $tagId
     */
    public function unlinkAllContext($tagId)
    {
        $this->getWriteDb()->delete($this->tableName, ['tag_id' => $tagId]);
    }

    /**
     * @param $tagId
     * @return array
     */
    public function getContextByTag($tagId)
    {
        $result = [];
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('ce.object_ref')
            ->from($this->tableName, 'ce')
            ->leftJoin('ce', 'sb__tag', 'c', 'c.id = ce.tag_id')
            ->where('ce.tag_id = :tag_id')
            ->setParameter('tag_id', $tagId);
        $q->andWhere("c.lote_deleted is null");
        $q->andWhere("ce.lote_deleted is null");

        $s = $q->execute();
        $data = $s->fetchAll(\PDO::FETCH_ASSOC);
        if(!empty($data)){
            foreach($data as $value){
                $result[] = $value['object_ref'];
            }
        }
        return $result;
    }

    /**
     * @param $tagId
     * @param $objectRef
     * @return bool
     */
    public function objectHasTagContext($tagId, $objectRef)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('count(*)')
            ->from($this->tableName, 'ce')
            ->where('ce.tag_id = :tag_id')
            ->setParameter('tag_id', $tagId)
            ->andWhere('ce.object_ref = :object_ref')
            ->setParameter('object_ref', $objectRef)
            ->andWhere("ce.lote_deleted is null");
        $s = $q->execute();
        return $s->fetchColumn() > 0;
    }

    /**
     * Retrieve a search query using the supplied parameters
     *
     * @access public
     * @param array $params - Search parameters
     * @return \Doctrine\DBAL\Query\QueryBuilder
     */
    public function getSearchQuery($params)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('tc.*')
            ->from($this->getTableName(), 'tc')
            ->leftJoin('tc', 'sb__tag', 't', 'tc.tag_id = t.id')
            ->where('t.lote_deleted is null')
            ->andWhere('tc.lote_deleted is null');

        if(isset($params['object_ref']) && $params['object_ref']) {
            $q->andWhere('tc.object_ref = :objectRef')
                ->setParameter('objectRef', $params['object_ref']);

            if(isset($params['object_id']) && $params['object_id']) {
                $q->andWhere('tc.object_id = :objectId')
                    ->setParameter('objectId', $params['object_id']);
            }
        }

        return $q;
    }

    public function getTagsByContext($context)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('t.*')
            ->from($this->tableName, 'tc')
            ->leftJoin('tc', 'sb__tag', 't', 't.id = tc.tag_id');
        $q->andWhere("t.lote_deleted is null");
        $q->andWhere("tc.lote_deleted is null");

        if(!empty($context)){
            if(is_array($context)){
                $q->andWhere('tc.object_ref in (:ref)')
                    ->setParameter('ref', $context, Connection::PARAM_STR_ARRAY);
            }else{
                $q->andWhere('tc.object_ref = :ref')
                    ->setParameter('ref', $context);
            }
        }
        $q->groupBy('t.id');
        $s = $q->execute();
        $result = $s->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }

}
