<?php
namespace BlueSky\Framework\Model\Tag;

use Doctrine\DBAL\Connection;
use BlueSky\Framework\Entity\Tag\Usage as TagUsageEntity;
use BlueSky\Framework\Object\Model\Base;
use BlueSky\Framework\Util\Arrays;

/**
 *
 * */
class Usage extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__tag_usage';

    /**
     * Get the tags used by an object
     * @access public
     * @param string $objectRef - the reference of the object
     * @param array|int $objectIds - the ID of the object
     * @return array
     * */
    public function getObjectTagArray($objectRef, $objectIds)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('u.id, u.object_id, t.value, t.colour_code, t.id as tag_id, t.display_value')
            ->from('sb__tag', 't')
            ->leftJoin('t', 'sb__tag_usage', 'u' , 'u.tag_id = t.id')
            ->andWhere('u.object_ref = :object_ref')
            ->setParameter('object_ref', $objectRef);
        if(is_array($objectIds) && count($objectIds) == 1) {
            $objectIds = array_pop($objectIds);
            $q->andWhere('u.object_id = :object_id');
            $q->setParameter('object_id', $objectIds);
        } elseif (is_array($objectIds) && count($objectIds) > 0) {
            $q->andWhere('u.object_id IN (:object_id)');
            $q->setParameter('object_id', $objectIds, Connection::PARAM_INT_ARRAY);
        } else {
            $q->andWhere('u.object_id = :object_id');
            $q->setParameter('object_id', $objectIds);
        }
        $q->orderBy("u.object_id", "asc");

        $tags = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
        return $tags;
//        foreach($tags as $t) {
//            $result[] = $t['value'];
//        }
//        return $result;
    }

    /**
     * Delete the usages of all tags for a particular object
     * @param string $objectRef - the reference of the object
     * @param string $fieldRef - the field within the object
     * @param int $objectId - the ID of the object that the usage is for
     * @return void
     */
    public function deleteUsage($objectRef, $fieldRef = '', $objectId = 0)
    {
        $conditions = ['object_ref' => $objectRef];
        if ($objectId) {
            $conditions['object_id'] = $objectId;
        }
        if(!empty($fieldRef)){
            $conditions['field_ref'] = $fieldRef;
        }
        $this->getWriteDb()->delete($this->getTableName(), $conditions);
    }

    /**
     * Add a new tag usage instance
     * @access public
     * @param integer $tagId - the tag ID that the usage is for
     * @param string $objectRef - the reference of the object that the usage is for
     * @param string $fieldRef - the field within the object that the usage is for
     * @param int $objectId - the ID of the object that the usage is for
     * @return TagUsageEntity
     */
    public function addUsage($tagId, $objectRef, $fieldRef = '', $objectId = 0)
    {
        $t = new TagUsageEntity($this->getState());
        $t->setWriteDb($this->getWriteDb());
        $t->setReadDb($this->getReadDb());
        $t->tag_id = $tagId;
        $t->object_ref = $objectRef;
        $t->object_id = $objectId;
        $t->field_ref = $fieldRef;
        $t->save();
        return $t;
    }

    /**
     * Sync the usage of a set of tags for an object
     * @access public
     * @param array $tags - an array of tag names
     * @param string $objectRef - the reference of the object that the usage is for
     * @param string $fieldRef - the field within the object that the usage is for
     * @param int $objectId - the ID of the object that the usage is for
     * @return void
     */
    public function syncUsage($tags, $objectRef, $fieldRef = '', $objectId = 0)
    {
        $tagData = [];
        $t = new Tag($this->getState());
        $t->setWriteDb($this->getWriteDb());
        $t->setReadDb($this->getReadDb());

        $caseSensitive = $this->getState()->getSettings()->get('tag.case_sensitive', true);
        if(!$caseSensitive && is_array($tags)) {
            $tags = Arrays::toLowerUnique($tags);
        }

        if(gettype($tags) == 'array' && count(array_filter($tags))>0){
            $tagData = $t->getOrCreateTags($tags, $caseSensitive);
        }
        $this->deleteUsage($objectRef, $fieldRef, $objectId);

        if(!empty($tagData)){
            foreach ($tagData as $t) {
                $this->addUsage($t['id'], $objectRef, $fieldRef, $objectId);
            }
        }
    }
    public function removeAppTags($object){

            $q = $this->getReadDb()->createQueryBuilder();
            $data = $q->select('t.id')
                ->from('sb__tag', 't')
                ->where('t.value = "app"')
                ->andWhere("t.lote_deleted is null");
            $query = $data->execute();
            $result = $query->fetch(\PDO::FETCH_ASSOC);
            $this->getWriteDb()->delete($this->getTableName(), ["tag_id" => $result['id'], "object_ref" => $object]);
            return $result['id'];

    }
    public function setDefaultTag($objectRef, $objectId = 0, $id)
    {
        $t = new Tag($this->getState());
        $value = $t->getTagsByObjectId($objectId, $objectRef);
        if($value && count($value) > 0){
            $obj_ref = "sb_event";
            $obj_id = $id;
            $this->addUsage($value[0]['tag_id'], $obj_ref , $obj_ref, $obj_id );
        }
    }
    public function getCalendarTag($cal_id = false){
        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select('t.*, te.colour_code, te.weighting')
            ->from($this->tableName, 't')
            ->leftJoin('t', 'sb__tag', 'te' , 't.tag_id = te.id')
            ->andWhere('t.object_ref = "default_calendar"');
        if($cal_id){
            $data->andWhere('t.object_id = :cal_id')
                ->setParameter('cal_id', $cal_id);
        }
        $query = $data->execute();
        $result = $query->fetch(\PDO::FETCH_ASSOC);
        return $result;
    }

    public function cleanupTags()
    {
        $this->getWriteDb()->delete("sb__tag", ['value' => 'amel']);
        $this->getWriteDb()->delete("sb__tag", ['value' => 'leigh']);
        $this->getWriteDb()->delete("sb__tag", ['value' => 'riz']);

        $t = new Tag($this->getState());
        $tags = $t->getAll();
        foreach($tags as $t) {
            $foundTags = $this->getTagsByValue($t['value']);
            if($foundTags && count($foundTags) > 1) {
                dump('found multiple for '.$t['value']);
                $this->combineTags($foundTags);
            }
        }
        $this->getWriteDb()->exec("delete t1 from sb__tag t1, sb__tag t2 where t1.id > t2.id and lower(t1.value) = lower(t2.value)");
        $this->getWriteDb()->exec("delete t1 from sb__tag_usage t1, sb__tag_usage t2 where t1.id > t2.id and (t1.tag_id = t2.tag_id and t1.`object_id` = t2.`object_id` and t1.`object_ref` = t2.`object_ref`) ");
        $this->getWriteDb()->exec("delete from sb__tag where id not in (select distinct tag_id from sb__tag_usage)");
    }

    private function combineTags($tags)
    {
        $firstTag = $tags[0];
        $remainingTags = array_slice($tags, 1);
        foreach($remainingTags as $t) {
            $this->getReadDb()->update("sb__tag_usage", ['tag_id' => $firstTag['id']], ['tag_id' => $t['id']]);
        }
    }

    /**
     * @return array|false
     * */
    private function getTagsByValue($value)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("*")->from("sb__tag", "t")->where("lower(value) = :value")->setParameter("value", strtolower($value));
        return $q->execute()->fetchAll();
    }

    public function getUsageList()
    {
        $result = [];
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('u.object_ref')
            ->from('sb__tag_usage', 'u')
            ->rightJoin('u', 'sb__tag', 't' , 't.id = u.tag_id')
            ->where('u.lote_deleted is null')
            ->andWhere('t.lote_deleted is null')
            ->groupBy('u.object_ref');
        $tags = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
        foreach($tags as $t) {
            $result[] = $t['object_ref'];
        }
        return array_filter($result);
    }

    public function deleteUsages($tagId)
    {
        return $this->getState()->getWriteDb()->delete($this->tableName, ['tag_id' => $tagId]);
    }

    public function isTagLinked($tagId, $objectId, $objectRef)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select('count(*) as count')
            ->from($this->tableName, 't')
            ->andWhere('t.tag_id = :tag_id')
            ->setParameter('tag_id', $tagId)
            ->andWhere('t.object_id = :object_id')
            ->setParameter('object_id', $objectId)
            ->andWhere('t.object_ref = :object_ref')
            ->setParameter('object_ref', $objectRef)
            ->andWhere("t.lote_deleted is null");

        $query = $data->execute();
        $result = $query->fetch(\PDO::FETCH_ASSOC);
        return $result['count'] > 0;
    }

    public function deleteTagLinks($tagId, $eventId , $objectRef)
    {
        return $this->getState()->getWriteDb()->delete($this->tableName, ['tag_id' => $tagId, 'object_id' => $eventId, 'object_ref' => $objectRef]);
    }

    /**
     * Retrieve a search query using the supplied parameters
     *
     * @access public
     * @param array $params - Search parameters
     * @return \Doctrine\DBAL\Query\QueryBuilder
     */
    public function getSearchQuery($params)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('tu.*')
            ->from($this->getTableName(), 'tu')
            ->leftJoin('tu', 'sb__tag', 't', 'tu.tag_id = t.id')
            ->where('t.lote_deleted is null')
            ->andWhere('tu.lote_deleted is null');

        if(isset($params['object_ref']) && $params['object_ref']) {
            $q->andWhere('tu.object_ref = :objectRef')
                ->setParameter('objectRef', $params['object_ref']);

            if(isset($params['object_id']) && $params['object_id']) {
                $q->andWhere('tu.object_id = :objectId')
                    ->setParameter('objectId', $params['object_id']);
            }
        }

        return $q;
    }

    public function getEventUsage($calendar_id){
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('distinct t.*')
            ->from($this->getTableName(), 'tu')
            ->leftJoin('tu', 'sb__tag', 't', 'tu.tag_id = t.id')
            ->leftJoin("tu", 'sb_event', 'e', 'e.id = tu.object_id')
            ->leftJoin("e", "sb_calendar_event", "ce", "ce.event_id = e.id")
            ->where('t.lote_deleted is null')
            ->andWhere('tu.lote_deleted is null')
            ->andWhere("ce.lote_deleted is null")
            ->andWhere("e.lote_deleted is null")
            ->andWhere("tu.object_ref = 'sb_event'")
            ->andWhere("ce.calendar_id = :cal_id")
            ->setParameter("cal_id", $calendar_id)
            ->orderBy("t.weighting", "desc");
        $query = $q->execute();
        $result = $query->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }
}
