<?php
namespace BlueSky\Framework\Model;

use BlueSky\Framework\Object\Model\Base;

/**
 * Model class for a file
 *
 * @package BlueSky\Framework\Model
 */
class App extends Base
{

    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__app';

    /**
     * Get all apps
     *
     * @param int $maxResults
     * @return array
     */
    public function getAll($maxResults = 1000)
    {
        $result = [];
        $q = $this->getState()->getReadDb()->createQueryBuilder();
        $q->select("*")
            ->from("sb__app", "a")
            ->where("a.lote_deleted is null")
            ->orderBy("priority", "asc")
            ->setMaxResults(1000);
        foreach($q->execute()->fetchAll(\PDO::FETCH_ASSOC) as $row) {
            $result[] = $row;
        }
        return $result;
    }

    /**
     * Retrieve search query
     *
     * @access public
     * @param array $phrase
     * @param array  $ignoreIds
     * @param int  $ignoreType
     * @return \Doctrine\DBAL\Query\QueryBuilder
     */

    public function getSearchQuery($phrase = "")
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("a.*")
            ->from($this->getTableName(), 'a')
            ->andWhere("a.lote_deleted is null");
        if (!empty($phrase)) {
            $q = $this->getPhraseSplitQuery($q, $phrase, ['a.name','a.reference']);
        }
        return $q;

    }

}
