<?php
///**
//
//
// */
//namespace BlueSky\Framework\Model\Cf;
//
//use BlueSky\Framework\Object\Data\Field\Type\BaseOption;
//use BlueSky\Framework\Object\Model\Base;
//
//class CfFieldOption extends Base
//{
//
//    protected $tableName = 'sb__cf_field_option';
//
//    /**
//     * Get field details by group
//     * @param int $fieldId
//     * @param int $sortOrder should be from the available SORT_OPTIONS_* consts
//     * @see \BlueSky\Framework\Object\Data\Field\Type\BaseOption
//     * @access public
//     * @return array|false
//     * */
//    public function getFieldOptions($fieldId, $sortOrder = BaseOption::SORT_OPTIONS_DEFAULT)
//    {
//        $q = $this->getReadDb()->createQueryBuilder();
//        $data = $q->select('f.*')
//            ->from('sb__cf_field_option', 'f')
//            ->where('f.field_id = :id')
//            ->setParameter('id', $fieldId)
//            ->andWhere("f.lote_deleted is null");
//
//        $sortField = 'sort_order';
//        $sortDirection = 'asc';
//        if ($sortOrder == BaseOption::SORT_OPTIONS_ALPHA_ASC) {
//            $sortField = 'value';
//        } elseif ($sortOrder == BaseOption::SORT_OPTIONS_ALPHA_DESC) {
//            $sortField = 'value';
//            $sortDirection = 'desc';
//        } elseif ($sortOrder == BaseOption::SORT_OPTIONS_WEIGHTING_ASC) {
//            $sortField = 'weighting';
//        } elseif ($sortOrder == BaseOption::SORT_OPTIONS_WEIGHTING_DESC) {
//            $sortField = 'weighting';
//            $sortDirection = 'desc';
//        }
//
//        $q->orderBy('f.' . $sortField, $sortDirection);
//
//        return $query = $data->execute()->fetchAll(\PDO::FETCH_ASSOC);
//    }
//
//}
