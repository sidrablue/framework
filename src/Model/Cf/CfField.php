<?php
///**
// 
// 
// */
//namespace BlueSky\Framework\Model\Cf;
//
//use Doctrine\DBAL\Connection;
//use BlueSky\Framework\Object\Model\Base;
//
//class CfField extends Base
//{
//
//    protected $tableName = 'sb__cf_field';
//
//    /**
//     * Get the data for a group that this field belongs to
//     * @param int $fieldId
//     * @access public
//     * @return array|false
//     * */
//    public function getGroup($fieldId)
//    {
//        $q = $this->getReadDb()->createQueryBuilder();
//        $data = $q->select('g.*')
//            ->from('sb__cf_field', 'f')
//            ->leftJoin('f', 'sb__cf_group', 'g', 'g.id = f.group_id')
//            ->where('f.id = :id')
//            ->setParameter('id', $fieldId);
//        return $query = $data->execute()->fetch(\PDO::FETCH_ASSOC);
//    }
//
//    /**
//     * Get the next sort order for a field group
//     * @param int $groupId
//     * @return int
//     * */
//    public function getNextSortOrder($groupId)
//    {
//        $q = $this->getReadDb()->createQueryBuilder();
//        $data = $q->select('max(f.sort_order) + 1')
//            ->from('sb__cf_field', 'f')
//            ->where('f.group_id = :id')
//            ->setParameter('id', $groupId);
//        return max(1,$data->execute()->fetchColumn());
//    }
//
//    /**
//     * Get field details by group
//     * @param int $groupId
//     * @access public
//     * @return array|false
//     * */
//    public function getGroupFields($groupId)
//    {
//        $q = $this->getReadDb()->createQueryBuilder();
//        $data = $q->select('f.*')
//            ->from('sb__cf_field', 'f')
//            ->where('f.group_id = :id')
//            ->setParameter('id', $groupId);
//        return $query = $data->execute()->fetchAll(\PDO::FETCH_ASSOC);
//    }
//
//    /**
//     * Get a list of fields from a given set of ID's
//     * @param array $fieldIds
//     * @access public
//     * @return array|false
//     * */
//    public function getFieldsByIds($fieldIds)
//    {
//        $result = [];
//        if(is_array($fieldIds) && count($fieldIds) > 0) {
//            $q = $this->getReadDb()->createQueryBuilder();
//            $data = $q->select('f.*')
//                ->from('sb__cf_field', 'f')
//                ->where('f.id in (:ids)')
//                ->setParameter('ids', $fieldIds, Connection::PARAM_STR_ARRAY);
//            if ($data = $data->execute()->fetchAll(\PDO::FETCH_ASSOC)) {
//                foreach ($data as $fieldData) {
//                    $cfEntity = new \BlueSky\Framework\Entity\Cf\CfField($this->getState());
//                    $cfEntity->setData($fieldData);
//                    $result[] = $cfEntity;
//                }
//            }
//        }
//        return $result;
//    }
//
//    /**
//     * Get a list of fields that are "option" types, for a specified object reference
//     * @access public
//     * @param string|array $objectRef
//     * @return array
//     * */
//    public function getOptionFieldsByObjectRef($objectRef)
//    {
//        $result = [];
//        $q = $this->getReadDb()->createQueryBuilder();
//        $q->select('f.*, o.reference as object_reference')
//            ->from('sb__cf_field', 'f')
//            ->leftJoin("f", "sb__cf_group", "g", "g.id = f.group_id")
//            ->leftJoin("g", "sb__cf_object", "o", "o.id = g.object_id")
//            ->where("f.lote_deleted is null")
//            ->andWhere("g.lote_deleted is null")
//            ->andWhere("o.lote_deleted is null")
//            ->andWhere("f.field_type in (:option_field_types)")
//            ->setParameter("option_field_types", $this->getOptionFieldTypes(), Connection::PARAM_STR_ARRAY);
//        if(is_array($objectRef)) {
//            $q->andWhere("o.reference in (:object_references)")
//                ->setParameter("object_references", $objectRef, Connection::PARAM_STR_ARRAY);
//        }
//        $q->orderBy("f.name", "asc")->addOrderBy("f.id", "asc");
//        $s = $q->execute();
//        while($row = $s->fetch()) {
//            $result[$row['object_reference']][] = $row;
//        }
//        return $result;
//    }
//
//    /**
//     * Get the list of filed types that correspond to "option" type fields
//     * @access public
//     * @return array
//     * */
//    public function getOptionFieldTypes()
//    {
//        return ['single_select', 'multi_select', 'radio_button', 'checkbox'];
//    }
//
//}
