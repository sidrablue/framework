<?php
namespace BlueSky\Framework\Model\Cf;

use BlueSky\Framework\Object\Model\Base;

class CfGroup extends Base
{

    protected $tableName = 'sb__cf_group';

    /**
     * Delete an Object
     *
     * @param int $id - the user to delete
     * @param boolean $strongDelete - true if row is to be deleted from table
     * @return void
     */
    public function delete($id, $strongDelete = false)
    {
        //@todo delete any custom fields associated with this group
        if ($fields = $this->getCfFields($id)) {
            $fm = new CfField($this->getState());
            foreach ($fields as $v) {
                $fm->delete($v['id'], $strongDelete);
            }
        }
        parent::delete($id, $strongDelete);
    }

    /**
     * Get all the fields in a custom field group
     * @param int $groupId
     * @return array
     * */
    public function getCfFields($groupId, $active_only = false)
    {
        $result = [];
        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select('f.*')
            ->from('sb__cf_field', 'f')
            ->where('f.cf_group_id = :id')
            ->andWhere('lote_deleted is null')
            ->setParameter('id', $groupId)
            ->addOrderBy('f.sort_order', 'asc');
        if($active_only){
            $data->andWhere("f.active = 1");
        }
        $query = $data->execute();
        while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
            $result[] = $row;
        }
        return $result;
    }

    /**
     * Get the data for an object that this group belongs to
     * @param int $groupId
     * @access public
     * @return array|false
     * */
    public function getObject($groupId)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select('o.*')
            ->from('sb__cf_group', 'g')
            ->leftJoin('g', 'sb__cf_object', 'o', 'o.id = g.object_id')
            ->where('g.id = :id')
            ->setParameter('id', $groupId);
        return $query = $data->execute()->fetch(\PDO::FETCH_ASSOC);
    }

    /**
     * Get the next sort order for an object group
     * @param int $objectId
     * @return int
     * */
    public function getNextSortOrder($objectId)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select('max(g.sort_order) + 1')
            ->from('sb__cf_group', 'g')
            ->where('g.object_id = :id')
            ->setParameter('id', $objectId);
        return max(1, $data->execute()->fetchColumn());
    }

    /**
     * Get the next group ID based on the sort order and a start group ID
     * @param int $startGroupId
     * @return int
     * */
    public function getNextOrderGroupId($startGroupId, $ignoreDeleted = true)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('g.id')
            ->from('sb__cf_group', 'g')
            ->leftJoin('g', 'sb__cf_group', 'g2', 'g2.object_id = g.object_id')
            ->where('g2.id = :id')
            ->setParameter('id', $startGroupId)
            ->andWhere('g.sort_order > g2.sort_order')
            ->orderBy('g.sort_order', 'asc')
            ->setMaxResults(1);
        if($ignoreDeleted == true) {
            $q->andWhere('g.lote_deleted is null');
        }
        return $q->execute()->fetchColumn();
    }

    /**
     * Get the next sort order for an object group
     * @param int|string $objectId
     * @return array|int
     * */
    public function getObjectGroups($objectId, $hide_deleted = false)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select('g.*')
            ->from('sb__cf_group', 'g')
            ->leftJoin('g', 'sb__cf_object', 'o', 'o.id = g.object_id');
        if (is_numeric($objectId)) {
            $q->where('o.id = :id')
                ->setParameter('id', $objectId);
        } else {
            $q->where('o.reference = :ref')
                ->setParameter('ref', $objectId);
        }
        if($hide_deleted){
            $q->andWhere("g.lote_deleted is null");
        }
        return $data->execute()->fetchAll(\PDO::FETCH_ASSOC);
    }

}
