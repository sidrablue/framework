<?php
///**
//
//
// */
//
//namespace BlueSky\Framework\Model\Cf;
//
//use BlueSky\Framework\Entity\Cf\CfFieldOption as CfFieldOptionEntity;
//use BlueSky\Framework\Entity\User\CfValue as CfValueEntity;
//use BlueSky\Framework\Entity\User\CfValueOptionSelected as CfValueOptionEntity;
//use BlueSky\Framework\Object\Model\Base;
//use BlueSky\Framework\Util\Arrays;
//
//class CfValueOptionSelected extends Base
//{
//
//    protected $tableName = 'sb__cf_value_option';
//
//    /**
//     * Get the selected options for a specified value
//     * @access public
//     * @param int $valueId
//     * @return array|false
//     * */
//    public function getValueOptions($valueId)
//    {
//        $q = $this->getReadDb()->createQueryBuilder();
//        $q->select("o.*")
//            ->from($this->getTableName(), 'o')
//            ->where("o.value_id = :value_id")
//            ->setParameter("value_id", $valueId)
//            ->andWhere("o.lote_deleted is null");
//        return $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
//    }
//
//    /**
//     * Sync a set of specified options for a selected value
//     * @param CfValueEntity $cfValue
//     * @param array|int $selectedOptions
//     * @param bool $isMatrix
//     * @return void
//     * */
//    public function syncOptions(CfValueEntity $cfValue, $selectedOptions, $isMatrix = false, $allowTextValueMatch = false)
//    {
//        $currentOptions = $this->getValueOptions($cfValue->id);
//        if ($isMatrix) {
//            foreach ($selectedOptions as $rows => $columns) {
//                $cfOptionRow = new CfFieldOptionEntity($this->getState());
//                $cfOptionRow->load($rows);
//                foreach ($columns as $colId => $colValue) {
//                    if ($colValue) {
//                        $cfOptionCol = new CfFieldOptionEntity($this->getState());
//                        $cfOptionCol->load($colId);
//                        $e = new CfValueOptionEntity($this->getState());
//                        $e->setTableName($cfValue->getTableName() . '_option_selected');
//                        $e->loadByFields(['value_id' => $cfValue->id, 'option_id_col' => $cfOptionRow->id, 'option_id' => $cfOptionCol->id]);
//                        $e->value_id = $cfValue->id;
//                        $e->option_id = $cfOptionRow->id;
//                        $e->option_id_col = $cfOptionCol->id;
//                        $e->value_string = $cfOptionRow->value;
//                        $e->value_string_col = $cfOptionCol->value;
//                        $e->value_text = $colValue;
//                        $e->save();
//                    }
//                }
//            }
//            foreach ($currentOptions as $o) {
//                if (!(isset($selectedOptions[$o['option_id']]) && isset($selectedOptions[$o['option_id']][$o['option_id_col']]))) {
//                    $e = new CfValueOptionEntity($this->getState());
//                    $e->setTableName($cfValue->getTableName() . '_option_selected');
//                    if ($e->loadByFields(['value_id' => $cfValue->id, 'option_id' => $o['id']])) {
//                        $e->delete(true);
//                    }
//                }
//            }
//        } else {
//            /*if (is_numeric($selectedOptions) || is_string($selectedOptions)) {
//                $selectedOptions = [$selectedOptions];
//            }
//            if(count($selectedOptions) == 1 && $allowTextValueMatch) {
//                if($optionData = $this->getOptionFromIdOrValue($cfValue->field_id, $selectedOptions[0])) {
//                    $selectedOptions = [$optionData['id']];
//                }
//            }*/
//            if(count($selectedOptions) > 0) {
//                foreach ($selectedOptions as $o) {
////                    $cfOption = new CfFieldOptionEntity($this->getState());
////                    $cfOption->load($o['id']);
//                    $e = new CfValueOptionEntity($this->getState());
//                    $e->setTableName($cfValue->getTableName() . '_option_selected');
//                    $e->loadByFields(['value_id' => $cfValue->id, 'option_id' => $o['id']]);
//                    $e->value_id = $cfValue->id;
//                    $e->option_id = $o['id'];
////                    $e->value_text = $e->value_string = $cfOption->value;
//                    $e->value_text = $e->value_string = $o['value'];
//                    $e->save();
//                }
//                foreach ($currentOptions as $o) {
//                    if (!in_array($o['option_id'], Arrays::getFieldValuesFrom2dArray($selectedOptions, 'id'))) {
//                        $e = new CfValueOptionEntity($this->getState());
//                        $e->setTableName($cfValue->getTableName() . '_option_selected');
//                        if ($e->loadByFields(['value_id' => $cfValue->id, 'option_id' => $o['option_id']])) {
//                            $e->delete();
//                        }
//                    }
//                }
//            }
//        }
//    }
//
//    /**
//     * Get an option from its ID or its value if one has been given instead
//     * @access public
//     * @param int $fieldId
//     * @param int|string $idOrValue
//     * @return false|array
//     * */
//    public function getOptionFromIdOrValue($fieldId, $idOrValue) {
//        $result = false;
//        $e = new CfFieldOptionEntity($this->getState());
//        if(is_numeric($idOrValue) && $e->load($idOrValue)) {
//            $result = $e->getData();
//        }
//        elseif($e->loadByFields(['field_id' => $fieldId, 'value' => $idOrValue])) {
//            $result = $e->getData();
//        }
//        return $result;
//    }
//
//    public function valueDeselected($valueId, $tableName)
//    {
//        $q = $this->getState()->getReadDb()->createQueryBuilder();
//        $ids = Arrays::getFieldValuesFrom2dArray($q->select('id')
//            ->from('sb__user__cf_value_option_selected')
//            ->where('lote_deleted is null')
//            ->andWhere('value_id = :valueId')
//            ->setParameter('valueId', $valueId)
//            ->execute()->fetchAll(\PDO::FETCH_ASSOC), 'id');
//        foreach ($ids as $id) {
//            $cvo = new CfValueOptionEntity($this->getState());
//            $cvo->setTableName($tableName . '_option_selected');
//            if ($cvo->load($id)) {
//                $cvo->delete();
//            }
//        }
//    }
//
//}
