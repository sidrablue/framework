<?php
/**
 * Copyright (c) BlueSky Digital Labs Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Model\Schema;

use BlueSky\Framework\Object\Entity\VirtualEntity;
use BlueSky\Framework\Object\Model\Base;
use BlueSky\Framework\Entity\Schema\Entity as SchemaEntity;
use BlueSky\Framework\Object\Entity\Base as BaseEntity;
use BlueSky\Framework\Entity\Schema\Field as FieldEntity;
use BlueSky\Framework\Schema\FieldOptions;
use BlueSky\Framework\Schema\SchemaValidation;
use BlueSky\Framework\Util\Strings;
use Doctrine\DBAL\Query\QueryBuilder;

/**
 * Base class for Item
 * @package Lote\App\Staff\Entity
 * @dbEntity true
 */
class Entity extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__schema_entity';


    /**
     * Get the list of custom field groups that the entity is currently linked to
     * @access public
     * @param string $entityRef - the reference of the parent entity to get children objects for
     * @return SchemaEntity[]
     *
     * todo: delete this function since the parent entity will already load its children
     * */
    public function getEntityChildren($entityRef)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('*')
            ->from('sb__schema_entity', 'e')
            ->andWhere('e.reference like :parentRef')
            ->setParameter('parentRef', "%$entityRef\\\\%")
            ->andWhere('e.lote_deleted is null');
        return $this->arraysToObjects($q->execute()->fetchAll(\PDO::FETCH_ASSOC), SchemaEntity::class);
    }

    /**
     * @param string $entityName - This is the name of the entity that the user inputs from at the form
     * @param string|null $parentReference - This is the parent entity reference
     * @param bool $sanitiseName - Whether to automatically remove any illegal characters or not
     * @return string - reference
     */
    public function createReferenceFromName(string $entityName, ?string $parentReference = null, bool $sanitiseName = false): string
    {
        $reference = ucwords($entityName);
        if ($sanitiseName) {
            $reference = preg_replace('/(^[^a-zA-Z]*)|([^a-zA-Z\d])/', '', $entityName);
            $reference = ucfirst($reference);
        } else {
            $reference = str_replace(' ', '', $reference);
        }
        if (!empty($parentReference)) {
            $reference = $parentReference . '\\' . $reference;
        }

        $referenceLength = strlen($reference);
        if ($referenceLength > 44) { /// 64 (MariaDB column name length max) - 20 (4x unique id opportunity)
            $lengthDelta = $referenceLength - 44;
            $reference = substr($reference, 0, 44) . substr($reference, $lengthDelta > 4 ? -4 : -$lengthDelta);
        }
        return $reference;
    }

    /**
     * This should only be used with Base classes. Virtual classes should not use this
     * @param string $tableName
     * @return string
     */
    public function createReferenceFromTableName(string $tableName): string
    {
        $isVirtual = strrpos($tableName, '__c');
        $reference = $tableName;
        if ($isVirtual !== false) {
            $reference = substr($reference, 0, -3);
        }
        $reference = ucwords(substr($reference, 3), '_');
        $reference = str_replace('___', '\\', $reference);
        $reference = str_replace('_', '', $reference);
        return $reference;
    }

    public static function createNameFromTableName(string $tableName): string
    {
        $name = ucwords(substr($tableName, 3), '_');
        $name = str_replace('_', ' ', $name);
        return $name;
    }

    /**
     * @param string $entityReference
     * @return string - Class name excluding the Namespace portion
     */
    public function createClassNameFromReference(string $entityReference): string
    {
        $className = strrchr($entityReference, '\\');
        if ($className === false) {
            $className = $entityReference;
        }
        $className = str_replace('\\', '', $className); /// This makes sure that the \ is not in the name
        $lowerCasedClassName = strtolower($className);
        while (in_array($lowerCasedClassName, SchemaValidation::reservedPhpKeywords)) {
            $className .= Strings::generateRandomString(4);
            $lowerCasedClassName = strtolower($className);
        }
        return $className;
    }

    public function createTableNameFromReference(string $reference): string
    {
        $tableName = preg_replace("/([A-Z])/", "_$1", $reference);
        $tableName = substr($tableName, 1);
        $tableName = strtolower($tableName);
        $tableName = str_replace('\\', '__', $tableName);
        if (strlen($tableName) > 58) {
            $tableName = substr($tableName, 0, 58);
        }
        return $tableName;
    }

    /**
     * fixme: remove this function and replace the entity link to be the reference of the entity (minus the backslash) instead
     * @param string $tableName
     * @return string
     * @deprecated
     */
    public function createEntityLinkReference(string $tableName): string
    {
        $lastPos = strrpos($tableName, '__c');
        $firstPos = strrpos($tableName, '___');
        $prefix = '_';
        if ($firstPos === false) {
            $firstPos = 3;
            if (strpos($tableName, 'sb__') === 0) {
                $firstPos++; /// if its a framework class, we want to remove the extra _ from the tablename
            }
        } else {
            $firstPos += 3;
        }

        if ($lastPos === false) {
            $lastPos = strlen($tableName) - $firstPos;
            $prefix .= '_';
        } else {
            $lastPos = -3;
        }
        return $prefix . substr($tableName, $firstPos, $lastPos);
    }

    public function getVirtualAccountEntityNamespace(): string
    {
        $accountReference = $this->getState()->accountReferenceFormatted;
        if (ctype_digit($accountReference[0])) {
            $systemName = $this->getState()->getConfig()->get('system.name');
            $accountReference = $systemName . $accountReference;
        }
        return APP_NAMESPACE . 'Virtual\Entity\\' . $accountReference . '\\';
    }

    public function getSearchQuery(?string $childOf = null, ?string $phrase = null, array $params = []): QueryBuilder
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("se.*")
            ->from($this->getTableName(), 'se')
            ->andWhere("se.lote_deleted is null");
        if (!empty($phrase) && $phrase !== '' && !is_null($phrase)) {
            $q = $this->getPhraseSplitQuery($q, $phrase, ['se.name', 'se.description']);
        }
        if (!empty($childOf) && $childOf !== '' && !is_null($childOf)) {
            $q->andWhere('se.reference like :parentRef')
                //4 slashes used to escape single slash, as query builder doesn't escape single slash properly
                ->setParameter('parentRef', $childOf . '\\\\%');
        }
        if (isset($params['is_active']) && $params['is_active'] !== '' && !is_null($params['is_active'])) {
            $q->andWhere('se.is_active = :is_active')
                ->setParameter('is_active', $params['is_active']);

        }
        return $q;

    }

    /**
     * reference exists
     * @access public
     * @param $reference
     * @param int $id
     * @return bool
     */
    public function referenceExists($reference, $id = 0)
    {
        return $this->valueExists('reference', $reference, $id);
    }

    /**
     * @param string|BaseEntity $class
     * @return SchemaEntity|null
     */
    public function buildFromClass($class): ?SchemaEntity
    {
        $output = null;

        $isVirtualClass = is_subclass_of($class, VirtualEntity::class, true);

        /** @var BaseEntity|VirtualEntity $c */
        $c = $class;
        if (is_string($class)) {
            try {
                $classReflector = new \ReflectionClass($class);
                if ($classReflector->isInstantiable()) {
                    if ($isVirtualClass) {
                        $c = new $class();
                    } else {
                        $c = new $class($this->getState());
                    }
                }
            } catch (\ReflectionException $exception) {}
        }


        $output = new SchemaEntity($this->getState());
        $output->reference = $c::getObjectRef();
        $output->name = $c::getObjectName();
        $output->class_name = get_class($c);
        $output->is_virtual = $isVirtualClass;

        $classMetaData = $c->getAnnotatedClass();
        $output->name = $classMetaData['name'];
        $output->is_hidden = $classMetaData['hidden'];


        foreach ($c->getAnnotatedColumns() as $fieldRaw) {
            $field = new FieldEntity($this->getState());
            $field->reference = $fieldRaw['column'];
            $field->schema_entity_ref = $output->reference;
            $field->is_virtual = $isVirtualClass;
            $field->options_structure = null; /// do not delete this, it gets used in the db update process
            if (!empty($fieldRaw['field_type'])) {
                $field->field_type = $fieldRaw['field_type'];
                $field->db_type = FieldEntity::FIELD_TO_DB_MAP[$fieldRaw['field_type']];
                $field->db_options = $fieldRaw['opts'];

                $field->db_index = $fieldRaw['dbindex'] ?? false;
                $field->name = $fieldRaw['custom_name'] ?? ucwords(str_replace('_', ' ', $fieldRaw['column']));

                $field->flags = $fieldRaw['flags'] ?? FieldEntity::FLAG_NONE;
                $field->default_value = $fieldRaw['opts']['default'] ?? null;
                $field->is_mandatory = $fieldRaw['opts']['notnull'] ?? true;
                $field->is_system_hidden = $fieldRaw['system_hidden'] ?? false;

                if ($fieldRaw['field_type'] === 'enum' || $fieldRaw['field_type'] === 'enum_integer') {
                    try {
                        $cr = new \ReflectionClass($c);
                        $enumConstants = $cr->getConstants();
                        $enumFields = $this->getEnumFieldsData($enumConstants, $fieldRaw['column']);
                        if (!empty($enumFields)) {
                            $rows = [];
                            foreach ($enumFields as $index => $option) {
                                $key = md5((string)$option['text']);
                                $rowKey = $key;
                                $columnKey = $key;
                                $rows[] = [
                                    'id' => $rowKey, 'index' => $index, 'text' => $option['text'],
                                    'columns' => [
                                        [
                                            'id' => $columnKey, 'text' => $option['text'], 'index' => 0,
                                            'data' => [ 'id' => md5((string)$option['value']), 'text' => $option['text'], 'value' => $option['value'], 'sort_weight' => 0, ]
                                        ]
                                    ]
                                ];
                            }
                            $result =  json_encode([ 'rows' => $rows, 'direction' => FieldOptions::DIRECTION_DOWN, 'sort_option' => 'default', 'custom_user_options' => false ]);
                            $field->options_structure = $result;
                        }
                    } catch (\ReflectionException $exception) {}
                }
                $output->addEntityField($field);
            }
        }

        return $output;
    }

    private function getEnumFieldsData($enumConstants, $columnName): ?array
    {
        $usableFields = [];
        foreach ($enumConstants as $name => $value) {
            $nameParts = explode('_', $name);
            if (!empty($nameParts) && count($nameParts) > 2 && $nameParts[0] === 'ENUM') {
                $columnNameParts = explode('_', $columnName);

                $namePartsCount = count($nameParts);
                $columnNamePartsCount = count($columnNameParts);

                if ($namePartsCount - $columnNamePartsCount > 1) { /// This means that the nameParts contains more than the ENUM preset + the column name
                    $isValidEnum = true;
                    for ($i = 0; $i < $columnNamePartsCount; $i++) {
                        if (strtolower($nameParts[$i + 1]) !== strtolower($columnNameParts[$i])) {
                            $isValidEnum = false;
                            break;
                        }
                    }

                    if ($isValidEnum) {
                        $text = '';
                        $startingIndex = 1 + $columnNamePartsCount; /// Move the index to the enum name ('ENUM' + column name)
                        for ($i = $startingIndex; $i < $namePartsCount; $i++) {
                            $text .= ucfirst(strtolower($nameParts[$i]));
                            if ($i !== $namePartsCount - 1) { $text .= ' '; }
                        }

                        $usableFields[] = [ 'value' => $value, 'text' => $text, ];
                    }
                }
            }
        }
        return $usableFields;
    }

}
