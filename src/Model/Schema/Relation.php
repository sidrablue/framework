<?php
namespace BlueSky\Framework\Model\Schema;

use BlueSky\Framework\Object\Model\Base;

/**
 * Base class for Item
 */
class Relation extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__schema_relation';

}
