<?php
/**
 * Copyright (c) BlueSky Digital Labs Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Model\Schema;

use BlueSky\Framework\Entity\Schema\Field as FieldEntity;
use BlueSky\Framework\Object\Model\Base;

/**
 * Base class for Item
 * @package BlueSky\Framework\Model\Schema
 */
class Field extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__schema_field';

    public function createReferenceFromName(string $fieldName, bool $sanitiseName = false): string
    {
        $reference = trim($fieldName);
        if ($sanitiseName) {
            $reference = preg_replace('/(^[^a-zA-Z]*)|([^a-zA-Z\d\s])/', '', $reference);
        }
        $reference = str_replace(' ', '_', $reference);
        $reference = strtolower($reference);
        $referenceLength = strlen($reference);
        if ($referenceLength > 44) { /// 64 (MariaDB column name length max)
            $lengthDelta = $referenceLength - 44;
            $reference = substr($reference, 0, 44) . substr($reference, $lengthDelta > 4 ? -4 : -$lengthDelta);
        }
        return $reference;
    }

    /**
     * Retrieve search query
     *
     * @access public
     * @param int $schemaEntityRef
     * @param string $phrase
     * @return \Doctrine\DBAL\Query\QueryBuilder
     */
    public function getSearchQuery($schemaEntityRef, $phrase = "")
    {
        if ($schemaEntityRef) {
            $reference = explode('\\', $schemaEntityRef);
            $parentRef = isset($reference[0]) ? trim($reference[0]) : '';
            $childRef = isset($reference[1]) ? trim($reference[1]) : '';
            $q = $this->getReadDb()->createQueryBuilder();
            $q->select("sf.*")
                ->from($this->getTableName(), 'sf')
                ->andWhere("sf.lote_deleted is null");
            if (!empty($childRef)) {
                $ref = $parentRef . '\\' . $childRef;
                $q->andWhere('sf.schema_entity_ref = :parentRef')
                    ->setParameter('parentRef', "$ref");
            } else {
                $q->andWhere('sf.schema_entity_ref = :parentRef')
                    ->setParameter('parentRef', "$parentRef");
            }
            if (!empty($phrase)) {
                $q = $this->getPhraseSplitQuery($q, $phrase, ['sf.name', 'sf.description']);
            }
            $q->addOrderBy('sf.sort_order', 'asc');
            return $q;
        }
    }

    /**
     * Check reference uniqueness
     *
     * @todo change the
     *
     * @deprecated use the schema validation class
     * @access public
     * @param string $reference
     * @param string $entityId
     * @param int $id
     * @return \Doctrine\DBAL\Query\QueryBuilder
     */

    public function isFieldReferenceUnique($reference, $entityId, $id = '')
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('count(*) as count')
            ->from($this->getTableName(), 'sf')
            ->where('sf.reference = :reference')
            ->setParameter('reference', $reference)
            ->andWhere('sf.schema_entity_ref = :schema_entity_ref')
            ->setParameter('schema_entity_ref', $entityId);
        if($id){
            $q->andWhere('sf.id != :id')
                ->setParameter('id', $id);
        }
        $result = $q->execute()->fetch(\PDO::FETCH_ASSOC);
        return $result['count'] == 0;
    }
}
