<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 */

declare(strict_types=1);

namespace BlueSky\Framework\Model;

use BlueSky\Framework\Entity\Queue as QueueEntity;
use BlueSky\Framework\Entity\Import as ImportEntity;
use BlueSky\Framework\Object\Model\Base;

/**
 * Model class for Importing
 */
class Import extends Base
{

    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__import';

    /**
     * Check if an import has already been completed
     * */
    public function isIncomplete(ImportEntity $importEntity)
    {
        return $importEntity->id && $importEntity->isIncomplete();
    }

    /**
     * Check if an import is stopped
     * @param int $id - the import ID
     * @return boolean
     * */
    public function isStopped(ImportEntity $importEntity)
    {
        $result = false;
        $queueEntity = new QueueEntity($this->getState());
        if ($queueEntity->loadByObjectRefAndId('sb__import', $importEntity->id)) {
            $result = $queueEntity->status === QueueEntity::STATUS_CANCELLED || $queueEntity->status === QueueEntity::STATUS_FAILED || $queueEntity->status === QueueEntity::STATUS_SUSPENDED;
        }
        unset($queueEntity);
        return $result;
    }

    public function updateQueueStatus(ImportEntity $importEntity)
    {
        $queueEntity = new QueueEntity($this->getState());
        if ($queueEntity->loadByObjectRefAndId('sb__import', $importEntity->id)) {
            if ($importEntity->total_lines === 0) {
                $queueEntity->status = QueueEntity::STATUS_EMPTY;
            } else {
                $queueEntity->percent_completed = min(floor(($importEntity->total_processed / $importEntity->total_lines) * 100), 100);
                $queueEntity->status_message = "Completed {$importEntity->total_processed} out of {$importEntity->total_lines}";
            }
            $queueEntity->save();
        }
        unset($queueEntity);
    }

    public function createQueueEntry($importId)
    {
        $queueEntity = new QueueEntity($this->getState());
        $queueEntity->object_ref = 'sb__import';
        $queueEntity->object_id = $importId;
        $queueEntity->is_queued = true;
        $queueEntity->status = QueueEntity::STATUS_PENDING;
        $queueEntity->save();
        return $queueEntity;
    }

    /**
     * Synchronise status of import entity with its queue entity counterpart
     * @param $id - Import entity ID
     * @return bool|int - success
     */
    public function pullStatusFromQueueObject(ImportEntity $importEntity)
    {
        $success = false;
        $queueEntity = new QueueEntity($this->getState());
        // Search for queue entity and ensure it has a valid status
        if ($queueEntity->loadByObjectRefAndId('sb__import', $importEntity->id) && !empty($queueEntity->status)) {
            $importEntity->status = $queueEntity->status;
            $success = $importEntity->save();
        }
        return $success;
    }

    public function queueJob($importId, $mode)
    {
        // Get or Create queue entity
        $qe = new QueueEntity($this->getState());
        $qe->loadByFields([
            'object_ref' => $this->getTableName(),
            'object_id' => $importId
        ]);
        if (!$qe->id) {
            $qe = $this->createQueueEntry($importId);
        }

        // Setup data
        $queueData = [];
        $queueData['reference'] = $this->getState()->getSettings()->get("system.reference");
        $queueData['data']['id'] = $importId;
        $queueData['data']['mode'] = $mode;
        $queueData['queue_entity_id'] = $qe->id;
        $notifData = [
            'user_id' => $this->getState()->getUser()->id,
            'subject' => 'User Import ',
            'view_url' => '_admin/import/'.$importId
        ];

        // Set mode-specific data and Enqueue the job
        if ($mode == 'remove') {
            $qe->title = 'User Removal Import';
            $notifData['subject'] .= 'Removal ';
            $notifData['view_url'] = str_replace('import/', 'import/removal/', $notifData['view_url']);
            $queueData['notification_data'] = $notifData;
            $this->getState()->getQueue()->add('user-update', '\Lote\App\Admin\Queue\Worker\User\Import\Removal', $queueData);
        } else {
            $qe->title = 'User Import';
            $queueData['notification_data'] = $notifData;
            /** @see \Lote\System\Admin\Queue\Worker\User\Import\Addition */
            $this->getState()->getQueue()->add('user-import', '\Lote\App\Admin\Queue\Worker\User\Import\Addition', $queueData);
        }
    }

    public function searchHistory($page = 1, $mode = null, $phrase = '', $sortField = 'id', $sortDirection = 'ASC')
    {
        $q = $this->getReadDb()->createQueryBuilder();

        $q->select('t.*, u.first_name as _user_first_name, u.last_name as _user_last_name, f.filename')
            ->from($this->getTableName(), 't')
            ->leftJoin('t', 'sb__user', 'u', 'u.id = t.lote_author_id')
            ->leftJoin('t', 'sb__file', 'f', 'f.id = t.file_id')
            ->andWhere('t.status not like "new"')
            ->andWhere('t.lote_deleted is null');

        // Mode: import|remove or null for both
        if ($mode == 'import') {
            $q->andWhere('t.mode not like "remove"');
        } elseif ($mode == 'remove') {
            $q->andWhere('t.mode like "remove"');
        }

        if (!empty(trim($sortField))) {
            // Default sort direction to ASC if $sortDirection is empty or not equal to 'DESC'
            if (empty($sortDirection) || strtoupper($sortDirection) != 'DESC') {
                $sortDirection = 'ASC';
            }
            $q->orderBy($sortField, $sortDirection);
        }

        return $this->getListByQuery($q, $page, 20);
    }

}