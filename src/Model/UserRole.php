<?php
namespace BlueSky\Framework\Model;

use Doctrine\DBAL\Connection;
use BlueSky\Framework\Object\Model\Base;

/**
 * Model class for roles
 */
class UserRole extends Base
{

    /**
     * @var String $tableName - the name of the table for this model
     * */
    protected $tableName = 'sb__user_role';

    /**
     * Retrieve role linked users.
     *
     * @access public
     * @param int $roleId - Role ID
     * @return array
     */
    public function getRoleUsers($roleId)
    {
        $data = $this->getReadDb()->createQueryBuilder();
        $data->select("ur.*,u.first_name,u.last_name,u.email")
            ->from($this->getTableName(), 'ur')
            ->leftJoin('ur', 'sb__user', 'u', 'u.id = ur.user_id')
            ->andWhere("ur.role_id = :role_id")
            ->setParameter('role_id', $roleId)
            ->andWhere("ur.lote_deleted is null")
            ->andWhere("u.lote_deleted is null");
        $query = $data->execute();
        $result = $query->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }
}
