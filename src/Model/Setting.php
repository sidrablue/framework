<?php
namespace BlueSky\Framework\Model;

use BlueSky\Framework\Object\Model\Base;

class Setting extends Base
{

    /**
     * The table name related to this model
     * @var string $tableName
     * */
    protected $tableName = 'sb__setting';

    /**
     * An array of sites
     * */
    protected $settingValue = [];

    /**
     * An array of sites
     * */
    protected $loaded = false;

    /**
     * The current skin ID that we are getting settings for
     * */
    protected $skinId = false;

    /**
     * @todo implement property site id based caching
     * @param string $reference
     * @param mixed @defaultValue
     * @param int $siteId
     * @return mixed
     * */
    public function get($reference, $defaultValue = null, $siteId = 0)
    {
        $force = false;
        if($siteId){
            $force = true;
        }
        $this->loadSettings($siteId, $force);
        return $this->getCachedValue($reference, $defaultValue, $siteId);
    }

    /**
     * Get a setting value, falling back to the default config if the setting value does not exist
     * @access public
     * @param string $reference
     * @param mixed @default
     * @return string
     * */
    public function getSettingOrConfig($reference, $default = null)
    {
        if(!$result = $this->get($reference, $default)) {
            if(!$this->settingExists($reference) || $result==null) {
                $result = $this->getState()->getConfig()->get($reference, $default);
            }
        }
        return $result;
    }

    public function getSearchQuery($params = [])
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('s.*')
            ->from('sb__setting', 's')
            ->andWhere('s.lote_deleted is null');
        if (isset($params['is_content_accessible']) && $params['is_content_accessible']) {
            $q->andWhere('is_content_accessible = :isContentAccessible')
                ->setParameter("isContentAccessible", 1);
        }
        $q->orderBy('s.app_ref','asc');
        $q->addOrderBy('s.title','asc');
        $q->addOrderBy('s.reference','asc');

        return $q;

    }

    /**
     * Get a setting value, falling back to the default config if the setting value does not exist
     * @access public
     * @param string $reference
     * @param string $default
     * @return string|array
     * */
    public function getConfigOrSetting($reference, $default = null)
    {
        if(!$result = $this->getState()->getConfig()->get($reference)) {
            $result = $this->get($reference, $default);
        }
        return $result;
    }

    /**
     * Get a setting value, falling back to the default config if the setting value does not exist
     * @access public
     * @param string $reference
     * @param string $paramReference
     * @return string
     * */
    public function getSettingAccountParamOrConfig($reference, $paramReference)
    {
        if(!$result = $this->get($reference)) {
            if(isset($this->getState()->getAccount()->{$paramReference}) && $this->getState()->getAccount()->{$paramReference}) {
                $result = $this->getState()->getAccount()->{$paramReference};
            }
            else {
                $result = $this->getState()->getConfig()->get($reference);
            }
        }
        return $result;
    }

    /**
     * Set the site ID to be used for determining settings
     * @access public
     * @param int $skinId
     * @return void
     * */
    public function setSkinId($skinId)
    {
        $this->skinId = $skinId;
        $this->clearAll();
    }

    /**
     * @param string $reference
     * @param mixed @defaultValue
     * @param int $siteId
     * @return mixed
     * */
    public function getCachedValue($reference, $defaultValue = null, $siteId = 0)
    {
        $result = $defaultValue;
        if (isset($this->settingValue[$siteId]) && array_key_exists($reference, $this->settingValue[$siteId])) {
            if ($this->settingValue[$siteId][$reference] !== null) {
                $result = $this->settingValue[$siteId][$reference];
            }
        }
        return $result;
    }

    /**
     * Check if a setting value exists in the DB, even if it is defined
     * @access public
     * @param string $reference
     * @param int $siteId
     * @return boolean
     * */
    public function settingExists($reference, $siteId = 0)
    {
        $this->loadSettings($siteId);
        return isset($this->settingValue[$siteId]) && array_key_exists($reference, $this->settingValue[$siteId]);
    }

    /**
     * Load all of the settings in the system
     * @access private
     * @param int $siteId
     * @param bool $force
     * */
    private function loadSettings($siteId = 0, $force = false)
    {
        if (!$this->loaded || $force) {
            $this->settingValue = [];
            $q = $this->getReadDb()->createQueryBuilder();
            if($this->skinId) {
                $q->select('s.id, s.reference, s.`setting_type`, s.title, s.value_default, s.value_custom, k.`value_custom` as value_custom_skin')
                    ->from('sb__setting', 's')
                    ->leftJoin('s', 'sb__setting_skin', 'k', 'k.setting_reference = s.reference and k.skin_id = :skin_id')
                    ->setParameter('skin_id', $this->skinId)
                    ->andWhere("s.lote_deleted is null")
                    ->andWhere("k.lote_deleted is null");
            }
            else {
                $q->select("s.*")
                    ->from($this->getTableName(), 's');
                $baseCondition = "(s.object_ref is null or s.object_ref='') and (s.object_id is null or s.object_id = '')";
                if ($siteId) {
                    $q->andWhere(" ({$baseCondition}) or (s.object_ref = 'sb__site' and s.object_id=:site_id)")
                        ->setParameter("site_id", $siteId);
                } else {
                    $q->andWhere($baseCondition);
                }
                $q->orderBy("object_ref");
            }
            $settings = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
            foreach ($settings as $s) {
                $this->cacheValue($s['reference'], $s, $siteId);
            }
            $this->loaded = true;
        }
    }

    private function cacheValue($reference, $data, $siteId = 0)
    {
        $value = null;
        if ($data) {
            $value = $data['value_default'];
            if (isset($data['value_custom_skin']) && ($data['value_custom_skin'] || $data['value_custom_skin'] === '0')) {
                $value = $data['value_custom_skin'];
            }
            elseif ($data['value_custom'] || $data['value_custom'] === '' || $data['value_custom'] === '0') {
                $value = $data['value_custom'];
            }
            if ($data['setting_type'] == 'array' || $data['setting_type'] == 'list' || $data['setting_type'] == 'hash' || $data['setting_type'] == 'paypalexpress' || $data['setting_type'] == 'multi_select') {
                $this->settingValue[$siteId][$reference] = json_decode($value, true);
            } else {
                $this->settingValue[$siteId][$reference] = $value;
            }
        } else {
            $this->settingValue[$siteId][$reference] = null;
        }
    }

    public function set($reference, $value, $siteId = 0)
    {
        $s = new \BlueSky\Framework\Entity\Setting($this->getState());
        $s->setReadDb($this->getReadDb());
        $s->setWriteDb($this->getWriteDb());
        if ($siteId) {
            if (!$s->loadByFields(['reference' => $reference, 'site_id' => $siteId])) {
                $s->reference = $reference;
                $s->site_id = $siteId;
            }
        } else {
            if (!$s->loadByField('reference', $reference)) {
                $s->reference = $reference;
            }
        }
        $s->value_custom = $value;
        $s->save();
        $this->settingValue[$siteId][$reference] = $value;
    }

    public function clear($reference, $siteId = 0)
    {
        unset($this->settingValue[$siteId][$reference]);
    }

    public function clearAll()
    {
        $this->settingValue = [];
        $this->loaded = false;
    }

    public function addSetting(
        $type,
        $settingReference,
        $appReference = null,
        $title = '',
        $defaultValue = null,
        $parentReference = '',
        $description = '',
        $contentAccess = null,
        $config = null,
        $active = null,
        $hidden = null,
        $locked = null
    ) {
        $s = new \BlueSky\Framework\Entity\Setting($this->getState());
        $s->setReadDb($this->getReadDb());
        $s->setWriteDb($this->getWriteDb());
        $s->loadByField('reference', $settingReference);
        $s->setting_type = $type;
        $s->reference = $settingReference;
        $s->site_id = 0;
        if ($appReference) {
            $s->app_ref = $appReference;
        }
        if ($defaultValue) {
            $s->value_default = $defaultValue;
        }
        if ($parentReference) {
            //$s->parent_id = $parentReference;
        }
        if ($title) {
            $s->title = $title;
        }
        if ($description) {
            $s->description = $description;
        }
        if ($contentAccess) {
            $s->is_content_accessible = $contentAccess;
        }
        if ($defaultValue) {
            $s->value_config = $config;
        }
        if (!is_null($active)) {
            $s->active = $active;
        }
        if (!is_null($hidden)) {
            $s->hidden = $hidden;
        }
        if (!is_null($locked)) {
            $s->hidden = $hidden;
        }
        return $s->save();
    }

    /**
     * Function to check if an option is available for a setting
     *
     * @param string $reference
     * @param string $value
     * @return boolean
     * */
    public function hasOption($reference, $value)
    {
        $return = false;
        $custom = $this->get($reference);
        $data = explode(",", $custom);
        if (in_array($value, $data)) {
            $return = true;
        }
        return $return;
    }

    public function getSettingsByPrefix($prefix, $skinId = 0, $useNewOrder = false)
    {
        $result = [];
        if (is_array($prefix)) {
            foreach ($prefix as $v) {
                if (is_string($v)) {
                    $result = array_merge($result, $this->getSettingsByReference($v, $skinId, $useNewOrder));
                }
            }
        } elseif (is_string($prefix)) {
            $result = $this->getSettingsByReference($prefix, $skinId, $useNewOrder);
        }

        foreach ($result as $key => $value) {
            if (!empty($result[$key]['value_config'])) {
                $result[$key]['value_config'] = json_decode($result[$key]['value_config'], true);
            }
            if ($value['setting_type'] == 'file' && $result[$key]['value_custom'] > 0) {
                $f = new File($this->getState());
                $f->setReadDb($this->getReadDb());
                $f->setWriteDb($this->getWriteDb());
                $result[$key]['_file'] = $f->get($result[$key]['value_custom']);
            }
            if($value['setting_type'] == 'list') {
                $result[$key]['value_custom'] = json_decode($result[$key]['value_custom'], true);
            }
        }
        return $result;
    }

    /**
     * Get settings by reference
     *
     * @access public
     * @param $reference
     * @param int $skinId
     * @return array
     */
    public function getSettingsByReference($reference, $skinId = 0, $useNewOrder = false)
    {
        $result = [];
        if ($skinId) {
            $result = $this->getSettingsBySkin($reference, $skinId);
        } else {
            $q = $this->getReadDb()->createQueryBuilder();
            $data = $q->select('*')
                ->from('sb__setting', 'sbc')
                ->where('reference like :reference')
                ->setParameter('reference', $reference . '%')
                ->andWhere('sbc.lote_deleted is null');
            if($useNewOrder){
                $q->orderBy("(sbc.setting_type = 'file')", "desc")
                    ->addOrderBy("(sbc.setting_type = 'color_rgb')", "desc")
                    ->addOrderBy("(sbc.setting_type = 'color')", "desc")
                    ->addOrderBy("(sbc.setting_type = 'text')", "desc")
                    ->addOrderBy("sbc.setting_type")
                    ->addOrderBy("sbc.title", "asc");
            }else{
                $data->addOrderBy('sbc.id');
                  //  ->addOrderBy('sbc.group')
                   // ->addOrderBy('sbc.sort_order')
                     //@todo - determine proper setting ordering
            }
            $this->addLoteAccessClauses($q, 'sbc');
            $query = $data->execute();
            while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
                $result[] = $row;
            }
        }
        return $result;
    }

    public function updateSettings($updateArr, $skinId = false)
    {
        foreach ($updateArr as $key => $value) {
            if($skinId > 0) {
                $s = new \BlueSky\Framework\Entity\Setting($this->getState());
                $s->load($key);
                if(!$s->locked && $s->isWritable()) {
                    $k = new SettingSkin($this->getState());
                    if(!$k->loadBySkinAndReference($skinId, $s->reference)) {
                        $k->setting_reference = $s->reference;
                        $k->skin_id = $skinId;
                    }
                    if($value===null) {
                        $k->delete();
                    }
                    else {
                        $k->value_custom = $value;
                        $k->save();
                    }
                }
            }
            else {
                $s = new \BlueSky\Framework\Entity\Setting($this->getState());
                $s->load($key);
                if(!$s->locked && $s->isWritable()) {
                    $s->value_custom = $value;
                    $s->save();
                }
            }
        }
        return true;
    }

    /**
     * Get settings for a skin
     *
     * @access public
     * @param $reference
     * @param $skinId
     * @return array
     */
    public function getSettingsBySkin($reference, $skinId)
    {
        $result = [];
        if ($skinId > 0) {
            $q = $this->getReadDb()->createQueryBuilder();
            $q->select('s.id, s.reference, s.hidden, s.group, s.sort_order, s.`setting_type`, s.title, null as "value_default", k.`value_custom`')
                ->from('sb__setting', 's')
                ->leftJoin('s', 'sb__setting_skin', 'k', 'k.setting_reference = s.reference and k.skin_id = :skin_id')
                ->where('s.reference like :reference')
                ->setParameter('reference', $reference . '%')
                ->setParameter('skin_id', $skinId)
                ->andWhere("s.lote_deleted is null")
                ->andWhere("k.lote_deleted is null")
                ->orderBy('s.id', 'asc');
            $this->addLoteAccessClauses($q, 's');
            $query = $q->execute();
            while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
                $result[] = $row;
            }
        }
        return $result;
    }

    public function getSettingsByGroup($group, $settingType = '') {
        $result = [];

        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('s.*')
            ->from('sb__setting', 's')
            ->where('s.group = :group')
            ->setParameter('group', $group);
        if (!empty($settingType)) {
            $q->andWhere('s.setting_type = :setting_type')
                ->setParameter('setting_type', $settingType);
        }
        $query = $q->execute();
        while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
            $result[] = $row;
        }
        return $result;
    }

    /**
     * Retreive a setting from the parent db
     *
     * @param $reference
     * @return bool|string - Returns setting value.  False if no parent db or setting cannot be found
     */
    public function getParentSetting($reference)
    {
        $obj = new \BlueSky\Framework\Entity\Setting($this->getState());
        $parentDb = $this->getState()->getParentDb();

        // EARLY RETURN: Return false if there is no parent db
        if (!$parentDb) {
            return false;
        }

        $obj->setDb($parentDb);
        $obj->loadByField('reference', $reference);

        return $obj->value_custom ? $obj->value_custom : $obj->value_default;
    }

    /**
     * Get the current Skin ID
     * @access public
     * @return int
     * */
    public function getSkinId()
    {
        return $this->skinId;
    }

}
