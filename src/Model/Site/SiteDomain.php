<?php
namespace BlueSky\Framework\Model\Site;

use BlueSky\Framework\Object\Model\Base;

/**
 * Model class for Site Domain
 */
class SiteDomain extends Base
{

    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__site_domain';

    /**
     * Retrieve domain by site
     *
     * @access public
     * @param int $siteId
     * @return array
     */
    public function getDomainSite($siteId)
    {
        $data = $this->getReadDb()->createQueryBuilder();
        $data->select("ds.*,d.name as domain_name,d.url as domain_url")
            ->from($this->getTableName(), 'ds')
            ->leftJoin('ds', 'sb__domain', 'd', 'd.id = ds.domain_id')
            ->andWhere("ds.site_id = :site_id")
            ->setParameter('site_id', $siteId)
            ->andWhere("ds.lote_deleted is null");
        $query = $data->execute();
        $result = $query->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }

    /**
     * Get all linked domains
     *
     * @access public
     * @param int $ignoreId
     * @return array
     */
    public function getLinkedDomains($ignoreId = '')
    {
        $data = $this->getReadDb()->createQueryBuilder();
        $data->select("ds.domain_id")
            ->from($this->getTableName(), 'ds')
            ->andWhere("ds.lote_deleted is null");
        if($ignoreId){
            $data->andWhere("ds.id != :id")
                ->setParameter('id', $ignoreId);
        }
        $query = $data->execute();
        $result = $query->fetchAll(\PDO::FETCH_ASSOC);
        return !empty($result) ? array_column($result, 'domain_id') : [];
    }
}
