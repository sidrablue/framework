<?php
namespace BlueSky\Framework\Model\Site;

use BlueSky\Framework\Object\Model\Base;

/**
 * Model class for Files
 */
class Skin extends Base
{

    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__site_skin';

    public function getActiveSkins()
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("s.*")
            ->from("sb__site_skin", "s")
            ->where("lote_deleted is null")
            ->addOrderBy("id", "asc");
        return $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getDefaultSkin()
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("s.*")
            ->from("sb__site_skin", "s")
            ->where("lote_deleted is null")
            ->andWhere("is_default = 1")
            ->addOrderBy("sort_order", "asc")
            ->orderBy("name");
        return $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getSkinsIndexed()
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("s.*")
            ->from("sb__site_skin", "s")
            ->where("lote_deleted is null")
            ->orderBy("is_default", "desc")
            ->addOrderBy("sort_order", "asc")
            ->addOrderBy("id", "asc");

        $result = $q->execute();
        $skins = [];
        while ($row = $result->fetch(\PDO::FETCH_ASSOC)) {
            $skins[$row['id']] = $row;
        }

        return $skins;
    }

    /**
     * Get Skin search query.
     * @access public
     * @param string $phrase
     * @param string $sort_by
     * @param string $sort_order
     * @param boolean $isDefault
     * @return \Doctrine\DBAL\Query\QueryBuilder
     */
    public function getSearchQuery($phrase = '', $sort_by = null, $sort_order = null, $isDefault = false)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('s.*')
            ->from($this->getTableName(), 's')
            ->andWhere('s.lote_deleted is null');

        if (!empty($phrase)) {
            $q = $this->getPhraseSplitQuery($q, $phrase, ['s.name']);
        }
        if ($isDefault !== false && $isDefault != "all") {
            if ($isDefault) {
                $q->andWhere('s.is_default = :is_default')
                    ->setParameter('is_default', $isDefault);
            } else {
                $q->andWhere('s.is_default=0 or s.is_default is null');
            }
        }
        if (!empty($sort_by) && !empty($sort_order)) {
            $q->orderBy($sort_by, $sort_order);
        } else {
            $q->orderBy('s.id');
        }
        return $q;
    }

    /**
     * This function returns all skins that needs to be disabled.
     * @access public
     * @param int $ignoreId
     * @return array
     */
    public function getSkinsToRemoveDefaultSkinOption($ignoreId = 0)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("s.*")
            ->from($this->tableName, "s")
            ->andWhere('s.is_default>0')
            ->andWhere("s.lote_deleted is null");
        if ($ignoreId) {
            $q->andWhere('s.id != :ignore_id')
                ->setParameter('ignore_id', $ignoreId);
        }
        $query = $q->execute();
        $result = $query->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }

}
