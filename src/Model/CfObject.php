<?php
namespace BlueSky\Framework\Model;

use BlueSky\Framework\Entity\CfGroup as CfGroupEntity;
use BlueSky\Framework\Object\Model\Base;

class CfObject extends Base
{

    protected $tableName = 'sb__cf_object';

    /**
     * Delete an Object
     *
     * @param bool $strongDelete - Whether or not to delete the table object completely
     * @param int $id - the user to delete
     * @return void
     */
    public function delete($id, $strongDelete = false)
    {
        //@todo delete any custom fields associated with this object
        parent::delete($id, $strongDelete);
    }

    /**
     * Get all objects
     * @return array
     */
   /* public function getAll()
    {
        $result = [];
        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select('*')
            ->from('sb__cf_object', 'o')
            ->addOrderBy('display_order', 'asc');
        $query = $data->execute();
        while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
            $result[] = $row;
        }
        return $result;
    }*/

    /**
     * Check if a particular group reference has been taken for an object
     * @param int $objectId - the ID of the object in question
     * @param string $groupReference
     * @param int $existingGroupId
     * @return boolean
     * */
    public function hasGroupReference($objectId, $groupReference, $existingGroupId = 0)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select('count(*)')
            ->from('sb__cf_group', 'g')
            ->where('g.object_id = :object_id')
            ->setParameter('object_id', $objectId)
            ->andWhere('reference = :reference')
            ->setParameter('reference', $groupReference);
        if ($existingGroupId) {
            $q->andWhere('g.id != :id')->setParameter('id', $existingGroupId);
        }
        $query = $data->execute();
        return $query->fetchColumn() > 0;
    }

    /**
     * Check if a particular field reference has been taken for an object
     * @deprecated references need to be unique to all the fields existent in the system
     * @see fieldReferenceExists
     * @param int $objectId - the ID of the object in question
     * @param string $fieldReference
     * @param int $existingFieldId
     * @return boolean
     * */
    public function hasFieldReference($objectId, $fieldReference, $existingFieldId = 0, $ignoreDeleted = true)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select('count(*)')
            ->from('sb__cf_field', 'f')
            ->leftJoin('f', 'sb__cf_group', 'g', 'g.id = f.group_id')
            ->where('g.object_id = :object_id')
            ->setParameter('object_id', $objectId)
            ->andWhere('f.reference = :reference')
            ->setParameter('reference', $fieldReference);
        if ($existingFieldId) {
            $q->andWhere('f.id != :id')->setParameter('id', $existingFieldId);
        }
        if(!$ignoreDeleted) {
            $q->andWhere('f.lote_deleted is null');
        }
        $query = $data->execute();
        return $query->fetchColumn() > 0;
    }

    /**
     * Check if a particular field reference has been taken
     * @param string $fieldReference the reference to check
     * @param int $existingFieldId do not include the current field in the search
     * @param bool $includeDeleted include the deleted fields
     * @return bool true if field reference exists
     */
    public function fieldReferenceExists($fieldReference, $existingFieldId = 0, $includeDeleted = false) {
        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select('count(*)')
            ->from('sb__cf_field', 'f')
            ->where('f.reference = :reference')
            ->setParameter('reference', $fieldReference);

        if (is_numeric($existingFieldId) && $existingFieldId > 0) {
            $q->andWhere('f.id != :id')
                ->setParameter('id', $existingFieldId);
        }

        if (!$includeDeleted) {
            $q->andWhere('f.lote_deleted is null');
        }

        $query = $data->execute();
        return $query->fetchColumn() > 0;
    }

    /**
     * Get all the field groups for an object
     * @param int|string $objectId
     * @return array
     * */
    public function getCfGroups($objectId, $includeDeleted = false)
    {
        $result = [];
        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select('g.*')
            ->from('sb__cf_group', 'g');
        if (is_numeric($objectId)) {
            $data->where('g.object_id = :id')
                ->setParameter('id', $objectId);

        } else {
            $data->leftJoin('g', 'sb__cf_object', 'o', 'o.id = g.object_id')
                ->where('o.reference = :reference')
                ->setParameter('reference', $objectId);
        }
        $data->addOrderBy('g.sort_order', 'asc');

        if(!$includeDeleted) {
            $q->andWhere('lote_deleted is null');
        }

        if ($query = $data->execute()) {
            while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
                $result[] = $row;
            }
        }
        return $result;
    }

    /**
     * Get filtered field groups for an object based on phrase and active status and include paging
     * @param int|string $objectId
     * @param string $phrase
     * @param bool|string $active
     * @param int $page
     * @param int $perPage
     * @param bool $includeDeleted
     * @return array
     * */
    public function getCfGroupsByQuery($objectId, $phrase = '', $active = '', $page = 1, $perPage = 20, $includeDeleted = false)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('g.*')
            ->from('sb__cf_group', 'g');
        if (is_numeric($objectId)) {
            $q->where('g.object_id = :id')
                ->setParameter('id', $objectId);

        } else {
            $q->leftJoin('g', 'sb__cf_object', 'o', 'o.id = g.object_id')
                ->where('o.reference = :reference')
                ->setParameter('reference', $objectId);
        }
        $q->addOrderBy('g.sort_order', 'asc');

        if(!$includeDeleted) {
            $q->andWhere('lote_deleted is null');
        }

        if (!is_null($phrase) && $phrase !== '') {
            $q->andWhere('(g.name like :phrase or g.description like :phrase)')
                ->setParameter('phrase', "%$phrase%");
        }

        if (!is_null($active) && $active !== '') {
            $q->andWhere('g.active = :active')
                ->setParameter('active', $active);
        }

        return $this->getListByQuery($q, $page, $perPage);
    }

    /**
     * Get the first group for an object
     * @param int $objectId - the object ID
     * @return CfGroupEntity|false
     * */
    public function getFirstGroup($objectId)
    {
        $result = false;
        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select('g.*')
            ->from('sb__cf_group', 'g')
            ->where('g.object_id = :id')
            ->setParameter('id', $objectId)
            ->orderBy('sort_order')
            ->setMaxResults('1');
        if ($query = $data->execute()) {
            while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
                $result = new CfGroupEntity($this->getState());
                $result->setData($row);
            }
        }
        return $result;
    }

    /**
     * Get the object by reference
     * @param int $objectId - the object ID
     * @return CfGroupEntity|false
     * */
    public function getObjectByReference($referenceId, $reference)
    {
        $result = false;
        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select('g.*')
            ->from($this->tableName, 'g')
            ->andWhere('g.reference_id = :reference_id')
            ->setParameter('reference_id', $referenceId)
            ->andWhere('g.reference = :reference')
            ->setParameter('reference', $reference);
        if ($query = $data->execute()) {
            while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
                $result = $row;
            }
        }
        return $result;
    }

}
