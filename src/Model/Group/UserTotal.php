<?php
namespace BlueSky\Framework\Model\Group;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Query\QueryBuilder;
use BlueSky\Framework\Entity\Group as GroupEntity;
use BlueSky\Framework\Entity\Group\UserTotal as UserTotalEntity;
use BlueSky\Framework\Model\Group;
use BlueSky\Framework\Model\Query\QueryUser;
use BlueSky\Framework\Model\User as UserModel;
use BlueSky\Framework\Object\Model\Base;
use BlueSky\Framework\Service\User\QueryTotal;
use BlueSky\Framework\Util\Arrays;

/**
 * Model class for Users
 */
class UserTotal extends Base
{

    /**
     * @var String $tableName - the name of the table for this model
     * */
    protected $tableName = 'sb__group_user_total';

    /**
     * @var array $totalFunctionMap - A map of field to function name that generates the group totals for that field
     * */
    protected $totalFunctionMap = [
        'total' => 'getTotals',
        'total_active' => 'getActiveTotals',
        'total_subscribed' => 'getSubscribedTotals',
        'total_active_subscribed' => 'getActiveSubscribedTotals',
        'total_subscribed_edm' => 'getSubscribedEdmTotals',
        'total_subscribed_sms' => 'getSubscribedSmsTotals',
        'total_active_subscribed_edm' => 'getActiveSubscribedEdmTotals',
        'total_active_subscribed_sms' => 'getActiveSubscribedSmsTotals',
        'total_unsubscribed_temporary' => 'getUnsubscribedTemporaryEdmTotals',
        'total_active_unsubscribed_temporary' => 'getActiveUnsubscribedTemporaryTotals',
        'total_unsubscribed_temporary_edm' => 'getUnsubscribedTemporaryEdmTotals',
        'total_unsubscribed_temporary_sms' => 'getUnsubscribedTemporarySmsTotals',
        'total_active_unsubscribed_temporary_edm' => 'getActiveUnsubscribedTemporarySmsTotals',
        'total_active_unsubscribed_temporary_sms' => 'getActiveUnsubscribedTemporarySmsTotals',
        'total_unsubscribed_permanent' => 'getUnsubscribedPermanantTotals',
        'total_active_unsubscribed_permanent' => 'getActiveUnsubscribedPermanantTotals',
        'total_unsubscribed_permanent_edm' => 'getUnsubscribedPermanantEdmTotals',
        'total_unsubscribed_permanent_sms' => 'getUnsubscribedPermanantSmsTotals',
        'total_active_unsubscribed_permanent_edm' => 'getActiveUnsubscribedPermanantEdmTotals',
        'total_active_unsubscribed_permanent_sms' => 'getActiveUnsubscribedPermanantSmsTotals',
    ];

    public function generateGroupTotals(): void
    {
        $this->generateStaticTotals();
        $this->generateDynamicTotals();
    }

    private function generateDynamicTotals(): void
    {
        $groupModel = new Group($this->getState());
        $dynamicGroups = $groupModel->getAllGroup(GroupEntity::GROUP_TYPE_DYNAMIC);
        foreach($dynamicGroups as $dynamicGroup) {
            try {
                $queryId = $dynamicGroup['query_id'];
                $qum = new QueryUser($this->getState());
                $data = $qum->getQuery($queryId);
                $queryTotal = new QueryTotal($this->getState());
                $ute = new UserTotalEntity($this->getState());
                $ute->loadByField("group_id", $dynamicGroup['id']);
                $ute->group_id = $dynamicGroup['id'];
                $ute->total_subscribed_edm = $queryTotal->getSubscribedEmail($data['clauses']);
                $ute->total_active_subscribed_edm = $queryTotal->getSubscribedActiveEmail($data['clauses']);
                $ute->total_subscribed_sms = $queryTotal->getSubscribedMobile($data['clauses']);
                $ute->total_active_subscribed_sms = $queryTotal->getSubscribedActiveMobile($data['clauses']);
                $ute->total = $queryTotal->getTotal($data['clauses']);
                $ute->total_active = $queryTotal->getTotalActive($data['clauses']);
                $ute->save();
                $this->getState()->getLoggers()->getMasterLogger('user-totals')->debug("{$this->getState()->accountReference} Generated group totals for {$dynamicGroup['id']}");
            } catch (\Exception $e) {
                $this->getState()->getLoggers()->getMasterLogger('user-totals')->error("{$this->getState()->accountReference} could not generate group totals for dynamic group id (exception): {$dynamicGroup['id']}. {$e->getTraceAsString()}");
            } catch (\TypeError $e) {
                $this->getState()->getLoggers()->getMasterLogger('user-totals')->error("{$this->getState()->accountReference} could not generate group totals for dynamic group id (type error): {$dynamicGroup['id']}. {$e->getTraceAsString()}");
            } catch (\Error $e) {
                $this->getState()->getLoggers()->getMasterLogger('user-totals')->error("{$this->getState()->accountReference} could not generate group totals for dynamic group id (error): {$dynamicGroup['id']}. {$e->getTraceAsString()}");
            }
        }
    }

    private function generateStaticTotals(): void
    {
        $groupModel = new Group($this->getState());
        $staticGroups = $groupModel->getAllGroup(GroupEntity::GROUP_TYPE_STATIC);
        $groupTotalsEntityArray = [];
        foreach ($staticGroups as $group) {
            $groupTotalsEntityArray[$group['id']] = new UserTotalEntity($this->getState());
            $groupTotalsEntityArray[$group['id']]->setGroupId($group['id']);
        }
        $this->populateTotalsIntoEntityArray($groupTotalsEntityArray);
        foreach ($groupTotalsEntityArray as $groupTotalEntity) {
            /** @var UserTotalEntity $groupTotalEntity */
            if ($groupTotalEntity->group_id > 0) {
                $groupTotalEntity->save();
            }
        }
    }

    /**
     * Populate all totals into an array of group total entities
     *
     * @access private
     * @param array $entityArray - ahn array of group total entiries
     * @return array - original entity array
     * */
    private function populateTotalsIntoEntityArray($entityArray)
    {
        foreach ($this->totalFunctionMap as $k => $v) {
            $tempTotals = $this->$v();
            foreach ($tempTotals as $groupId => $total) {
                if (isset($entityArray[$groupId])) {
                    $entityArray[$groupId]->{$k} = $total['cnt'];
                }
            }
        }
    }

    /**
     * Get the totals of users for all groups in the system
     *
     * @return array
     */
    public function getTotals()
    {
        $q = $this->getBaseQuery();
        $totals = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
        return Arrays::indexArrayByField($totals, 'group_id');
    }

    /**
     * Get the totals of users for all groups in the system
     *
     * @return array
     */
    public function getActiveTotals()
    {
        $q = $this->getBaseQuery();
        $q->andWhere('u.is_active = "1"');
        $totals = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
        return Arrays::indexArrayByField($totals, 'group_id');
    }

    /**
     * Get the totals of subscribed users for all groups in the system
     *
     * @return array
     */
    public function getSubscribedTotals()
    {
        return $this->getSubscribedTotalsBySubscriptionStatus('1');
    }

    /**
     * Get the totals of temporarily unsubscribed users for all groups in the system
     *
     * @return array
     */
    public function getUnsubscribedTemporaryTotals()
    {
        return $this->getSubscribedTotalsBySubscriptionStatus('0');
    }

    /**
     * Get the totals of permanantly unsubscribed users for all groups in the system
     *
     * @return array
     */
    public function getUnsubscribedPermanantTotals()
    {
        return $this->getSubscribedTotalsBySubscriptionStatus('-1');
    }

    /**
     * Get the subscribed total by the subscription status
     *
     * @access private
     * @param int $subscriptionStatus - 1, 0 or -1
     * @return array
     * */
    private function getSubscribedTotalsBySubscriptionStatus($subscriptionStatus)
    {
        $q = $this->getBaseQuery();
        $q->andWhere('us.mobile = :subscription_status || us.email = :subscription_status')
            ->setParameter("subscription_status", $subscriptionStatus);
        $totals = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
        return Arrays::indexArrayByField($totals, 'group_id');
    }

    /**
     * Get the totals of users for all groups in the system
     *
     * @return array
     */
    public function getActiveSubscribedTotals()
    {
        return $this->getActiveSubscribedTotalsBySubscriptionStatus('1');
    }

    /**
     * Get the totals of users for all groups in the system
     *
     * @return array
     */
    public function getActiveUnsubscribedTemporaryTotals()
    {
        return $this->getActiveSubscribedTotalsBySubscriptionStatus('0');
    }

    /**
     * Get the totals of users for all groups in the system
     *
     * @return array
     */
    public function getActiveUnsubscribedPermanantTotals()
    {
        return $this->getActiveSubscribedTotalsBySubscriptionStatus('1-');
    }

    /**
     * Get the subscribed active total by the subscription status
     *
     * @access private
     * @param int $subscriptionStatus - 1, 0 or -1
     * @return array
     * */
    private function getActiveSubscribedTotalsBySubscriptionStatus($subscriptionStatus)
    {
        $q = $this->getBaseQuery();
        $q->andWhere('u.is_active = "1"');
        $q->andWhere('us.mobile = :subscription_status || us.email = :subscription_status')
            ->setParameter("subscription_status", $subscriptionStatus);
        $totals = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
        return Arrays::indexArrayByField($totals, 'group_id');
    }

    /**
     * Get the totals of users for all groups in the system
     *
     * @return array
     */
    public function getSubscribedEdmTotals()
    {
        return $this->getSubscribedEdmTotalsBySubscriptionStatus('1');
    }

    /**
     * Get the totals of users for all groups in the system
     *
     * @return array
     */
    public function getUnsubscribedTemporaryEdmTotals()
    {
        return $this->getSubscribedEdmTotalsBySubscriptionStatus('0');
    }

    /**
     * Get the totals of users for all groups in the system
     *
     * @return array
     */
    public function getUnsubscribedPermanantEdmTotals()
    {
        return $this->getSubscribedEdmTotalsBySubscriptionStatus('-1');
    }

    /**
     * Get the totals of users for all groups in the system
     *
     * @return array
     */
    public function getActiveSubscribedEdmTotals()
    {
        return $this->getSubscribedEdmTotalsBySubscriptionStatus('1', true);
    }

    /**
     * Get the totals of users for all groups in the system
     *
     * @return array
     */
    public function getActiveUnsubscribedTemporaryEdmTotals()
    {
        return $this->getSubscribedEdmTotalsBySubscriptionStatus('0', true);
    }

    /**
     * Get the totals of users for all groups in the system
     *
     * @return array
     */
    public function getActiveUnsubscribedPermanantEdmTotals()
    {
        return $this->getSubscribedEdmTotalsBySubscriptionStatus('-1', true);
    }

    /**
     * Get the subscribed active total by the subscription status
     *
     * @access private
     * @param int $subscriptionStatus - 1, 0 or -1
     * @return array
     * */
    private function getSubscribedEdmTotalsBySubscriptionStatus($subscriptionStatus, $activeCheck = false)
    {
        $q = $this->getBaseQuery();
        $q->andWhere('u.email != ""');
        $q->andWhere('u.email is not null');
        $q->andWhere('us.email = :subscription_status')
            ->setParameter("subscription_status", $subscriptionStatus);
        if ($activeCheck) {
            $q->andWhere('u.is_active = "1"');
        }
        $totals = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
        return Arrays::indexArrayByField($totals, 'group_id');
    }

    /**
     * Get the totals of users for all groups in the system
     *
     * @return array
     */
    public function getSubscribedSmsTotals()
    {
        return $this->getSubscribedSmsTotalsBySubscriptionStatus('1');
    }

    /**
     * Get the totals of users for all groups in the system
     *
     * @return array
     */
    public function getUnsubscribedTemporarySmsTotals()
    {
        return $this->getSubscribedSmsTotalsBySubscriptionStatus('0');
    }

    /**
     * Get the totals of users for all groups in the system
     *
     * @return array
     */
    public function getUnsubscribedPermanantSmsTotals()
    {
        return $this->getSubscribedSmsTotalsBySubscriptionStatus('-1');
    }

    /**
     * Get the totals of users for all groups in the system
     *
     * @return array
     */
    public function getActiveSubscribedSmsTotals()
    {
        return $this->getSubscribedSmsTotalsBySubscriptionStatus('1', true);
    }

    /**
     * Get the totals of users for all groups in the system
     *
     * @return array
     */
    public function getActiveUnsubscribedTemporarySmsTotals()
    {
        return $this->getSubscribedSmsTotalsBySubscriptionStatus('0', true);
    }

    /**
     * Get the totals of users for all groups in the system
     *
     * @return array
     */
    public function getActiveUnsubscribedPermanantSmsTotals()
    {
        return $this->getSubscribedSmsTotalsBySubscriptionStatus('-1', true);
    }

    /**
     * Get the subscribed active total by the subscription status
     *
     * @access private
     * @param int $subscriptionStatus - 1, 0 or -1
     * @return array
     * */
    private function getSubscribedSmsTotalsBySubscriptionStatus($subscriptionStatus, $activeCheck = false)
    {
        $q = $this->getBaseQuery();
        $q->andWhere('u.mobile != ""');
        $q->andWhere('u.mobile is not null');
        $q->andWhere('us.mobile = :subscription_status')
            ->setParameter("subscription_status", $subscriptionStatus);
        if ($activeCheck) {
            $q->andWhere('u.is_active = "1"');
        }
        $totals = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
        return Arrays::indexArrayByField($totals, 'group_id');
    }

    /**
     * Get all of the groups, and optionally of a particular tag
     *
     * @return QueryBuilder
     * */
    private function getBaseQuery()
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('grp.id as group_id, count(distinct ug.user_id) as cnt')
            ->from('sb__group', 'grp')
            ->innerJoin("grp", "sb__user_group", "ug", "ug.group_id = grp.id")
            ->leftJoin('ug', 'sb__user', 'u', 'ug.user_id = u.id')
            ->leftJoin('u', 'sb__user_subscription', 'us', 'us.user_id = u.id')
            ->where('grp.lote_deleted is null')
            ->andWhere('u.lote_deleted is null')
            ->andWhere('ug.lote_deleted is null')
            ->addGroupBy('grp.id');
        return $q;
    }

}
