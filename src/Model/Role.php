<?php
namespace BlueSky\Framework\Model;

use BlueSky\Framework\Entity\Role as RoleEntity;
use BlueSky\Framework\Entity\RoleType as RoleTypeEntity;
use BlueSky\Framework\Entity\Type as TypeEntity;
use BlueSky\Framework\Entity\UserRole as UserRoleEntity;
use BlueSky\Framework\Object\Model\Base;

/**
 * Model class for roles
 */
class Role extends Base
{

    /** @var string $tableName - the name of the table for this model */
    protected $tableName = 'sb__role';

    //gets all roles
    public function getAllRoles(){
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('r.*')
            ->from($this->getTableName(), 'r')
            ->where("r.lote_deleted is null")
            ->andWhere("r.is_active = true");

        $result = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }

    public function userHasRole(int $userId, string $roleName): bool
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('r.id')->from($this->getTableName(), 'r')
            ->leftJoin('r', UserRoleEntity::TABLE_NAME, 'ur', 'ur.role_id = r.id')
            ->andWhere('r.name = :role')
            ->setParameter('role', $roleName)
            ->andWhere('ur.user_id = :user_id')
            ->setParameter('user_id', $userId)
            ->andWhere("r.is_active = true")
            ->andWhere("r.lote_deleted is null")
            ->andWhere("ur.lote_deleted is null");
        $result = $q->execute()->fetch(\PDO::FETCH_ASSOC);
        return (bool)$result;
    }

    //gets all the roles assigned to a given a user
    public function getUserRoles($user){
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('r.*, ur.is_primary')->from($this->getTableName(), 'r')
            ->leftJoin('r', UserRoleEntity::TABLE_NAME, 'ur', 'ur.role_id = r.id')
            ->where('ur.user_id = :user_id')->setParameter('user_id', $user)
            ->andWhere("r.is_active = true")
            ->andWhere("r.lote_deleted is null")
            ->andWhere("ur.lote_deleted is null");
        $result = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }

    //gets the primary user role given to the user, or the first non-primary user role if no primary user role found
    public function getPrimaryUserRole($user){
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('r.*')->from($this->getTableName(), 'r')
            ->leftJoin('r', UserRoleEntity::TABLE_NAME, 'ur', 'ur.role_id = r.id')
            ->where('ur.user_id = :user_id')->setParameter('user_id', $user)
            ->andWhere("ur.is_primary = true")
            ->andWhere("r.is_active = true")
            ->andWhere("r.lote_deleted is null")
            ->andWhere("ur.lote_deleted is null");
        $result = $q->execute()->fetch(\PDO::FETCH_ASSOC);

        if (empty($result)) {
            $q = $this->getReadDb()->createQueryBuilder();
            $q->select('r.*')->from($this->getTableName(), 'r')
                ->leftJoin('r', UserRoleEntity::TABLE_NAME, 'ur', 'ur.role_id = r.id')
                ->where('ur.user_id = :user_id')->setParameter('user_id', $user)
                ->andWhere("r.is_active = true")
                ->andWhere("r.lote_deleted is null")
                ->andWhere("ur.lote_deleted is null");
            $result = $q->execute()->fetch(\PDO::FETCH_ASSOC);
        }

        return $result;
    }

    //gets all the users assigned to a given a role
    public function getUsers($role){
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('u.*')->from('sb__user', 'u')
            ->leftJoin('u', UserRoleEntity::TABLE_NAME, 'ur', 'ur.user_id = u.id')
            ->where('ur.role_id = :role_id')->setParameter('role_id', $role)
            ->andWhere("u.lote_deleted is null")
            ->andWhere("ur.lote_deleted is null");
        $result = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }

    /**
     * Retrieve get types
     * @param $role
     * @access public
     * @return array
     */
    public function getTypes($role){
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('t.*')
            ->from(TypeEntity::TABLE_NAME, 't')
            ->leftJoin('t', RoleTypeEntity::TABLE_NAME, 'rt', 'rt.type_id = t.id')
            ->where('rt.role_id = :role_id')
            ->setParameter('role_id', $role)
            ->andWhere("t.lote_deleted is null")
            ->andWhere("rt.lote_deleted is null");
        $result = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }

    //add the specified roles to the specified user

    /**
     * @param $userId Int User ID
     * @param $roles array Int Array
     * @return bool
     */
    public function setUserRoles($userId, $roles){
        $success = true;
        if (count($roles) > 0) {
            foreach ($roles as $role) {
                $data = ['user_id' => $userId, 'role_id' => $role];
                $ure = new UserRoleEntity($this->getState());
                if (!$ure->loadByFields($data)) {
                    $ure->setData($data);
                    if (!$ure->save()) {
                        $success = false;
                    }
                }
            }
        }
        return $success;
    }

    //remove the specified roles from the specified user
    public function removeUserRoles(int $userId, array $roles): bool {
        $success = true;
        if (count($roles) > 0) {
            foreach ($roles as $role) {
                $ure = new UserRoleEntity($this->getState());
                if (
                    $ure->loadByFields([
                        'user_id' => $userId,
                        'role_id' => $role['id']
                    ])
                ) {
                    if (!$ure->delete()) {
                        $success = false;
                    }
                }
            }
        }
        return $success;
    }

    //set all the specified role ids to the specified user, and remove any that are not specified, can specify new primary role id but only changes primary role id if it associates with one of the user's new roles and is a valid primary role id
    public function setAllUserRoles(int $userId, array $newRoles, int $primaryRoleId = 0): bool {
        $success = true;

        $oldRoles = $this->getUserRoles($userId);
        if (count($oldRoles) > 0) {
            $oldRoles = array_column($oldRoles, 'id');
        }

        $allRoles = $this->getAllRoles();
        if (count($allRoles) > 0) {
            $allRoles = array_column($allRoles, 'id');
        }

        if (count($newRoles) > 0) {
            $newRoles = array_column($newRoles, 'id');
        }

        $rolesToDelete = [];
        $rolesToAdd = [];

        //Check which roles need to be deleted, note only valid roles returned by $this->>getUserRoles() can be deleted
        foreach ($oldRoles as $oldRole) {
            if (!in_array($oldRole, $newRoles)) {
                $rolesToDelete[] = ['id' => $oldRole];
            }
        }

        //Check which roles need to be added, note only valid roles returned by $this->getAllRoles() can be added
        foreach ($newRoles as $newRole) {
            if (!in_array($newRole, $oldRoles) && in_array($newRole, $allRoles)) {
                $rolesToAdd[] = $newRole;
            }
        }

        if ($success && count($rolesToDelete) > 0) {
            if (!$this->removeUserRoles($userId, $rolesToDelete)) {
                $success = false;
            }
        }

        if ($success && count($rolesToAdd) > 0) {
            if (!$this->setUserRoles($userId, $rolesToAdd)) {
                $success = false;
            }
        }

        //don't bother updating primary role if primary role id not specified
        if ($success && $primaryRoleId > 0) {
            $re = new RoleEntity($this->getState());
            if (in_array($primaryRoleId, $newRoles) && $re->load($primaryRoleId)) {
                if (!$this->setPrimaryRole($re->name, $userId)) {
                    $success = false;
                }
            } else {
                $success = false;
            }
        }

        return $success;
    }

    //gets all the roles assigned to a given type
    public function getTypeRoles($type){
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('r.*')->from($this->getTableName(), 'r')
            ->leftJoin('r', RoleTypeEntity::TABLE_NAME, 'rt', 'rt.role_id = r.id')
            ->where('rt.type_id = :type_id')->setParameter('type_id', $type)
            ->andWhere("r.lote_deleted is null")
            ->andWhere("rt.lote_deleted is null");
        $result = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }

    //assign all the roles to a specified type
    public function setTypeRoles($type, $roles){
        if (count($roles) > 0) {
            foreach ($roles as $role){
                if($role['has_access']==true) {
                    $data = [
                        'type_id' => $type,
                        'role_id' => $role['id']
                    ];
                    $rte = new RoleTypeEntity($this->getState());
                    $rte->loadByFields($data);
                    $rte->save();
                }
            }
        }
    }

    /**
     * Retrieve search query
     * @param $name
     * @param $isActive
     * @access public
     * @return \Doctrine\DBAL\Query\QueryBuilder
     */
    public function getSearchQuery($name = "", $isActive = false)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('r.*')
            ->from($this->getTableName(), 'r')
            ->andWhere('r.lote_deleted is null');
        if ($name) {
            $q = $this->getPhraseSplitQuery($q, $name, ['r.name']);
        }
        if ($isActive == '0') {
            $q->andWhere('r.is_active = true or r.is_active is null');
        } elseif ($isActive == '1') {
            $q->andWhere('r.is_active = false');
        }
        return $q;

    }

    /**
     * Sets primary role for user, return true on success
     * @access public
     * @var string $role
     * @var int $userId
     * @return boolean
     */
    public function setPrimaryRole(string $role, int $userId): bool
    {
        $success = true;
        $roleEntity = new RoleEntity($this->getState());
        if ($userId > 0 && $roleEntity->loadByField('name', $role)) {
            //Load the link between user/role, or create it if it doesn't exist
            $primaryUserRole = new UserRoleEntity($this->getState());
            $primaryUserRole->loadByFields([
                'user_id' => $userId,
                'role_id' => $roleEntity->id
            ]);
            $primaryUserRole->user_id = $userId;
            $primaryUserRole->role_id = $roleEntity->id;
            $primaryUserRole->is_primary = true;

            if (!$primaryUserRole->save()) {
                $success = false;
            }

            if ($success) {
                //Make sure other previously-primary roles are removed from primary
                $q = $this->getState()->getReadDb()->createQueryBuilder();
                $userRoleIds = $q->select('ur.id')
                    ->from(UserRoleEntity::TABLE_NAME, 'ur')
                    ->leftJoin('ur', 'sb__user', 'u', 'u.id = ur.user_id')
                    ->andWhere('ur.user_id = :userId')
                    ->setParameter('userId', $userId)
                    ->andWhere('ur.is_primary = true')
                    ->andWhere('ur.lote_deleted is null')
                    ->andWhere('u.lote_deleted is null')
                    ->execute()->fetchAll(\PDO::FETCH_COLUMN);

                //Make sure other roles aren't primary, and make the role given the primary role
                foreach ($userRoleIds as $userRoleId) {
                    if ($userRoleId != $primaryUserRole->id) {
                        $nonPrimaryUserRole = new UserRoleEntity($this->getState());
                        if ($nonPrimaryUserRole->load($userRoleId)) {
                            $nonPrimaryUserRole->is_primary = false;
                            if (!$nonPrimaryUserRole->save()) {
                                $success = false;
                                break;
                            }
                        } else {
                            $success = false;
                            break;
                        }
                    }
                }
            }
        } else {
            $success = false;
        }

        return $success;
    }
}
