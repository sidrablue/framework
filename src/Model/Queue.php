<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 */

declare(strict_types=1);

namespace BlueSky\Framework\Model;

use Doctrine\DBAL\Query\QueryBuilder;
use BlueSky\Framework\Entity\Notification;
use BlueSky\Framework\Object\Model\Base;
use BlueSky\Framework\Service\Email;
use BlueSky\Framework\View\Transform\Html\Service;
use BlueSky\Framework\Entity\Queue as QueueEntity;
use BlueSky\Framework\Util\Time;

class Queue extends Base
{
    /**
     * @var string $tableName
     */
    protected $tableName = 'sb__queue';

    /**
     * Fetch current jobs
     * @return array
     * @todo order by status then age
     */
    public function getCurrentJobs()
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("*")
            ->from($this->getTableName(), 't')
            ->andWhere('t.status = "processing" or t.status = "pending" or t.status = "suspended"')
            ->andWhere('t.lote_deleted is null');

        return $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function notifyAdmin($queueId, $notifId = null, $subject = 'Queued Job Complete')
    {
        if(filter_var($this->getState()->getSettings()->get('system.notification_email.address'), FILTER_VALIDATE_EMAIL)) {
            $obj = new QueueEntity($this->getState());
            $obj->load($queueId);
            $data = $obj->getViewData();
            if ($notifId) {
                $ne = new Notification($this->getState());
                $ne->load($notifId);
                $data['_view_url'] = $ne->view_url;
                $data['_download_url'] = $ne->download_url;
            }
            $e = new Email($this->getState());
            $content = Service::renderView(
                $this->getState(),
                'queue/admin_notification_email',
                ['item' => $data]
            );

            $e->sendTransactionalEmail(
                $subject,
                $content,
                $this->getState()->getSettings()->get('system.notification_email.address'),
                $this->getState()->getSettings()->get('system.email.address'),
                ucfirst($this->getState()->getSettings()->get('system.email.from')) . ' (' . $this->getState()->getSettings()->get('system.name') . ')'
            );
        }
    }

    public function getHistoryList($page = 1, $resultsPerPage = 1000, $countQuery = 'count(*) as cnt')
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("t.*")
            ->from($this->getTableName(), 't')
            ->andWhere('t.process_completed is not null')
            ->andWhere('t.lote_deleted is null')
            ->orderBy('t.process_completed', 'desc');
        $q = $this->addNotificationUrlsQuery($q);

        return $this->getListByQuery($q, $page, $resultsPerPage, $countQuery);
    }

    /**
     * @param QueryBuilder $q
     */
    protected function addNotificationUrlsQuery($q)
    {
        $q->addSelect('t_n.view_url as _view_url, t_n.download_url as _download_url')
            ->leftJoin('t', 'sb__notification', 't_n', 't_n.object_id = t.id and t_n.object_ref = :queueTable')
            ->setParameter('queueTable', $this->getTableName());
        return $q;
    }

    public function getPending()
    {
        $now = Time::getUtcNow();
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("t.*")
            ->from($this->getTableName(), 't')
            ->andWhere('t.`status` = "pending"')
            ->andWhere('t.`process_at` < :now')
            ->andWhere("t.lote_deleted is null")
            ->setParameter('now', $now);
        return $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
    }

}