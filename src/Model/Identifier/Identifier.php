<?php
namespace BlueSky\Framework\Model\Identifier;

use BlueSky\Framework\Object\Model\Base;
use BlueSky\Framework\Entity\Identifier\Identifier as IdentifierEntity;

/**
 * Model class for Identifier
 * @package BlueSky\Framework\Model\Identifier
 */
class Identifier extends Base
{

    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__identifier';

    /**
     * Generate identifier token.
     * @access public
     * @return string
     * */
    public function generateToken()
    {
        return uniqid();
    }

    /**
     * Make random hexadecimal token.
     * @access private
     * @param int $bytes
     * @return string
     * */
    private function generateRandomHexadecimalToken($bytes = 36)
    {
        $randomPseudoBytes = openssl_random_pseudo_bytes($bytes);
        $token = bin2hex($randomPseudoBytes);
        return $token;
    }


    /**
     * Retrieve identifier data from object reference and object id.
     * @access public
     * @param string $objectRef
     * @param int $objectId
     * @return array|boolean
     * */
    public function getIdentifierData($objectRef, $objectId)
    {
        return $this->findObject(['object_id' => $objectId, 'object_ref' => $objectRef]);
    }

    /**
     * Clear token.
     * @access public
     * @param string $objectRef
     * @param int $objectId
     * @return void
     * */
    public function clearToken($objectRef, $objectId)
    {
        if ($identifier = $this->findObject(['object_id' => $objectId, 'object_ref' => $objectRef])) {
            if ($identifier['preview_token']) {
                $ite = new IdentifierEntity($this->getState());
                $ite->load($identifier['id']);
                $ite->preview_token = null;
                $ite->save();
            }
        }
    }

    /**
     * Function used to regenerate token.
     * @access public
     * @param string $objectRef
     * @param int $objectId
     * @return void
     * */
    public function regenerateToken($objectRef, $objectId)
    {
        $ite = new IdentifierEntity($this->getState());
        $identifierData = ['object_id' => $objectId, 'object_ref' => $objectRef];
        if ($ite->loadByFields($identifierData)) {
            $ite->preview_token = $this->generateToken();
            $ite->save();
        } else {
            $identifierData['preview_token'] = $this->generateToken();
            $ite->setData($identifierData);
            $ite->save();
        }
    }

    /**
     * Function used to validate token.
     * @access public
     * @param string $objectRef
     * @param string $token
     * @param int $objectId
     * @return boolean
     * */
    public function isValidToken($objectRef, $objectId, $token = "")
    {
        $return = false;
        if ($token) {
            $ite = new IdentifierEntity($this->getState());
            $identifierData = ['object_id' => $objectId, 'object_ref' => $objectRef, 'preview_token' => $token];
            if ($ite->loadByFields($identifierData)) {
                $return = true;
            }
        }
        return $return;

    }

}
