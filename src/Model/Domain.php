<?php
namespace BlueSky\Framework\Model;

use BlueSky\Framework\Object\Model\Base;

/**
 * Model class for Files
 */
class Domain extends Base
{

    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__domain';

    /**
     * Get Skin search query.
     * @access public
     * @param string $phrase
     * @param string $sort_by
     * @param string $sort_order
     * @param boolean $isActive
     * @return \Doctrine\DBAL\Query\QueryBuilder
     */
    public function getSearchQuery($phrase = '', $sort_by = null, $sort_order = null, $isActive = false)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('d.*')
            ->from($this->getTableName(), 'd')
            ->andWhere('d.lote_deleted is null');

        if (!empty($phrase)) {
            $q = $this->getPhraseSplitQuery($q, $phrase, ['d.name']);
        }
        if ($isActive !== false && $isActive != "all") {
            if ($isActive) {
                $q->andWhere('d.is_active = :is_active')
                    ->setParameter('is_active', $isActive);
            } else {
                $q->andWhere('d.is_active=0 or d.is_active is null');
            }
        }
        if (!empty($sort_by) && !empty($sort_order)) {
            $q->orderBy($sort_by, $sort_order);
        } else {
            $q->orderBy('d.id');
        }
        return $q;
    }

    /**
     * Get active domains.
     * @access public
     * @param boolean $isCount
     * @return array
     */
    public function getActiveDomains($isCount = false)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $selectText = 'd.*';
        if($isCount){
            $selectText = 'count(*) as count';
        }
        $q->select($selectText)
            ->from($this->tableName, "d")
            ->where("d.lote_deleted is null")
            ->andWhere('d.is_active = :active')
            ->setParameter('active', 1)
            ->addOrderBy("id", "asc");
        if($isCount){
            return $q->execute()->fetch(\PDO::FETCH_ASSOC);
        }
        return $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * Check domain exists.
     * @access public
     * @param string $url
     * @param int $ignoreId
     * @return array
     */
    public function isDomainExists($url, $ignoreId)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("count(*) as count")
            ->from($this->tableName, "d")
            ->where("d.lote_deleted is null")
            ->andWhere('d.url = :url')
            ->setParameter('url', $url);
        if (!empty($ignoreId)) {
            $q->andWhere('d.id != :id')
                ->setParameter('id', $ignoreId);
        }
        $result = $q->execute()->fetch(\PDO::FETCH_ASSOC);
        return $result['count'] == 0;
    }
}
