<?php
namespace BlueSky\Framework\Model;

use BlueSky\Framework\Entity\Notification as NotificationEntity;
use BlueSky\Framework\Object\Model\Base;
use BlueSky\Framework\Util\Time;

/**
 * Model class for a Notifications
 *
 * @package BlueSky\Framework\Model
 */
class Notification extends Base
{
    /**
     * @var string $tableName
     */
    protected $tableName = 'sb__notification';

    /**
     * Get the content of a list of notes
     *
     * @access public
     * @param bool|int $userId
     * @return boolean
     */
    public function clearAll($userId = false)
    {
        $q = $this->getWriteDb()->createQueryBuilder();
        $q->update("sb__notification", "n")
            ->set("date_read", 'now()')
            ->andWhere("date_read is null")
            ->andWhere("lote_deleted is null");
        if ($userId) {
            $q->andWhere("user_id = :user_id")
                ->setParameter("user_id", $userId);
        }
        $q->execute();
        return true;
    }

    /**
     * Get an array list of the data for the latest unread notes
     *
     * @param bool $userId
     * @param int $page
     * @param int $resultsPerPage
     * @return array|false
     */
    public function getLatestUnread($userId = false, $page = 1, $resultsPerPage = 20)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("n.*, case when date_read is null then 0 else 1 end is_read")
            ->from("sb__notification", "n")
            ->andWhere("n.lote_deleted is null")
            ->andWhere('n.date_read is null')
            ->setMaxResults($resultsPerPage)
            ->setFirstResult($resultsPerPage * $page - $resultsPerPage)
            ->orderBy("n.lote_created", "desc")
            ->addOrderBy("id", "desc");
        if ($userId) {
            $q->andWhere("user_id = :user_id")
                ->setParameter("user_id", $userId);
        }
         return $result = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * Get an array list of the data for the latest unread notes
     *
     * @param bool $userId
     * @param string $objectRef
     * @param string $objectId
     * @param int $page
     * @param int $resultsPerPage
     * @return array|false
     */
    public function getUsersNotifications($userId, $objectRef = '', $objectId = '', $page = 1, $resultsPerPage = 20)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("n.*")
            ->from("sb__notification", "n")
            ->andWhere("n.lote_deleted is null")
            ->andWhere('n.user_id = :user_id')
            ->setParameter("user_id", $userId)
            ->setMaxResults($resultsPerPage)
            ->setFirstResult($resultsPerPage * $page - $resultsPerPage)
            ->orderBy("lote_created", "desc")
            ->addOrderBy("id", "desc");

        if ($objectRef) {
            $q->andWhere("object_ref = :object_ref")
                ->setParameter("object_ref", $objectRef);
        }
        if ($objectId) {
            $q->andWhere("object_id = :object_id")
                ->setParameter("object_id", $objectId);
        }
        return $result = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
    }
    

    /**
     * Get the total count of unread notifications
     *
     * @param bool $userId
     * @return array|false
     */
    public function getUnreadCount($userId = false, $objectRef = '')
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("count(*) as cnt")
            ->from("sb__notification", "n")
            ->where("n.date_read is null")
            ->andWhere("n.lote_deleted is null");
        if ($userId) {
            $q->andWhere("n.user_id = :user_id")
                ->setParameter("user_id", $userId);
        }
        if ($objectRef) {
            $q->andWhere("n.object_ref = :object_ref")
                ->setParameter("object_ref", $objectRef);
        }
        return $result = $q->execute()->fetchColumn();
    }

    /**
     * Mark a notification as read
     *
     * @param $notificationId
     * @param bool|\DateTime|string $date - the date that this notification was read
     * @param $userId
     * @return string
     * @access public
     */
    public function markRead($notificationId, $userId = 0, $date = false)
    {
        $result = false;
        $n = new NotificationEntity($this->getState());
        if ($n->load($notificationId)) {
            if(($userId && $n->user_id = $userId) || !$userId) {
                if ($date) {
                    $n->date_read = $date;
                } else {
                    $n->date_read = Time::getUtcNow();
                }
                $result = $n->save();
            }
        }
        return $result;
    }
}
