<?php
namespace BlueSky\Framework\Model\Auth;

use BlueSky\Framework\Entity\Auth\Session as AuthSessionEntity;
use BlueSky\Framework\Object\Model\Base;

/**
 * @package use BlueSky\Framework\Object\Model
 */
class Session extends Base
{

    protected $tableName = 'sb__auth_session';

    /**
     * Get a login session from a username, ip, site ID and optional session Id.
     * This function updates the session login entry if the IP does not match but the IP is within an allowed subnet
     * @param string $objectRef
     * @param int $objectId
     * @param string $ip
     * @param string|boolean $sessionId
     * @return array|false
     */
    public function getLogin($objectRef, $objectId, $ip, $sessionId = false)
    {
        $loginFirewall = false;
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("*")
            ->from('sb__auth_session', 's')
            ->where('s.object_ref = :object_ref')
            ->setParameter('object_ref', $objectRef)
            ->andWhere('s.object_id = :object_id')
            ->setParameter('object_id', $objectId)
            ->andWhere('(s.session_id = :session_id)')
            ->setParameter('session_id', $sessionId);
        $q->orderBy('id', 'desc');
        $result = $q->execute()->fetch(\PDO::FETCH_ASSOC);
        if($loginFirewall && $result['ip_address'] != $ip) {
            //this needs to be a middleware request / response
            /*$f = new Firewall($this->getState());
            if($f->isAllowed($result['ip_address'], '_login')) {
                $this->getWriteDb()->update('sb__auth_session', ['ip_address' => $ip], ['id' => $result['id']]);
                $result['ip_address'] = $ip;
            }
            else {
                $result = false;
            }*/
        }
        return $result;
    }

    /**
     * Delete a login session by its ID
     * @param int $id
     * @return void
     * */
    public function removeLoginById($id)
    {
        $this->getWriteDb()->delete('sb__auth_session', ['id' => $id]);
    }

    /**
     * Remove a login session from a username, ip and site ID
     * @param string|boolean $sessionId
     * @param string $username
     * @param int $siteId
     * @return array|false
     */
    public function removeLogin($sessionId, $username)
    {
        $params = ['object_key' => $username, 'session_id' => $sessionId];
        return $this->getWriteDb()->delete('sb__auth_session', $params);
    }

    /**
     * Save a login to the user session table
     * @param AuthSessionEntity $authSessionEntity
     * @param string $objectRef
     * @param int $objectId
     * @param string $objectKey
     * @param string $ip
     * @param string $sessionId
     * @param string $source
     * @param int $masqueradeUserId
     * @access public
     * @return boolean
     */
    public function saveLogin(AuthSessionEntity $authSessionEntity, $objectRef, $objectId, $objectKey, $ip, $sessionId = '', $source = 'local', $masqueradeUserId = 0)
    {
        if($data = $this->getLogin($objectRef, $objectId, $ip, $sessionId)) {
            $authSessionEntity->setData($data);
            $authSessionEntity->save();
        }
        else {
            $authSessionEntity->object_ref = $objectRef;
            $authSessionEntity->object_id = $objectId;
            $authSessionEntity->object_key = $objectKey;
            $authSessionEntity->ip_address = $ip;
            $authSessionEntity->source = $source;
            $authSessionEntity->session_id = $sessionId;
            $authSessionEntity->masquerade_user_id = $masqueradeUserId;
//            $authSessionEntity->hash = password_hash($data['token'] . $data['object_key'], PASSWORD_DEFAULT);
            $authSessionEntity->save();
        }
        return $authSessionEntity->id;
    }

    /**
     * Utility function to deleting expired login's periodically.
     * This function can be called but will only execute once every thousand times
     * */
    public function cleanup()
    {
        if (rand(1, 1000) == 19) {
            $this->deleteExpiredSessions();
        }
    }

    /**
     * Remove all expired login's from the session table
     * @access public
     * @return void
     * */
    public function deleteExpiredSessions()
    {
        $this->getWriteDb()->executeQuery('delete from sb__auth_session where expires > utc_timestamp()');
    }

    /**
     * Remove the current session ID
     * @access public
     * @return int
     * */
    public function getSessionId()
    {
        $result = 0;
        if(isset($this->data) && isset($this->data['id'])) {
            $result = $this->data['id'];
        }
        return $result;
    }

    public function getUserLastLoginDate($userId)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("max(s.datetime_last_activity) as last_login")
            ->from('sb__auth_session', 's')
            ->where('s.object_ref = :object_ref')
            ->andWhere("s.lote_deleted is null")
            ->setParameter('object_ref', "sb__user")
            ->andWhere('s.object_id = :object_id')
            ->setParameter('object_id', $userId);
        $result = $q->execute()->fetch(\PDO::FETCH_ASSOC);
        return isset($result['last_login']) ? $result['last_login'] : false;
    }


}
