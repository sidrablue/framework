<?php
namespace BlueSky\Framework\Model\Auth;

use BlueSky\Framework\Object\Model\Base;

/**
 * Model class for an Access entity
 *
 * @package BlueSky\Framework\Model
 */
class Log extends Base
{

    /**
     * @var string $tableName
     */
    protected $tableName = 'sb__auth_log';

}
