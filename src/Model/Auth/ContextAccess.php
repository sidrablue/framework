<?php
namespace BlueSky\Framework\Model\Auth;

use BlueSky\Framework\Object\Model\Base;
use BlueSky\Framework\Entity\Auth\AccessContext as ContextEntity;

/**
 * Model class for an Access entity
 *
 * @package BlueSky\Framework\Model
 */
class ContextAccess extends Base
{

    /**
     * @var string $tableName
     */
    protected $tableName = 'sb__auth_access_context';

    //return the permissions assigned to a role
    public function getPermissions($role){
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('p.*')->from('sb__auth_access_context', 'p')
            ->where('p.object_id = :object_id')->setParameter('object_id', $role)
            ->andWhere("p.lote_deleted is null");
        $result = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }

    public function setPermission($role, $context, $level){
        if (!($role=='') && !($context=='') && !($level=='')) {
            //if the fields are not empty, attempt to load the existing permission
            $ce = new ContextEntity($this->getState());
            if ($ce->loadByFields(['object_id' => $role, 'context' => $context])) {
                //if the existing permission can be loaded, update the permission level
                $pe->setData(['access_level' => $level]);
            } else {
                //if the permission cannot be found, create a new entry in the database with the permission details
                $this->getWriteDb()->insert('sb__auth_access_context',
                    ['context' => $context, 'access_level' => $level, 'object_id' => $role]);
            }
        }
    }

    public function hasPermissions($role){
        $result = false;
        $all = $this->getPermissions($role);
        if(count($all)>0){
            $result = true;
        }
        return $result;
    }
}
