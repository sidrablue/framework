<?php
namespace BlueSky\Framework\Model\Auth;

use BlueSky\Framework\Object\Model\Base;

/**
 * Model class for an Access entity
 *
 * @package BlueSky\Framework\Model
 */
class EntityAccess extends Base
{

    /**
     * @var string $tableName
     */
    protected $tableName = 'sb__auth_access_entity';

    /**
     * Get the object access defined
     * @param string $objectId
     * @param string $objectReference - the object in question
     * @param string $accessLevel - the access level, defaults to 'write'
     * @param string $accessType - the access type, defaults to 'role'
     * @return array|false
     * */
    public function getObjectAccess($objectId, $objectReference, $accessLevel = 'write', $accessType = 'role')
    {
        $result = false;
        if ($objectId && $objectReference) {
            $q = $this->getReadDb()->createQueryBuilder();
            $q->select("e.*")
                ->from($this->getTableName(), "e")
                ->where("e.object_ref = :object_ref")
                ->setParameter("object_ref", $objectReference)
                ->andWhere("e.object_id = :object_id")
                ->setParameter("object_id", $objectId);
            if ($accessLevel) {
                $q->andWhere("e.access_level = :access_level")->setParameter("access_level", $accessLevel);
            }
            if ($accessType) {
                $q->andWhere("e.access_type = :access_type")->setParameter("access_type", $accessType);
            }

            $result = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
        }
        return $result;
    }

    public function getUserAccess($accessType, $accessTypeId, $objectRef = false)
    {
        $result = false;
        if ($accessType && $accessTypeId) {
            $q = $this->getReadDb()->createQueryBuilder();
            $q->select("e.*")
                ->from($this->getTableName(), "e")
                ->where("e.access_type = :access_type")
                ->setParameter("access_type", $accessType)
                ->andWhere("e.access_type_id = :access_type_id")
                ->setParameter("access_type_id", $accessTypeId);
            if ($objectRef) {
                $q->andWhere("e.object_ref = :object_ref")->setParameter("object_ref", $objectRef);
            }
            $result = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
        }
        return $result;
    }


}
