<?php
namespace BlueSky\Framework\Model\Auth;

use BlueSky\Framework\Object\Model\Base;;
use BlueSky\Framework\Entity\Auth\Token as TokenEntity;

class Token extends Base
{

    /**
     * The table name related to this model
     * @var string $tableName
     * @access protected
     * */
    protected $tableName = 'sb__auth_token';


    /**
     * @return array - the token data
     * */
    public function getToken($publicKey, $sessionId = null, $activationIp = null)
    {
        $result = false;
        if ($publicKey) {
            $sql =  'select * from ' . $this->tableName .
                    ' where public_key = :publicKey '.
                    /*' and session_id = :sessionId '.
                    ' and activation_ip = :activationIp '.*/
                    ' and datetime_expires >= :date'.
                    ' limit 0,1';

            $params = [ 'publicKey' => $publicKey,
                      //  'sessionId' => ($sessionId == null) ? session_id() : $sessionId,
                      //  'activationIp' => ($activationIp == null) ? $_SERVER["REMOTE_ADDR"] : $activationIp,
                        'date' => gmdate("Y-m-d H:i:s", time())];

            $result = $this->getReadDb()->executeQuery($sql, $params)->fetchAll(\PDO::FETCH_ASSOC);
            // we only want one result so just normalising the results array down to one result
            if (!empty($result)) {
                $result = $result[0];
            }
        }
        return $result;
    }

    /**
     * @param string $publicKey
     * @return array - the token data
     * */
    public function useToken($publicKey)
    {
        $result = false;

        // only continue if we have a publicKey
        if ($publicKey) {
            // try and find an existing token, if it exist then continue. otherwise return false.
            $tokenData = $this->getToken($publicKey);
            if ($tokenData != false) {
                // if $tokenData['usage_limit'] = 0 then it has unlimited usage so we don't need
                // to worry about the usage count, otherwise we need to make sure that this
                // usage does not exceed the max allowed usage.
                if ($tokenData['usage_limit'] > 0) {
                    // increment the usage_count of the token by one
                    $tokenData['usage_count'] += 1;

                    if ($tokenData['usage_count'] > $tokenData['usage_limit']) {
                        // usage is greater than allowed, so expire the token
                        $this->expireToken($publicKey);
                        return false;
                    } else {
                        // usage is good, so just update the current usage count
                        $this->updateTokenUsage($publicKey, $tokenData['usage_count']);
                        $result = true;
                    }
                } else {
                    // unlimited token, so just update the usage so we know how often it has been used
                    $this->updateTokenUsage($publicKey, $tokenData['usage_count']);
                    $result = true;
                }
            }
        }

        return $result;
    }

    /**
     * @return array - the token data
     * */
    public function addToken($publicKey = null, $privateKey = null, $usageLimit = 1, $sessionId = null, $userID = null, $expire = 86400, $objectRef = null, $objectId = null)
    {
        // Set the token data up
        $tokenData = [];
        $tokenData['public_key'] = ($publicKey == null) ? uniqid() : $publicKey;
        $tokenData['private_key'] = ($privateKey == null) ? uniqid() : $privateKey;
        $tokenData['usage_limit'] = $usageLimit;
        $tokenData['usage_count'] = 0;
//        $tokenData['session_id'] = ($sessionId == null) ? session_id() : $sessionId;
        //$tokenData['user_id'] = $userID;
//        $tokenData['created'] = gmdate("Y-m-d H:i:s", time());
        //$tokenData['activated'] = gmdate("Y-m-d H:i:s", time());
        //$tokenData['activation_ip'] = $_SERVER["REMOTE_ADDR"];
        $tokenData['object_id'] = $objectId;
        $tokenData['object_ref'] = $objectRef;

        // Default expiry is 24 hrs. 24*60*60 = 86400 seconds
        $tokenData['datetime_expires'] = gmdate("Y-m-d H:i:s", (time() + $expire));

        // Write into the database
        $this->getWriteDb()->insert($this->tableName, $tokenData);

        // update the id in the token data
        $tokenData['id'] = $this->getWriteDb()->lastInsertId();

        // and return the token data
        return $tokenData;
    }

    public function expireToken($publicKey) {
        // update a token with the current timestamp as the expiry. Invalidating the token
        $result = false;

        $tokenData = [];
        $tokenData['datetime_expires'] = gmdate("Y-m-d H:i:s", time());

        $whereData = [];
        $whereData['public_key'] = $publicKey;

        $result = $this->getWriteDb()->update($this->tableName, $tokenData, $whereData);
        return $result;
    }

    public function updateTokenUsage($publicKey, $usageCount) {
        // update a token with the current timestamp as the expiry. Invalidating the token
        $result = false;

        $tokenData = [];
        $tokenData['usage_count'] = $usageCount;

        $whereData = [];
        $whereData['public_key'] = $publicKey;

        $result = $this->getWriteDb()->update($this->tableName, $tokenData, $whereData);
        return $result;
    }

    public function getTokenData($objectId, $objectRef, $publicKey)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('*')
            ->from($this->tableName, 'u')
            ->where('u.object_id = :object_id')
            ->andWhere('u.object_ref = :object_ref')
            ->andWhere('u.public_key = :public_key')
            ->andWhere('u.lote_deleted is null')
            ->setParameter('object_id', $objectId)
            ->setParameter('object_ref', $objectRef)
            ->setParameter('public_key', $publicKey);
        $query = $q->execute();
        $result = $query->fetch(\PDO::FETCH_ASSOC);
        return $result;
    }

    public function getTokensByIdAndRef($objectId, $objectRef)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('*')
            ->from($this->tableName, 'u')
            ->where('u.object_id = :object_id')
            ->setParameter('object_id',$objectId)
            ->andWhere('u.object_ref = :object_ref')
            ->setParameter('object_ref',$objectRef)
            ->andWhere("u.lote_deleted is null");
        $query = $q->execute();
        return $query->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getTokenByReference($objectRef, $objectId, $token = '')
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('*')
            ->from($this->tableName, 'u')
            ->where('u.object_id = :object_id')
            ->setParameter('object_id',$objectId)
            ->andWhere('u.object_ref = :object_ref')
            ->setParameter('object_ref',$objectRef)
            ->andWhere('u.lote_deleted is null');
        if($token){
            $q->andWhere('u.public_key = :public_key')
                ->setParameter('public_key',$token);
        }
        $query = $q->execute();
        $result = $query->fetch(\PDO::FETCH_ASSOC);
        return $result;
    }

    /**
     * @param string $publicKey
     * @param string $referenceId
     * @param string $referenceKey
     * @return array|bool - the token data
     * */
    public function useTokenByReference($publicKey, $referenceId, $referenceKey)
    {
        $result = false;

        // only continue if we have all the details
        if ($publicKey && $referenceId && $referenceKey) {
            // try and find an existing token, if it exist then continue. otherwise return false.
            $params = ['public_key' => $publicKey, 'reference_id' => $referenceId, 'reference_key' => $referenceKey];
            $tokenObj = new TokenEntity($this->getState());
            if ($tokenObj->loadByFields($params)) {
                // if $tokenData['usage_limit'] = 0 then it has unlimited usage so we don't need
                // to worry about the usage count, otherwise we need to make sure that this
                // usage does not exceed the max allowed usage.
                if ($tokenObj->usage_limit > 0) {
                    // increment the usage_count of the token by one
                    $tokenObj->usage_count += 1;

                    if ($tokenObj->usage_count > $tokenObj->usage_limit) {
                        // usage is greater than allowed, so expire the token
                        $this->expireToken($publicKey);
                        return false;
                    } else {
                        // usage is good, so just update the current usage count
                        $this->updateTokenUsage($publicKey, $tokenObj->usage_count);
                        $result = true;
                    }
                } else {
                    // unlimited token, so just update the usage so we know how often it has been used
                    $this->updateTokenUsage($publicKey, $tokenObj->usage_count);
                    $result = true;
                }
            }
        }
        return $result;
    }

}
