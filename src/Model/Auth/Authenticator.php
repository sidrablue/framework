<?php
namespace BlueSky\Framework\Model\Auth;

use BlueSky\Framework\Object\Model\Base;
use Sonata\GoogleAuthenticator;
use BlueSky\Framework\Entity\User\User;
use BlueSky\Framework\Entity\Auth\Authenticator as AuthEntity;
use Sonata\GoogleAuthenticator\GoogleQrUrl;


/**
 * Model class for an Authenticator entity
 *
 * @package BlueSky\Framework\Model
 */
class Authenticator extends Base
{

    /**
     * @var string $tableName
     */
    protected $tableName = 'sb__auth_authenticator';

    public function generate($userId, $force = false)
    {
        $auth = new GoogleAuthenticator\GoogleAuthenticator();
        $twoFactorAuth = new AuthEntity($this->getState());
        if (!$twoFactorAuth->loadByField('object_id', $userId)) {
            $user = new User($this->getState());
            $twoFactorAuth->secret = $auth->generateSecret();
            $twoFactorAuth->object_id = $userId;
            $twoFactorAuth->object_ref = $user->getTableName();
        } elseif ($force) {
            $twoFactorAuth->secret = $auth->generateSecret();
        }
        $twoFactorAuth->save();
        return true;
    }

    public function getQrCode($userId)
    {
        $url = parse_url($this->getState()->getRequest()->getHttpHost());
        $user = new User($this->getState());
        $user->load($userId);
        return GoogleQrUrl::generate($user->username, $this->getSecret($userId), $url['host']);
    }

    public function getSecret($userId)
    {
        $twoFactorAuth = new AuthEntity($this->getState());
        $twoFactorAuth->loadByField('object_id', $userId);
        return $twoFactorAuth->secret;
    }

    public function authenticate($userId, $code)
    {
        $auth = new GoogleAuthenticator\GoogleAuthenticator();
        return $auth->checkCode($this->getSecret($userId), $code);
    }

}
