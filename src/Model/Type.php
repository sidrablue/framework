<?php
namespace BlueSky\Framework\Model;

use Doctrine\DBAL\Connection;
use Lote\System\Admin\Entity\Query as QueryEntity;
use Lote\System\Admin\Model\QueryUser as QueryUserModel;
use BlueSky\Framework\Model\User as UserModel;
use BlueSky\Framework\Object\Model\Base;
use BlueSky\Framework\Util\Arrays;

/**
 * Model class for types
 */
class Type extends Base
{

    /**
     * @var String $tableName - the name of the table for this model
     * */
    protected $tableName = 'sb__type';

    //lists all the types
    public function getAllTypes(){
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('t.*')
            ->from('sb__type', 't')
            ->where("t.lote_deleted is null");
        $result = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }

    //return a list of types given a user
    public function getUserType($user){
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('t.*')->from('sb__type', 't')
            ->leftJoin('t', 'sb__user_type', 'ut', 'ut.type_id = t.id')
            ->where('ut.user_id = :user_id')->setParameter('user_id', $user)
            ->andWhere("t.lote_deleted is null")
            ->andWhere("ut.lote_deleted is null");
        $result = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }

    //return users associated with the type
    public function getUsers($type){
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('u.*')->from('sb__user', 'u')
            ->leftJoin('u', 'sb__user_type', 'ut', 'ut.user_id = u.id')
            ->where('ut.type_id = :type_id')->setParameter('type_id', $type)
            ->andWhere("u.lote_deleted is null")
            ->andWhere("ut.lote_deleted is null");
        $result = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }

    //adds all the types to a specified user
    public function setUserType($user, $types){
        if (count($types) > 0) {
            foreach ($types as $type){
                $this->getWriteDb()->insert('sb__user_type', ['user_id' => $user, 'type_id' => $type]);
            }
        }
    }
}
