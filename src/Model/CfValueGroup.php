<?php
namespace BlueSky\Framework\Model;

use Doctrine\DBAL\Connection;
use BlueSky\Framework\Entity\CfValueGroup as CfValueGroupEntity;
use BlueSky\Framework\Object\Model\Base;
use BlueSky\Framework\Entity\CfValueGroup as ValueGroupEntity;

class CfValueGroup extends Base
{

    protected $tableName = 'sb__cf_value_group';

    /**
     * Set the table name if there is a custom one
     * @access public
     * @param string $newName
     * @return void
     * */
    public function setTableName($newName)
    {
        $this->tableName = $newName;
    }

    /**
     * Get the list of custom field groups that the object is currently linked to
     * @access public
     * @param int $objectId - the ID of the object to check for
     * @return array
     * */
    public function getObjectGroups($objectId)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('*')
            ->from($this->getTableName(), 't')
            ->where('t.object_id = :object_id')
            ->setParameter('object_id', $objectId);
        $s = $q->execute();
        return $s->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * Remove fields that belong to field groups that the object is no longer linked to
     * @access public
     * @param int $objectId
     * @return void
     * */
    public function removeUnusedFields($objectId)
    {
        if ($objectId) {
            $sql = "delete from {$this->getFieldsTableName()}
                    where object_id = :object_id and field_id not in (
                      select f.id from sb__cf_field f
                      left join {$this->getTableName()} g on g.group_id = f.group_id
                      where g.object_id = :object_id
                    )";
            $this->getWriteDb()->executeQuery($sql, ['object_id' => $objectId]);
        }
    }

    /**
     * Get the name of the fields table, which is the name of this groups table without the ending _group
     * @access private
     * @return string
     * */
    private function getFieldsTableName()
    {
        return str_replace('_group', '', $this->getTableName());
    }

    /**
     * Delete a set of cf value group entries for an object
     * @param int $objectId
     * @param array $groups
     * @return void
     * */
    public function deleteGroupsByObject($objectId, $groups)
    {
        if ($objectId && is_array($groups) && count($groups) > 0) {
            $q = $this->getWriteDb()->createQueryBuilder();
            $q->delete($this->getTableName())
                ->where('group_id in (:group_id)')
                ->setParameter('group_id', $groups, Connection::PARAM_INT_ARRAY)
                ->andWhere('object_id = :object_id')
                ->setParameter('object_id', $objectId);
            $q->execute();
        }
    }

    /**
     * Delete a set of cf value group entries for an object
     * @param int $objectId
     * @param array $groups
     * @return void
     * */
    public function addObjectGroups($objectId, $groups)
    {
        if ($objectId && is_array($groups) && count($groups) > 0) {
            foreach($groups as $v) {
                $e = new ValueGroupEntity($this->getState());
                $e->setTableName($this->getTableName());
                $e->object_id = $objectId;
                $e->active = 1;
                $e->group_id = $v;
                $e->save();
            }
        }
    }

    /**
     * Check if an object has an association to a particular custom group
     * @access public
     * @param int $objectId
     * @param int $fieldGroupId
     * @return boolean
     * */
    public function hasCustomGroup($objectId, $fieldGroupId)
    {


        $q = $this->getWriteDb()->createQueryBuilder();
        $q->select('count(*)')
            ->from($this->getTableName(), 'g')
            ->andWhere('g.group_id = :group_id')
            ->setParameter('group_id', $fieldGroupId)
            ->andWhere('g.object_id = :object_id')
            ->setParameter('object_id', $objectId);
        return $q->execute()->fetchColumn() > 0;
    }

    /**
     * Add a association for this entity to a custom field group
     * @param int $groupId
     * @param int $objectId
     * @return void
     * */
    public function addFieldGroupEntry($groupId, $objectId)
    {
        if(!$this->hasCustomGroup($objectId, $groupId)) {
            $e = new CfValueGroupEntity($this->getState());
            $e->setTableName($this->getTableName());
            $e->object_id = $objectId;
            $e->group_id = $groupId;
            $e->hidden = false;
            $e->save();
        }
    }

}
