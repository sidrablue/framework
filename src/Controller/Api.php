<?php
namespace BlueSky\Framework\Controller;

/**
 * Default API controller for the Lote Framework
 * @package BlueSky\Framework\Controller
 * */
class Api extends Request
{

    /**
     * @const int LOTE_API_INVALID - error code for when an API request cannot be resolved to a class
     * */
    const LOTE_API_INVALID = 10;

    /**
     * @const int LOTE_API_INVALID - error code for when an API request resolves to a class but not a method
     * */
    const LOTE_API_INVALID_METHOD = 20;

    /**
     * @const int LOTE_API_INVALID - error code for when an API request is made for a deprecated function
     * */
    const LOTE_API_DEPRECATED = 30;

    /**
     * @var array $deprecatedFunctions - The list of deprecated functions in this version of the API that were
     * available in previous versions
     * */
    protected $deprecatedFunctions = [];

    /**
     * @var string $lastError - the last error
     * */
    protected $lastError = "";

    /**
     * Get the action name of this request, based on the HTTP request type and/or the _api_method parameter
     * @param array $route - the route data
     * @access protected
     * @return string
     * */
    protected function getActionName($route)
    {
        $this->executedAction = 'unknownAction';
        if (php_sapi_name() == 'cli') {
            if (isset($route['action'])) {
                $this->executedAction = $route['action'];
            }
        } else {
            $route = $this->getState()->getRoute();
            if (isset($route['_api_method']) && $route['_api_method']) {
                $this->executedAction = $route['_api_method'];
            }
        }
        return $this->executedAction;
    }

    /**
     * Get the controller to execute for this API call which is determined from the route parameters
     * @access public
     * @param array $routeData - the data for the route
     * @return string|false
     * */
    public function getController($routeData)
    {
        $result = false;
        if (isset($routeData['_api_controller'])) {
            $controller = str_replace("{version}", 'V' . $this->getApiVersion($routeData), $routeData['_api_controller']);
            if (class_exists($controller)) {
                $result = $controller;
            }
        }
        return $result;
    }

    /**
     * Get the API version to use for this request, which falls back to "1"
     * @access public
     * @param array $routeData - the data for the route
     * @return string
     * */
    protected function getApiVersion($routeData)
    {
        $result = '1';
        if (isset($routeData['lote_api_version'])) {
            $result = $routeData['lote_api_version'];
        } elseif ($this->getState()->getRequest()->headers->get("X-API-VERSION")) {
            $result = $this->getState()->getRequest()->headers->get("X-API-VERSION");
        } else {
            $accept = $this->getState()->getRequest()->headers->get("accept");
            if (strpos($accept, 'application/vnd.lote.') === 0) {

            }
        }
        return $result;
    }

    /**
     * Specify the unknownAction handler
     * @access protected
     * @return void
     * */
    protected function unknownAction()
    {
        $this->invalidApiCall();
    }

    /**
     * Magic method __call handler to call the invalidApiCall function on an invalid function being called
     * @access protected
     * @return void
     * */
    public function __call($name, array $arguments)
    {
        $this->invalidApiCall();
    }

    protected function executeAction($methodName)
    {
        if (array_search($methodName, $this->deprecatedFunctions) !== false) {
            $this->invalidApiCall();
        } elseif (method_exists($this, $methodName)) {
            $this->$methodName();
        } elseif (method_exists($this, $this->defaultAction)) {
            $this->{$this->defaultAction}();
        } else {
            $this->$methodName();
        }
    }

    protected function invalidApiCall()
    {
        $route = $this->getState()->getRoute();
        if (class_exists($this->getController($route))) {
            $methodName = $this->getActionName($route);
            if (array_search($methodName, $this->deprecatedFunctions) !== false) {
                $this->getState()->getView()->message = "This function is deprecated";
                $this->getState()->getView()->error_code = self::LOTE_API_DEPRECATED;
                $this->getState()->getView()->status_code = 410;
            } else {
                $this->getState()->getView()->message = "Valid API class and version, but unkonwn function name of '" . $this->getActionName($route) . "'";
                $this->getState()->getView()->error_code = self::LOTE_API_INVALID_METHOD;
                $this->getState()->getView()->status_code = 404;
            }
        } else {
            $this->getState()->getView()->message = "Invalid API call, unable to resolve.";
            $this->getState()->getView()->error_code = self::LOTE_API_INVALID;
            $this->getState()->getView()->status_code = 404;
        }
        $this->getState()->getResponse()->setStatusCode("404");
    }

    protected function setHttpStatus($statusCode, $errorText = "")
    {
        $this->getState()->getResponse()->setStatusCode($statusCode);
        if(!$errorText) {
            $errorText = \Symfony\Component\HttpFoundation\Response::$statusTexts[$statusCode];
        }
        $this->setLastError($errorText);
    }

    public function getHttpStatus()
    {
        return $this->getState()->getResponse()->getStatusCode();
    }

    protected function setLastError($error)
    {
        $this->lastError = $error;
    }

    public function getLastError()
    {
        return $this->lastError;
    }

}
