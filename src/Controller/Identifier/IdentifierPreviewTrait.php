<?php
namespace BlueSky\Framework\Controller\Identifier;

use BlueSky\Framework\Entity\Identifier\Identifier as IdentifierEntity;
use BlueSky\Framework\Model\Identifier\Identifier as IdentifierModel;

/**
 * Trait IdentifierPreviewTrait
 *
 * @package BlueSky\Framework\Controller\Identifier
 */
trait IdentifierPreviewTrait
{

    /**
     * Function to update identifier.
     * @param array $data
     * @access public
     * @return void
     */
    public function afterInsertData($data)
    {
        $im = new IdentifierModel($this->getState());
        $token = $im->generateToken();
        if ($token) {
            $ite = new IdentifierEntity($this->getState());
            $ite->setData(['object_id' => $data['id'], 'object_ref' => $this->tableName, 'preview_token' => $token]);
            $ite->save();

        }
    }

}
