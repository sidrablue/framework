<?php
namespace BlueSky\Framework\Controller\Identifier;

use BlueSky\Framework\Entity\Identifier\Identifier as IdentifierEntity;

/**
 * Trait IdentifierTrait
 *
 * @package BlueSky\Framework\Controller\Identifier
 */
trait IdentifierTrait
{

    /**
     * Function to update identifier.
     * @param array $data
     * @access public
     * @return void
     */
    public function afterInsertData($data)
    {
        $ite = new IdentifierEntity($this->getState());
        $ite->setData(['object_id' => $data['id'], 'object_ref' => $this->tableName]);
        $ite->save();
    }

}
