<?php
namespace BlueSky\Framework\Controller;

use BlueSky\Framework\Entity\User\User;
use BlueSky\Framework\Event\Data;
use BlueSky\Framework\Model\Auth\Authenticator;
use BlueSky\Framework\Model\User\AutoLogin as UserAutoLogin;
use BlueSky\Framework\Util\Time;
use BlueSky\Framework\Entity\Auth\Session as AuthSessionEntity;

/**
 * Default REST based controller for the Lote Framework
 * @package BlueSky\Framework\Controller
 * */
class Login extends Rest
{

    protected function login()
    {
        $this->getState()->getRequest()->cookies->set('lote_cookie_test', true);
        $this->getState()->getView()->cookieEnabled = $this->getState()->getRequest()->cookies->get('lote_cookie_test');
        $this->getState()->getView()->setRenderFile('login/login');
    }

    protected function twoFactorRequired()
    {
        $this->getState()->getView()->setRenderFile('login/two_factor');
    }

    protected function twoFactorValidate()
    {
        $code = $this->getVar("code");
        $authModel = new Authenticator($this->getState());
        if ($authModel->authenticate($this->getState()->getUser()->id, $code)) {
            $e = new AuthSessionEntity($this->state);
            if($e->loadByFields(["object_ref" => "sb__user", "object_id" => $this->getState()->getUser()->id, 'session_id' => session_id()])) {
                $e->is_two_factor_verified = true;
                $e->save();
            }
            $this->getState()->getView()->success = true;
        } else {
            $this->setError("Invalid code");
        }
    }

    protected function validUsernameSubmitted()
    {
        $result = false;
        $username = $this->getVar("username");
        $u = new User($this->getState());
        if ($username) {
            $u->loadByUsername($username);
            if (!$u->id) {
                $u->reset();
                $u->loadByEmail($username);
                $result = $u->id > 0;
            } else {
                $result = true;
            }
        }
        return $result;
    }

    protected function googleValidate()
    {
        $this->getState()->getView()->success = false;
        $token = $this->getVar("googleAccessToken");

        if (!is_null($token)) {
            $tokenCheckEndpoint = 'https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=' . $token;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $tokenCheckEndpoint);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $response = curl_exec($ch);
            $response = json_decode($response, true);
            curl_close($ch);

            $clientId = $this->getState()->getSettings()->getSettingOrConfig("system.login.google.client_id");
            if (!empty($response) && !isset($response["error_description"]) && $response["aud"] == $clientId) {
                if (isset($response["email"])) {
                    $u = new User($this->getState());
                    if ($u->loadByEmail($response["email"])) {
                        if ($this->getState()->getLogin()->autoLogin($u->id)) {
                            $this->getState()->getView()->success = true;
                        }
                    }
                }
            }
        }
    }

    protected function office365Validate()
    {
        $curlRedirectUri = $this->getState()->getRequest()->getSchemeAndHttpHost() . '/_admin/login/office365-validate';

        unset($_SESSION['access_token']);

        if (!isset($_SESSION['access_token'])) {
            $office365Token = $_SESSION['access_token'] = $this->getOffice365AccessToken($curlRedirectUri);
        } else {
            $office365Token = $_SESSION['access_token'];
        }

        if (isset($office365Token)) {
            if ($u = $this->getUserFromOffice365($office365Token)) {
                if ($this->getState()->getLogin()->autoLogin($u->id)) {
                    $this->redirect('/_admin');
                } else {
                    $this->setError("Could not log perform Office365 auto login.");
                }
            } else {
                $this->setError("Your Office365 email could not be matched to a user in this system.");
            }
        } else {
            $this->setError("Invalid Office365 token. Please contact an administrator");
        }
        $this->loginError();
    }

    protected function loginError()
    {
        $this->getState()->getView()->setRenderFile("login/error");
    }


    protected function getOffice365AccessToken($curlRedirectUri)
    {
        $result = null;
        $clientId = $this->getState()->getSettings()->getSettingOrConfig("system.login.office365.client_id");
        $clientSecret = $this->getState()->getSettings()->getSettingOrConfig("system.login.office365.client_secret");
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://login.microsoftonline.com/common/oauth2/token");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt(
            $ch,
            CURLOPT_POSTFIELDS,
            "code=" . $_GET['code'] . "&client_id=" . $clientId . "&redirect_uri=" . $curlRedirectUri . "&grant_type=authorization_code&resource=https://graph.microsoft.com&client_secret=" . $clientSecret
        );
        $data = curl_exec($ch);
        curl_close($ch);
        $data = json_decode($data);
        if (isset($data->access_token)) {
            $result = $data->access_token;

        }
        return $result;
    }

    protected function getUserFromOffice365($office365Token)
    {
        $result = false;
        if ($email = $this->getEmailFromOffice365($office365Token)) {
            $u = new User($this->getState());
            if ($u->loadByEmail($email)) {
                $u->office_365_last_validated = date("Y-m-d H:i:s");
                $u->save();
                $result = $u;
            }
        }
        return $result;
    }

    protected function getEmailFromOffice365($office365Token)
    {
        $result = false;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://graph.microsoft.com/v1.0/me");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            "Authorization: Bearer $office365Token",
            'Content-Type: application/json;odata.metadata=minimal;odata.streaming=true'
        ]);
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        $data = curl_exec($ch);
        curl_close($ch);
        // Get the data from the JSON response
        $data = json_decode($data, true);
        if (isset($data['userPrincipalName'])) {
            $result = $data['userPrincipalName'];
        }
        return $result;
    }

    function windowsValidate()
    {
        $clientKey = '0000000040160F76';
        $clientSecret = 'LfzZM0kbfWpp9upBKgVBvw2gpi83NsPP';
        $redirectUri = $this->getState()->getRequest()->getSchemeAndHttpHost() . '/_admin/login/windows-validate';

        if (!isset($_SESSION['access_token'])) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://login.live.com/oauth20_token.srf");
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
            curl_setopt(
                $ch,
                CURLOPT_POSTFIELDS,
                "code=" . $_GET['code'] . "&client_id=" . $clientKey . "&client_secret=" . $clientSecret . "&redirect_uri=" . $redirectUri . "&grant_type=authorization_code"
            );
            $data = curl_exec($ch);
            curl_close($ch);
            $data = json_decode($data);
            if (isset($data->access_token)) {
                $windowsLiveToken = $data->access_token;
                $_SESSION['access_token'] = $data->access_token;
            }
        } else {
            $windowsLiveToken = $_SESSION['access_token'];
        }

        if (isset($windowsLiveToken)) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://apis.live.net/v5.0/me?access_token=" . $windowsLiveToken);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $data = curl_exec($ch);
            curl_close($ch);
            // Get the data from the JSON response
            $data = json_decode($data, true);
            if (isset($data['emails']) && isset($data['emails']['account'])) {
                $u = new User($this->getState());
                if ($u->loadByEmail($data['emails']['account'])) {
                    if ($this->getState()->getLogin()->autoLogin($u->id)) {
                        $this->redirect('/_admin');
                    }
                }
            }
        }
    }

    protected function validate()
    {
        $this->validateBase($this->state->getSettings()->getSettingOrConfig("login.jwt.auto",false));
    }

    protected function validateBase($generateToken = false)
    {
        if ($this->getState()->getLogin()->login(true)) {
            if ($this->getState()->getMasterDb() && $this->getState()->getAccount() && $this->getState()->getAccount()->isLoginLocked()) {
                $this->setVar("success", false);
                $this->setVar("error", "This account is inactive");
            } else {
                $this->saveLastLoggedIn();
                $this->setVar("success", true);
                if ($generateToken) {
                    $this->setVar("user_login_token",
                        $this->getState()->getLogin()->generateToken($this->getState()->getUser()->id));
                }
                $eventData = new Data([], []);
                $this->getState()->getEventManager()->dispatch('lote.login.validate.true', $eventData);
            }
        } else {
            $this->setVar("success", false);
            $this->setVar("error", $this->getState()->getLogin()->getLoginError());
        }
    }

    protected function autoLogin()
    {
        $m = new UserAutoLogin($this->getState());
        if ($l = $m->getAutoLogin($this->getVar('login_hash'), $this->getVar('user_hash'))) {
            if ($this->getState()->getLogin()->autoLogin($l->user_id)) {
                $this->redirect($l->redirect);
            }
        }
    }

    protected function parentLogin()
    {
        $m = new UserAutoLogin($this->getState());
        if ($l = $m->getAutoLogin($this->getVar('login_hash'), $this->getVar('user_hash'))) {
            if ($this->getState()->getLogin()->autoLogin($l->user_id)) {
                $this->redirect($l->redirect);
            }
        }
    }

    protected function logout()
    {
        $this->getState()->getLogin()->logout();
        if ($redirectURl = $this->getVar("redirect", false)) {
            $url = $redirectURl;
        } else {
            $url = $this->getRouteAppPrefix('', true) . '/login';
        }
        $this->redirect($url);
    }

    protected function retrieve()
    {
        $this->getState()->getView()->setRenderFile('login/retrieve');
    }

    private function saveLastLoggedIn()
    {
        if ($this->getState()->getUser()->isAdmin() && $this->getState()->getMasterDb()) {
            $clientRef = $this->getState()->getSettings()->get('system.reference');
            if (!empty($clientRef)) {
               /* $obj = new \Lote\App\Master\Entity\Master\Account($this->getState());
                $accountDetails = $obj->loadByField('reference', $clientRef);
                if (!empty($accountDetails['id'])) {
                    $obj->last_logged_in = new \DateTimeImmutable();
                    $obj->save();
                }*/
            }
        }
        return true;
    }

    public function masquerade()
    {
        $this->getState()->getView()->success = false;
        if ($this->getState()->getUser()->isAdmin()) {
            $currentUserId = $this->getState()->getUser()->id;
            $userId = $this->getVar("user_id");
            $ue = new User($this->getState());
            if ($ue->load($userId)) {
                $this->getState()->getView()->success = $this->getState()->getLogin()->autoLogin($userId, $currentUserId);
                $this->getState()->getView()->user_id = $userId;
                if ($redirect = $this->getVar("redirect")) {
                    $this->redirect(base64_decode($redirect));
                }
            }
        }
    }

}
