<?php
namespace BlueSky\Framework\Controller\Install;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Schema\Table;
use ReflectionClass;
use BlueSky\Framework\Application\App\Finder;
use BlueSky\Framework\Application\Config;
use BlueSky\Framework\Controller\Rest;
use BlueSky\Framework\Entity\App;
use BlueSky\Framework\Entity\Schema\Entity;
use BlueSky\Framework\Event\Data;
use BlueSky\Framework\Object\Entity\Base;
use BlueSky\Framework\Util\Strings;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;
use BlueSky\Framework\CodeGen\CodeGen;
use BlueSky\Framework\Entity\Schema\Field as SchemaField;
use BlueSky\Framework\Model\Schema\Entity as EntityModel;
use zpt\anno\Annotations;
use BlueSky\Framework\Entity\Schema\Relation as RelationEntity;
use BlueSky\Framework\Model\Schema\Relation as RelationModel;
use BlueSky\Framework\Entity\User\User as UserEntity;

define('LOTE_VERSION_PATH', LOTE_LIB_PATH . 'res/release/');

/**
 * Image view controller of the framework
 * @package BlueSky\Framework\Controller\Core
 * */
class Update extends Rest
{

    private $relations = [];

    protected function runHotfix()
    {
        global $loader;
        $loader->addPsr4("Lote\\Script\\Hotfix\\", LOTE_LIB_PATH . '../scripts/hotfix/');
        $route = $this->getState()->getRoute();
        if (isset($route['fix'])) {
            $class = 'Lote\Script\Hotfix\\' . $route['fix'];
            if (class_exists($class)) {
                $updateObject = new $class($this->getState());
                if (method_exists($updateObject, 'run')) {
                    $updateObject->run();
                    echo("Fix finished for {$this->getState()->getReadDb()->getDatabase()}\n");
                } else {
                    echo("Could not find method 'run'\n");
                }
            } else {
                echo("Could not find class {$route['fix']}\n");
            }
        } else {
            echo("No fix specified\n");
        }
    }

    public function check(SymfonyStyle $io, bool $virtualOnly = false, bool $quietMode = false)
    {
        $this->sync($io, true, $virtualOnly, $quietMode);
    }

    public function update(SymfonyStyle $io, bool $virtualOnly = false, bool $quietMode = false)
    {
        $this->sync($io, false, $virtualOnly, $quietMode);
    }

    private function getRootTableMigrations($db)
    {
        $requiredRootSchema = new Schema();
        $currentRootSchema = new Schema();
        $rootClasses = ['BlueSky\\Framework\\Entity\\App',
            'BlueSky\\Framework\\Entity\\Schema\\Entity',
            'BlueSky\\Framework\\Entity\\Schema\\Field',
            'BlueSky\\Framework\\Entity\\Schema\\Relation'];
        foreach ($rootClasses as $class) {
            $s = new \BlueSky\Framework\Application\App\Schema();
            $s->getSchema($requiredRootSchema, $class, false);
        }

        $namespaces = $db->getSchemaManager()->getDatabasePlatform()->supportsSchemas() ? $db->getSchemaManager()->listNamespaceNames() : [];
        $sequences = $db->getSchemaManager()->getDatabasePlatform()->supportsSequences() ? $db->getSchemaManager()->listSequences() : [];
        $rootTables = ['sb__app', 'sb__schema_entity', 'sb__schema_field', 'sb__schema_relation'];
        foreach ($db->getSchemaManager()->listTables() as $v) {
            /** @var Table $v */
            if (in_array($v->getName(), $rootTables)) {
                $tables[] = $v;
            }
        }
        $currentRootSchema = new Schema($tables, $sequences, $db->getSchemaManager()->createSchemaConfig(), $namespaces);
        return $currentRootSchema->getMigrateToSql($requiredRootSchema, $db->getDatabasePlatform());
    }

    private function isMaster()
    {
        return $this->getState()->getConfig()->get("master.enabled", false);
    }

    protected function sync(SymfonyStyle $io, $simulate = true, bool $virtualOnly = false, bool $quietMode = false)
    {
        if (!defined('LOTE_DB_OPERATION')) {
            define("LOTE_DB_OPERATION", '1');
        }
        if (!$this->getState()->isInstalled()) {
            if ($quietMode) {
                $io->comment("System minimum required tables not found. Installing");
                $this->getState()->installCoreSchema($io, false);
            } else {
                $shouldInstall = $io->confirm('System minimum required tables not found. Do you want to install them?');
                if (!$shouldInstall) { return; }
                else {
                    $this->getState()->installCoreSchema($io);
                }
            }
        }

        $db = $this->getState()->getWriteDb();

        if ($migrations = $this->getRootTableMigrations($this->getState()->getReadDb())) {
            $io->caution("Root tables out of sync, updating...");
            foreach ($migrations as $sql) {
                $this->outputQuery($io, $sql);
                $db->exec($sql);
            }
            $io->success("Root tables updated.");
        }

        if (!$this->getState()->isValidInstalled()) {
            $io->caution('System core out of track with DB. Updating core tables' . PHP_EOL);
            $this->getState()->updateCoreSchema($io);
        }
        $schemaTemplate = $this->buildUpdateSchema($this->getState()->getApps()->has("master"), $virtualOnly);
        $currentSchema = $db->getSchemaManager()->createSchema();
        if ($virtualOnly) {
            $virtualTables = [];
            foreach ($currentSchema->getTables() as $table) {
                $tableName = $table->getShortestName(null);
                if (substr($tableName, 0, 3) === 'sb_' && substr($tableName, -3) === '__c') {
                    $virtualTables[] = $table;
                }
            }
            $currentSchema = new Schema($virtualTables);
        }
        $migration = $currentSchema->getMigrateToSql($schemaTemplate, $db->getDatabasePlatform());
        if (is_array($migration) && count($migration) > 0) {
            foreach ($migration as $sql) {
                if ($simulate) {
                    $this->outputQuery($io, $sql);
                } else {
                    $db->exec($sql);
                }
            }
            if ($simulate) {
                $io->warning("DB Schema updates required");
            } else {
                $io->success("Db Schema Updated");
            }
        } else {
            $io->writeln("<fg=blue;options=bold>Schema is up to date</>");
        }

        $io->writeln('Generated virtual classes');

        $this->syncSettings($io, $virtualOnly, $simulate);
    }

    /**
     * @param Entity $entity
     * @throws \Doctrine\DBAL\Schema\SchemaException
     * @throws \Doctrine\DBAL\DBALException
     */
    public function updateByEntity(Entity $entity)
    {
        $dbSchema = $this->getState()->getWriteDb()->getSchemaManager();
        $schema = new Schema();
        $appSchema = new \BlueSky\Framework\Application\App\Schema();
        $appSchema->getSchemaByEntity($schema, $entity);

        $tableNames = [];
        $tables = $schema->getTables();
        foreach ($tables as $table) {
            $tableNames[] = $table->getShortestName($schema->getNamespaceName());
        }
        $existingTables = [];
        foreach ($dbSchema->listTableNames() as $tableName) {
            if (in_array($tableName, $tableNames)) {
                $existingTables[] = $this->getState()->getWriteDb()->getSchemaManager()->listTableDetails($tableName);
            }
        }
        $existingSchema = new Schema($existingTables);

        $migration = $existingSchema->getMigrateToSql($schema, $this->getState()->getWriteDb()->getDatabasePlatform());

        if (is_array($migration) && count($migration) > 0) {
            foreach ($migration as $sql) {
                $this->getState()->getWriteDb()->exec($sql);
            }
        }

        $cg = new CodeGen($this->getState()->accountReference);
        $this->generateVirtualClassesRecursive($cg, [$entity]);
    }

    private function outputQuery(SymfonyStyle $io, $query)
    {
        if (Strings::startsWith("drop", strtolower($query))) {
            $io->writeln("<fg=red>{$query}</>");
        } elseif (Strings::startsWith("alter", strtolower($query))) {
            $io->writeln("<comment>{$query}</comment>");
        } else {
            $io->writeln("<info>{$query}</info>");
        }
    }

    private function syncSettings(SymfonyStyle $io, bool $virtualOnly, bool $simulate = true)
    {
        $messages = [];
        $schemaManager = $this->getState()->getWriteDb()->getSchemaManager();
        if ($schemaManager->tablesExist(["sb__setting"])) {
            $settings = [];
            $settings[] = ['reference' => 'system.version.number', 'value' => $this->getVersion()];
            foreach ($settings as $s) {
                $currentValue = $this->getState()->getSettings()->get($s['reference']);
                if ($currentValue != $s['value']) {
                    if ($simulate) {
                        $io->warning("Setting update required for '{$s['reference']}', current value = '$currentValue', new value = '{$s['value']}'");
                    } else {
                        $this->getState()->getSettings()->set($s['reference'], $s['value']);
                        $io->note("Setting '{$s['reference']}' updated to '{$s['value']}'");
                    }
                }
            }
            if (empty($messages)) {
                $io->writeln('<fg=blue;options=bold>Settings are up to date</>');
            }
        } else {
            $io->error("Settings table does not exist");
        }

        $this->processRelations($virtualOnly);
        $this->getState()->getSchema()->rebuildEntityCache();
        return $messages;
    }

    private function getVersion()
    {
        $result = 'alpha-dev';
        if (defined("LOTE_VERSION_NUMBER")) {
            $result = LOTE_VERSION_NUMBER;
        }
        return $result;
    }

    public function install()
    {
        if (!$this->getState()->isInstalled()) {
            $db = $this->getState()->getWriteDb();
            $schemaTemplate = $this->buildInstallSchema();
            $currentSchema = $db->getSchemaManager()->createSchema();
            $migration = $currentSchema->getMigrateToSql($schemaTemplate, $db->getDatabasePlatform());
            foreach ($migration as $sql) {
                $db->exec($sql);
            }
            $this->insertSeedData();
        } else {
            $route = $this->getState()->getRoute();
            $modules = $route['module'];
            foreach ($modules as $m) {
                $this->installModule($m);
            }
            $system = $route['system'];
            foreach ($system as $s) {
                $this->installSystem($s);
            }
        }
    }

    public function uninstall()
    {
        if ($this->getState()->isInstalled()) {
            $route = $this->getState()->getRoute();
            $modules = $route['app'];
            foreach ($modules as $m) {
                die('todo');
                //$this->uninstallApp($m);
            }
        }
    }

    private function removeAppEntry($reference)
    {
        $this->getState()->getWriteDb()->delete('sb__app', ['reference' => $reference]);
    }

    private function executeData($version, $type = "Module", $method)
    {
        if ($class = $this->getFileClassName($version . 'Seed.php')) {
            require_once($version . 'Seed.php');
            $className = '\\Lote\\Release\\' . $type . '\\' . $class;
            $c = new $className($this->getState());
            if (method_exists($c, $method)) {
                $data = [];
                $c->{$method}($data);
            }
        }
    }

    private function upData($version, $type = "Module")
    {
        $this->executeData($version, $type, 'up');
    }

    private function downData($version, $type = "Module")
    {
        $this->executeData($version, $type, 'down');
    }

    private function buildUpdateSchema($isMaster = false, bool $virtualOnly = false): Schema
    {
        $toSchema = new Schema();
        if (!$virtualOnly) {
            $this->getCoreSchema($toSchema);
            $this->getProjectSchema($isMaster, $toSchema);

            /**
             * @var Config $v
             * */
            $apps = $this->getState()->getApps()->getAll();
            foreach ($apps as $v) {
                $this->addLibSchema($toSchema, $v->getReference(), $isMaster);
            }
        }

        $this->addVirtualSchemas($toSchema);

        $eventData = new Data();
        $eventData->data['schema'] = $toSchema;
        $this->getState()->getEventManager()->dispatch("core:db:build-update-schema", $eventData);
        return $toSchema;
    }

    /**
     * @return Schema
     * */
    private function buildInstallSchema()
    {
        $route = $this->getState()->getRoute();
        $toSchema = new Schema();
        $this->getCoreSchema($toSchema);
        $this->getProjectSchema($this->isMaster(), $toSchema);

        foreach ($route['app'] as $v) {
            $this->addLibSchema($toSchema, $v);
        }
        return $toSchema;
    }


    /**
     * @return void
     * */
    private function insertSeedData()
    {
        $route = $this->getState()->getRoute();
        $allData = [];

        $this->coreSeed($allData);

        foreach ($route['app'] as $v) {
            $this->insertAppSeed($allData, $v);
        }
    }

    /**
     * @return Schema|false
     * */
    private function coreSeed($data)
    {
        $result = new Schema();
        if ($version = $this->getLatestInstallVersion(LOTE_VERSION_PATH . 'sidrablue')) {
            if ($class = $this->getFileClassName($version . 'Seed.php')) {
                require_once($version . 'Seed.php');
                $className = '\\Lote\\Release\\SidraBlue\\Core\\' . $class;
                $c = new $className($this->state);
                if (method_exists($c, 'up')) {
                    $c->up($data);
                }
                $this->updateAppEntry($version . 'info.json');
            }
        }
        return $result;
    }


    protected function updateAppEntry($fileName)
    {
        if (file_exists($fileName)) {
            $json = json_decode(file_get_contents($fileName), true);
            if ($this->validJsonInfoSchema($json)) {
                $a = new App($this->getState());
                $a->loadByField('reference', $json['reference']);
                $a->type = $json['type'];
                $a->name = $json['name'];
                $a->description = $json['description'];
                $a->reference = $json['reference'];
                $a->enabled = true;
                $a->version = $json['version'];
                $a->save();
            } else {
                dump("INVALID SCHEMA");
            }
        } else {
            dump("NO APP DATA: " . $fileName);
        }
    }

    private function validJsonInfoSchema($json)
    {
        $result = false;
        if (is_array($json) && count($json) > 0) {
            $result = isset($json['type']) && isset($json['name']) && isset($json['reference']) && isset($json['version']) && isset($json['description']);
        }
        return $result;
    }

    private function insertAppSeed(&$allData, $moduleReference, $moduleVersion = false)
    {
        if ($moduleVersion) {
            $version = LOTE_VERSION_PATH . 'component/module/' . $moduleReference . '/' . $moduleVersion . '/';
        } else {
            $version = $this->getLatestInstallVersion(LOTE_VERSION_PATH . 'component/module/' . $moduleReference);
        }
        if ($class = $this->getFileClassName($version . 'Seed.php')) {
            require_once($version . 'Seed.php');
            $className = '\\Lote\\Release\\Module\\' . $class;
            if (class_exists($className)) {
                $c = new $className($this->getState());
                if (method_exists($c, 'up')) {
                    $c->up($allData);
                }
            }
        }
        $this->updateAppEntry($version . 'info.json');
    }

    private function addVirtualSchemas(Schema $toSchema)
    {
        $em = new EntityModel($this->getState());
        $virtualEntities = $em->listByField('is_virtual', true);
        $s = new \BlueSky\Framework\Application\App\Schema();

        $entities = [];
        foreach ($virtualEntities as $virtualEntity) {
            $e = new Entity($this->getState());
            $e->load($virtualEntity['id']);
            $entities[] = $e;
            $s->getSchemaByEntity($toSchema, $e);

            if (strpos($e->reference, 'User\\') === 0) {
                $this->relations[] = [
                    'from_entity_reference' => UserEntity::getObjectRef(),
                    'to_entity_reference' => $e->reference,
                    'from_field_reference' => 'id',
                    'to_field_reference' => 'user_id',
                    'type' => RelationEntity::ONE_TO_ONE,
                    'connection_type' => RelationEntity::CONNECTION_TYPE_VIRTUAL,
                    'app_reference' => RelationEntity::APP_REFERENCE_SYSTEM,
                ];
            }

            $this->getState()->getSchema()->_forceAddEntity($e);
        }

        $codeGen = new CodeGen($this->getState()->accountReference);
        $this->generateVirtualClassesRecursive($codeGen, $entities);
    }

    private function addLibSchema($toSchema, $appReference, $isMaster = false)
    {
        $f = new Finder();
        foreach ($classes = $f->getLibEntityClasses($appReference) as $class) {
            $s = new \BlueSky\Framework\Application\App\Schema();
            if ($s->getSchema($toSchema, $class, $isMaster)) {
                $this->syncClassSchema($class);
                $this->updateRelations($class);
            }
        }
    }

    private function getFileClassName($filePath)
    {
        $result = false;
        if (file_exists($filePath)) {
            $phpSrc = file_get_contents($filePath);
            $classes = $this->getPhpClasses($phpSrc);
            if (isset($classes) && is_array($classes) && isset($classes[0])) {
                $result = $classes[0];
            }
        }
        return $result;
    }

    private function getPhpClasses($phpSrc)
    {
        $classes = [];
        $tokens = token_get_all($phpSrc);
        $count = count($tokens);
        for ($i = 2; $i < $count; $i++) {
            if ($tokens[$i - 2][0] == T_CLASS
                && $tokens[$i - 1][0] == T_WHITESPACE
                && $tokens[$i][0] == T_STRING
            ) {

                $class_name = $tokens[$i][1];
                $classes[] = $class_name;
            }
        }
        return $classes;
    }

    private function getCoreSchema(Schema $schema): Schema
    {
        $f = new Finder();
        foreach ($classes = $f->getCoreEntityClasses() as $class) {
            $s = new \BlueSky\Framework\Application\App\Schema();
            if ($s->getSchema($schema, $class, false)) {
                $this->syncClassSchema($class);
                $this->updateRelations($class);
            }
        }
        return $schema;
    }

    private function getProjectSchema(bool $isMaster, Schema $schema): Schema
    {
        if(!$isMaster) {
            $f = new Finder();
            foreach ($classes = $f->getProjectEntityClasses() as $class) {
                $s = new \BlueSky\Framework\Application\App\Schema();
                if ($s->getSchema($schema, $class, $isMaster)) {
                    $this->syncClassSchema($class);
                    $this->updateRelations($class);
                }
            }
        }
        return $schema;
    }

    private function getLatestInstallVersion($baseFolder)
    {
        $result = false;
        if (is_dir($baseFolder)) {
            $d = new \DirectoryIterator($baseFolder);
            foreach ($d as $v) {
                if ($v->getFileName() != '.' && $v->getFileName() != '..') {
                    $installBase = str_replace('\\', '/', $v->getPathname()) . '/';
                    if (file_exists($installBase . 'Schema.php')) {
                        $result = $installBase;
                    }
                }
            }
        }
        return $result;
    }

    private function syncClassSchema($className): void
    {
        if ($className !== Entity::class && $className !== SchemaField::class) {
            /** @var Base $c */
            $c = new $className($this->getState());
            if (substr($c->getTableName(), 0, 3) === 'sb_') {
                $classEntity = $c->getSchema(); /// this will have been created from the buildEntityClass method

                /** @var SchemaField[] $newFields */
                $newFields = [];

                $e = new Entity($this->getState());
                if ($e->loadByField('reference', $c::getObjectRef())) {
                    $classEntityFields = $classEntity->getEntityFields();
                    foreach ($e->getEntityFields() as $entityField) {
                        $fieldFound = false;
                        foreach ($classEntityFields as $k => $classField) {
                            if ($classField->reference === $entityField->reference) {
                                $fieldFound = true;
                                if ($this->fieldsEqual($classField, $entityField)) {
                                    unset($classEntityFields[$k]);
                                } else {
                                    $newFields[] = $classField;
                                    $entityField->delete();
                                }
                                break;
                            }
                        }
                        if (!$fieldFound) {
                            $fe = new SchemaField($this->getState());
                            if ($fe->loadByFields(['reference' => $entityField->reference, 'schema_entity_ref' => $e->reference])) {
                                $fe->delete();
                            }
                        }
                    }

                    foreach ($classEntityFields as $newField) {
                        $newFields[] = $newField;
                    }
                } else {
                    $e = $classEntity;
                }


                foreach ($newFields as $newField) {
//                    $field = new SchemaField($this->getState());
//                    if ($e->id) {
//                        $field->loadByFields(['reference' => $field->reference, 'schema_entity_ref' => $e->reference]);
//                    }
//                    $field->setData($newField->getDataEncoded());
                    $e->addEntityField($newField);
                }
                $e->save();
                $this->getState()->getSchema()->_forceAddEntity($e);
            }
        }
    }

    private function updateRelations(string $className): void
    {
        $classAnno = new Annotations(new ReflectionClass($className));
        if ($classAnno->hasAnnotation('relation')) {
            $relation = $classAnno->offsetGet('relation');
            $requiredKeys = ['type', 'from_field_reference', 'to_field_reference'];
            $optionalKeysSet1 = ['from_entity_reference', 'to_entity_reference'];

            $requiredKeysCount = 0;
            $optionalKeysSet1Count = 0;
            foreach ($relation as $key => $value) {
                if (in_array($key, $requiredKeys)) {
                    $requiredKeysCount++;
                } elseif (in_array($key, $optionalKeysSet1)) {
                    $optionalKeysSet1Count++;
                }
            }

            $relation['type'] = RelationEntity::KEY_MAP_TYPE[$relation['type']]; /// This will map the name eg ONE_TO_ONE to one_to_one
            $relation['connection_type'] = isset($relation['connection_type']) ? RelationEntity::KEY_MAP_CONNECTION_TYPE[$relation['connection_type']] : RelationEntity::CONNECTION_TYPE_CORE;

            if (
                $requiredKeysCount < count($requiredKeys) &&
                $optionalKeysSet1Count < 1
            ) {
                throw new \Exception("@relation for $className is not valid");
            }

            $appReference = RelationEntity::APP_REFERENCE_SYSTEM;
            if (strpos($className, 'Lote\App\\') === 0) {
                $appReference = substr($className, 9); /// 9 is the char count of "Lote\App\"
                $appReference = substr($appReference, 0, strpos($appReference, '\\'));
            }
            $relation['app_reference'] = $appReference;

            $reference = $className::getObjectRef();
            if (empty($relation['from_entity_reference'])) {
                $relation['from_entity_reference'] = $reference;
            } elseif (empty($relation['to_entity_reference'])) {
                $relation['to_entity_reference'] = $reference;
            }

            $this->relations[] = $relation;
        }
    }

    private function processRelations(bool $virtualOnly): void
    {
        $rm = new RelationModel($this->getState());
        $allRelations = $rm->getAll();
        foreach ($this->relations as $relation) {
            $re = new RelationEntity($this->getState());
            $re->loadByFields([
                'from_entity_reference' => $relation['from_entity_reference'],
                'to_entity_reference' => $relation['to_entity_reference'],
                'from_field_reference' => $relation['from_field_reference'],
                'to_field_reference' => $relation['to_field_reference'],
            ]);
            $re->setData($relation);
            $re->save();
            foreach ($allRelations as $key => $existingRelation) {
                if (
                    $existingRelation['from_entity_reference'] === $relation['from_entity_reference'] &&
                    $existingRelation['to_entity_reference'] === $relation['to_entity_reference'] &&
                    $existingRelation['from_entity_reference'] === $relation['from_entity_reference'] &&
                    $existingRelation['to_field_reference'] === $relation['to_field_reference']
                ) {
                    unset($allRelations[$key]);
                }
            }
        }

        foreach ($allRelations as $redundantRelation) {
            if ($virtualOnly) {
                $entity = $this->getState()->getSchema()->getEntityByReference($redundantRelation['to_entity_reference']);
                if ($entity !== null && $entity->is_virtual) {
                    $re = new RelationEntity($this->getState());
                    if ($re->load($redundantRelation['id'])) {
                        $re->delete();
                    }
                }
            } else {
                $re = new RelationEntity($this->getState());
                if ($re->load($redundantRelation['id'])) {
                    $re->delete();
                }
            }
        }
    }

    /**
     * This is not to be used outside of the buildEntityClass method
     * This only checks a subset of the field data to determine its equality
     *
     * @param SchemaField $field1
     * @param SchemaField $field2
     * @return bool
     */
    private function fieldsEqual(SchemaField $field1, SchemaField $field2): bool
    {
//        $field1Data = $field1->getData();
//        $field2Data = $field2->getData();
        $equal = (
            $field1->entityData['reference'] == $field2->entityData['reference'] &&
            $field1->entityData['schema_entity_ref'] == $field2->entityData['schema_entity_ref'] &&
            $field1->entityData['field_type'] == $field2->entityData['field_type'] &&
            $field1->entityData['db_type'] == $field2->entityData['db_type'] &&
            $field1->entityData['db_options'] == $field2->entityData['db_options'] &&
            $field1->entityData['db_index'] == $field2->entityData['db_index'] &&
            $field1->entityData['name'] == $field2->entityData['name'] &&
            $field1->entityData['default_value'] == $field2->entityData['default_value'] &&
            $field1->entityData['is_mandatory'] == $field2->entityData['is_mandatory'] &&
            $field1->entityData['is_virtual'] == $field2->entityData['is_virtual'] &&
            $field1->entityData['options_structure'] == $field2->entityData['options_structure'] &&
            $field1->entityData['flags'] == $field2->entityData['flags'] &&
            $field1->entityData['is_system_hidden'] == $field2->entityData['is_system_hidden']
        );

        return $equal;
    }

    /**
     * This function will loop through the hierarchy of the entities and create the virtual classes for each one
     *
     * @param CodeGen $cg
     * @param Entity[] $entities
     */
    private function generateVirtualClassesRecursive(CodeGen $cg, array $entities): void
    {
        foreach ($entities as $entity) {
            if ($entity->is_virtual) {
                $cg->generateEntityVirtualClass($entity);
                $this->generateVirtualClassesRecursive($cg, $entity->getChildEntities());
            }
        }
    }

}
