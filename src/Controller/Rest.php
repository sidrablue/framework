<?php
namespace BlueSky\Framework\Controller;

/**
 * Default REST based controller for the Lote Framework
 * @package BlueSky\Framework\Controller
 * @todo - break this up into a Rest and RestPhpcrItem controller
 * */
class Rest extends Request
{

    /**
     * Get the action name of this request, based on the HTTP request type and/or the _method parameter
     * @param array $route - the route data
     * @access protected
     * @return string
     * */
    protected function getActionName($route)
    {
        $this->executedAction = 'unknownAction';
        if(php_sapi_name()=='cli') {
            if(isset($route['action'])) {
                $this->executedAction = $route['action'];
            }
        }
        else {
            /**
             * @var \Symfony\Component\HttpFoundation\Request $r
             * */
            $r = $this->getState()->getRequest();
            $this->executedAction = $r->get("_method");

            if(isset($this->route['_method']) && !empty($this->route['_method'])) {
                $this->executedAction = $this->route['_method'];
            }
            elseif(isset($function) && preg_match('/^[a-z]\w+$/i', $function)) {
                $this->executedAction = strtolower($r->getMethod()).ucfirst($function);
            }
            else {
                if(method_exists($this, $r->getMethod().$this->className)){
                    $this->executedAction = strtolower($r->getMethod()).$this->className;
                }
                elseif(method_exists($this, $r->getMethod())){
                    $this->executedAction = strtolower($r->getMethod());
                }
            }
        }
        return $this->executedAction;
    }

    /**
     * Update the access for an object
     * @access protected
     * @param int $objectId
     * @param string $objectReference
     * @param array $typePermission - permission set for the type
     * @param string $accessType - Access type role|user
     * @return void
     */
    protected function updateAccess($objectId, $objectReference, $typePermission, $accessType = 'role')
    {
        if (is_array($typePermission)) {
            foreach ($typePermission as $typeId => $accessLevel) {
                $ae = new \BlueSky\Framework\Entity\Auth\AccessEntity($this->getState());
                $ae->loadByFields(['object_ref' => $objectReference, 'object_id' => $objectId, 'access_type_id' => $typeId, 'access_type' => $accessType]);
                if ($ae->id && $accessLevel == '') {
                    $ae->delete();
                } elseif ($accessLevel) {
                    $ae->object_ref = $objectReference;
                    $ae->object_id = $objectId;
                    $ae->access_type = $accessType;
                    $ae->access_type_id = $typeId;
                    $ae->access_level = $accessLevel;
                    $ae->save();
                }
            }
        }
    }

    /**
     * Save access roles with selected Ids
     *
     * @access protected
     * @param object $obj - Object
     * @param array $rolesSelected - Selected Roles
     * @param string $accessLevel - Access level to set
     * @param string $accessType - Access type role|user
     * @return void
     */
    protected function saveAccessRoles($obj, $rolesSelected, $accessLevel = 'view', $accessType = 'role')
    {
        // access roles
        $rm = new \BlueSky\Framework\Model\Role($this->getState());
        $roles = $rm->getAllRoles();

        $loteAccess = [];
        if (!empty($roles)) {
            foreach ($roles as $v) {
                if (!empty($rolesSelected) && in_array($v['id'], $rolesSelected)) {
                    $loteAccess[$v['id']] = $accessLevel;
                } else {
                    $loteAccess[$v['id']] = '';
                }
            }
        }

        $this->updateAccess($obj->id, $obj->getTableName(), $loteAccess, $accessType);
    }

}
