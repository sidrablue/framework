<?php
namespace BlueSky\Framework\Controller;

use BlueSky\Framework\Model\UrlPassword;
use BlueSky\Framework\Util\Arrays;

/**
 * Default Request based controller for the Lote Framework
 * @package BlueSky\Framework\Controller
 * @todo - break this up into a Rest and RestPhpcrItem controller
 * */
class Request extends Base
{

    /**
     * Get the action name of this request, based on the HTTP request type and/or the _method parameter
     * @param array $route - the route data
     * @access protected
     * @return string
     * */
    protected function getActionName($route)
    {
        $this->executedAction = 'unknownAction';
        if(php_sapi_name()=='cli') {
            if(isset($route['action'])) {
                $this->executedAction = $route['action'];
            }
        }
        else {
            /**
             * @var \Symfony\Component\HttpFoundation\Request $r
             * */
            $r = $this->getState()->getRequest();
            $this->executedAction = $r->get("_method");

            if(isset($this->route['_method']) && !empty($this->route['_method'])) {
                $this->executedAction = $this->route['_method'];
            }
            elseif(isset($function) && preg_match('/^[a-z]\w+$/i', $function)) {
                $this->executedAction = strtolower($r->getMethod()).ucfirst($function);
            }
            else {
                if(method_exists($this, $r->getMethod().$this->className)){
                    $this->executedAction = strtolower($r->getMethod()).$this->className;
                }
                elseif(method_exists($this, $r->getMethod())){
                    $this->executedAction = strtolower($r->getMethod());
                }
            }
        }
        return $this->executedAction;
    }

    /**
     * Check if this object has a URL restricted password, and if so redirect the user to password validation
     * @access protected
     * @param string $objectRef
     * @param int $objectId
     * @param boolean $skinId
     * @return bool
     * */
    protected function checkObjectUrlPassword($objectRef, $objectId, $skinId = false)
    {
        $result = false;
        $m = new UrlPassword($this->getState());
        $passwords = $m->getPasswordsByObject($objectRef, $objectId);
        if(!$this->isWhiteListedPasswordIp() && !$this->getState()->getUser()->is_admin && count($passwords) > 0) {
            //Assertion: It is restricted
            $passwords = Arrays::getFieldValuesFrom2dArray($passwords, 'password');
            $bag = $this->getState()->getRequest()->getSession()->get("route/password/{$objectRef}/{$objectId}", false);
            if (!$bag || !array_intersect($bag, $passwords)) {
                $uri = trim($this->getState()->getRequest()->getPathInfo(), '/');
                if(!$skinId) {
                    $skinId = $this->getState()->getSettings()->getSkinId();
                }
                $data = ['uri' => $uri, 'object_ref' => $objectRef, 'object_id' => $objectId, 'skin_id' => $skinId];
                $this->redirect($this->getState()->getRequest()->getSchemeAndHttpHost() . "/_access/password-restricted/" . base64_encode(serialize($data)), false);
            }
            else {
                $result = true;
            }
        }
        elseif($this->getState()->getUser()->is_admin) {
            $result = true;
        }
        elseif($this->isWhiteListedPasswordIp()) {
            $result = true;
        }
        return $result;
    }

    /**
     *
     * @todo - Eventually deprecate this function to ensure that proper (possibly HMAC) keys are used for such requests
     * */
    private function isWhiteListedPasswordIp()
    {
        $result = false;
        if($whitelist = $this->getState()->getConfig()->get("lote.request.password.whitelist_ip",false)) {
            $whitelistIps = array_map('trim', explode(",", $whitelist));
            if(array_intersect($whitelistIps, $this->getState()->getRequest()->getClientIps())) {
                $result = true;
            }
        }
        return $result;
    }

    /**
     * Check if this object has a URL restricted passwords
     * @access protected
     * @param string $objectRef
     * @param int $objectId
     * @return bool
     * */
    protected function hasObjectUrlPassword($objectRef, $objectId)
    {
        $m = new UrlPassword($this->getState());
        return count($m->getPasswordsByObject($objectRef, $objectId)) > 0;
    }

}
