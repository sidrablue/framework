<?php
namespace BlueSky\Framework\Controller\Core;

use BlueSky\Framework\Util\Timezone\Ms;

class Icalendar {

    var $fileText;
    var $cal;
    var $eventCount;
    var $todoCount;
    var $lastKey;
    
    private function readFile($file) {
        $this->file = $file;
        $fileText = join("", file($file)); //load file
        $fileText = preg_replace("/[\r\n]{1,} ([:;])/", "\\1", $fileText);
        return $fileText; // return all text
    }

    public function getEventCount() {
        return $this->eventCount;
    }

    public function getTodoCount() {
        return $this->todoCount;
    }

    public function parse($uri) {
        $this->cal = array(); // new empty array

        $this->eventCount = -1;
        $this->fileText = $this->readFile($uri);

        $this->fileText = explode("\n", $this->fileText);
        if (!stristr($this->fileText[0], 'BEGIN:VCALENDAR'))
            return 'error not VCALENDAR';

        foreach ($this->fileText as $text) {

            $text = trim($text);
            if (!empty($text)) {
                list($key, $value) = $this->returnKeyValue($text);

                switch ($text) {
                    case "BEGIN:VTODO":
                        $this->todoCount = $this->todoCount + 1;
                        $type = "VTODO";
                        break;

                    case "BEGIN:VEVENT":
                        $this->eventCount = $this->eventCount + 1;
                        $type = "VEVENT";
                        break;

                    case "BEGIN:VCALENDAR":
                    case "BEGIN:DAYLIGHT":
                    case "BEGIN:VTIMEZONE":
                    case "BEGIN:STANDARD":
                        $type = $value; 
                        break;

                    case "END:VTODO": 
                    case "END:VEVENT":

                    case "END:VCALENDAR":
                    case "END:DAYLIGHT":
                    case "END:VTIMEZONE":
                    case "END:STANDARD":
                        $type = "VCALENDAR";
                        break;

                    default: 
                        $this->addToArray($type, $key, $value);
                        break;
                }
            }
        }
        return $this->cal;
    }


    private function addToArray($type, $key, $value) {
        if ($key == false) {
            $key = $this->lastKey;
            switch ($type) {
                case 'VEVENT': $value = $this->cal[$type][$this->eventCount][$key] . $value;
                    break;
                case 'VTODO': $value = $this->cal[$type][$this->todoCount][$key] . $value;
                    break;
            }
        }

        if (($key == "DTSTAMP") or ($key == "LAST-MODIFIED") or ($key == "CREATED"))
            $value = $this->icalDateToUnix($value);
        if ($key == "RRULE")
            $value = $this->icalRrule($value);
        if (str_contains($key, "DTSTART")  or str_contains($key, "DTEND")) {
            list($key, $value) = $this->icalDtDate($key, $value);
        }

        switch ($type) {
            case "VTODO":
                $this->cal[$type][$this->todoCount][$key] = $value;
                break;

            case "VEVENT":
                $this->cal[$type][$this->eventCount][$key] = $value;
                break;

            default:
                $this->cal[$type][$key] = $value;
                break;
        }
        $this->lastKey = $key;
    }

    private function returnKeyValue($text) {
        preg_match("/([^:]+)[:]([\w\W]+)/", $text, $matches);

        if (empty($matches)) {
            return array(false, $text);
        } else {
            $matches = array_splice($matches, 1, 2);
            return $matches;
        }
    }

    private function icalRrule($value) {
        $rrule = explode(';', $value);
        foreach ($rrule as $line) {
            $rcontent = explode('=', $line);
            $result[$rcontent[0]] = $rcontent[1];
        }
        return $result;
    }

    private function icalDateToUnix($icalDate) {
        // some dates being passed through as timestamp with description/location appended, could not find cause
        $icalDate = str_replace('DESCRIPTION:', '', $icalDate);
        $icalDate = str_replace('LOCATION:', '', $icalDate);
        $icalDate = str_replace('T', '', $icalDate);
        $icalDate = str_replace('Z', '', $icalDate);

        if(strlen($icalDate) > 10) {
            $dt = \DateTime::createFromFormat("Ymd His", $icalDate);
            $value = $dt->getTimestamp();
            return $value;
        }

        return $icalDate;

/*        //ereg('([0-9]{4})([0-9]{2})([0-9]{2})([0-9]{0,2})([0-9]{0,2})([0-9]{0,2})', $icalDate, $date);
        preg_match('/([0-9]{4})([0-9]{2})([0-9]{2})([0-9]{0,2})([0-9]{0,2})([0-9]{0,2})/', $icalDate, $date);

        if ($date[1] <= 1970) {
            $date[1] = 1971;
        }
        return mktime((int)$date[4], (int)$date[5], (int)$date[6], (int)$date[2], (int)$date[3], (int)$date[1]);*/
    }

    private function icalDtDate($key, $value) {

        if (strcmp($key, "DTSTART;VALUE=DATE") === 0 or strcmp($key, "DTEND;VALUE=DATE") === 0) {
            $dt = \DateTime::createFromFormat("Ymd His", $value." 000000");
            $value = $dt->getTimestamp();
        } elseif (strpos($key, "DTSTART;TZID") === 0 or strpos($key, "DTEND;TZID") === 0) {
            $tzstr = explode(';TZID=', $key)[1];
            if(Ms::getOlson($tzstr)) {
                $tzstr = Ms::getOlson($tzstr);
            }
            $dtz = new \DateTimeZone($tzstr);
            $dtstr = explode('T', $value);
            $dt = \DateTime::createFromFormat("Ymd His", $dtstr[0].$dtstr[1], $dtz);
            $value = $dt->getTimestamp();
        } else {
            $myArr=explode("T",$value);
            if(isset($myArr[1])){
                $cdate=$myArr[0]." ".$myArr[1];
            }else{
                $cdate=$myArr[0];
            }
            $value = $this->icalDateToUnix($cdate);
        }
        $key = explode(';',$key)[0];
        return [$key, $value];

        /*$temp = explode(";", $key);

        if (!isset($temp[1])) { // neni TZID
            //$data = str_replace('T', '', $data);
            return array($key, $value);
        }else{
            $key = $temp[0];
            $temp = explode("=", $temp[1]);
            $returnValue[$temp[0]] = $temp[1];
            $returnValue['unixtime'] = $value;

            return array($key, $returnValue);
        }*/
    }

    public function getSortEventList() {
        $temp = $this->getEventList();
        if (!empty($temp)) {
            usort($temp, array(&$this, "icalDtstartCompare"));
            return $temp;
        } else {
            return false;
        }
    }

    private function icalDtstartCompare($a, $b) {
        return strnatcasecmp($a['DTSTART']['unixtime'], $b['DTSTART']['unixtime']);
    }

    private function getEventList() {
        return $this->cal['VEVENT'];
    }

    public function getTodoList() {
        return $this->cal['VTODO'];
    }

    public function getCalenderData() {
        return $this->cal['VCALENDAR'];
    }

    public function getAllData() {
        return $this->cal;
    }

}

?>