<?php
namespace BlueSky\Framework\Controller\Core;

use BlueSky\Framework\Entity\File;
use BlueSky\Framework\Event\Data;
use BlueSky\Framework\Service\Thumb;
use BlueSky\Framework\Util\Strings;
use BlueSky\Framework\Util\Time;

/**
 * Image view controller of the framework
 * @package BlueSky\Framework\Controller\Core
 * */
class Image extends Base
{

    /**
     * @var int $id which will be used to fetch image from the database
     * */
    protected $id;

    /**
     * @var int $age specifies the expiration of cached image(in seconds)
     * */
    protected $age;

    /**
     * @var int $width specifies the width of image to be displayed
     * */
    protected $width;

    /**
     * @var int $height specifies the height of image to be displayed
     * */
    protected $height;

    /**
     * @var int $size specifies the size of image to be displayed. If size is set, width and height will be
     * equal to the size.
     * */
    protected $size;

    /**
     * @var bool $preventCaching - true if no cache headers are to be sent with this image
     * */
    protected $preventCaching;

    /**
     * @var int $debug specifies the type of error display. If equal to 1, text error message will be shown
     * and otherwise a blank image
     * */
    protected $debug;

    /**
     * Get the action name of this request, based on the HTTP request type and/or the _method parameter
     * @access protected
     * @return void
     * */
    protected function renderImage()
    {
        $file = false;
        $e = new File($this->getState());

        if ($this->getVar('id') > 0) {
            if ($e->load($this->getVar('id'))) {
                $file = $e;
            }
        }

        if ($this->getVar('object_ref') && $this->getVar('object_id')) {
            if ($e->loadByFields(['object_ref' => $this->getVar('object_ref'), 'object_id' => $this->getVar('object_id')])) {
                $file = $e;
            }
        }
        if ($file && $this->checkUserAccess($file)) {
            if ($file->file_type == 'image' || $file->extension === 'pdf') {
                $this->getState()->getEventManager()->dispatch('core.image.render', new Data(['entity' => $file]));
                $t = new Thumb($this->getState(), $file);
                if ($width = $this->getVar('width')) {
                    $t->setWidth($width);
                }
                if ($height = $this->getVar('height')) {
                    $t->setHeight($height);
                }
                if ($size = $this->getVar('size')) {
                    $t->setSize($size);
                }
                $t->render();
            }
        }


        die;
    }

    /**
     * Get the action name of this request, based on the HTTP request type and/or the _method parameter
     * @access protected
     * @return void
     * */
    protected function renderImageByReference()
    {
        $keys = $this->getCacheKeys();
        if ($this->checkUserCacheAccess($this->getReference())) {
            if ($this->isCached($keys)) {
                $this->renderFromCache($keys);
            } else {
                $e = new File($this->getState());
                if ($e->loadByField('location_reference', $this->getReference())) {
                    $data = ['file_id' => $e->id];
                    $eventData = new Data($data);
                    $this->getState()->getEventManager()->dispatch('file.download.has_access', $eventData);
                    if ($eventData->getData('has_access', '') === false) {
                        die();
                    }
                    $this->renderImageByEntity($e);
                } elseif ($e->loadByField('reference', $this->getReference()) && $e->reference != $e->location_reference) {
                    $data = ['file_id' => $e->id];
                    $eventData = new Data($data);
                    $this->getState()->getEventManager()->dispatch('file.download.has_access', $eventData);
                    if ($eventData->getData('has_access', '') === false) {
                        die();
                    }
                    $this->renderImageByEntity($e);
                } else {
                    $t = new Thumb($this->getState());
                    $t->displayBlankImage();
                }
            }
        }
    }

    private function checkUserAccess(File $file)
    {
        $result = true;
        $data = [
            'file_id' => $file->id,
        ];

        $eventData = new Data($data);
        $this->getState()->getEventManager()->dispatch('image.render.has_access', $eventData);
        $eventDataResult = $eventData->getData();
        if (isset($eventDataResult['has_access']) && $eventData->getData()['has_access'] === false) {
            $result = false;
        }
        return $result;
    }

    private function checkUserCacheAccess(string $reference)
    {
        $result = true;
        $data = [
            'reference' => $reference,
        ];

        $eventData = new Data($data);
        $this->getState()->getEventManager()->dispatch('image.cache.has_access', $eventData);
        $eventDataResult = $eventData->getData();
        if (isset($eventDataResult['has_access']) && $eventData->getData()['has_access'] === false) {
            $result = false;
        }
        return $result;
    }


    /**
     * Check if a file is private and inaccessible due to the user nor being logged in
     *
     * @access private
     * @param File $e - the file entity to check for
     * @return boolean
     * */
    private function isPrivateAndInaccessible(File $e): bool
    {
        $result = false;
        if (!$e->is_public && $e->lote_access != null && $e->lote_access != 1 && !$this->getState()->getUser()->id) {
            $result = true;
        }
        return $result;
    }

    /**
     * Output an image by its entity
     *
     * @access private
     * @param File $e - the file entity to render from
     * @return void
     * */
    private function renderImageByEntity(File $e): void
    {
        if ($e->file_type == "image" && !$this->isPrivateAndInaccessible($e)) {
            $this->getState()->getEventManager()->dispatch('core.image.render', new Data(['entity' => $e]));
            $t = new Thumb($this->getState(), $e);
            $t->setWidth($this->getWidth());
            $t->setHeight($this->getHeight());
            $t->setSize($this->getSize());
            $t->setAlgorithm($this->getAlgorithm());
            $t->render();
        } else {
            $t = new Thumb($this->getState());
            $t->displayBlankImage();
        }
        die;
    }

    private function getCacheKeys($isPrivate = false)
    {
        return $this->getState()->getFileCache()->getKeyArray($isPrivate, $this->getReference(), $this->getWidth(),
            $this->getHeight(), $this->getSize());
    }

    private function getReference()
    {
        return strtok($this->getVar('reference'), '?');
    }

    private function getKeys()
    {
        return $this->getState()->getFileCache()->getKeyArray($this->getReference(), $this->getWidth(),
            $this->getHeight(), $this->getSize(), $this->getTimestamp());
    }

    private function renderFromCache($keys)
    {
        if ($this->getState()->getFileCache()->has($keys)) {
            /*if (!$this->getState()->getImageServer()->isEnabled() && $this->getState()->getSettings()->get("media.cache.adapter", 'local') == "aws") {
                $c = new Content($this->_state);
                $url = $c->getImageCachedUrl($this->getReference(), $this->getWidth(), $this->getHeight(), $this->getSize(), '');
                header("HTTP/1.1 302 Moved Temporarily");
                header("Location:".$url);
                exit;
            }
            else {*/
                $content = $this->getState()->getFileCache()->get($keys);
                $this->getLoteUpdatedTimestamp($this->getReference());
                if ($this->getTimestamp()) {
                    $etagFile = md5($this->getReference().json_encode($keys));
                    $etagHeader=(isset($_SERVER['HTTP_IF_NONE_MATCH']) ? trim($_SERVER['HTTP_IF_NONE_MATCH']) : false);

                    header("Etag: $etagFile");

                    $loteUpdated = $this->getLoteUpdatedTimestamp($this->getReference());
                    //get the last-modified-date of this very file
                    if (@strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE'])==$loteUpdated || $etagHeader == $etagFile)
                    {
                        header("HTTP/1.1 304 Not Modified");
                        header('Content-Type:image');
                        header("Last-Modified: ".gmdate("D, d M Y H:i:s", $loteUpdated));
                        //header('Cache-Control: public');
                        die;
                    }
                    else {
                        header('Content-Type:image');
                        header("Last-Modified: ".gmdate("D, d M Y H:i:s", $loteUpdated));
                        header("Cache-Control: private, max-age=604800");
                        header('Expires: ' . gmdate('D, d M Y H:i:s', time() + 604800) . ' GMT');
                    }

                } else {
                    header('Content-Type:image');
                    header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
                    header("Cache-Control: post-check=0, pre-check=0", false);
                    header("Pragma: no-cache");
                }
                header('Content-Length: ' . strlen($content));
                echo($content);
            //}
        }
        die;
    }

    private function getLoteUpdatedTimestamp($fileRef) {
        $result = false;
        if($this->getState()->getDbCache()->has("filecache.media.file.".Strings::generateHtmlId($fileRef))) {
            $result = $this->getState()->getDbCache()->get("filecache.media.file.".Strings::generateHtmlId($fileRef));
        }
        else {
            $e = new File($this->getState());
            if ($e->loadByField('location_reference', $this->getReference())) {
                $result = Time::dateToTimestamp($e->lote_updated);
                $this->getState()->getDbCache()->set("filecache.media.file.".Strings::generateHtmlId($fileRef), $result);
            }
        }
        return $result;
    }

    private function getWidth()
    {
        $result = $this->getVar('Width', 0);
        if (!$result) {
            $result = $this->getVar('width', 0);
        }
        return $result;
    }

    private function getHeight()
    {
        $result = $this->getVar('Height', 0);
        if (!$result) {
            $result = $this->getVar('height', 0);
        }
        return $result;
    }

    private function getSize()
    {
        $result = $this->getVar('size', 0);
        if (!$result) {
            $result = $this->getVar('Size', 0);
        }
        return $result;
    }

    private function getTimestamp()
    {
        $result = $this->getVar('ts', false);
        if (!$result) {
            $result = $this->getVar('timestamp', false);
        }
        return $result;
    }

    /**
     * Get the algorithm to use for resizing the image if required
     * @access private
     * @return string|bool
     * */
    private function getAlgorithm()
    {
        return $this->getVar('algorithm', 'auto');
    }

    /**
     * Abstract function which must be implemented by the child to determine whether or not the current request
     * can be met without requiring the full framework state to be loaded
     * @access public
     * @param array $route - the static route
     * @return boolean
     * */
    public function requiresFullFramework($route)
    {
        $result = true;
        if ($route['_method'] == 'renderImageByReference') {
            $result = !$this->isCached($this->getKeys());
        } elseif ($route['_method'] == 'renderImage') {

        }
        return $result;
    }

    private function isCached($keys)
    {
        return $this->getState()->getFileCache()->has($keys);
    }

}
