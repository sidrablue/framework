<?php
namespace BlueSky\Framework\Controller\Core;;

use BlueSky\Framework\Controller\Rest;

class Util extends Rest
{

    protected function ping(){
        $this->getState()->getView()->success = true;
        $this->getState()->getView()->user = ['id' => $this->getState()->getUser()->id];
    }

}