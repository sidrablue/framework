<?php
namespace BlueSky\Framework\Controller\Core;

use BlueSky\Framework\Controller\Rest;
use BlueSky\Framework\Model\File as FileModel;
use BlueSky\Framework\Entity\File as FileEntity;
use BlueSky\Framework\Service\Thumb;
use Imagick;

class File extends Rest
{

    protected function download()
    {
        $id = $this->getVar('id');
        $f = new FileModel($this->getState());
        $f->download($id);
        //should not get to here
    }


    protected function preview()
    {
        $id = $this->getVar('id');
        $f = new FileModel($this->getState());
        $fe = new FileEntity($this->getState());

        $f->download($id, "inline");
        //should not get to here
    }

    /**
     * Download a file by reference, using the default rendering method of "inline"
     * @access protected
     * @return void
     * */
    protected function downloadByReference()
    {
        $reference = $this->getVar('reference');
        $reference = parse_url($reference);
        $reference = $reference['path'];
        $ref = explode("/", $reference);
        $ref[count($ref)-1] = urldecode($ref[count($ref)-1]);
        $reference = implode("/", $ref);
        $f = new FileModel($this->getState());
        $f->downloadByReference($reference);
    }

    /**
     * Force a file to be downloaded as an attachment, attempting to prevent rendering in the browser
     * @access protected
     * @return void
     * */
    protected function attachmentDownloadByReference()
    {
        $reference = $this->getVar('reference');
        $reference = parse_url($reference);
        $reference = $reference['path'];
        $f = new FileModel($this->getState());
        $f->downloadByReference($reference,"attachment");
    }


    /**
     * Open a file in the browser forcing inline, allowing the browser to choose if it will download it or render it
     * @access protected
     * @return void
     * */
    protected function inlineDownloadByReference()
    {
        $reference = $this->getVar('reference');
        $reference = parse_url($reference);
        $reference = $reference['path'];
        $f = new FileModel($this->getState());
        $f->downloadByReference($reference,"inline");
    }

}
