<?php
declare(strict_types=1);

namespace BlueSky\Framework\Controller\Form;


use BlueSky\Framework\Object\Container\BaseContainer;

class StandardMessageContainer extends BaseContainer
{

    /** @var bool $success - true if the form validates correctly */
    public $success = true;

    /** @var string[] $errors - the errors that occurred  */
    public $errors = [];

    /**
     * @param bool $success
     * @return StandardMessageContainer
     */
    public function setSuccess(bool $success): StandardMessageContainer
    {
        $this->success = $success;
        return $this;
    }

    /**
     * @param string[] $errors
     * @return StandardMessageContainer
     */
    public function setErrors(array $errors): StandardMessageContainer
    {
        $this->errors = $errors;
        return $this;
    }

    public function addError(string $fieldRef, string $error): StandardMessageContainer
    {
        $this->errors[$fieldRef] = $error;
        return $this;
    }
}