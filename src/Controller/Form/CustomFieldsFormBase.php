<?php
/**
 * Copyright (c) BlueSky Digital Labs Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Controller\Form;

use BlueSky\Framework\Controller\Form\Standard as FormBase;
use BlueSky\Framework\Util\Input\Filter;

class CustomFieldsFormBase extends FormBase
{
    public function setupEditForm(Filter $filter)
    {
        // $this->verifyField($this->getValues());


        $filter->setRule(
            'name',
            'Please enter a value/Field name cannot start with _',
            function ($value) {
                $result = false;
                if (!empty($value) && substr($value, 0, 1) != '_') {
                    $result = true;
                }
                return $result;
            }
        );
    }

}
