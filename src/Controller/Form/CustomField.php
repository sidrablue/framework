<?php
namespace BlueSky\Framework\Controller\Form;

use BlueSky\Framework\Object\Data\Field\Factory as FieldFactory;
use BlueSky\Framework\Object\Model;
use BlueSky\Framework\Entity;
use BlueSky\Framework\Controller\Rest;
use Aura\Input\Form as AuraForm;
use Aura\Input\Builder;
use BlueSky\Framework\Util\Input\Filter as Filter;

/**
 * Base form controller class
 * */
class CustomField extends Rest
{

    /**
     * @var array $formFields - the fields for the form
     * */
    protected $formFields;

    /**
     * @var int $id - the default parameter for any form
     * */
    protected $id;

    /**
     * @var array $errors - the errors for this form
     * */
    protected $errors;

    /**
     * Generic validation function for the class
     * @access protected
     * @return void
     * */
    protected function validate()
    {
        $this->getState()->getView()->success = false;
        $baseValidated = $this->validateBaseFields();
        $customValidated = $this->validateCustomFields();

        if ($baseValidated && $customValidated) {
            if ($this->id = $this->save($this->getValues(), $this->getCustomData())) {
                $this->getState()->getView()->success = true;
                $this->getState()->getView()->id = $this->id;
            } else {
                $this->getState()->getView()->error = 'Could not complete form save';
            }
        } else {
            $this->getState()->getView()->errors = $this->errors;
        }
    }

    /**
     * Validate the base fields for this form
     * @access protected
     * @return boolean
     * */
    protected function validateBaseFields()
    {
        $form = $this->getNewForm();
        $this->setupEditForm($form->getFilter());
        $values = $this->getValues();
        $form->fill($values);
        $result = $form->filter() && $this->onValidate($values);
        if (!$result) {
            foreach ($form->getFilter()->getAllMessages() as $name => $messages) {
                $this->errors[$name] = $messages;
            }
        }
        return $result;
    }

    /**
     * Validate the base fields for this form
     * @access protected
     * @return boolean
     * */
    protected function validateCustomFields()
    {
        $result = true;
        $customData = $this->getCustomData();
        if (is_array($customData)) {
            foreach ($customData as $id => $value) {
                $f = new Entity\Cf\CfField($this->getState());
                if ($f->load($id)) {
                    $field = FieldFactory::createInstance($this->getState(), $f->field_type, 'user');
                    $field->setDefinition($f);
                    $field->setValue($value);
                    if (!$field->validate()) {
                        $this->errors['_custom'][$f->id] = $field->getErrorText();
                        $result = false;
                    }

                }
            }
        }
        return $result;
    }

    /**
     * Get the custom values
     * @return array|boolean
     * */
    protected function getCustomData()
    {
        return $this->getVar('_custom');
    }

    /**
     * Get a new form object to use for validating input
     * @access protected
     * @return AuraForm
     */
    protected function getNewForm($autoPopulate = true)
    {
        $form = new AuraForm(new Builder, new Filter);
        if ($autoPopulate) {
            foreach ($this->formFields as $v) {
                $form->setField($v);
            }
            $form->setField('id');
        }
        return $form;
    }

    /**
     * Save the form
     * @param array $data - the data for this submission
     *
     * */
    protected function save($data)
    {

    }

    /**
     * Setup the form filter for validating an edit
     * @access protected
     * @param Filter $filter
     * @return void
     */
    protected function setupEditForm(Filter $filter)
    {
        //do nothing
    }

    /**
     * @return array
     * */
    protected function getValues()
    {
        $values = [];
        foreach ($this->formFields as $v) {
            //$values[$v] = $this->getVar($v);
            $values[$v] = $this->getState()->getRequest()->get($v);
        }
        return array_merge($this->getState()->getRequest()->request->all(), $values);
        //return $this->getState()->getRequest()->request->all();
    }

    protected function onValidate()
    {
        return true;
    }

}
