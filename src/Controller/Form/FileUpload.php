<?php
namespace BlueSky\Framework\Controller\Form;

use BlueSky\Framework\Object\Model;
use BlueSky\Framework\Entity;
use BlueSky\Framework\File\Upload\Validator;
use BlueSky\Framework\Controller\Rest;

/**
 * Base form controller class
 * */
class FileUpload extends Rest
{

    /**
     * @var int $id - the id of the file in the sb__file table
     * */
    protected $id;

    /**
     * @var string $fileReference - the reference of the file in the $_FILES array
     * */
    protected $fileReference;

    /**
     * @var Validator $validator - the file validator to use
     * */
    protected $validator;

    /**
     * On create handler, to create a validator
     * @access protected
     * @return void
     * */
    protected function onCreate()
    {
        parent::onCreate();
        $this->validator = new Validator($this->fileReference, true);
    }

    /**
     * Generic validation function for the class
     * @access protected
     * @return void
     * */
    protected function validate()
    {
        $this->getState()->getView()->success = false;
        if ($this->validateFile()) {
            if ($this->id = $this->save()) {
                $this->getState()->getView()->success = true;
                $this->getState()->getView()->id = $this->id;
            } else {
                $this->getState()->getView()->error = 'Could not complete form save';
            }
        } else {
            $this->getState()->getView()->errors = $this->validator->getValidationErrors();
        }
    }

    /**
     * Validate the uploaded file
     * @access protected
     * @return boolean
     */
    protected function validateFile()
    {
        return false;
    }

    /**
     * Save the file to the file store, and return its file ID
     * @access protected
     * @return int - the file_id
     * */
    protected function save()
    {

    }

}
