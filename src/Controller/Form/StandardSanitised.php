<?php
namespace BlueSky\Framework\Controller\Form;

use BlueSky\Framework\Service\InputExploitValidator;

/**
 * Base form controller class
 * */
class StandardSanitised extends Standard
{

    /** @var string[] $whitelistedFields the field names that can have code injection pass through into the system  */
    protected $whitelistedFields = [];

    protected function onValidate(array $values): StandardMessageContainer
    {
        $output = new StandardMessageContainer();
        foreach ($this->formFields as $formField) {
            if (!in_array($formField, $this->whitelistedFields)) {
                $value = $values[$formField] ?? null;
                if ($value !== null && is_string($value)) {
                    if (InputExploitValidator::containsAnyCode($value)) {
                        $output->setSuccess(false)
                            ->addError($formField, 'Code injection detected. Please remove code instances.');
                    }
                }
            }
        }
        return $output;
    }

}
