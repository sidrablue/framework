<?php
namespace BlueSky\Framework\Controller\Form;

use BlueSky\Framework\Object\Model;
use BlueSky\Framework\Entity;
use BlueSky\Framework\Controller\Rest;
use Aura\Input\Form as AuraForm;
use Aura\Input\Builder;
use BlueSky\Framework\Util\Input\Filter as Filter;

/**
 * Base form controller class
 * */
class Standard extends Rest
{

    /**
     * @var array $formFields - the fields for the form
     * */
    protected $formFields;

    /**
     * @var int $id - the default parameter for any form
     * */
    protected $id;

    /**
     * Generic validation function for the class
     * @access protected
     * @return void
     * */
    protected function validate()
    {
        $this->getState()->getView()->success = false;
        $form = $this->getNewForm();
        $this->setupEditForm($form->getFilter());
        $values = $this->getValues();
        $form->fill($values);
        $pass = $form->filter();
        $logData = ['uri'=>$this->getState()->getRequest()->getPathInfo(), 'pass' => $pass];
        $extraValidation = $this->onValidate($values);
        if ($pass && $extraValidation->success) {
            if ($this->id = $this->save($this->getValues() ?? [])) {
                $this->getState()->getView()->success = true;
                $this->getState()->getView()->id = $this->id;
            }
            else {
                if(!$this->getState()->getView()->error){
                    $this->getState()->getView()->error = $logData['error'] = 'Could not complete form save';
                }
            }
        } else {
            $errors = [];
            foreach ($form->getFilter()->getAllMessages() as $name => $messages) {
                $errors[$name] = $messages;
                $logData['errors'][$name] = $messages;
            }
            foreach ($extraValidation->errors as $name => $messages) {
                $errors[$name] = $messages;
                $logData['errors'][$name] = $messages;
            }
            $logData['errors'] = $errors;
            $this->getState()->getView()->errors = $errors;
        }
        $logData['success'] = $this->getState()->getView()->success;
        $logData['values'] = $values;
        $this->getState()->getLoggers()->getAccountLogger("validate")->debug("In Base Form Validate", $logData);
    }

    /**
     * Generic validation function for the class
     * @access protected
     * @return array
     * @todo - use this function in the validate()
     * */
    public function validateCheck($data)
    {
        $result = [];
        $form = $this->getNewForm();
        $this->setupEditForm($form->getFilter());
        $form->fill($data);
        $pass = $form->filter();
        if (!$pass || !$this->onValidate($data ?? [])) {
            foreach ($form->getFilter()->getAllMessages() as $name => $messages) {
                $result[$name] = $messages;
            }
        }
        return $result;
    }

    /**
     * Get a new form object to use for validating input
     * @access protected
     * @return AuraForm
     */
    protected function getNewForm($autoPopulate = true)
    {
        $form = new AuraForm(new Builder, new Filter);
        if ($autoPopulate) {
            foreach ($this->formFields as $v) {
                $form->setField($v);
            }
            $form->setField('id');
        }
        return $form;
    }

    /**
     * Save the form
     * @param array $data - the data for this submission
     *
     * */
    protected function save($data, $additionalData = null) 
    {

    }

    /**
     * Setup the form filter for validating an edit
     * @access protected
     * @param Filter $filter
     * @return void
     */
    protected function setupEditForm(Filter $filter)
    {
        //do nothing
    }

    /**
     * @return array
     * */
    protected function getValues()
    {
        $values = [];
        foreach($this->formFields as $v) {
            $values[$v] = $this->getVar($v);
        }
        return array_merge($values, $this->getState()->getRequest()->request->all());
        //return $this->getState()->getRequest()->request->all();
    }

    protected function onValidate(array $values): StandardMessageContainer
    {
        return new StandardMessageContainer();
    }

}
