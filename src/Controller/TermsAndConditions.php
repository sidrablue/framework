<?php
namespace BlueSky\Framework\Controller;

use BlueSky\Framework\Entity\TermsAndConditions as TermsAndConditionsEntity;
use BlueSky\Framework\Entity\UserTermsAndConditions;
use BlueSky\Framework\Util\Input\Filter as Filter;
use BlueSky\Framework\Controller\Form\Standard as FormBase;
use BlueSky\Framework\Util\Time;

/**
 * Class for Apps operations
 */
class TermsAndConditions extends FormBase
{
    /**
     * @var array $formFields
     * */
    protected $formFields = ['tc_agree', 'tc_id'];

    protected function view()
    {
        $this->viewBase();
        $this->getState()->getView()->setRenderFile('user/terms_and_conditions');
    }

    protected function acceptanceRequired()
    {
        $this->viewBase();
        $this->getState()->getView()->setRenderFile('user/form/terms_and_conditions_required');
    }

    private function viewBase()
    {
        $tce = new TermsAndConditionsEntity($this->getState());
        if ($tce->load($this->getState()->getMasterState()->getSettings()->get('system.terms_and_conditions.id', 0))) {
            $this->getState()->getView()->terms_and_conditions = $tce->getViewData();
            $userTerms = new UserTermsAndConditions($this->getState());
            $termFields = [];
            $termFields['account_ref'] = $this->getState()->accountReference;
            $termFields['user_id'] = $this->getState()->getUser()->id;
            $termFields['terms_and_conditions_id'] = $tce->id;
            if ($userTerms->loadByFields($termFields)) {
                $this->getState()->getView()->user_terms = $userTerms->getData();
            }
        }
    }

    public function hasAcceptedTerms()
    {
        $result = true;
        $tce = new TermsAndConditionsEntity($this->getState());
        if ($tce->load($this->getState()->getMasterState()->getSettings()->get('system.terms_and_conditions.id', 0))) {
            $userTerms = new UserTermsAndConditions($this->getState());
            $termFields = [
                'account_ref' => $this->getState()->accountReference,
                'user_id' => $this->getState()->getUser()->id,
                'terms_and_conditions_id' => $tce->id
            ];
            $result = $userTerms->loadByFields($termFields);
        }
        return $result;
    }

    /**
     * @param Filter $filter
     */
    protected function setupEditForm(Filter $filter)
    {
        $filter->setRule(
            'tc_agree',
            'You must agree to the terms and conditions before you continue to the site.',
            function ($value) {
                return $value == '1';
            }
        );
        $filter->setRule(
            'tc_id',
            'Invalid terms and conditions specified. Please contact the administrator.',
            function ($value, $fields, &$message) {
                $result = false;
                $tce = new TermsAndConditionsEntity($this->getState());
                if ($tce->load($value)) {
                    $termFields['account_ref'] = $this->getState()->accountReference;
                    $termFields['user_id'] = $this->getState()->getUser()->id;
                    $termFields['terms_and_conditions_id'] = $tce->id;
                    $userTerms = new UserTermsAndConditions($this->getState());
                    if (!$userTerms->loadByFields($termFields)) {
                        $result = true;
                    } else {
                        $message = "You have already agreed to these terms and conditions";
                    }
                }
                return $result;
            }
        );
    }

    protected function save($data, $additionalData = null)
    {
        $user = $this->getState()->getUser();
        $tcu = new UserTermsAndConditions($this->getState());

        $termFields['account_ref'] = $this->getState()->accountReference;
        $termFields['user_id'] = $user->id;
        $termFields['terms_and_conditions_id'] = $data['tc_id'];
        $tcu->loadByFields($termFields);

        $tcu->account_ref = $this->getState()->accountReference;
        $tcu->user_id = $user->id;
        $tcu->terms_and_conditions_id = $data['tc_id'];
        $tcu->date_agreed = Time::getUtcNow();
        if($res = $tcu->save()) {
            $this->getState()->getRequest()->getSession()->remove("admin.terms_and_conditions_required");
        }
        return $res;
    }
}