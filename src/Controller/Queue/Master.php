<?php
namespace BlueSky\Framework\Controller\Queue;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DriverManager;
use BlueSky\Framework\Entity;
use BlueSky\Framework\Object\Model;
use BlueSky\Framework\State\Cli;

class Master
{

    /** The state object for this request
     * @var $state Cli
     * */
    private $state;

    /**
     * The name of this actual called class
     * @var string $className
     * */
    protected $className;

    /**
     * Default constructor
     * @param Cli $state
     */
    public function __construct(Cli $state = null)
    {
        $this->_className = substr(get_called_class(), strrpos(get_called_class(), '\\') + 1);
        if($state) {
            $this->state = $state;
        }
        else {
            $this->state = new Cli(null, true);
        }
        $this->onCreate();
    }

    public function __destruct()
    {
        $this->state->tearDown();
    }

    /**
     * On create handler for implementation in child classes
     * */
    protected function onCreate()
    {
        //do nothing
    }

    /**
     * Get the state
     * @return Cli
     * */
    protected function getState()
    {
        return $this->state;
    }

    /**
     * Set the state
     * @param Cli $state
     * */
    protected function setState(Cli $state)
    {
        $this->state = $state;
    }

    /**
     * Get the database name for an account by its sms_number
     * @access protected
     * @param string $smsNumber
     * @return string
     * */
    public function getAccountReferenceBySmsNumber($smsNumber)
    {
        $q = $this->getState()->getMasterDb()->createQueryBuilder();
        $q->select('a.reference')
            ->from('sb_master_account','a')
            ->where("a.sms_number = :sms_number")
            ->setParameter('sms_number', $smsNumber);
        return $q->execute()->fetchColumn();
    }

    /**
     * Get the database name for an account by its mandrill reference
     * @access protected
     * @param string $reference
     * @return string
     * */
    public function getAccountReferenceByMandrillReference($reference)
    {
        $q = $this->getState()->getMasterDb()->createQueryBuilder();
        $q->select('a.reference')
            ->from('sb_master_account','a')
            ->where("a.mandrill_reference = :reference")
            ->setParameter('reference', $reference);;
        return $q->execute()->fetchColumn();
    }

    /**
     * Get the database name for an account by its mandrill reference
     * @access protected
     * @param int $sparkpostId
     * @return string
     * */
    public function getAccountReferenceBySparkPostId($sparkpostId)
    {
        $q = $this->getState()->getMasterDb()->createQueryBuilder();
        $q->select('a.reference')
            ->from('sb_master_account','a')
            ->where("a.sparkpost_id = :sparkpost_id")
            ->setParameter('sparkpost_id', $sparkpostId);
        return $q->execute()->fetchColumn();
    }

}
