<?php
namespace BlueSky\Framework\Controller\Queue;

use BlueSky\Framework\State\Cli;

/**
 * Base queue worker class
 */
abstract class Worker
{

    protected function tearDown()
    {

    }

    abstract public function perform();

}
