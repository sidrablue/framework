<?php
namespace BlueSky\Framework\Controller\Queue;

use BlueSky\Framework\Entity;
use BlueSky\Framework\Object\Model;
use BlueSky\Framework\State\Cli;

class Account
{

    /** The state object for this request
     * @var $state Cli
     * */
    private $state;

    /**
     * The name of this actual called class
     * @var string $className
     * */
    protected $className;

    /**
     * Default constructor
     * @param Cli $state
     */
    public function __construct(Cli $state = null)
    {
        $this->_className = substr(get_called_class(), strrpos(get_called_class(), '\\') + 1);
        if ($state) {
            $this->state = $state;
        }
        $this->onCreate();
    }

    /**
     * On create handler for implementation in child classes
     * */
    protected function onCreate()
    {
        //do nothing
    }

    /**
     * Get the state
     * @return Cli
     * */
    protected function getState()
    {
        return $this->state;
    }

    /**
     * Set the state
     * @param Cli $state
     * */
    protected function setState(Cli $state)
    {
        $this->state = $state;
    }

}
