<?php
namespace BlueSky\Framework\Controller\Access\Url;

use Lote\Module\Page\Entity\Layout;
use Lote\Module\Page\Model\Item;
use Lote\System\Cms\Entity\Skin;
use BlueSky\Framework\Model\Url;
use BlueSky\Framework\Model\User\AutoLogin as UserAutoLogin;
use BlueSky\Framework\Controller\Rest;

/**
 * Default REST based controller for the Lote Framework
 * @package BlueSky\Framework\Controller\Access\Url
 * */
class Manage extends Rest
{

    protected function urlAccess()
    {
        $url = base64_decode($this->getVar('encoded_url'));
        if($data = @unserialize($url)) {
            $url = $data['uri'];
            if($this->getState()->hasStaticMatch($url)) {
                if($url['skin_id']) {
                    $this->getState()->getSettings()->setSkinId($url['skin_id']);
                    $s = new Skin($this->getState());
                    if($s->load($url['skin_id']) && $s->skin){
                        $this->getState()->getView()->setConfigVar('_skin', $s->skin);
                    }
                }
            }
            $this->getState()->getView()->object_id = $data['object_id'];
            $this->getState()->getView()->object_ref = $data['object_ref'];
        }
        else {
            $url = preg_replace('#^/#', '', base64_decode($this->getVar('encoded_url')));
            $this->setUrlAccessSkin($url);
        }
        $this->getState()->getView()->url = trim($url, '/');
        $this->getState()->getView()->setRenderFile('access/url/view');
    }

    private function setUrlAccessSkin($url){
        $u = new Url($this->getState());
        if($match = $u->match($url)){
            if($match['object_ref'] == "sb_page_item"){
                $w = new \Lote\Module\Page\Entity\Item($this->getState());
                $s = new Skin($this->getState());
                if($w->load($match['object_id'])){
                    $skin_to_use = 0;
                    $skin_name_to_use = 0;
                    if($s->load($w->skin_id)){
                        $skin_to_use = $w->skin_id;
                        $skin_name_to_use = $s->skin;
                    }
                    if($skin_to_use == 0){
                        $l = new Layout($this->getState());
                        if($l->load($w->layout_id)){
                            if($s->load($l->skin_id)){
                                $skin_to_use = $s->id;
                                $skin_name_to_use = $s->skin;
                            }
                        }
                    }
                    if($skin_to_use != 0){
                        $this->getState()->getSettings()->setSkinId($skin_to_use);
                        $this->getState()->getView()->setConfigVar('_skin', $skin_name_to_use);
                    }
                }
            }
        }
    }

    protected function autoLogin()
    {
        $m = new UserAutoLogin($this->getState());
        if ($l = $m->getAutoLogin($this->getVar('login_hash'), $this->getVar('user_hash'))) {
            if ($this->getState()->getLogin()->autoLogin($l->user_id)) {
                $this->redirect($l->redirect);
            }
        }
    }

    protected function parentLogin()
    {
        $m = new UserAutoLogin($this->getState());
        if ($l = $m->getAutoLogin($this->getVar('login_hash'), $this->getVar('user_hash'))) {
            if ($this->getState()->getLogin()->autoLogin($l->user_id)) {
                $this->redirect($l->redirect);
            }
        }
    }

    protected function logout()
    {
        $this->getState()->getLogin()->logout();
        $url = $this->getRouteAppPrefix('', true) . '/login';
        $this->redirect($url);
    }

    protected function retrieve()
    {
        $this->getState()->getView()->setRenderFile('login/retrieve');
    }

}