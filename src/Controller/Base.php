<?php
namespace BlueSky\Framework\Controller;

use BlueSky\Framework\State\Base as State;
use BlueSky\Framework\State\Web as Web;
use BlueSky\Framework\Object\Model;
use BlueSky\Framework\Entity;
use BlueSky\Framework\Util\Strings;

class Base
{

    public const VAR_TYPE_MIXED = 0;
    public const VAR_TYPE_STRING = 1;
    public const VAR_TYPE_INT = 2;
    public const VAR_TYPE_BOOL = 3;
    public const VAR_TYPE_FLOAT = 4;
    public const VAR_TYPE_JSON = 5;


    /**
     * The name of this actual called class
     * @var string $className
     * */
    protected $className;

    /**
     * The state object for this request
     * @var State
     * */
    protected $state;

    /**
     * The route variables
     * @var array
     * */
    protected $route = [];

    /**
     * Default results per page
     * @var int
     * */
    protected $resultsPerPage = 20;

    /**
     * The default action of this controller
     * @var string
     * */
    protected $defaultAction;

    /**
     * The executed action of this controller
     * @var string
     * */
    protected $executedAction;

    public function __construct(State $state = null)
    {
        $this->className = substr(get_called_class(), strrpos(get_called_class(), '\\') + 1);
        if ($state) {
            $this->state = $state;
            //$this->_state->getApps()->setupSystem($this->_state, $this->getState()->getView()->getSystemRef());
        }
        $this->onCreate();
    }

    /**
     * On create handler for implementation in child classes
     * */
    protected function onCreate()
    {
        //do nothing
    }

    /**
     * @return Web
     * */
    protected function getState()
    {
        return $this->state;
    }

    /**
     * Void action handler
     * */
    protected function void()
    {

    }

    /**
     * Get a variable from either the route/request data, and if that fails then from a session var if it exists
     * @param string $key - the variable name
     * @param mixed $default - the default value for the variable
     * @param mixed $sessionNamespace - the session namespace
     * @return mixed
     * */
    protected function getVarOrSessionVar($key, $default = null, $sessionNamespace = '')
    {
        return $this->getVar($key, $this->getSessionVar($sessionNamespace, $key, $default));
    }

    /**
     * Get a variable from either the route parameters or from the request data
     * @param string $key - the variable name
     * @param mixed $default - the default value for the variable
     * @return mixed
     * */
    protected function getVar($key, $default = null)
    {
        $result = $default;
        if (isset($this->route[$key])) {
            $result = $this->route[$key];
        } elseif (!$this->getState()->isCli()) {
            $result = $this->getState()->getRequest()->get($key, $default);
            if (!$result) {
                if (strlen(file_get_contents('php://input')) > 0) {
                    $data = json_decode(file_get_contents('php://input'), true);
                    if (is_array($data) && isset($data[$key])) {
                        $result = $data[$key];
                    }
                }
            }
        } elseif ($this->getState()->getView()->{$key}) {
            $result = $this->getState()->getView()->{$key};
        }
        return $result;
    }

    protected function getVarN(string $key, $default = null, int $type = self::VAR_TYPE_MIXED)
    {
        $result = $this->getVar($key, $default);
        if ($result === '' && $default !== '') {
            $result = $default;
        }
        if ($type !== self::VAR_TYPE_MIXED && $type !== self::VAR_TYPE_STRING && $result !== null) {
            switch ($type) {
                case self::VAR_TYPE_BOOL:
                    $result = filter_var($result, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
                    break;
                case self::VAR_TYPE_INT:
                    if (is_numeric($result)) {
                        $result = (int) $result;
                    } else {
                        $result = null;
                    }
                    break;
                case self::VAR_TYPE_FLOAT:
                    if (is_numeric($result)) {
                        $result = (float) $result;
                    } else {
                        $result = null;
                    }
                    break;
                case self::VAR_TYPE_JSON:
                    if (!is_array($result) && is_string($result) && Strings::isJson($result)) {
                        $result = json_decode($result, true);
                    }
                    if (!is_array($result)) {
                        $result = null;
                    }
                    break;
            }
        }
        return $result;
    }

    protected function getInt(string $key, int $default = 0): int
    {
        return intval($this->getVar($key, $default));
    }

    /**
     * Helper function to set a variable in the view
     * @param string $name - the name of the variable
     * @param mixed $value - the value of the variable
     * @return void
     * */
    public function setVar($name, $value)
    {
        if(!$this->getState()->isCli()){
            $this->getState()->getRequest()->request->set($name, $value);
        }
        $this->getState()->getView()->{$name} = $value;
    }

    /**
     * Get a session variable that is assumed to be a temporary browser valued value
     * @param mixed $sessionNamespace - the session namespace
     * @param string $key - the variable name
     * @param mixed $default - the default value for the variable
     * @return mixed
     * */
    protected function getSessionVar($sessionNamespace, $key, $default = null)
    {
        return $this->getState()->getRequest()->getSession()->get($sessionNamespace.$key, $default);
    }

    /**
     * Helper function to set a session variable in the view
     * @param mixed $sessionNamespace - the session namespace
     * @param string $key - the name of the variable
     * @param mixed $value - the value of the variable
     * @return void
     * */
    public function setSessionVar($sessionNamespace, $key, $value)
    {
        $this->getState()->getRequest()->getSession()->set($sessionNamespace.$key, $value);
    }

    /**
     * @param \BlueSky\Framework\State\Base $state
     */
    public function setState(State $state = null)
    {
        $this->state = $state;
        $this->route = $this->getState()->getRoute();
    }

    /**
     * @todo implement action logging
     * */
    protected function getActionName($routeData)
    {
        $this->executedAction = isset($routeData['action']) ? $routeData['action'] . 'Action' : null;
        return $this->executedAction;
    }

    public function run($route)
    {
        $this->route = $route;
        $this->getState()->getEventManager()->dispatch('before.exec');
        $this->getState()->getEventManager()->dispatch('before.action');
        $this->executeAction($this->getActionName($route));
        $this->getState()->getEventManager()->dispatch('after.action');
        $this->getState()->getEventManager()->dispatch('before.render');
        $this->render();
        $this->getState()->getEventManager()->dispatch('after.render');
        $this->getState()->getEventManager()->dispatch('after.execute');
    }

    public function runLite($route)
    {
        $this->route = $route;
        $this->executeAction($this->getActionName($route));
        $this->render();
    }

    /*protected function assignRouteAction()
    {
        $route  = $this->getState()->getRoute();
        $actionName = $this->executedAction;
        if(isset($route['action'])) {
            $actionName = $route['action'];
        }
        elseif(isset($route['_method'])) {
            $actionName = $route['_method'];
        }
        var_dump($actionName);
        $route['_handler']['action'] = $actionName;
        $this->getState()->setRouteData($route);
    }*/

    protected function executeAction($methodName)
    {
        if (method_exists($this, $methodName)) {
            $this->$methodName();
        } elseif (method_exists($this, $this->defaultAction)) {
            $this->{$this->defaultAction}();
        } else {
            $this->$methodName();

            //throw new \Exception($this->state->getTranslator()->trans("class_action_method_not_found",['%class%'=>$this->_className,'%function%'=>$methodName]));
        }
    }

    public function __call($name , array $arguments)
    {
        throw new \Exception("class method not found {$this->className}->{$name}");
    }

    public function render()
    {
        if(!$this->getState()->getView()->isRedirect()) {
            $this->getState()->getResponse()->setContent($this->getState()->getView()->render());
        }
    }

    public function redirect($path, $isPermanent = false)
    {
        $isPermanent?$code=301:$code=302;
        $this->getState()->getView()->redirect($path, $code);
    }

    public function getRouteAppPrefix($default = '', $useParent = false)
    {
        $result = $default;
        $route = $this->state->getRoute();
        if (isset($route['_handler']) && isset($route['_handler']['app'])) {
            $systemRef = $route['_handler']['app'];
            if ($useParent) {
                if(!$this->routePathExists("/$systemRef/login") && isset($route['_handler']) && isset($route['_handler']['parent'])) {
                    $systemRef = $route['_handler']['parent'];
                }
            }
            if ($systemRef == 'admin') {
                $systemRef = '_admin';
            }
            $result = '/' . $systemRef;
        }
        elseif($result=="admin") {
            $result = "_admin";
        }
        return $result;
    }

    protected function routePathExists($path) {
        $result = false;
        foreach($this->getState()->getRouter()->getIterator() as $i) {
            if($i->getPath()==$path) {
                $result = true;
                break;
            }
        }
        return $result;
    }

    /**
     * Set the error text for this request
     * @access protected
     * @param string $error
     * @return void
     * */
    protected function setError($error)
    {
        $this->getState()->getView()->error = $error;
        $this->getState()->getView()->success = false;
    }

    /**
     * Add SB identifiers to a tree array which would contain phpcr UUIDS
     * @param array $items - the items to add identifiers to
     * @access protected
     * @return array
     * */
    protected function addTreeIdentifiers($items)
    {
        $uuids = [];
        foreach ($items as $v) {
            if (isset($v['attr']) && isset($v['attr']['identifier'])) {
                $uuids[] = $v['attr']['identifier'];
            }
        }
        $identifiers = $this->getState()->getIdentifier()->getIdentifiers($uuids, 'phpcr_node');
        foreach ($items as $k => $v) {
            if (isset($v['attr']) && isset($v['attr']['identifier'])) {
                $uid = $v['attr']['identifier'];
                if (isset($identifiers[$uid])) {
                    $items[$k]['attr']['identifier_id'] = $identifiers[$uid];
                    $items[$k]['attr']['id'] = "node_" . $identifiers[$uid];
                }
            }
        }
        return $items;
    }

    /**
     * Add SB identifiers to an array of phpcr node entries
     * @param array $items - the items to add identifiers to
     * @access protected
     * @return array
     * */
    protected function addIdentifiers($items)
    {
        $uuids = [];
        foreach ($items as $v) {
            if (isset($v['uuid'])) {
                $uuids[] = $v['uuid'];
            }
        }
        $identifiers = $this->getState()->getIdentifier()->getIdentifiers($uuids, 'phpcr_node');
        foreach ($items as $k => $v) {
            if (isset($v['uuid'])) {
                $uid = $v['uuid'];
                if (isset($identifiers[$uid])) {
                    $items[$k]['identifier_id'] = $identifiers[$uid];
                }
            }
        }
        return $items;
    }

    /**
     * Get a path from one standard phpcr path to another
     * @param string $fromPath
     * @param string $toPath
     * @param boolean $includeFromNode
     * @param callback $callback
     * @return array
     * */
    protected function getBreadcrumbs($fromPath, $toPath, $includeFromNode = false, $callback = null)
    {
        $crumbs = [];
        $crumbNodes = [];
        if ($includeFromNode) {
            if ($n = $this->getState()->getPhpcrService()->getNodeFromPath($fromPath)) {
                $crumbNodes[] = $this->getState()->getPhpcrService()->getLoteProperties($n);
            }
        }
        $path = $this->getState()->getPhpcrService()->getPathNames($fromPath, $toPath);
        foreach ($path as $k => $v) {
            $crumbNodes[] = $this->getState()->getPhpcrService()->getLoteProperties($k);
        }

        foreach ($crumbNodes as $node) {
            $c = [];
            if(isset($node['identifier_id'])) {
                $c['lote_identifier_id'] = $node['identifier_id'];
            }
            elseif(isset($c['lote_identifier_id'])) {
                $c['lote_identifier_id'] = $node['lote_identifier_id'];
            }
            if (isset($node['type'])) {
                $c['type'] = $node['type'];
            }
            if (isset($node['title'])) {
                $c['title'] = $node['title'];
                if (isset($node['name'])) {
                    $c['name'] = $node['name'];
                }
            } elseif (isset($node['name'])) {
                $c['title'] = $node['name'];
            }
            if (isset($node['url'])) {
                $c['url'] = $node['url'];
            } elseif ($callback) {
                $c['url'] = $callback($c);
            }
            $c['reference'] = $fromPath;
            $crumbs[] = $c;
        }
        return $crumbs;
    }

    /**
     * @return boolean
     * */
    protected function isPreview()
    {
        $isPreview = $this->getVar("preview", false);

        $te = new Entity\Token($this->getState());
        $te->loadByField('public_key', $this->getVar('token'), 0);
        $objectId = $this->getVar('obj_id', 0);

        return ($this->getState()->getUser()->isAdmin() || $te->object_id === $objectId) && ($isPreview && $isPreview !== 'false');
    }

    protected function checkEntityAccess($objectRef, $objectId) {
        return true;
    }

}