<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 * Updated: 20 March 2018 at 01:23 PM AWST
 * Updated By: Parham Bakhtiari <parham@sidrablue.com.au>
 */

declare(strict_types = 1);
namespace BlueSky\Framework\CodeGen;

use BlueSky\Framework\Entity\Schema\Entity;
use BlueSky\Framework\Util\Dir;
use Zend\Code\Generator\ClassGenerator;
use Zend\Code\Generator\DocBlock\Tag;
use Zend\Code\Generator\DocBlockGenerator;
use Zend\Code\Generator\FileGenerator;
use Zend\Code\Generator\MethodGenerator;
use Zend\Code\Generator\PropertyGenerator;
use BlueSky\Framework\Model\Schema\Entity as EntityModel;

/**
 * Class CodeGen
 * @package BlueSky\Framework\Schema
 *
 * This class is responsible for managing the virtual class files
 */
class CodeGen
{

    /** @var string $reference - the account reference */
    private $reference;

    /** @var string $referenceFormatted - the formatted version of the account reference, just upper cased */
    private $referenceFormatted;

    /**
     * CodeGen constructor.
     * @param string $reference - the account reference
     */
    public function __construct(string $reference)
    {
        $this->reference = $reference;
        $this->referenceFormatted = str_replace('_', '', ucwords($this->reference, '_ '));
        if (ctype_digit($this->referenceFormatted[0])) {
            $this->referenceFormatted = substr(APP_NAMESPACE, 0, -1) . $this->referenceFormatted;
        }
    }

    /**
     * This function will clear out the entity files within a reference
     *
     * @return bool - false if a file cannot be deleted. @Note: No early exits
     */
    public function cleanEntityDirectory(): bool
    {
        $result = true;
        $files = glob(LOTE_APP_PATH . 'src/Virtual/Entity/' . $this->referenceFormatted . '/*.php');
        foreach ($files as $file){
            $tempRes = unlink($file);
            if ($result) { /// make sure we only keep result on false, dont break so that other files can still be deleted
                $result = $tempRes;
            }
        }
        return $result;
    }

    /**
     * This will generate the actual virtual source file that will be used by the system
     * the config can be generated through a helper functions
     *
     * @see generateConfig
     *
     * @param Entity $entity
     * @return bool - true on file create
     */
    public function generateEntityVirtualClass(Entity $entity): bool
    {
        $success = false;
        if ($entity->is_virtual) {
            $entityName = substr($entity->class_name, strrpos($entity->class_name, '\\') + 1);

            /** @var DocBlockGenerator $cgDoc - The class level annotator */
            $cgDoc = new DocBlockGenerator();
            $cgDoc->setTag(new Tag\GenericTag('dbTable', $entity->getEntityTableName(true)));

            /** @var ClassGenerator $cg - The actual class object generation class */
            $cg = new ClassGenerator();
            $cg->setName($entityName)
                ->setDocBlock($cgDoc)
                ->setExtendedClass('BlueSky\Framework\Object\Entity\VirtualEntity')
                ->addUse('BlueSky\Framework\Object\Entity\VirtualEntity')
                ->addUse('BlueSky\Framework\Entity\Schema\Field', 'SchemaField')
                ->setNamespaceName(APP_NAMESPACE . 'Virtual\Entity\\' . $this->referenceFormatted);

            $properties = $this->generateEntityProperties($entity);
            $cg->addProperties($properties);

            $methods = $this->generateEntityMethods($entity);
            $cg->addMethods($methods);

            /** @var string $code - The string representation of the Class */
            $code = FileGenerator::fromArray([
                'classes' => [$cg]
            ])->generate();

            $success = $this->saveVirtualFile($entityName, $code);
        }

        return $success;
    }

    /**
     * This function will generate the properties that the class will have (built based on the database values)
     *
     * The return type of the function is an array since it is directly passed through to the zend framework code generator
     * The IDE support for type hinting is not advanced enough to handle generics with the php 7 DS classes
     *
     * @param Entity $entity
     * @return PropertyGenerator[]
     */
    private function generateEntityProperties(Entity $entity): array
    {
        $entityProperties = $entity->getEntityFields();
        $properties = [];
        foreach ($entityProperties as $entityField) {
            /** @var \BlueSky\Framework\Entity\Schema\Field $entityField */
            $fieldName = $entityField->reference;
            $defaultValue = $entityField->default_value;
            if ($defaultValue === '') {
                $defaultValue = null;
            }
            $typeCode = $entityField->field_type;
            $typeDB = $entityField->db_type;

            /** @var PropertyGenerator $property - The property that will be attached to the class */
            $property = new PropertyGenerator($fieldName, $defaultValue);

            /** @var array $varTagArr - The variables below should never be null */
            $varTagArr = [$typeCode];
            if ($fieldName !== 'lote_created' && $fieldName !== 'lote_updated') {
                $varTagArr[] = 'null';
            }
            /** @var DocBlockGenerator $doc - Generate the annotations for the property */
            $doc = DocBlockGenerator::fromArray([
                'tags' => [
                    new Tag\VarTag($fieldName, $varTagArr),
                    new Tag\GenericTag('dbColumn', $fieldName),
                    new Tag\GenericTag('codeType', $typeCode),
                    new Tag\GenericTag('fieldType', $typeDB)
                ]
            ]);

            /** Add additional annotation tags to the property if its a foreign key variable */
            if (!empty($entityField->fk_object)) {
                $doc->setTag(new Tag\GenericTag('entityClass', $entityField->fk_object));
                $doc->setTag(new Tag\GenericTag('populateOnLoad', 'true'));
            }

            $property->setDocBlock($doc);

            $properties[] = $property;
        }

        $properties[] = new PropertyGenerator('TABLE_NAME', $entity->getEntityTableName(true), [ PropertyGenerator::FLAG_PUBLIC, PropertyGenerator::FLAG_CONSTANT ]);

        return $properties;
    }

    private function generateEntityMethods(Entity $entity): array
    {
        $em = new EntityModel();
        $sanitisedName = str_replace('\\', '\\\\', $entity->name);
        $sanitisedName = str_replace("'", "\'", $sanitisedName);
        $tableName = $entity->getEntityTableName(true);
        $methods = [
            new MethodGenerator(
                'getTableName',
                [],
                [ MethodGenerator::FLAG_STATIC, MethodGenerator::FLAG_PUBLIC ],
                "return '{$tableName}';"
            ),
            new MethodGenerator(
                'getObjectRef',
                [],
                [ MethodGenerator::FLAG_STATIC, MethodGenerator::FLAG_PUBLIC ],
                "return '{$entity->reference}';"
            ),
            new MethodGenerator(
                'getObjectName',
                [],
                [ MethodGenerator::FLAG_STATIC, MethodGenerator::FLAG_PUBLIC ],
                "return '{$sanitisedName}';"
            ),
            new MethodGenerator(
                'getEntityLinkKey',
                [],
                [ MethodGenerator::FLAG_STATIC, MethodGenerator::FLAG_PUBLIC ],
                "return '{$em->createEntityLinkReference($tableName)}';"
            ),
        ];

//        $buildSelfSchemaCode = '$schema = parent::buildSelfSchema();' . PHP_EOL;
//        foreach ($entity->getEntityFields() as $k => $field) {
//            $fieldDataIndexed = [];
//            foreach ($field->getData() as $key => $value) {
//                $fieldDataIndexed[] = "'$key' => '$value'";
//            }
//            $fieldDataString = implode(', ', $fieldDataIndexed);
//            $buildSelfSchemaCode .= "\$schema->addEntityField(new SchemaField(null, [{$fieldDataString}]));" . PHP_EOL;
//        }
//        $buildSelfSchemaCode .= 'return $schema;';

//        $methods[] = new MethodGenerator(
//            'buildSelfSchema',
//            [],
//            [ MethodGenerator::FLAG_PROTECTED ],
//            $buildSelfSchemaCode
//        );

        return $methods;
    }

    /**
     * This function will save the virtual file content into the filename specified
     * @Note: filename is just the name of the file/class. Directory path and file extensions are automatically generated
     * @param string $fileName
     * @param string $content
     * @return bool
     */
    private function saveVirtualFile(string $fileName, string $content): bool
    {
        $fileDir = $this->getVirtualFilesDirectory();
        Dir::make($fileDir); /// this will check if directory exists

        return file_put_contents($fileDir . $fileName . '.php', $content) !== false;
    }

    /**
     * This function will get the absolute path to the current account's virtual files directory
     *
     * @return string - directory of the virtual files
     */
    private function getVirtualFilesDirectory(): string
    {
        return LOTE_APP_PATH . 'src/Virtual/Entity/' . $this->referenceFormatted . '/';
    }

}