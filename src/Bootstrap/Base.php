<?php
namespace BlueSky\Framework\Bootstrap;

use BlueSky\Framework\State;
use BlueSky\Framework\Routing\Route;

class Base
{

    /**
     * @var \BlueSky\Framework\State\Web
     */
    private $state;

    /**
     * Default constructor for the Bootstrap object
     * @access public
     * @param null $state
     */
    public function __construct($state = null)
    {
        $this->state = $state;
        if (!$this->state) {
            $this->state = State\Factory::newInstance();
        }
        if(php_sapi_name()!='cli') {
            $this->state->setupCoreRoutes($this->state->getRouter());
            if(!$this->getCoreStaticRoute()) {
                $this->getState()->getApps()->setupRoutes($this->getState()->getRouter());
            }
        }
    }

    /**
     * Check if the route is a core static one that does not require the loading of the app routes to deduce it.
     *
     * This is used to prevent an un-needed database connection
     * @access private
     * @return array|false
     * */
    protected function getCoreStaticRoute()
    {
        $result = false;
        $match = $this->getState()->hasStaticMatch($this->getState()->getRequestUrl());
        if($match && isset($match['_static']) && $match['_static'] == true) {
            $result = $match;
        }
        return $result;
    }

    /**
     * Get the state of this bootstrap
     * @access public
     * @return \BlueSky\Framework\State\Web
     * */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Determine if the current user has access to the specified context
     * @param array $route
     * @return boolean
     */
    protected function hasAccess($route)
    {
        $result = true;
        if (!$this->getState()->getUser()->isAdmin() && isset($route['_context']) && isset($route['_context']['right']) && isset($route['_context']['path'])) {
            $path = $route['_context']['path'];
            $right = $route['_context']['right'];
            $result = $this->getState()->getAccess()->hasAccess($path, $right);
        } elseif (!$this->getState()->getUser()->isAdmin() && isset($route['_context_array']) && is_array($route['_context_array'])) {
            foreach ($route['_context_array'] as $context) {
                $path = $context['path'];
                $right = $context['right'];
                if ($result = $this->getState()->getAccess()->hasAccess($path, $right)) {
                    break;
                }
            }
        } elseif(!$this->getState()->getUser()->isAdmin() && isset($route['_context_function']) && is_array($route['_context_function']) ) {
            $result = false;
            $class = $route['_context_function']['class'];
            $function = $route['_context_function']['function'];
            $params = $route['_context_function']['params'];

            $reflectedClass = new \ReflectionClass($route['_context_function']['class']);
            if($reflectedClass->isInstantiable()) {
                $object = new $class($this->getState());
                if(is_callable([$object, $function])) {
                    if(is_array($params)) {
                        $result = $object->$function($params);
                    }
                }
            }
        }
        return $result;
    }

}
