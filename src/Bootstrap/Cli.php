<?php
namespace BlueSky\Framework\Bootstrap;

use BlueSky\Framework\Controller\Request as RequestController;

class Cli extends Base
{

    /**
     * Get the state of this bootstrap
     * @access protected
     * @return \BlueSky\Framework\State\Cli
     * */
    public function getState()
    {
        return parent::getState();
    }

    /**
     * Get the controller for a requested based on the provided route data
     * @param array $route
     * @return Base
     */
    private function getController($route)
    {
        if(isset($route['controller'])  && class_exists(''.$route['controller'])) {
            $controller = new $route['controller']($this->getState());
        }
        else{
            $controller = new RequestController($this->getState());
        }
        return $controller;
    }


    public function execute()
    {
        $this->getState()->getLoggers()->getAccountLogger("base")->info("Begin Bootstrap CLI Execution");
        if($route = $this->getState()->getRoute()){
            $this->getState()->getLoggers()->getAccountLogger("base")->info('Route matched', $route);
            $this->getState()->getApps();
            //check permissions
            //$this->setupUser();
            if(true || $this->hasAccess($route)) {
                /** @var \BlueSky\Framework\Controller\Base $controller */
                if($controller = $this->getController($route)){
                    $this->getState()->getLoggers()->getAccountLogger("base")->info("Controller Found", [get_class($controller)]);
                    $controller->run($route);
                }
                else {
                    $this->getState()->getLoggers()->getAccountLogger("base")->notice("No valid Controller", [$route]);
                }
            }
            else {
                $this->getState()->getLoggers()->getAccountLogger("base")->notice("No permissions for context", [$route]);
                $this->getState()->getView()->loginRequired();
            }
        }
        else {
            $this->getState()->getLoggers()->getAccountLogger("base")->notice("No valid controller", ['controller'=>$this->getState()->getRequest()->getArgv("controller")]);
            echo $this->getState()->getView()->render();
        }
    }

}