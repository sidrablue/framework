<?php
namespace BlueSky\Framework\Bootstrap;

class Factory
{
    public static function newInstance()
    {
        if(php_sapi_name()=='cli') {
            return new Cli();
        }
        else {
            return new Web();
        }
    }
}