<?php
namespace BlueSky\Framework\Auth;

use BlueSky\Framework\Entity\Auth\Token as AuthTokenEntity;
use BlueSky\Framework\Entity\User\User as UserEntity;
use BlueSky\Framework\Entity\Auth\Session as AuthSession;
use BlueSky\Framework\Model\User\User;
use BlueSky\Framework\Service\Jwt;
use BlueSky\Framework\State\Web as WebState;
use BlueSky\Framework\Auth\Login\Method;
use BlueSky\Framework\Auth\Login\Credentials;
use BlueSky\Framework\Util\Time;


class Session
{

    /**
     * @var WebState $state
     */
    private $state;

    /**
     * @var Method\Base $state
     */
    private $method;

    /**
     * @var boolean $isNewLogin
     */
    private $isNewLogin;

    public function __construct(WebState $state)
    {
        $this->state = $state;
        $this->authSession = new AuthSession($state);
    }

    /**
     * @param boolean $acceptInput - true if the login check is meant to account for a new login attempt
     * @return boolean
     * */
    public function login($acceptInput = false)
    {
        $m = new User($this->state);
        $this->isNewLogin = $acceptInput;
        $result = false;
        $this->method = Method\Factory::getInstance($this->state, '');
        $credentials = Credentials\Factory::getInstance($this->state->getRequest(), $acceptInput);

        if ($credentials) {
            $this->method = Method\Factory::getInstance($this->state, 'lote', $credentials);
            if ($this->method->login()) {
                $result = true;
            } elseif ($credentials->getUsername() && !$m->usernameExists($credentials->getUsername()) &&
                $this->state->getSettings()->get('login.ldap', false)) {
                $this->method = Method\Factory::getInstance($this->state, 'ldap', $credentials);
                return $this->method->login();
            }
        }
        return $result;
    }

    /**
     * Generate a login token to use in future login requests
     * @param int|bool $userId - the ID of the user that this token is for
     * @param bool $linkToDb - true to create a corresponding DB entry for this token
     * @param int $expires - when this token expires
     * @return string
     * */
    public function generateToken($userId = false, $linkToDb = false, $expires = 28880)
    {
        $jwtService = new Jwt();
        $tokenJwtId = false;
        $tokenKey = $this->state->getSettings()->getSettingOrConfig("login.jwt.key", false);
        if (!$userId) {
            $userId = $this->state->getUser()->id;
        }
        
        if ($linkToDb) {
            $now = Time::getUtcObject('now');
            $now->add(new \DateInterval('PT' . $expires . 'S'));

            $ate = new AuthTokenEntity($this->state);
            $ate->object_ref = UserEntity::TABLE_NAME;
            $ate->object_id = $userId;
            $ate->object_key = 'login';
            $ate->datetime_expires = $now->format("Y-m-d H:i:s");
            $ate->public_key = uniqid();
            $ate->private_key = uniqid();
            $ate->save();

            $tokenJwtId = $ate->public_key;
            $tokenKey = $ate->private_key;
        }
        $token = $jwtService->generateTokenString(
            $userId,
            $this->state->getUrl()->getBaseUrl(),
            $expires,
            $tokenKey,
            $tokenJwtId
        );
        return $token;
    }

    public function updateToken(AuthTokenEntity $ate, int $userId, int $expires = 28880): string
    {
        $jwtService = new Jwt();
        $tokenJwtId = false;
        $tokenKey = $this->state->getSettings()->getSettingOrConfig("login.jwt.key", false);

        $now = Time::getUtcObject('now');
        $now->add(new \DateInterval('PT' . $expires . 'S'));

        $ate->object_ref = UserEntity::TABLE_NAME;
        $ate->object_id = $userId;
        $ate->object_key = 'login';
        $ate->datetime_expires = $now->format("Y-m-d H:i:s");
        $ate->public_key = uniqid();
        $ate->private_key = uniqid();
        $ate->save();

        $tokenJwtId = $ate->public_key;
        $tokenKey = $ate->private_key;

        $token = $jwtService->generateTokenString(
            $userId,
            $this->state->getUrl()->getBaseUrl(),
            $expires,
            $tokenKey,
            $tokenJwtId
        );
        return $token;
    }

    /**
     * Perform a login to the system as the specified user ID
     * @access public
     * @param int $userId - the users ID
     * @param int $masqueradeUserId - the user ID to log back in as, after the logout of hte current session
     * @return boolean - true if the login succeeded
     * */
    public function autoLogin($userId, $masqueradeUserId = 0)
    {
        $result = false;
        $credentials = new Credentials\Auto($this->state->getRequest());
        $credentials->setUserId($userId);
        $credentials->setMasqueradeUserId($masqueradeUserId);
        $this->method = Method\Factory::getInstance($this->state, 'auto', $credentials);
        if($this->method->login()) {
            $result = true;
        }
        return $result;
    }

    /**
     *
     * */
    public function passwordResetRequired() {
        $result = false;
        if($this->method) {
            $result = $this->method->passwordResetRequired();
        }
        return $result;
    }

    /**
     * @todo - the update password function has moved and needs to be updated.
     * */
    public function updateLoggedInPassword($password, &$errorMessage, $userData = false)
    {
        $result = false;
        if(!$userData) {
            $userData = $this->state->getUser()->getData();
        }
        if(isset($userData) && isset($userData['source']) && $userData['source']=='ldap') {
            $method = Method\Factory::getInstance($this->state, 'ldap', null);
            if($method->updatePassword($userData['username'], $password, $errorMessage)){
                $this->state->getUser()->updatePassword($password, $errorMessage, $userData['username']);
                $result = true;
            }
        }
        else {
            $errorMessage = 'Passwords can only be updated for LDAP users. This user is a local user.';
        }

        return $result;
    }

    /**
     * @return boolean
     * */
    public function logout()
    {
        $result = false;

        if($credentials = Credentials\Factory::getInstance($this->state->getRequest(), false)) {
            $this->method = Method\Factory::getInstance($this->state, 'lote', $credentials);
            if($this->method->logout()) {
                $result = true;
            }
        }
        return $result;
    }

    /**
     * @return string
     * */
    public function getLoginError()
    {
        return $this->method->getErrorMessage();
    }

    /**
     * @return int|string
     * */
    public function getLoginErrorCode()
    {
        return $this->method->getErrorCode();
    }

    /**
     * @return string
     * */
    public function getUsername()
    {
        return $this->method->getUsername();
    }

    /**
     * @return string
     * */
    public function getPassword()
    {
        return $this->method->getPassword();
    }

    /**
     * @return boolean
     * */
    public function isNewLogin()
    {
        return $this->isNewLogin;
    }

}
