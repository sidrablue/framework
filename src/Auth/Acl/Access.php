<?php
namespace BlueSky\Framework\Auth\Acl;

use BlueSky\Framework\Entity\Role as RoleEntity;
use BlueSky\Framework\Entity\UserRole as UserRoleEntity;
use BlueSky\Framework\Model\Role as RoleModel;
use Doctrine\DBAL\Connection;
use BlueSky\Framework\Event\Data;
use BlueSky\Framework\Object\Entity\Base as BaseEntity;
use BlueSky\Framework\Object\State;
use BlueSky\Framework\Object\Model\Base as BaseModel;

class Access extends State
{

    /**
     * @var array $groupAccess - The group access permissions
     * */
    protected $groupAccess = false;
    protected $roleAccess = false;

    /**
     * @var array $accessSequence - the hierarchical array of permissions and their level. The higher the level the greater the access
     * */
    protected $accessSequence = ['none' => 0, 'view' => 1, 'edit' => 2, 'administer' => 3];

    /**
     * Check if a user has access to a specific context
     * @param $path
     * @param $right
     * @param $ignoreAdmin
     * @return boolean
     */
    public function hasAccess($path, $right, $ignoreAdmin = false, $checkAppAccess = true)
    {
        $result = true;
        if (!$this->getState()->getUser()->is_admin || $ignoreAdmin === true) {
            $this->getState()->getApps(); //ensure that the apps are loaded
            if ($this->getState()->getContext()->isRestricted($path)) {
                if (!$this->getState()->getUser()->id) {
                    $result = false;
                } else {
                    $result = $this->checkAccess($path, $right, $ignoreAdmin, $checkAppAccess);
                }
            }
        }
        return $result;
    }

    public function hasRole(string $roleReference): bool
    {
        $rm = new RoleModel($this->getState());
        return $rm->userHasRole($this->getState()->getUser()->id, $roleReference);
    }

    public function getPrimaryUserRole(int $userId = 0): string
    {
        $rm = new RoleModel($this->getState());
        $userRole = $rm->getPrimaryUserRole($userId > 0 ? $userId : $this->getState()->getUser()->id);
        return (string) !empty($userRole) ? $userRole['name'] : "";
    }

    /**
     * Check if a user has access for a specific route in a given path
     * @access private
     * @param string $path - the path, such as "admin.user"
     * @param string $right - the right, such as "view", "edit", "administer"
     * @param boolean $ignoreAdmin - True if access checks ignore admin status
     * @param boolean $checkAppAccess - True if app access levels are checked
     * @return boolean
     * */
    private function checkAccess($path, $right, $ignoreAdmin, $checkAppAccess)
    {
        $result = false;
        if($checkAppAccess) {
            $result = $this->checkAppAccess($path, $right, $ignoreAdmin);
        }
        $this->loadGroupAccess();
        $originalPath = $path;
        while ($path && !$result) {
            if (isset($this->groupAccess[$path])) {
                $permission = $this->groupAccess[$path];
                if ($right == $permission || ($right == 'view' && $permission != 'none') || ($right == 'edit' && $permission == 'administer')) {
                    $result = true;
                }
                break;
            } else {
                $path = substr($path, 0, strrpos($path, '.'));
            }
        }

        $path = $originalPath;
        $this->loadRoleAccess();
        while ($path && !$result) {
            if (isset($this->roleAccess[$path])) {
                $permission = $this->roleAccess[$path];
                if ($right == $permission || ($right == 'view' && $permission != 'none') || ($right == 'edit' && $permission == 'administer')) {
                    $result = true;
                }
                break;
            } else {
                $path = substr($path, 0, strrpos($path, '\\'));
            }
        }
        return $result;
    }

    /**
     * Check if a user has access for a specific route in a given path via event dispatch
     *
     * @access private
     * @param string $path - the path, such as "admin.user"
     * @param string $right - the right, such as "view", "edit", "administer"
     * @param boolean $ignoreAdmin - True if access checks ignore admin status
     * @return boolean
     * */
    private function checkAppAccess($path, $right, $ignoreAdmin)
    {
        $data = new Data();
        $data->setData([
            'path' => $path,
            'right' => $right,
            'ignoreAdmin' => $ignoreAdmin,
        ]);
        $this->getState()->getEventManager()->dispatch('app.has_access', $data);

        return $data->getData('has_access', false);;
    }

    /**
     * Load the group access for the current user, which is the access of all of that users groups combined
     * @access private
     * @return void
     * */
    private function loadGroupAccess()
    {
        if ($this->groupAccess === false && $this->getState()->getGroupService()->getGroupIds()) {
            $this->groupAccess = [];
            $q = $this->getState()->getReadDb()->createQueryBuilder();
            $q->select("a.*,a.access_level as mode")
                ->from("sb__auth_access_context", "a")
                ->leftJoin("a", "sb__group", "g", "g.id = a.object_id and a.object_ref = 'sb__group'")
                ->where("g.id in (:groups)")
                ->setParameter("groups", $this->getState()->getGroupService()->getGroupIds(), Connection::PARAM_INT_ARRAY)
                ->andWhere("a.lote_deleted is null")
                ->andWhere("g.lote_deleted is null")
                ->addOrderBy("g.sort_order");
            $allRows = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
            foreach ($allRows as $row) {
                if ($this->isHigherAccess($row)) {
                    $this->groupAccess[$row['context']] = $row['mode'];
                }
            }
        }
    }

    /**
     * Check if a given row has higher access than is currently defined for the context within that row
     * @access private
     * @param array $row
     * @return boolean
     * */
    private function isHigherAccess(array $row)
    {
        if ($result = isset($row['context'])) {
            if (isset($this->groupAccess[$row['context']])) {
                $currentAccess = $this->groupAccess[$row['context']];
                if (isset($this->accessSequence[$currentAccess]) && isset($this->accessSequence[$row['mode']])) {
                    $result = $this->accessSequence[$row['mode']] > $this->accessSequence[$currentAccess];
                }
            }
        }
        return $result;
    }

    /**
     * Load the role access for the current user, which is the access of all of that users roles combined
     * @access private
     * @return void
     * */
    private function loadRoleAccess()
    {
        if ($this->roleAccess === false) {
            $this->roleAccess = [];
            $q = $this->getState()->getReadDb()->createQueryBuilder();
            $q->select("a.*,a.access_level as mode")
                ->from("sb__auth_access_context", "a")
                ->leftJoin("a", "sb__role", "r", "r.id = a.object_id and a.object_ref = 'sb__role'")
                ->leftJoin('r', 'sb__user_role', 'ur', 'ur.role_id = r.id')
                ->andWhere('ur.user_id = :userId')
                ->setParameter('userId', $this->getState()->getUser()->id)
                ->andWhere("a.lote_deleted is null")
                ->andWhere("r.lote_deleted is null");
            $allRows = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
            foreach ($allRows as $row) {
                if ($this->isHigherRoleAccess($row)) {
                    $this->roleAccess[$row['context']] = $row['mode'];
                }
            }
        }
    }

    /**
     * Check if a given row has higher access than is currently defined for the context within that row
     * @access private
     * @param array $row
     * @return boolean
     * */
    private function isHigherRoleAccess(array $row)
    {
        if ($result = isset($row['context'])) {
            if (isset($this->roleAccess[$row['context']])) {
                $currentAccess = $this->roleAccess[$row['context']];
                if (isset($this->accessSequence[$currentAccess]) && isset($this->accessSequence[$row['mode']])) {
                    $result = $this->accessSequence[$row['mode']] > $this->accessSequence[$currentAccess];
                }
            }
        }
        return $result;
    }

    /**
     * Check if the current user has access to a given entity
     * @access public
     * @var BaseEntity $entity
     * @return boolean
     * */
    public function hasEntityAccess($entity)
    {
        $return = true;
        $data = $entity->getData();
        if (is_null($data['lote_access'])) {

        } elseif ($data['lote_access'] === 0 || $data['lote_access'] === '0') {
            if (!$this->getState()->getUser()->id) {
                $return = false;
            }
        } elseif ($data['lote_access'] == -1) {
            if (!$this->getState()->getUser()->is_admin) {
                $obj = new BaseModel($this->getState());
                $count = $obj->checkAccessForEntity($entity);
                if ($count == 0) {
                    $return = false;
                }
            }
        }
        return $return;
    }

    /**
     * Sets primary role for user, return true on success
     * @access public
     * @var string $role
     * @var int $userId
     * @return boolean
     */
    public function setPrimaryRole(string $role, int $userId = 0): bool
    {
        $rm = new RoleModel($this->getState());
        return $rm->setPrimaryRole($role, $userId > 0 ? $userId : $this->getState()->getUser()->id);
    }

}
