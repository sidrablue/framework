<?php
namespace BlueSky\Framework\Auth\Acl;

class Context
{

    /**
     * @var array $access
     * */
    protected $context = [];

    /**
     * Add
     * */
    public function add($context, Array $data)
    {
        if($this->startsWith($context, '.') || $this->endsWith($context, '.') || (strpos($context,'..') !== false)){
            return false;
            //throw new \Exception("Invalid context supplied");
        }
        $data['reference'] = $context;
        $this->context[$context] = $data;
    }

    private function startsWith($haystack, $needle)
    {
        return $needle === "" || strpos($haystack, $needle) === 0;
    }

    private function endsWith($haystack, $needle)
    {
        return $needle === "" || substr($haystack, -strlen($needle)) === $needle;
    }


    public function get($key = null)
    {
        $result = '';
        if($key && isset($this->context[$key])) {
            $result = $this->context[$key];
        }
        elseif(!$key) {
            $result = $this->context;
        }
        return $result;
    }

    public function isRestricted($path)
    {
        $restricted = false;

        while ($path) {
            if(isset($this->context[$path])) {
                $data = $this->context[$path];
                $restricted = isset($data['restricted']) && $data['restricted'] == true;
                break;
            } else {
                $path = substr($path, 0, strrpos($path, '\\'));
            }
        }
        return $restricted;
    }

}
