<?php
namespace BlueSky\Framework\Auth\Login\Variables;

/**
 * Factory for generating an instance of a Variable
 * @package BlueSky\Framework\Auth\Variables
 * @see BlueSky\Framework\Auth\Login\Variables\Variable
 * */
class Factory
{

    /**
     * Create an instance of Variable
     * @param  $state
     * @param string $reference
     * @return Variable
     * */
    public static function createInstance($state, $reference = "text")
    {
        $className = __NAMESPACE__.'\Variable';
        return new $className($state);
    }

}
