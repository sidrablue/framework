<?php
namespace BlueSky\Framework\Auth\Login\Variables;

use BlueSky\Framework\Object\State as BaseState;

 class Variable extends BaseState implements VariableInterface
{

    /**
     * Get a session variable that is assumed to be a temporary browser valued value
     * @param mixed $sessionNamespace - the session namespace
     * @param string $key - the variable name
     * @param mixed $default - the default value for the variable
     * @return mixed
     * */
    public function get($sessionNamespace, $key, $default = null)
    {
        return $this->getState()->getRequest()->getSession()->get($sessionNamespace . $key, $default);
    }

    /**
     * Function to set a session variable in the view
     * @param mixed $sessionNamespace - the session namespace
     * @param string $key - the name of the variable
     * @param mixed $value - the value of the variable
     * @return void
     * */
    public function set($sessionNamespace, $key, $value)
    {
        $this->getState()->getRequest()->getSession()->set($sessionNamespace.$key, $value);
    }
}
