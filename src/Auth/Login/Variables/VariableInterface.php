<?php
namespace BlueSky\Framework\Auth\Login\Variables;

use BlueSky\Framework\State\Base as BaseState;

/**
 * Interface VariableInterface
 *
 * @package BlueSky\Framework\Auth\Login\Variables
 *  */
interface VariableInterface
{

    /**
     * Get the variable object
     * @access public
     * @return BaseState
     */
    public function get($sessionNamespace, $key, $default = null);

    /**
     * Set the variable object
     * @access public
     * @param BaseState $state
     * @return void
     */
    public function set($sessionNamespace, $key, $value);

}
