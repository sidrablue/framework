<?php
namespace BlueSky\Framework\Auth\Login\Credentials;

/**
 * HTTP Session authentication credentials class
 * @package BlueSky\Framework\Auth\Credentials
 * */
class Session extends Base
{

    /**
     * Load and determine the username and password from the session
     * @throws \Exception
     * @return void
     */
    protected function loadCredentials()
    {
        if($this->request->hasSession()) {
            $this->token = $this->request->headers->get("Authorization", false);
            $this->username = $this->request->getSession()->get('username', '');
            $this->password = $this->request->getSession()->get('hash', '');
            $this->userId = $this->request->getSession()->get('_lote_login_user_id', '');
        }
    }

}
