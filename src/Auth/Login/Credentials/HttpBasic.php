<?php
namespace BlueSky\Framework\Auth\Login\Credentials;

/**
 * HTTP Basic authentication credentials class
 * @package BlueSky\Framework\Auth\Credentials
 * */
class HttpBasic extends Base
{

    /**
     * Load and determine the username and password from HTTP Basic parameters
     * @throws \Exception
     * @return void
     */
    protected function loadCredentials()
    {
        $this->token = $this->request->server->get('HTTP_X_LOTE_TOKEN', '');
        $this->username = $this->request->server->get('PHP_AUTH_USER', '');
        $this->password = $this->request->server->get('PHP_AUTH_PW', '');
    }

}
