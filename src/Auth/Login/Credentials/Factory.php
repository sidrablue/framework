<?php

namespace BlueSky\Framework\Auth\Login\Credentials;

use Symfony\Component\HttpFoundation\Request;

/**
 * Factory for generating an instance of the login credentials, based upon the known and provided data
 * @package BlueSky\Framework\Auth\Credentials
 * @see BlueSky\Framework\Auth\Credentials\Base
 * */
class Factory
{

    /**
     * Get the credentials for the current login request, based on a priority of identifying supplied credentials.
     * The priority is in the order of:
     *  1. HTTP Digest
     *  2. HTTP Basic
     *  3. HTTP Post
     *  4. Session
     *  5. URL parameters
     * @param Request $request - the object containing the request parameters
     * @param boolean $acceptInput - true if POST and GET input are to be accepted as valid login credential sources
     * @param boolean $acceptAuto - true if auto login requests are to be acceptable
     * @return Base
     * */
    public static function getInstance(Request $request, $acceptInput = false, $acceptAuto = false)
    {
        $result = null;
        if ($request->server->has('PHP_AUTH_DIGEST')) {
            $result = new HttpDigest($request);
        } elseif ($request->server->has('PHP_AUTH_USER') || $request->headers->has('X-LOTE-TOKEN')) {
            $result = new HttpBasic($request);
        } elseif ($request->headers->has("X-API-KEY")) {
            $result = new Api($request);
        } elseif ($acceptInput && $request->isMethod('post')) {
            $result = new Post($request);
        } elseif ($request->hasSession() && $request->getSession()->get("hash", false)) {
            $result = new Session($request);
        } elseif ($acceptAuto) {
            $result = new Auto($request);
        }
        return $result;
    }

}
