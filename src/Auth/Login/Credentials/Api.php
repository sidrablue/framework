<?php

namespace BlueSky\Framework\Auth\Login\Credentials;

/**
 * HTTP Session authentication credentials class
 * @package BlueSky\Framework\Auth\Credentials
 * */
class Api extends Base
{

    /**
     * Load and determine the username and password from the session
     * @throws \Exception
     * @return void
     */
    protected function loadCredentials()
    {
        $this->token = $this->request->headers->get("X-API-KEY");
    }

}
