<?php
namespace BlueSky\Framework\Auth\Login\Credentials;

use Symfony\Component\HttpFoundation\Request;

/**
 * Base class defining login credentials, which are loaded and set up by the child class implementing it.
 * An example of a child class is Post, whereby the username and password are retrieved from POST variables
 * @package BlueSky\Framework\Auth\Credentials
 * */
class Base
{

    /**
     * The login token
     * @var string $token
     * @access protected
     * */
    protected $token;

    /**
     * The login username
     * @var string $username
     * @access protected
     * */
    protected $username;

    /**
     * The login username
     * @var string $password
     * @access protected
     * */
    protected $password;

    /**
     * The IP Address
     * @var string $ip
     * @access protected
     * */
    protected $ip;

    /**
     * The IP source to use, if for example a cookie is being sent.
     * Cookie sources are only used when the request is being made locally
     * @var string $ipSource
     * @access protected
     * */
    protected $ipSource = '';

    /**
     * The value of the IP to use, which may be different to the request IP
     * @var string $ipValue
     * @access protected
     *  */
    protected $ipValue = '';

    /**
     * The users ID
     * @var integer $userId
     * @access protected
     * */
    protected $userId;

    /**
     * The session ID
     * @var integer $sessionId
     * @access protected
     * */
    protected $sessionId;

    /**
     * The request data
     * @var Request $request
     * @access protected
     * */
    protected $request;

    /**
     * The request data
     * @var int $masqueradeUserId
     * @access protected
     * */
    protected $masqueradeUserId;

    /**
     * Constructor for the Credentials Base objects, and any children
     * @param Request $request - the request data to be used to determine supplied credentials
     * @return Base
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
        if($request->hasSession()) {
            $this->sessionId = $request->getSession()->getId();
        }
        $this->setupIps();
        $this->loadCredentials();
    }

    /**
     * Setup the IP addresses
     * @access protected
     * @return void
     *  */
    protected function setupIps()
    {
        $this->ip = $this->request->getClientIp();
        if(isset($_COOKIE['ip']) &&
            (gethostbyname($_SERVER['SERVER_NAME'])==$_SERVER['REMOTE_ADDR'] ||
                gethostbyname($_SERVER['SERVER_NAME'])==$_SERVER['SERVER_ADDR'])) {
            $this->ipSource = 'cookie';
            $this->ipValue = $_COOKIE['ip'];
        }
    }

    /**
     * Get the session ID
     * @access public
     * @return string
     * */
    public function getSessionId()
    {
        return $this->sessionId;
    }

    /**
     * Get the username
     * @access public
     * @return string
     * */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Get the masquerade user id
     * @access public
     * @return int
     * */
    public function getMasqueradeUserId()
    {
        return $this->masqueradeUserId;
    }

    /**
     * Get the password
     * @access public
     * @return string
     * */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Get the users ID
     * @access public
     * @return string
     * */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set the users ID
     * @access public
     * @param int $userId
     * @return string
     * */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * Set the masquerade user ID
     * @access public
     * @param int $masqueradeUserId
     * @return string
     * */
    public function setMasqueradeUserId($masqueradeUserId)
    {
        $this->masqueradeUserId = $masqueradeUserId;
    }

    /**
     * Get the IP Address
     * @access public
     * @return string
     * */
    public function getIp()
    {
        $result = $this->ip;
        if($this->ipSource=='cookie') {
            $result = $this->ipValue;
        }
        return $result;
    }

    /**
     * Get the Authorization token
     * @access public
     * @return string
     * */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Load and determine the username and password based upon the credentials source, as determined by the
     * child class implementation
     * @throws \Exception
     * @return void
     */
    protected function loadCredentials()
    {
        //throw new \Exception('Child class implementation of '.__NAMESPACE__.'\\'.__CLASS__.'.::loadCredentials required');
    }

}
