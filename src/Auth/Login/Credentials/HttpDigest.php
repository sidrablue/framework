<?php
namespace BlueSky\Framework\Auth\Login\Credentials;

/**
 * HTTP Digest authentication credentials class
 * @package BlueSky\Framework\Auth\Credentials
 * */
class HttpDigest extends Base
{

    /**
     * Load and determine the username and password from HTTP Digest parameters
     * @throws \Exception
     * @return void
     */
    protected function loadCredentials()
    {
        parent::loadCredentials();//throws exception for now
    }

}
