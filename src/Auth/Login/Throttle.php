<?php
namespace BlueSky\Framework\Auth\Login;

use BlueSky\Framework\Object\Model\Base;

class Throttle extends Base
{

    protected $_attemptLimit = 5;
    protected $_suspensionTime = 30;


    public function check()
    {

    }

    public function isBanned()
    {

    }

    public function isSuspended()
    {

    }

    public function addFailedLogin()
    {

    }

    public function suspend()
    {

    }

    public function ban()
    {

    }

    public function removeBan()
    {

    }

    public function removeSuspension()
    {

    }

    public function resetThrottle()
    {

    }

    public function getSuspension()
    {

    }

    public function getSuspensionTime()
    {

    }

    public function getBan()
    {

    }

    public function getBanTime()
    {

    }


}