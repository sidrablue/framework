<?php
namespace BlueSky\Framework\Auth\Login\Method;

use BlueSky\Framework\Auth\Login\Credentials\Api;
use BlueSky\Framework\Auth\Login\Credentials\Session;
use BlueSky\Framework\Auth\Session as AuthSession;
use BlueSky\Framework\Entity\Auth\Token as AuthTokenEntity;
use BlueSky\Framework\Entity\User\ApiKey;
use BlueSky\Framework\Entity\User\User;
use BlueSky\Framework\Model\Auth\Session as AuthSessionModel;
use BlueSky\Framework\Model\Auth\Token as AuthTokenModel;
use BlueSky\Framework\Model\User\Password;
use BlueSky\Framework\Service\Jwt;
use BlueSky\Framework\Util\Time;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use M6Web\Component\Firewall\Firewall;
use Whitelist\Check as WhitelistCheck;

/**
 * Lote login method, via database validation of the users credentials
 * @package BlueSky\Framework\Auth\Login\Method
 * */
class Lote extends Base
{

    /**
     * Validate a login against the database for this user
     * @access protected
     * @return boolean
     */
    protected function validateLogin()
    {
        $result = false;
        $m = new User($this->state);
        if ($this->credentials->getToken()) {
            $result = $this->validateLoginByToken($m);
        } elseif ($this->credentials->getUsername() || $this->credentials->getUserId()) {
            if ($this->credentials->getUsername()) {
                $m->loadForLogin($this->credentials->getUsername());
            } elseif ($this->credentials->getUserId()) {
                $m->load($this->credentials->getUserId());
            }
            $result = $this->validateLoginByUserCredentials($m);
        }
        return $result;
    }

    /**
     * Validate login by token
     *
     * @param User $ue
     * @return boolean
     * */
    protected function validateLoginByToken(User $ue)
    {
        $result = false;
        $token = $this->credentials->getToken();
        if ($this->credentials instanceof Api) {
            $api = new ApiKey($this->state);
            if ($api->loadByXApiKey($token)) {
                if ($ue->load($api->user_id)) {
                    $this->state->getUser()->setData($ue->getData());
                    $result = true;
                }
            }
        } else {
            $jwtService = new Jwt();
            $tokenKey = $this->state->getSettings()->getSettingOrConfig("login.jwt.key", false);
            $isValid = $jwtService->validateTokenString($token, $this->state->getUrl()->getBaseUrl(), $tokenKey);
            $userId = $jwtService->getTokenUserId($token);

            if ($tokenId = $jwtService->getTokenId($token)) {
                $ate = new AuthTokenEntity($this->state);
                $ate->loadByField('public_key', $tokenId);

                $matchingTokenFound = $ate->id
                    && $ate->object_ref == User::TABLE_NAME
                    && $ate->object_id == $userId
                    && $ate->object_key == 'login'
                    && (!$ate->usage_limit || $ate->usage_count < $ate->usage_limit);

                if ($matchingTokenFound) {
                    $isValid = $jwtService->validateTokenString($token, $this->state->getUrl()->getBaseUrl(), $ate->private_key);

                    $signer = new Sha256();
                    $parsedToken = (new Parser())->parse((string) $token);
                    $isVerified = $parsedToken->verify($signer, $ate->private_key);

                    if (!$isValid && $isVerified) {
                        $now = new \DateTime();
                        $oneMonthExpiry = $ate->datetime_expires;
                        $oneMonthExpiry->add(new \DateInterval('P1M'));
                        $canRevalidate = $oneMonthExpiry > $now;

                        if ($canRevalidate) {
                            $hourInSeconds = 60 * 60;
                            $as = new AuthSession($this->state);
                            $token = $as->updateToken($ate, $ate->object_id, $hourInSeconds);
                            $isValid = true;
                        }
                    }
                    if ($isValid) {
                        $ate->usage_count++;
                        $ate->save();
                    }
                } else {
                    $isValid = false;
                }
            }

            if ($isValid && $ue->load($userId)) {
                $this->state->getUser()->setData($ue->getData());
                $this->state->getResponse()->headers->set("X-LOTE-TOKEN", $token);
                $result = true;
            }
        }
        return $result;
    }

    /**
     * Validate login by user credentials, as this user was loaded by a username and/or user ID
     * @param User $m
     * @return boolean
     * */
    protected function validateLoginByUserCredentials(User $m) {
        $result = false;
        if ($m->id) {
            if ($m->is_active) {
                $this->credentials->setUserId($m->id);
                $pm = new Password($this->state);
                if ($this->credentials instanceof Session) {
                    //if this is a session instance, then we need to verify the session hash
                    $s = new AuthSessionModel($this->state);
                    if($data = $s->getLogin("sb__user", $m->id, $this->credentials->getIp(), $this->credentials->getSessionId())) {
                        $loginHash = $this->getLoginHashKey($m, $data['token']);
                        if (password_verify($loginHash, $this->credentials->getPassword())) {
                            if($result = $this->saveLogin(false)) {
                                if(!$data['is_two_factor_verified'] && $this->requiresTwoFactor()) {
                                    $this->state->getView()->twoFactorRequired();
                                }
                                else {
                                    $this->state->getUser()->setData($m->getData());
                                }
                            }
                        } else {
                            $this->loginError = 2; //invalid session
                        }
                    }
                    else {
                        $this->loginError = 5; //login expired
                    }
                } elseif ($pm->isValidPassword($this->credentials->getPassword(), $m->id, $m->email)) {
                    if($result = $this->saveLogin()) {
                        $this->state->getUser()->setData($m->getData());
                        if($this->requiresTwoFactor()) {
                            $this->state->getView()->two_factor_required = true;
                        }
                    }
                } else {
                    $this->loginError = 1; //invalid credentials
                }
            } else {
                $this->loginError = 4; //inactive
            }
        }
        return $result;
    }

    /**
     * Check if two step verification is required
     */
    private function requiresTwoFactor()
    {
        $result = false;
        if ($this->twoFactorEnabled() && !$this->loginIpWhitelisted()) {
            $route = $this->state->getRoute();
            if (isset($route['is_two_factor']) && $route['is_two_factor']) {
                $result = false;
            } else {
                $result = true;
            }
        }
        return $result;
    }

    private function twoFactorEnabled()
    {
        return $this->state->getSettings()->getSettingOrConfig("login.two_factor.enabled", false);
    }

    private function loginIpWhitelisted()
    {
        $result = false;
        if ($whiteList = $this->state->getSettings()->getSettingOrConfig("login.two_factor.ip_whitelist", false)) {
            $firewall = new Firewall();
            if($firewall->setDefaultState(false)->addList(explode(",", $whiteList), 'local', true)->handle()) {
                $result = true;
            }
        }
        return $result;
    }

    /**
     * Save the current login
     * @param $updateHash - true if the session hash needs to be updated, generally meaning that this is a new login
     * @access protected
     * @return bool
     * */
    protected function saveLogin($updateHash = true)
    {
        $s = $this->state->getAuthSession();
        $m = new AuthSessionModel($this->state);
        if ($m->saveLogin($s, 'sb__user', $this->credentials->getUserId(), $this->credentials->getUsername(), $this->credentials->getIp(), $this->credentials->getSessionId())) {
            $session = $this->state->getRequest()->getSession();
            $this->state->getUser()->load($s->user_id);
            $this->state->getUser()->updateProperty('last_login', Time::getUtcNow());
            $session->set('ip', $this->credentials->getIp());
            $session->set('_lote_login_user_id', $this->credentials->getUserId());
            $session->set('username', $this->credentials->getUsername());
            if ($updateHash) {
                $hashKey = $this->getLoginHashKey($this->state->getUser(), $s->token);
                $session->set('hash', password_hash($hashKey, PASSWORD_DEFAULT));
            }
        }
        return $s->id;
    }

    /**
     * @return Boolean
     * */
    public function passwordResetRequired()
    {
        return false;
    }

    /**
     * Get the key that is used to generate a login hash from
     * @param User $u - the user entity
     * @param string $loginToken - the unique token assigned to this login
     * @access private
     * @return string
     * */
    private function getLoginHashKey($u, $loginToken)
    {
        return $u->password;
    }

    /**
     * Perform a system logout
     * @access public
     * @return void
     * */
    public function logout()
    {
        $sessionEntity = new \BlueSky\Framework\Entity\Auth\Session($this->state);
        $sessionEntity->loadByFields(['object_ref' => 'sb__user', 'session_id' => $this->credentials->getSessionId()]);
        if($sessionEntity->masquerade_user_id) {
            $this->state->getLogin()->autoLogin($sessionEntity->masquerade_user_id);
        } else {
            $token = $this->credentials->getToken();
            if( $token && !($this->credentials instanceof Api)) {
                $jwtService = new Jwt();
                if ($tokenId = $jwtService->getTokenId($token)) {
                    $ate = new AuthTokenEntity($this->state);
                    $ate->loadByField('public_key', $tokenId);

                    $matchingTokenFound = $ate->id
                        && $ate->object_ref == User::TABLE_NAME
                        && $ate->object_id == $jwtService->getTokenUserId($token)
                        && $ate->object_key == 'login';

                    if ($matchingTokenFound) {
                        $ate->delete();
                    }
                }
            }
            $this->state->getRequest()->getSession()->invalidate();
        }
    }

    /**
     * Check if a user is currently logged in
     * @access public
     * @return boolean
     * */
    public function isLoggedIn()
    {
        return $this->state->getAuthSession()->isValidLogin();
    }

}
