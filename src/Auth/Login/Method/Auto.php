<?php
namespace BlueSky\Framework\Auth\Login\Method;

use BlueSky\Framework\Model\Auth\Session as AuthSession;
use BlueSky\Framework\Entity\User\User;
use BlueSky\Framework\Util\Time;

/**
 * Lote login method, via database validation of the users credentials
 * @package BlueSky\Framework\Auth\Login\Method
 * */
class Auto extends Base
{

    /**
     * @var User
     * */
    private $user;

    /**
     * Validate that sufficient login parameters have been provided to attempt a login
     * @access protected
     * @return boolean
     */
    protected function validParams()
    {
        return $this->credentials->getUserId();
    }


    /**
     * Validate a login against the database for this user
     * @access protected
     * @return boolean
     */
    protected function validateLogin()
    {
        $result = false;
        $this->user = new User($this->state);
        $this->user->load($this->credentials->getUserId());
        if ($this->user->id) {
            if (true || $this->user->is_active) {
                $s = $this->state->getAuthSession();
                if($s->username !=$this->user->username) {
                    $m = new AuthSession($this->state);
                    $m->delete($s->id);
                }
                $result = $this->saveLogin();
            } else {
                $this->loginError = 4; //inactive
            }
        }
        return $result;
    }

    /**
     * Save the current login
     * @param $updateHash
     * @access protected
     * @return bool
     * */
    protected function saveLogin($updateHash = true)
    {
        $s = $this->state->getAuthSession();
        $m = new AuthSession($this->state);
        if ($loginData = $m->saveLogin($s, 'sb__user', $this->credentials->getUserId(), $this->credentials->getUsername(), $this->credentials->getIp(), $this->credentials->getSessionId(), 'local', $this->credentials->getMasqueradeUserId())) {
            $session = $this->state->getRequest()->getSession();
            $this->state->getUser()->setData($this->user->getData());
            $this->state->getUser()->updateProperty('last_login', Time::getUtcNow());
            $session->set('_lote_login_user_id', $this->user->id);
            $session->set('username', $this->user->username);
            if ($updateHash) {
                $hashKey = $this->getLoginHashKey($this->state->getUser());
                $session->set('hash', password_hash($hashKey, PASSWORD_DEFAULT));
            }
            $session->set('ip', $this->credentials->getIp());
        }
        return $s->id;
    }

    /**
     * @return Boolean
     * */
    public function passwordResetRequired()
    {
        return false;
    }

    /**
     * Get the key that is used to generate a login hash from
     * @param User $u - the user entity
     * @access private
     * @return string
     * */
    private function getLoginHashKey($u)
    {
        return $u->password;
    }

    /**
     * Perform a system logout
     * @access public
     * @return void
     * */
    public function logout()
    {
        $s = new AuthSession($this->state);
        $s->removeLogin($this->credentials->getSessionId(), $this->credentials->getUsername(), $this->siteId);
    }

    /**
     * Check if a user is currently logged in
     * @access public
     * @return boolean
     * */
    public function isLoggedIn()
    {
        return $this->user->isLoggedIn();
    }

}
