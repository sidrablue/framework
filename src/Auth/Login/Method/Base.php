<?php
namespace BlueSky\Framework\Auth\Login\Method;

use BlueSky\Framework\Auth\Login\Credentials\Factory as CredentialsFactory;
use BlueSky\Framework\Auth\Login\Credentials\Base as Credentials;
use BlueSky\Framework\State\Web as State; //@todo - implement for Base state instead of Web state

/**
 * Base class for all login methods, such as Ldap, Lote, and in the future OAuth, etc.
 * @package BlueSky\Framework\Auth\Login\Method
 * */
class Base
{

    /**
     * The request data
     * @var State $request
     * @access protected
     * */
    protected $state;

    /**
     * The login credentials
     * @var Credentials $_credentials
     * @access protected
     * */
    protected $credentials;

    /**
     * The error to indicate the credentials error
     * @var int $paramError
     * @access protected
     * */
    protected $paramError;

    /**
     * The error to indicate the login validation error
     * @var int $loginError
     * @access protected
     * */
    protected $loginError;

    /**
     * Default constructor, to which the login credentials must be specified
     * @param State $state - the request state
     * @param Credentials $credentials - the login credentials to use for validation
     */
    public function __construct(State $state, Credentials $credentials = null)
    {
        $this->state = $state;
        if($credentials==null) {
            $credentials = CredentialsFactory::getInstance($this->state->getRequest());
        }
        $this->credentials = $credentials;
        $this->onCreate();
    }

    /**
     * Placeholder function to extend in child if required
     * */
    protected function onCreate()
    {
        //implement in child
    }

    /**
     * Attempt a login using the current login method.
     * Define the method in the child
     * @return boolean
     */
    public function login()
    {
        $result = false;
        if ($this->validParams()) {
            if ($result = $this->validateLogin()) {
                $result = $this->saveLogin();
            } else {
                if($this->loginError==0) {
                    $this->loginError = 1;
                }
            }
        }
        return $result;
    }

    /**
     * Perform a login attempt via the current method, which should be implemented by a child class
     * @access protected
     * @return boolean
     */
    protected function validateLogin()
    {
        return false;
    }

    /**
     * Get the login error message
     * @access public
     * @return string
     * */
    public function getErrorMessage()
    {
        $errorMessage = 'Invalid login attempt'; //@translate
        if ($this->paramError == 1) {
            $errorMessage = 'No username or password specified'; //@translate
        } elseif ($this->paramError == 2) {
            $errorMessage = 'Please specify a username'; //@translate
        } elseif ($this->paramError == 3) {
            $errorMessage = 'Please specify a password'; //@translate
        } elseif ($this->loginError == 1) {
            $errorMessage = 'Invalid credentials specified'; //@translate
        } elseif ($this->loginError == 2) {
            $errorMessage = 'Invalid session detected'; //@translate
        } elseif ($this->loginError == 4) {
            $errorMessage = 'Account is inactive'; //@translate
        } elseif ($this->loginError == 5) {
            $errorMessage = 'Password reset required'; //@translate
        }
        return $errorMessage;
    }

    /**
     * Get the login error code
     * @access public
     * @return int
     */
    public function getErrorCode()
    {
        return $this->loginError;
    }

    /**
     * Get the username
     * @access public
     * @return string
     */
    public function getUsername()
    {
        $result = '';
        if(isset($this->credentials)) {
            return $this->credentials->getUsername();
        }
        return $result;
    }

    /**
     * Get the password
     * @access public
     * @return string
     */
    public function getPassword()
    {
        $result = '';
        if(isset($this->credentials)) {
            return $this->credentials->getPassword();
        }
        return $result;
    }

    /**
     * Validate that sufficient login parameters have been provided to attempt a login
     * @access protected
     * @return boolean
     */
    protected function validParams()
    {
        if(!$this->credentials->getUserId() && !$this->credentials->getToken()) {
            if ($this->credentials->getUsername() == '' && $this->credentials->getPassword() == '') {
                $this->paramError = 1;
            } elseif ($this->credentials->getUsername() == '') {
                $this->paramError = 2;
            } elseif ($this->credentials->getPassword() == '') {
                $this->paramError = 3;
            }
        }
        return $this->paramError == 0;
    }

    /**
     * Attempt a logout using the current login method.
     * Define the method in the child
     * @access public
     * @return boolean
     */
    public function logout()
    {
        return false;
    }

    /**
     * Check if the current user is logged in within the current login method
     * Define the method in the child
     * @access public
     * @return boolean
     */
    public function isLoggedIn()
    {
        return false;
    }


    /**
     * Save the login to the login session so that the user remains logged in
     * @access protected
     * @return boolean
     */
    protected function saveLogin()
    {
        //@todo
    }

    /**
     * @return Boolean
     * */
    public function passwordResetRequired()
    {
        return false;
    }

    public function updatePassword($username, $password, &$errorMessage) {
        return false;
    }

}
