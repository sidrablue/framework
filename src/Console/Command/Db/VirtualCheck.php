<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 */



namespace BlueSky\Framework\Console\Command\Db;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Base class for all entities
 */
class VirtualCheck extends Check
{

    protected function configure()
    {
        parent::configure();
        $this->setName('db:virtual:check')
            ->setDescription('Run a DB check on virtual tables only')
            ->setHelp('Run a DB check on virtual tables only');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->executeUpdate($input, $output, true, true);
    }

}
