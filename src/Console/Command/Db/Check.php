<?php

namespace BlueSky\Framework\Console\Command\Db;

use BlueSky\Framework\Console\Command as ConsoleCommand;
use BlueSky\Framework\Controller\Install\Update;
use BlueSky\Framework\State\Cli;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class Check extends ConsoleCommand
{
    use ConsoleCommand\AccountBasedTrait;

    protected function configure()
    {
        parent::configure();
        $this->setName('db:check')
            ->setDescription('Run a DB check')
            ->setHelp('Run a DB update');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->executeUpdate($input, $output, true, false);
    }

    protected function executeUpdate(InputInterface $input, OutputInterface $output, bool $checkOnly, bool $virtualOnly)
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Run a DB ' . ($checkOnly ? "Check" : "Update"));
        $lastAccountRef = "";

        try {
            $accountsToUpdate = $this->getAccountsToUpdate($input, $io, $checkOnly);
            foreach ($accountsToUpdate as $accountRef) {
                $cliState = new Cli($accountRef);
                $lastAccountRef = $accountRef;
                $u = new Update($cliState);
                if ($checkOnly) {
                    $u->check($io, $virtualOnly, $this->shouldUseDefault());
                } else {
                    $u->update($io, $virtualOnly, $this->shouldUseDefault());
                }
                $io->writeln("DB " . ($checkOnly ? "Check" : "Update") . " completed for <comment>{$accountRef}</comment>");
            }
        } catch (\Exception $e) {
            $io->error(["Unable to complete DB Update for account: " . $lastAccountRef, $e->getMessage()]);
            $io->text($e->getTraceAsString());
        }
    }
}
