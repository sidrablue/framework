<?php
namespace BlueSky\Framework\Console\Command\Db;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Base class for all entities
 */
class Update extends Check
{

    protected function configure()
    {
        parent::configure();
        $this->setName('db:update')
            ->setDescription('Run a DB update')
            ->setHelp('Run a DB update');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->executeUpdate($input, $output, false, false);
    }

}
