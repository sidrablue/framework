<?php
namespace BlueSky\Framework\Console\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

trait AccountBasedTrait
{
    private function getAccountsToUpdate(InputInterface $input, SymfonyStyle $io, $checkOnly = false): array
    {
        $result = [];
        if ($reference = $input->getOption("reference")) {
            $io->writeln("Running DB ".($checkOnly ? "Check" : "Update")." on2 <comment>{$reference}</comment>");
            $result[] = $reference;
        } elseif(defined("LOTE_ACCOUNT_REF")) {
            $io->writeln("Running DB ".($checkOnly ? "Check" : "Update")." on <comment>".LOTE_ACCOUNT_REF."</comment>");
            $result[] = LOTE_ACCOUNT_REF;
        } else {
            $accountNames = glob(LOTE_ASSET_PATH . 'conf/account/*.ini');
            foreach ($accountNames as $accountName) {
                $result[] = pathinfo(basename($accountName), PATHINFO_FILENAME);
            }
        }
        return $result;
    }
}
