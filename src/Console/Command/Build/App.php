<?php
namespace BlueSky\Framework\Console\Command\Build;

use BlueSky\Framework\Application\Build\Builder;
use BlueSky\Framework\Console\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Base class for all entities
 */
class App extends Command
{

    protected function configure()
    {
        parent::configure();
        $this->setName('build:app')
            ->setDescription('Build the apps')
            ->setHelp('Do a build of the apps')
            ->addOption("auto-db-update", null, InputOption::VALUE_OPTIONAL, 'Set to "1" to run the DB update at the end of the build');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Building Apps');
        try {
            $this->buildApps($io);
            $helper = $this->getHelper('question');
            $question = new Question("Would you like to do a DB update? [Y\\n]");
            $answer = $helper->ask($input, $output, $question);
            if($input->getOption('auto-db-update') == '1' || in_array(strtolower($answer), ['y','yes','ok'])){
                $this->runDbUpdate($io, $output);
            } else {
                $io->note("No db update executed");
            }
            $io->newLine();
            $io->success("Build completed");
        } catch (\Exception $e) {
            $io->error("Unable to finish build");
            $io->text($e->getTraceAsString());
        }
    }

    /**
     * Run an app update as part of this command
     * @access private
     * @param SymfonyStyle $io
     * @throws \Exception
     * @return void
     */
    protected function buildApps(SymfonyStyle $io)
    {
        $builder = new Builder($io);
        $builder->build();
        $io->newLine();
        $io->success("App update completed");
    }

    /**
     * Run a DB update as part of this command
     * @access private
     * @param SymfonyStyle $io
     * @param OutputInterface $output
     * @throws \Exception
     * @return void
     */
    protected function runDbUpdate(SymfonyStyle $io, OutputInterface $output)
    {
        $argumentInput = new ArrayInput([]);
        $command = $this->getApplication()->find("db:update");
        $command->run($argumentInput, $output);
    }

    /**
     * Run all update checks as required
     * @access private
     * @param SymfonyStyle $io
     * @param OutputInterface $output
     * @throws \Exception
     * @return void
     */
    protected function runChecks(SymfonyStyle $io, OutputInterface $output)
    {
        $argumentInput = new ArrayInput([]);
        $command = $this->getApplication()->find("build:setup");
        $command->run($argumentInput, $output);
    }

}
