<?php
namespace BlueSky\Framework\Console\Command\Build;

use BlueSky\Framework\Console\Command;
use BlueSky\Framework\Console\Command\AccountBasedTrait;
use BlueSky\Framework\State\Cli;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Base class for all entities
 */
class Setup extends Command
{

    use AccountBasedTrait;

    /**
     *
     */
    protected function configure()
    {
        parent::configure();
        $this->setName('build:setup')
            ->setDescription('Run the build setups');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Running setup checks');
        try {
            $this->runSetup($input, $io);
            $io->success("Setup completed");
        } catch (\Exception $e) {
            $io->error("Unable to finish setup");
            $io->text($e->getTraceAsString());
        }
    }

    /**
     * Run the app setup
     * @access private
     * @param InputInterface $input
     * @param SymfonyStyle $io
     * @return void
     */
    protected function runSetup(InputInterface $input, SymfonyStyle $io)
    {
        $lastAccountRef = "";
        try {
            $accountsToUpdate = $this->getAccountsToUpdate($input, $io);
            foreach ($accountsToUpdate as $accountRef) {
                $io->note("Setup check for {$accountRef}");
                $cliState = new Cli($accountRef);
                /**
                 * @var \BlueSky\Framework\Application\Config $v
                 * */
                foreach ($cliState->getApps()->getAll() as $v) {
                    $file = $v->getDirectory() . 'res/build/Setup.php';
                    if (file_exists($file)) {
                        require_once($file);
                        $namespace = $v->getNamespace() . "Build\\Setup";
                        if (class_exists($namespace)) {
                            $object = new $namespace($cliState);
                            $object->check();
                        }
                    }
                }
                $io->writeln("Setup completed for <comment>{$accountRef}</comment>");
            }
        } catch (\Exception $e) {
            $io->error("Unable to complete DB Update for account: " . $lastAccountRef);
            $io->text($e->getTraceAsString());
        }
    }

}
