<?php
namespace BlueSky\Framework\Console\Command\Build;

use BlueSky\Framework\Application\Build\Builder;
use BlueSky\Framework\Console\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Base class for all entities
 */
class Full extends App
{

    protected function configure()
    {
        parent::configure();
        $this->setName('build:full')
            ->setDescription('Build the full system')
            ->setHelp('Do a full build, including git checkout, composer update, code updates and db updates');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Doing a full build');
        try {
            $this->buildApps($io);
            $this->updateFramework($io);
            $this->runDbUpdate($io, $output);
            $this->runChecks($io, $output);

            $io->newLine();
            $io->success("Build completed");
        }
        catch (\Exception $e) {
            $io->error("Unable to finish build");
            $io->text($e->getTraceAsString());
        }
    }

    /**
     * Update hte framework, including a git pull and a composer update
     * @access private
     * @param SymfonyStyle $io
     * @throws \Exception
     * @return void
     * @todo - make this a composer install eventually, and then a repository based update
     */
    private function updateFramework(SymfonyStyle $io)
    {
        $io->note("Doing a full build, running git and composer");
        chdir(LOTE_BASE_PATH);
        shell_exec("git pull");
        $io->success("Git pull completed successfully");
        shell_exec("composer update --no-dev -o");
        $io->newLine();
        $io->success("Composer completed successfully");
    }

}
