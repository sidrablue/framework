<?php

namespace BlueSky\Framework\Console\Command\Build;

use BlueSky\Framework\Application\Build\Builder;
use BlueSky\Framework\Console\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Base class for all entities
 */
class Symlinks extends Command
{

    protected function configure()
    {
        parent::configure();
        $this->setName('build:symlinks')
            ->setDescription('Build the symlinks')
            ->setHelp('Do a build of the symlinks');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Building Symlinks');
        try {
            $builder = new Builder($io);
            $builder->addWebSymlinkIfMissing();
            $io->success("Symlink creation completed");
        } catch (\Exception $e) {
            $io->error("Unable to build symlinks");
            $io->text($e->getTraceAsString());
        }
    }

    /**
     * Run an app update as part of this command
     * @access private
     * @param SymfonyStyle $io
     * @return void
     * @throws \Exception
     */
    protected function buildApps(SymfonyStyle $io)
    {
        $builder = new Builder($io);
        $builder->build();
        $io->newLine();
        $io->success("App update completed");
    }

    /**
     * Run a DB update as part of this command
     * @access private
     * @param SymfonyStyle $io
     * @param OutputInterface $output
     * @return void
     * @throws \Exception
     */
    protected function runDbUpdate(SymfonyStyle $io, OutputInterface $output)
    {
        $argumentInput = new ArrayInput([]);
        $command = $this->getApplication()->find("db:update");
        $command->run($argumentInput, $output);
    }

    /**
     * Run all update checks as required
     * @access private
     * @param SymfonyStyle $io
     * @param OutputInterface $output
     * @return void
     * @throws \Exception
     */
    protected function runChecks(SymfonyStyle $io, OutputInterface $output)
    {
        $argumentInput = new ArrayInput([]);
        $command = $this->getApplication()->find("build:setup");
        $command->run($argumentInput, $output);
    }

}
