<?php
declare(strict_types=1);

namespace BlueSky\Framework\Console\Command\Build;

use Cz\Git\GitException;
use BlueSky\Framework\Console\Command;
use BlueSky\Framework\Service\GitStatus;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Base class for all entities
 */
class Status extends Command
{

    protected function configure()
    {
        parent::configure();
        $this->setName('build:status')
            ->setDescription('Status of the build')
            ->setHelp('Show the current build status');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Checking Status');
        try {
            $projectStatus = $this->checkProject($io);
            $appStatuses = $this->checkApps($io);
            $frameworkStatus = $this->checkFramework($io);
            $this->printStatus($io, array_merge([$projectStatus], $appStatuses, [$frameworkStatus]));
        } catch (\Exception $e) {
            $io->error("Unable to determine status");
            $io->text($e->getTraceAsString());
        }
    }

    private function printStatus(SymfonyStyle $io, array $gitStatuses): void
    {
        $statuses = [];
        foreach ($gitStatuses as $v) {
            /** @var GitStatus $v */
            $asArray = $v->asArray();
            foreach ($asArray as $x => $y) {
                $asArray[$x] = ($v->hasChanges()?"<fg=red>{$y}</>":"<fg=yellow>{$y}</>");
            }
            $statuses[] = $asArray;
        }
        $io->table(['Name', 'Branch', 'Status'], $statuses);
    }

    private function checkProject(SymfonyStyle $io): GitStatus
    {
        $gitStatus = new GitStatus(LOTE_BASE_PATH, 'PROJECT');
        if ($gitStatus->isValid() && $gitStatus->hasChanges()) {
            $io->warning("PROJECT: commit required: " . LOTE_BASE_PATH);
        }
        return $gitStatus;
    }

    private function checkApps(SymfonyStyle $io): array
    {
        $result = [];
        $appDirectories = glob(LOTE_LIB_APP_PATH . '*', GLOB_ONLYDIR);
        foreach ($appDirectories as $appDir) {
            $gitStatus = new GitStatus($appDir, 'APP');
            if ($gitStatus->isValid() && $gitStatus->hasChanges()) {
                $io->warning("APP: Changes exist in {$gitStatus->getName()}: {$gitStatus->getPath()}");
            }
            $result[] = $gitStatus;
        }
        return $result;
    }

    private function checkFramework(SymfonyStyle $io): GitStatus
    {
        $gitStatus = new GitStatus(LOTE_FRAMEWORK_PATH, 'FRAMEWORK');
        if ($gitStatus->isValid() && $gitStatus->hasChanges()) {
            $io->warning("FRAMEWORK: commit required: " . LOTE_FRAMEWORK_PATH);
        }
        return $gitStatus;
    }

}
