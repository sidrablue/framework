<?php
namespace BlueSky\Framework\Console\Command\Queue;

use BlueSky\Framework\Console\Command;
use BlueSky\Framework\Console\Command\Queue\Traits\AliasTrait;
use BlueSky\Framework\Console\Command\Queue\Traits\QueuesRegisteredTrait;
use BlueSky\Framework\Util\Arrays;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class Names extends Command
{
    use AliasTrait, QueuesRegisteredTrait;

    protected function configure()
    {
        parent::configure();
        $this->setName('queue:names')
            ->setDescription('Show all of the registered queues')
            ->setHelp('This command shows the currently registered queues that can be run.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        if($registeredQueues = $this->getRegisteredQueues()) {
            $formattedQueues = [];
            foreach($registeredQueues as $k=>$v) {
                $tempQueue = [];
                $tempQueue['name'] = $v;
                $tempQueue['file'] = $k;
                $formattedQueues[] = $tempQueue;
            }
            $io->title(count($registeredQueues)." Named Queue(s)");
            Arrays::order2dArray($formattedQueues, 'name');
            $io->table(['Name', 'Location'], $formattedQueues);
        }
        else {
            $io->comment("No queues registered");
        }
    }
}
