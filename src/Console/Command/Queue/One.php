<?php

namespace BlueSky\Framework\Console\Command\Queue;

use BlueSky\Framework\Console\Command;
use BlueSky\Framework\Console\Command\Queue\Traits\AliasTrait;
use BlueSky\Framework\Util\Dir;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class One extends Command
{
    use AliasTrait;

    protected function configure()
    {
        parent::configure();
        $this->setName('queue:one')
            ->setDescription('Run a single queue')
            ->addArgument('queue', InputArgument::REQUIRED, 'Specify the name of the queue to run')
            ->setHelp('This command runs a single queue processor...');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $queueName = $input->getArgument("queue");
        $io->success("Running queue: {$queueName}");

        $outputDir = LOTE_ASSET_PATH . 'log/_queue';
        chdir(LOTE_LIB_PATH . '/vendor/bluesky/framework/bin/');
        if (isWindows()) {
            Dir::make($outputDir);
            $command = "php queue.php {$queueName} " . $this->getAliasPrefix() . $queueName;
        } else {
            shell_exec("su -s /bin/bash www-data -c 'mkdir -p $outputDir'");
            $command = "php queue.php {$queueName} " . $this->getAliasPrefix() . "_{$queueName}";
        }

        while (@ ob_end_flush()) ; // end all output buffers if any
        $proc = popen($command, 'r');
        while (!feof($proc)) {
            $io->write(fread($proc, 4096));
            @ flush();
        }
    }
}
