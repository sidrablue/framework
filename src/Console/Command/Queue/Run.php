<?php

namespace BlueSky\Framework\Console\Command\Queue;

use BlueSky\Framework\Console\Command;
use BlueSky\Framework\Console\Command\Queue\Traits\AliasTrait;
use BlueSky\Framework\Console\Command\Queue\Traits\QueuesRegisteredTrait;
use BlueSky\Framework\Util\Dir;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class Run extends Command
{
    use AliasTrait, QueuesRegisteredTrait;

    protected function configure()
    {
        parent::configure();
        $this->setName('queue:run')
            ->setDescription('Run the queues')
            ->setHelp('This command runs all of the redis processing queues...');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->title("Starting Queues");
        foreach ($this->getRegisteredQueues() as $queueFile => $name) {
            if (file_exists($queueFile)) {
                $data = require $queueFile;
                $count = $data['count'] ?? 1;
                $io->writeln("Starting $count instance(s) of {$name}");
                for ($i = 0; $i < $count; $i++) {
                    $this->runQueue($name, $queueFile);
                }
            }
            $io->writeln('');
        }
    }

    private function runQueue($name, $queueFile): void
    {
        $outputDir = LOTE_ASSET_PATH . 'log/_queue';
        chdir(LOTE_LIB_PATH . '/vendor/bluesky/framework/bin/');
        if (isWindows()) {
            Dir::make($outputDir);
            pclose(popen("start /B php queue.php {$name} " . $this->getAliasPrefix() . $name . " {$queueFile} > nul 2>&1", "r"));
        } else {
            Dir::make($outputDir);
            shell_exec("su -s /bin/bash www-data -c 'mkdir -p $outputDir'");
            shell_exec("su -s /bin/bash www-data -c 'nohup php queue.php {$name} " . $this->getAliasPrefix() . "_{$name} > {$outputDir}/$name.out &'");
        }
    }
}