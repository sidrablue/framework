<?php
namespace BlueSky\Framework\Console\Command\Queue;

use BlueSky\Framework\Console\Command;
use BlueSky\Framework\Console\Command\Queue\Traits\AliasTrait;
use BlueSky\Framework\Console\Command\Queue\Traits\ProcessesTrait;
use BlueSky\Framework\Util\Arrays;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class Status extends Command
{
    use AliasTrait, ProcessesTrait;

    protected function configure()
    {
        parent::configure();
        $this->setName('queue:status')
            ->setDescription('Show all of the running queues')
            ->setHelp('This command shows the current running queues.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        if($runningQueues = $this->getRunningQueues()) {
            Arrays::order2dArray($runningQueues, 'queue');
            $io->title(count($runningQueues)." Running Queue(s)");
            $io->table(['ID', 'Queue'], $runningQueues);
        }
        else {
            $io->comment("No queues running");
        }
    }
}
