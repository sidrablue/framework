<?php
namespace BlueSky\Framework\Console\Command\Queue;

use BlueSky\Framework\Console\Command;
use BlueSky\Framework\Console\Command\Queue\Traits\AliasTrait;
use BlueSky\Framework\Console\Command\Queue\Traits\ProcessesTrait;
use BlueSky\Framework\Util\Dir;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;


/**
 * Stop all running queues
 */
class Kill extends Command
{

    use AliasTrait, ProcessesTrait;

    /**
     * @inheritdoc
     * */
    protected function configure()
    {
        parent::configure();
        $this->setName('queue:kill')
            ->setDescription('Kill all of the queues')
            ->setHelp('This command runs all of the redis processing queues...');
    }

    /**
     * @inheritdoc
     * */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        if ($runningQueues = $this->getRunningQueues()) {
            $total = count($runningQueues);
            $io->title("Stopping the following queues");
            $io->table(['ID', 'Queue'], $runningQueues);
            $io->progressStart($total);
            foreach ($runningQueues as $q) {
                $this->killQueueById($q['pid']);
            }
            while($runningQueues = $this->getRunningQueues()) {
                if($total > count($runningQueues)) {
                    $io->progressAdvance($total - count($runningQueues));
                    $total = $runningQueues;
                }
                sleep(1);
            }
            $io->progressFinish();
            $io->success("All queues stopped");
        } else {
            $io->comment("No queues running");
        }
    }

    /**
     * Kill a queue by its process ID.
     *
     * @access private
     * @param int $id
     * @return void
     * */
    private function killQueueById($id)
    {
        if (isWindows()) {
            shell_exec("taskkill /PID {$id} /F");
        } elseif (isUnix()) {
            shell_exec("kill -9 {$id}");
        }
    }

}
