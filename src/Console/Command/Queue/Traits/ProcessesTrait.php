<?php
declare(strict_types=1);

namespace BlueSky\Framework\Console\Command\Queue\Traits;

trait ProcessesTrait
{
    protected function getRunningQueues(): array
    {
        if (isWindows()) {
            $runningQueues = $this->getRunningQueuesInWindows();
        } else {
            $runningQueues = $this->getRunningQueuesInLinux();
        }
        arsort($runningQueues);
        return $runningQueues;
    }

    private function getRunningQueuesInWindows(): array
    {
        $result = [];
        if ($queues = shell_exec("wmic process where caption=\"php.exe\" get commandline,processid /format:csv")) {
            $queues = explode("\r\n", trim($queues));
            foreach ($queues as $q) {
                if (strpos($q, $this->getAliasPrefix()) > 0) {
                    $data = str_getcsv($q);
                    $result[] = ['pid' => $data[2], 'queue' => basename($data[1], '.php')];
                }
            }
        }
        return $result;
    }

    private function getRunningQueuesInLinux(): array
    {
        $result = [];
        if ($queues = shell_exec('ps aux | grep "' . $this->getaliasPrefix() . '" | grep "queue.php"')) {
            $queues = explode("\n", trim($queues));
            foreach ($queues as $q) {
                $q = explode(" ", preg_replace('!\s+!', ' ', $q));
                if ($q[10] == 'php') {
                    $result[] = ['pid' => $q[1], 'queue' => $q[12]];
                }
            }
        }
        return $result;
    }
}
