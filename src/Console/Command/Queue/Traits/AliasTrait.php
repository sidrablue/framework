<?php
declare(strict_types=1);

namespace BlueSky\Framework\Console\Command\Queue\Traits;

trait AliasTrait
{
    protected function getAliasPrefix(): string
    {
        if (!defined('QUEUE_ALIAS_PREFIX')) {
            define('QUEUE_ALIAS_PREFIX', 'LOTE_QUEUE_');
        }
        return (string)QUEUE_ALIAS_PREFIX;
    }
}
