<?php
namespace BlueSky\Framework\Console\Command\Queue\Traits;

/**
 * Base class for all entities
 */
trait QueuesRegisteredTrait
{

    /**
     * Get the queues that are registered within the system
     * */
    protected function getRegisteredQueues(): array
    {
        $appQueues = glob(LOTE_APP_PATH . "queue/worker/*.php");
        $libQueues = glob(LOTE_LIB_APP_PATH . "*/queue/*.php");
        $queueNames = [];
        foreach (array_merge($appQueues, $libQueues) as $queueFile) {
            $queueNames[$queueFile] = basename($queueFile, ".php");
        }
        return $queueNames;
    }

}
