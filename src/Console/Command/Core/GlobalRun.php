<?php
namespace BlueSky\Framework\Console\Command\Core;

use Pool;
use BlueSky\Framework\Console\Command as ConsoleCommand;
use BlueSky\Framework\Console\GlobalRunThread;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class GlobalRun extends ConsoleCommand
{

    protected function configure()
    {
        parent::configure();
        $this->setName('core:global:run')
            ->setDescription('Run a command on all accounts')
            ->setHelp('This command will run through every account and execute the same console command on it')
            ->addOption('instances', null, InputOption::VALUE_OPTIONAL, 'The number of instances to run in parallel', 1)
            ->addArgument('command_name', InputArgument::REQUIRED, 'The command to be used');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $baseDir = LOTE_APP_PATH . "/conf/account/";
        $accountNames = glob($baseDir . '*.ini');
        chdir(LOTE_BASE_PATH);
        $defaultMode = $this->shouldUseDefault() ? ' --default' : '';
        $command = $input->getArgument('command_name') . $defaultMode;
        $instances = $input->getOption('instances') ?? 1;
        if (!is_numeric($instances) || $instances < 1) {
            $instances = 1;
        }
        $instances = (int) $instances; // make sure the instance size is an integer. Float values will not go through
        if (PHP_ZTS && extension_loaded('pthreads') && $instances > 1) {
            $pool = new Pool($instances);
            $tasks = [];
            foreach ($accountNames as $accountName) {
                $tasks[] = new GlobalRunThread($accountName, $command);
            }
            foreach ($tasks as $task) {
                $pool->submit($task);
            }
            while ($pool->collect()) { continue; }

            $pool->shutdown();
        } else {
            foreach ($accountNames as $n) {
                if (is_file($n)) {
                    $name = pathinfo($n, PATHINFO_FILENAME);
                    $cmd = "php console.php --reference=$name $command";
                    $output->write(shell_exec($cmd));
                    echo "Command completed for $name\n";
                }
            }
        }
    }


}
