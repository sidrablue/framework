<?php
namespace BlueSky\Framework\Console\Command;

use Symfony\Component\Console\Command\LockableTrait as SymfonyLockableTrait;

trait LockTrait
{

    use SymfonyLockableTrait;

}
