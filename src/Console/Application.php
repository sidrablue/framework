<?php
namespace BlueSky\Framework\Console;


use BlueSky\Framework\Interfaces\StateInterface;
use BlueSky\Framework\State\Base as BaseState;
use Symfony\Component\Console\Application as SymfonyApplication;

class Application extends SymfonyApplication implements StateInterface
{

    /** @var BaseState $state */
    private $state;

    /**
     * Set the state object
     * @access public
     * @param BaseState $state
     * @return void
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * Get the state object
     * @access public
     * @return BaseState
     */
    public function getState()
    {
        return $this->state;
    }

    public function addCommandsFromPath($path, $namespace)
    {
        $files = [];
        $path = str_replace("\\", "/", $path);
        if (is_dir($path)) {
            $directory = new \RecursiveDirectoryIterator($path);
            $iterator = new \RecursiveIteratorIterator($directory);
            $regexIterator = new \RegexIterator($iterator, '/^((?!Command\/Old).)*\.php$/i', \RecursiveRegexIterator::GET_MATCH);
            foreach ($regexIterator as $info) {
                $info = str_replace("\\", "/", $info[0]);
                $files[$info] = pathinfo($info, PATHINFO_FILENAME);
            }
        }
        foreach ($files as $k => $v) {
            $commandClassName = $namespace;
            if (dirname($k) != $path) {
                $subPath = dirname(str_replace($path, "", $k));
                $subNameSpace = str_replace("/", "\\", $subPath);
                $commandClassName .= $subNameSpace;
            }
            $commandClassName .= '\\' . $v;
            if (class_exists($commandClassName)) {
                /** @var $c Command */
                $c = new $commandClassName();
                $c->setState($this->getState());
                $this->add($c);
            }
        }
    }

}
