<?php
namespace BlueSky\Framework\Console;

use BlueSky\Framework\Interfaces\StateInterface;
use BlueSky\Framework\State\Base as BaseState;
use Symfony\Component\Console\Command\Command as SymfonyCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Command extends SymfonyCommand implements StateInterface
{

    /** @var BaseState $state */
    private $state;

    /** @var bool $use_default */
    private $use_default = false;

    /**
     * Set the state object
     * @access public
     * @param BaseState $state
     * @return void
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * Get the state object
     * @access public
     * @return BaseState
     */
    public function getState()
    {
        return $this->state;
    }

    protected function configure()
    {
        $this->addOption('default', null, null, 'Run this command in default mode (runs with all prompts/questions using default values)');
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        if($input->hasOption('default')) {
            $this->use_default = filter_var($input->getOption('default'), FILTER_VALIDATE_BOOLEAN);
        }
    }

    /**
     * Depending on if the script should be run in cli mode
     * This means that all prompts/confirms/questions will be accepted with default options
     * @return bool
     */
    protected function shouldUseDefault(): bool
    {
        return $this->use_default;
    }

}
