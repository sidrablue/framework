<?php
/**
 * Copyright (c) BlueSky Digital Labs Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Console;

use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DriverManager;
use Exception;
use BlueSky\Framework\Console\Command as ConsoleCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

abstract class CommandRunAllDbs extends ConsoleCommand
{

    /** @var InputInterface $currentInput */
    private $currentInput;

    /** @var OutputInterface $currentOutput */
    private $currentOutput;

    /** @var SymfonyStyle $currentIO */
    private $currentIO;

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->currentInput = $input;
        $this->currentOutput = $output;
        $this->currentIO = new SymfonyStyle($input, $output);

        $masterHost = $this->getState()->getConfig()->get('database.master.host');
        $masterUser = $this->getState()->getConfig()->get('database.master.user');
        $masterPassword = $this->getState()->getConfig()->get('database.master.pass');

        if (!empty($masterHost) && !empty($masterUser)) {
            $conn = DriverManager::getConnection([
                'user' => $masterUser,
                'password' => $masterPassword,
                'host' => $masterHost,
                'driver' => 'pdo_mysql',
                'server_version' => 'mariadb-10.3.13',
            ], new Configuration());

            $configDir = realpath(LOTE_CONF_PATH . 'account/') . '/';

            $this->beforeExecuteForConnection();

            foreach (glob($configDir . "*.ini") as $filename) {
                $filename = basename($filename);
                if ($filename) {
                    try {
                        $configFilePath = $configDir . $filename;
                        $config = parse_ini_file($configFilePath);
                        $accountReference = str_replace('.ini', '', $filename);
                        $this->getIO()->comment("Running for account: {$accountReference}");
                        $conn->exec("use {$config['name']}");
                        $this->executeForConnection($conn, $accountReference);
                        $this->getIO()->success("Finished running for account: {$accountReference}");
                    } catch (Exception $e) {
                        $this->getIO()->error($e->getMessage());
                    }
                }
            }

            $this->afterExecuteForConnection();
        }
    }

    protected function getIO(): SymfonyStyle { return $this->currentIO; }

    protected function getInput(): InputInterface { return $this->currentInput; }

    protected function getOutput(): OutputInterface { return $this->currentOutput; }

    protected function beforeExecuteForConnection(): void { }

    protected function afterExecuteForConnection(): void { }

    /**
     * @param Connection $connection
     * @param string $accountReference
     * @throws Exception
     */
    abstract protected function executeForConnection(Connection $connection, string $accountReference): void;


}
