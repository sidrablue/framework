<?php
/**
 * Copyright (c) BlueSky Digital Labs Pty Ltd
 */

declare(strict_types=1);
namespace BlueSky\Framework\Console;

use Symfony\Component\Console\Output\OutputInterface;
use Threaded;

class GlobalRunThread extends Threaded
{
    /** @var string $accountName */
    private $accountName;

    /** @var string $command */
    private $command;

    public function __construct(string $accountName, string $command)
    {
        $this->accountName = $accountName;
        $this->command = $command;
    }

    public function run()
    {
        if (is_file($this->accountName)) {
            $name = pathinfo($this->accountName, PATHINFO_FILENAME);
            $cmd = "php console.php --reference=$name {$this->command}";
            echo shell_exec($cmd);
            echo "Command completed for $name\n";
        }
    }
}