<?php
namespace BlueSky\Framework\Util;

/**
 * IP manipulation utilities.
 *  */
class IpAddress
{

    /**
     * Scan a directory for its contents recursively
     *
     * @param $ipAddress
     * @param $ipAddressAndRange - a range in the format IP/Subnet, e.g. of 11.22.33.44/20
     * @return boolean
     */
    public static function isInRange($ipAddress, $ipAddressAndRange)
    {
        list ($subnet, $bits) = explode('/', $ipAddressAndRange);
        $ip = ip2long($ipAddress);
        $subnet = ip2long($subnet);
        $mask = -1 << (32 - $bits);
        $subnet &= $mask;
        return ($ip & $mask) == $subnet;
    }

}
