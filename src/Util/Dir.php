<?php
namespace BlueSky\Framework\Util;

/**
 * Directory manipulation utilities.
 *  */
class Dir
{

    /**
     * Delete a directory and all of its contents
     * @param string $dir - the path of the directory
     * @access public
     * @return string
     * */
    public static function delete($dir)
    {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (filetype($dir . "/" . $object) == "dir") {
                        self::delete($dir . "/" . $object);
                    } else {
                        unlink($dir . "/" . $object);
                    }
                }
            }
            reset($objects);
            rmdir($dir);
        }
    }

    /**
     * Make a directory and all of its parents
     * @param string $dir - the path of the directory
     * @access public
     * @return string
     * */
    public static function make($dir)
    {
        if (!is_dir($dir)) {
            mkdir($dir, 0755, true);
        }
    }

    /**
     * Scan a directory for its contents recursively
     *
     * @param $path
     * @return array - File and Directory contents of given path
     */
    public static function scan($basePath, $returnRelativePath = true)
    {
        $result = ['files' => [], 'directories' => []];

        $directoryIterator = new \RecursiveDirectoryIterator($basePath);
        $iteratorIterator = new \RecursiveIteratorIterator($directoryIterator, \RecursiveIteratorIterator::SELF_FIRST);

        $basePath = rtrim($basePath, "/");

        foreach ($iteratorIterator as $file) {
            $path = $file->getRealPath();
            if ($returnRelativePath) {
                $path = str_replace("\\", "/", $path);
                $path = preg_replace("/^" . preg_quote($basePath, "/") . "/", "", $path);
                $path = ltrim($path, "/");
            }
            if ($file->isDir()) {
                $result['directories'][$path] = true; // prevent duplicate folders
            } elseif ($file->isFile()) {
                $result['files'][] = $path;
            }
        }
        $result['directories'] = array_keys($result['directories']);

        return $result;
    }

}
