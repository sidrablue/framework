<?php
namespace BlueSky\Framework\Util\Timezone;

/**
 * Class for Array manipulation utilities.
 */
class Ms
{
    private static $timezones =
        [
            "Samoa Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Pacific/Apia",
                            "offset" => "-11",
                            "ms" => "Samoa Standard Time",
                            "name" => "[UTC-11:00] Samoa",
                            "utc_offset" => "-11:00"
                        ],

                    "1" =>
                        [
                            "olson" => "Pacific/Niue",
                            "offset" => "-11",
                            "ms" => "Samoa Standard Time",
                            "name" => "[UTC-11:00] Samoa",
                            "utc_offset" => "-11:00",
                        ],

                    "2" =>
                        [
                            "olson" => "Pacific/Pago_Pago",
                            "offset" => "-11",
                            "ms" => "Samoa Standard Time",
                            "name" => "[UTC-11:00] Samoa",
                            "utc_offset" => "-11:00",
                        ],

                ],

            "Hawaiian Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Pacific/Midway",
                            "offset" => "-11",
                            "ms" => "Hawaiian Standard Time",
                            "name" => "[UTC-10:00] Hawaii",
                            "utc_offset" => "-10:00",
                        ],

                    "1" =>
                        [
                            "olson" => "America/Adak",
                            "offset" => "-10",
                            "ms" => "Hawaiian Standard Time",
                            "name" => "[UTC-10:00] Hawaii",
                            "utc_offset" => "-10:00",
                        ],

                    "2" =>
                        [
                            "olson" => "Pacific/Fakaofo",
                            "offset" => "-10",
                            "ms" => "Hawaiian Standard Time",
                            "name" => "[UTC-10:00] Hawaii",
                            "utc_offset" => "-10:00",
                        ],

                    "3" =>
                        [
                            "olson" => "Pacific/Honolulu",
                            "offset" => "-10",
                            "ms" => "Hawaiian Standard Time",
                            "name" => "[UTC-10:00] Hawaii",
                            "utc_offset" => "-10:00",
                        ],

                    "4" =>
                        [
                            "olson" => "Pacific/Johnston",
                            "offset" => "-10",
                            "ms" => "Hawaiian Standard Time",
                            "name" => "[UTC-10:00] Hawaii",
                            "utc_offset" => "-10:00",
                        ],

                    "5" =>
                        [
                            "olson" => "Pacific/Rarotonga",
                            "offset" => "-10",
                            "ms" => "Hawaiian Standard Time",
                            "name" => "[UTC-10:00] Hawaii",
                            "utc_offset" => "-10:00",
                        ],

                    "6" =>
                        [
                            "olson" => "Pacific/Tahiti",
                            "offset" => "-10",
                            "ms" => "Hawaiian Standard Time",
                            "name" => "[UTC-10:00] Hawaii",
                            "utc_offset" => "-10:00",
                        ],

                    "7" =>
                        [
                            "olson" => "Pacific/Marquesas",
                            "offset" => "-9.5",
                            "ms" => "Hawaiian Standard Time",
                            "name" => "[UTC-10:00] Hawaii",
                            "utc_offset" => "-10:00",
                        ],

                ],

            "Alaskan Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "America/Anchorage",
                            "offset" => "-9",
                            "ms" => "Alaskan Standard Time",
                            "name" => "[UTC-09:00] Alaska",
                            "utc_offset" => "-09:00",
                        ],

                    "1" =>
                        [
                            "olson" => "America/Juneau",
                            "offset" => "-9",
                            "ms" => "Alaskan Standard Time",
                            "name" => "[UTC-09:00] Alaska",
                            "utc_offset" => "-09:00",
                        ],

                    "2" =>
                        [
                            "olson" => "America/Nome",
                            "offset" => "-9",
                            "ms" => "Alaskan Standard Time",
                            "name" => "[UTC-09:00] Alaska",
                            "utc_offset" => "-09:00",
                        ],

                    "3" =>
                        [
                            "olson" => "America/Yakutat",
                            "offset" => "-9",
                            "ms" => "Alaskan Standard Time",
                            "name" => "[UTC-09:00] Alaska",
                            "utc_offset" => "-09:00",
                        ],

                    "4" =>
                        [
                            "olson" => "Pacific/Gambier",
                            "offset" => "-9",
                            "ms" => "Alaskan Standard Time",
                            "name" => "[UTC-09:00] Alaska",
                            "utc_offset" => "-09:00",
                        ],

                ],

            "Pacific Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "America/Dawson",
                            "offset" => "-8",
                            "ms" => "Pacific Standard Time",
                            "name" => "[UTC-08:00] Pacific Time [US & Canada]",
                            "utc_offset" => "-08:00",
                        ],

                    "1" =>
                        [
                            "olson" => "America/Los_Angeles",
                            "offset" => "-8",
                            "ms" => "Pacific Standard Time",
                            "name" => "[UTC-08:00] Pacific Time [US & Canada]",
                            "utc_offset" => "-08:00",
                        ],

                    "2" =>
                        [
                            "olson" => "America/Vancouver",
                            "offset" => "-8",
                            "ms" => "Pacific Standard Time",
                            "name" => "[UTC-08:00] Pacific Time [US & Canada]",
                            "utc_offset" => "-08:00",
                        ],

                    "3" =>
                        [
                            "olson" => "America/Whitehorse",
                            "offset" => "-8",
                            "ms" => "Pacific Standard Time",
                            "name" => "[UTC-08:00] Pacific Time [US & Canada]",
                            "utc_offset" => "-08:00",
                        ],

                    "4" =>
                        [
                            "olson" => "Pacific/Pitcairn",
                            "offset" => "-8",
                            "ms" => "Pacific Standard Time",
                            "name" => "[UTC-08:00] Pacific Time [US & Canada]",
                            "utc_offset" => "-08:00",
                        ],

                ],

            "Pacific Standard Time [Mexico]" =>
                [
                    "0" =>
                        [
                            "olson" => "America/Santa_Isabel",
                            "offset" => "-8",
                            "ms" => "Pacific Standard Time [Mexico]",
                            "name" => "[UTC-08:00] Baja California",
                            "utc_offset" => "-08:00",
                        ],

                    "1" =>
                        [
                            "olson" => "America/Tijuana",
                            "offset" => "-8",
                            "ms" => "Pacific Standard Time [Mexico]",
                            "name" => "[UTC-08:00] Baja California",
                            "utc_offset" => "-08:00",
                        ],

                ],

            "Mountain Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "America/Boise",
                            "offset" => "-7",
                            "ms" => "Mountain Standard Time",
                            "name" => "[UTC-07:00] Mountain Time [US & Canada]",
                            "utc_offset" => "-07:00",
                        ],

                    "1" =>
                        [
                            "olson" => "America/Cambridge_Bay",
                            "offset" => "-7",
                            "ms" => "Mountain Standard Time",
                            "name" => "[UTC-07:00] Mountain Time [US & Canada]",
                            "utc_offset" => "-07:00",
                        ],

                    "2" =>
                        [
                            "olson" => "America/Dawson_Creek",
                            "offset" => "-7",
                            "ms" => "Mountain Standard Time",
                            "name" => "[UTC-07:00] Mountain Time [US & Canada]",
                            "utc_offset" => "-07:00",
                        ],

                    "3" =>
                        [
                            "olson" => "America/Denver",
                            "offset" => "-7",
                            "ms" => "Mountain Standard Time",
                            "name" => "[UTC-07:00] Mountain Time [US & Canada]",
                            "utc_offset" => "-07:00",
                        ],

                    "4" =>
                        [
                            "olson" => "America/Edmonton",
                            "offset" => "-7",
                            "ms" => "Mountain Standard Time",
                            "name" => "[UTC-07:00] Mountain Time [US & Canada]",
                            "utc_offset" => "-07:00",
                        ],

                    "5" =>
                        [
                            "olson" => "America/Hermosillo",
                            "offset" => "-7",
                            "ms" => "Mountain Standard Time",
                            "name" => "[UTC-07:00] Mountain Time [US & Canada]",
                            "utc_offset" => "-07:00",
                        ],

                    "6" =>
                        [
                            "olson" => "America/Inuvik",
                            "offset" => "-7",
                            "ms" => "Mountain Standard Time",
                            "name" => "[UTC-07:00] Mountain Time [US & Canada]",
                            "utc_offset" => "-07:00",
                        ],

                    "7" =>
                        [
                            "olson" => "America/Mazatlan",
                            "offset" => "-7",
                            "ms" => "Mountain Standard Time",
                            "name" => "[UTC-07:00] Mountain Time [US & Canada]",
                            "utc_offset" => "-07:00",
                        ],

                    "8" =>
                        [
                            "olson" => "America/Shiprock",
                            "offset" => "-7",
                            "ms" => "Mountain Standard Time",
                            "name" => "[UTC-07:00] Mountain Time [US & Canada]",
                            "utc_offset" => "-07:00",
                        ],

                    "9" =>
                        [
                            "olson" => "America/Yellowknife",
                            "offset" => "-7",
                            "ms" => "Mountain Standard Time",
                            "name" => "[UTC-07:00] Mountain Time [US & Canada]",
                            "utc_offset" => "-07:00",
                        ],

                ],

            "Mountain Standard Time [Mexico]" =>
                [
                    "0" =>
                        [
                            "olson" => "America/Chihuahua",
                            "offset" => "-7",
                            "ms" => "Mountain Standard Time [Mexico]",
                            "name" => "[UTC-07:00] Chihuahua, La Paz, Mazatlan",
                            "utc_offset" => "-07:00",
                        ],

                ],

            "US Mountain Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "America/Ojinaga",
                            "offset" => "-7",
                            "ms" => "US Mountain Standard Time",
                            "name" => "[UTC-07:00] Arizona",
                            "utc_offset" => "-07:00",
                        ],

                    "1" =>
                        [
                            "olson" => "America/Phoenix",
                            "offset" => "-7",
                            "ms" => "US Mountain Standard Time",
                            "name" => "[UTC-07:00] Arizona",
                            "utc_offset" => "-07:00",
                        ],

                ],

            "Central America Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "America/Belize",
                            "offset" => "-6",
                            "ms" => "Central America Standard Time",
                            "name" => "[UTC-06:00] Central America",
                            "utc_offset" => "-06:00",
                        ],

                    "1" =>
                        [
                            "olson" => "America/Costa_Rica",
                            "offset" => "-6",
                            "ms" => "Central America Standard Time",
                            "name" => "[UTC-06:00] Central America",
                            "utc_offset" => "-06:00",
                        ],

                    "2" =>
                        [
                            "olson" => "America/El_Salvador",
                            "offset" => "-6",
                            "ms" => "Central America Standard Time",
                            "name" => "[UTC-06:00] Central America",
                            "utc_offset" => "-06:00",
                        ],

                    "3" =>
                        [
                            "olson" => "America/Guatemala",
                            "offset" => "-6",
                            "ms" => "Central America Standard Time",
                            "name" => "[UTC-06:00] Central America",
                            "utc_offset" => "-06:00",
                        ],

                    "4" =>
                        [
                            "olson" => "Pacific/Easter",
                            "offset" => "-6",
                            "ms" => "Central America Standard Time",
                            "name" => "[UTC-06:00] Central America",
                            "utc_offset" => "-06:00",
                        ],

                    "5" =>
                        [
                            "olson" => "Pacific/Galapagos",
                            "offset" => "-6",
                            "ms" => "Central America Standard Time",
                            "name" => "[UTC-06:00] Central America",
                            "utc_offset" => "-06:00",
                        ],

                ],

            "Central Standard Time [Mexico]" =>
                [
                    "0" =>
                        [
                            "olson" => "America/Cancun",
                            "offset" => "-6",
                            "ms" => "Central Standard Time [Mexico]",
                            "name" => "[UTC-06:00] Guadalajara, Mexico City, Monterrey",
                            "utc_offset" => "-06:00",
                        ],

                    "1" =>
                        [
                            "olson" => "America/Mexico_City",
                            "offset" => "-6",
                            "ms" => "Central Standard Time [Mexico]",
                            "name" => "[UTC-06:00] Guadalajara, Mexico City, Monterrey",
                            "utc_offset" => "-06:00",
                        ],

                    "2" =>
                        [
                            "olson" => "America/Monterrey",
                            "offset" => "-6",
                            "ms" => "Central Standard Time [Mexico]",
                            "name" => "[UTC-06:00] Guadalajara, Mexico City, Monterrey",
                            "utc_offset" => "-06:00",
                        ],

                ],

            "Central Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "America/Chicago",
                            "offset" => "-6",
                            "ms" => "Central Standard Time",
                            "name" => "[UTC-06:00] Central Time [US & Canada]",
                            "utc_offset" => "-06:00",
                        ],

                    "1" =>
                        [
                            "olson" => "America/Indiana/Knox",
                            "offset" => "-6",
                            "ms" => "Central Standard Time",
                            "name" => "[UTC-06:00] Central Time [US & Canada]",
                            "utc_offset" => "-06:00",
                        ],

                    "2" =>
                        [
                            "olson" => "America/Indiana/Tell_City",
                            "offset" => "-6",
                            "ms" => "Central Standard Time",
                            "name" => "[UTC-06:00] Central Time [US & Canada]",
                            "utc_offset" => "-06:00",
                        ],

                    "3" =>
                        [
                            "olson" => "America/Managua",
                            "offset" => "-6",
                            "ms" => "Central Standard Time",
                            "name" => "[UTC-06:00] Central Time [US & Canada]",
                            "utc_offset" => "-06:00",
                        ],

                    "4" =>
                        [
                            "olson" => "America/Matamoros",
                            "offset" => "-6",
                            "ms" => "Central Standard Time",
                            "name" => "[UTC-06:00] Central Time [US & Canada]",
                            "utc_offset" => "-06:00",
                        ],

                    "5" =>
                        [
                            "olson" => "America/Menominee",
                            "offset" => "-6",
                            "ms" => "Central Standard Time",
                            "name" => "[UTC-06:00] Central Time [US & Canada]",
                            "utc_offset" => "-06:00",
                        ],

                    "6" =>
                        [
                            "olson" => "America/Merida",
                            "offset" => "-6",
                            "ms" => "Central Standard Time",
                            "name" => "[UTC-06:00] Central Time [US & Canada]",
                            "utc_offset" => "-06:00",
                        ],

                    "7" =>
                        [
                            "olson" => "America/North_Dakota/Center",
                            "offset" => "-6",
                            "ms" => "Central Standard Time",
                            "name" => "[UTC-06:00] Central Time [US & Canada]",
                            "utc_offset" => "-06:00",
                        ],

                    "8" =>
                        [
                            "olson" => "America/North_Dakota/New_Salem",
                            "offset" => "-6",
                            "ms" => "Central Standard Time",
                            "name" => "[UTC-06:00] Central Time [US & Canada]",
                            "utc_offset" => "-06:00",
                        ],

                    "9" =>
                        [
                            "olson" => "America/Rainy_River",
                            "offset" => "-6",
                            "ms" => "Central Standard Time",
                            "name" => "[UTC-06:00] Central Time [US & Canada]",
                            "utc_offset" => "-06:00",
                        ],

                    "10" =>
                        [
                            "olson" => "America/Rankin_Inlet",
                            "offset" => "-6",
                            "ms" => "Central Standard Time",
                            "name" => "[UTC-06:00] Central Time [US & Canada]",
                            "utc_offset" => "-06:00",
                        ],

                    "11" =>
                        [
                            "olson" => "America/Regina",
                            "offset" => "-6",
                            "ms" => "Central Standard Time",
                            "name" => "[UTC-06:00] Central Time [US & Canada]",
                            "utc_offset" => "-06:00",
                        ],

                    "12" =>
                        [
                            "olson" => "America/Swift_Current",
                            "offset" => "-6",
                            "ms" => "Central Standard Time",
                            "name" => "[UTC-06:00] Central Time [US & Canada]",
                            "utc_offset" => "-06:00",
                        ],

                    "13" =>
                        [
                            "olson" => "America/Tegucigalpa",
                            "offset" => "-6",
                            "ms" => "Central Standard Time",
                            "name" => "[UTC-06:00] Central Time [US & Canada]",
                            "utc_offset" => "-06:00",
                        ],

                    "14" =>
                        [
                            "olson" => "America/Winnipeg",
                            "offset" => "-6",
                            "ms" => "Central Standard Time",
                            "name" => "[UTC-06:00] Central Time [US & Canada]",
                            "utc_offset" => "-06:00",
                        ],

                ],

            "Eastern Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "America/Atikokan",
                            "offset" => "-5",
                            "ms" => "Eastern Standard Time",
                            "name" => "[UTC-05:00] Eastern Time [US & Canada]",
                            "utc_offset" => "-05:00",
                        ],

                    "1" =>
                        [
                            "olson" => "America/Cayman",
                            "offset" => "-5",
                            "ms" => "Eastern Standard Time",
                            "name" => "[UTC-05:00] Eastern Time [US & Canada]",
                            "utc_offset" => "-05:00",
                        ],

                    "2" =>
                        [
                            "olson" => "America/Detroit",
                            "offset" => "-5",
                            "ms" => "Eastern Standard Time",
                            "name" => "[UTC-05:00] Eastern Time [US & Canada]",
                            "utc_offset" => "-05:00",
                        ],

                    "3" =>
                        [
                            "olson" => "America/Grand_Turk",
                            "offset" => "-5",
                            "ms" => "Eastern Standard Time",
                            "name" => "[UTC-05:00] Eastern Time [US & Canada]",
                            "utc_offset" => "-05:00",
                        ],

                    "4" =>
                        [
                            "olson" => "America/Guayaquil",
                            "offset" => "-5",
                            "ms" => "Eastern Standard Time",
                            "name" => "[UTC-05:00] Eastern Time [US & Canada]",
                            "utc_offset" => "-05:00",
                        ],

                    "5" =>
                        [
                            "olson" => "America/Iqaluit",
                            "offset" => "-5",
                            "ms" => "Eastern Standard Time",
                            "name" => "[UTC-05:00] Eastern Time [US & Canada]",
                            "utc_offset" => "-05:00",
                        ],

                    "6" =>
                        [
                            "olson" => "America/Jamaica",
                            "offset" => "-5",
                            "ms" => "Eastern Standard Time",
                            "name" => "[UTC-05:00] Eastern Time [US & Canada]",
                            "utc_offset" => "-05:00",
                        ],

                    "7" =>
                        [
                            "olson" => "America/Kentucky/Louisville",
                            "offset" => "-5",
                            "ms" => "Eastern Standard Time",
                            "name" => "[UTC-05:00] Eastern Time [US & Canada]",
                            "utc_offset" => "-05:00",
                        ],

                    "8" =>
                        [
                            "olson" => "America/Kentucky/Monticello",
                            "offset" => "-5",
                            "ms" => "Eastern Standard Time",
                            "name" => "[UTC-05:00] Eastern Time [US & Canada]",
                            "utc_offset" => "-05:00",
                        ],

                    "9" =>
                        [
                            "olson" => "America/Montreal",
                            "offset" => "-5",
                            "ms" => "Eastern Standard Time",
                            "name" => "[UTC-05:00] Eastern Time [US & Canada]",
                            "utc_offset" => "-05:00",
                        ],

                    "10" =>
                        [
                            "olson" => "America/Nassau",
                            "offset" => "-5",
                            "ms" => "Eastern Standard Time",
                            "name" => "[UTC-05:00] Eastern Time [US & Canada]",
                            "utc_offset" => "-05:00",
                        ],

                    "11" =>
                        [
                            "olson" => "America/New_York",
                            "offset" => "-5",
                            "ms" => "Eastern Standard Time",
                            "name" => "[UTC-05:00] Eastern Time [US & Canada]",
                            "utc_offset" => "-05:00",
                        ],

                    "12" =>
                        [
                            "olson" => "America/Nipigon",
                            "offset" => "-5",
                            "ms" => "Eastern Standard Time",
                            "name" => "[UTC-05:00] Eastern Time [US & Canada]",
                            "utc_offset" => "-05:00",
                        ],

                    "13" =>
                        [
                            "olson" => "America/Pangnirtung",
                            "offset" => "-5",
                            "ms" => "Eastern Standard Time",
                            "name" => "[UTC-05:00] Eastern Time [US & Canada]",
                            "utc_offset" => "-05:00",
                        ],

                    "14" =>
                        [
                            "olson" => "America/Port-au-Prince",
                            "offset" => "-5",
                            "ms" => "Eastern Standard Time",
                            "name" => "[UTC-05:00] Eastern Time [US & Canada]",
                            "utc_offset" => "-05:00",
                        ],

                    "15" =>
                        [
                            "olson" => "America/Resolute",
                            "offset" => "-5",
                            "ms" => "Eastern Standard Time",
                            "name" => "[UTC-05:00] Eastern Time [US & Canada]",
                            "utc_offset" => "-05:00",
                        ],

                    "16" =>
                        [
                            "olson" => "America/Thunder_Bay",
                            "offset" => "-5",
                            "ms" => "Eastern Standard Time",
                            "name" => "[UTC-05:00] Eastern Time [US & Canada]",
                            "utc_offset" => "-05:00",
                        ],

                    "17" =>
                        [
                            "olson" => "America/Toronto",
                            "offset" => "-5",
                            "ms" => "Eastern Standard Time",
                            "name" => "[UTC-05:00] Eastern Time [US & Canada]",
                            "utc_offset" => "-05:00",
                        ],

                ],

            "SA Pacific Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "America/Bogota",
                            "offset" => "-5",
                            "ms" => "SA Pacific Standard Time",
                            "name" => "[UTC-05:00] Bogota, Lima, Quito",
                            "utc_offset" => "-05:00",
                        ],

                    "1" =>
                        [
                            "olson" => "America/Havana",
                            "offset" => "-5",
                            "ms" => "SA Pacific Standard Time",
                            "name" => "[UTC-05:00] Bogota, Lima, Quito",
                            "utc_offset" => "-05:00",
                        ],

                    "2" =>
                        [
                            "olson" => "America/Lima",
                            "offset" => "-5",
                            "ms" => "SA Pacific Standard Time",
                            "name" => "[UTC-05:00] Bogota, Lima, Quito",
                            "utc_offset" => "-05:00",
                        ],

                    "3" =>
                        [
                            "olson" => "America/Panama",
                            "offset" => "-5",
                            "ms" => "SA Pacific Standard Time",
                            "name" => "[UTC-05:00] Bogota, Lima, Quito",
                            "utc_offset" => "-05:00",
                        ],

                ],

            "US Eastern Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "America/Indiana/Indianapolis",
                            "offset" => "-5",
                            "ms" => "US Eastern Standard Time",
                            "name" => "[UTC-05:00] Indiana [East]",
                            "utc_offset" => "-05:00",
                        ],

                    "1" =>
                        [
                            "olson" => "America/Indiana/Marengo",
                            "offset" => "-5",
                            "ms" => "US Eastern Standard Time",
                            "name" => "[UTC-05:00] Indiana [East]",
                            "utc_offset" => "-05:00",
                        ],

                    "2" =>
                        [
                            "olson" => "America/Indiana/Petersburg",
                            "offset" => "-5",
                            "ms" => "US Eastern Standard Time",
                            "name" => "[UTC-05:00] Indiana [East]",
                            "utc_offset" => "-05:00",
                        ],

                    "3" =>
                        [
                            "olson" => "America/Indiana/Vevay",
                            "offset" => "-5",
                            "ms" => "US Eastern Standard Time",
                            "name" => "[UTC-05:00] Indiana [East]",
                            "utc_offset" => "-05:00",
                        ],

                    "4" =>
                        [
                            "olson" => "America/Indiana/Vincennes",
                            "offset" => "-5",
                            "ms" => "US Eastern Standard Time",
                            "name" => "[UTC-05:00] Indiana [East]",
                            "utc_offset" => "-05:00",
                        ],

                    "5" =>
                        [
                            "olson" => "America/Indiana/Winamac",
                            "offset" => "-5",
                            "ms" => "US Eastern Standard Time",
                            "name" => "[UTC-05:00] Indiana [East]",
                            "utc_offset" => "-05:00",
                        ],

                ],

            "Venezuela Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "America/Caracas",
                            "offset" => "-4.5",
                            "ms" => "Venezuela Standard Time",
                            "name" => "[UTC-04:30] Caracas",
                            "utc_offset" => "-04:30",
                        ],

                ],

            "Pacific SA Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "America/Anguilla",
                            "offset" => "-4",
                            "ms" => "Pacific SA Standard Time",
                            "name" => "[UTC-04:00] Santiago",
                            "utc_offset" => "-04:00",
                        ],

                    "1" =>
                        [
                            "olson" => "America/Barbados",
                            "offset" => "-4",
                            "ms" => "Pacific SA Standard Time",
                            "name" => "[UTC-04:00] Santiago",
                            "utc_offset" => "-04:00",
                        ],

                    "2" =>
                        [
                            "olson" => "America/Dominica",
                            "offset" => "-4",
                            "ms" => "Pacific SA Standard Time",
                            "name" => "[UTC-04:00] Santiago",
                            "utc_offset" => "-04:00",
                        ],

                    "3" =>
                        [
                            "olson" => "America/Santiago",
                            "offset" => "-4",
                            "ms" => "Pacific SA Standard Time",
                            "name" => "[UTC-04:00] Santiago",
                            "utc_offset" => "-04:00",
                        ],

                ],

            "Atlantic Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "America/Antigua",
                            "offset" => "-4",
                            "ms" => "Atlantic Standard Time",
                            "name" => "[UTC-04:00] Atlantic Time [Canada]",
                            "utc_offset" => "-04:00",
                        ],

                    "1" =>
                        [
                            "olson" => "America/Aruba",
                            "offset" => "-4",
                            "ms" => "Atlantic Standard Time",
                            "name" => "[UTC-04:00] Atlantic Time [Canada]",
                            "utc_offset" => "-04:00",
                        ],

                    "2" =>
                        [
                            "olson" => "America/Blanc-Sablon",
                            "offset" => "-4",
                            "ms" => "Atlantic Standard Time",
                            "name" => "[UTC-04:00] Atlantic Time [Canada]",
                            "utc_offset" => "-04:00",
                        ],

                    "3" =>
                        [
                            "olson" => "America/Boa_Vista",
                            "offset" => "-4",
                            "ms" => "Atlantic Standard Time",
                            "name" => "[UTC-04:00] Atlantic Time [Canada]",
                            "utc_offset" => "-04:00",
                        ],

                    "4" =>
                        [
                            "olson" => "America/Campo_Grande",
                            "offset" => "-4",
                            "ms" => "Atlantic Standard Time",
                            "name" => "[UTC-04:00] Atlantic Time [Canada]",
                            "utc_offset" => "-04:00",
                        ],

                    "5" =>
                        [
                            "olson" => "America/Eirunepe",
                            "offset" => "-4",
                            "ms" => "Atlantic Standard Time",
                            "name" => "[UTC-04:00] Atlantic Time [Canada]",
                            "utc_offset" => "-04:00",
                        ],

                    "6" =>
                        [
                            "olson" => "America/Glace_Bay",
                            "offset" => "-4",
                            "ms" => "Atlantic Standard Time",
                            "name" => "[UTC-04:00] Atlantic Time [Canada]",
                            "utc_offset" => "-04:00",
                        ],

                    "7" =>
                        [
                            "olson" => "America/Goose_Bay",
                            "offset" => "-4",
                            "ms" => "Atlantic Standard Time",
                            "name" => "[UTC-04:00] Atlantic Time [Canada]",
                            "utc_offset" => "-04:00",
                        ],

                    "8" =>
                        [
                            "olson" => "America/Grenada",
                            "offset" => "-4",
                            "ms" => "Atlantic Standard Time",
                            "name" => "[UTC-04:00] Atlantic Time [Canada]",
                            "utc_offset" => "-04:00",
                        ],

                    "9" =>
                        [
                            "olson" => "America/Guadeloupe",
                            "offset" => "-4",
                            "ms" => "Atlantic Standard Time",
                            "name" => "[UTC-04:00] Atlantic Time [Canada]",
                            "utc_offset" => "-04:00",
                        ],

                    "10" =>
                        [
                            "olson" => "America/Guyana",
                            "offset" => "-4",
                            "ms" => "Atlantic Standard Time",
                            "name" => "[UTC-04:00] Atlantic Time [Canada]",
                            "utc_offset" => "-04:00",
                        ],

                    "11" =>
                        [
                            "olson" => "America/Halifax",
                            "offset" => "-4",
                            "ms" => "Atlantic Standard Time",
                            "name" => "[UTC-04:00] Atlantic Time [Canada]",
                            "utc_offset" => "-04:00",
                        ],

                    "12" =>
                        [
                            "olson" => "America/Marigot",
                            "offset" => "-4",
                            "ms" => "Atlantic Standard Time",
                            "name" => "[UTC-04:00] Atlantic Time [Canada]",
                            "utc_offset" => "-04:00",
                        ],

                    "13" =>
                        [
                            "olson" => "America/Martinique",
                            "offset" => "-4",
                            "ms" => "Atlantic Standard Time",
                            "name" => "[UTC-04:00] Atlantic Time [Canada]",
                            "utc_offset" => "-04:00",
                        ],

                    "14" =>
                        [
                            "olson" => "America/Moncton",
                            "offset" => "-4",
                            "ms" => "Atlantic Standard Time",
                            "name" => "[UTC-04:00] Atlantic Time [Canada]",
                            "utc_offset" => "-04:00",
                        ],

                    "15" =>
                        [
                            "olson" => "America/Montserrat",
                            "offset" => "-4",
                            "ms" => "Atlantic Standard Time",
                            "name" => "[UTC-04:00] Atlantic Time [Canada]",
                            "utc_offset" => "-04:00",
                        ],

                    "16" =>
                        [
                            "olson" => "America/Port_of_Spain",
                            "offset" => "-4",
                            "ms" => "Atlantic Standard Time",
                            "name" => "[UTC-04:00] Atlantic Time [Canada]",
                            "utc_offset" => "-04:00",
                        ],

                    "17" =>
                        [
                            "olson" => "America/Santo_Domingo",
                            "offset" => "-4",
                            "ms" => "Atlantic Standard Time",
                            "name" => "[UTC-04:00] Atlantic Time [Canada]",
                            "utc_offset" => "-04:00",
                        ],

                    "18" =>
                        [
                            "olson" => "America/St_Barthelemy",
                            "offset" => "-4",
                            "ms" => "Atlantic Standard Time",
                            "name" => "[UTC-04:00] Atlantic Time [Canada]",
                            "utc_offset" => "-04:00",
                        ],

                    "19" =>
                        [
                            "olson" => "America/St_Kitts",
                            "offset" => "-4",
                            "ms" => "Atlantic Standard Time",
                            "name" => "[UTC-04:00] Atlantic Time [Canada]",
                            "utc_offset" => "-04:00",
                        ],

                    "20" =>
                        [
                            "olson" => "America/St_Lucia",
                            "offset" => "-4",
                            "ms" => "Atlantic Standard Time",
                            "name" => "[UTC-04:00] Atlantic Time [Canada]",
                            "utc_offset" => "-04:00",
                        ],

                    "21" =>
                        [
                            "olson" => "America/St_Thomas",
                            "offset" => "-4",
                            "ms" => "Atlantic Standard Time",
                            "name" => "[UTC-04:00] Atlantic Time [Canada]",
                            "utc_offset" => "-04:00",
                        ],

                    "22" =>
                        [
                            "olson" => "America/St_Vincent",
                            "offset" => "-4",
                            "ms" => "Atlantic Standard Time",
                            "name" => "[UTC-04:00] Atlantic Time [Canada]",
                            "utc_offset" => "-04:00",
                        ],

                    "23" =>
                        [
                            "olson" => "America/Thule",
                            "offset" => "-4",
                            "ms" => "Atlantic Standard Time",
                            "name" => "[UTC-04:00] Atlantic Time [Canada]",
                            "utc_offset" => "-04:00",
                        ],

                    "24" =>
                        [
                            "olson" => "America/Tortola",
                            "offset" => "-4",
                            "ms" => "Atlantic Standard Time",
                            "name" => "[UTC-04:00] Atlantic Time [Canada]",
                            "utc_offset" => "-04:00",
                        ],

                    "25" =>
                        [
                            "olson" => "Antarctica/Palmer",
                            "offset" => "-4",
                            "ms" => "Atlantic Standard Time",
                            "name" => "[UTC-04:00] Atlantic Time [Canada]",
                            "utc_offset" => "-04:00",
                        ],

                    "26" =>
                        [
                            "olson" => "Atlantic/Bermuda",
                            "offset" => "-4",
                            "ms" => "Atlantic Standard Time",
                            "name" => "[UTC-04:00] Atlantic Time [Canada]",
                            "utc_offset" => "-04:00",
                        ],

                ],

            "Paraguay Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "America/Asuncion",
                            "offset" => "-4",
                            "ms" => "Paraguay Standard Time",
                            "name" => "[UTC-04:00] Asuncion",
                            "utc_offset" => "-04:00",
                        ],

                ],

            "Central Brazilian Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "America/Cuiaba",
                            "offset" => "-4",
                            "ms" => "Central Brazilian Standard Time",
                            "name" => "[UTC-04:00] Cuiaba",
                            "utc_offset" => "-04:00",
                        ],

                    "1" =>
                        [
                            "olson" => "America/Curacao",
                            "offset" => "-4",
                            "ms" => "Central Brazilian Standard Time",
                            "name" => "[UTC-04:00] Cuiaba",
                            "utc_offset" => "-04:00",
                        ],

                    "2" =>
                        [
                            "olson" => "America/Porto_Velho",
                            "offset" => "-4",
                            "ms" => "Central Brazilian Standard Time",
                            "name" => "[UTC-04:00] Cuiaba",
                            "utc_offset" => "-04:00",
                        ],

                    "3" =>
                        [
                            "olson" => "America/Puerto_Rico",
                            "offset" => "-4",
                            "ms" => "Central Brazilian Standard Time",
                            "name" => "[UTC-04:00] Cuiaba",
                            "utc_offset" => "-04:00",
                        ],

                    "4" =>
                        [
                            "olson" => "America/Rio_Branco",
                            "offset" => "-4",
                            "ms" => "Central Brazilian Standard Time",
                            "name" => "[UTC-04:00] Cuiaba",
                            "utc_offset" => "-04:00",
                        ],

                ],

            "SA Western Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "America/La_Paz",
                            "offset" => "-4",
                            "ms" => "SA Western Standard Time",
                            "name" => "[UTC-04:00] Georgetown, La Paz, Manaus, San Juan",
                            "utc_offset" => "-04:00",
                        ],

                    "1" =>
                        [
                            "olson" => "America/Manaus",
                            "offset" => "-4",
                            "ms" => "SA Western Standard Time",
                            "name" => "[UTC-04:00] Georgetown, La Paz, Manaus, San Juan",
                            "utc_offset" => "-04:00",
                        ],

                ],

            "Newfoundland Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "America/St_Johns",
                            "offset" => "-3.5",
                            "ms" => "Newfoundland Standard Time",
                            "name" => "[UTC-03:30] Newfoundland",
                            "utc_offset" => "-03:30",
                        ],

                ],

            "E. South America Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "America/Araguaina",
                            "offset" => "-3",
                            "ms" => "E. South America Standard Time",
                            "name" => "[UTC-03:00] Brasilia",
                            "utc_offset" => "-03:00",
                        ],

                    "1" =>
                        [
                            "olson" => "America/Bahia",
                            "offset" => "-3",
                            "ms" => "E. South America Standard Time",
                            "name" => "[UTC-03:00] Brasilia",
                            "utc_offset" => "-03:00",
                        ],

                    "2" =>
                        [
                            "olson" => "America/Belem",
                            "offset" => "-3",
                            "ms" => "E. South America Standard Time",
                            "name" => "[UTC-03:00] Brasilia",
                            "utc_offset" => "-03:00",
                        ],

                    "3" =>
                        [
                            "olson" => "America/Maceio",
                            "offset" => "-3",
                            "ms" => "E. South America Standard Time",
                            "name" => "[UTC-03:00] Brasilia",
                            "utc_offset" => "-03:00",
                        ],

                    "4" =>
                        [
                            "olson" => "America/Miquelon",
                            "offset" => "-3",
                            "ms" => "E. South America Standard Time",
                            "name" => "[UTC-03:00] Brasilia",
                            "utc_offset" => "-03:00",
                        ],

                    "5" =>
                        [
                            "olson" => "America/Santarem",
                            "offset" => "-3",
                            "ms" => "E. South America Standard Time",
                            "name" => "[UTC-03:00] Brasilia",
                            "utc_offset" => "-03:00",
                        ],

                    "6" =>
                        [
                            "olson" => "America/Sao_Paulo",
                            "offset" => "-3",
                            "ms" => "E. South America Standard Time",
                            "name" => "[UTC-03:00] Brasilia",
                            "utc_offset" => "-03:00",
                        ],

                ],

            "Argentina Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "America/Argentina/Buenos_Aires",
                            "offset" => "-3",
                            "ms" => "Argentina Standard Time",
                            "name" => "[UTC-03:00] Buenos Aires",
                            "utc_offset" => "-03:00",
                        ],

                    "1" =>
                        [
                            "olson" => "America/Argentina/Catamarca",
                            "offset" => "-3",
                            "ms" => "Argentina Standard Time",
                            "name" => "[UTC-03:00] Buenos Aires",
                            "utc_offset" => "-03:00",
                        ],

                    "2" =>
                        [
                            "olson" => "America/Argentina/Cordoba",
                            "offset" => "-3",
                            "ms" => "Argentina Standard Time",
                            "name" => "[UTC-03:00] Buenos Aires",
                            "utc_offset" => "-03:00",
                        ],

                    "3" =>
                        [
                            "olson" => "America/Argentina/Jujuy",
                            "offset" => "-3",
                            "ms" => "Argentina Standard Time",
                            "name" => "[UTC-03:00] Buenos Aires",
                            "utc_offset" => "-03:00",
                        ],

                    "4" =>
                        [
                            "olson" => "America/Argentina/La_Rioja",
                            "offset" => "-3",
                            "ms" => "Argentina Standard Time",
                            "name" => "[UTC-03:00] Buenos Aires",
                            "utc_offset" => "-03:00",
                        ],

                    "5" =>
                        [
                            "olson" => "America/Argentina/Mendoza",
                            "offset" => "-3",
                            "ms" => "Argentina Standard Time",
                            "name" => "[UTC-03:00] Buenos Aires",
                            "utc_offset" => "-03:00",
                        ],

                    "6" =>
                        [
                            "olson" => "America/Argentina/Rio_Gallegos",
                            "offset" => "-3",
                            "ms" => "Argentina Standard Time",
                            "name" => "[UTC-03:00] Buenos Aires",
                            "utc_offset" => "-03:00",
                        ],

                    "7" =>
                        [
                            "olson" => "America/Argentina/Salta",
                            "offset" => "-3",
                            "ms" => "Argentina Standard Time",
                            "name" => "[UTC-03:00] Buenos Aires",
                            "utc_offset" => "-03:00",
                        ],

                    "8" =>
                        [
                            "olson" => "America/Argentina/San_Juan",
                            "offset" => "-3",
                            "ms" => "Argentina Standard Time",
                            "name" => "[UTC-03:00] Buenos Aires",
                            "utc_offset" => "-03:00",
                        ],

                    "9" =>
                        [
                            "olson" => "America/Argentina/San_Luis",
                            "offset" => "-3",
                            "ms" => "Argentina Standard Time",
                            "name" => "[UTC-03:00] Buenos Aires",
                            "utc_offset" => "-03:00",
                        ],

                    "10" =>
                        [
                            "olson" => "America/Argentina/Tucuman",
                            "offset" => "-3",
                            "ms" => "Argentina Standard Time",
                            "name" => "[UTC-03:00] Buenos Aires",
                            "utc_offset" => "-03:00",
                        ],

                    "11" =>
                        [
                            "olson" => "America/Argentina/Ushuaia",
                            "offset" => "-3",
                            "ms" => "Argentina Standard Time",
                            "name" => "[UTC-03:00] Buenos Aires",
                            "utc_offset" => "-03:00",
                        ],

                    "12" =>
                        [
                            "olson" => "Antarctica/Rothera",
                            "offset" => "-3",
                            "ms" => "Argentina Standard Time",
                            "name" => "[UTC-03:00] Buenos Aires",
                            "utc_offset" => "-03:00",
                        ],

                    "13" =>
                        [
                            "olson" => "Atlantic/Stanley",
                            "offset" => "-3",
                            "ms" => "Argentina Standard Time",
                            "name" => "[UTC-03:00] Buenos Aires",
                            "utc_offset" => "-03:00",
                        ],

                ],

            "SA Eastern Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "America/Cayenne",
                            "offset" => "-3",
                            "ms" => "SA Eastern Standard Time",
                            "name" => "[UTC-03:00] Cayenne, Fortaleza",
                            "utc_offset" => "-03:00",
                        ],

                    "1" =>
                        [
                            "olson" => "America/Fortaleza",
                            "offset" => "-3",
                            "ms" => "SA Eastern Standard Time",
                            "name" => "[UTC-03:00] Cayenne, Fortaleza",
                            "utc_offset" => "-03:00",
                        ],

                    "2" =>
                        [
                            "olson" => "America/Paramaribo",
                            "offset" => "-3",
                            "ms" => "SA Eastern Standard Time",
                            "name" => "[UTC-03:00] Cayenne, Fortaleza",
                            "utc_offset" => "-03:00",
                        ],

                    "3" =>
                        [
                            "olson" => "America/Recife",
                            "offset" => "-3",
                            "ms" => "SA Eastern Standard Time",
                            "name" => "[UTC-03:00] Cayenne, Fortaleza",
                            "utc_offset" => "-03:00",
                        ],

                ],

            "Greenland Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "America/Godthab",
                            "offset" => "-3",
                            "ms" => "Greenland Standard Time",
                            "name" => "[UTC-03:00] Greenland",
                            "utc_offset" => "-03:00",
                        ],

                ],

            "Montevideo Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "America/Montevideo",
                            "offset" => "-3",
                            "ms" => "Montevideo Standard Time",
                            "name" => "[UTC-03:00] Montevideo",
                            "utc_offset" => "-03:00",
                        ],

                ],

            "Mid-Atlantic Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "America/Noronha",
                            "offset" => "-2",
                            "ms" => "Mid-Atlantic Standard Time",
                            "name" => "[UTC-02:00] Mid-Atlantic",
                            "utc_offset" => "-02:00",
                        ],

                    "1" =>
                        [
                            "olson" => "Atlantic/South_Georgia",
                            "offset" => "-2",
                            "ms" => "Mid-Atlantic Standard Time",
                            "name" => "[UTC-02:00] Mid-Atlantic",
                            "utc_offset" => "-02:00",
                        ],

                ],

            "Azores Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "America/Scoresbysund",
                            "offset" => "-1",
                            "ms" => "Azores Standard Time",
                            "name" => "[UTC-01:00] Azores",
                            "utc_offset" => "-01:00",
                        ],

                    "1" =>
                        [
                            "olson" => "Atlantic/Azores",
                            "offset" => "-1",
                            "ms" => "Azores Standard Time",
                            "name" => "[UTC-01:00] Azores",
                            "utc_offset" => "-01:00",
                        ],

                ],

            "Cape Verde Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Atlantic/Cape_Verde",
                            "offset" => "-1",
                            "ms" => "Cape Verde Standard Time",
                            "name" => "[UTC-01:00] Cape Verde Is.",
                            "utc_offset" => "-01:00",
                        ],

                ],

            "GMT Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Africa/Abidjan",
                            "offset" => "0",
                            "ms" => "GMT Standard Time",
                            "name" => "[UTC] Dublin, Edinburgh, Lisbon, London",
                            "utc_offset" => '+0:00'
                        ],

                    "1" =>
                        [
                            "olson" => "Africa/Accra",
                            "offset" => "0",
                            "ms" => "GMT Standard Time",
                            "name" => "[UTC] Dublin, Edinburgh, Lisbon, London",
                            "utc_offset" => '+0:00'
                        ],

                    "2" =>
                        [
                            "olson" => "Africa/Bamako",
                            "offset" => "0",
                            "ms" => "GMT Standard Time",
                            "name" => "[UTC] Dublin, Edinburgh, Lisbon, London",
                            "utc_offset" => '+0:00'
                        ],

                    "3" =>
                        [
                            "olson" => "Africa/Banjul",
                            "offset" => "0",
                            "ms" => "GMT Standard Time",
                            "name" => "[UTC] Dublin, Edinburgh, Lisbon, London",
                            "utc_offset" => '+0:00'
                        ],

                    "4" =>
                        [
                            "olson" => "Africa/Bissau",
                            "offset" => "0",
                            "ms" => "GMT Standard Time",
                            "name" => "[UTC] Dublin, Edinburgh, Lisbon, London",
                            "utc_offset" => '+0:00'
                        ],

                    "5" =>
                        [
                            "olson" => "Africa/Conakry",
                            "offset" => "0",
                            "ms" => "GMT Standard Time",
                            "name" => "[UTC] Dublin, Edinburgh, Lisbon, London",
                            "utc_offset" => '+0:00'
                        ],

                    "6" =>
                        [
                            "olson" => "Africa/Dakar",
                            "offset" => "0",
                            "ms" => "GMT Standard Time",
                            "name" => "[UTC] Dublin, Edinburgh, Lisbon, London",
                            "utc_offset" => '+0:00'
                        ],

                    "7" =>
                        [
                            "olson" => "Africa/El_Aaiun",
                            "offset" => "0",
                            "ms" => "GMT Standard Time",
                            "name" => "[UTC] Dublin, Edinburgh, Lisbon, London",
                            "utc_offset" => '+0:00'
                        ],

                    "8" =>
                        [
                            "olson" => "Africa/Freetown",
                            "offset" => "0",
                            "ms" => "GMT Standard Time",
                            "name" => "[UTC] Dublin, Edinburgh, Lisbon, London",
                            "utc_offset" => '+0:00'
                        ],

                    "9" =>
                        [
                            "olson" => "Africa/Lome",
                            "offset" => "0",
                            "ms" => "GMT Standard Time",
                            "name" => "[UTC] Dublin, Edinburgh, Lisbon, London",
                            "utc_offset" => '+0:00'
                        ],

                    "10" =>
                        [
                            "olson" => "Africa/Monrovia",
                            "offset" => "0",
                            "ms" => "GMT Standard Time",
                            "name" => "[UTC] Dublin, Edinburgh, Lisbon, London",
                            "utc_offset" => '+0:00'
                        ],

                    "11" =>
                        [
                            "olson" => "Africa/Nouakchott",
                            "offset" => "0",
                            "ms" => "GMT Standard Time",
                            "name" => "[UTC] Dublin, Edinburgh, Lisbon, London",
                            "utc_offset" => '+0:00'
                        ],

                    "12" =>
                        [
                            "olson" => "Africa/Ouagadougou",
                            "offset" => "0",
                            "ms" => "GMT Standard Time",
                            "name" => "[UTC] Dublin, Edinburgh, Lisbon, London",
                            "utc_offset" => '+0:00'
                        ],

                    "13" =>
                        [
                            "olson" => "Africa/Sao_Tome",
                            "offset" => "0",
                            "ms" => "GMT Standard Time",
                            "name" => "[UTC] Dublin, Edinburgh, Lisbon, London",
                            "utc_offset" => '+0:00'
                        ],

                    "14" =>
                        [
                            "olson" => "America/Danmarkshavn",
                            "offset" => "0",
                            "ms" => "GMT Standard Time",
                            "name" => "[UTC] Dublin, Edinburgh, Lisbon, London",
                            "utc_offset" => '+0:00'
                        ],

                    "15" =>
                        [
                            "olson" => "Antarctica/Vostok",
                            "offset" => "0",
                            "ms" => "GMT Standard Time",
                            "name" => "[UTC] Dublin, Edinburgh, Lisbon, London",
                            "utc_offset" => '+0:00'
                        ],

                    "16" =>
                        [
                            "olson" => "Atlantic/Canary",
                            "offset" => "0",
                            "ms" => "GMT Standard Time",
                            "name" => "[UTC] Dublin, Edinburgh, Lisbon, London",
                            "utc_offset" => '+0:00'
                        ],

                    "17" =>
                        [
                            "olson" => "Atlantic/Faroe",
                            "offset" => "0",
                            "ms" => "GMT Standard Time",
                            "name" => "[UTC] Dublin, Edinburgh, Lisbon, London",
                            "utc_offset" => '+0:00'
                        ],

                    "18" =>
                        [
                            "olson" => "Atlantic/Madeira",
                            "offset" => "0",
                            "ms" => "GMT Standard Time",
                            "name" => "[UTC] Dublin, Edinburgh, Lisbon, London",
                            "utc_offset" => '+0:00'
                        ],

                    "19" =>
                        [
                            "olson" => "Europe/Dublin",
                            "offset" => "0",
                            "ms" => "GMT Standard Time",
                            "name" => "[UTC] Dublin, Edinburgh, Lisbon, London",
                            "utc_offset" => '+0:00'
                        ],

                    "20" =>
                        [
                            "olson" => "Europe/Guernsey",
                            "offset" => "0",
                            "ms" => "GMT Standard Time",
                            "name" => "[UTC] Dublin, Edinburgh, Lisbon, London",
                            "utc_offset" => '+0:00'
                        ],

                    "21" =>
                        [
                            "olson" => "Europe/Isle_of_Man",
                            "offset" => "0",
                            "ms" => "GMT Standard Time",
                            "name" => "[UTC] Dublin, Edinburgh, Lisbon, London",
                            "utc_offset" => '+0:00'
                        ],

                    "22" =>
                        [
                            "olson" => "Europe/Jersey",
                            "offset" => "0",
                            "ms" => "GMT Standard Time",
                            "name" => "[UTC] Dublin, Edinburgh, Lisbon, London",
                            "utc_offset" => '+0:00'
                        ],

                    "23" =>
                        [
                            "olson" => "Europe/Lisbon",
                            "offset" => "0",
                            "ms" => "GMT Standard Time",
                            "name" => "[UTC] Dublin, Edinburgh, Lisbon, London",
                            "utc_offset" => '+0:00'
                        ],

                    "24" =>
                        [
                            "olson" => "Europe/London",
                            "offset" => "0",
                            "ms" => "GMT Standard Time",
                            "name" => "[UTC] Dublin, Edinburgh, Lisbon, London",
                            "utc_offset" => '+0:00'
                        ],

                    "25" =>
                        [
                            "olson" => "GMT",
                            "offset" => "0",
                            "ms" => "GMT Standard Time",
                            "name" => "[UTC] Dublin, Edinburgh, Lisbon, London",
                            "utc_offset" => '+0:00'
                        ],

                ],

            "Morocco Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Africa/Casablanca",
                            "offset" => "0",
                            "ms" => "Morocco Standard Time",
                            "name" => "[UTC] Casablanca",
                            "utc_offset" => '+0:00'
                        ],

                ],

            "Greenwich Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Atlantic/Reykjavik",
                            "offset" => "0",
                            "ms" => "Greenwich Standard Time",
                            "name" => "[UTC] Monrovia, Reykjavik",
                            "utc_offset" => '+0:00'
                        ],

                    "1" =>
                        [
                            "olson" => "Atlantic/St_Helena",
                            "offset" => "0",
                            "ms" => "Greenwich Standard Time",
                            "name" => "[UTC] Monrovia, Reykjavik",
                            "utc_offset" => '+0:00'
                        ],

                ],

            "UTC" =>
                [
                    "0" =>
                        [
                            "olson" => "UTC",
                            "offset" => "0",
                            "ms" => "UTC",
                            "name" => "[UTC] Coordinated Universal Time",
                            "utc_offset" => '+0:00'
                        ],

                ],

            "W. Central Africa Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Africa/Algiers",
                            "offset" => "1",
                            "ms" => "W. Central Africa Standard Time",
                            "name" => "[UTC+01:00] West Central Africa",
                            "utc_offset" => "+01:00",
                        ],

                    "1" =>
                        [
                            "olson" => "Africa/Bangui",
                            "offset" => "1",
                            "ms" => "W. Central Africa Standard Time",
                            "name" => "[UTC+01:00] West Central Africa",
                            "utc_offset" => "+01:00",
                        ],

                    "2" =>
                        [
                            "olson" => "Africa/Brazzaville",
                            "offset" => "1",
                            "ms" => "W. Central Africa Standard Time",
                            "name" => "[UTC+01:00] West Central Africa",
                            "utc_offset" => "+01:00",
                        ],

                    "3" =>
                        [
                            "olson" => "Africa/Ceuta",
                            "offset" => "1",
                            "ms" => "W. Central Africa Standard Time",
                            "name" => "[UTC+01:00] West Central Africa",
                            "utc_offset" => "+01:00",
                        ],

                    "4" =>
                        [
                            "olson" => "Africa/Douala",
                            "offset" => "1",
                            "ms" => "W. Central Africa Standard Time",
                            "name" => "[UTC+01:00] West Central Africa",
                            "utc_offset" => "+01:00",
                        ],

                    "5" =>
                        [
                            "olson" => "Africa/Kinshasa",
                            "offset" => "1",
                            "ms" => "W. Central Africa Standard Time",
                            "name" => "[UTC+01:00] West Central Africa",
                            "utc_offset" => "+01:00",
                        ],

                    "6" =>
                        [
                            "olson" => "Africa/Lagos",
                            "offset" => "1",
                            "ms" => "W. Central Africa Standard Time",
                            "name" => "[UTC+01:00] West Central Africa",
                            "utc_offset" => "+01:00",
                        ],

                    "7" =>
                        [
                            "olson" => "Africa/Libreville",
                            "offset" => "1",
                            "ms" => "W. Central Africa Standard Time",
                            "name" => "[UTC+01:00] West Central Africa",
                            "utc_offset" => "+01:00",
                        ],

                    "8" =>
                        [
                            "olson" => "Africa/Luanda",
                            "offset" => "1",
                            "ms" => "W. Central Africa Standard Time",
                            "name" => "[UTC+01:00] West Central Africa",
                            "utc_offset" => "+01:00",
                        ],

                    "9" =>
                        [
                            "olson" => "Africa/Malabo",
                            "offset" => "1",
                            "ms" => "W. Central Africa Standard Time",
                            "name" => "[UTC+01:00] West Central Africa",
                            "utc_offset" => "+01:00",
                        ],

                    "10" =>
                        [
                            "olson" => "Africa/Ndjamena",
                            "offset" => "1",
                            "ms" => "W. Central Africa Standard Time",
                            "name" => "[UTC+01:00] West Central Africa",
                            "utc_offset" => "+01:00",
                        ],

                    "11" =>
                        [
                            "olson" => "Africa/Niamey",
                            "offset" => "1",
                            "ms" => "W. Central Africa Standard Time",
                            "name" => "[UTC+01:00] West Central Africa",
                            "utc_offset" => "+01:00",
                        ],

                    "12" =>
                        [
                            "olson" => "Africa/Porto-Novo",
                            "offset" => "1",
                            "ms" => "W. Central Africa Standard Time",
                            "name" => "[UTC+01:00] West Central Africa",
                            "utc_offset" => "+01:00",
                        ],

                    "13" =>
                        [
                            "olson" => "Africa/Tunis",
                            "offset" => "1",
                            "ms" => "W. Central Africa Standard Time",
                            "name" => "[UTC+01:00] West Central Africa",
                            "utc_offset" => "+01:00",
                        ],

                ],

            "Namibia Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Africa/Windhoek",
                            "offset" => "1",
                            "ms" => "Namibia Standard Time",
                            "name" => "[UTC+01:00] Windhoek",
                            "utc_offset" => "+01:00",
                        ],

                ],

            "W. Europe Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Arctic/Longyearbyen",
                            "offset" => "1",
                            "ms" => "W. Europe Standard Time",
                            "name" => "[UTC+01:00] Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna",
                            "utc_offset" => "+01:00",
                        ],

                    "1" =>
                        [
                            "olson" => "Europe/Amsterdam",
                            "offset" => "1",
                            "ms" => "W. Europe Standard Time",
                            "name" => "[UTC+01:00] Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna",
                            "utc_offset" => "+01:00",
                        ],

                    "2" =>
                        [
                            "olson" => "Europe/Andorra",
                            "offset" => "1",
                            "ms" => "W. Europe Standard Time",
                            "name" => "[UTC+01:00] Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna",
                            "utc_offset" => "+01:00",
                        ],

                    "3" =>
                        [
                            "olson" => "Europe/Berlin",
                            "offset" => "1",
                            "ms" => "W. Europe Standard Time",
                            "name" => "[UTC+01:00] Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna",
                            "utc_offset" => "+01:00",
                        ],

                    "4" =>
                        [
                            "olson" => "Europe/Gibraltar",
                            "offset" => "1",
                            "ms" => "W. Europe Standard Time",
                            "name" => "[UTC+01:00] Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna",
                            "utc_offset" => "+01:00",
                        ],

                    "5" =>
                        [
                            "olson" => "Europe/Luxembourg",
                            "offset" => "1",
                            "ms" => "W. Europe Standard Time",
                            "name" => "[UTC+01:00] Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna",
                            "utc_offset" => "+01:00",
                        ],

                    "6" =>
                        [
                            "olson" => "Europe/Malta",
                            "offset" => "1",
                            "ms" => "W. Europe Standard Time",
                            "name" => "[UTC+01:00] Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna",
                            "utc_offset" => "+01:00",
                        ],

                    "7" =>
                        [
                            "olson" => "Europe/Monaco",
                            "offset" => "1",
                            "ms" => "W. Europe Standard Time",
                            "name" => "[UTC+01:00] Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna",
                            "utc_offset" => "+01:00",
                        ],

                    "8" =>
                        [
                            "olson" => "Europe/Oslo",
                            "offset" => "1",
                            "ms" => "W. Europe Standard Time",
                            "name" => "[UTC+01:00] Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna",
                            "utc_offset" => "+01:00",
                        ],

                    "9" =>
                        [
                            "olson" => "Europe/Podgorica",
                            "offset" => "1",
                            "ms" => "W. Europe Standard Time",
                            "name" => "[UTC+01:00] Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna",
                            "utc_offset" => "+01:00",
                        ],

                    "10" =>
                        [
                            "olson" => "Europe/Rome",
                            "offset" => "1",
                            "ms" => "W. Europe Standard Time",
                            "name" => "[UTC+01:00] Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna",
                            "utc_offset" => "+01:00",
                        ],

                    "11" =>
                        [
                            "olson" => "Europe/San_Marino",
                            "offset" => "1",
                            "ms" => "W. Europe Standard Time",
                            "name" => "[UTC+01:00] Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna",
                            "utc_offset" => "+01:00",
                        ],

                    "12" =>
                        [
                            "olson" => "Europe/Stockholm",
                            "offset" => "1",
                            "ms" => "W. Europe Standard Time",
                            "name" => "[UTC+01:00] Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna",
                            "utc_offset" => "+01:00",
                        ],

                    "13" =>
                        [
                            "olson" => "Europe/Tirane",
                            "offset" => "1",
                            "ms" => "W. Europe Standard Time",
                            "name" => "[UTC+01:00] Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna",
                            "utc_offset" => "+01:00",
                        ],

                    "14" =>
                        [
                            "olson" => "Europe/Vaduz",
                            "offset" => "1",
                            "ms" => "W. Europe Standard Time",
                            "name" => "[UTC+01:00] Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna",
                            "utc_offset" => "+01:00",
                        ],

                    "15" =>
                        [
                            "olson" => "Europe/Vatican",
                            "offset" => "1",
                            "ms" => "W. Europe Standard Time",
                            "name" => "[UTC+01:00] Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna",
                            "utc_offset" => "+01:00",
                        ],

                    "16" =>
                        [
                            "olson" => "Europe/Vienna",
                            "offset" => "1",
                            "ms" => "W. Europe Standard Time",
                            "name" => "[UTC+01:00] Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna",
                            "utc_offset" => "+01:00",
                        ],

                    "17" =>
                        [
                            "olson" => "Europe/Zurich",
                            "offset" => "1",
                            "ms" => "W. Europe Standard Time",
                            "name" => "[UTC+01:00] Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna",
                            "utc_offset" => "+01:00",
                        ],

                ],

            "Central Europe Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Europe/Belgrade",
                            "offset" => "1",
                            "ms" => "Central Europe Standard Time",
                            "name" => "[UTC+01:00] Belgrade, Bratislava, Budapest, Ljubljana, Prague",
                            "utc_offset" => "+01:00",
                        ],

                    "1" =>
                        [
                            "olson" => "Europe/Bratislava",
                            "offset" => "1",
                            "ms" => "Central Europe Standard Time",
                            "name" => "[UTC+01:00] Belgrade, Bratislava, Budapest, Ljubljana, Prague",
                            "utc_offset" => "+01:00",
                        ],

                    "2" =>
                        [
                            "olson" => "Europe/Budapest",
                            "offset" => "1",
                            "ms" => "Central Europe Standard Time",
                            "name" => "[UTC+01:00] Belgrade, Bratislava, Budapest, Ljubljana, Prague",
                            "utc_offset" => "+01:00",
                        ],

                    "3" =>
                        [
                            "olson" => "Europe/Ljubljana",
                            "offset" => "1",
                            "ms" => "Central Europe Standard Time",
                            "name" => "[UTC+01:00] Belgrade, Bratislava, Budapest, Ljubljana, Prague",
                            "utc_offset" => "+01:00",
                        ],

                    "4" =>
                        [
                            "olson" => "Europe/Prague",
                            "offset" => "1",
                            "ms" => "Central Europe Standard Time",
                            "name" => "[UTC+01:00] Belgrade, Bratislava, Budapest, Ljubljana, Prague",
                            "utc_offset" => "+01:00",
                        ],

                    "5" =>
                        [
                            "olson" => "Europe/Sarajevo",
                            "offset" => "1",
                            "ms" => "Central Europe Standard Time",
                            "name" => "[UTC+01:00] Sarajevo, Skopje, Warsaw, Zagreb",
                            "utc_offset" => "+01:00",
                        ],

                    "6" =>
                        [
                            "olson" => "Europe/Skopje",
                            "offset" => "1",
                            "ms" => "Central Europe Standard Time",
                            "name" => "[UTC+01:00] Sarajevo, Skopje, Warsaw, Zagreb",
                            "utc_offset" => "+01:00",
                        ],

                    "7" =>
                        [
                            "olson" => "Europe/Warsaw",
                            "offset" => "1",
                            "ms" => "Central Europe Standard Time",
                            "name" => "[UTC+01:00] Sarajevo, Skopje, Warsaw, Zagreb",
                            "utc_offset" => "+01:00",
                        ],

                    "8" =>
                        [
                            "olson" => "Europe/Zagreb",
                            "offset" => "1",
                            "ms" => "Central Europe Standard Time",
                            "name" => "[UTC+01:00] Sarajevo, Skopje, Warsaw, Zagreb",
                            "utc_offset" => "+01:00",
                        ],

                ],

            "Romance Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Europe/Brussels",
                            "offset" => "1",
                            "ms" => "Romance Standard Time",
                            "name" => "[UTC+01:00] Brussels, Copenhagen, Madrid, Paris",
                            "utc_offset" => "+01:00",
                        ],

                    "1" =>
                        [
                            "olson" => "Europe/Copenhagen",
                            "offset" => "1",
                            "ms" => "Romance Standard Time",
                            "name" => "[UTC+01:00] Brussels, Copenhagen, Madrid, Paris",
                            "utc_offset" => "+01:00",
                        ],

                    "2" =>
                        [
                            "olson" => "Europe/Madrid",
                            "offset" => "1",
                            "ms" => "Romance Standard Time",
                            "name" => "[UTC+01:00] Brussels, Copenhagen, Madrid, Paris",
                            "utc_offset" => "+01:00",
                        ],

                    "3" =>
                        [
                            "olson" => "Europe/Paris",
                            "offset" => "1",
                            "ms" => "Romance Standard Time",
                            "name" => "[UTC+01:00] Brussels, Copenhagen, Madrid, Paris",
                            "utc_offset" => "+01:00",
                        ],

                ],

            "South Africa Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Africa/Blantyre",
                            "offset" => "2",
                            "ms" => "South Africa Standard Time",
                            "name" => "[UTC+02:00] Harare, Pretoria",
                            "utc_offset" => "+02:00",
                        ],

                    "1" =>
                        [
                            "olson" => "Africa/Bujumbura",
                            "offset" => "2",
                            "ms" => "South Africa Standard Time",
                            "name" => "[UTC+02:00] Harare, Pretoria",
                            "utc_offset" => "+02:00",
                        ],

                    "2" =>
                        [
                            "olson" => "Africa/Gaborone",
                            "offset" => "2",
                            "ms" => "South Africa Standard Time",
                            "name" => "[UTC+02:00] Harare, Pretoria",
                            "utc_offset" => "+02:00",
                        ],

                    "3" =>
                        [
                            "olson" => "Africa/Harare",
                            "offset" => "2",
                            "ms" => "South Africa Standard Time",
                            "name" => "[UTC+02:00] Harare, Pretoria",
                            "utc_offset" => "+02:00",
                        ],

                    "4" =>
                        [
                            "olson" => "Africa/Johannesburg",
                            "offset" => "2",
                            "ms" => "South Africa Standard Time",
                            "name" => "[UTC+02:00] Harare, Pretoria",
                            "utc_offset" => "+02:00",
                        ],

                    "5" =>
                        [
                            "olson" => "Africa/Kigali",
                            "offset" => "2",
                            "ms" => "South Africa Standard Time",
                            "name" => "[UTC+02:00] Harare, Pretoria",
                            "utc_offset" => "+02:00",
                        ],

                    "6" =>
                        [
                            "olson" => "Africa/Lubumbashi",
                            "offset" => "2",
                            "ms" => "South Africa Standard Time",
                            "name" => "[UTC+02:00] Harare, Pretoria",
                            "utc_offset" => "+02:00",
                        ],

                    "7" =>
                        [
                            "olson" => "Africa/Lusaka",
                            "offset" => "2",
                            "ms" => "South Africa Standard Time",
                            "name" => "[UTC+02:00] Harare, Pretoria",
                            "utc_offset" => "+02:00",
                        ],

                    "8" =>
                        [
                            "olson" => "Africa/Maputo",
                            "offset" => "2",
                            "ms" => "South Africa Standard Time",
                            "name" => "[UTC+02:00] Harare, Pretoria",
                            "utc_offset" => "+02:00",
                        ],

                    "9" =>
                        [
                            "olson" => "Africa/Maseru",
                            "offset" => "2",
                            "ms" => "South Africa Standard Time",
                            "name" => "[UTC+02:00] Harare, Pretoria",
                            "utc_offset" => "+02:00",
                        ],

                    "10" =>
                        [
                            "olson" => "Africa/Mbabane",
                            "offset" => "2",
                            "ms" => "South Africa Standard Time",
                            "name" => "[UTC+02:00] Harare, Pretoria",
                            "utc_offset" => "+02:00",
                        ],

                ],

            "Egypt Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Africa/Cairo",
                            "offset" => "2",
                            "ms" => "Egypt Standard Time",
                            "name" => "[UTC+02:00] Cairo",
                            "utc_offset" => "+02:00",
                        ],

                ],

            "Middle East Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Africa/Tripoli",
                            "offset" => "2",
                            "ms" => "Middle East Standard Time",
                            "name" => "[UTC+02:00] Beirut",
                            "utc_offset" => "+02:00",
                        ],

                    "1" =>
                        [
                            "olson" => "Asia/Beirut",
                            "offset" => "2",
                            "ms" => "Middle East Standard Time",
                            "name" => "[UTC+02:00] Beirut",
                            "utc_offset" => "+02:00",
                        ],

                    "2" =>
                        [
                            "olson" => "Asia/Gaza",
                            "offset" => "2",
                            "ms" => "Middle East Standard Time",
                            "name" => "[UTC+02:00] Beirut",
                            "utc_offset" => "+02:00",
                        ],

                ],

            "Jordan Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Asia/Amman",
                            "offset" => "2",
                            "ms" => "Jordan Standard Time",
                            "name" => "[UTC+02:00] Amman",
                            "utc_offset" => "+02:00",
                        ],

                ],

            "Syria Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Asia/Damascus",
                            "offset" => "2",
                            "ms" => "Syria Standard Time",
                            "name" => "[UTC+02:00] Damascus",
                            "utc_offset" => "+02:00",
                        ],

                ],

            "Israel Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Asia/Jerusalem",
                            "offset" => "2",
                            "ms" => "Israel Standard Time",
                            "name" => "[UTC+02:00] Jerusalem",
                            "utc_offset" => "+02:00",
                        ],

                ],

            "GTB Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Asia/Nicosia",
                            "offset" => "2",
                            "ms" => "GTB Standard Time",
                            "name" => "[UTC+02:00] Athens, Bucharest",
                            "utc_offset" => "+02:00",
                        ],

                    "1" =>
                        [
                            "olson" => "Europe/Athens",
                            "offset" => "2",
                            "ms" => "GTB Standard Time",
                            "name" => "[UTC+02:00] Athens, Bucharest",
                            "utc_offset" => "+02:00",
                        ],

                    "2" =>
                        [
                            "olson" => "Europe/Bucharest",
                            "offset" => "2",
                            "ms" => "GTB Standard Time",
                            "name" => "[UTC+02:00] Athens, Bucharest",
                            "utc_offset" => "+02:00",
                        ],

                ],

            "E. Europe Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Europe/Chisinau",
                            "offset" => "2",
                            "ms" => "E. Europe Standard Time",
                            "name" => "[UTC+02:00] Minsk",
                            "utc_offset" => "+02:00",
                        ],

                    "1" =>
                        [
                            "olson" => "Europe/Kiev",
                            "offset" => "2",
                            "ms" => "E. Europe Standard Time",
                            "name" => "[UTC+02:00] Minsk",
                            "utc_offset" => "+02:00",
                        ],

                    "2" =>
                        [
                            "olson" => "Europe/Mariehamn",
                            "offset" => "2",
                            "ms" => "E. Europe Standard Time",
                            "name" => "[UTC+02:00] Minsk",
                            "utc_offset" => "+02:00",
                        ],

                    "3" =>
                        [
                            "olson" => "Europe/Simferopol",
                            "offset" => "2",
                            "ms" => "E. Europe Standard Time",
                            "name" => "[UTC+02:00] Minsk",
                            "utc_offset" => "+02:00",
                        ],

                    "4" =>
                        [
                            "olson" => "Europe/Uzhgorod",
                            "offset" => "2",
                            "ms" => "E. Europe Standard Time",
                            "name" => "[UTC+02:00] Minsk",
                            "utc_offset" => "+02:00",
                        ],

                    "5" =>
                        [
                            "olson" => "Europe/Zaporozhye",
                            "offset" => "2",
                            "ms" => "E. Europe Standard Time",
                            "name" => "[UTC+02:00] Minsk",
                            "utc_offset" => "+02:00",
                        ],

                ],

            "FLE Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Europe/Helsinki",
                            "offset" => "2",
                            "ms" => "FLE Standard Time",
                            "name" => "[UTC+02:00] Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius",
                            "utc_offset" => "+02:00",
                        ],

                    "1" =>
                        [
                            "olson" => "Europe/Riga",
                            "offset" => "2",
                            "ms" => "FLE Standard Time",
                            "name" => "[UTC+02:00] Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius",
                            "utc_offset" => "+02:00",
                        ],

                    "2" =>
                        [
                            "olson" => "Europe/Sofia",
                            "offset" => "2",
                            "ms" => "FLE Standard Time",
                            "name" => "[UTC+02:00] Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius",
                            "utc_offset" => "+02:00",
                        ],

                    "3" =>
                        [
                            "olson" => "Europe/Tallinn",
                            "offset" => "2",
                            "ms" => "FLE Standard Time",
                            "name" => "[UTC+02:00] Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius",
                            "utc_offset" => "+02:00",
                        ],

                    "4" =>
                        [
                            "olson" => "Europe/Vilnius",
                            "offset" => "2",
                            "ms" => "FLE Standard Time",
                            "name" => "[UTC+02:00] Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius",
                            "utc_offset" => "+02:00",
                        ],

                ],

            "Turkey Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Europe/Istanbul",
                            "offset" => "2",
                            "ms" => "Turkey Standard Time",
                            "name" => "[UTC+02:00] Istanbul",
                            "utc_offset" => "+02:00",
                        ],

                ],

            "E. Africa Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Africa/Addis_Ababa",
                            "offset" => "3",
                            "ms" => "E. Africa Standard Time",
                            "name" => "[UTC+03:00] Nairobi",
                            "utc_offset" => "+03:00",
                        ],

                    "1" =>
                        [
                            "olson" => "Africa/Asmara",
                            "offset" => "3",
                            "ms" => "E. Africa Standard Time",
                            "name" => "[UTC+03:00] Nairobi",
                            "utc_offset" => "+03:00",
                        ],

                    "2" =>
                        [
                            "olson" => "Africa/Dar_es_Salaam",
                            "offset" => "3",
                            "ms" => "E. Africa Standard Time",
                            "name" => "[UTC+03:00] Nairobi",
                            "utc_offset" => "+03:00",
                        ],

                    "3" =>
                        [
                            "olson" => "Africa/Djibouti",
                            "offset" => "3",
                            "ms" => "E. Africa Standard Time",
                            "name" => "[UTC+03:00] Nairobi",
                            "utc_offset" => "+03:00",
                        ],

                    "4" =>
                        [
                            "olson" => "Africa/Juba",
                            "offset" => "3",
                            "ms" => "E. Africa Standard Time",
                            "name" => "[UTC+03:00] Nairobi",
                            "utc_offset" => "+03:00",
                        ],

                    "5" =>
                        [
                            "olson" => "Africa/Kampala",
                            "offset" => "3",
                            "ms" => "E. Africa Standard Time",
                            "name" => "[UTC+03:00] Nairobi",
                            "utc_offset" => "+03:00",
                        ],

                    "6" =>
                        [
                            "olson" => "Africa/Khartoum",
                            "offset" => "3",
                            "ms" => "E. Africa Standard Time",
                            "name" => "[UTC+03:00] Nairobi",
                            "utc_offset" => "+03:00",
                        ],

                    "7" =>
                        [
                            "olson" => "Africa/Mogadishu",
                            "offset" => "3",
                            "ms" => "E. Africa Standard Time",
                            "name" => "[UTC+03:00] Nairobi",
                            "utc_offset" => "+03:00",
                        ],

                    "8" =>
                        [
                            "olson" => "Africa/Nairobi",
                            "offset" => "3",
                            "ms" => "E. Africa Standard Time",
                            "name" => "[UTC+03:00] Nairobi",
                            "utc_offset" => "+03:00",
                        ],

                ],

            "Kaliningrad Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Antarctica/Syowa",
                            "offset" => "3",
                            "ms" => "Kaliningrad Standard Time",
                            "name" => "[UTC+03:00] Kaliningrad",
                            "utc_offset" => "+03:00",
                        ],

                    "1" =>
                        [
                            "olson" => "Asia/Aden",
                            "offset" => "3",
                            "ms" => "Kaliningrad Standard Time",
                            "name" => "[UTC+03:00] Kaliningrad",
                            "utc_offset" => "+03:00",
                        ],

                    "2" =>
                        [
                            "olson" => "Europe/Kaliningrad",
                            "offset" => "3",
                            "ms" => "Kaliningrad Standard Time",
                            "name" => "[UTC+03:00] Kaliningrad",
                            "utc_offset" => "+03:00",
                        ],

                    "3" =>
                        [
                            "olson" => "Europe/Minsk",
                            "offset" => "3",
                            "ms" => "Kaliningrad Standard Time",
                            "name" => "[UTC+03:00] Kaliningrad",
                            "utc_offset" => "+03:00",
                        ],

                ],

            "Arabic Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Asia/Baghdad",
                            "offset" => "3",
                            "ms" => "Arabic Standard Time",
                            "name" => "[UTC+03:00] Baghdad",
                            "utc_offset" => "+03:00",
                        ],

                    "1" =>
                        [
                            "olson" => "Asia/Bahrain",
                            "offset" => "3",
                            "ms" => "Arabic Standard Time",
                            "name" => "[UTC+03:00] Baghdad",
                            "utc_offset" => "+03:00",
                        ],

                ],

            "Arab Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Asia/Kuwait",
                            "offset" => "3",
                            "ms" => "Arab Standard Time",
                            "name" => "[UTC+03:00] Kuwait, Riyadh",
                            "utc_offset" => "+03:00",
                        ],

                    "1" =>
                        [
                            "olson" => "Asia/Qatar",
                            "offset" => "3",
                            "ms" => "Arab Standard Time",
                            "name" => "[UTC+03:00] Kuwait, Riyadh",
                            "utc_offset" => "+03:00",
                        ],

                    "2" =>
                        [
                            "olson" => "Asia/Riyadh",
                            "offset" => "3",
                            "ms" => "Arab Standard Time",
                            "name" => "[UTC+03:00] Kuwait, Riyadh",
                            "utc_offset" => "+03:00",
                        ],

                    "3" =>
                        [
                            "olson" => "Indian/Antananarivo",
                            "offset" => "3",
                            "ms" => "Arab Standard Time",
                            "name" => "[UTC+03:00] Kuwait, Riyadh",
                            "utc_offset" => "+03:00",
                        ],

                    "4" =>
                        [
                            "olson" => "Indian/Comoro",
                            "offset" => "3",
                            "ms" => "Arab Standard Time",
                            "name" => "[UTC+03:00] Kuwait, Riyadh",
                            "utc_offset" => "+03:00",
                        ],

                    "5" =>
                        [
                            "olson" => "Indian/Mayotte",
                            "offset" => "3",
                            "ms" => "Arab Standard Time",
                            "name" => "[UTC+03:00] Kuwait, Riyadh",
                            "utc_offset" => "+03:00",
                        ],

                ],

            "Iran Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Asia/Tehran",
                            "offset" => "3.5",
                            "ms" => "Iran Standard Time",
                            "name" => "[UTC+03:30] Tehran",
                            "utc_offset" => "+03:30",
                        ],

                ],

            "Caucasus Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Asia/Baku",
                            "offset" => "4",
                            "ms" => "Caucasus Standard Time",
                            "name" => "[UTC+04:00] Yerevan",
                            "utc_offset" => "+04:00",
                        ],

                    "1" =>
                        [
                            "olson" => "Asia/Yerevan",
                            "offset" => "4",
                            "ms" => "Caucasus Standard Time",
                            "name" => "[UTC+04:00] Yerevan",
                            "utc_offset" => "+04:00",
                        ],

                ],

            "Arabian Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Asia/Dubai",
                            "offset" => "4",
                            "ms" => "Arabian Standard Time",
                            "name" => "[UTC+04:00] Abu Dhabi, Muscat",
                            "utc_offset" => "+04:00",
                        ],

                    "1" =>
                        [
                            "olson" => "Asia/Muscat",
                            "offset" => "4",
                            "ms" => "Arabian Standard Time",
                            "name" => "[UTC+04:00] Abu Dhabi, Muscat",
                            "utc_offset" => "+04:00",
                        ],

                ],

            "Georgian Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Asia/Tbilisi",
                            "offset" => "4",
                            "ms" => "Georgian Standard Time",
                            "name" => "[UTC+04:00] Tbilisi",
                            "utc_offset" => "+04:00",
                        ],

                ],

            "Russian Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Europe/Moscow",
                            "offset" => "4",
                            "ms" => "Russian Standard Time",
                            "name" => "[UTC+04:00] Moscow, St. Petersburg, Volgograd",
                            "utc_offset" => "+04:00",
                        ],

                    "1" =>
                        [
                            "olson" => "Europe/Samara",
                            "offset" => "4",
                            "ms" => "Russian Standard Time",
                            "name" => "[UTC+04:00] Moscow, St. Petersburg, Volgograd",
                            "utc_offset" => "+04:00",
                        ],

                    "2" =>
                        [
                            "olson" => "Europe/Volgograd",
                            "offset" => "4",
                            "ms" => "Russian Standard Time",
                            "name" => "[UTC+04:00] Moscow, St. Petersburg, Volgograd",
                            "utc_offset" => "+04:00",
                        ],

                ],

            "Mauritius Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Indian/Mahe",
                            "offset" => "4",
                            "ms" => "Mauritius Standard Time",
                            "name" => "[UTC+04:00] Port Louis",
                            "utc_offset" => "+04:00",
                        ],

                    "1" =>
                        [
                            "olson" => "Indian/Mauritius",
                            "offset" => "4",
                            "ms" => "Mauritius Standard Time",
                            "name" => "[UTC+04:00] Port Louis",
                            "utc_offset" => "+04:00",
                        ],

                    "2" =>
                        [
                            "olson" => "Indian/Reunion",
                            "offset" => "4",
                            "ms" => "Mauritius Standard Time",
                            "name" => "[UTC+04:00] Port Louis",
                            "utc_offset" => "+04:00",
                        ],

                ],

            "Afghanistan Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Asia/Kabul",
                            "offset" => "4.5",
                            "ms" => "Afghanistan Standard Time",
                            "name" => "[UTC+04:30] Kabul",
                            "utc_offset" => "+04:30",
                        ],

                ],

            "West Asia Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Asia/Aqtau",
                            "offset" => "5",
                            "ms" => "West Asia Standard Time",
                            "name" => "[UTC+05:00] Tashkent",
                            "utc_offset" => "+05:00",
                        ],

                    "1" =>
                        [
                            "olson" => "Asia/Aqtobe",
                            "offset" => "5",
                            "ms" => "West Asia Standard Time",
                            "name" => "[UTC+05:00] Tashkent",
                            "utc_offset" => "+05:00",
                        ],

                    "2" =>
                        [
                            "olson" => "Asia/Ashgabat",
                            "offset" => "5",
                            "ms" => "West Asia Standard Time",
                            "name" => "[UTC+05:00] Tashkent",
                            "utc_offset" => "+05:00",
                        ],

                    "3" =>
                        [
                            "olson" => "Asia/Dushanbe",
                            "offset" => "5",
                            "ms" => "West Asia Standard Time",
                            "name" => "[UTC+05:00] Tashkent",
                            "utc_offset" => "+05:00",
                        ],

                    "4" =>
                        [
                            "olson" => "Asia/Oral",
                            "offset" => "5",
                            "ms" => "West Asia Standard Time",
                            "name" => "[UTC+05:00] Tashkent",
                            "utc_offset" => "+05:00",
                        ],

                    "5" =>
                        [
                            "olson" => "Asia/Samarkand",
                            "offset" => "5",
                            "ms" => "West Asia Standard Time",
                            "name" => "[UTC+05:00] Tashkent",
                            "utc_offset" => "+05:00",
                        ],

                    "6" =>
                        [
                            "olson" => "Asia/Tashkent",
                            "offset" => "5",
                            "ms" => "West Asia Standard Time",
                            "name" => "[UTC+05:00] Tashkent",
                            "utc_offset" => "+05:00",
                        ],

                    "7" =>
                        [
                            "olson" => "Indian/Kerguelen",
                            "offset" => "5",
                            "ms" => "West Asia Standard Time",
                            "name" => "[UTC+05:00] Tashkent",
                            "utc_offset" => "+05:00",
                        ],

                    "8" =>
                        [
                            "olson" => "Indian/Maldives",
                            "offset" => "5",
                            "ms" => "West Asia Standard Time",
                            "name" => "[UTC+05:00] Tashkent",
                            "utc_offset" => "+05:00",
                        ],

                ],

            "Pakistan Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Asia/Karachi",
                            "offset" => "5",
                            "ms" => "Pakistan Standard Time",
                            "name" => "[UTC+05:00] Islamabad, Karachi",
                            "utc_offset" => "+05:00",
                        ],

                ],

            "Sri Lanka Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Asia/Colombo",
                            "offset" => "5.5",
                            "ms" => "Sri Lanka Standard Time",
                            "name" => "[UTC+05:30] Sri Jayawardenepura",
                            "utc_offset" => "+05:30",
                        ],

                ],

            "India Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Asia/Kolkata",
                            "offset" => "5.5",
                            "ms" => "India Standard Time",
                            "name" => "[UTC+05:30] Chennai, Kolkata, Mumbai, New Delhi",
                            "utc_offset" => "+05:30",
                        ],

                ],

            "Nepal Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Asia/Kathmandu",
                            "offset" => "5.75",
                            "ms" => "Nepal Standard Time",
                            "name" => "[UTC+05:45] Kathmandu",
                            "utc_offset" => "+05:45",
                        ],

                ],

            "Central Asia Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Antarctica/Mawson",
                            "offset" => "6",
                            "ms" => "Central Asia Standard Time",
                            "name" => "[UTC+06:00] Astana",
                            "utc_offset" => "+06:00",
                        ],

                    "1" =>
                        [
                            "olson" => "Asia/Almaty",
                            "offset" => "6",
                            "ms" => "Central Asia Standard Time",
                            "name" => "[UTC+06:00] Astana",
                            "utc_offset" => "+06:00",
                        ],

                    "2" =>
                        [
                            "olson" => "Asia/Bishkek",
                            "offset" => "6",
                            "ms" => "Central Asia Standard Time",
                            "name" => "[UTC+06:00] Astana",
                            "utc_offset" => "+06:00",
                        ],

                    "3" =>
                        [
                            "olson" => "Asia/Qyzylorda",
                            "offset" => "6",
                            "ms" => "Central Asia Standard Time",
                            "name" => "[UTC+06:00] Astana",
                            "utc_offset" => "+06:00",
                        ],

                    "4" =>
                        [
                            "olson" => "Asia/Thimphu",
                            "offset" => "6",
                            "ms" => "Central Asia Standard Time",
                            "name" => "[UTC+06:00] Astana",
                            "utc_offset" => "+06:00",
                        ],

                    "5" =>
                        [
                            "olson" => "Indian/Chagos",
                            "offset" => "6",
                            "ms" => "Central Asia Standard Time",
                            "name" => "[UTC+06:00] Astana",
                            "utc_offset" => "+06:00",
                        ],

                ],

            "Bangladesh Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Asia/Dhaka",
                            "offset" => "6",
                            "ms" => "Bangladesh Standard Time",
                            "name" => "[UTC+06:00] Dhaka",
                            "utc_offset" => "+06:00",
                        ],

                ],

            "Ekaterinburg Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Asia/Yekaterinburg",
                            "offset" => "6",
                            "ms" => "Ekaterinburg Standard Time",
                            "name" => "[UTC+06:00] Ekaterinburg",
                            "utc_offset" => "+06:00",
                        ],

                ],

            "Myanmar Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Asia/Rangoon",
                            "offset" => "6.5",
                            "ms" => "Myanmar Standard Time",
                            "name" => "[UTC+06:30] Yangon [Rangoon]",
                            "utc_offset" => "+06:30",
                        ],

                    "1" =>
                        [
                            "olson" => "Indian/Cocos",
                            "offset" => "6.5",
                            "ms" => "Myanmar Standard Time",
                            "name" => "[UTC+06:30] Yangon [Rangoon]",
                            "utc_offset" => "+06:30",
                        ],

                ],

            "SE Asia Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Antarctica/Davis",
                            "offset" => "7",
                            "ms" => "SE Asia Standard Time",
                            "name" => "[UTC+07:00] Bangkok, Hanoi, Jakarta",
                            "utc_offset" => "+07:00",
                        ],

                    "1" =>
                        [
                            "olson" => "Asia/Bangkok",
                            "offset" => "7",
                            "ms" => "SE Asia Standard Time",
                            "name" => "[UTC+07:00] Bangkok, Hanoi, Jakarta",
                            "utc_offset" => "+07:00",
                        ],

                    "2" =>
                        [
                            "olson" => "Asia/Ho_Chi_Minh",
                            "offset" => "7",
                            "ms" => "SE Asia Standard Time",
                            "name" => "[UTC+07:00] Bangkok, Hanoi, Jakarta",
                            "utc_offset" => "+07:00",
                        ],

                    "3" =>
                        [
                            "olson" => "Asia/Jakarta",
                            "offset" => "7",
                            "ms" => "SE Asia Standard Time",
                            "name" => "[UTC+07:00] Bangkok, Hanoi, Jakarta",
                            "utc_offset" => "+07:00",
                        ],

                    "4" =>
                        [
                            "olson" => "Asia/Phnom_Penh",
                            "offset" => "7",
                            "ms" => "SE Asia Standard Time",
                            "name" => "[UTC+07:00] Bangkok, Hanoi, Jakarta",
                            "utc_offset" => "+07:00",
                        ],

                    "5" =>
                        [
                            "olson" => "Asia/Pontianak",
                            "offset" => "7",
                            "ms" => "SE Asia Standard Time",
                            "name" => "[UTC+07:00] Bangkok, Hanoi, Jakarta",
                            "utc_offset" => "+07:00",
                        ],

                    "6" =>
                        [
                            "olson" => "Asia/Vientiane",
                            "offset" => "7",
                            "ms" => "SE Asia Standard Time",
                            "name" => "[UTC+07:00] Bangkok, Hanoi, Jakarta",
                            "utc_offset" => "+07:00",
                        ],

                    "7" =>
                        [
                            "olson" => "Indian/Christmas",
                            "offset" => "7",
                            "ms" => "SE Asia Standard Time",
                            "name" => "[UTC+07:00] Bangkok, Hanoi, Jakarta",
                            "utc_offset" => "+07:00",
                        ],

                ],

            "N. Central Asia Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Asia/Hovd",
                            "offset" => "7",
                            "ms" => "N. Central Asia Standard Time",
                            "name" => "[UTC+07:00] Novosibirsk",
                            "utc_offset" => "+07:00",
                        ],

                    "1" =>
                        [
                            "olson" => "Asia/Novokuznetsk",
                            "offset" => "7",
                            "ms" => "N. Central Asia Standard Time",
                            "name" => "[UTC+07:00] Novosibirsk",
                            "utc_offset" => "+07:00",
                        ],

                    "2" =>
                        [
                            "olson" => "Asia/Novosibirsk",
                            "offset" => "7",
                            "ms" => "N. Central Asia Standard Time",
                            "name" => "[UTC+07:00] Novosibirsk",
                            "utc_offset" => "+07:00",
                        ],

                    "3" =>
                        [
                            "olson" => "Asia/Omsk",
                            "offset" => "7",
                            "ms" => "N. Central Asia Standard Time",
                            "name" => "[UTC+07:00] Novosibirsk",
                            "utc_offset" => "+07:00",
                        ],

                ],

            "W. Australia Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Antarctica/Casey",
                            "offset" => "8",
                            "ms" => "W. Australia Standard Time",
                            "name" => "[UTC+08:00] Perth",
                            "utc_offset" => "+08:00",
                        ],

                    "1" =>
                        [
                            "olson" => "Australia/Perth",
                            "offset" => "8",
                            "ms" => "W. Australia Standard Time",
                            "name" => "[UTC+08:00] Perth",
                            "utc_offset" => "+08:00",
                        ],

                    "2" =>
                        [
                            "olson" => "Australia/Eucla",
                            "offset" => "8.75",
                            "ms" => "W. Australia Standard Time",
                            "name" => "[UTC+08:00] Perth",
                            "utc_offset" => "+08:00",
                        ],

                ],

            "North Asia Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Asia/Brunei",
                            "offset" => "8",
                            "ms" => "North Asia Standard Time",
                            "name" => "[UTC+08:00] Krasnoyarsk",
                            "utc_offset" => "+08:00",
                        ],

                    "1" =>
                        [
                            "olson" => "Asia/Kashgar",
                            "offset" => "8",
                            "ms" => "North Asia Standard Time",
                            "name" => "[UTC+08:00] Krasnoyarsk",
                            "utc_offset" => "+08:00",
                        ],

                    "2" =>
                        [
                            "olson" => "Asia/Krasnoyarsk",
                            "offset" => "8",
                            "ms" => "North Asia Standard Time",
                            "name" => "[UTC+08:00] Krasnoyarsk",
                            "utc_offset" => "+08:00",
                        ],

                    "3" =>
                        [
                            "olson" => "Asia/Kuching",
                            "offset" => "8",
                            "ms" => "North Asia Standard Time",
                            "name" => "[UTC+08:00] Krasnoyarsk",
                            "utc_offset" => "+08:00",
                        ],

                    "4" =>
                        [
                            "olson" => "Asia/Macau",
                            "offset" => "8",
                            "ms" => "North Asia Standard Time",
                            "name" => "[UTC+08:00] Krasnoyarsk",
                            "utc_offset" => "+08:00",
                        ],

                    "5" =>
                        [
                            "olson" => "Asia/Makassar",
                            "offset" => "8",
                            "ms" => "North Asia Standard Time",
                            "name" => "[UTC+08:00] Krasnoyarsk",
                            "utc_offset" => "+08:00",
                        ],

                    "6" =>
                        [
                            "olson" => "Asia/Manila",
                            "offset" => "8",
                            "ms" => "North Asia Standard Time",
                            "name" => "[UTC+08:00] Krasnoyarsk",
                            "utc_offset" => "+08:00",
                        ],

                ],

            "China Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Asia/Choibalsan",
                            "offset" => "8",
                            "ms" => "China Standard Time",
                            "name" => "[UTC+08:00] Beijing, Chongqing, Hong Kong, Urumqi",
                            "utc_offset" => "+08:00",
                        ],

                    "1" =>
                        [
                            "olson" => "Asia/Chongqing",
                            "offset" => "8",
                            "ms" => "China Standard Time",
                            "name" => "[UTC+08:00] Beijing, Chongqing, Hong Kong, Urumqi",
                            "utc_offset" => "+08:00",
                        ],

                    "2" =>
                        [
                            "olson" => "Asia/Harbin",
                            "offset" => "8",
                            "ms" => "China Standard Time",
                            "name" => "[UTC+08:00] Beijing, Chongqing, Hong Kong, Urumqi",
                            "utc_offset" => "+08:00",
                        ],

                    "3" =>
                        [
                            "olson" => "Asia/Hong_Kong",
                            "offset" => "8",
                            "ms" => "China Standard Time",
                            "name" => "[UTC+08:00] Beijing, Chongqing, Hong Kong, Urumqi",
                            "utc_offset" => "+08:00",
                        ],

                    "4" =>
                        [
                            "olson" => "Asia/Shanghai",
                            "offset" => "8",
                            "ms" => "China Standard Time",
                            "name" => "[UTC+08:00] Beijing, Chongqing, Hong Kong, Urumqi",
                            "utc_offset" => "+08:00",
                        ],

                    "5" =>
                        [
                            "olson" => "Asia/Urumqi",
                            "offset" => "8",
                            "ms" => "China Standard Time",
                            "name" => "[UTC+08:00] Beijing, Chongqing, Hong Kong, Urumqi",
                            "utc_offset" => "+08:00",
                        ],

                ],

            "Singapore Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Asia/Kuala_Lumpur",
                            "offset" => "8",
                            "ms" => "Singapore Standard Time",
                            "name" => "[UTC+08:00] Kuala Lumpur, Singapore",
                            "utc_offset" => "+08:00",
                        ],

                    "1" =>
                        [
                            "olson" => "Asia/Singapore",
                            "offset" => "8",
                            "ms" => "Singapore Standard Time",
                            "name" => "[UTC+08:00] Kuala Lumpur, Singapore",
                            "utc_offset" => "+08:00",
                        ],

                ],

            "Taipei Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Asia/Taipei",
                            "offset" => "8",
                            "ms" => "Taipei Standard Time",
                            "name" => "[UTC+08:00] Taipei",
                            "utc_offset" => "+08:00",
                        ],

                ],

            "Ulaanbaatar Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Asia/Ulaanbaatar",
                            "offset" => "8",
                            "ms" => "Ulaanbaatar Standard Time",
                            "name" => "[UTC+08:00] Ulaanbaatar",
                            "utc_offset" => "+08:00",
                        ],

                ],

            "North Asia East Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Asia/Dili",
                            "offset" => "9",
                            "ms" => "North Asia East Standard Time",
                            "name" => "[UTC+09:00] Irkutsk",
                            "utc_offset" => "+09:00",
                        ],

                    "1" =>
                        [
                            "olson" => "Asia/Irkutsk",
                            "offset" => "9",
                            "ms" => "North Asia East Standard Time",
                            "name" => "[UTC+09:00] Irkutsk",
                            "utc_offset" => "+09:00",
                        ],

                    "2" =>
                        [
                            "olson" => "Asia/Jayapura",
                            "offset" => "9",
                            "ms" => "North Asia East Standard Time",
                            "name" => "[UTC+09:00] Irkutsk",
                            "utc_offset" => "+09:00",
                        ],

                    "3" =>
                        [
                            "olson" => "Pacific/Palau",
                            "offset" => "9",
                            "ms" => "North Asia East Standard Time",
                            "name" => "[UTC+09:00] Irkutsk",
                            "utc_offset" => "+09:00",
                        ],

                ],

            "Korea Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Asia/Pyongyang",
                            "offset" => "9",
                            "ms" => "Korea Standard Time",
                            "name" => "[UTC+09:00] Seoul",
                            "utc_offset" => "+09:00",
                        ],

                    "1" =>
                        [
                            "olson" => "Asia/Seoul",
                            "offset" => "9",
                            "ms" => "Korea Standard Time",
                            "name" => "[UTC+09:00] Seoul",
                            "utc_offset" => "+09:00",
                        ],

                ],

            "Tokyo Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Asia/Tokyo",
                            "offset" => "9",
                            "ms" => "Tokyo Standard Time",
                            "name" => "[UTC+09:00] Osaka, Sapporo, Tokyo",
                            "utc_offset" => "+09:00",
                        ],

                ],

            "Cen. Australia Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Australia/Adelaide",
                            "offset" => "9.5",
                            "ms" => "Cen. Australia Standard Time",
                            "name" => "[UTC+09:30] Adelaide",
                            "utc_offset" => "+09:30",
                        ],

                ],

            "AUS Central Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Australia/Broken_Hill",
                            "offset" => "9.5",
                            "ms" => "AUS Central Standard Time",
                            "name" => "[UTC+09:30] Darwin",
                            "utc_offset" => "+09:30",
                        ],

                    "1" =>
                        [
                            "olson" => "Australia/Darwin",
                            "offset" => "9.5",
                            "ms" => "AUS Central Standard Time",
                            "name" => "[UTC+09:30] Darwin",
                            "utc_offset" => "+09:30",
                        ],

                ],

            "E. Australia Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Antarctica/DumontDUrville",
                            "offset" => "10",
                            "ms" => "E. Australia Standard Time",
                            "name" => "[UTC+10:00] Brisbane",
                            "utc_offset" => "+10:00",
                        ],

                    "1" =>
                        [
                            "olson" => "Australia/Brisbane",
                            "offset" => "10",
                            "ms" => "E. Australia Standard Time",
                            "name" => "[UTC+10:00] Brisbane",
                            "utc_offset" => "+10:00",
                        ],

                    "2" =>
                        [
                            "olson" => "Australia/Lindeman",
                            "offset" => "10",
                            "ms" => "E. Australia Standard Time",
                            "name" => "[UTC+10:00] Brisbane",
                            "utc_offset" => "+10:00",
                        ],

                    "3" =>
                        [
                            "olson" => "Australia/Lord_Howe",
                            "offset" => "10.5",
                            "ms" => "E. Australia Standard Time",
                            "name" => "[UTC+10:00] Brisbane",
                            "utc_offset" => "+10:00",
                        ],

                ],

            "Yakutsk Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Asia/Yakutsk",
                            "offset" => "10",
                            "ms" => "Yakutsk Standard Time",
                            "name" => "[UTC+10:00] Yakutsk",
                            "utc_offset" => "+10:00",
                        ],

                ],

            "Tasmania Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Australia/Currie",
                            "offset" => "10",
                            "ms" => "Tasmania Standard Time",
                            "name" => "[UTC+10:00] Hobart",
                            "utc_offset" => "+10:00",
                        ],

                    "1" =>
                        [
                            "olson" => "Australia/Hobart",
                            "offset" => "10",
                            "ms" => "Tasmania Standard Time",
                            "name" => "[UTC+10:00] Hobart",
                            "utc_offset" => "+10:00",
                        ],

                ],

            "AUS Eastern Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Australia/Melbourne",
                            "offset" => "10",
                            "ms" => "AUS Eastern Standard Time",
                            "name" => "[UTC+10:00] Canberra, Melbourne, Sydney",
                            "utc_offset" => "+10:00",
                        ],

                    "1" =>
                        [
                            "olson" => "Australia/Sydney",
                            "offset" => "10",
                            "ms" => "AUS Eastern Standard Time",
                            "name" => "[UTC+10:00] Canberra, Melbourne, Sydney",
                            "utc_offset" => "+10:00",
                        ],

                ],

            "West Pacific Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Pacific/Guam",
                            "offset" => "10",
                            "ms" => "West Pacific Standard Time",
                            "name" => "[UTC+10:00] Guam, Port Moresby",
                            "utc_offset" => "+10:00",
                        ],

                    "1" =>
                        [
                            "olson" => "Pacific/Port_Moresby",
                            "offset" => "10",
                            "ms" => "West Pacific Standard Time",
                            "name" => "[UTC+10:00] Guam, Port Moresby",
                            "utc_offset" => "+10:00",
                        ],

                    "2" =>
                        [
                            "olson" => "Pacific/Saipan",
                            "offset" => "10",
                            "ms" => "West Pacific Standard Time",
                            "name" => "[UTC+10:00] Guam, Port Moresby",
                            "utc_offset" => "+10:00",
                        ],

                    "3" =>
                        [
                            "olson" => "Pacific/Truk",
                            "offset" => "10",
                            "ms" => "West Pacific Standard Time",
                            "name" => "[UTC+10:00] Guam, Port Moresby",
                            "utc_offset" => "+10:00",
                        ],

                ],

            "Vladivostok Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Asia/Sakhalin",
                            "offset" => "11",
                            "ms" => "Vladivostok Standard Time",
                            "name" => "[UTC+11:00] Vladivostok",
                            "utc_offset" => "+11:00",
                        ],

                    "1" =>
                        [
                            "olson" => "Asia/Vladivostok",
                            "offset" => "11",
                            "ms" => "Vladivostok Standard Time",
                            "name" => "[UTC+11:00] Vladivostok",
                            "utc_offset" => "+11:00",
                        ],

                ],

            "Central Pacific Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Pacific/Efate",
                            "offset" => "11",
                            "ms" => "Central Pacific Standard Time",
                            "name" => "[UTC+11:00] Solomon Is., New Caledonia",
                            "utc_offset" => "+11:00",
                        ],

                    "1" =>
                        [
                            "olson" => "Pacific/Guadalcanal",
                            "offset" => "11",
                            "ms" => "Central Pacific Standard Time",
                            "name" => "[UTC+11:00] Solomon Is., New Caledonia",
                            "utc_offset" => "+11:00",
                        ],

                    "2" =>
                        [
                            "olson" => "Pacific/Kosrae",
                            "offset" => "11",
                            "ms" => "Central Pacific Standard Time",
                            "name" => "[UTC+11:00] Solomon Is., New Caledonia",
                            "utc_offset" => "+11:00",
                        ],

                    "3" =>
                        [
                            "olson" => "Pacific/Noumea",
                            "offset" => "11",
                            "ms" => "Central Pacific Standard Time",
                            "name" => "[UTC+11:00] Solomon Is., New Caledonia",
                            "utc_offset" => "+11:00",
                        ],

                    "4" =>
                        [
                            "olson" => "Pacific/Ponape",
                            "offset" => "11",
                            "ms" => "Central Pacific Standard Time",
                            "name" => "[UTC+11:00] Solomon Is., New Caledonia",
                            "utc_offset" => "+11:00",
                        ],

                    "5" =>
                        [
                            "olson" => "Pacific/Norfolk",
                            "offset" => "11.5",
                            "ms" => "Central Pacific Standard Time",
                            "name" => "[UTC+11:00] Solomon Is., New Caledonia",
                            "utc_offset" => "+11:00",
                        ],

                ],

            "UTC+12" =>
                [
                    "0" =>
                        [
                            "olson" => "Antarctica/McMurdo",
                            "offset" => "12",
                            "ms" => "UTC+12",
                            "name" => "[UTC+12:00] Coordinated Universal Time+12",
                            "utc_offset" => "+12:00",
                        ],

                    "1" =>
                        [
                            "olson" => "Antarctica/South_Pole",
                            "offset" => "12",
                            "ms" => "UTC+12",
                            "name" => "[UTC+12:00] Coordinated Universal Time+12",
                            "utc_offset" => "+12:00",
                        ],

                ],

            "Magadan Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Asia/Anadyr",
                            "offset" => "12",
                            "ms" => "Magadan Standard Time",
                            "name" => "[UTC+12:00] Magadan",
                            "utc_offset" => "+12:00",
                        ],

                    "1" =>
                        [
                            "olson" => "Asia/Kamchatka",
                            "offset" => "12",
                            "ms" => "Magadan Standard Time",
                            "name" => "[UTC+12:00] Magadan",
                            "utc_offset" => "+12:00",
                        ],

                    "2" =>
                        [
                            "olson" => "Asia/Magadan",
                            "offset" => "12",
                            "ms" => "Magadan Standard Time",
                            "name" => "[UTC+12:00] Magadan",
                            "utc_offset" => "+12:00",
                        ],

                ],

            "New Zealand Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Pacific/Auckland",
                            "offset" => "12",
                            "ms" => "New Zealand Standard Time",
                            "name" => "[UTC+12:00] Auckland, Wellington",
                            "utc_offset" => "+12:00",
                        ],

                ],

            "Fiji Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Pacific/Fiji",
                            "offset" => "12",
                            "ms" => "Fiji Standard Time",
                            "name" => "[UTC+12:00] Fiji",
                            "utc_offset" => "+12:00",
                        ],

                    "1" =>
                        [
                            "olson" => "Pacific/Funafuti",
                            "offset" => "12",
                            "ms" => "Fiji Standard Time",
                            "name" => "[UTC+12:00] Fiji",
                            "utc_offset" => "+12:00",
                        ],

                    "2" =>
                        [
                            "olson" => "Pacific/Kwajalein",
                            "offset" => "12",
                            "ms" => "Fiji Standard Time",
                            "name" => "[UTC+12:00] Fiji",
                            "utc_offset" => "+12:00",
                        ],

                    "3" =>
                        [
                            "olson" => "Pacific/Majuro",
                            "offset" => "12",
                            "ms" => "Fiji Standard Time",
                            "name" => "[UTC+12:00] Fiji",
                            "utc_offset" => "+12:00",
                        ],

                    "4" =>
                        [
                            "olson" => "Pacific/Nauru",
                            "offset" => "12",
                            "ms" => "Fiji Standard Time",
                            "name" => "[UTC+12:00] Fiji",
                            "utc_offset" => "+12:00",
                        ],

                    "5" =>
                        [
                            "olson" => "Pacific/Tarawa",
                            "offset" => "12",
                            "ms" => "Fiji Standard Time",
                            "name" => "[UTC+12:00] Fiji",
                            "utc_offset" => "+12:00",
                        ],

                    "6" =>
                        [
                            "olson" => "Pacific/Wake",
                            "offset" => "12",
                            "ms" => "Fiji Standard Time",
                            "name" => "[UTC+12:00] Fiji",
                            "utc_offset" => "+12:00",
                        ],

                    "7" =>
                        [
                            "olson" => "Pacific/Wallis",
                            "offset" => "12",
                            "ms" => "Fiji Standard Time",
                            "name" => "[UTC+12:00] Fiji",
                            "utc_offset" => "+12:00",
                        ],

                ],

            "Tonga Standard Time" =>
                [
                    "0" =>
                        [
                            "olson" => "Pacific/Chatham",
                            "offset" => "12.75",
                            "ms" => "Tonga Standard Time",
                            "name" => "[UTC+13:00] Nuku'alofa",
                            "utc_offset" => "+13:00",
                        ],

                    "1" =>
                        [
                            "olson" => "Pacific/Enderbury",
                            "offset" => "13",
                            "ms" => "Tonga Standard Time",
                            "name" => "[UTC+13:00] Nuku'alofa",
                            "utc_offset" => "+13:00",
                        ],

                    "2" =>
                        [
                            "olson" => "Pacific/Tongatapu",
                            "offset" => "13",
                            "ms" => "Tonga Standard Time",
                            "name" => "[UTC+13:00] Nuku'alofa",
                            "utc_offset" => "+13:00",
                        ],

                    "3" =>
                        [
                            "olson" => "Pacific/Kiritimati",
                            "offset" => "14",
                            "ms" => "Tonga Standard Time",
                            "name" => "[UTC+13:00] Nuku'alofa",
                            "utc_offset" => "+13:00",
                        ],

                ],

        ];

    private static function getValue($msName, $value) {
        $result = false;
        if(isset(self::$timezones[$msName]) && isset(self::$timezones[$msName][0][$value])) {
            $result = self::$timezones[$msName][0][$value];
        }
        return $result;
    }

    public static function getOffset($msName)
    {
        return self::getValue($msName, 'utc_offset');
    }

    public static function getOlson($msName)
    {
        return self::getValue($msName, 'olson');
    }

    public static function getName($msName)
    {
        return self::getValue($msName, 'name');
    }

    public static function getMsName($msName)
    {
        return self::getValue($msName, 'ms');
    }

}
