<?php
namespace BlueSky\Framework\Util;

/**
 * URL manipulation utilities.
 *  */
class Url
{

    public static function exists($url)
    {
        $result = false;
        if ($fp = curl_init($url)) {
            $result = true;
        }
        return $result;
    }

    public static function isAbsolute($url)
    {
        $url = trim($url);
        $passesFilterVar = filter_var($url, FILTER_VALIDATE_URL);
        $parts = parse_url($url);
        $parsesWithParseUrl = isset($parts['scheme']) || isset($parts['host']);
        $startsWithHttp = strpos($url, 'http://') === 0 || strpos($url, "https://") === 0;
        return $passesFilterVar || $parsesWithParseUrl || $startsWithHttp;
    }

    /**
     * Add a scheme to a URL if one does not exist
     * @access public static
     * @param string $url
     * @return string
     * */
    public static function addScheme($url)
    {
        if (is_string($url) && $url != '') {
            if ($parts = parse_url($url)) {
                if (!isset($parts["scheme"])) {
                    $url = "http://$url";
                }
            }
        }
        return $url;
    }

}
