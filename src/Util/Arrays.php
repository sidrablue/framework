<?php
namespace BlueSky\Framework\Util;

/**
 * Class for Array manipulation utilities.
 */
class Arrays
{
    /**
     * Ordering Array
     * @access public
     * @param array $array
     * @param String $fieldName
     * @return array $ret
     */
    public static function order2dArray(&$array, $fieldName)
    {
        $sorter = [];
        $ret = [];
        reset($array);
        foreach ($array as $ii => $va) {
            $sorter[$ii] = $va[$fieldName];
        }
        asort($sorter);
        foreach ($sorter as $ii => $va) {
            $ret[$ii] = $array[$ii];
        }
        $array = $ret;
    }

    /**
     * Trim the elements of a 1 dimensional array
     * @access public static
     * @param array $array - a 1d array of string based elements
     * @return array
     * */
    public static function trim($array)
    {
        $array = array_map('trim', $array);
        return array_map('ltrim', $array);
    }

    /**
     * Trim the elements of a multi dimensional array
     * @param array $array - a multi dimensional string based array
     */
    public static function trimMultiDimension(&$array) {
        array_walk_recursive($array, 'trim');
    }

    /**
     * Build a hierarchical array from a flat array given a parent field name and a child field name
     * @access public static
     * @param array $array - a 1d array of string based elements
     * @param string $parentFieldName - the key of the parent field
     * @param string $childFieldName - the key under which to add the child fields
     * @return array
     * */
    public static function buildHierarchy($array, $parentFieldName, $childFieldName)
    {
        return self::buildChildren($array, $parentFieldName, $childFieldName);
    }

    /**
     * Build a hierarchical array from a flat array for a particular parent ID given a parent field name and a child field name
     * @access private static
     * @param array $array - a 1d array of string based elements
     * @param string $parentFieldName - the key of the parent field
     * @param string $childFieldName - the key under which to add the child fields
     * @param int $parentId - the ID of the current parent
     * @return array
     * */
    private static function buildChildren($array, $parentFieldName, $childFieldName, $parentId = 0)
    {
        $result = [];
        foreach ($array as $v) {
            if (isset($v[$parentFieldName])) {
                if ($v[$parentFieldName] == $parentId || !$parentId && !$v[$parentFieldName]) {
                    $children = self::buildChildren($array, $parentFieldName, $childFieldName, $v['id']);
                    if (count($children) > 0) {
                        $v[$childFieldName] = $children;
                    }
                    $result[] = $v;
                }
            }
        }
        return $result;
    }

    /**
     * Build a flat hierarchical array
     * @access public static
     * @param array $array - a 1d array of string based elements
     * @param string $parentFieldName - the key of the parent field
     * @param string $nameFieldName - the key of the name field
     * @param string $prefix - the prefix under which to add chidlren
     * @param string $currentPrefix - the current prefix
     * @param int $parentId - the ID of the current parent
     * @return array
     * */
    public static function buildFlatHierarchy(
        $array,
        $parentFieldName,
        $nameFieldName,
        $prefix = '--',
        $currentPrefix = '',
        $parentId = 0
    ) {
        $result = [];
        foreach ($array as $v) {
            if (isset($v[$parentFieldName])) {
                if ($v[$parentFieldName] == $parentId || !$parentId && !$v[$parentFieldName]) {
                    $v[$nameFieldName] = $currentPrefix . $v[$nameFieldName];
                    $result[] = $v;
                    $children = self::buildFlatHierarchy($array, $parentFieldName, $nameFieldName, $prefix,
                        $currentPrefix . $prefix, $v['id']);
                    if (count($children) > 0) {
                        $result = array_merge($result, $children);
                    }
                }
            }
        }
        return $result;
    }

    /**
     * Ordering an array of objects based on a property
     * @access public
     * @param array $objectClassArray
     * @param string $propertyName
     * @param boolean $sortDescending
     */
    public static function order2dClassArray(&$objectClassArray, $propertyName, $sortDescending = false)
    {
        usort($objectClassArray, function ($obj1, $obj2) use ($propertyName, $sortDescending) {
            if ($sortDescending) {
                $result = $obj2->$propertyName - $obj1->$propertyName;
            } else {
                $result = $obj1->$propertyName - $obj2->$propertyName;
            }
            return $result;
        });
    }

    /**
     * Get the elements from an array by their prefix
     * @param array $array - the array of values
     * @param string $prefix - the prefix of the values that we want to return
     * @return array
     * */
    public static function getValuesByPrefix($array, $prefix)
    {
        return array_intersect_key($array, array_flip(preg_grep("/^{$prefix}/", array_keys($array))));
    }

    /**
     * Get the elements from an array by their prefix
     * @param array $array - the array of values
     * @param string $key - the key of the element
     * @param string $value - the value of the element
     * @return array
     * */
    public static function has2dValue($array, $key, $value)
    {
        $result = false;
        foreach ($array as $v) {
            if (isset($v[$key]) && $v[$key] == $value) {
                $result = true;
                break;
            }
        }
        return $result;
    }

    /**
     * Remove from an array all values from an array whose key begins with a specific prefix
     * @param array $array - the array of values
     * @param string $prefix - the prefix of the keys whose values we want to remove
     * @return array
     * */
    public static function removeValuesByKeyPrefix($array, $prefix)
    {
        $result = [];
        foreach ($array as $k => $v) {
            if (strpos($k, $prefix) !== 0) {
                $result[$k] = $v;
            }
        }
        return $result;
    }

    /**
     * Remove a prefix from an array, where the keys are optionally prefixed by a specified string
     * @param array $array - the array of values
     * @param string $prefix - the prefix of the keys that we want to remove
     * @return array
     * */
    public static function removeKeyPrefix($array, $prefix)
    {
        $result = [];
        foreach ($array as $k => $v) {
            $k = preg_replace("/^{$prefix}/", '', $k);
            $result[$k] = $v;
        }
        return $result;
    }

    /**
     * Clearing second Array
     * @access public
     * @param array $array
     * @param String $fieldNamesToKeep
     * @return array $result
     */
    public static function clean2dArray($array, $fieldNamesToKeep)
    {
        $result = [];
        foreach ($array as $item) {
            $result[] = self::cleanArray($item, $fieldNamesToKeep);
        }
        return $result;
    }

    /**
     * Clean Array
     * @access public
     * @param array $array
     * @param string|array $fieldNamesToKeep
     * @param bool $sortByNamesToKeep
     * @return array $result
     */
    public static function cleanArray($array, $fieldNamesToKeep, $sortByNamesToKeep = false)
    {
        $result = [];
        if (is_array($array)) {
            if (is_array($fieldNamesToKeep) && $sortByNamesToKeep) {
                foreach ($fieldNamesToKeep as $v) {
                    if (array_key_exists($v, $array)) {
                        $result[$v] = $array[$v];
                    }
                }
            } else {
                foreach ($array as $k => $v) {
                    if (array_search($k, $fieldNamesToKeep) !== false) {
                        $result[$k] = $v;
                    }
                }
            }
        }
        return $result;
    }

    /**
     * Index a 2D array by a certain field
     * @access public
     * @param array $array
     * @param String $indexField
     * @return array $result
     */
    public static function indexArrayByField($array, $indexField)
    {
        $result = [];
        foreach ($array as $item) {
            $result[$item[$indexField]] = $item;
        }
        return $result;
    }

    /**
     * Remove certain elements from a 2D array
     * @access public
     * @param array $array
     * @param String $fieldNamesToRemove
     * @return array $result
     */
    public static function removeAssocElements($array, $fieldNamesToRemove)
    {
        $result = [];
        foreach ($array as $k => $v) {
            if (array_search($v, $fieldNamesToRemove) === false) {
                $result[$k] = $v;
            }
        }
        return $result;
    }

    /**
     * To get value from second array
     * @access public
     * @param array $array
     * @param String $fieldName
     * @param Boolean $distinct
     * @return array $result
     */
    public static function getFieldValuesFrom2dArray($array, $fieldName, $distinct = false)
    {
        $result = [];
        if (isset($array) && is_array($array) && count($array) > 0) {
            $count = count($array);
            $array = array_values($array);
            for ($i = 0; $i < $count; $i++) {
                if (isset($array[$i][$fieldName])) {
                    $result[] = $array[$i][$fieldName];
                }
            }
        }
        if ($distinct) {
            $result = array_values(array_unique($result));
        }
        return $result;
    }

    /**
     * To get value from second array
     * @access public
     * @param array $array
     * @param String $fieldName
     * @param Boolean $distinct
     * @param null|string|callable $prepend - can be string or callback function taking a single parameter
     * @param null|string|callable $append - can be string or callback function taking two parameters (src and original src respectively)
     * @return array $result
     */
    public static function getStringValuesFrom2dArray($array, $fieldName, $distinct = false, $prepend = null, $append = null)
    {
        $result = [];
        if (isset($array) && is_array($array) && count($array) > 0) {
            $count = count($array);
            $array = array_values($array);
            for ($i = 0; $i < $count; $i++) {
                if (isset($array[$i][$fieldName])) {
                    $value = $array[$i][$fieldName];
                    $valueOriginal = $value;


                    $prependType = gettype($prepend);
                    if ($prependType === 'string') {
                        $value = $prepend . $value;
                    } elseif ($prependType === 'object') {
                        $prependValue = $prepend($value);
                        $value = $prependValue . $value;
                    }

                    $appendType = gettype($append);
                    if ($appendType === 'string') {
                        $value = $value . $append;
                    } elseif ($appendType === 'object') {
                        $appendValue = $append($value, $valueOriginal);
                        $value = $value . $appendValue;
                    }
                    
                    $result[] = $value;
                }
            }
        }
        if ($distinct) {
            $result = array_values(array_unique($result));
        }
        return $result;
    }

    /**
     * Iterate through a 2d array and return a new array with each element prefixed by an element in the original array
     * @param array $array
     * @param string $valueKey
     * @return array
     * */
    public static function prefixByValue($array, $valueKey/*, $strict = false*/)
    {
        $result = [];
        foreach ($array as $v) {
            if (isset($v[$valueKey])) {
                $result[$v[$valueKey]] = $v;
            }
        }
        return $result;
    }

    /**
     * The main function for converting to an XML document.
     * Pass in a multi dimensional array and this recursively loops through and builds up an XML document.
     *
     * @param array $data
     * @param string $rootNodeName - what you want the root node to be - defaultsto data.
     * @param \SimpleXMLElement $xml - should only be used recursively
     * @param string $parentKey - the parent key name
     * @return string XML
     */
    public static function toXml($data, $rootNodeName = 'data', $xml = null, $parentKey = null)
    {
        if ($xml == null) {
            $xml = simplexml_load_string("<$rootNodeName />");
        }
        // loop through the data passed in.
        if (is_array($data) && count($data) > 0) {
            foreach ($data as $key => $value) {
                // no numeric keys in our xml please!
                if (is_numeric($key) && $parentKey == "searchResults") {
                    $key = $parentKey . "_element";
                } else {
                    if (is_numeric($key)) {
                        // make string key...
                        $key = $parentKey . "_element";//. (string) $key;
                    }
                }
                // replace anything not alpha numeric
                $key = preg_replace('/[^a-z_0-9]/i', '', $key);
                // if there is another array found recrusively call this function
                if (is_array($value)) {
                    $node = $xml->addChild($key);
                    self::toXml($value, $rootNodeName, $node, $key);
                } else {
                    $value = htmlentities($value);
                    $xml->addChild($key, $value);
                }
            }
        }
        // pass back as string. or simple xml object if you want!
        return $xml->asXML();
    }

    /**
     * Convert an array to lower case elements and make it unique
     * @access public
     * @param array $array
     * @return array
     * */
    public static function toLowerUnique($array)
    {
        $result = [];
        if (is_array($array)) {
            foreach ($array as $v) {
                if (is_scalar($v) && !is_bool($v)) {
                    $v = strtolower($v);
                    if (!in_array($v, $result)) {
                        $result[] = $v;
                    }
                }
            }
        }
        return $result;
    }


    /**
     * Take an indent list and convert it to an associative array
     *
     * Indent list example:
     * array (size=3)
     *   0 =>
     *       array (size=2)
     *          'value' => string 'testValue01'
     *          'indent' => string '0'
     *   1 =>
     *       array (size=2)
     *           'value' => string 'testValue02'
     *           'indent' => string '1'
     *   2 =>
     *       array (size=2)
     *           'value' => string 'TestValue03'
     *           'indent' => string '2'
     *
     * @access public
     * @param array $indentList - Indent list to be converted
     * @return array
     */
    public static function indentListToArray($indentList)
    {
        $result = [];
        $maxIndent = 0;
        $previousIndent = -1;
        $error = false;
        $resultArray = [];

        if (is_array($indentList)) {
            //Find the maximum indent within the list
            foreach ($indentList as $indentListEntry) {
                $nextIndent = $indentListEntry['indent'];
                $maxIndent = max($nextIndent, $maxIndent);

                //Check to make sure data is in an acceptable format
                if ($nextIndent - $previousIndent > 1 || $nextIndent < 0) {
                    $error = true;
                }
                $previousIndent = $nextIndent;
            }
            if (!$error) {
                //Isolate indexes for each indent level
                for ($ii = $maxIndent; $ii >= 0; $ii--) {
                    $aboveIndex = 0;
                    $index = 0;
                    foreach ($indentList as $indentListEntry) {
                        //Mark the index of the parent
                        if ($indentListEntry['indent'] == $ii - 1) {
                            $aboveIndex = $index;
                        } elseif ($indentListEntry['indent'] == $ii) {
                            $result[$ii][$aboveIndex][] = $index;
                        }
                        $index++;
                    }
                }

                //Use the created index list to recursively create the array
                $resultArray = self::getIndentListValues(0, 0, $result, $indentList);
            }
        }
        return $resultArray;
    }

    /**
     * Recursive function to read a list of indent sorted indexes and create an associative array
     *
     * @access private
     * @param int $level - The indent level
     * @param int $topIndex - The index to create the associative array from
     * @param array $indexes - The list indexes, sorted by indent level
     * @param array $indentList - Original indent list containing list values
     * @return array
     */
    private static function getIndentListValues($level, $topIndex, $indexes, $indentList)
    {
        $result = [];
        foreach ($indexes[$level][$topIndex] as $index) {
            //Continue to recurse branch nodes
            if (isset($indexes[$level + 1]) && isset($indexes[$level+1][$index])) {
                $result[$indentList[$index]['value']] = self::getIndentListValues($level + 1, $index, $indexes,
                    $indentList);
            } else {
                //Return resulting leaf nodes
                $result[] = $indentList[$index]['value'];
            }
        }
        return $result;
    }

}
