<?php
/**
 * Copyright (c) BlueSky Digital Labs Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Util;

use BlueSky\Framework\Object\Entity\VirtualTypes\Type\StdDateTime;
use DateTime;

/**
 * Class for Date manipulation utilities.
 */
class Date
{

    public static function dateFormat(string $date, string $format): string
    {
        $result = '';
        try {
            if (!empty($date)) {
                if ($date instanceof DateTimeInterface) {
                    $d = $date;
                } else {
                    $d = new DateTime($date);
                }
                $result = $d->format($format);
            }
        } catch (\Exception $e) {
            if (strlen($date) > 10) {
                $result = self::dateFormat(substr($date, 10), $format);
            }
        }
        return $result;
    }

    public static function validateDate(string $date): bool
    {
        /// Assuming date comes in format yyyy-mm-dd
        $result = false;
        if (strlen($date) <= 10) {
            $pieces = explode('-', $date);
            if (count($pieces) == 3) {
                if (is_numeric($pieces[2]) && is_numeric($pieces[1]) && is_numeric($pieces[0])) {
                    $day = $pieces[2];
                    $month = $pieces[1];
                    $year = $pieces[0];
                    $result = checkdate((int)$month, (int)$day, (int)$year); //australian date time format
                }
            }
        }
        return $result;
    }

    public static function validateTime(string $time): bool
    {
        /// Assuming time comes in format hh:mm:ss
        $result = false;
        /* time check */
        $pieces = explode(':', $time);
        if (count($pieces) == 3) {
            if (is_numeric($pieces[2]) && is_numeric($pieces[1]) && is_numeric($pieces[0])) {
                $hour = $pieces[0];
                $minute = $pieces[1];
                $second = $pieces[0];
                $result = $hour >= 0 && $hour <= 23 &&
                    $minute >= 0 && $minute <= 59 &&
                    $second >= 0 && $second <= 59;
            }
        }
        return $result;
    }

    /* Requires minimum of custom field branch in framework */
    public static function validateDateTime(string $dateTime): bool
    {
        $dateValid = false;
        $timeValid = false;
        /// Assuming datetime coming in is a valid datetime string (yyyy-mm-dd hh:mm:ss)
        if (!empty($dateTime)) {
            $dateTime = trim($dateTime);
            if (strlen($dateTime) <= 19) {
                $pieces = explode(' ', $dateTime);
                $date = $pieces[0];
                $time = $pieces[1];
                $year = strstr($date, '-', true);

                $dateValid = $year >= StdDateTime::MINIMUM_YEAR && self::validateDate($date);
                $timeValid = self::validateTime($time);
            }
        }
        return $dateValid && $timeValid;
    }

    public static function asString(string $date = 'now', string $format = 'c'): string
    {
        $d = new DateTime($date);
        return $d->format($format);
    }

    /* Check how many days ago a date occurred */
    public static function daysAgo(DateTime $date): string
    {
        $now = Time::getUtcObject('now');
        return $date->diff($now)->format("%a");
    }

}
