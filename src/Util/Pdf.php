<?php
namespace BlueSky\Framework\Util;


/**
 * Class for PDF Printing
 * @deprecated Deprecated by Sidrablue\Util\Printer\HtmlToPdf
 */
class Pdf
{
    /**
     * Print a page to PDF
     *
     * @deprecated No longer recommended
     * @see \Sidrablue\Util\Printer\HtmlToPdf::printPdf()
     *
     * @access public
     * @param String $url
     * @param String $fileName
     * @param bool $printVersion
     * @param string $sessionId
     * @param bool $jsDelay
     * @return string
     */
    public static function printPdf($url, $fileName, $printVersion = false, $sessionId = '', $jsDelay = false, $convertEntities = true)
    {
        return self::wkhtmlPrint($url, $fileName, $printVersion, $sessionId, $jsDelay, $convertEntities);
    }

    private static function wkhtmlPrint($url, $fileName, $printVersion = false, $sessionId = '', $jsDelay = false, $convertEntities = true)
    {
        $url = str_replace('https://', 'http://', $url );

        $isLandScapePrint =  false;
        $disableSmartShrinking =  false;
        $saveLocally = false;
        $noMargin =  false;
        $zoom = 1.0;
        $minFontSize = false;
        $pageWidth =  false;
        $pageHeight =  false;
        $dpi = 2400;
        $showPageNum =  false;
        //Retrieve additional commands from url
        $urlParse = parse_url($url);
        if(isset($urlParse['query']) && !empty($urlParse['query'])){
            parse_str($urlParse['query'], $query);
            if(isset($query['is_landscape']) && $query['is_landscape'] == 1){
                $isLandScapePrint = true;
            }
            if(isset($query['print_method']) && $query['print_method'] == "save_file"){
                $saveLocally = true;
            }
            if(isset($query['disable_smart_shrinking']) && $query['disable_smart_shrinking'] == 1){
                $disableSmartShrinking = true;
            }
            if(isset($query['no_margin']) && $query['no_margin'] == 1){
                $noMargin = true;
            }

            if(isset($query['dpi']) && $query['dpi']){
                $dpi = $query['dpi'];
            }
            if(isset($query['zoom']) && $query['zoom'] > 0.0){
                $zoom = $query['zoom'];
            }
            if(isset($query['width']) && $query['width'] > 0){
                $pageWidth = $query['width'];
            }
            if(isset($query['height']) && $query['height'] > 0){
                $pageHeight = $query['height'];
            }
            if(isset($query['minimum_font_size'])){
                $minFontSize = $query['minimum_font_size'];
            }
            if(isset($query['show_page_num']) && $query['show_page_num'] == "1"){
                $showPageNum = true;
            }
        }

        Dir::make(LOTE_ASSET_PATH . 'temp/');
        if ($saveLocally && $fileName) {
            $tempName = $fileName;
        } else {
            $tempName = uniqid('tmp_');
        }

        $pdfFile = LOTE_ASSET_PATH . 'temp/' . $tempName . '.pdf';
        $command = "wkhtmltopdf ";

        if($isLandScapePrint){
            $command .= ' -O landscape';
        }

        if($noMargin){
            $command .= ' -T 0 -R 0 -B 0 -L 0';
        }

        if($disableSmartShrinking){
            $command .= " --disable-smart-shrinking";
        }

        if($pageWidth){
            $command .= " --page-width ".$pageWidth."mm";
        }

        if($pageHeight){
            $command .= " --page-height ".$pageHeight."mm";
        }

        if($minFontSize){
            $command .= " --minimum-font-size " . $minFontSize;
        }

        if ($printVersion) {
            $command .= '  ';
        }
        if ($sessionId) {
            $command .= ' --cookie sessionid ' . $sessionId;
        }
        if ($sessionId) {
            $command .= ' --cookie ip ' . $_SERVER['REMOTE_ADDR'];
        }
        if ($jsDelay) {
            $command .= ' --javascript-delay 10 ';  //--window-status lote_print_loaded ';
        }
        if($convertEntities) {
            $url = htmlentities($url);
        }
        if ($showPageNum) {
            $command .= ' --footer-right "Page [page] of [toPage]" ';
        }
        $command .= " --dpi ".$dpi;
        $command .= " --zoom " . $zoom;
        $command .= " \"{$url}\" {$pdfFile}";
        shell_exec($command);
        if($saveLocally) {
            return $pdfFile;
        } else {
            $pdfContent = file_get_contents($pdfFile);

            header('Pragma: public');
            header('Expires: 0');
            header('Content-Type: application/pdf');
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
            header('Content-Length: ' . strlen($pdfContent));
            header('Content-Disposition: attachment;filename="' . $fileName . '"');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Content-Type: application/force-download');
            header('Content-Type: application/octet-stream');
            header('Content-Type: application/download');
            ob_clean();
            flush();
            echo $pdfContent;
            unlink($pdfFile);
            die;
        }
    }

//    //The below version of wkhtmlPrint is equivalent to the above, only using the new HtmlToPdf class
//    private static function wkhtmlPrint($url, $fileName, $printVersion = false, $sessionId = '', $jsDelay = false, $convertEntities = true)
//    {
//        $pageOptions = new PageOptions();
//        $globalOptions = new GlobalOptions();
//
//        //Retrieve additional commands from url
//        $urlParse = parse_url($url);
//        if(isset($urlParse['query']) && !empty($urlParse['query'])){
//            parse_str($urlParse['query'], $query);
//            if(isset($query['is_landscape']) && $query['is_landscape'] == 1){
//                $globalOptions->setOrientationToLandscape();
//            }
//            if(isset($query['disable_smart_shrinking']) && $query['disable_smart_shrinking'] == 1){
//                $pageOptions->setSmartShrinking(false);
//            }
//            if(isset($query['no_margin']) && $query['no_margin'] == 1){
//                $globalOptions->setAllMargins(0.0);
//            }
//            if(isset($query['zoom']) && $query['zoom'] > 0.0){
//                $pageOptions->setZoom($query['zoom']);
//            }
//            if(isset($query['width']) && $query['width'] > 0){
//                $globalOptions->setPageWidth($query['width']);
//            }
//            if(isset($query['height']) && $query['height'] > 0){
//                $globalOptions->setPageHeight($query['height']);
//            }
//            if(isset($query['minimum_font_size'])){
//                $pageOptions->setMinFontSize($query['minimum_font_size']);
//            }
//        }
//
//        if ($sessionId) {
//            $pageOptions->addCookie('sessionid', $sessionId);
//            $pageOptions->addCookie('ip', $_SERVER['REMOTE_ADDR']);
//        }
//        if ($jsDelay) {
//            $pageOptions->setJavascriptDelay(10);
//        }
//        if($convertEntities) {
//            $url = htmlentities($url);
//        }
//
//        $printer = new HtmlToPdf($url, $fileName);
//        $printer->setPageOptions($pageOptions);
//        $printer->setGlobalOptions($globalOptions);
//        $printer->printPdf();
//    }
}
