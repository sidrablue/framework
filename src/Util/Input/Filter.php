<?php
namespace BlueSky\Framework\Util\Input;

use Aura\Input\Filter as BaseFilter;
use Aura\Input\FilterInterface;
use Closure;

/**
 *
 * A filter
 *
 * @package SidraBlue.Util.Input
 *
 */
class Filter extends BaseFilter implements FilterInterface
{


    /**
     *
     * The array of messages to be used when rules fail, that contains the combined messages for all items and not just
     * the first message that occurs
     *
     * @var array
     *
     */
    protected $allMessages = [];

    /**
     *
     * Gets the combined messages for all fields, or for a single field.
     *
     * @param string $field If empty, return all messages for all fields;
     * otherwise, return only messages for the named field.
     *
     * @return array|null
     *
     */
    public function getAllMessages($field = null)
    {
        $result = [];
        if (!$field) {
            $result = $this->allMessages;
        }

        if (isset($this->allMessages[$field])) {
            $result = $this->allMessages[$field];
        }

        return $result;
    }

    /**
     *
     * Sets a filter rule on a field.
     * @param string $field The field name.
     * @param string $message The message when the rule fails.
     * @param Closure $closure A closure that implements the rule. It must
     * have the signature `function ($value, &$fields)`; it must return
     * boolean true on success, or boolean false on failure.
     */
    public function setRule($field, $message, \Closure $closure)
    {
        $this->rules[$field][] = [$message, $closure];
    }

    /**
     * Filter (sanitize and validate) the data.
     *
     * @param mixed $values The values to be filtered.
     *
     * @return bool True if all rules passed; false if one or more failed.
     *
     */
    public function values(&$values)
    {
        // reset the messages
        $this->messages = [];

        // go through each of the rules
        foreach ($this->rules as $field => $rules) {
            // go through multiple rules for a field
            foreach ($rules as $rule) {
                // get the message and closure
                list($message, $closure) = $rule;

                // apply the closure to the data and get back the result
                $passed = $closure($values->$field, $values, $message, $this);

                // if the rule did not pass, retain a message for the field.
                // note that it is in an array, so that other implementations
                // can allow for multiple messages.
                if (!$passed) {
                    $this->allMessages[$field][] = $message;
                    if (!isset($this->messages[$field])) {
                        $this->messages[$field] = $message;
                    }
                }
            }
        }

        // if there are messages, one or more values failed
        return $this->messages ? false : true;
    }

    /**
     *
     * Manually add messages to a particular field.
     *
     * @param string $field Add to this field.
     *
     * @param string|array $messages Add these messages to the field.
     *
     * @return void
     *
     */
    public function addMessages($field, $messages)
    {
        if (is_array($messages)) {
            foreach ($messages as $v) {
                $this->allMessages[$field][] = $v;
            }
        } else {
            $this->allMessages[$field][] = $messages;
        }
    }

}
