<?php
namespace BlueSky\Framework\Util\Printer;

use BlueSky\Framework\Util\Dir;
use BlueSky\Framework\Util\Printer\Options\Base as OptionsBase;
use BlueSky\Framework\Util\Printer\Options\GlobalOptions;
use BlueSky\Framework\Util\Printer\Options\HeaderFooterOptions;
use BlueSky\Framework\Util\Printer\Options\OutlineOptions;
use BlueSky\Framework\Util\Printer\Options\PageOptions;
use BlueSky\Framework\Util\Printer\Options\TocOptions;
use SidraBlue\Util\Printer\Options\CoverPage;

/**
 * Class HtmlToPdf
 * @package Sidrablue\Util\Print
 */
class HtmlToPdf
{
    /**
     * @var string $url - Print URL
     */
    private $url;

    /**
     * @var string $fileName - Print file name
     */
    private $fileName;

    /**
     * @var string $filePath - Print file path
     */
    private $filePath;

    /**
     * @var GlobalOptions $globalOptions - Global print options
     */
    private $globalOptions = null;

    /**
     * @var OutlineOptions $outlineOptions - Outline print options
     */
    private $outlineOptions = null;

    /**
     * @var PageOptions $pageOptions - Page print options
     */
    private $pageOptions = null;

    /**
     * @var HeaderFooterOptions $headerFooterOptions - Header and footer print options
     */
    private $headerFooterOptions = null;

    /**
     * @var TocOptions $tocOptions - Table of contents print options
     */
    private $tocOptions = null;

    /**
     * @var CoverPage $coverPage - Cover page
     */
    private $coverPage = null;

    /**
     * HtmlToPdf constructor.
     *
     * @access public
     * @param string $url - Print URL
     * @param string $fileName - Print file name
     * @return void
     */
    public function __construct($url, $fileName)
    {
        $this->url = str_replace('https://', 'http://', $url );
        $this->fileName = $fileName;

        Dir::make(LOTE_ASSET_PATH . 'temp/');
        $tempName = uniqid('tmp_');
        $pdfFile = LOTE_ASSET_PATH . 'temp/' . $tempName . '.pdf';

        $this->filePath = $pdfFile;

        $this->setDefaultOptions();
    }

//    /**
//     * Set required print options
//     *
//     * @access public
//     * @param ...OptionsBase $options - Splat of options objects
//     * @return void
//     */
//    public function setOptions(OptionsBase ...$options)
//    {
//        foreach($options as $optionsObject) {
//            if($optionsObject instanceof GlobalOptions) {
//                $this->setGlobalOptions($optionsObject);
//            }
//            if($optionsObject instanceof OutlineOptions) {
//                $this->setGlobalOptions($optionsObject);
//            }
//            if($optionsObject instanceof PageOptions) {
//                $this->setPageOptions($optionsObject);
//            }
//            if($optionsObject instanceof HeaderFooterOptions) {
//                $this->setHeaderFooterOptions($optionsObject);
//            }
//            if($optionsObject instanceof TocOptions) {
//                $this->setTocOptions($optionsObject);
//            }
//        }
//    }

    /**
     * Add global options
     *
     * @access public
     * @param GlobalOptions $options - Global options
     * @return void
     */
    public function setGlobalOptions(GlobalOptions $options)
    {
        $this->globalOptions = $options;
    }

    /**
     * Add outline options
     *
     * @access public
     * @param OutlineOptions $options - Outline options
     * @return void
     */
    public function setOutlineOptions(OutlineOptions $options)
    {
        $this->outlineOptions = $options;
    }

    /**
     * Add page options
     *
     * @access public
     * @param PageOptions $options - Page options
     * @return void
     */
    public function setPageOptions(PageOptions $options)
    {
        $this->pageOptions = $options;
    }

    /**
     * Add header and footer options
     *
     * @access public
     * @param HeaderFooterOptions $options - Header and footer options
     * @return void
     */
    public function setHeaderFooterOptions(HeaderFooterOptions $options)
    {
        $this->headerFooterOptions = $options;
    }

    /**
     * Add table of contents options
     *
     * @access public
     * @param TocOptions $options - Table of contents options
     * @return void
     */
    public function setTocOptions(TocOptions $options)
    {
        $this->tocOptions = $options;
    }

    /**
     * Print pdf
     *
     * @access public
     * @return void
     */
    public function printPdf()
    {
        $command = $this->constructCommand();
        $this->generatePdf($command);
        die;
    }


    public function storePdf(){
        $command = $this->constructCommand();
        shell_exec($command);
        return $this->filePath;
    }


    /**
     * View pdf
     *
     * @access public
     * @return void
     */
    public function viewPdf()
    {
        $command = $this->constructCommand();
        $this->generatePreview($command);
        die;
    }


    /**
     * Set default options
     *
     * @access protected
     * @return void
     */
    protected function setDefaultOptions()
    {
        $this->setGlobalOptions(new GlobalOptions());
        $this->setOutlineOptions(new OutlineOptions());
        $this->setPageOptions(new PageOptions());
        $this->setHeaderFooterOptions(new HeaderFooterOptions());
        $this->setTocOptions(new TocOptions());
    }

    /**
     * Construct the print command
     *
     * @access protected
     * @return string
     */
    protected function constructCommand()
    {
        $command = "wkhtmltopdf ";

        $command .= $this->globalOptions->constructCommand();
        $command .= $this->outlineOptions->constructCommand();
        $command .= $this->pageOptions->constructCommand();
        $command .= $this->headerFooterOptions->constructCommand();
        $command .= $this->tocOptions->constructCommand();

        $command .= " \"{$this->url}\" {$this->filePath}";
        return $command;
    }

    /**
     * Execute print command and handle output
     *
     * @access protected
     * @param string $command - Print command
     * @param string $disposition - inline|attachment
     * @return void
     */
    protected function generatePdf($command, $disposition = 'attachment')
    {
        shell_exec($command);
        $pdfContent = file_get_contents($this->filePath);

        header('Pragma: public');
        header('Expires: 0');
        header('Content-Type: application/pdf');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Content-Length: ' . strlen($pdfContent));
        header("Content-Disposition: {$disposition};filename=\"{$this->fileName}\"");
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');

        if ($disposition == 'attachment') {
            header('Content-Type: application/force-download');
            header('Content-Type: application/octet-stream');
            header('Content-Type: application/download');
        }

        ob_clean();
        flush();
        echo $pdfContent;
        unlink($this->filePath);
    }

    /**
     * Execute view command and handle output
     *
     * @access protected
     * @param string $command - Print command
     * @return void
     */
    protected function generatePreview($command)
    {
        shell_exec($command);
        if(file_exists($this->filePath)) {
            $pdfContent = file_get_contents($this->filePath);

            header('Pragma: public');
            header('Expires: 0');
            header('Content-Type: application/pdf');
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
            header('Content-Length: ' . strlen($pdfContent));
            header('Content-Disposition: inline;filename="' . $this->fileName . '"');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            ob_clean();
            flush();
            echo $pdfContent;
            unlink($this->filePath);
        } else {
            die("Unable to preview PDF");
        }
    }
}
