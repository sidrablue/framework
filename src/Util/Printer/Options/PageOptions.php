<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */


namespace BlueSky\Framework\Util\Printer\Options;

/**
 * Class PageOptions
 * @package BlueSky\Framework\Util\Printer\Options
 */
class PageOptions extends Base
{
//Page Options:
//      --allow <path>                  Allow the file or files from the specified folder to be loaded (repeatable)
//      --background                    Do print background (default)
//      --no-background                 Do not print background
//      --bypass-proxy-for <value>      Bypass proxy for host (repeatable)
//      --cache-dir <path>              Web cache directory
//      --checkbox-checked-svg <path>   Use this SVG file when rendering checked
//                                      checkboxes
//      --checkbox-svg <path>           Use this SVG file when rendering unchecked
//                                      checkboxes
//      --cookie <name> <value>         Set an additional cookie (repeatable),
//                                      value should be url encoded.
//      --custom-header <name> <value>  Set an additional HTTP header (repeatable)
//      --custom-header-propagation     Add HTTP headers specified by
//                                      --custom-header for each resource request.
//      --no-custom-header-propagation  Do not add HTTP headers specified by
//                                      --custom-header for each resource request.
//      --debug-javascript              Show javascript debugging output
//      --no-debug-javascript           Do not show javascript debugging output
//                                      (default)
//      --default-header                Add a default header, with the name of the
//                                      page to the left, and the page number to
//                                      the right, this is short for:
//                                      --header-left='[webpage]'
//                                      --header-right='[page]/[toPage]' --top 2cm
//                                      --header-line
//      --encoding <encoding>           Set the default text encoding, for input
//      --disable-external-links        Do not make links to remote web pages
//      --enable-external-links         Make links to remote web pages (default)
//      --disable-forms                 Do not turn HTML form fields into pdf form
//                                      fields (default)
//      --enable-forms                  Turn HTML form fields into pdf form fields
//      --images                        Do load or print images (default)
//      --no-images                     Do not load or print images
//      --disable-internal-links        Do not make local links
//      --enable-internal-links         Make local links (default)
//  -n, --disable-javascript            Do not allow web pages to run javascript
//      --enable-javascript             Do allow web pages to run javascript
//                                      (default)
//      --javascript-delay <msec>       Wait some milliseconds for javascript
//                                      finish (default 200)
//      --keep-relative-links           Keep relative external links as relative
//                                      external links
//      --load-error-handling <handler> Specify how to handle pages that fail to
//                                      load: abort, ignore or skip (default
//                                      abort)
//      --load-media-error-handling <handler> Specify how to handle media files
//                                      that fail to load: abort, ignore or skip
//                                      (default ignore)
//      --disable-local-file-access     Do not allowed conversion of a local file
//                                      to read in other local files, unless
//                                      explicitly allowed with --allow
//      --enable-local-file-access      Allowed conversion of a local file to read
//                                      in other local files. (default)
//      --minimum-font-size <int>       Minimum font size
//      --exclude-from-outline          Do not include the page in the table of
//                                      contents and outlines
//      --include-in-outline            Include the page in the table of contents
//                                      and outlines (default)
//      --page-offset <offset>          Set the starting page number (default 0)
//      --password <password>           HTTP Authentication password
//      --disable-plugins               Disable installed plugins (default)
//      --enable-plugins                Enable installed plugins (plugins will
//                                      likely not work)
//      --post <name> <value>           Add an additional post field (repeatable)
//      --post-file <name> <path>       Post an additional file (repeatable)
//      --print-media-type              Use print media-type instead of screen
//      --no-print-media-type           Do not use print media-type instead of
//                                      screen (default)
//  -p, --proxy <proxy>                 Use a proxy
//      --radiobutton-checked-svg <path> Use this SVG file when rendering checked
//                                      radiobuttons
//      --radiobutton-svg <path>        Use this SVG file when rendering unchecked
//                                      radiobuttons
//      --resolve-relative-links        Resolve relative external links into
//                                      absolute links (default)
//      --run-script <js>               Run this additional javascript after the
//                                      page is done loading (repeatable)
//      --disable-smart-shrinking       Disable the intelligent shrinking strategy
//                                      used by WebKit that makes the pixel/dpi
//                                      ratio none constant
//      --enable-smart-shrinking        Enable the intelligent shrinking strategy
//                                      used by WebKit that makes the pixel/dpi
//                                      ratio none constant (default)
//      --stop-slow-scripts             Stop slow running javascripts (default)
//      --no-stop-slow-scripts          Do not Stop slow running javascripts
//      --disable-toc-back-links        Do not link from section header to toc
//                                      (default)
//      --enable-toc-back-links         Link from section header to toc
//      --user-style-sheet <url>        Specify a user style sheet, to load with
//                                      every page
//      --username <username>           HTTP Authentication username
//      --viewport-size <>              Set viewport size if you have custom
//                                      scrollbars or css attribute overflow to
//                                      emulate window size
//      --window-status <windowStatus>  Wait until window.status is equal to this
//                                      string before rendering page
//      --zoom <float>                  Use this zoom factor (default 1)

    /**
     * @var double $zoom - Zoom factor
     */
    private $zoom;

    /**
     * @var int $minFontSize - Minimum font size
     */
    private $minFontSize;

    /**
     * @var int $javascriptDelay - Javascript Delay
     */
    private $javascriptDelay;

    /**
     * @var bool smartShrinking - Enable smart shrinking
     */
    private $smartShrinking;

    /**
     * @var bool printMediaType - Set media type from screen to print
     */
    private $printMediaType;

    /**
     * @var array cookies - Page cookies
     */
    private $cookies;

    /**
     * @var string windowStatus - Window Status
     */
    private $windowStatus;

    /**
     * @var int pageOffset - Page Offset
     */
    private $pageOffset;

    /**
     * PageOptions default constructor
     *
     * @access public
     */
    public function __construct()
    {
        $this->setZoom(1.0);
        $this->setSmartShrinking(true);
    }

    /**
     * Construct print command for the page options
     *
     * @access public
     * @return string
     */
    public function constructCommand()
    {
        $command = $this->getZoomCommand();
        $command .= $this->getMinFontSizeCommand();
        $command .= $this->getJavascriptDelayCommand();
        $command .= $this->getSmartShrinkingCommand();
        $command .= $this->getPrintMediaTypeCommand();
        $command .= $this->getCookiesCommand();
        $command .= $this->getWindowStatusCommand();
        $command .= $this->getPageOffsetCommand();
        return $command;
    }

    /**
     * Zoom setter
     *
     * @access public
     * @param double $zoom - page zoom
     * @return void
     */
    public function setZoom($zoom)
    {
        if($zoom > 0.0) {
            $this->zoom = $zoom;
        }
    }

    /**
     * Minimum font size setter
     *
     * @access public
     * @param int $minFontSize - Minimum font size
     * @return void
     */
    public function setMinFontSize($minFontSize)
    {
        if($minFontSize > 0) {
            $this->minFontSize = $minFontSize;
        }
    }

    /**
     * Javascript delay setter
     *
     * @access public
     * @param int $javascriptDelay - Javascript delay
     * @return void
     */
    public function setJavascriptDelay($javascriptDelay)
    {
        if($javascriptDelay > 0) {
            $this->javascriptDelay = $javascriptDelay;
        }
    }

    /**
     * Smart shrinking setter
     *
     * @access public
     * @param bool $smartShrinking - Smart shrinking
     * @return void
     */
    public function setSmartShrinking($smartShrinking)
    {
        $this->smartShrinking = $smartShrinking;
    }

    /**
     * Print media type setter
     *
     * @access public
     * @param bool $printMediaType - Print media type
     * @return void
     */
    public function setPrintMediaType($printMediaType)
    {
        $this->printMediaType = $printMediaType;
    }

    /**
     * Window status setter
     *
     * @access public
     * @param string $windowStatus - Window status
     * @return void
     */
    public function setWindowStatus($windowStatus)
    {
        $this->windowStatus = $windowStatus;
    }

    /**
     * Page offset setter
     *
     * @access public
     * @param int $pageOffset - Page offset
     * @return void
     */
    public function setPageOffset($pageOffset = 0)
    {
        $this->pageOffset = $pageOffset;
    }

    /**
     * Add cookie
     *
     * @access public
     * @param string $name - Cookie name
     * @param string $value - Cookie value
     * @return void
     */
    public function addCookie($name, $value)
    {
        $this->cookies[$name] = $value;
    }

    /**
     * Remove cookie
     *
     * @access public
     * @param string $name - Cookie name
     * @return void
     */
    public function removeCookie($name)
    {
        if(isset($this->cookies[$name])) {
            unset($this->cookies[$name]);
        }
    }

    /**
     * Retrieve zoom command
     *
     * @access private
     * @return string
     */
    private function getZoomCommand()
    {
        $command = '';
        if($this->zoom) {
            $command .= " --zoom {$this->zoom}";
        }
        return $command;
    }

    /**
     * Retrieve minimum font size command
     *
     * @access private
     * @return string
     */
    private function getMinFontSizeCommand()
    {
        $command = '';
        if($this->minFontSize) {
            $command .= " --minimum-font-size {$this->minFontSize}";
        }
        return $command;
    }

    /**
     * Retrieve javascript delay command
     *
     * @access private
     * @return string
     */
    private function getJavascriptDelayCommand()
    {
        $command = '';
        if($this->javascriptDelay) {
            $command .= " --javascript-delay {$this->javascriptDelay}";
        }
        return $command;
    }

    /**
     * Retrieve smart shrinking command
     *
     * @access private
     * @return string
     */
    private function getSmartShrinkingCommand()
    {
        $command = '';
        if(!$this->smartShrinking) {
            $command .= " --disable-smart-shrinking";
        }
        return $command;
    }


    /**
     * Retrieve print media type command
     *
     * @access private
     * @return string
     */
    private function getPrintMediaTypeCommand()
    {
        $command = '';
        if($this->printMediaType) {
            $command .= " --print-media-type";
        }
        return $command;
    }

    /**
     * Retrieve window status command
     *
     * @access private
     * @return string
     */
    private function getWindowStatusCommand()
    {
        $command = '';
        if($this->windowStatus) {
            $command .= " --window-status {$this->windowStatus}";
        }
        return $command;
    }

    /**
     * Retrieve page offset command
     *
     * @access private
     * @return string
     */
    private function getPageOffsetCommand()
    {
        $command = '';
        if ($this->pageOffset > 0) {
            $command .= " --page-offset {$this->pageOffset}";
        }
        return $command;
    }

    /**
     * Retrieve cookies command
     *
     * @access private
     * @return string
     */
    private function getCookiesCommand()
    {
        $command = '';
        if(is_array($this->cookies)) {
            foreach($this->cookies as $name=>$value) {
                $command .= " --cookie {$name} {$value}";
            }
        }
        return $command;
    }
}