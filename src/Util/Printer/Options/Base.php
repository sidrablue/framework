<?php



namespace BlueSky\Framework\Util\Printer\Options;

/**
 * Class Base
 * @package BlueSky\Framework\Util\Printer\Options
 */
abstract class Base
{
    /**
     * Construct options command line flags
     *
     * @access protected
     * @return string
     */
    protected abstract function constructCommand();
}