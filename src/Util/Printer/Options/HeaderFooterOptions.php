<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */


namespace BlueSky\Framework\Util\Printer\Options;

/**
 * Class HeaderFooterOptions
 * @package BlueSky\Framework\Util\Printer\Options
 */
class HeaderFooterOptions extends Base
{
//Headers And Footer Options:
//    --footer-center <text>          Centered footer text
//    --footer-font-name <name>       Set footer font name (default Arial)
//    --footer-font-size <size>       Set footer font size (default 12)
//    --footer-html <url>             Adds a html footer
//    --footer-left <text>            Left aligned footer text
//    --footer-line                   Display line above the footer
//    --no-footer-line                Do not display line above the footer(default)
//    --footer-right <text>           Right aligned footer text
//    --footer-spacing <real>         Spacing between footer and content in mm (default 0)
//    --header-center <text>          Centered header text
//    --header-font-name <name>       Set header font name (default Arial)
//    --header-font-size <size>       Set header font size (default 12)
//    --header-html <url>             Adds a html header
//    --header-left <text>            Left aligned header text
//    --header-line                   Display line below the header
//    --no-header-line                Do not display line below the header (default)
//    --header-right <text>           Right aligned header text
//    --header-spacing <real>         Spacing between header and content in mm (default 0)
//    --replace <name> <value>        Replace [name] with value in header and footer (repeatable)

    /**
     * @var string $footerLeft
     */
    private $footerLeft;

    /**
     * @var float $footerFontSize
     */
    private $footerFontSize;

    /**
     * @var bool $footerLine
     */
    private $footerLine;

    /**
     * @var float $footerSpacing
     */
    private $footerSpacing;

    /**
     * @var string $footerHtml
     */
    private $footerHtml;

    /**
     * HeaderFooterOptions default constructor
     *
     * @access public
     */
    public function __construct()
    {

    }

    /**
     * @return string
     */
    public function getFooterLeftCommand(): string
    {
        $command = "";
        if($this->footerLeft) {
            $command = " --footer-left {$this->footerLeft}";
        }
        return $command;
    }

    /**
     * @param string $footerLeft
     */
    public function setFooterLeft(string $footerLeft): void
    {
        $this->footerLeft = $footerLeft;
    }

    /**
     * @return string
     */
    public function getFooterLineCommand(): string
    {
        $command = "";
        if($this->footerLine) {
            $command = " --footer-line";
        }
        return $command;
    }

    /**
     * @param bool $footerLine
     */
    public function setFooterLine(bool $footerLine): void
    {
        $this->footerLine = $footerLine;
    }

    /**
     * @return string
     */
    public function getFooterSpacingCommand(): string
    {
        $command = "";
        if($this->footerSpacing != null) {
            $command = " --footer-spacing {$this->footerSpacing}";
        }
        return $command;
    }

    /**
     * @param float $footerSpacing
     */
    public function setFooterSpacing(float $footerSpacing): void
    {
        $this->footerSpacing = $footerSpacing;
    }

    /**
     * @param float $footerFontSize
     */
    public function setFooterFontSize(float $footerFontSize): void
    {
        $this->footerFontSize = $footerFontSize;
    }

    /**
     * @return string
     */
    public function getFooterFontSizeCommand(): string
    {
        $command = "";
        if($this->footerFontSize) {
            $command = " --footer-font-size {$this->footerFontSize}";
        }
        return $command;
    }
    /**
     * @param string $footerHtml
     */
    public function setFooterHtml(string $footerHtml): void
    {
        $this->footerHtml = $footerHtml;
    }

    /**
     * @return string
     */
    public function getFooterHtmlCommand(): string
    {
        $command = "";
        if($this->footerHtml) {
            $command = " --footer-html {$this->footerHtml}";
        }
        return $command;
    }

    /**
     * Construct print command for the header and footer options
     *
     * @access public
     * @return string
     */
    public function constructCommand()
    {
        $command = $this->getFooterLineCommand();
        $command .= $this->getFooterLeftCommand();
        $command .= $this->getFooterFontSizeCommand();
        $command .= $this->getFooterSpacingCommand();
        $command .= $this->getFooterHtmlCommand();
        return $command;
    }
}