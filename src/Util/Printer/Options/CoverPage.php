<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */


namespace BlueSky\Framework\Util\Printer\Options;

/**
 * Class CoverPage
 *
 * @package BlueSky\Framework\Util\Printer\Options
 */
class CoverPage extends Base
{
    /**
     * A cover objects puts the content of a single webpage into the output document,
     * the page does not appear in the table of contents, and does not have headers
     * and footers.
     *
     * cover <input url/file name> [PAGE OPTION]...
     * All options that can be specified for a page object can also be specified for
     * a cover.
     */

    /**
     * @var PageOptions $pageOptions - Cover page options
     */
    private $pageOptions;

    /**
     * @var string $url - Cover page url
     */
    private $url;

    /**
     * Cover constructor.
     *
     * @access public
     * @param $url
     */
    public function __construct($url)
    {
        $this->url = $url;
    }

    /**
     * Add page options to
     *
     * @access public
     * @param PageOptions $pageOptions
     */
    public function addPageOptions(PageOptions $pageOptions)
    {
        $this->pageOptions = $pageOptions;
    }

    /**
     * Construct print command for the table of contents options
     *
     * @access public
     * @return string
     */
    public function constructCommand()
    {
        $command = " cover {$this->url}";
        if($this->pageOptions instanceof PageOptions) {
            $command .= " {$this->pageOptions->constructCommand()}";
        }

        return $command;
    }
}
