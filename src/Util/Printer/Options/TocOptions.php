<?php



namespace BlueSky\Framework\Util\Printer\Options;

/**
 * Class TocOptions
 * @package BlueSky\Framework\Util\Printer\Options
 */
class TocOptions extends Base
{
//TOC Options:
//      --disable-dotted-lines          Do not use dotted lines in the toc
//      --toc-header-text <text>        The header text of the toc (default Table
//                                      of Contents)
//      --toc-level-indentation <width> For each level of headings in the toc
//                                      indent by this length (default 1em)
//      --disable-toc-links             Do not link from toc to sections
//      --toc-text-size-shrink <real>   For each level of headings in the toc the
//                                      font is scaled by this factor (default
//                                      0.8)
//      --xsl-style-sheet <file>        Use the supplied xsl style sheet for
//                                      printing the table of content

    /**
     * TocOptions default constructor
     *
     * @access public
     */
    public function __construct()
    {

    }

    /**
     * Construct print command for the table of contents options
     *
     * @access public
     * @return string
     */
    public function constructCommand()
    {
        return '';
    }
}