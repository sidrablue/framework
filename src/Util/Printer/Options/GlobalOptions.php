<?php



namespace BlueSky\Framework\Util\Printer\Options;

/**
 * Class GlobalOptions
 * @package BlueSky\Framework\Util\Printer\Options
 */
class GlobalOptions extends Base
{
//Global Options:
//      --collate                       Collate when printing multiple copies (default)
//      --no-collate                    Do not collate when printing multiple copies
//      --cookie-jar <path>             Read and write cookies from and to the supplied cookie jar file
//      --copies <number>               Number of copies to print into the pdf file (default 1)
//  -d, --dpi <dpi>                     Change the dpi explicitly (this has no effect on X11 based systems) (default 96)
//  -H, --extended-help                 Display more extensive help, detailing less common command switches
//  -g, --grayscale                     PDF will be generated in grayscale
//  -h, --help                          Display help
//      --htmldoc                       Output program html help
//      --image-dpi <integer>           When embedding images scale them down to this dpi (default 600)
//      --image-quality <integer>       When jpeg compressing images use this
//                                      quality (default 94)
//      --license                       Output license information and exit
//  -l, --lowquality                    Generates lower quality pdf/ps. Useful to
//                                      shrink the result document space
//      --manpage                       Output program man page
//  -B, --margin-bottom <unitreal>      Set the page bottom margin
//  -L, --margin-left <unitreal>        Set the page left margin (default 10mm)
//  -R, --margin-right <unitreal>       Set the page right margin (default 10mm)
//  -T, --margin-top <unitreal>         Set the page top margin
//  -O, --orientation <orientation>     Set orientation to Landscape or Portrait
//                                      (default Portrait)
//      --page-height <unitreal>        Page height
//  -s, --page-size <Size>              Set paper size to: A4, Letter, etc.
//                                      (default A4)
//      --page-width <unitreal>         Page width
//      --no-pdf-compression            Do not use lossless compression on pdf
//                                      objects
//  -q, --quiet                         Be less verbose
//      --read-args-from-stdin          Read command line arguments from stdin
//      --readme                        Output program readme
//      --title <text>                  The title of the generated pdf file (The
//                                      title of the first document is used if not
//                                      specified)
//      --use-xserver                   Use the X server (some plugins and other
//                                      stuff might not work without X11)
//  -V, --version                       Output version information and exit

    /**
     * @var bool $collate - True when you want to collate printing multiple copies
     */
    private $collate;

    /**
     * @var string $cookieJarPath - Read and write cookies from and to the given path
     */
    private $cookieJarPath;

    /**
     * @var int $copies - Number of copies to print
     */
    private $copies;

    /**
     * @var int $dpi - Number of copies to print
     */
    private $dpi;

    /**
     * @var bool $extendedHelp - Display more extensive help
     */
    private $extendedHelp = false;

    /**
     * @var bool $grayscale - Generate PDF in grey scale
     */
    private $grayscale = false;

    /**
     * @var bool $help - Display help
     */
    private $help = false;

    /**
     * @var bool $htmldoc - Output program html help
     */
    private $htmlDoc = false;

    /**
     * @var int $imageDpi - Scale embedded images to this DPI
     */
    private $imageDpi;

    /**
     * @var int $imageQuality - Quality for JPEG compression
     */
    private $imageQuality;

    /**
     * @var bool $license - Output license information
     */
    private $license = false;

    /**
     * @var bool $lowQuality - Produce low quality PDF's
     */
    private $lowQuality = false;

    /**
     * @var bool $manPage - output man page
     */
    private $manPage = false;

    /**
     * @var double $marginBottom - Bottom margin
     */
    private $marginBottom;

    /**
     * @var double $marginTop - Top margin
     */
    private $marginTop;

    /**
     * @var double $marginLeft - Left margin
     */
    private $marginLeft;

    /**
     * @var double $marginRight - Right margin
     */
    private $marginRight;

    /**
     * @var string $orientation - Page orientation
     */
    private $orientation;

    /**
     * @var double $pageHeight - Page height
     */
    private $pageHeight;

    /**
     * @var string $pageSize - Paper size
     */
    private $pageSize;

    /**
     * @var double $pageWidth - Page width
     */
    private $pageWidth;

    /**
     * @var bool $noPdfCompression - True = Do not use compression of PDF objects
     */
    private $noPdfCompression = false;

    /**
     * @var bool $quiet - Be less verbose
     */
    private $quiet = false;

    /**
     * @var bool $readArgsFromStdIn - Read command line arguments from stdin
     */
    private $readArgsFromStdIn = false;

    /**
     * @var bool $readme - Output readme
     */
    private $readme = false;

    /**
     * @var string $title - The title of the generated pdf
     */
    private $title;

    /**
     * @var string $useXServer - Use the X server
     */
    private $useXServer;

    /**
     * @var bool $version - Output version information
     */
    private $version;

    /**
     * GlobalOptions default constructor
     *
     * @access public
     */
    public function __construct()
    {
        $this->setDpi(96);
        $this->setOrientationToPortrait();
    }

    /**
     * Construct print command for the global options
     *
     * @access public
     * @return string
     */
    public function constructCommand()
    {
        $command = $this->getDpiCommand();
        $command .= $this->getMarginCommand();
        $command .= $this->getOrientationCommand();
        $command .= $this->getDimensionsCommand();
        return $command;
    }

    /**
     * Dpi setter
     *
     * @access public
     * @param int $dpi - page dpi
     * @return void
     */
    public function setDpi($dpi)
    {
        if($dpi > 0) {
            $this->dpi = $dpi;
        }
    }
    /**
     * Set top margin
     *
     * @access public
     * @param double $margin - Margin in mm
     * @return void
     */
    public function setTopMargin($margin)
    {
        if($margin > 0.0) {
            $this->marginTop = $margin;
        }
    }

    /**
     * Set bottom margin
     *
     * @access public
     * @param double $margin - Margin in mm
     * @return void
     */
    public function setBottomMargin($margin)
    {
        if($margin > 0.0) {
            $this->marginBottom = $margin;
        }
    }

    /**
     * Set left margin
     *
     * @access public
     * @param double $margin - Margin in mm
     * @return void
     */
    public function setLeftMargin($margin)
    {
        if($margin > 0.0) {
            $this->marginLeft = $margin;
        }
    }

    /**
     * Set right margin
     *
     * @access public
     * @param double $margin - Margin in mm
     * @return void
     */
    public function setRightMargin($margin)
    {
        if($margin > 0.0) {
            $this->marginRight = $margin;
        }
    }

    /**
     * Set all margin
     *
     * @access public
     * @param double $margin - Margin in mm
     * @return void
     */
    public function setAllMargins($margin)
    {
        if($margin >= 0) {
            $this->marginTop = $margin;
            $this->marginBottom = $margin;
            $this->marginLeft = $margin;
            $this->marginRight = $margin;
        }
    }

    /**
     * Set orientation to landscape mode
     *
     * @access public
     * @return void
     */
    public function setOrientationToLandscape()
    {
        $this->orientation = 'Landscape';
    }

    /**
     * Set orientation to portrait mode
     *
     * @access public
     * @return void
     */
    public function setOrientationToPortrait()
    {
        $this->orientation = 'Portrait';
    }

    /**
     * Set page width
     *
     * @access public
     * @param double $pageWidth - Page width
     * @return void
     */
    public function setPageWidth($pageWidth)
    {
        if($pageWidth > 0.0) {
            $this->pageWidth = $pageWidth;
        }
    }

    /**
     * Set page height
     *
     * @access public
     * @param double $pageHeight - Page height
     * @return void
     */
    public function setPageHeight($pageHeight)
    {
        if($pageHeight > 0.0) {
            $this->pageHeight = $pageHeight;
        }
    }

    /**
     * Retrieve DPI command
     *
     * @access private
     * @return string
     */
    private function getDpiCommand()
    {
        return " -d {$this->dpi}";
    }

    /**
     * Retrieve margin command
     *
     * @access private
     * @return string
     */
    private function getMarginCommand()
    {
        $marginCommand = '';
        if(is_numeric($this->marginTop)) {
            $marginCommand .= " -T {$this->marginTop}";
        }
        if(is_numeric($this->marginBottom)) {
            $marginCommand .= " -B {$this->marginBottom}";
        }
        if(is_numeric($this->marginLeft)) {
            $marginCommand .= " -L {$this->marginLeft}";
        }
        if(is_numeric($this->marginRight)) {
            $marginCommand .= " -R {$this->marginRight}";
        }

        return $marginCommand;
    }

    /**
     * Retrieve orientation command
     *
     * @access private
     * @return string
     */
    private function getOrientationCommand()
    {
        return " -O {$this->orientation}";
    }

    /**
     * Retrieve page dimensions command
     *
     * @access private
     * @return string
     */
    private function getDimensionsCommand()
    {
        $command = '';
        if($this->pageSize) {
            $command .= " -s {$this->pageSize}";
        } else {
            if($this->pageWidth) {
                $command .= " --page-width {$this->pageWidth}";
            }
            if($this->pageHeight) {
                $command .= " --page-height {$this->pageHeight}";
            }
        }
        return $command;
    }
}