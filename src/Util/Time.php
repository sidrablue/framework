<?php
namespace BlueSky\Framework\Util;

/**
 * @todo - finish this class
 * Time manipulation utilities.
 *  */
class Time
{

    public const TIMEZONE_LIST = [
        'Kwajalein' => ['name' => '(GMT-12:00) International Date Line West', 'offset' => '-12:00', 'timezone_id' => 'Kwajalein'],
        'Pacific/Midway' => ['name' => '(GMT-11:00) Midway Island', 'offset' => '-11:00', 'timezone_id' => 'Pacific/Midway'],
        'Pacific/Samoa' => ['name' => '(GMT-11:00) Samoa', 'offset' => '-11:00', 'timezone_id' => 'Pacific/Samoa'],
        'Pacific/Honolulu' => ['name' => '(GMT-10:00) Hawaii', 'offset' => '-10:00', 'timezone_id' => 'Pacific/Honolulu'],
        'America/Anchorage' => ['name' => '(GMT-09:00) Alaska', 'offset' => '-09:00', 'timezone_id' => 'America/Anchorage'],
        'America/Los_Angeles' => ['name' => '(GMT-08:00) Pacific Time (US &amp; Canada)', 'offset' => '-08:00', 'timezone_id' => 'America/Los_Angeles'],
        'America/Tijuana' => ['name' => '(GMT-08:00) Tijuana, Baja California', 'offset' => '-08:00', 'timezone_id' => 'America/Tijuana'],
        'America/Denver' => ['name' => '(GMT-07:00) Mountain Time (US &amp; Canada)', 'offset' => '-07:00', 'timezone_id' => 'America/Denver'],
        'America/Chihuahua' => ['name' => '(GMT-07:00) Chihuahua', 'offset' => '-07:00', 'timezone_id' => 'America/Chihuahua'],
        'America/Mazatlan' => ['name' => '(GMT-07:00) Mazatlan', 'offset' => '-07:00', 'timezone_id' => 'America/Mazatlan'],
        'America/Phoenix' => ['name' => '(GMT-07:00) Arizona', 'offset' => '-07:00', 'timezone_id' => 'America/Phoenix'],
        'America/Regina' => ['name' => '(GMT-06:00) Saskatchewan', 'offset' => '-06:00', 'timezone_id' => 'America/Regina'],
        'America/Tegucigalpa' => ['name' => '(GMT-06:00) Central America', 'offset' => '-06:00', 'timezone_id' => 'America/Tegucigalpa'],
        'America/Chicago' => ['name' => '(GMT-06:00) Central Time (US &amp; Canada)', 'offset' => '-06:00', 'timezone_id' => 'America/Chicago'],
        'America/Mexico_City' => ['name' => '(GMT-06:00) Mexico City', 'offset' => '-06:00', 'timezone_id' => 'America/Mexico_City'],
        'America/Monterrey' => ['name' => '(GMT-06:00) Monterrey', 'offset' => '-06:00', 'timezone_id' => 'America/Monterrey'],
        'America/New_York' => ['name' => '(GMT-05:00) Eastern Time (US &amp; Canada)', 'offset' => '-05:00', 'timezone_id' => 'America/New_York'],
        'America/Bogota' => ['name' => '(GMT-05:00) Bogota', 'offset' => '-05:00', 'timezone_id' => 'America/Bogota'],
        'America/Lima' => ['name' => '(GMT-05:00) Lima', 'offset' => '-05:00', 'timezone_id' => 'America/Lima'],
        'America/Rio_Branco' => ['name' => '(GMT-05:00) Rio Branco', 'offset' => '-05:00', 'timezone_id' => 'America/Rio_Branco'],
        'America/Indiana/Indianapolis' => ['name' => '(GMT-05:00) Indiana (East)', 'offset' => '-05:00', 'timezone_id' => 'America/Indiana/Indianapolis'],
        'America/Caracas' => ['name' => '(GMT-04:30) Caracas', 'offset' => '-04:30', 'timezone_id' => 'America/Caracas'],
        'America/Halifax' => ['name' => '(GMT-04:00) Atlantic Time (Canada)', 'offset' => '-04:30', 'timezone_id' => 'America/Halifax'],
        'America/Manaus' => ['name' => '(GMT-04:00) Manaus', 'offset' => '-04:00', 'timezone_id' => 'America/Manaus'],
        'America/Santiago' => ['name' => '(GMT-04:00) Santiago', 'offset' => '-04:00', 'timezone_id' => 'America/Santiago'],
        'America/La_Paz' => ['name' => '(GMT-04:00) La Paz', 'offset' => '-04:00', 'timezone_id' => 'America/La_Paz'],
        'America/St_Johns' => ['name' => '(GMT-03:30) Newfoundland', 'offset' => '-03:30', 'timezone_id' => 'America/St_Johns'],
        'America/Argentina/Buenos_Aires' => ['name' => '(GMT-03:00) Georgetown', 'offset' => '-03:00', 'timezone_id' => 'America/Argentina/Buenos_Aires'],
        'America/Sao_Paulo' => ['name' => '(GMT-03:00) Brasilia', 'offset' => '-03:00', 'timezone_id' => 'America/Sao_Paulo'],
        'America/Godthab' => ['name' => '(GMT-03:00) Greenland', 'offset' => '-03:00', 'timezone_id' => 'America/Godthab'],
        'America/Montevideo' => ['name' => '(GMT-03:00) Montevideo', 'offset' => '-03:00', 'timezone_id' => 'America/Montevideo'],
        'Atlantic/South_Georgia' => ['name' => '(GMT-02:00) Mid-Atlantic', 'offset' => '-02:00', 'timezone_id' => 'Atlantic/South_Georgia'],
        'Atlantic/Azores' => ['name' => '(GMT-01:00) Azores', 'offset' => '-01:00', 'timezone_id' => 'Atlantic/Azores'],
        'Atlantic/Cape_Verde' => ['name' => '(GMT-01:00) Cape Verde Is.', 'offset' => '-01:00', 'timezone_id' => 'Atlantic/Cape_Verde'],
        'Europe/Dublin' => ['name' => '(GMT) Dublin', 'offset' => '00:00', 'timezone_id' => 'Europe/Dublin'],
        'Europe/Lisbon' => ['name' => '(GMT) Lisbon', 'offset' => '00:00', 'timezone_id' => 'Europe/Lisbon'],
        'Europe/London' => ['name' => '(GMT) London', 'offset' => '00:00', 'timezone_id' => 'Europe/London'],
        'Africa/Monrovia' => ['name' => '(GMT) Monrovia', 'offset' => '00:00', 'timezone_id' => 'Africa/Monrovia'],
        'Atlantic/Reykjavik' => ['name' => '(GMT) Reykjavik', 'offset' => '00:00', 'timezone_id' => 'Atlantic/Reykjavik'],
        'Africa/Casablanca' => ['name' => '(GMT) Casablanca', 'offset' => '00:00', 'timezone_id' => 'Africa/Casablanca'],
        'Europe/Belgrade' => ['name' => '(GMT+01:00) Belgrade', 'offset' => '+01:00', 'timezone_id' => 'Europe/Belgrade'],
        'Europe/Bratislava' => ['name' => '(GMT+01:00) Bratislava', 'offset' => '+01:00', 'timezone_id' => 'Europe/Bratislava'],
        'Europe/Budapest' => ['name' => '(GMT+01:00) Budapest', 'offset' => '+01:00', 'timezone_id' => 'Europe/Budapest'],
        'Europe/Ljubljana' => ['name' => '(GMT+01:00) Ljubljana', 'offset' => '+01:00', 'timezone_id' => 'Europe/Ljubljana'],
        'Europe/Prague' => ['name' => '(GMT+01:00) Prague', 'offset' => '+01:00', 'timezone_id' => 'Europe/Prague'],
        'Europe/Sarajevo' => ['name' => '(GMT+01:00) Sarajevo', 'offset' => '+01:00', 'timezone_id' => 'Europe/Sarajevo'],
        'Europe/Skopje' => ['name' => '(GMT+01:00) Skopje', 'offset' => '+01:00', 'timezone_id' => 'Europe/Skopje'],
        'Europe/Warsaw' => ['name' => '(GMT+01:00) Warsaw', 'offset' => '+01:00', 'timezone_id' => 'Europe/Warsaw'],
        'Europe/Zagreb' => ['name' => '(GMT+01:00) Zagreb', 'offset' => '+01:00', 'timezone_id' => 'Europe/Zagreb'],
        'Europe/Brussels' => ['name' => '(GMT+01:00) Brussels', 'offset' => '+01:00', 'timezone_id' => 'Europe/Brussels'],
        'Europe/Copenhagen' => ['name' => '(GMT+01:00) Copenhagen', 'offset' => '+01:00', 'timezone_id' => 'Europe/Copenhagen'],
        'Europe/Madrid' => ['name' => '(GMT+01:00) Madrid', 'offset' => '+01:00', 'timezone_id' => 'Europe/Madrid'],
        'Europe/Paris' => ['name' => '(GMT+01:00) Paris', 'offset' => '+01:00', 'timezone_id' => 'Europe/Paris'],
        'Africa/Algiers' => ['name' => '(GMT+01:00) West Central Africa', 'offset' => '+01:00', 'timezone_id' => 'Africa/Algiers'],
        'Europe/Amsterdam' => ['name' => '(GMT+01:00) Amsterdam', 'offset' => '+01:00', 'timezone_id' => 'Europe/Amsterdam'],
        'Europe/Berlin' => ['name' => '(GMT+01:00) Berlin', 'offset' => '+01:00', 'timezone_id' => 'Europe/Berlin'],
        'Europe/Rome' => ['name' => '(GMT+01:00) Rome', 'offset' => '+01:00', 'timezone_id' => 'Europe/Rome'],
        'Europe/Stockholm' => ['name' => '(GMT+01:00) Stockholm', 'offset' => '+01:00', 'timezone_id' => 'Europe/Stockholm'],
        'Europe/Vienna' => ['name' => '(GMT+01:00) Vienna', 'offset' => '+01:00', 'timezone_id' => 'Europe/Vienna'],
        'Europe/Minsk' => ['name' => '(GMT+02:00) Minsk', 'offset' => '+02:00', 'timezone_id' => 'Europe/Minsk'],
        'Africa/Cairo' => ['name' => '(GMT+02:00) Cairo', 'offset' => '+02:00', 'timezone_id' => 'Africa/Cairo'],
        'Europe/Helsinki' => ['name' => '(GMT+02:00) Helsinki', 'offset' => '+02:00', 'timezone_id' => 'Europe/Helsinki'],
        'Europe/Riga' => ['name' => '(GMT+02:00) Riga', 'offset' => '+02:00', 'timezone_id' => 'Europe/Riga'],
        'Europe/Sofia' => ['name' => '(GMT+02:00) Sofia', 'offset' => '+02:00', 'timezone_id' => 'Europe/Sofia'],
        'Europe/Tallinn' => ['name' => '(GMT+02:00) Tallinn', 'offset' => '+02:00', 'timezone_id' => 'Europe/Tallinn'],
        'Europe/Vilnius' => ['name' => '(GMT+02:00) Vilnius', 'offset' => '+02:00', 'timezone_id' => 'Europe/Vilnius'],
        'Europe/Athens' => ['name' => '(GMT+02:00) Athens', 'offset' => '+02:00', 'timezone_id' => 'Europe/Athens'],
        'Europe/Bucharest' => ['name' => '(GMT+02:00) Bucharest', 'offset' => '+02:00', 'timezone_id' => 'Europe/Bucharest'],
        'Europe/Istanbul' => ['name' => '(GMT+02:00) Istanbul', 'offset' => '+02:00', 'timezone_id' => 'Europe/Istanbul'],
        'Asia/Jerusalem' => ['name' => '(GMT+02:00) Jerusalem', 'offset' => '+02:00', 'timezone_id' => 'Asia/Jerusalem'],
        'Asia/Amman' => ['name' => '(GMT+02:00) Amman', 'offset' => '+02:00', 'timezone_id' => 'Asia/Amman'],
        'Asia/Beirut' => ['name' => '(GMT+02:00) Beirut', 'offset' => '+02:00', 'timezone_id' => 'Asia/Beirut'],
        'Africa/Windhoek' => ['name' => '(GMT+02:00) Windhoek', 'offset' => '+02:00', 'timezone_id' => 'Africa/Windhoek'],
        'Africa/Harare' => ['name' => '(GMT+02:00) Harare', 'offset' => '+02:00', 'timezone_id' => 'Africa/Harare'],
        'Asia/Kuwait' => ['name' => '(GMT+03:00) Kuwait', 'offset' => '+03:00', 'timezone_id' => 'Asia/Kuwait'],
        'Asia/Riyadh' => ['name' => '(GMT+03:00) Riyadh', 'offset' => '+03:00', 'timezone_id' => 'Asia/Riyadh'],
        'Asia/Baghdad' => ['name' => '(GMT+03:00) Baghdad', 'offset' => '+03:00', 'timezone_id' => 'Asia/Baghdad'],
        'Africa/Nairobi' => ['name' => '(GMT+03:00) Nairobi', 'offset' => '+03:00', 'timezone_id' => 'Africa/Nairobi'],
        'Asia/Tbilisi' => ['name' => '(GMT+03:00) Tbilisi', 'offset' => '+03:00', 'timezone_id' => 'Asia/Tbilisi'],
        'Europe/Moscow' => ['name' => '(GMT+03:00) Moscow', 'offset' => '+03:00', 'timezone_id' => 'Europe/Moscow'],
        'Europe/Volgograd' => ['name' => '(GMT+03:00) Volgograd', 'offset' => '', 'timezone_id' => 'Europe/Volgograd'],
        'Asia/Tehran' => ['name' => '(GMT+03:30) Tehran', 'offset' => '+03:30', 'timezone_id' => 'Asia/Tehran'],
        'Asia/Muscat' => ['name' => '(GMT+04:00) Muscat', 'offset' => '+04:00', 'timezone_id' => 'Asia/Muscat'],
        'Asia/Baku' => ['name' => '(GMT+04:00) Baku', 'offset' => '+04:00', 'timezone_id' => 'Asia/Baku'],
        'Asia/Yerevan' => ['name' => '(GMT+04:00) Yerevan', 'offset' => '+04:00', 'timezone_id' => 'Asia/Yerevan'],
        'Asia/Yekaterinburg' => ['name' => '(GMT+05:00) Ekaterinburg', 'offset' => '+05:00', 'timezone_id' => 'Asia/Yekaterinburg'],
        'Asia/Karachi' => ['name' => '(GMT+05:00) Karachi', 'offset' => '+05:00', 'timezone_id' => 'Asia/Karachi'],
        'Asia/Tashkent' => ['name' => '(GMT+05:00) Tashkent', 'offset' => '+05:00', 'timezone_id' => 'Asia/Tashkent'],
        'Asia/Kolkata' => ['name' => '(GMT+05:30) Calcutta', 'offset' => '+05:30', 'timezone_id' => 'Asia/Kolkata'],
        'Asia/Colombo' => ['name' => '(GMT+05:30) Sri Jayawardenepura', 'offset' => '+05:30', 'timezone_id' => 'Asia/Colombo'],
        'Asia/Katmandu' => ['name' => '(GMT+05:45) Kathmandu', 'offset' => '+05:45', 'timezone_id' => 'Asia/Katmandu'],
        'Asia/Dhaka' => ['name' => '(GMT+06:00) Dhaka', 'offset' => '+06:00', 'timezone_id' => 'Asia/Dhaka'],
        'Asia/Almaty' => ['name' => '(GMT+06:00) Almaty', 'offset' => '+06:00', 'timezone_id' => 'Asia/Almaty'],
        'Asia/Novosibirsk' => ['name' => '(GMT+06:00) Novosibirsk', 'offset' => '+06:00', 'timezone_id' => 'Asia/Novosibirsk'],
        'Asia/Rangoon' => ['name' => '(GMT+06:30) Yangon (Rangoon)', 'offset' => '+06:30', 'timezone_id' => 'Asia/Rangoon'],
        'Asia/Krasnoyarsk' => ['name' => '(GMT+07:00) Krasnoyarsk', 'offset' => '+07:00', 'timezone_id' => 'Asia/Krasnoyarsk'],
        'Asia/Bangkok' => ['name' => '(GMT+07:00) Bangkok', 'offset' => '+07:00', 'timezone_id' => 'Asia/Bangkok'],
        'Asia/Jakarta' => ['name' => '(GMT+07:00) Jakarta', 'offset' => '+07:00', 'timezone_id' => 'Asia/Jakarta'],
        'Asia/Brunei' => ['name' => '(GMT+08:00) Beijing', 'offset' => '+08:00', 'timezone_id' => 'Asia/Brunei'],
        'Asia/Chongqing' => ['name' => '(GMT+08:00) Chongqing', 'offset' => '+08:00', 'timezone_id' => 'Asia/Chongqing'],
        'Asia/Hong_Kong' => ['name' => '(GMT+08:00) Hong Kong', 'offset' => '+08:00', 'timezone_id' => 'Asia/Hong_Kong'],
        'Asia/Urumqi' => ['name' => '(GMT+08:00) Urumqi', 'offset' => '+08:00', 'timezone_id' => 'Asia/Urumqi'],
        'Asia/Irkutsk' => ['name' => '(GMT+08:00) Irkutsk', 'offset' => '+08:00', 'timezone_id' => 'Asia/Irkutsk'],
        'Asia/Ulaanbaatar' => ['name' => '(GMT+08:00) Ulaan Bataar', 'offset' => '+08:00', 'timezone_id' => 'Asia/Ulaanbaatar'],
        'Asia/Kuala_Lumpur' => ['name' => '(GMT+08:00) Kuala Lumpur', 'offset' => '+08:00', 'timezone_id' => 'Asia/Kuala_Lumpur'],
        'Asia/Singapore' => ['name' => '(GMT+08:00) Singapore', 'offset' => '+08:00', 'timezone_id' => 'Asia/Singapore'],
        'Asia/Taipei' => ['name' => '(GMT+08:00) Taipei', 'offset' => '+08:00', 'timezone_id' => 'Asia/Taipei'],
        'Australia/Perth' => ['name' => '(GMT+08:00) Perth', 'offset' => '+08:00', 'timezone_id' => 'Australia/Perth'],
        'Asia/Seoul' => ['name' => '(GMT+09:00) Seoul', 'offset' => '+09:00', 'timezone_id' => 'Asia/Seoul'],
        'Asia/Tokyo' => ['name' => '(GMT+09:00) Tokyo', 'offset' => '+09:00', 'timezone_id' => 'Asia/Tokyo'],
        'Asia/Yakutsk' => ['name' => '(GMT+09:00) Yakutsk', 'offset' => '+09:00', 'timezone_id' => 'Asia/Yakutsk'],
        'Australia/Darwin' => ['name' => '(GMT+09:30) Darwin', 'offset' => '+09:30', 'timezone_id' => 'Australia/Darwin'],
        'Australia/Adelaide' => ['name' => '(GMT+09:30) Adelaide', 'offset' => '+09:30', 'timezone_id' => 'Australia/Adelaide'],
        'Australia/Canberra' => ['name' => '(GMT+10:00) Canberra', 'offset' => '+10:00', 'timezone_id' => 'Australia/Canberra'],
        'Australia/Melbourne' => ['name' => '(GMT+10:00) Melbourne', 'offset' => '+10:00', 'timezone_id' => 'Australia/Melbourne'],
        'Australia/Sydney' => ['name' => '(GMT+10:00) Sydney', 'offset' => '+10:00', 'timezone_id' => 'Australia/Sydney'],
        'Australia/Brisbane' => ['name' => '(GMT+10:00) Brisbane', 'offset' => '+10:00', 'timezone_id' => 'Australia/Brisbane'],
        'Australia/Hobart' => ['name' => '(GMT+10:00) Hobart', 'offset' => '+10:00', 'timezone_id' => 'Australia/Hobart'],
        'Asia/Vladivostok' => ['name' => '(GMT+10:00) Vladivostok', 'offset' => '+10:00', 'timezone_id' => 'Asia/Vladivostok'],
        'Pacific/Guam' => ['name' => '(GMT+10:00) Guam', 'offset' => '+10:00', 'timezone_id' => 'Pacific/Guam'],
        'Pacific/Port_Moresby' => ['name' => '(GMT+10:00) Port Moresby', 'offset' => '+10:00', 'timezone_id' => 'Pacific/Port_Moresby'],
        'Asia/Magadan' => ['name' => '(GMT+11:00) Magadan', 'offset' => '+11:00', 'timezone_id' => 'Asia/Magadan'],
        'Pacific/Fiji' => ['name' => '(GMT+12:00) Fiji', 'offset' => '+12:00', 'timezone_id' => 'Pacific/Fiji'],
        'Asia/Kamchatka' => ['name' => '(GMT+12:00) Kamchatka', 'offset' => '+12:00', 'timezone_id' => 'Asia/Kamchatka'],
        'Pacific/Auckland' => ['name' => '(GMT+12:00) Auckland', 'offset' => '+12:00', 'timezone_id' => 'Pacific/Auckland'],
        'Pacific/Tongatapu' => ['name' => '(GMT+13:00) Nukualofa', 'offset' => '+13:00', 'timezone_id' => 'Pacific/Tongatapu'],
    ];

    /**
     * Get a DateTime object in UTC set to now
     * @access public static
     * @param string $format - if a format is set, it will return a string of that format instead of the DateTime object
     * @return \DateTime|String
     * */
    public static function getUtcNow($format = 'Y-m-d H:i:s')
    {
        $result = '';
        try {
            $result = new \DateTime('now', new \DateTimeZone('UTC'));
        if ($format) {
            $result = $result->format($format);
        }
        } catch (\Exception $e) {}
        return $result;
    }

    /**
     * Convert a string representation of a date to a DateTime object
     * @access public static
     * @param string $string - The date time string
     * @return \DateTime|bool
     * */
    public static function strToDateTime($string)
    {
        $result = false;
        if ($string) {
            try {
                $result = new \DateTime($string, new \DateTimeZone('UTC'));
            } catch (\Exception $e) {
                //do nothing
            }
        }
        return $result;
    }

    /**
     * Convert an offset from seconds into the [+-]HH:MM format, such as +05:30
     * @access public static
     * @param int $offsetInSeconds
     * @return string
     * */
    public static function offsetToHours($offsetInSeconds)
    {
        $result = '00:00';
        if ($offsetInSeconds != 0) {
            $offsetInSeconds = $offsetInSeconds / 60;
            $result = ($offsetInSeconds >= 0 ? "+" : "-");
            $offsetInSeconds = abs($offsetInSeconds);
            $result .= sprintf("%02d:%02d", floor($offsetInSeconds / 60), $offsetInSeconds % 60);
        }
        return $result;
    }

    /**
     * Get the information for a particular timezone
     *
     * @access public static
     * @param string $timeZoneId - the PHP timezone ID
     * @return array
     * */
    public static function getTimezone($timeZoneId)
    {
        $result = [];
        if ($timeZoneId) {
            $result = self::TIMEZONE_LIST[$timeZoneId] ?? [];
        }
        return $result;
    }

    /**
     * Get the information for a particular timezone
     *
     * @access public static
     * @param string $timeZoneId - the PHP timezone ID
     * @return bool
     * */
    public static function isValidTimezone($timeZoneId)
    {
        return isset(self::TIMEZONE_LIST[$timeZoneId]);
    }

    /**
     *
     *  */
    public static function strToGmtTime($string)
    {

    }

    public static function utcToLocal($date, $format)
    {
        $d = new \DateTime($date, new \DateTimeZone('UTC'));
        $d->setTimezone(new \DateTimeZone(date_default_timezone_get()));
        return $d->format($format);
    }

    /**
     * Convert an unknown variable into a \DateTime object in UTC
     * @param \DateTime|string $time
     * @return \DateTime
     * */
    public static function getUtcObject($time)
    {
        if ($time instanceof \DateTime) {
            $result = $time;
        } else {
            try {
                $result = new \DateTime($time, new \DateTimeZone('UTC'));
            } catch (\Exception $e) {
                $result = false;
            }
        }
        return $result;
    }

    public static function ldapTimestampToDateString($ldapTimestamp)
    {
        $result = '0000-00-00 00:00:00';
        if ($ldapTimestamp == -1) {
            $result = date("Y-m-d H:i:s", time());
        } elseif ($ldapTimestamp > 0) {
            $secondsAfterActiveDirectoryEpoch = $ldapTimestamp / (10000000);
            $activeDirectory2UnixSeconds = ((1970 - 1601) * 365 - 3 + round((1970 - 1601) / 4)) * 86400;
            $unixTimestamp = intval($secondsAfterActiveDirectoryEpoch - $activeDirectory2UnixSeconds);
            $result = date("Y-m-d H:i:s", $unixTimestamp);
        }
        return $result; // formatted date
    }

    public static function isValidDbDate($date)
    {
        return $date && $date != '0000-00-00 00:00:00';
    }

    /**
     * @return array
     * @deprecated - use the TIMEZONE_LIST variable directly
     */
    public static function getAllTimezones()
    {
        return self::TIMEZONE_LIST;
    }

    /**
     * Convert a date to a timestamp
     * @access public static
     * @param string $time
     * @param string $timezone
     * @return string
     * */
    public static function dateToTimestamp($time, $timezone = 'UTC') {
        $result = '';
        try {
            if ($time instanceof \DateTimeInterface) {
                $time = $time->format('Y-m-d H:i:s');
            }
            $d = new \DateTime($time, new \DateTimeZone($timezone));
            $d->setTimezone(new \DateTimeZone('UTC'));
            $result = $d->getTimestamp();
        }
        catch(\Exception $e) {
            //do nothing
        }
        return $result;
    }

}
