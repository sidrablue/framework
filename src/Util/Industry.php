<?php
namespace BlueSky\Framework\Util;

/**
 * Industry Utility Class
 *  */
class Industry
{

    private static $industryList = [
        'Agriculture',
        'Accounting',
        'Advertising',
        'Aerospace',
        'Aircraft',
        'Airline',
        'Apparel & Accessories',
        'Automotive',
        'Banking',
        'Broadcasting',
        'Brokerage',
        'Biotechnology',
        'Call Centers',
        'Cargo Handling',
        'Chemical',
        'Computer',
        'Consulting',
        'Consumer Products',
        'Cosmetics',
        'Defense',
        'Department Stores',
        'Education',
        'Electronics',
        'Energy',
        'Entertainment & Leisure',
        'Executive Search',
        'Financial Services',
        'Food, Beverage & Tobacco',
        'Grocery',
        'Health Care',
        'Internet Publishing',
        'Investment Banking',
        'Legal',
        'Manufacturing',
        'Motion Picture & Video',
        'Music',
        'Newspaper Publishers',
        'Online Auctions',
        'Pension Funds',
        'Pharmaceuticals',
        'Private Equity',
        'Publishing',
        'Real Estate',
        'Retail & Wholesale',
        'Securities & Commodity Exchanges',
        'Service',
        'Soap & Detergent',
        'Software',
        'Sports',
        'Technology',
        'Telecommunications',
        'Television',
        'Transportation',
        'Trucking',
        'Venture Capital'
    ];

    /**
     * Get the full industry list
     * @access public
     * @return array
     * */
    public static function getList()
    {
        return self::$industryList;
    }

}
