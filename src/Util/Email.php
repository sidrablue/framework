<?php
namespace BlueSky\Framework\Util;

/**
 * Email manipulation utilities.
 *  */
class Email
{

    /**
     * Get the full country list
     * @access public
     * @param string $htmlContent - the HTML content of the email
     * @param string $previewText - string preview text for the email
     * @return string
     * */
    public static function addPreviewText($htmlContent, $previewText)
    {
        $result = $htmlContent;
        $dom = new \DOMDocument();
        @$dom->loadHTML($htmlContent);
        $body = $dom->getElementsByTagName('body');

        if ($dom->textContent) {
            $head = $dom->getElementsByTagName('head');
            if ($head->length == 0) {
                $head = $dom->createElement("head");
                $dom->documentElement->insertBefore($head, $dom->documentElement->firstChild);
            }
            $head = $dom->getElementsByTagName('head');
            $styleNode = $dom->createElement("style",
                '.preheader { display:none !important; visibility:hidden; opacity:0; color:transparent; height:0; width:0; }');
            $head->item(0)->appendChild($styleNode);

            $spanNode = $dom->createElement("span");
            $spanTextNode = $dom->createTextNode($previewText);
            $spanNode->appendChild($spanTextNode);
            $spanNode->setAttribute("class", 'preheader');
            $spanNode->setAttribute("style",
                'display: none !important; visibility: hidden; opacity: 0; color: transparent; height: 0; width: 0;');
            $body->item(0)->insertBefore($spanNode, $body->item(0)->firstChild);
            $dom->removeChild($dom->firstChild);
            $result = $dom->saveHTML();
        }
        return $result;
    }

    /**
     * Add the footer image to an email
     * @access public
     * @param string $htmlContent - the HTML content of the email
     * @param string $imageUrl - string preview text for the email
     * @return string
     * */
    public static function addFooterImage($htmlContent, $imageUrl)
    {
        $result = $htmlContent;

        $dom = new \DOMDocument();
        @$dom->loadHTML($htmlContent);
        $body = $dom->getElementsByTagName('body');

        $pNode = $dom->createElement("p");
        $pNode->setAttribute('style', 'text-align: center;');

        $imgNode = $dom->createElement("img");
        $imgNode->setAttribute('src', $imageUrl);
        if ($body->item(0)) {
            $pNode->appendChild($imgNode);
            $body->item(0)->appendChild($pNode);
            $dom->removeChild($dom->firstChild);
            $result = $dom->saveHTML();
        }
        return $result;
    }

}
