<?php
namespace BlueSky\Framework\Util\Image;

use BlueSky\Framework\Util\Dir;
use BlueSky\Framework\Entity\File as FileEntity;

/**
 * Image resize class for cached PHPCR binary data.
 *
 * This class is used to resize images which are cached after retrieving from DB according to the width
 * and height specified in the request. The origin of the class is from
 * http://net.tutsplus.com/tutorials/php/image-resizing-made-easy-with-php/
 *  */
class Resize
{
    /**
     * @var string $fileName - the file name of the image
     * */
    private $fileName;

    /**
     * @var int $width specifies the width of image to be resized
     * */
    private $width;

    /**
     * @var int $height specifies the height of image to be resized
     * */
    private $height;

    /**
     * @var int $algorithm specifies the resize algorithm to use
     * */
    private $algorithm;

    /**
     * @var \Imagick $imagick - the $imagick object
     * */
    private $imagick;

    private $fileEntity;

    /**
     * Default constructor to set up the required request variables
     * @param string $fileName - the image file to be resized
     * @access public
     * @return  Resize
     */
    function __construct($fileName, $fileBlob = null, FileEntity $fileEntity = null)
    {
        $this->imagick = new \Imagick();
        $this->fileEntity = $fileEntity;
        // Open up the file
        if (file_exists($fileName) || $fileBlob) {
            try {
                $this->fileName = $fileName;
                if ($fileBlob) {
                    $this->imagick->setResolution(300, 300);
                    $this->imagick->readImageBlob($fileBlob);
                } else {
                    $this->imagick->readImage($this->fileName);
                }
                $this->width = $this->imagick->getImageWidth();
                $this->height = $this->imagick->getImageHeight();
            } catch (\ImagickException $e) {

            }
        }
    }

    /**
     * @return int
     */
    public function getAlgorithm()
    {
        return $this->algorithm;
    }

    /**
     * @param int $algorithm
     */
    public function setAlgorithm($algorithm)
    {
        $this->algorithm = $algorithm;
    }

    /**
     * Check if the current object is resizable
     * @access public
     * @return boolean
     * */
    public function isResizable()
    {
        try {
            $result = $this->imagick->valid();
        }
        catch(\ImagickException $e) {
            $result = false;
        }
        return $result;
    }

    /**
     * Function to resize a given image
     * @param int $newWidth - resize width
     * @param int $newHeight - resize height
     * @param string $option - The sizing option
     * @access public
     * @return boolean
     */
    public function resizeImage($newWidth, $newHeight, $option = 'auto')
    {
        $result = false;
        try {
            if ($this->imagick->valid() && $this->getImageBlob()) {
                $optionArray = $this->getDimensions($newWidth, $newHeight, $this->algorithm);
                $optimalWidth = $optionArray['optimalWidth'];
                $optimalHeight = $optionArray['optimalHeight'];
                if($this->imagick->getImageFormat()=="GIF") {
                    $image = $this->imagick->coalesceImages();
                    foreach ($image as $frame) {
                        $frame->thumbnailImage($optimalWidth, $optimalHeight);
                        $frame->setImagePage($optimalWidth, $optimalHeight, 0, 0);
                    }
                    $result = $image->deconstructImages();
                    $tempPath = LOTE_TEMP_PATH.'gif_temp_'.uniqid();
                    Dir::make(dirname($tempPath));
                    $image->writeImages($tempPath, true);
                    $result = file_get_contents($tempPath);
                    unlink($tempPath);
                } else {
                    if ($this->fileEntity !== null && $this->fileEntity->extension === 'pdf') {
                        $this->imagick->setIteratorIndex(0);
                        $this->imagick->setImageFormat('jpg');
                    }
                    $result = $this->imagick->thumbnailImage($optimalWidth, $optimalHeight);
                    if ($option == 'crop') {
                        $result = $this->imagick->cropThumbnailImage($optimalWidth, $optimalHeight);
                    }
                    if($result) {
                        $result = $this->getImageBlob();
                    }
                }
            }
        } catch (\ImagickException $e) {

        }
        return $result;
    }

    private function getImageBlob()
    {
        $result = '';
        if($this->imagick->valid()) {
            if($this->imagick->getImageFormat()=="GIF") {
                $result = $this->imagick->getImagesBlob();
            }
            else {
                $result = $this->imagick->getImageBlob();
            }
        }
        return $result;
    }

    /**
     * Get the content of the resized file
     * @access public
     * @return string
     * */
    public function getContent()
    {
        $result = '';
        if ($this->imagick && $this->isResizable()) {
            return $this->getImageBlob();
        }
        return $result;
    }

    /**
     * Function to get image dimensions according to the option selected for resize
     * @param int $newWidth - resize width
     * @param int $newHeight - resize height
     * @param string $option - resize mode
     * @access private
     * @return  array optimal width and height
     */
    private function getDimensions($newWidth, $newHeight, $option)
    {
        switch ($option) {
            case 'exact':
                $optimalWidth = $newWidth;
                $optimalHeight = $newHeight;
                break;
            case 'portrait':
                $optimalWidth = $this->getSizeByFixedHeight($newWidth, $newHeight);
                $optimalHeight = $newHeight;
                break;
            case 'landscape':
                $optimalWidth = $newWidth;
                $optimalHeight = $this->getSizeByFixedWidth($newWidth, $newHeight);
                break;
            case 'auto':
                $optionArray = $this->getSizeByAuto($newWidth, $newHeight);
                $optimalWidth = $optionArray['optimalWidth'];
                $optimalHeight = $optionArray['optimalHeight'];
                break;
            case 'crop':
                $optionArray = $this->getOptimalCrop($newWidth, $newHeight);
                $optimalWidth = $optionArray['optimalWidth'];
                $optimalHeight = $optionArray['optimalHeight'];
                break;
            default:
                $optimalWidth = $newWidth;
                $optimalHeight = $newHeight;
                break;
        }
        return array('optimalWidth' => $optimalWidth, 'optimalHeight' => $optimalHeight);
    }

    /**
     * Function to get image size by height
     * @param int $newHeight - height of image
     * @access private
     * @return  int $newWidth
     */
    private function getWidthByFixedHeight($newHeight)
    {
        if ($newHeight > 0) {
            $ratio = $this->width / $this->height;
            $result = $newHeight * $ratio;
        } else {
            $result = $this->height;
        }
        return $result;
    }

    /**
     * Function to get image size by width
     * @param int $newWidth - width of image
     * @access private
     * @return  int $newHeight
     */
    private function getHeightByFixedWidth($newWidth)
    {
        if ($newWidth > 0) {
            $ratio = $this->height / $this->width;
            $result = $newWidth * $ratio;
        } else {
            $result = $this->width;
        }
        return $result;
    }

    /**
     * Function to get image size if selected mode is auto
     * @param int $newWidth - width of image
     * @param int $newHeight - height of image
     * @access private
     * @return array optimal width and height
     */
    private function getSizeByAuto($newWidth, $newHeight)
    {
        if ($newWidth > 0 || $newHeight > 0) {
            if ($newWidth > 0 && $newHeight > 0) {
                $optimalWidth = $newWidth;
                $optimalHeight = $newHeight;
            } elseif ($newHeight > 0) {
                $optimalHeight = $newHeight;
                $optimalWidth = $this->getWidthByFixedHeight($newHeight);
            } else {
                //Assertion: newWidth must be > 0
                $optimalWidth = $newWidth;
                $optimalHeight = $this->getHeightByFixedWidth($newWidth);
            }
        } else {
            $optimalWidth = $newWidth;
            $optimalHeight = $newHeight;
        }
        return ['optimalWidth' => $optimalWidth, 'optimalHeight' => $optimalHeight];
    }

    /**
     * Function to get image size if selected mode is crop
     * @param int $newWidth - width of image
     * @param int $newHeight - height of image
     * @access private
     * @return array optimal width and height
     */
    private function getOptimalCrop($newWidth, $newHeight)
    {

        $heightRatio = $this->height / $newHeight;
        $widthRatio = $this->width / $newWidth;

        if ($heightRatio < $widthRatio) {
            $optimalRatio = $heightRatio;
        } else {
            $optimalRatio = $widthRatio;
        }

        $optimalHeight = $this->height / $optimalRatio;
        $optimalWidth = $this->width / $optimalRatio;

        return array('optimalWidth' => $optimalWidth, 'optimalHeight' => $optimalHeight);
    }

}
