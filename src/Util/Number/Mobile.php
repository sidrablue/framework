<?php
namespace BlueSky\Framework\Util\Number;

use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;
use BlueSky\Framework\Util\Strings;

/**
 * Mobile reference utilities.
 */
class Mobile
{

    /**
     * Standardise the mobile number
     * @access public static
     * @param int $number
     * @param string|boolean $region
     * @param string $fallbackRegion
     * @return string|bool
     */
    public static function standardise($number, $region = false, $fallbackRegion = "AU")
    {
        $result = false;
        $value = Strings::removeSpaces($number);
        if (!empty($value)) {
            try {
                $phoneUtil = PhoneNumberUtil::getInstance();
                if ($region) {
                    $number = $phoneUtil->parse($value, $region);
                    $result = $phoneUtil->format($number, PhoneNumberFormat::E164);
                } elseif (self::isPossiblyAuMobileNumber($value)) {
                    $aussieNumberProto = $phoneUtil->parse($value, "AU");
                    $result = $phoneUtil->format($aussieNumberProto, PhoneNumberFormat::E164);
                } elseif (self::isPossiblyNzMobileNumber($value)) {
                    $aussieNumberProto = $phoneUtil->parse($value, "NZ");
                    $result = $phoneUtil->format($aussieNumberProto, PhoneNumberFormat::E164);
                } elseif (self::isPossiblyGbMobileNumber($value)) {
                    $aussieNumberProto = $phoneUtil->parse($value, "GB");
                    $result = $phoneUtil->format($aussieNumberProto, PhoneNumberFormat::E164);
                } else {
                    $aussieNumberProto = $phoneUtil->parse($value, $fallbackRegion);
                    $result = $phoneUtil->format($aussieNumberProto, PhoneNumberFormat::E164);
                }
            } catch (NumberParseException $e) {
                //var_dump($e);
            }
        }
        return $result;
    }

    /**
     * Check if a number is possible an AU mobile number
     * @access public
     * @param string $number
     * @return boolean
     * */
    public static function isPossiblyAuMobileNumber($number)
    {
        $len = strlen(Strings::removeSpaces($number));
        return ($len == 9 && Strings::startsWith("4", $number)) || ($len == 10 && Strings::startsWith("04", $number));
    }

    /**
     * Check if a number is possible an NZ mobile number
     * @access public
     * @param string $number
     * @return boolean
     * */
    public static function isPossiblyNzMobileNumber($number)
    {
        $len = strlen(Strings::removeSpaces($number));
        return ($len == 9 && Strings::startsWith("27", $number)) || ($len == 10 && Strings::startsWith("027", $number));
    }

    /**
     * Check if a number is possible a GB mobile number
     * @access public
     * @param string $number
     * @return boolean
     * */
    public static function isPossiblyGbMobileNumber($number)
    {
        $len = strlen(Strings::removeSpaces($number));
        return ($len == 10 && Strings::startsWith("7", $number)) || ($len == 11 && Strings::startsWith("07", $number));
    }

}
