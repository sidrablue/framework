<?php
namespace BlueSky\Framework\Util\Number;

/**
 * BPay reference utilities.
 */
class BPay
{

    /**
     * Generate a BPay reference number
     * @access public static
     * @param int $number
     * @return string
     */
    public static function generate($number)
    {
        $result = false;
        if (is_numeric($number) && $number > 0) {
            // Get the length of the seed number
            $length = strlen($number);
            $total = 0;
            // For each character in seed number, sum the character multiplied by its one based array position (instead of normal PHP zero based numbering)
            $number .= '';
            for ($i = 0; $i < $length; $i++) {
                $total += $number{$i} * ($i + 1);
            }

            // The check digit is the result of the sum total from above mod 10
            $checkDigit = fmod($total, 10);

            $result = $number . $checkDigit;
        }
        return $result;
    }

    /**
     * Validate that a given number conforms to the BPay format
     * @access public static
     * @param int $number
     * @return boolean
     * */
    public static function validate($number)
    {
        // TODO - Create a BPay validation function
    }

}
