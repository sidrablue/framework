<?php
namespace BlueSky\Framework\Util;

class Paging
{

    /**
     * Calculate the paging from a total, offset and results paging and return
     * an array in the format of:
     *    [ 'total' => '221', 'page' => '2', 'total_pages' => '12', 'start'= > 20, 'end' => 40 ]
     * @param int $total - the total number of results
     * @param int $offset - the offset used to retrieve the query results
     * @param int $limit - the number of results in each individual page
     * @access public
     * @return array
     * */
    public static function getPaging($total, $offset = 0, $limit = 20)
    {
        $paging = [];
        $offset == 0 ? $currentPage = 1 : $currentPage = floor($offset / $limit) + 1;
        $paging['total'] = intval($total ?? "0");
        $paging['page'] = $currentPage;
        $paging['total_pages'] = $total == 0 ? 1 : ceil($total / $limit);
        $paging['start'] = $total == 0 ? $offset : $offset + 1;
        $paging['end'] = min($offset + $limit, $total);
        $paging['limit'] = $limit;
        return $paging;
    }

}