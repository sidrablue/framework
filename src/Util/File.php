<?php
namespace BlueSky\Framework\Util;

/**
 * File manipulation utilities.
 *  */
class File
{

    private static $extensions = array(
        "application/pdf" => "pdf",
        "text/plain" => "txt",
        "text/html" => "html",
        "application/octet-stream" => "exe",
        "application/zip" => "zip",
        "application/vnd.ms-excel" => "xls",
        "application/vnd.ms-powerpoint" => "ppt",
        "image/gif" => "gif",
        "image/png" => "png",
        "image/jpg" => "jpg",
        "image/jpeg" => "jpg",
        "image/bmp" => "bmp",
        "application/msword" => "doc",
        "application/vnd.openxmlformats-officedocument.wordprocessingml.document" => "docx",
        "application/vnd.openxmlformats-officedocument.wordprocessingml.template" => "dotx",
        "application/vnd.ms-word.document.macroEnabled.12" => "docm",
        "application/vnd.ms-word.template.macroEnabled.12" => "dotm",
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" => "xlsx",
        "application/vnd.openxmlformats-officedocument.spreadsheetml.template" => "xltx",
        "application/vnd.ms-excel.sheet.macroEnabled.12" => "xlsm",
        "application/vnd.ms-excel.template.macroEnabled.12" => "xltm",
        "application/vnd.ms-excel.addin.macroEnabled.12" => "xlam",
        "application/vnd.ms-excel.sheet.binary.macroEnabled.12" => "xlsb",
        "application/vnd.openxmlformats-officedocument.presentationml.presentation" => "pptx",
        "application/vnd.openxmlformats-officedocument.presentationml.template" => "potx",
        "application/vnd.openxmlformats-officedocument.presentationml.slideshow" => "ppsx",
        "application/vnd.ms-powerpoint.addin.macroEnabled.12" => "ppam",
        "application/vnd.ms-powerpoint.presentation.macroEnabled.12" => "pptm",
        "application/vnd.ms-powerpoint.template.macroEnabled.12" => "potm",
        "application/vnd.ms-powerpoint.slideshow.macroEnabled.12" => "ppsm"

    );


    /**
     * Get an extension from a Mime Type
     * @param string $mimeType - the mime type to look for
     * @param string $defaultExtension - the string for which to check the format
     * @access public
     * @return string
     * */
    public static function getExtension($mimeType, $defaultExtension = 'txt')
    {
        $result = $defaultExtension;
        if (isset(self::$extensions[$mimeType])) {
            $result = self::$extensions[$mimeType];
        }
        return $result;
    }

    /**
     * Get the first CSV line of a file
     * @param string $fileName - the file name to look for
     * @access public
     * @return array|false
     * */
    public static function getFirstCsvLine($fileName)
    {
        ini_set('auto_detect_line_endings', true);
        $result = false;
        $handle = fopen($fileName, 'r+');
        if ($handle) {
            $result = fgetcsv($handle);
        }
        fclose($handle);
        return $result;
    }

    /**
     * Get the mime type of the specified file
     * @param string $path - the path of the file
     * @access public
     * @return string
     * */
    public static function getMimeType($path)
    {
        $mimeType = '';
        if (file_exists($path)) {
            $fInfo = finfo_open(FILEINFO_MIME_TYPE);
            $mimeType = finfo_file($fInfo, $path);
        }
        return $mimeType;
    }

    /**
     * Get the file type of the specified mime type
     * @param string $mimeType - the mime type of the file
     * @access public
     * @return string
     * */
    public static function getFileCategory($mimeType)
    {
        $fileType = '';
        if (strpos($mimeType, 'text/') !== false || strpos($mimeType, 'application/') !== false) {
            $fileType = 'document';
        } elseif (strpos($mimeType, 'image/') !== false) {
            $fileType = 'image';
        } elseif (strpos($mimeType, 'video/') !== false) {
            $fileType = 'video';
        }

        return $fileType;
    }


}
