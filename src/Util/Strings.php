<?php
namespace BlueSky\Framework\Util;

/**
 * String manipulation utilities.
 *  */
class Strings
{

    /**
     * Decode wildcards used in content with {{{place_holder}} formats.
     * These wildcards are html entity encoded at the point of save with a DomDocument
     * @param string $content - the content to decode
     * @access public
     * @return string
     * */
    public static function decodeTripleCurlyWildcards($content)
    {
        // Callback function to URL decode match
        $urldecode = function ($matches) {
            return urldecode($matches[0]);
        };
        // Turn DOMDocument into HTML string, then restore/urldecode placeholders
        $content = preg_replace_callback(
            '/' . urlencode('{{{') . '[\w\d\_]+' . urlencode('}}}') . '/',
            $urldecode,
            $content
        );
        return $content;
    }

    /**
     * Check if a string is a valid UUID format
     * @param string $string - the string for which to check the format
     * @access public
     * @return boolean
     * */
    public static function isUuid($string)
    {
        return preg_match('/^[0-9A-F]{8}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{12}$/i', $string);
    }

    /**
     * PHP equivalent to JS encodeURIComponent function
     * @access public static
     * @param string $stringToEncode
     * @return string
     * */
    public static function encodeURI($stringToEncode)
    {
        $revert = ['%21' => '!', '%2A' => '*', '%27' => "'", '%28' => '(', '%29' => ')'];
        return strtr(rawurlencode($stringToEncode), $revert);
    }

    /**
     * Check if a string is valid JSON
     * @param string $string
     * @return boolean
     * */
    public static function isJson($string)
    {
        $result = false;
        if (is_string($string)) {
            $val = json_decode($string, true);
            $result = (json_last_error() == JSON_ERROR_NONE) && is_array($val);
        }
        return $result;
    }

    /**
     * Wrap a string in HTML tags
     * @param string $string
     * @return string
     * */
    public static function createHtmlBody($string)
    {
        $dom = new \DOMDocument();
        @$dom->loadHTML($string);
        $body = $dom->getElementsByTagName('body');
        if (!($body && $body->item(0))) {
            $pNode = $dom->createElement("body");
            $pNode->textContent = $string;
            if ($dom->documentElement == null) {
                $string = '<html><body>' . $string . '</body></html>';
            } else {
                $dom->documentElement->appendChild($pNode);
                $string = $dom->saveHTML();
            }
        }
        return $string;
    }

    /**
     * Remove all white space from a string
     * @param string $string - the string for which to check the format
     * @access public
     * @return string
     * */
    public static function removeSpaces($string)
    {
        return preg_replace('/\s+/', '', $string);
    }

    /**
     * Generate a valid HTML id from a given string
     * @param string $string - the string for which to generate the ID for
     * @access public
     * @return boolean
     * */
    public static function generateHtmlId($string)
    {
        return strtolower(trim(preg_replace('/[^\da-z]/i', '_', $string),'_'));
    }

    /**
     * Generate a valid HTML id from a given string
     * @param string $string - the string for which to generate the ID for
     * @access public
     * @return boolean
     * */
    public static function generatePathId($string)
    {
        return strtolower(preg_replace('/[^\d\.a-z]/i', '_', $string));
    }

    /**
     * Check if a string starts with a given key
     * @access public static
     * @param string $needle - the potential start of the given string
     * @param string $haystack - the string to check with
     * @return boolean
     * */
    public static function startsWith($needle, $haystack)
    {
        $startsWith = false;
        if (strlen($haystack) > 0) {
            $pos = strpos($haystack, $needle . '');
            if ($pos === 0) {
                $startsWith = true;
            }
        }
        return $startsWith;
    }

    /**
     * Check if a string ends with a given key
     * @access public static
     * @param string $needle - the potential start of the given string
     * @param string $haystack - the string to check with
     * @return boolean
     * */
    public static function endsWith($needle, $haystack)
    {
        $endsWith = false;
        if (strlen($haystack) > 0) {
            $pos = strpos($haystack, "{$needle}");
            if ($pos === (strlen($haystack) - strlen($needle))) {
                $endsWith = true;
            }
        }
        return $endsWith;
    }

    /**
     * Generate a GUID
     * @access public static
     * @return string
     * */
    public static function guid()
    {
        return substr(uniqid(), 0, 8) . '-' . substr(uniqid(), 8, 4) . '-' . substr(uniqid(), 12, 4) . '-' . substr(
            uniqid(),
            16,
            4
        ) . '-' . substr(uniqid(), 20, 12);
    }


    /**
     * Check if a password is in the valid format.
     *
     * @access public static
     * @param $password
     * @param $length
     * @param $upper
     * @param $lower
     * @param $number
     * @param $special
     * @return bool
     */
    public static function validPassword($password, $length, $upper, $lower, $number, $special)
    {
        $lengthValid = strlen($password) >= $length;
        $upperValid = preg_match('/^(?=(.*[A-Z]){' . $upper . '}).+$/', $password);
        $lowerValid = preg_match('/^(?=(.*[a-z]){' . $lower . '}).+$/', $password);
        $numberValid = preg_match('/^(?=(.*[0-9]){' . $number . '}).+$/', $password);
        $specialValid = preg_match('/[\W_]{' . $special . ',}/', $password);

        $result = false;

        if ($lengthValid && $upperValid && $lowerValid && $numberValid && $specialValid) {
            $result = true;
        }

        return $result;
    }

    /**
     * Check if a string is for a valid date
     * @access public static
     * @param string $dateString
     * @return boolean
     * */
    public static function isValidDate($dateString)
    {
        return (bool)strtotime($dateString);
    }

    /**
     * Generate a random string
     * @param int $length
     * @return string
     * */
    public static function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}
