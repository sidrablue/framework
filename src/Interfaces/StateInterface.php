<?php
namespace BlueSky\Framework\Interfaces;

use BlueSky\Framework\State\Base as BaseState;

/**
 * Interface StateInterface
 *
 * @package BlueSky\Framework\Framework\Interfaces
 * @author Amel Holic <amel@sidrablue.com.au>
 */
interface StateInterface
{
    /**
     * Get the state object
     * @access public
     * @return BaseState
     */
    public function getState();

    /**
     * Set the state object
     * @access public
     * @param BaseState $state
     * @return void
     */
    public function setState($state);

}
