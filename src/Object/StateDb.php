<?php
namespace BlueSky\Framework\Object;

use BlueSky\Framework\State\Base;
use BlueSky\Framework\State\Web;

/**
 * Base class for all state based objects
 */
class StateDb
{

    /**
     * @var Web $requestState
     * @access private
     * The model state
     * */
    private $requestState;

    /**
     * @var \Doctrine\DBAL\Connection $readDb - the custom read DB if one is defined
     */
    protected $readDb;

    /**
     * @var \Doctrine\DBAL\Connection $writeDb - the custom write DB if one is defined
     */
    protected $writeDb;

    /**
     * Base constructor to define the state object
     * @access public
     * @param Base $state
     * */
    public function __construct(Base $state = null)
    {
        $this->setState($state);
    }

    /**
     * Get the state object
     * @access public
     * @return Web
     * */
    public function getState()
    {
        return $this->requestState;
    }

    protected function setState(Base $state = null)
    {
        $this->requestState = $state;
    }


    /**
     * Manually overwrite the read DB
     * @access public
     * @param \Doctrine\DBAL\Connection $readDb - the custom read DB if one is defined
     * @return void
     * */
    public function setReadDb($readDb = null)
    {
        $this->readDb = $readDb;
    }

    /**
     * Manually overwrite the write DB
     * @access public
     * @param \Doctrine\DBAL\Connection $writeDb - the custom write DB if one is defined
     * @return void
     * */
    public function setWriteDb($writeDb = null)
    {
        $this->writeDb = $writeDb;
    }

    /**
     * Manually overwrite the read and write DB
     * @access public
     * @param \Doctrine\DBAL\Connection $db - the custom DB if one is defined
     * @return void
     * */
    public function setDb($db = null)
    {
        $this->readDb = $db;
        $this->writeDb = $db;
    }

    /**
     * Define the read DB function so that it can be overwritten
     * @access protected
     * @return \Doctrine\DBAL\Connection
     * */
    protected function getReadDb()
    {
        $result = $this->readDb;
        if(!$result) {
            $result = $this->getState()->getReadDb();
        }
        return $result;
    }

    /**
     * Define the write DB function so that it can be overwritten
     * @access protected
     * @return \Doctrine\DBAL\Connection
     * */
    protected function getWriteDb()
    {
        $result = $this->writeDb;
        if(!$result) {
            $result = $this->getState()->getWriteDb();
        }
        return $result;
    }

}
