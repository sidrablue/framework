<?php
/**
 * Copyright (c) BlueSky Digital Labs Pty Ltd
 */

declare(strict_types=1);
namespace BlueSky\Framework\Object\Container;

abstract class BaseContainer implements \JsonSerializable
{

    public function asArray(): array {
        $output = get_object_vars($this);
        $output['_class'] = str_replace('\\', '.', get_called_class());
        array_walk_recursive($output, function (&$item) {
            if ($item instanceof BaseContainer) {
                $item = $item->asArray();
            }
        });
        return $output;
    }

    public static function fromArray(array $data): ?BaseContainer {
        $output = null;
        $class = str_replace('.', '\\', $data['_class']);
        if (class_exists($class)) {
            $class = new $class();

            foreach ($data as $key => $datum) {
                if (property_exists($class, $key)) {
                    if (!is_array($datum)) {
                        $class->{$key} = $datum;
                    } else {
                        $input = [];
                        foreach ($datum as $k => $v) {
                            $c = str_replace('.', '\\', $v['_class']);
                            if (class_exists($c)) {
                                $input[$k] = $c::fromArray($v);
                            }
                        }
                        $class->{$key} = $input;
                    }
                }
            }
            $output = $class;
        }
        return $output;
    }

    public function jsonSerialize()
    {
        return $this->asArray();
    }

}
