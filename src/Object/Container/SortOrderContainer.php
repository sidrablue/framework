<?php
declare(strict_types = 1);
namespace BlueSky\Framework\Object\Container;

class SortOrderContainer extends BaseContainer
{
    public const SORT_TYPE_ASC = 'ASC';
    public const SORT_TYPE_DESC = 'DESC';

    /** @var string $columnName */
    public $columnName;

    /** @var string $sortType */
    public $sortType;

    /**
     * SortOrderContainer constructor.
     * @param string $columnName
     * @param string $sortType
     */
    public function __construct(string $columnName, string $sortType)
    {
        $this->columnName = $columnName;
        $this->sortType = $sortType;
    }

}
