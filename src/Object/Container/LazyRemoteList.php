<?php
/**
 * Copyright (c) BlueSky Digital Labs Pty Ltd
 */

declare(strict_types=1);
namespace BlueSky\Framework\Object\Container;

use ArrayAccess;
use Doctrine\DBAL\Connection;
use Iterator;
use PDO;
use BlueSky\Framework\State\Base as BaseState;
use BlueSky\Framework\Entity\Schema\Entity as SchemaEntity;
use BlueSky\Framework\Object\Entity\Base as BaseEntity;

class LazyRemoteList implements Iterator, ArrayAccess
{
    public const DEFAULT_PAGING_LIMIT = 20;
    private const UNINITIALISED = -2;

    private $state;
    private $baseIds;
    private $schemaEntity;
    private $sortBy;
    private $perPage;

    private $baseIdsCount;
    private $tableName;

    private $slidingWindow = [];
    private $currentIndex = self::UNINITIALISED;
    private $lowerLimitIndex = 0;
    private $currentPage = 0;

    /**
     * LazyRemoteList constructor.
     * @param BaseState $state
     * @param SchemaEntity $schemaEntity
     * @param int[] $baseIds
     * @param array $sortBy
     * @param int $perPage
     */
    public function __construct(BaseState $state, SchemaEntity $schemaEntity, array $baseIds = [], array $sortBy = [], int $perPage = self::DEFAULT_PAGING_LIMIT)
    {
        $this->state = $state;
        $this->schemaEntity = $schemaEntity;
        $this->baseIds = $baseIds;
        $this->sortBy = $sortBy;
        $this->perPage = $perPage;

        $this->baseIdsCount = count($baseIds);
        $this->tableName = $this->schemaEntity->getEntityTableName();
    }

    public function hasNextPage(): bool { return $this->lowerLimitIndex + $this->perPage < $this->baseIdsCount; }

    public function hasPreviousPage(): bool { return $this->lowerLimitIndex !== 0; }

    public function getCurrentPageMetaData(): array { return $this->baseIds ?? []; }

    public function getCurrentPagePagingData(): PageContainer
    {
        return PageContainer::getPaging($this->baseIdsCount, max($this->currentIndex, 0), $this->perPage);
    }

    public function getCurrentPageData(bool $transformToEntity = true): array
    {
        if (!$transformToEntity) {
            $output = $this->loadPage(max($this->currentIndex, 0), false);
        } else {
            if ($this->currentIndex === self::UNINITIALISED) {
                $this->loadInitialPage();
            }
            $output = $this->slidingWindow;
        }
        return $output;
    }

    public function getNextPage(): ?array
    {
        $output = null;
        if ($this->currentIndex === self::UNINITIALISED) {
            $output = $this->loadInitialPage();
        } elseif ($this->hasNextPage()) {
            $this->lowerLimitIndex += $this->perPage;
            $output = $this->loadPage($this->lowerLimitIndex);
            $this->slidingWindow = $output;
            $this->currentPage = (int)($this->lowerLimitIndex / $this->perPage);
            $this->currentIndex = 0;
        }
        return $output;
    }

    public function getPreviousPage(): ?array
    {
        $output = null;
        if ($this->currentIndex === self::UNINITIALISED) {
            $output = $this->loadInitialPage();
        } elseif ($this->hasPreviousPage()) {
            $this->lowerLimitIndex -= $this->perPage;
            $output = $this->loadPage($this->lowerLimitIndex);
            $this->slidingWindow = $output;
            $this->currentPage = (int)($this->lowerLimitIndex / $this->perPage);
            $this->currentIndex = 0;
        }
        return $output;
    }

    private function goToPage(int $page): ?array {
        $output = null;
        $index = $page * $this->perPage;
        if ($index < $this->baseIdsCount) {
            $output = $this->loadPage($index);
            $this->currentIndex = 0;
            $this->currentPage = $page;
            $this->lowerLimitIndex = $index;
            $this->slidingWindow = $output;
        }

        return $output;
    }

    public function getPerPage(): int { return $this->perPage; }

    private function loadPage(int $startingIndex, bool $transformToEntity = true): array
    {
        $q = $this->state->getReadDb()->createQueryBuilder()
            ->select('*')
            ->from($this->tableName)
            ->andWhere('lote_deleted is null')
            ->andWhere('id in (:ids)')
            ->setParameter('ids', $this->baseIds, Connection::PARAM_INT_ARRAY)
            ->setMaxResults($this->perPage)
            ->setFirstResult($startingIndex);
        $results = $q->execute()->fetchAll(PDO::FETCH_ASSOC);
        if ($transformToEntity) {
            $results = array_map(function ($item) {
                $e = $this->schemaEntity;
                if ($e->is_virtual) {
                    $output = $this->state->getEntityManager()->createInstance($e);
                    $output->setData($item);
                } else {
                    $className = $e->class_name;
                    /** @var BaseEntity $output */
                    $output = new $className($this->state);
                    $output->setIgnoreModifications(true);
                    $output->setData($item);
                    $output->setIgnoreModifications(false);
                }
                return $output;
            }, $results);
        }
        return $results;
    }

    private function loadInitialPage(): array {
        $results = $this->loadPage(0);
        $this->currentIndex = 0;
        $this->slidingWindow = $results;
        return $results;
    }


    /**
     * Return the current element
     * @link https://php.net/manual/en/iterator.current.php
     * @return mixed Can return any type.
     * @since 5.0.0
     */
    public function current()
    {
        if ($this->currentIndex === self::UNINITIALISED) {
            $this->loadInitialPage();
        }
        return $this->slidingWindow[$this->currentIndex];
    }

    /**
     * Move forward to next element
     * @link https://php.net/manual/en/iterator.next.php
     * @return void Any returned value is ignored.
     * @since 5.0.0
     */
    public function next()
    {
        $this->currentIndex++;
        if ($this->currentIndex === $this->perPage) {
            if ($this->hasNextPage()) {
                $this->getNextPage();
                $this->currentIndex = 0;
            }
        }
    }

    /**
     * Return the key of the current element
     * @link https://php.net/manual/en/iterator.key.php
     * @return mixed scalar on success, or null on failure.
     * @since 5.0.0
     */
    public function key()
    {
        if ($this->currentIndex === self::UNINITIALISED) {
            $this->loadInitialPage();
        }
        return $this->currentIndex;
    }

    /**
     * Checks if current position is valid
     * @link https://php.net/manual/en/iterator.valid.php
     * @return boolean The return value will be casted to boolean and then evaluated.
     * Returns true on success or false on failure.
     * @since 5.0.0
     */
    public function valid(): bool
    {
        $output = true;
        if ($this->currentIndex + $this->lowerLimitIndex === $this->baseIdsCount) {
            $output = false;
        }
        return $output;
    }

    /**
     * Rewind the Iterator to the first element
     * @link https://php.net/manual/en/iterator.rewind.php
     * @return void Any returned value is ignored.
     * @since 5.0.0
     */
    public function rewind()
    {
        $this->loadInitialPage();
    }

    /**
     * Whether a offset exists
     * @link https://php.net/manual/en/arrayaccess.offsetexists.php
     * @param mixed $offset <p>
     * An offset to check for.
     * </p>
     * @return boolean true on success or false on failure.
     * </p>
     * <p>
     * The return value will be casted to boolean if non-boolean was returned.
     * @since 5.0.0
     */
    public function offsetExists($offset): bool
    {
        return $offset >= 0 && $offset < $this->baseIdsCount;
    }

    /**
     * Offset to retrieve
     * @link https://php.net/manual/en/arrayaccess.offsetget.php
     * @param mixed $offset <p>
     * The offset to retrieve.
     * </p>
     * @return mixed Can return all value types.
     * @since 5.0.0
     */
    public function offsetGet($offset)
    {
        $output = null;
        if ($this->offsetExists($offset)) {
            $page = (int)($offset / $this->perPage);
            if ($page !== $this->currentPage) {
                $this->goToPage($page);
            }
            $delta = $offset % $this->perPage;
            $output = $this->slidingWindow[$delta];
        }
        return $output;
    }

    /**
     * Offset to set
     * @link https://php.net/manual/en/arrayaccess.offsetset.php
     * @param mixed $offset <p>
     * The offset to assign the value to.
     * </p>
     * @param mixed $value <p>
     * The value to set.
     * </p>
     * @return void
     * @since 5.0.0
     */
    public function offsetSet($offset, $value)
    {
        if ($this->offsetExists($offset)) {
            $page = (int)($offset / $this->perPage);
            if ($page !== $this->currentPage) {
                $this->goToPage($page);
            }
            $delta = $offset % $this->perPage;
            $this->slidingWindow[$delta] = $value;
        }
    }

    /**
     * Offset to unset
     * @link https://php.net/manual/en/arrayaccess.offsetunset.php
     * @param mixed $offset <p>
     * The offset to unset.
     * </p>
     * @return void
     * @since 5.0.0
     */
    public function offsetUnset($offset)
    {
        if ($this->offsetExists($offset)) {
            $page = (int)($offset / $this->perPage);
            if ($page !== $this->currentPage) {
                $this->goToPage($page);
            }
            $delta = $offset % $this->perPage;
            unset($this->slidingWindow[$delta]);
        }
    }
}