<?php
/**
 * Copyright (c) BlueSky Digital Labs Pty Ltd
 */

declare(strict_types=1);
namespace BlueSky\Framework\Object\Container;

class PageContainer extends BaseContainer
{

    /** @var int $total */
    public $total;

    /** @var int $total */
    public $page;

    /** @var int $total */
    public $totalPages;

    /** @var int $total */
    public $start;

    /** @var int $total */
    public $end;

    /** @var int $total */
    public $limit;

    /**
     * Calculate the paging from a total, offset and results paging and return
     * an array in the format of:
     *    [ 'total' => '221', 'page' => '2', 'total_pages' => '12', 'start'= > 20, 'end' => 40 ]
     * @param int $total - the total number of results
     * @param int $offset - the offset used to retrieve the query results
     * @param int $limit - the number of results in each individual page
     * @access public
     * @return PageContainer
     * */
    public static function getPaging(int $total, int $offset = 0, int $limit = 20): PageContainer
    {
        $paging = new PageContainer();
        $offset === 0 ? $currentPage = 1 : $currentPage = floor($offset / $limit) + 1;
        $paging->total = $total;
        $paging->page = $currentPage;
        $paging->totalPages = $total === 0 ? 1 : ceil($total / $limit);
        $paging->start = $total === 0 ? $offset : $offset + 1;
        $paging->end = min($offset + $limit, $total);
        $paging->limit = $limit;
        return $paging;
    }

    /**
     * @param int $total
     * @return PageContainer
     */
    public function setTotal(int $total): PageContainer
    {
        $this->total = $total;
        return $this;
    }

    /**
     * @param int $page
     * @return PageContainer
     */
    public function setPage(int $page): PageContainer
    {
        $this->page = $page;
        return $this;
    }

    /**
     * @param int $totalPages
     * @return PageContainer
     */
    public function setTotalPages(int $totalPages): PageContainer
    {
        $this->totalPages = $totalPages;
        return $this;
    }

    /**
     * @param int $start
     * @return PageContainer
     */
    public function setStart(int $start): PageContainer
    {
        $this->start = $start;
        return $this;
    }

    /**
     * @param int $end
     * @return PageContainer
     */
    public function setEnd(int $end): PageContainer
    {
        $this->end = $end;
        return $this;
    }

    /**
     * @param int $limit
     * @return PageContainer
     */
    public function setLimit(int $limit): PageContainer
    {
        $this->limit = $limit;
        return $this;
    }

}