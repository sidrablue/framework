<?php
/**
 * Copyright (c) BlueSky Digital Labs Pty Ltd
 */

declare(strict_types=1);
namespace BlueSky\Framework\Object\Container;

class QueryListContainer extends BaseContainer
{

    /** @var array $rows */
    public $rows;

    /** @var PageContainer $paging */
    public $paging;

    /** @var int $total */
    public $total;

    /**
     * @param array $rows
     * @return QueryListContainer
     */
    public function setRows(array $rows): QueryListContainer
    {
        $this->rows = $rows;
        return $this;
    }

    /**
     * @param PageContainer $paging
     * @return QueryListContainer
     */
    public function setPaging(PageContainer $paging): QueryListContainer
    {
        $this->paging = $paging;
        return $this;
    }

    /**
     * @param int $total
     * @return QueryListContainer
     */
    public function setTotal(int $total): QueryListContainer
    {
        $this->total = $total;
        return $this;
    }


}