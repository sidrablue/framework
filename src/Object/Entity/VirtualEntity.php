<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 * Updated: 19 March 2018 at 06:54 PM AWST
 * Updated By: Parham Bakhtiari <parham@sidrablue.com.au>
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Object\Entity;

use BlueSky\Framework\Entity\Schema\Entity as SchemaEntity;
use BlueSky\Framework\Entity\Schema\Field as FieldEntity;
use BlueSky\Framework\Entity\Schema\Field as SchemaField;
use BlueSky\Framework\Object\Entity\VirtualTypes\Factory as VirtualDataFactory;

abstract class VirtualEntity implements \JsonSerializable, \ArrayAccess
{
    /**
     * @var int $id
     * The id of the table row
     * @dbColumn id
     * @fieldType integer
     * @codeType integer
     * @dbOptions autoincrement = true
     * */
    public $id;

    /**
     * @var string $lote_created - the time that this row was created. This is a \DateTime compatible 'c' format value
     * @dbColumn lote_created
     * @fieldType datetime
     * @codeType datetime_immutable
     * @dbOptions notnull = false
     */
    public $lote_created;

    /**
     * @var string $lote_updated - the last time that this row was updated. This is a \DateTime compatible 'c' format value
     * @dbColumn lote_updated
     * @fieldType datetime
     * @codeType datetime_immutable
     * @dbOptions notnull = false
     */
    public $lote_updated;

    /**
     * @var string $lote_deleted - the time that this row was deleted. This is a \DateTime compatible 'c' format value
     * @dbColumn lote_deleted
     * @fieldType datetime
     * @dbOptions notnull = false
     * @codeType datetime_immutable
     * @dbIndex true
     */
    public $lote_deleted;

    /**
     * @var int $lote_deleted_by - the user that deleted this object
     * @dbColumn lote_deleted_by
     * @fieldType integer
     * @codeType integer
     * @dbOptions notnull = false
     */
    public $lote_deleted_by;

    /**
     * @var string $lote_author_id - the ID of the user who created this object
     * @dbColumn lote_author_id
     * @fieldType integer
     * @codeType integer
     * @dbOptions notnull = false
     */
    public $lote_author_id;

    /**
     * @var int $lote_access - the access for this entity
     * @dbColumn lote_access
     * @fieldType integer
     * @codeType integer
     * @dbOptions notnull = false, default = 0, length = 4
     */
    public $lote_access;

    /** @var SchemaEntity $schema  */
    private $schema;

    /**
     * VirtualEntity constructor.
     *
     * The schema is used in preference of the class annotations
     *
     * @param SchemaEntity $schema
     * @param array|null $data
     */
    public function __construct(SchemaEntity $schema, array $data = null)
    {
        $this->schema = $schema;
        if (!empty($data)) {
            try {
                $this->setData($data);
            } catch (\Exception $exception) {}
        }
    }

    protected function getEntity(): SchemaEntity { return $this->schema; }

    /**
     * Gets all the valid properties of the class as an array
     *
     * @return array
     */
    public function toArray(): array { return $this->getData(true); }

    /**
     * Sets the data of every valid property in the class
     * It will also decode all the properties after setting them
     *
     * @param array $data
     */
    public function setData(array $data): void
    {
        $entity = $this->getEntity();
        foreach ($data as $k => $v) {
            $field = $entity->getEntityField($k) ?? $entity->getLoteFields()[$k] ?? null;
            if (!is_null($field)) {
                $this->{$k} = $this->decodePropertyValue($v, $field);
            } else {
                $this->{$k} = null;
            }
        }
    }

    /**
     * Used to format the output of var_dump calls
     *
     * @return array
     */
    public function __debugInfo() { return $this->getData(); }

    /**
     * This will return all valid properties from the class
     * All properties should be in their correct type (ie. a bool property being in a bool type)
     *
     * All foreign key references are returned as well
     * For example, if the class has a property called car_id (int), and car was loaded in during the load stage then
     * the property car (Car) is also returned as valid class property
     *
     * @param bool $encodeData - this flag will determine if the output array should be encoded in string or not
     * @param bool $displayable - this flag will determine if the output array should be encoded in the displayable format
     * @return array
     */
    public function getData(bool $encodeData = false, bool $displayable = false): array
    {
        $fields = array_merge($this->getEntity()->getEntityFields(), $this->getEntity()->getLoteFields());
        $output = [];
        foreach ($fields as $field) {
            /** @var FieldEntity $field */
            $fieldReference = $field->reference;
            $outputValue = $this->{$fieldReference};
            if ($encodeData) {
                $output[$fieldReference] = $this->encodePropertyValue($outputValue, $field, $displayable);
            } else {
                $output[$fieldReference] = $outputValue;
                if ($field->isForeignKey()) {
                    $this->loadForeignKey($fieldReference, $output);
                }
            }
        }

        return $output;
    }

    /**
     * This function will attach the foreign key value to the virtual class data output
     *
     * @param string $key
     * @param array $container - The current data set
     */
    private function loadForeignKey(string $key, array &$container): void
    {
        $objKey = substr($key, 0, -3); /// Remove the _id from the end of the key name
        if (isset($this->{$objKey})) {
            $obj = $this->{$objKey};
            if ($obj instanceof Base) {
                $container[$objKey] = $this->{$objKey}->getData();
            } elseif ($obj instanceof VirtualEntity) {
                $container[$objKey] = $this->{$objKey}->getData();
            }
        }
    }

    /**
     * This function will decode a single field from string to its represented object based on its type
     *
     * @param $input
     * @param SchemaField $field
     * @return mixed
     */
    protected function decodePropertyValue($input, SchemaField $field)
    {
        $output = null;
        if (gettype($input) === 'string') {
            $output = VirtualDataFactory::createInstance($field->field_type, $input)->decode();
        } elseif (VirtualDataFactory::createInstance($field->field_type, $input)->doesTypesMatch()) {
            $output = $input;
        } elseif (!is_null($input)) { /// todo: this will need some refactoring
            try {
                if ($field->reference === 'lote_created') {
                    $output = new \DateTimeImmutable();
                } elseif ($field->reference === 'lote_updated') {
                    $output = new \DateTime();
                }
            } catch (\Exception $e) { }
        }
        return $output;
    }

    /**
     * This function will encode a value to a string representation of that type
     *
     * @see encodePropertyValues
     *
     * @param mixed $value
     * @param SchemaField $field
     * @param bool $displayable
     * @return string|null
     */
    protected function encodePropertyValue($value, SchemaField $field, bool $displayable): ?string
    {
        $valueEncoded = $value;
        if (!is_null($value)) {
            $vdf = VirtualDataFactory::createInstance($field->field_type, $value);
            $valueEncoded = $displayable ? $vdf->displayFormat() : $vdf->encode();
        }
        return $valueEncoded;
    }

    /** @return string */
    abstract static function getTableName();

    /** @return string */
    abstract static function getObjectRef();

    /** @return string */
    abstract static function getObjectName();

    /**
     * This returns the key used in the entity parent to child link
     * Eg. In User, if a Car custom field is added then $user->_car will point to this object
     * @return string
     */
    abstract static function getEntityLinkKey();

    /**
     * This will set the current object as deleted
     * It will not save the object to the cache or db
     */
    public function softDelete(): void
    {
        try {
            $this->lote_deleted = new \DateTimeImmutable();
        } catch (\Exception $e) { }
    }

    /**
     * This method is executed through reflection, so it doesn't have any return parameters
     *
     * @Note: On error, throw an exception
     *
     * @throws \Exception
     */
    protected function beforeCreate(): void
    {
        $this->lote_created = new \DateTimeImmutable();
        $this->lote_updated = new \DateTime();
    }

    /**
     * This method is executed through reflection, so it doesn't have any return parameters
     * On error, throw an exception
     * @throws \Exception
     */
    protected function afterCreate(): void { }

    /**
     * This method is executed through reflection, so it doesn't have any return parameters
     *
     * @Note: On error, throw an exception
     *
     * @throws \Exception
     */
    protected function beforeUpdate(): void { $this->lote_updated = new \DateTime(); }

    /**
     * This method is executed through reflection, so it doesn't have any return parameters
     *
     * @Note: On error, throw an exception
     *
     * @throws \Exception
     */
    protected function afterUpdate(): void { }

    protected function buildSelfSchema()
    {
        $schema = new SchemaEntity();
        $loteFields = $schema->getLoteFields();
        $schema->setEntityFields($loteFields);
        return $schema;
    }

    public function getDataAsApiResponse(int $withFlag = FieldEntity::FLAG_EXPORTABLE)
    {
        $output = [];
        foreach ($this->schema->getEntityFields() as $key => $field) {
            if ($field->hasFlag($withFlag)) {
                $value = $this->{$field->reference};
                if ($value !== null) {
                    $value = VirtualTypesFactory::createInstance($field->field_type,
                        $this->{$field->reference})->apiEncode();
                }
                $apiKey = str_replace('_', '', lcfirst(ucwords($key, '_')));
                $output[$apiKey] = $value;
            }
        }
        return $output;
    }

    public function offsetExists($offset)
    {
        return $this->schema->getEntityField($offset) || isset($this->schema->getLoteFields()[$offset]);
    }

    public function offsetGet($offset)
    {
        $output = null;
        if ($this->offsetExists($offset) && $this->{$offset} !== null) {
            $vdf = VirtualDataFactory::createInstance($this->getEntity()->getEntityField($offset)->field_type, $this->{$offset});
            $output = $vdf->displayFormat();
        }
        return $output;
    }

    public function offsetUnset($offset)
    {
        if ($this->offsetExists($offset)) {
            unset($this->{$offset});
        }
    }

    public function offsetSet($offset, $value)
    {
        if ($this->offsetExists($offset)) {
            $this->{$offset} = $value;
        }
    }

    public function jsonSerialize() { return $this->getData(); }

    public function equals(VirtualEntity $entity): bool
    {
        $output = false;
        if ($this::getObjectRef() === $entity::getObjectRef()) {
            foreach ($this->schema->getEntityFields() as $field) {
                $fieldRef = $field->reference;
                if ($field->db_type === 'json') {
                    $output = $this->{$fieldRef} == $entity->{$fieldRef};
                } else {
                    $output = $this->{$fieldRef} === $entity->{$fieldRef};
                }
                if (!$output) {
                    break;
                }
            }
        }
        return $output;
    }

}
