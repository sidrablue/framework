<?php
/**
 * Copyright (c) BlueSky Digital Labs Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Object\Entity;

use BlueSky\Framework\Object\Entity\VirtualTypes\Type\StdDateTime;
use DateTimeImmutable;
use Doctrine\DBAL\Query\QueryBuilder;
use BlueSky\Framework\Entity\Audit\ObjectLog;
use BlueSky\Framework\Entity\User\User;
use BlueSky\Framework\Entity\Schema\Field as FieldEntity;
use BlueSky\Framework\Object\Container\SortOrderContainer;
use BlueSky\Framework\State\Web;
use BlueSky\Framework\Util\Time;
use BlueSky\Framework\Object\StateDb as StateDbBase;
use zpt\anno\Annotations;
use BlueSky\Framework\Model\Schema\Entity as EntityModel;
use BlueSky\Framework\Entity\Schema\Entity as SchemaEntity;
use BlueSky\Framework\Object\Entity\VirtualTypes\Factory as VirtualTypesFactory;

/**
 * Base class for all entities
 */
abstract class Base extends StateDbBase implements \JsonSerializable, \ArrayAccess
{

    /**
     * The id of the table row
     * @fieldType primary_key_int
     * @flags FLAG_EXPORTABLE
     * @dbOptions autoincrement = true
     * @var int $id
     * */
    public $id;

    /**
     * @fieldType datetime_immutable
     * @dbOptions notnull = false
     * @flags FLAG_EXPORTABLE
     * @var DateTimeImmutable $lote_created - the time that this row was created. This is a \DateTime compatible 'c' format value
     */
    public $lote_created;

    /**
     * @fieldType datetime
     * @dbOptions notnull = false
     * @flags FLAG_EXPORTABLE
     * @var DateTimeImmutable $lote_updated - the last time that this row was updated. This is a \DateTime compatible 'c' format value
     */
    public $lote_updated;

    /**
     * @fieldType datetime_immutable
     * @dbOptions notnull = false
     * @dbIndex true
     * @var DateTimeImmutable $lote_deleted - the time that this row was deleted. This is a \DateTime compatible 'c' format value
     */
    public $lote_deleted;

    /**
     * @fieldType fkey
     * @foreignEntityClass BlueSky\Framework\Entity\User\User
     * @dbOptions notnull = false
     * @var int $lote_deleted_by - the user that deleted this object
     */
    public $lote_deleted_by;

    /**
     * @fieldType fkey
     * @foreignEntityClass BlueSky\Framework\Entity\User\User
     * @dbOptions notnull = false
     * @var string $lote_author_id - the ID of the user who created this object
     */
    public $lote_author_id;

    /**
     * @fieldType integer
     * @dbOptions notnull = false, default = 0, length = 4
     * @var int $lote_access - the access for this entity
     */
    public $lote_access;

    /** @var array $entityData */
    public $entityData = [];

    /** @var ObjectLog $auditObjectLog */
    public $auditObjectLog;

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName;

    public const TABLE_NAME = '';

    /** @var boolean $isAudited */
    protected $isAudited = false;

    /**
     * @var boolean $ignoreModifications
     * True if any modifications to this entity are to be ignored, and therefore will not result in "dirty" fields
     * */
    protected $ignoreModifications = false;

    /**
     * The properties of this entity which are defined for programming purposes, but are unset at the point of
     * construction in order to track changes
     * */
    protected $entityProperties = [];

    /**
     * Properties that have been modified for this entity
     * */
    protected $modifiedProperties = [];

    /** @var SchemaEntity|null $schema  */
    protected $schema = null;

    /**
     * @var bool $shouldFreshLoad - if true, the cache should be skipped and a new instance should be loaded from the db
     * FIXME: This can be edited to check if the cache is up to date or not first.
     *      If not, then load it from the db, if it is, then use the cache value
     *      This was added in since the logged in user is loaded without any relationships
     *      but if you need relationships added later, the old loaded cache hit is used
     */
    protected $shouldFreshLoad = false;

    /**
     * @param Web $state
     * @param int|array $data
     * */
    public function __construct($state = null, $data = null)
    {
        parent::__construct($state);
        $this->beforeCreate();
        $this->setupEntityProperties();
        if (is_numeric($data) && $data > 0) {
            $this->load($data);
        } elseif (is_array($data)) {
            $this->setData($data);
        }
        $this->afterCreate();
    }

    /**
     * @param mixed $tableName
     * @deprecated - we dont use dynamic entities anymore. This function is redundant now
     */
    public function setTableName(string $tableName) { $this->tableName = $tableName; }

    /**
     * @return string
     */
    public static function getTableName(): string { return static::TABLE_NAME ?? get_called_class(); }

    public static function getObjectName(): string { return EntityModel::createNameFromTableName(static::TABLE_NAME); }

    public static function getObjectRef(): string
    {
        $entityModel = new EntityModel();
        return $entityModel->createReferenceFromTableName(static::TABLE_NAME);
    }

    /**
     * Setup the entity properties for the purpose of tracking changes via magic methods, but still having them
     * available for programming purposes
     * @access protected
     * @return void
     * */
    protected function setupEntityProperties()
    {
        $entityProperties = $this->getSchema();
        foreach ($entityProperties->getEntityFields() as $field) {
            if ($field->reference != 'id') {
                $this->entityProperties[$field->reference] = $field->reference;
                unset($this->{$field->reference});
            }
        }
    }

    /**
     * Get all class properties which should map to database fields
     * @deprecated - use the schema object directly (this is adding an unnecessary O(n) complexity)
     * @return array
     */
    protected function getFields()
    {
        $result = [];
        $fields = $this->getSchema()->getEntityFields();
        /** @var FieldEntity $field */
        foreach ($fields as $field) {
            $result[] = $field->reference;
        }
        return $result;
    }

    public function getSchema(): SchemaEntity
    {
        if (is_null($this->schema)) {
            $em = new EntityModel($this->getState());
            if ($this->getState() === null || (defined('LOTE_DEBUG') && LOTE_DEBUG) || (defined('LOTE_DB_OPERATION') && LOTE_DB_OPERATION)) {
                $this->schema = $em->buildFromClass($this);
            } else {
                $this->schema = $this->getState()->getSchema()->getEntityByReference(static::getObjectRef());
                if (is_null($this->schema)) {
                    $this->schema = $em->buildFromClass($this);
                }
            }
        }
        return $this->schema;
    }

    public function setIgnoreModifications(bool $ignoreModifications): void  { $this->ignoreModifications = $ignoreModifications; }

    public function getAnnotatedClass()
    {
        static $result = null;
        if (is_null($result)) {
            $result = [];
            $classReflector = new \ReflectionClass(get_called_class());
            $anno = new Annotations($classReflector);
            $result['nameRaw'] = ltrim(preg_replace('/(?<!\ )[A-Z]/', ' $0', $classReflector->getShortName()));
            $result['name'] = $anno->hasAnnotation('name') ? $anno->offsetGet('name') : $result['nameRaw'];
            $result['hidden'] = $anno->offsetExists('hidden') ? $anno->offsetGet('hidden') : true;
        }
        return $result;
    }

    public function getAnnotatedColumns()
    {
        static $result = null;
        if (is_null($result)) {
            $result = [];
            $classReflector = new \ReflectionClass(get_called_class());
            foreach ($classReflector->getProperties() as $methodReflector) {
                $anno = new Annotations($methodReflector);
                if ($anno->hasAnnotation('fieldtype')) {
                    $prop = [];
                    $prop['opts'] = [];
                    $prop['column'] = $anno->hasAnnotation('dbcolumn') ? $anno->offsetGet('dbcolumn') : $methodReflector->getName();
                    $prop['field_type'] = $anno->offsetGet("fieldtype");
                    if ($anno->hasAnnotation("dboptions")) {
                        $prop['opts'] = $anno->offsetGet("dboptions");
                    }
                    if ($anno->hasAnnotation("dbcomment")) {
                        $prop['comment'] = $anno->offsetGet("dbcomment");
                    }
                    if ($anno->hasAnnotation("dbindex") && $anno->offsetGet("dbindex") === true) {
                        $prop['dbindex'] = true;
                    }
                    if ($anno->hasAnnotation('customName')) {
                        $prop['custom_name'] = $anno->offsetGet('customName');
                    }

                    $flags = $anno->hasAnnotation('flags') ? $anno->offsetGet('flags') : 0;
                    if (is_string($flags)) { $flags = [$flags]; }
                    if (is_array($flags)) {
                        $totalFlags = 0;
                        foreach ($flags as $flag) {
                            $value = FieldEntity::getKeyValue($flag);
                            if ($value === null) {
                                dump('contains an invalid flag ' . $flag . ' in ' . get_called_class() . ' -> ' . $prop['column']);
                            } else {
                                $totalFlags |= $value;
                            }
                        }
                        $flags = $totalFlags;
                    }
                    $prop['flags'] = $flags;
                    $prop['system_hidden'] = $anno->hasAnnotation('systemHidden') && $anno->offsetGet('systemHidden') === true;
                    if ($anno->hasAnnotation("foreignEntityClass") && $anno->offsetGet("foreignEntityClass")) {
                        $prop['opts']['foreignEntityClass'] = $anno->offsetGet("foreignEntityClass");
                    }
                    $prop['type'] = FieldEntity::FIELD_TO_DB_MAP[$prop['field_type']] ?? $prop['field_type'];
                    if ($prop['column'] == 'id') {
                        array_unshift($result, $prop);
                    } else {
                        $result[] = $prop;
                    }
                }
            }
        }
        return $result;
    }

    /**
     * Before create event handler to be implemented in the child
     * @access protected
     * @return void
     * */
    protected function beforeCreate() { }

    /**
     * After create event handler to be implemented in the child
     * @access protected
     * @return void
     * */
    protected function afterCreate() { }

    /**
     * Before the query is executed
     * This is called from the load function before the query is executed
     * Can be used to add additional query parameters
     *
     * @see load
     * @see BaseExtensible::beforeFetch
     *
     * @param QueryBuilder $q
     * @param string $tableAlias - eg. for sb__user u, u is sent through
     * @return QueryBuilder
     */
    protected function beforeFetch(QueryBuilder $q, string $tableAlias): QueryBuilder { return $q; }

    /**
     * After the query is executed
     * This is called from the load function after the query has been executed
     * Can be used to decode some custom fields
     *
     * @see load
     * @see BaseExtensible::afterFetch
     *
     * @param array $rawData
     * @return array
     */
    protected function afterFetch(array $rawData): array { return $rawData; }

    public function setShouldFreshLoad(bool $shouldFreshLoad): Base
    {
        $this->shouldFreshLoad = $shouldFreshLoad;
        return $this;
    }

    protected function setDataDecoded(array $data)
    {
        if (is_array($data)) {
            foreach ($data as $key => $value) {
                $this->$key = $value;
                if (!$this->ignoreModifications) {
                    $this->modifiedProperties[$key] = $value;
                }
            }
        }
    }

    private function getDecodedValue(string $fieldReference, $value) {
        $decodedValue = null;
        try {
            if ($field = $this->getSchema()->getEntityField($fieldReference)) {
                $type = $field->db_type;
                if (!is_null($value)) {
                    if (gettype($value) === 'string' &&  /// this check makes sure that the data does not get decoded twice
                        $type !== 'string' /// this check is a slight optimisation to escape on string types since they dont require any decoding
                    ) {
                        if ($type === 'integer') {
                            if ($value === '' &&
                                !empty($field->db_options) &&
                                isset($field->db_options['notnull']) &&
                                $field->db_options['notnull'] === false
                            ) {
                                $value = null;
                            } else {
                                $value = (int)$value;
                            }
                        } elseif ($type === 'json') {
                            $value = json_decode($value, true);
                        } elseif ($type === 'datetime') {
                            $value = new \DateTime($value);
                        } elseif ($type === 'datetime_immutable') {
                            $value = new \DateTimeImmutable($value);
                        } elseif ($type === 'boolean') {
                            $value = filter_var($value, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
                        }
                    }
                    $decodedValue = $value;
                } elseif ($type === 'datetime' && ($fieldReference === 'lote_created' || $fieldReference === 'lote_updated')) { /// initialise a minimum datetime object
                    $decodedValue = new \DateTime(StdDateTime::MINIMUM_DATETIME_STRING);
                } elseif ($type === 'datetime_immutable' && ($fieldReference === 'lote_created' || $fieldReference === 'lote_updated')) { /// initialise a minimum datetime object
                    $decodedValue = new \DateTime(StdDateTime::MINIMUM_DATETIME_STRING);
                }
            }
        } catch (\Exception $e) { }

        return $decodedValue;
    }

    /**
     * Set the data for this object
     * @param array $data
     * @return void
     * */
    public function setData($data)
    {
        if (is_array($data) && count($data) > 0) {
            foreach ($data as $key => $value) {
                $decodedValue = $this->getDecodedValue($key, $value);
                $this->$key = $decodedValue;
                if (!$this->ignoreModifications) {
                    $this->modifiedProperties[$key] = $decodedValue;
                }

            }
        }
    }

    protected function encodeData(array $data, SchemaEntity $entity, bool $displayable = false): array
    {
        foreach ($data as $key => $value) {
            if (!is_null($value)) {
                if ($field = $entity->getEntityField($key)) {
                    $vtf = VirtualTypesFactory::createInstance($field->field_type, $value);
                    $data[$key] = $displayable ? $vtf->displayFormat() : $vtf->encode();
                }
            }
        }

        return $data;
    }

    public function getDataEncoded(?bool $displayable = false, ?string $fieldReference = null)
    {
        $data = $this->getData();
        if ($fieldReference !== null && array_key_exists($fieldReference, $data)) {
            $data = [ $fieldReference => $data[$fieldReference] ];
        }
        return $this->encodeData($data, $this->getSchema(), $displayable);
    }

    public function getDataAsApiResponse(int $withFlags = FieldEntity::FLAG_EXPORTABLE, bool $transformedToCamelCase = true): array
    {
        $output = [];
        foreach ($this->getSchema()->getEntityFields() as $key => $field) {
            if ($field->hasFlag($withFlags)) {
                $value = $this->{$field->reference};
                if ($value !== null) {
                    $value = VirtualTypesFactory::createInstance($field->field_type,
                        $this->{$field->reference})->apiEncode();

                    //convert to timezone
                    if($field->field_type == FieldEntity::FIELD_TYPE_DATETIME || $field->field_type == FieldEntity::FIELD_TYPE_DATETIME_IMMUTABLE){
                        if(
                            ($this->getState()->getRequest()->headers) &&
                            ($timezone = $this->getState()->getRequest()->headers->get('x-bsdl-client-tz'))
                        ){
                            if($value instanceof \DateTime){
                                $value = $value->format("Y-m-d H:i:s");
                            }
                            $value = new \DateTime($value);
                            $value->setTimezone(new \DateTimeZone($timezone));
                            $value = $value->format('Y-m-d H:i:s');
                        }
                    }
                }
                $apiKey = $key;
                if ($transformedToCamelCase) {
                    $apiKey = str_replace('_', '', lcfirst(ucwords($key, '_')));
                }
                $output[$apiKey] = $value;
            }
        }
        return $output;
    }

    /**
     * Get the data for this object as an array
     * @return array
     * */
    public function getData()
    {
        $data = $this->entityData;
        if (!empty($data) && isset($this->id) && !isset($data['id'])) {
            $data['id'] = $this->id;
        }
        return $data;
    }

    /**
     * Get the edit for this object as an array
     * @return array
     * */
    public function getEditData()
    {
        $data = $this->getData();
        if ($field = $this->getSchema()->getEntityField('lote_access')) {
            if (isset($data['lote_access']) && $data['lote_access'] == -1) {
                $data['_lote_access_roles'] = $this->getEntityAccess();
            }
        }
        return $data;
    }

    /**
     * Get the edit for this object as an array
     * @return array
     * */
    public function getViewData()
    {
        return $this->getEditData();
//        $data = $this->getData();
//        if (property_exists($this, 'lote_access') && $data['lote_access'] == -1) {
//            $data['_lote_access_roles'] = $this->getEntityAccess();
//        }
//        return $data;
    }

    /**
     * Check if a default value exists on a field that is not 'null'
     * @param string $fieldName - the name of the field in question
     * @return bool
     * todo is this needed?
     */
    protected function hasDefaultFieldValue(string $fieldName)
    {
        return $this->getDefaultFieldValue($fieldName) !== null;
    }

    /**
     * Get all class properties which should map to database fields
     * @param string $fieldName - the name of the field in question
     * @return mixed
     */
    protected function getDefaultFieldValue(string $fieldName)
    {
        $result = null;
        if (!empty($fieldName)) {
            if ($field = $this->getSchema()->getEntityField($fieldName)) {
                if ($field->default_value !== null) {
                    $result = VirtualTypesFactory::createInstance($field->field_type, $field->default_value)->decode();
                }
            }
        }
//        $reflectionClass = new \ReflectionClass(get_called_class());
//        $properties = $reflectionClass->getDefaultProperties();
//        if (array_key_exists($fieldName, $properties)) {
//            $result = $properties[$fieldName];
//        }
        return $result;
    }

    /**
     * Get all class properties which should map to database fields
     * @deprecated - all fields should be considered savable
     * @return FieldEntity[]
     */
    protected function getSaveFields(): array
    {
        $result = [];
        foreach ($this->getSchema()->getEntityFields() as $v) {
            if (isset($this->entityProperties[$v->reference]) || $v->isExtension()) {
                $result[] = $v;
            }
        }
        return $result;
    }

    /**
     * Check if a field has been modified in this entity
     * @access private
     * @param FieldEntity $fieldName
     * @return boolean
     * @todo - change the implementation of this...
     * */
    private function isDirtyField(FieldEntity $fieldName)
    {
        return count($this->modifiedProperties) > 0 && array_key_exists($fieldName->reference, $this->modifiedProperties);
    }

    /**
     * Save this object
     * @access public
     * @return int
     */
    public function save()
    {
        $fields = $this->getSaveFields();
        $data = [];

        foreach ($fields as $v) {
            if ($this->isDirtyField($v)) {
                $ref = $v->reference;
                $data[$ref] = $this->$ref;
            }
        }

        if (isset($data['lote_access']) && is_array($data['lote_access'])) {
            if (isset($data['lote_access']['type'])) {
                $data['lote_access'] = $data['lote_access']['type'];
            } elseif ($data['lote_access'][0] <= 1) {
                $data['lote_access'] = $data['lote_access'][0];
            } else {
                unset($data['lote_access']);
                $data['lote_access'] = '-1';
            }
        }
        unset($data['id']);
        if (!empty($data)) {
            $data = $this->encodeData($data, $this->getSchema());
            if ($this->id > 0) {
                $result = $this->id;
                $this->lote_updated = new \DateTimeImmutable();
                $data['lote_updated'] = $this->lote_updated->format('Y-m-d H:i:s');
                $data = $this->beforeUpdate($data);
                $this->clearCachedData();
                if ($data) {
                    $this->logAudit(true);
                    $this->clearCachedData();
                    $this->getWriteDb()->update($this->getTableName(), $data, ['id' => $this->id]);
                } else {
                    $result = false;
                }
            } else {
                if (!isset($data['lote_author_id']) && $this->getState()->getUser() && $this->getState()->getUser()->id > 0) {
                    $data['lote_author_id'] = $this->getState()->getUser()->id;
                }
                $this->lote_created = $this->lote_updated = new \DateTimeImmutable();
                $data['lote_created'] = $data['lote_updated'] = $this->lote_updated->format('Y-m-d H:i:s');
                $data = $this->beforeInsert($data);
                $this->getWriteDb()->insert($this->getTableName(), $data);
                $result = $this->id = $this->entityData['id'] = $data['id'] = (int) $this->getWriteDb()->lastInsertId();
                $this->clearCachedData();
                $this->logAudit(false);
                $this->clearCachedData();
                $this->afterInsertData($data);
            }
        } else {
            $result = $this->id;
        }
        return $result;
    }

    public function _saveByInsert() {
        $data = $this->getDataEncoded();
        if (isset($data['lote_access']) && is_array($data['lote_access'])) {
            if ($data['lote_access'][0] <= 1) {
                $data['lote_access'] = $data['lote_access'][0];
            } else {
                unset($data['lote_access']);
                $data['lote_access'] = '-1';
            }
        }
        if (!empty($data)) {
            $data = $this->encodeData($data, $this->getSchema());
            if (!isset($data['lote_author_id']) && $this->getState()->getUser() && $this->getState()->getUser()->id > 0) {
                $data['lote_author_id'] = $this->getState()->getUser()->id;
            }
            $this->lote_created = $this->lote_updated = new \DateTimeImmutable();
            $data['lote_created'] = $data['lote_updated'] = $this->lote_updated->format('Y-m-d H:i:s');
            $data = $this->beforeInsert($data);
            $this->getWriteDb()->insert($this->getTableName(), $data);
            $result = $this->id = $this->entityData['id'] = $data['id'] = $this->getWriteDb()->lastInsertId();
            $this->clearCachedData();
            $this->afterInsertData($data);

        } else {
            $result = $this->id;
        }
        return $result;
    }

    protected function makeRedisKey($reference): string
    {
        $reference = $reference ?? "";
        return str_replace(['{', '}', '(' ,')' ,'/' ,'\'', '\\' ,'@'], "_", $reference);
    }

    /**
     * Clear any references to this data if it exists
     * @access private
     * @return void
     */
    protected function clearCachedData(): void
    {
        $this->clearDbCache();
        $this->clearDataCache();
        $this->onClearCachedData();
    }

    /**
     * Update the local data cache with the content of this entity
     * @access private
     * @return void
     * */
    protected function updateDataCache(): void
    {
        $this->getState()->getData()->setValue("entity:" . $this->getTableName(), $this->id, $this->getData());
        if ($this->reference) {
            $this->getState()->getData()->setValue("entity:" . $this->getTableName(), $this->reference, $this->getData());
        }
        if ($this->location_reference) {
            $this->getState()->getData()->setValue("entity:" . $this->getTableName(), $this->location_reference, $this->getData());
        }
    }

    /**
     * Clear the local data cache for this entity
     * @access private
     * @return void
     * */
    private function clearDataCache(): void
    {
        $this->getState()->getData()->deleteValue("entity:" . $this->getTableName(), $this->id);
        if ($this->reference) {
            $this->getState()->getData()->deleteValue("entity:" . $this->getTableName(), $this->reference);
        }
        if (isset($this->location_reference)) {
            $this->getState()->getData()->deleteValue("entity:" . $this->getTableName(), $this->location_reference);
        }
    }

    /**
     * Clear the redis cache for this entity
     * @access private
     * @return void
     * */
    private function clearDbCache(): void
    {
        $redisClient = $this->getState()->getDbCache();
        if ($this->id && $redisClient->has('entity.' . $this->getTableName() . '.' . $this->id)) {
            $redisClient->delete('entity.' . $this->getTableName() . '.' . $this->id);
        }
        if (isset($this->reference) && $redisClient->has('entity.' . $this->getTableName() . '.' . $this->makeRedisKey($this->reference))) {
            $redisClient->delete('entity.' . $this->getTableName() . '.' . $this->makeRedisKey($this->reference));
        }
        if (isset($this->location_reference) && $redisClient->has('entity.' . $this->getTableName() . '.' . $this->makeRedisKey($this->location_reference))) {
            $redisClient->delete('entity.' . $this->getTableName() . '.' . $this->makeRedisKey($this->location_reference));
        }
    }

    /**
     * Function to call in the child after clearing cached data
     * @access protected
     * @return array
     * */
    protected function onClearCachedData() { }

    /**
     * Function to call before inserting data so that data can be manipulated if required
     * @param array $data
     * @return array
     * */
    protected function beforeInsert($data) { return $data; }

    /**
     * Function to call after inserting data so that data can be manipulated if required
     * @param array $data
     * @return array
     * */
    protected function afterInsertData($data) { return $data; }

    /**
     * Function to call before updating data so that data can be manipulated if required
     * @param array $data
     * @return array
     * */
    protected function beforeUpdate($data) { return $data; }

    /**
     * Get a single object by id
     *
     * @param integer $id
     * @param bool $includeDeleted - Whether or not to include objects that have been deleted
     * @return int|bool
     */
    public function load($id, $includeDeleted = false)
    {
        $this->ignoreModifications = true;
        if ($id) {
            if (!$this->shouldFreshLoad && $this->getState()->getData()->hasValue("entity:" . $this->getTableName(), $id)) {
                $this->setData($this->getState()->getData()->getValue("entity:" . $this->getTableName(), $id));
            } else {
                $q = $this->getReadDb()->createQueryBuilder();
                $q->select('t.*')
                    ->from($this->getTableName(), 't')
                    ->where('t.id = :id')
                    ->setParameter('id', $id);
                if (!$includeDeleted) {
                    $q->andWhere('t.lote_deleted is null');
                }

                $q = $this->beforeFetch($q, 't');

                if($data = $q->execute()->fetch(\PDO::FETCH_ASSOC)) {
                    $data = $this->afterFetch($data);
                    $this->setData($data);
                }
                $this->updateDataCache();
            }
            $this->afterLoad();
        }
        $this->ignoreModifications = false;
        return $this->id;
    }

    /**
     * Reload the details of this entity
     * @access public
     * @return boolean
     * */
    public function reload()
    {
        $this->clearCachedData();
        $result = false;
        $id = $this->id;
        if ($id) {
            $this->reset();
            $result = $this->load($id) > 0;
            $this->updateDataCache();
        }
        return $result;
    }

    /**
     * Event handler to call after the load of an entity
     * */
    protected function afterLoad() { }

    public function loadByField(string $fieldName, $fieldValue, bool $includeDeleted = false, ?SortOrderContainer $sortOrder = null): bool
    {
        return $this->loadByFieldBase($fieldName, $fieldValue, false, $includeDeleted, $sortOrder);
    }

    public function loadByBinaryField(string $fieldName, $fieldValue, bool $includeDeleted = false, ?SortOrderContainer $sortOrder = null): bool
    {
        return $this->loadByFieldBase($fieldName, $fieldValue, true, $includeDeleted, $sortOrder);
    }

    private function loadByFieldBase(string $fieldName, $fieldValue, bool $binaryMatch = false, bool $includeDeleted = false, ?SortOrderContainer $sortOrder = null)
    {
        $this->ignoreModifications = true;
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('t.*')
            ->from($this->getTableName(), 't');
        if ($binaryMatch) {
            $q->where('binary ' . "t.{$fieldName}" . ' = :' . $fieldName);
        } else {
            $q->where("t.{$fieldName}" . ' = :' . $fieldName);
        }

        $q->setParameter($fieldName, $fieldValue);
        if (!$includeDeleted) {
            $q->andWhere('t.lote_deleted is null');
        }

        if ($sortOrder !== null) {
            $q->addOrderBy("t.{$sortOrder->columnName}", $sortOrder->sortType);
        }

        $q = $this->beforeFetch($q, 't');
        $data = $q->execute()->fetch(\PDO::FETCH_ASSOC);
        unset($q);
        if ($data) {
            $data = $this->afterFetch($data);
        }
        $this->setData($data);
        $this->getState()->getData()->setValue("entity:" . $this->getTableName(), $this->id, $this->getData());
        $this->afterLoad();

        $this->ignoreModifications = false;
        return !empty($data);
    }

    public function loadByFields(array $fieldNameValues, bool $includeDeleted = false, ?SortOrderContainer $sortOrder = null): bool
    {
        $this->ignoreModifications = true;
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('t.*')
            ->from($this->getTableName(), 't');
        foreach ($fieldNameValues as $fn => $fv) {
            if (is_array($fv)) {
                $maxIndex = count($fv) - 1;
                $index = 0;
                $sqlStatement = '';
                if ($maxIndex >= 0) {
                    foreach ($fv as $fnOr => $fvOr) {
                        /// This block will trim any prefixed _
                        /// This is due to the limitations of the associative arrays.
                        /// Eg. so that email = null and email = '' can be placed in the same or block
                        $charIndex = 0;
                        $fnOrCount = strlen($fnOr);
                        while ($charIndex < $fnOrCount && $fnOr[$charIndex] === '_') { $charIndex++; }
                        if ($charIndex > 0) { $fnOr = substr($fnOr, $charIndex); }

                        $fnOrKey = 't.' . $fnOr;
                        if ($fvOr === null) {
                            $sqlStatement .= $fnOrKey . ' is null';
                        } else {
                            $key = $fnOr . '_' . uniqid();
                            $sqlStatement .= $fnOrKey . ' = :' . $key;
                            $q->setParameter($key, $fvOr);
                        }

                        if ($index++ < $maxIndex) {
                            $sqlStatement .= ' or ';
                        }
                    }
                }
                $q->andWhere($sqlStatement);
            } else {
                $key = $fn . '_' . uniqid();
                $fnKey = 't.' . $fn;
                $fv === null ? $q->andWhere($fnKey . ' is null') : $q->andWhere($fnKey . ' = :' . $key)->setParameter($key, $fv);
            }
        }
        if (!$includeDeleted) {
            $q->andWhere('t.lote_deleted is null');
        }
        if ($sortOrder !== null) {
            $q->addOrderBy("t.{$sortOrder->columnName}", $sortOrder->sortType);
        }
        $q = $this->beforeFetch($q, 't');
        $q->setMaxResults(1);
        $data = $q->execute()->fetch(\PDO::FETCH_ASSOC);
        if ($data) {
            $data = $this->afterFetch($data);
        }
        $this->setData($data);
        $this->getState()->getData()->setValue("entity:" . $this->getTableName(), $this->id, $this->getData());
        $this->afterLoad();

        $this->ignoreModifications = false;
        return !empty($data);
    }

    /**
     * Update properties for this object
     * @param array $properties
     * @deprecated use the save function
     * @return boolean
     */
    public function update(Array $properties)
    {
        $result = false;
        if ($this->id > 0) {
            $params = [];
            foreach ($properties as $key => $value) {
                if (property_exists($this, $key)) {
                    $this->{$key} = $value;
                    $params[$key] = $value;
                }
            }
            $this->getWriteDb()->update($this->getTableName(), $params, ['id' => $this->id]);
            $result = true;
        }
        return $result;
    }


    /**
     * Update a single property
     *
     * @param string $key Property name
     * @param mixed $value Property value
     * @deprecated use the save function instead
     * @return boolean
     */
    public function updateProperty($key, $value)
    {
        $result = false;
        if ($this->id > 0 && property_exists($this, $key)) {
            $this->{$key} = $value;
            $this->getWriteDb()->update($this->getTableName(), [$key => $value], ['id' => $this->id]);
            $this->save();
            $result = true;
        }
        return $result;
    }

    /**
     * Delete this object
     * @param bool $strongDelete - Whether or not to delete the table object completely
     * @return boolean
     */
    public function delete(bool $strongDelete = false)
    {
        if ($strongDelete) {
            $result = $this->getWriteDb()->delete($this->getTableName(), ['id' => $this->id]) > 0;
        } else {
            $data = ['lote_deleted' => Time::getUtcNow(), 'lote_deleted_by' => $this->getState()->getUser()->id];
            $result = $this->getWriteDb()->update($this->getTableName(), $data, ['id' => $this->id]) > 0;
        }
        $this->clearCachedData();
        $this->reset();
        return $result;
    }

    /**
     * Get the access for this entity
     * @access protected
     * @return array
     *  */
    protected function getEntityAccess()
    {
        $result = [];
        if ($this->id) {
            $q = $this->getReadDb()->createQueryBuilder();
            $q->select('a.*')
                ->from('sb__auth_access_entity', 'a')
                ->where('object_ref = :object_ref')
                ->setParameter('object_ref', $this->getTableName())
                ->andWhere('object_id = :object_id')
                ->setParameter('object_id', $this->id);
            foreach ($q->execute()->fetchAll(\PDO::FETCH_ASSOC) as $row) {
                $result[] = $row;
            }
        }
        return $result;
    }

    public function hasEntityAccess(User $user = null)
    {
        $result = false;
        if (!$user) {
            $user = $this->getState()->getUser();
        }
        if ($user->isAdmin()) {
            $result = true;
        } elseif ($this->lote_access === '0' || $this->lote_access === 0) {
            $result = true;
        } elseif ($this->lote_access == -1) {
            $accessGroups = $this->getEntityAccess();
            $userModel = new \BlueSky\Framework\Model\User($this->getState());
            $userGroups = $userModel->getUserGroupIds($user->id, '', 'admin');
            $result = !empty(array_intersect($accessGroups, $userGroups));
        } elseif ($this->lote_access == '1' || $this->lote_access == null) {
            $result = true;
        }
        return $result;
    }

    /**
     * Reset this entity to its original state
     * @access public
     * @return void
     * @todo - reset all of the fields to their default state
     * */
    public function reset()
    {
        $this->id = 0;
        $this->modifiedProperties = [];
//        $this->unsetEntityProperties();
    }


//// MOVE TO ENTITY MANAGER //////
    /**
     * Log changes to this entity
     * @param bool $isUpdate
     * */
    protected function logAudit(bool $isUpdate = true): void
    {
        if ($this->isAudited) {
            if (count($this->modifiedProperties) > 0) {
                $a = $this->getState()->getAudit();
                $oldObject = null;
                if ($isUpdate && $this->id > 0) {
                    $oldObject = clone $this;
                    $oldObject->load($this->id);
                }
                $this->auditObjectLog = $a->addObjectLog($this, $oldObject);
            }
        }
    }

    public function setAuditing(bool $audit): void { $this->isAudited = $audit; }

    /**
     * Get the audit log
     * @return ObjectLog
     * */
    public function getAuditLog() { return $this->auditObjectLog; }

    /// Check /////////
    public function __set($key, $value)
    {
        if (isset($this->entityProperties[$key])) {
            $keyIsSet = isset($this->$key);
            if (!$this->ignoreModifications &&
                (
                    (!$keyIsSet && $value !== null) ||
                    ($keyIsSet && $this->$key !== $value) ||
                    $value !== $this->getDefaultFieldValue($key)
                )
            ) {
                $this->modifiedProperties[$key] = $value;
            }
            $decodedValue = $this->schema !== null ? $this->getDecodedValue($key, $value) : $value;
            $this->entityData[$key] = $decodedValue;

        } else {
            $this->$key = $value;
            $this->entityData[$key] = $value;
        }
    }

    public function &__get($key)
    {
        $result = null;
        if (isset($this->entityData[$key])) {
            $result = $this->entityData[$key];
        } elseif ($key[0] !== '_') {
            $result = $this->getDefaultFieldValue($key);
        }
        return $result;
    }

    public function __isset($key) { return array_search($key, $this->entityProperties ?? []) !== false; }

    public function __unset($key)
    {
        if (isset($this->entityData[$key])) {
            if (!$this->ignoreModifications) {
                $this->modifiedProperties[$key] = $this->entityData[$key];
            }
        }
        unset($this->entityData[$key]);
    }

    public function __destruct()
    {
        if ($this->entityData !== null) {
            foreach ($this->entityData as $key => $value) {
                free($this->$key);
            }
        }
        free($this->entityData);
        free($this->entityProperties);
        free($this->modifiedProperties);

        free($this->schema);

        free($this->auditObjectLog);
    }

    public function getModifiedProperties()
    {
        return $this->modifiedProperties;
    }

    public function markPropertyUnmodified($propertyName)
    {
        if (array_key_exists($propertyName, $this->modifiedProperties)) {
            unset($this->modifiedProperties[$propertyName]);
        }
    }

    /// Check /////////

    public function toArray() { return $this->getData(); }

    public function jsonSerialize() { return $this->getData(); }

    public function __debugInfo() { return $this->getData(); }

    public function offsetGet($offset) {
        $output = null;
        if (!ctype_upper($offset[0])) {
            $output = $this->getDataEncoded(true, $offset)[$offset] ?? $this->__get($offset);
        } else {
            $output = $this->__get($offset);
        }
        return $output;
    }

    public function offsetExists($offset): bool { return $this->__isset($offset); }

    public function offsetSet($offset, $value) { $this->__set($offset, $value); }

    public function offsetUnset($offset) { $this->__unset($offset); }
    /**
     * Get the value of a specific field for this model
     * @param string $fieldName
     * @param mixed $default
     * @access public
     * @deprecated - Use the field directly
     * @return string
     * */
    public function getField($fieldName, $default = '')
    {
        $result = $default;
        if (isset($this->{$fieldName})) {
            $result = $this->{$fieldName};
        }
        return $result;
    }

    public function getModelClassNameFromEntity()
    {
        return str_replace("\Entity\\", "\Model\\", get_class($this));
    }

}
