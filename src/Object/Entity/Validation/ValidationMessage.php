<?php
namespace BlueSky\Framework\Object\Entity\Validation;

class ValidationMessage
{
    /** @var bool $success */
    private $success;

    /** @var null|string $error */
    private $error;

    /** @var string $fieldReference */
    private $fieldReference;

    /**
     * ValidationMessage constructor.
     * @param string $fieldReference
     * @param bool $success
     * @param string|null $error - can be null if success is true
     */
    public function __construct(string $fieldReference, bool $success, ?string $error = null)
    {
        $this->success = $success;
        $this->error = $error;
        $this->fieldReference = $fieldReference;
    }

    public function isValid(): bool { return $this->success; }

    public function getErrorMessage(): ?string { return $this->error; }

    public function getFieldReference(): string { return $this->fieldReference; }

}