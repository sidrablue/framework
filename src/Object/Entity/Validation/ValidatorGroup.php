<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 */

declare(strict_types=1);
namespace BlueSky\Framework\Object\Entity\Validation;

use BlueSky\Framework\Object\Entity\Validation\Validators\ValidatorBase;

class ValidatorGroup
{
    /** @var ValidatorBase[] $validators - the key of the array should be the field reference */
    private $validators;

    /** @var array|null $data - Data has to be an associative array with the key names equalling the field reference */
    private $data;

//    /** @var null|Closure  */
//    private $preValidateHook = null;

    public function __construct(array $validators, array $data = null)
    {
        $this->validators = $validators;
        $this->data = $data;
    }

    public function setData(array $data): void
    {
        $this->data = $data;
    }

//    public function setPreValidateHook(Closure $callback)
//    {
//        $this->preValidateHook = $callback;
//    }

    public function validate(array $data = null, array $runtimeOpts = []): ValidationMessageGroup
    {
        if (!is_null($data)) {
            $this->setData($data);
        }

        $outputMessage = new ValidationMessageGroup();
        foreach ($this->data as $fieldReference => $value) {
            if (isset($this->validators[$fieldReference])) {
                $message = $this->validators[$fieldReference]->validate($value, $runtimeOpts);
                if (!$message->isValid()) {
                    $outputMessage->addMessage($message);
                }
            }
            /// todo: does this need to fail if the data is not found in the entity?
        }
        return $outputMessage;
    }

}
