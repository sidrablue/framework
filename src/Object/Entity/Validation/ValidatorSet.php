<?php
declare(strict_types = 1);
namespace BlueSky\Framework\Object\Entity\Validation;

use BlueSky\Framework\Object\Entity\Validation\Filters\FilterBase;
use BlueSky\Framework\Object\Entity\Validation\Validators\Nil;
use BlueSky\Framework\Object\Entity\Validation\Validators\ValidatorBase;

class ValidatorSet
{
    private function __construct() { }

    /**
     * @param string $reference - the field reference
     * @param array $input - Should be the value of the validation key from db_options
     * @return null|ValidatorBase
     */
    public static function createValidatorSet(string $reference, $input): ?ValidatorBase
    {
        $output = null;
        if (is_array($input)) {
            if (isset($input['validatorSet'])) {
                $filterSet = $input['validatorSet'];
                if (is_string($filterSet)) {
                    $output = self::generateValidatorByTemplate($filterSet);
                } elseif (is_array($filterSet)) {
                    $validatorType = str_replace('_', '', ucwords($filterSet['type'], '_'));
                    $validatorOpts = $filterSet['opts'] ?? [];
                    $validator = self::generateValidatorByType($reference, $validatorType, $validatorOpts);
                    if (!($validator instanceof Nil)) {
                        $filters = $input['filters'] ?? null;
                        if ($filters) {
                            foreach ($filters as $filterData) {
                                if ($filter = self::generateFilter($reference, $validatorType, $filterData)) {
                                    $validator->attachFilter($filter);
                                }
                            }
                        }
                    }
                    $output = $validator;
                }
            }
        }
        return $output;
    }

    private static function generateValidatorByTemplate(string $template): ?ValidatorBase
    {
        return null;
    }

    private static function generateValidatorByType(string $reference, string $type, array $opts = []): ValidatorBase
    {
        $output = null;
        if (file_exists(LOTE_FRAMEWORK_PATH . 'Object/Entity/Validation/Validators/' . $type . '.php')) {
            $className = 'BlueSky\Framework\Object\Entity\Validation\Validators\\' . $type;
            $output = new $className($reference, $opts);
        } else {
            // todo: remove this else block once all the field validators have been implemented
            $output = new Nil($reference);
        }
        return $output;
    }

    private static function generateFilter(string $reference, string $validatorType, array $filter): ?FilterBase
    {
        $output = null;
        if (!empty($filter['type'])) {
            $filterType = str_replace('_', '', ucwords($filter['type'], '_'));
            $validatorOpts = $filter['opts'] ?? [];
            if (file_exists(LOTE_FRAMEWORK_PATH . 'Object/Entity/Validation/Filters/' . $validatorType . '/' . $filterType . '.php')) {
                $className = 'BlueSky\Framework\Object\Entity\Validation\\Filters\\' . $validatorType . '\\' . $filterType;
                $output = new $className($reference, $validatorOpts);
            } elseif (file_exists(LOTE_FRAMEWORK_PATH . 'Object/Entity/Validation/Filters/' . $filterType . '.php')) {
                $className = 'BlueSky\Framework\Object\Entity\Validation\\Filters\\' . $filterType;
                $output = new $className($reference, $validatorOpts);
            }
        }
        return $output;
    }

}