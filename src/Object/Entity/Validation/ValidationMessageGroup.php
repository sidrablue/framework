<?php
namespace BlueSky\Framework\Object\Entity\Validation;

class ValidationMessageGroup
{
    /** @var bool $isValid */
    private $isValid = true;

    /** @var string[] error $messages */
    private $messages = [];

    public function __construct() { }

    public function isValid(): bool
    {
        return $this->isValid;
    }

    public function addMessage(ValidationMessage $message): void
    {
        if (!$message->isValid()) {
            $this->messages[$message->getFieldReference()] = $message->getErrorMessage();
            $this->isValid = false;
        }
    }

    public function  getErrorMessage(): array
    {
        return $this->messages;
    }

}
