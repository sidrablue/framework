<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Object\Entity\Validation\Validators;

use Exception;
use BlueSky\Framework\Object\Entity\Validation\Filters\FilterBase;
use BlueSky\Framework\Object\Entity\Validation\ValidationMessage;
use BlueSky\Framework\Object\Entity\Validation\TransformException;


abstract class ValidatorBase
{
    /** @var FilterBase[] $filters  */
    private $filters = [];

    private $value;

    protected $fieldReference;

    public const RUNTIME_OPTS_CURRENT_ROW_ID = 'current_row_id';

    public function __construct(string $fieldReference, array $opts = [])
    {
        $this->fieldReference = $fieldReference;
    }

    public function attachFilter(FilterBase $filter): void
    {
        if (!is_null($filter)) {
            $this->filters[] = $filter;
        }
    }

    public function hasFilter(string $filterReference): bool
    {
        $output = false;
        foreach ($this->filters as $index => $filter) {
            if ($filter::REFERENCE === $filterReference) {
                $output = true;
                break;
            }
        }
        return $output;
    }

    public function removeFilter($filterReference): void
    {
        foreach ($this->filters as $index => $filter) {
            if ($filter::REFERENCE === $filterReference) {
                unset($this->filters[$index]);
            }
        }
    }

    protected abstract function validateInternal($value, array $runtimeOpts = []): ValidationMessage;

    protected abstract function transform($value, array $runtimeOpts = []);

    public final function getTransformedValue() { return $this->value; }

    protected function allowNullValue(): bool { return true; }

    public final function validate($value, array $runtimeOpts = []): ValidationMessage
    {
        try {
            $this->value = $this->transform($value, $runtimeOpts);
            if ($this->value !== null || $this->allowNullValue()) {
                $message = $this->validateInternal($this->value, $runtimeOpts);
                if ($message->isValid()) {
                    foreach ($this->filters as $filter) {
                        $message = $filter->validate($this->value, $runtimeOpts);
                        if (!$message->isValid()) {
                            break;
                        }
                    }
                }
            } else {
                $message = new ValidationMessage($this->fieldReference, false, "Value {$value} did not transform correctly");
            }
        } catch (TransformException $e) {
            $message = new ValidationMessage($this->fieldReference,false, "{$e->getMessage()} ({$value})");
        } catch (Exception $e) {
            $message = new ValidationMessage($this->fieldReference,false, "Value {$value} did not transform correctly");
        }

        return $message;
    }

}