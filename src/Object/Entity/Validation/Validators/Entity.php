<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Object\Entity\Validation\Validators;

use BlueSky\Framework\Object\Entity\Validation\ValidationMessage;
use BlueSky\Framework\Entity\Schema\Field as FieldEntity;

class Entity extends ValidatorBase
{
    /** @var FieldEntity[] $fields */
    private $fields;

    public function __construct(array $opts = [])
    {
        parent::__construct('', $opts);
        $this->fields = $opts['fields'];
    }

    protected function validateInternal($value, array $runtimeOpts = []): ValidationMessage
    {
        $isValid = true;
        $error = null;
        /** @var FieldEntity[] $value */
        foreach ($this->fields as $field) {
            if ($field->is_mandatory) {
                $foundKey = false;
                foreach ($value as $k => $f) {
                    if ($field->reference = $f->reference) {
                        $foundKey = $k;
                        break;
                    }
                }
                if ($foundKey !== false) {
                    unset($value[$foundKey]);
                } else {
                    $isValid = false;
                    $error = "Mandatory field {$field->reference} not found";
                    break;
                }
            }
        }
        return new ValidationMessage($this->fieldReference, $isValid, $error);
    }

    protected function allowNullValue(): bool { return false; }

    protected function transform($value, array $runtimeOpts = [])
    {
        $output = null;
        if (is_array($value)) {
            $output = $value;
        }
        return $output;
    }
}