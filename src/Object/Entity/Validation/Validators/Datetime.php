<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Object\Entity\Validation\Validators;

use BlueSky\Framework\Object\Entity\Validation\TransformException;
use BlueSky\Framework\Object\Entity\Validation\ValidationMessage;
use BlueSky\Framework\Util\Date as DateUtil;
use BlueSky\Framework\Object\Entity\VirtualTypes\Type\StdDateTime;
use DateTimeInterface;

class Datetime extends ValidatorBase
{
    protected function validateInternal($value, array $runtimeOpts = []): ValidationMessage
    {
        /** @var DateTimeInterface $value */

        $output = null;
        $du = new DateUtil();

        $date = $time = '';
        if ($value !== null) {
            $date = $value->format('Y-m-d');
            $time = $value->format('H:i:s');
        }

        //Currently validateDate() does not check minimum date string, may want to re-order logic below if it does
        if ($value === null) {
            $output = new ValidationMessage($this->fieldReference, true);
        } elseif ($date === '' && $time !== '') {
            $output = new ValidationMessage($this->fieldReference, false, 'Date not set');
        } elseif ($date !== '' && $time === '') {
            $output = new ValidationMessage($this->fieldReference, false, 'Time not set');
        } elseif (!$du::validateDate($date)) {
            $output = new ValidationMessage($this->fieldReference, false, 'Invalid date format');
        } elseif ($date < StdDateTime::MINIMUM_DATE_STRING) {
            $output = new ValidationMessage($this->fieldReference, false, "Value below minimum date (" . StdDateTime::MINIMUM_DATE_STRING . ")");
        } elseif (!$du::validateTime($time)) {
            $output = new ValidationMessage($this->fieldReference, false, 'Invalid time format');
        } else {
            $output = new ValidationMessage($this->fieldReference, true);
        }

        return $output;
    }

    protected function allowNullValue(): bool { return true; }

    protected function transform($value, array $runtimeOpts = [])
    {
        $output = null;
        if ($value instanceof \DateTimeInterface) {
            $output = $value;
        } else if (is_string($value)) {
            $value = trim($value);
            if ($value === '') {
                $output = null;
            } else {
                $explodedValue = explode(' ', $value);
                if (count($explodedValue) < 2) {
                    throw new TransformException('Date or time not specified');
                } else {
                    $output = new \DateTime($value);
                }
            }
        } else if ($value !== null) {
            throw new TransformException('Invalid datetime format');
        }
        return $output;
    }

}