<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Object\Entity\Validation\Validators;

use BlueSky\Framework\Object\Entity\Validation\TransformException;
use BlueSky\Framework\Object\Entity\Validation\ValidationMessage;
use BlueSky\Framework\Util\Date as DateUtil;

class Time extends ValidatorBase
{
    protected function validateInternal($value, array $runtimeOpts = []): ValidationMessage
    {
        $output = null;
        $du = new DateUtil();

        if ($value === null) {
            $output = new ValidationMessage($this->fieldReference, true);
        } else {
            $time = $value->format('H:i:s');
            if ($time === '' || !$du::validateTime($time)) {
                $output = new ValidationMessage($this->fieldReference, false, 'Invalid time format');
            } else {
                $output = new ValidationMessage($this->fieldReference, true);
            }
        }

        return $output;
    }

    protected function allowNullValue(): bool { return true; }

    protected function transform($value, array $runtimeOpts = [])
    {
        $output = null;

        if ($value instanceof \DateTimeInterface) {
            $output = $value;
        } else if (is_string($value)) {
            $value = trim($value);
            if ($value === '') {
                $output = null;
            } else {
                $output = new \DateTime($value);
            }
        } else if ($value !== null) {
            throw new TransformException('Invalid time format');
        }
        return $output;
    }

}