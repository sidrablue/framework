<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 */

declare(strict_types=1);
namespace BlueSky\Framework\Object\Entity\Validation\Validators;

use BlueSky\Framework\Object\Entity\Validation\ValidationMessage;
use BlueSky\Framework\Util\Strings;

class MultiSelect extends ValidatorBase
{

    public const RUNTIME_OPTS_OPTIONS = 'OptionsOpts';

    protected function validateInternal($value, array $runtimeOpts = []): ValidationMessage
    {
        $validStructure = true;
        $error = null;

        if ($value !== null) {
            if (is_array($value)) {
                if (!isset($value['contains_custom_selection']) || !is_bool($value['contains_custom_selection'])) {
                    $validStructure = false;
                    $error = 'Custom Input must be set to a bool value';
                } elseif (!isset($value['selections']) || !is_array($value['selections'])) {
                    $validStructure = false;
                    $error = 'Selections must be set to an array';
                }
            } else {
                $error = 'Value is not a json object';
                $validStructure = false;
            }
        }
        return new ValidationMessage($this->fieldReference,$validStructure, $error);
    }

    /**
     * Assuming that the data coming in from the client correctly maps the rows and columns according to the
     * matrix direction
     *
     * @param mixed $value
     * @param array $runtimeOpts
     * @return array|mixed|null
     */
    protected function transform($value, array $runtimeOpts = [])
    {
        $output = null;
        $input = $value;
        if (is_string($input)) {
            if (Strings::isJson($input)) {
                $input = json_decode($input, true);
            } elseif (!empty($runtimeOpts[self::RUNTIME_OPTS_OPTIONS])) {
                $opts =  $runtimeOpts[self::RUNTIME_OPTS_OPTIONS];
                $ova = array_map('trim', explode(';', $input));
                $selections = [];
                $containsCustomSelection = false;
                foreach ($ova as $ovi => $ovv) {
                    foreach ($opts['rows'] as $ri => $row) {
                        $found = false;
                        foreach($row['columns'] as $column) {
                            $data = $column['data'];
                            if (trim($data['text']) === $ovv) {
                                $selections[$data['id']] = [
                                    'name' => $data['text'],
                                    'value' => $data['value'],
                                ];
                                unset($ova[$ovi], $opts['rows'][$ri]);
                                $found = true;
                                break;
                            }
                        }
                        if ($found) { break; }
                    }
                }
                if (!empty($ova) && $opts['custom_user_options']) {
                    $containsCustomSelection = true;
                    $ovv = array_pop($ova);
                    $selections['_other'] = [
                        'value' => $ovv,
                        'name' => $ovv
                    ];
                }
                if (!empty($selections)) {
                    $input = [
                        'contains_custom_selection' => $containsCustomSelection,
                        'selections' => $selections
                    ];
                }
            }
        }

        if (is_array($input)) {
            $output = $input;
        }

        return $output;
    }
}