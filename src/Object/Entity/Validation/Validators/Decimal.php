<?php
/**
 * Copyright (c) BlueSky Digital Labs Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Object\Entity\Validation\Validators;

use BlueSky\Framework\Object\Entity\Validation\TransformException;
use BlueSky\Framework\Object\Entity\Validation\ValidationMessage;

class Decimal extends ValidatorBase
{
    protected function validateInternal($value, array $runtimeOpts = []): ValidationMessage
    {
        return new ValidationMessage($this->fieldReference,true);
    }

    protected function transform($value, array $runtimeOpts = [])
    {
        $output = null;
        if (is_numeric($value)) {
            $output = floatval($value);
        } elseif ($value !== '') {
            throw new TransformException('Invalid numeric value');
        }
        return $output;
    }

}