<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Object\Entity\Validation\Validators;

use BlueSky\Framework\Object\Entity\Validation\ValidationMessage;

class Boolean extends ValidatorBase
{
    protected function validateInternal($value, array $runtimeOpts = []): ValidationMessage
    {
        return new ValidationMessage($this->fieldReference, true);
    }

    protected function allowNullValue(): bool { return false; }

    protected function transform($value, array $runtimeOpts = [])
    {
        return filter_var($value, FILTER_VALIDATE_BOOLEAN);
    }

}