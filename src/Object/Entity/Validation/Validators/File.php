<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Object\Entity\Validation\Validators;

use BlueSky\Framework\Object\Entity\Validation\ValidationMessage;
use BlueSky\Framework\Model\File as FileModel;

class File extends ValidatorBase
{
    protected function validateInternal($value, array $runtimeOpts = []): ValidationMessage
    {
        return new ValidationMessage($this->fieldReference,true);
    }

    protected function transform($value, array $runtimeOpts = [])
    {
        $output = null;
        if(is_array($value)){
            $output = strtolower(pathinfo($value['name'],PATHINFO_EXTENSION));    
        }
        return $output;
    }

}
