<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 */

declare(strict_types=1);
namespace BlueSky\Framework\Object\Entity\Validation\Validators;

use BlueSky\Framework\Object\Entity\Validation\ValidationMessage;
use BlueSky\Framework\Util\Strings;

class SingleSelect extends ValidatorBase
{

    protected function validateInternal($value, array $runtimeOpts = []): ValidationMessage
    {
        $validStructure = true;
        $error = null;

        if ($value !== null) {
            if (is_array($value)) {
                if (!isset($value['custom_input']) || !is_bool($value['custom_input'])) {
                    $validStructure = false;
                    $error = 'Custom Input must be set to a bool value';
                } elseif (!isset($value['value']) || !is_string($value['value'])) {
                    $validStructure = false;
                    $error = 'Value must be set and must be a string';
                } elseif (!isset($value['name']) || !is_string($value['name'])) {
                    $validStructure = false;
                    $error = 'Name must be set and must be a string';
                }
            } else {
                $error = 'Value is not an array';
                $validStructure = false;
            }
        }

        return new ValidationMessage($this->fieldReference,$validStructure, $error);
    }

    /**
     * Assuming that the data coming in from the client correctly maps the rows and columns according to the
     * matrix direction
     *
     * @param mixed $value
     * @param array $runtimeOpts
     * @return array|mixed|null
     */
    protected function transform($value, array $runtimeOpts = [])
    {
        $output = null;
        $input = $value;
        if (is_string($input)) {
            if (Strings::isJson($value)) {
                $input = json_decode($input, true);
            } elseif (!empty($runtimeOpts[MultiSelect::RUNTIME_OPTS_OPTIONS])) {
                $opts =  $runtimeOpts[MultiSelect::RUNTIME_OPTS_OPTIONS];
                $ov = trim($value);
                foreach ($opts['rows'] as $row) {
                    foreach($row['columns'] as $column) {
                        $data = $column['data'];
                        if (trim($data['text']) === $ov) {
                            $input = [
                                'custom_input' => false,
                                'value' => $data['value'],
                                'name' => $data['text'],
                                'id' => $data['id'],
                            ];
                            break;
                        }
                    }
                    if (is_array($input)) {
                        break;
                    }
                }
                if (is_string($input) && $opts['custom_user_options']) {
                    $input = [
                        'custom_input' => true,
                        'value' => $value,
                        'text' => $value,
                        'id' => '_other',
                    ];
                }
            }
        }

        if (is_array($input)) {
            $output = $input;
        }

        return $output;
    }
}