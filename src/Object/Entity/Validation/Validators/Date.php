<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Object\Entity\Validation\Validators;

use BlueSky\Framework\Object\Entity\Validation\TransformException;

class Date extends DateTime
{
    protected function transform($value, array $runtimeOpts = [])
    {
        $output = null;
        if ($value instanceof \DateTimeInterface) {
            $output = $value;
        } else if (is_string($value) && trim($value) === '') {
            $output = null;
        } else if (is_string($value)){
            $output = new \DateTime("{$value} 00:00:00");
        } else if ($value !== null) {
            throw new TransformException('Invalid date format');
        }
        return $output;
    }

}