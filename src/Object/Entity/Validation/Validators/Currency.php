<?php
/**
 * Copyright (c) BlueSky Digital Labs Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Object\Entity\Validation\Validators;

use Exception;

class Currency extends Decimal
{

    /**
     * @param mixed $value
     * @param array $runtimeOpts
     * @return float|void|null
     * @throws Exception
     */
    protected function transform($value, array $runtimeOpts = [])
    {
        $output = null;
        $matched = [];
        preg_match('/[\.0-9]+/', $value, $matched);
        if (!empty($matched)) {
            $val = $matched[0];
            if (is_numeric($val)) {
                $output = floatval($val);
            }
        } else {
            $output = parent::transform($value);
        }
        return $output;
    }

}