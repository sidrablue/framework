<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Object\Entity\Validation\Validators;

use BlueSky\Framework\Object\Entity\Validation\ValidationMessage;
use GuzzleHttp\Client as GuzzleClient;
use BlueSky\Framework\State\Web;

class Recaptcha extends ValidatorBase
{
    public const OPTS_ACCOUNT_STATE_KEY = 'account_state';

    /** @var Web $secretKey */
    private $state = null;

    public function __construct(string $fieldReference, array $opts = [])
    {
        parent::__construct($fieldReference, $opts);
        if (!empty($opts[self::OPTS_ACCOUNT_STATE_KEY])) {
            $state = new Web($opts[self::OPTS_ACCOUNT_STATE_KEY]);
            $this->state = $state;
        }
    }

    protected function allowNullValue(): bool { return false; }

    protected function validateInternal($value, array $runtimeOpts = []): ValidationMessage
    {
        $output = new ValidationMessage($this->fieldReference,true);
        if ($this->state !== null) {
            $secretKey = $this->state->getSettings()->getSettingOrConfig('system.recaptcha.secret_key', null);
            if (!empty($secretKey)) {
                $client = new GuzzleClient();
                try {
                    $response = $client->request('POST', "https://www.google.com/recaptcha/api/siteverify?secret=$secretKey&response=$value");
                    $content = $response->getBody()->getContents();
                    if (empty($content)) {
                        $output = new ValidationMessage($this->fieldReference, false, 'reCAPTCHA Verification Failed');
                    } else {
                        $content = json_decode($content, true);
                        if (!isset($content['success']) || $content['success'] == false) {
                            $output = new ValidationMessage($this->fieldReference, false, 'reCAPTCHA Verification Failed');
                        }
                    }
                } catch (\Exception $e) {
                    $output = new ValidationMessage($this->fieldReference, false, 'reCAPTCHA Verification Failed');
                }
            } else {
                $output = new ValidationMessage($this->fieldReference,false, 'No Secret Key found for reCAPTCHA');
            }
        } else {
            $output = new ValidationMessage($this->fieldReference,false, 'Invalid Account State saved for reCAPTCHA');
        }
        return $output;
    }

    protected function transform($value, array $runtimeOpts = [])  { return (string) $value; }

}