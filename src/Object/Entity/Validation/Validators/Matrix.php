<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 */

declare(strict_types=1);
namespace BlueSky\Framework\Object\Entity\Validation\Validators;

use BlueSky\Framework\Object\Entity\Validation\ValidationMessage;

class Matrix extends ValidatorBase
{

    protected function validateInternal($value, array $runtimeOpts = []): ValidationMessage
    {
        $validStructure = true;
        $error = null;
        if (!empty($input['rows'])) {
            foreach ($input['rows'] as $rows) {
                if (!empty($rows['columns'])) {
                    foreach ($rows['columns'] as $column) {
                        if (
                            !isset($column['id'], $column['value']) ||
                            !is_numeric($column['id']) || $column['id'] < 0 ||
                            is_null($column['value']) || $column['value'] === ''
                        ) {
                            $validStructure = false;
                            $error = 'Invalid Column Structure/Value';
                            break;
                        }
                    }
                } else {
                    $validStructure = false;
                    $error = 'Column(s) not found';
                }

                if (!$validStructure) {
                    break;
                }
            }
        } else {
            $validStructure = false;
            $error = 'Row(s) not found';
        }

        return new ValidationMessage($this->fieldReference,$validStructure, $error);
    }

    /**
     * Assuming that the data coming in from the client correctly maps the rows and columns according to the
     * matrix direction
     *
     * @param mixed $value
     * @param array $runtimeOpts
     * @return array|mixed|null
     */
    protected function transform($value, array $runtimeOpts = [])
    {
        $output = null;
        $input = $value;
        if (is_string($input)) {
            $input = json_decode($input, true);
        }

        if (is_array($input)) {
            $output = $input;
        }

        return $output;
    }
}