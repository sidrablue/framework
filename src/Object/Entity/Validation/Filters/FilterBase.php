<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Object\Entity\Validation\Filters;

use BlueSky\Framework\Object\Entity\Validation\ValidationMessage;

abstract class FilterBase
{
    /** @var string $fieldReference  */
    protected $fieldReference;

    public const REFERENCE = '';

    public function __construct(string $reference, array $opts = [])
    {
        $this->fieldReference = $reference;
    }

    protected function allowNullValue(): bool { return false; }

    protected abstract function validateInternal($value, array $runtimeOpts = []): ValidationMessage;

    public final function validate($value, array $runtimeOpts = []): ValidationMessage
    {
        $output = new ValidationMessage($this->fieldReference, true);
        if (($value !== null && $value !== '') || $this->allowNullValue()) {
            $output = $this->validateInternal($value, $runtimeOpts);
        }
        return $output;
    }
}