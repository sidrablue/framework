<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Object\Entity\Validation\Filters\SingleSelect;

use BlueSky\Framework\Object\Entity\Validation\ValidationMessage;
use BlueSky\Framework\Object\Entity\Validation\Filters\FilterBase;

class IsMandatory extends FilterBase
{
    public const REFERENCE = 'IsMandatory';

    protected function allowNullValue(): bool { return true; }

    protected function validateInternal($value, array $runtimeOpts = []): ValidationMessage
    {
        $success = $value !== null;
        $error = !$success ? 'Value is required' : null;
        if ($success) {
            if (is_array($value)) {
                $valueInput = $value['value'];
                $nameInput = $value['name'];
                if ($valueInput === null || $valueInput === '') {
                    $success = false;
                    $error = 'Value is required';
                } elseif ($nameInput === null || $nameInput === '') {
                    $success = false;
                    $error = 'Name is required';
                }
            } else {
                $success = false;
                $error = 'Invalid input format';
            }
        }
        return new ValidationMessage($this->fieldReference, $success, $error);
    }
}