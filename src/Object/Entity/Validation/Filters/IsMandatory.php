<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Object\Entity\Validation\Filters;


use BlueSky\Framework\Object\Entity\Validation\ValidationMessage;

class IsMandatory extends FilterBase
{
    public const REFERENCE = 'IsMandatory';

    protected function allowNullValue(): bool { return true; }

    protected function validateInternal($value, array $runtimeOpts = []): ValidationMessage
    {
        $success = !is_null($value) && $value !== '';
        $error = !$success ? 'Value is required' : null;
        return new ValidationMessage($this->fieldReference, $success, $error);
    }
}