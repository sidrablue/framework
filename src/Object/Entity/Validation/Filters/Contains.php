<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Object\Entity\Validation\Filters;

use BlueSky\Framework\Object\Entity\Validation\ValidationMessage;

class Contains extends FilterBase
{
    public const REFERENCE = 'Contains';

    protected $match;

    public function __construct(string $fieldReference, array $opts = [])
    {
        parent::__construct($fieldReference,$opts);
        $this->match = $opts['match'];
    }

    protected function validateInternal($value, array $runtimeOpts = []): ValidationMessage
    {
        $contains = strpos($value, $this->match) !== false;
        $error = null;
        if (!$contains) {
            $error = 'Value does not contain text';
        }
        return new ValidationMessage($this->fieldReference, $contains, $error);
    }

}