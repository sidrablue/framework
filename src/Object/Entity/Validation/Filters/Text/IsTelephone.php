<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 */

declare(strict_types=1);
namespace BlueSky\Framework\Object\Entity\Validation\Filters\Text;

use BlueSky\Framework\Object\Entity\Validation\Filters\FilterBase;
use BlueSky\Framework\Object\Entity\Validation\ValidationMessage;
use BlueSky\Framework\Util\Number\Mobile;

class IsTelephone extends FilterBase
{
    public const REFERENCE = 'IsTelephone';

    protected function validateInternal($value, array $runtimeOpts = []): ValidationMessage
    {
        $number = Mobile::standardise($value);
        $error = null;
        if (!$number) {
            $error = 'Not a valid Number';
        }
        return new ValidationMessage($this->fieldReference, !empty($number), $error);
    }
}