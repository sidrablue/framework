<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Object\Entity\Validation\Filters\Text;

use BlueSky\Framework\Object\Entity\Validation\Filters\FilterBase;
use BlueSky\Framework\Object\Entity\Validation\ValidationMessage;

class IsEmail extends FilterBase
{
    public const REFERENCE = 'IsEmail';

    protected function validateInternal($value, array $runtimeOpts = []): ValidationMessage
    {
        $valid = filter_var($value, FILTER_VALIDATE_EMAIL) !== false;
        $error = null;
        if (!$valid) {
            $error = 'Invalid email address';
        }
        return new ValidationMessage($this->fieldReference, $valid, $error);
    }

}