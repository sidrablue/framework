<?php
/**
 * Copyright (c) BlueSky Digital Labs Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Object\Entity\Validation\Filters;

use PDO;
use BlueSky\Framework\Object\Entity\Validation\ValidationMessage;
use BlueSky\Framework\Entity\Schema\Field as FieldEntity;
use BlueSky\Framework\Object\Entity\Validation\Validators\ValidatorBase;
use BlueSky\Framework\State\Base as BaseState;
use Exception;

class IsUnique extends FilterBase
{
    public const REFERENCE = 'IsUnique';

    /** @var BaseState $state */
    private $state;

    /** @var FieldEntity $field */
    private $field;

    /**
     * Unique constructor.
     * @param array $opts
     * @param string $fieldReference
     * @throws Exception
     */
    public function __construct(string $fieldReference, array $opts = [])
    {
        parent::__construct($fieldReference, $opts);
        if (!isset($opts['account_ref'], $opts['schema_entity_ref'], $opts['field_entity_ref'])) {
            throw new \Exception('Account, schema entity and field entity references need to be set');
        }

        $this->state = new BaseState($opts['account_ref']);
        $entity = $this->state->getSchema()->getEntityByReference($opts['schema_entity_ref']);
        if ($entity == null) {
            throw new \Exception('Could not get schema entity where schema entity reference=' . $opts['schema_entity_ref']);
        }
        $this->field = $entity->getEntityField($opts['field_entity_ref']);
        if ($this->field == null) {
            throw new \Exception('Could not get field entity where field entity reference=' . $opts['field_entity_ref']);
        }
    }

    protected function validateInternal($value, array $runtimeOpts = []): ValidationMessage
    {
        $parentEntity = $this->field->getParentEntity();
        $tableName = null;
        /* Note: edge case where $tableName stays null could exist, which would cause a problem */
        if ($parentEntity) {
            $tableName = $parentEntity->getEntityTableName();
        }
        $q = $this->state->getReadDb()->createQueryBuilder()
            ->addSelect('id')
            ->from($tableName)
            ->andWhere("{$this->field->reference} = :match")
            ->andWhere('lote_deleted is null')
            ->setParameter('match', $value);

        if (!empty($runtimeOpts[ValidatorBase::RUNTIME_OPTS_CURRENT_ROW_ID])) {
            $q->andWhere('id != :rowId')
                ->setParameter('rowId', $runtimeOpts[ValidatorBase::RUNTIME_OPTS_CURRENT_ROW_ID]);
        }

        $unique = empty($q->execute()->fetch(PDO::FETCH_ASSOC));

        $error = null;
        if (!$unique) {
            $error = "{$this->field->name} Value ({$value}) is not unique";
        }

        return new ValidationMessage($this->fieldReference, $unique, $error);

    }
}