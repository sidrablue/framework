<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Object\Entity\Validation\Filters\File;

use BlueSky\Framework\Object\Entity\Validation\Filters\FilterBase;
use BlueSky\Framework\Object\Entity\Validation\ValidationMessage;
use BlueSky\Framework\File\Upload\Validator as FileValidator;

class FileType extends FilterBase
{
    public const REFERENCE = 'FileType';

    protected $allowed;

    public function __construct(string $fieldReference, array $opts = [])
    {
        parent::__construct($fieldReference,$opts);
        $this->allowed = $opts['allowed'];
    }
    protected function validateInternal($value, array $runtimeOpts = []): ValidationMessage
    {
        $valid = true;
        $error = null;
        if($this->allowed){
        $allowedExts = explode(',', $this->allowed);
            $valid = in_array($value,$allowedExts);
        }
        if (!$valid) {
            $error = 'File type not accepted. Valid extensions: '.$this->allowed;
        }
        return new ValidationMessage($this->fieldReference, $valid, $error);
    }

}