<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Object\Entity\Validation\Filters\MultiSelect;

use BlueSky\Framework\Object\Entity\Validation\ValidationMessage;
use BlueSky\Framework\Object\Entity\Validation\Filters\FilterBase;

class IsMandatory extends FilterBase
{
    public const REFERENCE = 'IsMandatory';

    protected function allowNullValue(): bool { return true; }

    protected function validateInternal($value, array $runtimeOpts = []): ValidationMessage
    {
        $success = $value !== null;
        $error = !$success ? 'Value is required' : null;
        if ($success) {
            if (is_array($value)) {
                if (empty($value['selections'])) {
                    $success = false;
                    $error = 'Selection is required';
                }
            } else {
                $success = false;
                $error = 'Invalid input format';
            }
        }
        return new ValidationMessage($this->fieldReference, $success, $error);
    }
}