<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Object\Entity\Validation\Filters\Matrix;

use BlueSky\Framework\Object\Entity\Validation\Filters\FilterBase;
use BlueSky\Framework\Object\Entity\Validation\ValidationMessage;

class InputCount extends FilterBase
{
    public const REFERENCE = 'InputCount';

    /** @var int $minimum - minimum required selection per row  */
    private $minimum;

    /** @var int $minimum - maximum possible selection per row  */
    private $maximum;

    public function __construct(string $fieldReference,array $opts = [])
    {
        parent::__construct($fieldReference,$opts);
        $this->minimum = $opts['minimum'] ?? 0;
        $this->maximum = $opts['maximum'] ?? PHP_INT_MAX;
    }

    protected function validateInternal($value, array $runtimeOpts = []): ValidationMessage
    {
        $validInput = true;
        $error = null;
        foreach ($value['rows'] as $row) {
            $columnCount = count($row['columns']);
            if ($columnCount < $this->minimum || $columnCount > $this->maximum) {
                $validInput = false;
                $error = 'Invalid number of selections made';
                break;
            }
        }
        return new ValidationMessage($this->fieldReference, $validInput, $error);
    }
}