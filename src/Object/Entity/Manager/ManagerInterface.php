<?php
declare(strict_types=1);

namespace BlueSky\Framework\Object\Entity\Manager;

use BlueSky\Framework\Object\Entity\VirtualEntity;

/**
 * Interface ManagerInterface
 */
interface ManagerInterface
{
    /**
     * @param string $entityName
     * @param int $entityId
     * @param array $criteria
     * @return mixed
     */
    public function get(string $entityName, int $entityId, array $criteria = []): ?VirtualEntity;


    /**
     * @param VirtualEntity $entity
     * @return void
     */
    public function add(VirtualEntity $entity): void;

    /**
     * @param VirtualEntity $entity
     * @return mixed
     */
    public function save(VirtualEntity $entity): void;

    /**
     * @return mixed
     */
    public function flush(): void;

    /**
     * @return mixed
     */
    public function clear(): void;

    /**
     * @param VirtualEntity $entity
     * @return void
     */
    public function delete(VirtualEntity $entity): void;

}
