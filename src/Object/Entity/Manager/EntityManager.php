<?php
/**
 * Copyright (c) SidraBlue Pty Ltd
 * Updated: 20 March 2018 at 02:21 PM AWST
 * Updated By: Parham Bakhtiari <parham@sidrablue.com.au>
 */

declare(strict_types = 1);

namespace BlueSky\Framework\Object\Entity\Manager;

use Doctrine\DBAL\Query\QueryBuilder;
use BlueSky\Framework\Entity\Schema\Entity;
use BlueSky\Framework\Object\Entity\Base;
use BlueSky\Framework\Object\Entity\BaseExtensible;
use BlueSky\Framework\Object\Entity\VirtualEntity;
use BlueSky\Framework\Object\StateDb;
use BlueSky\Framework\CodeGen\CodeGen;
//use BlueSky\Framework\Schema\Definition;
use BlueSky\Framework\State\StateTrait;
use ReflectionClass;
use zpt\anno\Annotations;

/**
 * Interface ManagerInterface
 */
class EntityManager
{

    use StateTrait;

    /**
     * @var VirtualEntity[] $entityCache
     * The map of known entities currently managed by the entity manager
     * */
    private $entityCache;

    /**
     * @var Definition[] $definitionCache
     * The map of known entity definition
     */
    private $definitionCache;

    /**
     * @var array $saveCache
     * The map of entities currently waiting to be saved
     * */
    private $saveCache;

    protected function onCreate()
    {
        $this->entityCache = [];
        $this->saveCache = [];
        $this->definitionCache = [];
    }

    /**
     * This is the public facing function for getInternal
     *
     * The associations are described in the getInternal function documentation
     *
     * @see getInternal
     *
     * @param string $entityReference
     * @param int $entityId - maps to the row in the db for that entity. eg for Ref Test, the entity id is the id from sb_test__c
     * @param bool $loadFKeys - load the foreign keys so that the links between the class are objects
     * @param Association[]|null $associations
     * @return null|VirtualEntity
     * @throws \Exception
     */
    public function get(string $entityReference, int $entityId, bool $loadFKeys = true, array $associations = null): ?VirtualEntity
    {
        return $this->getInternal($entityReference, $entityId, $loadFKeys, $associations);
    }

    public function getOrCreateByFilter(string $entityReference, array $filters, ?array $defaultData = null, bool $loadFKeys = true, array $associations = null): VirtualEntity
    {
        if ($entity = $this->getByFilter($entityReference, $filters, $loadFKeys, $associations)) {
            $output = $entity;
        } else {
            $output = $this->createInstance($entityReference);
            if ($defaultData === null) {
                $output->setData($filters);
            } else {
                $output->setData($defaultData);
            }
        }
        return $output;
    }

    public function getByFilter(string $entityReference, array $filters, bool $loadFKeys = true, array $associations = null): ?VirtualEntity
    {
        $entity = $this->createInstance($entityReference);
        $q = $this->getState()->getReadDb()->createQueryBuilder()
            ->select('t.*')
            ->from($entity::getTableName(), 't')
            ->andWhere('t.lote_deleted is null');
        $q = $this->processArrayToQuery($q, $filters);

        return $this->getByQuery($entityReference, $q, $loadFKeys, $associations);
    }

    private function processArrayToQuery(QueryBuilder $q, array $filters): QueryBuilder
    {
        foreach ($filters as $fieldName => $fieldValue) {
            if (is_array($fieldValue)) {
                $maxIndex = count($fieldValue) - 1;
                $index = 0;
                $sqlStatement = '';
                if ($maxIndex >= 0) {
                    foreach ($fieldValue as $fieldNameOr => $fieldValueOr) {
                        /// This block will trim any prefixed _
                        /// This is due to the limitations of the associative arrays.
                        /// Eg. so that email = null and email = '' can be placed in the same or block
                        $charIndex = 0;
                        $fnOrCount = strlen($fieldNameOr);
                        while ($charIndex < $fnOrCount && $fieldNameOr[$charIndex] === '_') { $charIndex++; }
                        if ($charIndex > 0) { $fieldNameOr = substr($fieldNameOr, $charIndex); }

                        $fieldNameOrKey = 't.' . $fieldNameOr;
                        if ($fieldValueOr === null) {
                            $sqlStatement .= $fieldNameOrKey . ' is null';
                        } else {
                            $key = $fieldNameOr . '_' . uniqid();
                            $sqlStatement .= $fieldNameOrKey . ' = :' . $key;
                            $q->setParameter($key, $fieldValueOr);
                        }

                        if ($index++ < $maxIndex) {
                            $sqlStatement .= ' or ';
                        }
                    }
                }
                $q->andWhere($sqlStatement);
            } else {
                $key = $fieldName . '_' . uniqid();
                $fnKey = 't.' . $fieldName;
                $fieldValue === null ? $q->andWhere($fnKey . ' is null') : $q->andWhere($fnKey . ' = :' . $key)->setParameter($key, $fieldValue);
            }
        }
        return $q;
    }

    public function getByQuery(string $entityReference, QueryBuilder $q, bool $loadFKeys = true, array $associations = null): ?VirtualEntity
    {
        $output = null;
        $q->setMaxResults(1);
        $items = $this->getListByQuery($entityReference, $q, $loadFKeys, $associations);
        if (!empty($items)) {
            $output = $items[0];
        }
        return $output;
    }

    /**
     * @param string|Entity $entityReference
     * @return VirtualEntity|null
     */
    public function createInstance($entityReference): ?VirtualEntity
    {
        $output = null;
        $e = $entityReference;
        if (is_string($entityReference)) {
            $e = $this->getState()->getSchema()->getEntityByReference($entityReference);
        }
        if ($e->is_virtual && $this->isEntityResolvable($e)) {
            $className = $e->class_name;
            /** @var VirtualEntity $className */
            $output = new $className($e);
        }
        return $output;
    }

    /**
     * This function will be used for
     *
     * @param string $entityReference
     * @param QueryBuilder $q
     * @param bool $loadFKeys
     * @param array|null $associations
     * @return array
     * @throws \Exception
     * @throws \ReflectionException
     * @throws \zpt\anno\ReflectorNotCommentedException
     */
    public function getListByQuery(string $entityReference, QueryBuilder $q, bool $loadFKeys = true, array $associations = null): array
    {
        $output = [];
        $e = $this->getState()->getSchema()->getEntityByReference($entityReference);
        if (!is_null($e) && $this->isEntityResolvable($e)) {
            $entityClassName = $e->class_name;
            $dataSet = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
            foreach ($dataSet as $item) {
                /** @var VirtualEntity $entityClassName */
                $dataObj = new $entityClassName($e, $item);

                if ($loadFKeys) {
                    $this->loadLinkedEntities($dataObj);
                }

                $this->cache($dataObj);

                $output[] = $dataObj;
            }
        }

        return $output;
    }

    /**
     * This function will attempt to load in an entity to return to the user
     *
     * The loadFKeys (load foreign keys) variable is to specify whether the foreign key references in the entity should
     * also be loaded in and attached to the entity as well
     *
     * The associations is an array that specifies what other EXTERNAL fields should also be loaded and attached
     * This is used when the entity has a reference to another entity which would usually return a collection of entities
     * For example, in the User table, the user has a list of subscriptions which can be found in the User Subscriptions table
     * The associations are in the form of @todo finish off this section of commenting when relations have been implemented
     * @fixme the associations have not been implemented yet
     *
     * The parent references is an array of class names that have been loaded in. This will make sure that there isnt a circular reference
     * in the loading.
     * For example, loading in a user will cause its virtual groups to be loaded in. However, since the user was loaded in first,
     * you dont want to the virtual group to load in its user reference too since they have already been loaded in the parent level
     *
     * @todo maybe look into having a lazy load of foreign key references using the __invoke magic method?
     *
     * @param string $entityReference
     * @param int $entityId
     * @param bool $loadFEntities
     * @param Association[]|null $associations
     * @param string[] $referenceStack
     * @return null|VirtualEntity
     * @throws \Exception
     * @throws \ReflectionException
     * @throws \zpt\anno\ReflectorNotCommentedException
     */
    private function getInternal(string $entityReference, int $entityId, bool $loadFEntities = true, array $associations = null, array &$referenceStack = []): ?VirtualEntity
    {
        $result = null;
        $e = $this->getState()->getSchema()->getEntityByReference($entityReference);
        if (!is_null($e) && $this->isEntityResolvable($e)) {
            $entityClassName = $e->class_name;
            if (!$result = $this->getCached($entityClassName, $entityId)) {
                /** @var VirtualEntity $entityClassName */
                $tableName = $entityClassName::getTableName();
                $q = $this->getState()->getReadDb()->createQueryBuilder();
                $q->select('a.*')
                    ->from($tableName, 'a')
                    ->andWhere('a.id = :id')
                    ->setParameter('id', $entityId)
                    ->andWhere('a.lote_deleted is null')
                    ->setMaxResults(1);

                /** @var VirtualEntity $entityClassName */
                $data = $q->execute()->fetch(\PDO::FETCH_ASSOC);
                $result = new $entityClassName($e, is_array($data) ? $data : null);

                if ($loadFEntities) {
                    $this->loadLinkedEntities($result, $referenceStack);
                }

                $this->cache($result);
            }
        }
        return $result;
    }

    /**
     * This function is responsible of loading and attaching foreign key references to the virtual object
     * It will use the virtual object as an input
     * The annotations of the virtual object are checked for the keys
     *      populateOnLoad and entityClass
     * If the populateOnLoad is set to true and the entityClass is a valid Class reference then that class will be loaded
     *
     * In the attachment of the foreign key object, the new property name will be a replacement of the _id part of the foreign key name
     * IT IS ASSUMED that the foreign keys will all end in _id
     * The code will replace the _id part of the property name and use that as a new property.
     * For example, car_id (int) becomes car (Car)
     * The foreign key property will not be deleted when the referencing object is attached
     *
     * @see loadBaseClassFKey - This is used to load a foreign key reference if its base class is from Base
     * @see Base
     * @see VirtualEntity
     *
     * @param VirtualEntity $input
     * @param array $referenceStack
     */
    public function loadLinkedEntities(VirtualEntity $input, array &$referenceStack = [])
    {
        $reflectionClass = new ReflectionClass($input);
        $reflectionProperties = $reflectionClass->getProperties();
        foreach ($reflectionProperties as $reflectionProperty) {
            $anno = new Annotations($reflectionProperty);
            if ($anno->hasAnnotation('populateOnLoad') && $anno->offsetGet('populateOnLoad') && $anno->hasAnnotation('entityClass')) {
                $entityClass = $anno->offsetGet('entityClass');
                if (!in_array($entityClass, $referenceStack)) {

                    $classParents = class_parents($entityClass); /// Get the list classes that the calling class has extended
                    $indexParent = end($classParents); /// We can successfully differentiate between a Base class and a Virtual class by getting their root class

                    $propertyName = $reflectionProperty->getName(); /// This
                    $entityId = $input->{$propertyName};
                    $idPos = strrpos($propertyName, '_id');
                    $fKeyPropertyName = substr($propertyName, 0, $idPos);
                    $fKeyProperty = null;

                    if ($indexParent === StateDb::class) {
                        $fKeyProperty = $this->loadBaseClassFKey($entityClass, $entityId);
                    } elseif ($indexParent === VirtualEntity::class) {
                        $fKeyPropertyName = $this->get($entityClass, $input->{$reflectionProperty->name}, false, $referenceStack);
                    }

                    $input->{$fKeyPropertyName} = $fKeyProperty;
                    $referenceStack[] = $entityClass;
                }
            }
        }
    }

    /**
     * This function is responsible for creating a new instance of the Base entity class
     * It uses the query builder to populate the fields and not the load function in the Base class
     * This is because if the class is an instance of BaseExtensible then a circular reference may occur since the
     * the class will try to join to its virtual fields which could call this function again
     *
     * @see Base
     * @see BaseExtensible
     *
     * @param string $entityClass
     * @param int $entityId
     * @return null|Base
     */
    private function loadBaseClassFKey(string $entityClass, int $entityId): ?Base
    {
        $output = null;

        if ($entityClass && $entityId) {
            /** @var Base $bClass */
            $bClass = new $entityClass($this->getState());
            /// Use the query builder here since the load method could potentially cause a circular reference
            /// The circular reference is from the joinVirtualTables in the BaseExtensible class
            $q = $this->getState()->getReadDb()->createQueryBuilder();
            $data = $q->select('a.*')
                ->from($bClass->getTableName(), 'a')
                ->andWhere('a.id = :id')
                ->setParameter('id', $entityId)
                ->andWhere('a.lote_deleted is null')
                ->setMaxResults(1)
                ->execute()->fetch(\PDO::FETCH_ASSOC);

            if (!empty($data)) {
                $bClass->setData($data);
                $output = $bClass;
            }
        }

        return $output;
    }

    /**
     * This function will ensure that the class that is going to be instantiated is actually available in the code
     * If the class file does not exist, it will attempt to read the entity from the schema loader and regenerate the source file
     * If the entity does not exist then no further action is taken
     *
     * @param Entity $entity
     * @return bool - true if resolvable
     */
    private function isEntityResolvable(Entity $entity): bool
    {
        $entityClassName = $entity->class_name;
        $result = class_exists($entityClassName);
        if (!$result) { /// Attempt to regenerate the file
            $cg = new CodeGen($this->getState()->accountReference);
            $result = $cg->generateEntityVirtualClass($entity);
            $entityName = substr($entity->class_name, strrpos($entity->class_name, '\\') + 1);
            require_once LOTE_APP_PATH . 'src/Virtual/Entity/' . $this->getState()->accountReferenceFormatted . '/' . $entityName . '.php';
        }

        return $result;
    }

    /**
     * This function will cache the object so that it wont require another database call when the object it being requested again
     *
     * @access public
     * @param VirtualEntity $entity
     * @return void
     */
    public function cache(VirtualEntity $entity): void
    {
        if ($entity->id) {
            // todo look into why clone was used
            $this->entityCache[$this->makeHash(get_class($entity), $entity->id)] = clone $entity;
        }
    }

    /**
     * This is the hash generation for the cache. Providing unique hashes for each object
     *
     * @param string $entity
     * @param int|string $entityId
     * @return string
     */
    private function makeHash(string $entity, $entityId): string {
        return $entity . "::" . $entityId;
    }

    /**
     * This function will commit the changes into memory
     * NOTE: unless $immediately is set to true, this will not save the changes to the database
     * A subsequent call to flush is required to save the changes
     *
     * @see flush
     *
     * @param VirtualEntity $entity
     * @param bool $immediately
     */
    public function save(VirtualEntity $entity, $immediately = false): void
    {
        $this->saveCache[$this->makeHash($entity::getObjectRef(), $entity->id ?? spl_object_hash($entity))] = $entity;
        if ($immediately) {
            $this->flush();
        }
    }

    /**
     * This will save any changes that have been made to the entities into the database
     * Make sure to call save before this function to commit the changes into the queue
     *
     * @see save
     */
    public function flush(): void
    {
        foreach ($this->saveCache as $key => $entityToSave) {
            $cachedEntity = $this->getCached(get_class($entityToSave), $entityToSave->id ?? spl_object_hash($entityToSave));
            if (!$cachedEntity || !$cachedEntity->equals($entityToSave)) {
                $this->saveEntityToDb($entityToSave);
                unset($this->saveCache[$key]);
            }
        }
    }

    /**
     * This function is the interface that will save the changes into the database
     * It uses a reflection to call 2 methods from the VirtualEntity class
     * These functions are set as protected since they should not be called from anywhere else in the system
     * This is the only place where they will be called from
     *
     * @see VirtualEntity::beforeCreate
     * @see VirtualEntity::beforeUpdate
     * @see VirtualEntity::afterCreate
     * @see VirtualEntity::afterUpdate
     *
     * @param VirtualEntity $entity
     */
    private function saveEntityToDb(VirtualEntity $entity): void
    {
        try {
            $class = new ReflectionClass($entity);
            $methodType = $entity->id > 0 ? 'Update' : 'Create';
            $beforeOperationMethod = $class->getMethod('before' . $methodType);
            $beforeOperationMethod->setAccessible(true);
            $beforeOperationMethod->invoke($entity);

            $data = $entity->toArray();
            unset($data['id']);

            if ($entity->id > 0) {
                $this->getState()->getWriteDb()->update($entity::getTableName(), $data, ['id' => $entity->id]);
            } else {
                $this->getState()->getWriteDb()->insert($entity::getTableName(), $data);
                $entity->id = $this->getState()->getWriteDb()->lastInsertId();
            }

            $beforeOperationMethod = $class->getMethod('after' . $methodType);
            $beforeOperationMethod->setAccessible(true);
            $beforeOperationMethod->invoke($entity);
        } catch (\Exception $e) {
            $this->getState()->getLoggers()->getMasterLogger('lote')->error('Could not save entity to db...' . $e->getMessage(), $e->getTrace());
        }
    }

    /**
     * This function will attempt to load an entity from the memory cache
     *
     * @param string $entityName
     * @param integer|string $entityId
     * @return null|VirtualEntity
     */
    private function getCached(string $entityName, $entityId): ?VirtualEntity
    {
        return $this->entityCache[$this->makeHash($entityName, $entityId)] ?? null;
    }

    /**
     * This function will set the object as deleted. It will not save it though, that will require a save and flush
     *
     * @see save
     * @see flush
     *
     * @param VirtualEntity $entity
     * @param bool $immediately
     */
    public function softDelete(VirtualEntity $entity, $immediately = false)
    {
        $entity->softDelete();
        //need to load and delete custom field data
        $this->save($entity, $immediately);
    }

    /**
     * This function will clear both the entity manager cache as well as the save cache
     */
    public function clearCacheAll(): void
    {
        $this->entityCache = [];
        $this->saveCache = [];
    }

    /**
     * This function will remove only certain items from the cache. This is used to slightly optimise the cache
     * It will check whether the cache hash contains the class name then it will remove it
     * One scenario where this can be used is when updating a virtual class such as Car by adding an additional field to it
     * That will cause a cache rebuild on the schema object and here, this function can be called to remove the existing objects in memory that match that class
     * This will cause the unmodified objects to not be removed -> less database queries are required
     *
     * @param string $entityClassName
     */
    public function clearCache(string $entityClassName): void
    {
        foreach ($this->entityCache as $cacheKey => $_) {
            if (strpos($entityClassName, $cacheKey) === 0) {
                unset($this->entityCache[$cacheKey]);

                /// This step is to save a few clock cycles with the starts_with function
                /// This is a micro optimisation to reduce the set that has to be searched in the saveCache
                if (isset($this->saveCache[$cacheKey])) {
                    unset($this->saveCache[$cacheKey]);
                }
            }
        }

        foreach ($this->saveCache as $cacheKey => $_) {
            if (strpos($entityClassName, $cacheKey)) {
                unset($this->saveCache[$cacheKey]);
            }
        }
    }

}
