<?php
/**
 * Copyright (c) BlueSky Digital Labs Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Object\Entity;

use Doctrine\DBAL\Query\QueryBuilder;
use BlueSky\Framework\Entity\Schema\Field as FieldEntity;
use BlueSky\Framework\Object\Container\LazyRemoteList;
use BlueSky\Framework\Object\Entity\Base as BaseEntity;
use BlueSky\Framework\Entity\Schema\Entity as SchemaEntity;
use BlueSky\Framework\Entity\Schema\Relation as RelationEntity;
use BlueSky\Framework\Object\Model\BaseExtensible as BaseExtensibleModel;

/**
 * Class BaseExtensible
 * @package BlueSky\Framework\Object\Entity
 *
 * This class will be used for entities that can be extended through relationships
 */
abstract class BaseExtensible extends BaseEntity
{

    private $virtualGroupVariableNames = [];
    private $lazyListVariableNames = [];

    private $attachedEntityAndFields = [];

    /**
     * This function is responsible for adding the additional query parameters required to load in all virtual groups as well
     *
     * @param QueryBuilder $q
     * @param string $tableAlias
     * @return QueryBuilder
     */
    protected function beforeFetch(QueryBuilder $q, string $tableAlias): QueryBuilder
    {
        $m = new BaseExtensibleModel($this->getState());
        return $m->addCustomFieldsToQuery($q, $this->getSchema(), $tableAlias, $this->attachedEntityAndFields);
    }

    /**
     * @param SchemaEntity|FieldEntity|SchemaEntity[]|FieldEntity[] ...$entitiesOrFields
     * @return BaseExtensible
     */
    public function attachEntitiesOrFields(...$entitiesOrFields): BaseExtensible
    {
        foreach ($entitiesOrFields as $entityOrField) {
            /// Items can be added in as array as well
            if (!is_array($entityOrField)) {
                $entityOrField = [$entityOrField];
            }
            foreach ($entityOrField as $item) {
                $this->attachedEntityAndFields[] = $item;
            }
        }
        return $this;
    }

    public function hydrate(): BaseExtensible
    {
        if ($this->id > 0) {
            $this->clearCachedData();
            $this->load($this->id);
        }
        return $this;
    }

    public function clearRelationEntitiesAndFields(): void
    {
        unset($this->attachedEntityAndFields);
        $this->attachedEntityAndFields = [];
    }

    /**
     * This function is responsible for setting any virtual groups that was loaded in with the query
     *
     * @param array $rawData
     * @return array
     */
    protected function afterFetch(array $rawData): array {
        return $this->setVirtualFields($rawData);
    }

    public function setData($data)
    {
        $newData = $data;
        if (is_array($newData)) {
            $newData = $this->setVirtualFields($newData);
        }
        parent::setData($newData);
        unset($newData);
    }

    /**
     * @param array $data
     * @return array - the key/values that were not used
     */
    private function setVirtualFields(array $data): array
    {
        $entity = $this->getSchema();
        $relations = $entity->getRelations();
        $virtualGroupSchemas = $entity->getChildEntitiesWithRelation(RelationEntity::ALL_RELATIONS);
        $resultsFormatted = $this->virtualFetchFormatter($data);
        foreach ($resultsFormatted as $key => $item) {
            /** @var SchemaEntity $schema */
            $schema = $virtualGroupSchemas[$key];
            if (!empty($schema->class_name)) {
                $relation = $relations->getRelationByToReference($schema->reference);
                if ($relation->type === RelationEntity::ONE_TO_ONE || $relation->type === RelationEntity::OBJECT_REF_ID_OO) {
                    if ($relation->containsValidJoin($item)) {
                        if ($schema->is_virtual) {
                            /** @var VirtualEntity $bg */
                            $bg = $this->getState()->getEntityManager()->createInstance($schema);
                            if ($bg !== null) {
                                $bg->setData($item);
                                /// TODO: look into a better way of loading linked entities (lazy load when needed)
//                            $baseReference = [get_called_class()];
//                            $this->getState()->getEntityManager()->loadLinkedEntities($bg, $baseReference);
                                $this->getState()->getEntityManager()->cache($bg);
                            }
                        } else {
                            $baseClassEntity = $schema->class_name;
                            /** @var BaseEntity $bg */
                            $bg = new $baseClassEntity($this->getState());
                            $bg->setIgnoreModifications(true);
                            $bg->setData($item);
                            $bg->setIgnoreModifications(false);
                        }
                        $this->{$schema->reference} = $bg;
                    }
                } elseif ($relation->type === RelationEntity::ONE_TO_MANY || $relation->type === RelationEntity::MANY_TO_MANY) {
                    if (!empty($item['id'])) {
                        $baseIds = explode(',', $item['id']);
                        $lazyList = new LazyRemoteList($this->getState(), $schema, $baseIds);
                        $this->{$schema->reference} = $lazyList;
                    }
                }
            }
        }
        unset($entity, $relations, $virtualGroupSchemas, $resultsFormatted, $key, $item, $schema, $relation, $bg, $baseIds, $lazyList);
        return $data;
    }

    /**
     * This function is responsible for grouping the data for virtual groups back together
     * The data coming in should be a 1d array -> with virtual groups having the form of table name.field (take note of the period)
     *
     * The schema is used to validate the data and only group the data that is part of the schema
     *
     * @param array $data
     * @return array
     */
    private function virtualFetchFormatter(array &$data): array
    {
        $results = [];
        foreach ($data as $key => $value) {
            if (is_array($value) && ctype_upper($key[0])) {
                $results[$key] = $value;
                unset($data[$key]);
            } else {
                $resultColumnInfo = explode('.', $key);
                if (count($resultColumnInfo) > 1) {
                    unset($data[$key]);

                    $lastIndex = count($resultColumnInfo) - 1;
                    $fieldReference = $resultColumnInfo[$lastIndex];
                    unset($resultColumnInfo[$lastIndex]);

                    $entityReference = implode('\\', $resultColumnInfo);
                    if (!array_key_exists($entityReference, $results)) {
                        $results[$entityReference] = [];
                    }
                    $results[$entityReference][$fieldReference] = $value;
                }
            }
        }
        unset($key, $value, $lastIndex, $fieldReference, $entityReference);
        return $results;
    }

    /**
     * This function will save the base entity as well as saving all the virtual fields attached to the entity
     *
     * @return int - id
     */
    public function save()
    {
        $id = parent::save();
        foreach ($this->virtualGroupVariableNames as $keyName) {
            $val = $this->{$keyName};
            if (!($val instanceof LazyRemoteList)) {
                if ($relation = $this->getSchema()->getRelations()->getRelationByToReference($keyName)) {
                    $toField = $relation->to_field_reference;
                    if (empty($val->{$toField})) {
                        $val->{$toField} = $id;
                    }

                    if ($val instanceof VirtualEntity) {
                        $this->getState()->getEntityManager()->save($this->{$keyName});
                    } elseif ($val instanceof BaseEntity) {
                        $val->save();
                    }
                }
            }
        }
        $this->getState()->getEntityManager()->flush();
        unset($val, $relation, $toField);
        return $id;
    }

    public function delete(bool $strongDelete = false)
    {
        parent::delete($strongDelete);
        $deletedVirtual = false;
        foreach ($this->virtualGroupVariableNames as $name) {
            $group = $this->{$name};
            if ($group) {
                if (is_subclass_of($group, BaseEntity::class)) {
                    $group->delete($strongDelete);
                } else {
                    $this->getState()->getEntityManager()->softDelete($group);
                    $deletedVirtual = true;
                }
            }
        }
        if ($deletedVirtual) {
            $this->getState()->getEntityManager()->flush();
        }
    }

    public function getDataEncoded(?bool $displayable = false, ?string $fieldReference = null)
    {
        $results = parent::getDataEncoded($displayable, $fieldReference);
        foreach ($this->virtualGroupVariableNames as $name) {
            if ($fieldReference === null || $fieldReference === $name) {
                $group = $this->{$name};
                if (is_subclass_of($group, BaseEntity::class)) {
                    /** @var $group BaseEntity */
                    $results[$name] = $group->getDataEncoded($displayable);
                } elseif (is_subclass_of($group, VirtualEntity::class)) {
                    /** @var $group VirtualEntity */
                    $results[$name] = $group->getData(true, $displayable);
                }
            }
        }
        return $results;
    }

    public function getDataAsApiResponse(int $withFlags = FieldEntity::FLAG_EXPORTABLE, bool $transformedToCamelCase = true): array
    {
        $data = parent::getDataAsApiResponse($withFlags, $transformedToCamelCase);
        foreach ($this->virtualGroupVariableNames as $groupNameActual) {
            $groupName = str_replace('\\', '.', $groupNameActual);
            $data[$groupName] = $this->{$groupNameActual}->getDataAsApiResponse($withFlags, $transformedToCamelCase);
        }
        return $data;
    }

    /**
     * This function will return a decoded set of data that represents the entity's data properties
     * It will also add any attached virtual groups to the array being returned
     *
     * @return array
     */
    public function getData(): array
    {
        $results = parent::getData();
        foreach ($this->virtualGroupVariableNames as $name) {
            /** @var $group VirtualEntity */
            $group = $this->{$name};
            if ($group !== null) {
                $results[$name] = $group->getData();
            }
        }
        foreach ($this->lazyListVariableNames as $name) {
            /** @var $lazyList LazyRemoteList */
            $lazyList = $this->{$name};
            if ($lazyList !== null) {
                $results[$name] = $lazyList;
            }
        }
        return $results;
    }

    public function __isset($key)
    {
        if (ctype_upper($key[0])) {
            $output = array_search($key, $this->virtualGroupVariableNames) !== false;
            if ($output === false) {
                $output = array_search($key, $this->lazyListVariableNames) !== false;
            }
        } else {
            $output = parent::__isset($key);
        }
        return $output;
    }

    public function &__get($key)
    {
        $result = null;

        if (ctype_upper($key[0])) {
            if (isset($this->{$key})) {
                $result = $this->{$key};
            } else {
                if ($this->getSchema()->reference === $key) {
                    $result = $this;
                } else {
                    $result = parent::__get($key);
                }
            }
        } else {
            $result = parent::__get($key);
        }
        return $result;
    }

    public function __set($key, $value)
    {
        if (ctype_upper($key[0])) {
            if (is_array($value) || $value instanceof LazyRemoteList) {
                if (!in_array($key, $this->lazyListVariableNames)) {
                    $this->lazyListVariableNames[] = $key;
                }
            } else {
                if (!in_array($key, $this->virtualGroupVariableNames)) {
                    $this->virtualGroupVariableNames[] = $key;
                }
            }
            $this->{$key} = $value;
        } else {
            parent::__set($key, $value);
        }
    }

    public function offsetGet($offset)
    {
        if (is_string($offset)) {
            $keys = explode('.', $offset);
            if (count($keys) === 1) {
                $output = parent::offsetGet($offset);
            } else {
                /** @var Base|VirtualEntity $reference */
                $reference = null;
                foreach ($keys as $index => $key) {
                    if ($index === 0) {
                        $reference = parent::offsetGet($key);
                    } else {
                        $reference = $reference[$key] ?? null;
                    }
                    if ($reference === null) { break; }
                }
                $output = $reference;
            }
        } else {
            $output = parent::offsetGet($offset);
        }
        return $output;
    }

    public function offsetExists($offset): bool
    {
        return $this->offsetGet($offset) !== null;
    }

}
