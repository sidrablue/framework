<?php
namespace BlueSky\Framework\Object\Entity;

use BlueSky\Framework\Entity\Audit\ObjectLog;
use BlueSky\Framework\Entity\Cf\CfField;
use BlueSky\Framework\Entity\Cf\CfGroup;
use BlueSky\Framework\Entity\Cf\CfObject as CfObjectEntity;
use BlueSky\Framework\Model\CfValueGroup as ValueGroupModel;
use BlueSky\Framework\Model\CfValueGroup;
use BlueSky\Framework\State\Web;
use BlueSky\Framework\Object\Data\Field\Factory as FieldFactory;
use BlueSky\Framework\Util\Arrays;
use BlueSky\Framework\Util\Strings;

/**
 * Base class for all entities
 */
abstract class VirtualField extends Base
{

    /**
     * The virtual data for this object, if it exists
     * */
    protected $virtualData = [];

    /**
     * @var bool $virtualDataLoaded
     * The virtual data for this object, if it exists
     * */
    protected $virtualDataLoaded = false;

    /**
     * The reference of the virtual data for this object
     * */
    protected $virtualDataReference = [];

    /**
     * @param Web $state
     * @param int|array $data
     * @param array $virtualData
     * */
    public function __construct($state = null, $data = null, $virtualData = [])
    {
        parent::__construct($state, $data);
        if ($virtualData) {
            $this->virtualData = $virtualData;
        } elseif (isset($this->id) && $this->id) {
            $this->loadVirtualViewData();
        }
    }

    /**
     * Get a single object by id
     *
     * @param integer $id
     * @param bool $includeDeleted - Whether or not to include objects that have been deleted
     * @return Object
     */
    public function load($id, $includeDeleted = false)
    {
        if (parent::load($id, $includeDeleted)) {
            $this->loadVirtualViewData();
        }
        return $this->id;
    }


    /**
     * Load the virtual data for this object
     * @access protected
     * @param bool $loadEmptyGroups - true if empty groups are meant to be loaded
     * @return array
     * */
    protected function loadVirtualViewData($loadEmptyGroups = false)
    {

        die('yes here');
        $s = new \BlueSky\Framework\Service\VirtualField($this->getState());
        $this->virtualData = $s->getVirtualViewData($this->id, $this->getTableName(), $loadEmptyGroups);
        $this->virtualDataLoaded = true;
        return $this->virtualData;
    }

    /**
     * Get the virtual data for this object
     * @access public
     * @param bool $loadEmptyGroups - true if empty groups are meant to be loaded
     * @return array
     * */
    public function getVirtualViewData($loadEmptyGroups = false)
    {
        $this->loadVirtualViewData($loadEmptyGroups);
        return $this->virtualData;
    }

    /**
     * Get the virtual data for this object
     * @access public
     * @param string $fieldReference - the reference of the virtual field
     * @param bool $forceLoad - true to force reload the data
     * @return mixed
     * */
    public function getVirtualFieldValue($fieldReference, $forceLoad = false)
    {
        $result = null;
        if (!$this->virtualDataLoaded || $forceLoad) {
            $allData = $this->getVirtualViewData();
        } else {
            $allData = $this->virtualData;
        }
        foreach ($allData as $group) {
            /**
             * @param CfGroup $group
             * */
            foreach ($group->getChildFields() as $field) {
                /** @param CfField $field */
                if ($field->reference == $fieldReference) {
                    $result = $field->getDataDefinition()->getValue();
                    break;
                }
            }
        }
        return $result;
    }

    /**
     * Get the virtual data for this object
     * @access public
     * @param string $reference - the object reference
     * @return array
     * */
    public function getVirtualEditData($reference = 'user')
    {
        if ($this->virtualDataReference) {
            $reference = $this->virtualDataReference;
        }
        $s = new \BlueSky\Framework\Service\VirtualField($this->getState());
        return $s->getVirtualEditData($this->id, $this->getTableName(), $reference);
    }

    /**
     * Set the virtual data for this object
     * @param array $virtualData
     * @param array $editableGroups - the
     * @param string $tablePrefix - the prefix of the table holding the data
     * @param boolean $syncData - true if this data set is considered the full set and missing items are to be removed
     * @return void
     * */
    public function updateVirtualData($virtualData, $editableGroups = [], $tablePrefix = 'sb__', $syncData = true)
    {
        $groupsWithData = [];
        if (is_array($virtualData)) {
            foreach ($virtualData as $id => $value) {
                $virtualField = new CfField($this->getState());
                if ($virtualField->load($id)) {
                    if (empty($editableGroups) || array_search($virtualField->group_id, $editableGroups) !== false) {
                        $groupsWithData[] = $virtualField->group_id;
                        $fieldType = FieldFactory::createInstance($this->getState(), $virtualField->field_type);
                        $fieldType->setDefinition($virtualField);
                        $oldValue = $this->getVirtualFieldValue($virtualField->reference);

                        if (Strings::isJson($value) && is_array($oldValue)) {
                            $oldValue = json_encode($oldValue);
                        }


                        if($oldValue != $value) {
                            if ($virtualField->field_type != 'file' || ($value === null || $value > 0)) {
                                $fieldType->updateValue($this->id, $virtualField->id, $value, $tablePrefix . 'cf_value');
                            }
                            $this->logCfFieldChange($id, $oldValue, $value);
                        }
                    }
                }
            }
        }
        if ($syncData) {
            $this->syncVirtualGroups($groupsWithData, $editableGroups, $tablePrefix);
            $this->removeUnusedFields($tablePrefix);
        } else {
            $this->addObjectValueGroup($groupsWithData);
        }
    }

    /**
     * Add a log entry for a change to a virtual field value
     * @param int $fieldId - the ID of the virtual field
     * @param string $newValue - the new value of the field
     * @param string $oldValue - the old value of the field
     * @return void
     * */
    private function logCfFieldChange($fieldId, $newValue, $oldValue)
    {
        if(!$this->auditObjectLog instanceof ObjectLog) {
            $this->logAudit(true);
        }
        if($this->auditObjectLog instanceof ObjectLog) {
            $this->getState()->getAudit()->addFieldLog('cf_field', $this->auditObjectLog, $fieldId, $newValue, $oldValue);
        }
    }

    /**
     * Add a log entry for a new addition to a virtual field group
     * @param int $groupId - the ID of the virtual field group
     * @return void
     * */
    private function logCfGroupLink($groupId)
    {
        $this->logCfGroupAssociation($groupId, 'link');
    }

    /**
     * Add a log entry for a new addition to a virtual field group
     * @param int $groupId - the ID of the virtual field group
     * @return void
     * */
    private function logCfGroupUnlink($groupId)
    {
        $this->logCfGroupAssociation($groupId, 'unlink');
    }

    /**
     * Add a log entry for a new association to a virtual field group
     * @param int $groupId - the ID of the virtual field group
     * @param string $type - the type of association, 'link' or 'unlink'
     * @return void
     * */
    private function logCfGroupAssociation($groupId, $type='link')
    {
        if(!$this->auditObjectLog instanceof ObjectLog) {
            $this->logAudit(true);
        }
        if($this->auditObjectLog instanceof ObjectLog) {
            $this->getState()->getAudit()->addFieldLog($type, $this->auditObjectLog, 'cf_group', $groupId);
        }
    }

    public function addObjectValueGroup($groupIds)
    {
        $m = new CfValueGroup($this->getState());
        $m->setTableName($this->getTableName() . '__cf_value_group');
        foreach ($groupIds as $gid) {
            if (!$m->hasVirtualGroup($this->id, $gid)) {
                $m->addObjectGroups($this->id, [$gid]);
                $this->logCfGroupLink($gid);
            }
        }
    }

    /**
     * Remove fields that belong to field groups that the object is no longer linked to
     * @access private
     * @param string $tablePrefix - the prefix of the table in which data is found for this entity
     * @return void
     * */
    private function removeUnusedFields($tablePrefix)
    {
        $m = new ValueGroupModel($this->getState());
        $m->setTableName($tablePrefix . 'cf_value_group');
        $m->removeUnusedFields($this->id);
    }

    /**
     * Remove any unused cf groups from this entity, and remove any unused fields that belong to those groups
     * @access private
     * @param array $groupsWithData - the cf group ID's of fields that have data
     * @param array $editableGroups - the list of cf group ID's that can be edited in this request
     * @param string $tablePrefix - the prefix of the table in which data is found for this entity
     * @return void
     * */
    private function syncVirtualGroups($groupsWithData, $editableGroups, $tablePrefix)
    {
        $groupsWithData = array_unique($groupsWithData);

        $m = new ValueGroupModel($this->getState());
        $m->setTableName($tablePrefix . 'cf_value_group');
        $currentGroups = $m->getObjectGroups($this->id);
        $currentGroupIds = Arrays::getFieldValuesFrom2dArray($currentGroups, 'group_id');

        $missingGroups = array_diff($groupsWithData, $currentGroupIds);
        if (!empty($editableGroups)) {
            $missingGroups = array_intersect($missingGroups, $editableGroups);
        }
        $m->addObjectGroups($this->id, $missingGroups);
        foreach($missingGroups as $v) {
            $this->logCfGroupLink($v);
        }

        $removedGroups = array_diff($currentGroupIds, $groupsWithData);
        if (!empty($editableGroups)) {
            $removedGroups = array_intersect($missingGroups, $editableGroups);
        }
        $m->deleteGroupsByObject($this->id, $removedGroups);
        foreach($removedGroups as $v) {
            $this->logCfGroupUnlink($v);
        }
    }

    /**
     * Check if this object contains data for a specified virtual field group
     * @access public
     * @param int $groupId - the virtual group id
     * @return boolean
     * */
    public function hasVirtualGroup($groupId)
    {
        $result = false;
        if (is_array($this->virtualData)) {
            foreach ($this->virtualData as $v) {
                if ($v->id == $groupId) {
                    $result = true;
                    break;
                }
            }
        }
        return $result;
    }

    /**
     * Get all class properties which should map to database fields
     * @return array
     */
    protected function getFields()
    {
        $fields = parent::getFields();
        //$fields['_virtual'] = $this->getVirtualViewData();
        return $fields;
    }

    /**
     * Update properties for this object
     * @param array $properties
     * @return boolean
     */
    public function update(Array $properties)
    {
        parent::update($properties);
    }


    /**
     * Update a single property
     *
     * @param string $key Property name
     * @param mixed $value Property value
     *
     * @return boolean
     */
    public function updateProperty($key, $value)
    {
        $result = false;
        if ($this->id > 0 && property_exists($this, $key)) {
            $this->$key = $value;
            $this->getState()->getWriteDb()->update($this->getTableName(), [$key => $value], ['id' => $this->id]);
            $this->save();
            $result = true;
        }
        return $result;
    }

    /**
     * Delete this object
     * @param bool $strongDelete - Whether or not to delete the table object completely
     * @return void
     */
    public function delete($strongDelete = false)
    {
        parent::delete($strongDelete);
        $this->reset();
    }

    /**
     * Get the edit for this object as an array
     * @return array
     * */
    public function getEditData()
    {
        $data = parent::getData();
        if ($data['id']) {
            $data['_virtual'] = $this->getVirtualEditData();
        }
        return $data;
    }

    /**
     * Get the virtual fields for this object
     *
     * Note: default value added to allow input of other cf object types without breaking existing code
     * @todo: refactor existing code to use "getVirtualFields" with an $id input. Then, remove default value.
     *
     * @param int|string $id - id or reference of the cf object
     * @access public
     * @return array
     * */
    public function getVirtualFields($id = 'user')
    {
        $e = new CfObjectEntity($this->getState());
        $e->load($id, true, false, true);
        return $e->getGroups();
    }

    /**
     * Get the fields
     * @return array
     * */
    public function getImportFieldList($id = 'user')
    {
        $result = [];
        $groups = $this->getVirtualFields($id);

        foreach ($groups as $v) {
            $fields = [];
            /**
             * @var CfGroup $v
             * */
            foreach ($v->getChildFields() as $f) {
                /**
                 * @var CfField $f
                 * */
                if ($f->getDataDefinition()->supportsImporting()) {
                    $fields[] = $f->getData();
                }
            }
            if (!empty($fields)) {
                $g = $v->getData();
                $g['_fields'] = $fields;
                $result[] = $g;
            }
        }
        return $result;
    }

}
