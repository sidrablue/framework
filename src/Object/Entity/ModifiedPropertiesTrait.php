<?php
namespace BlueSky\Framework\Object\Entity;

trait ModifiedPropertiesTrait {

    private function setModified($field, $value) {
        if(!isset($this->modifiedProperties)) {
            $this->modifiedProperties = [];
        }
        $this->modifiedProperties[$field] = $value;
    }

    private function setUnmodified($field) {
        if(isset($this->modifiedProperties) && array_key_exists($field, $this->modifiedProperties)) {
            unset($this->modifiedProperties[$field]);
            if(count($this->modifiedProperties)==0) {
                unset($this->modifiedProperties);
            }
        }
    }

}