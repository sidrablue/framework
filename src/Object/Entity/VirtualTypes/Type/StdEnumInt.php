<?php
/**
 * Copyright (c) BlueSky Digital Labs Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Object\Entity\VirtualTypes\Type;

/**
 * Class StdEnumInt
 * @package BlueSky\Framework\Object\Entity\VirtualTypes\Type
 */
class StdEnumInt extends Base
{
    
    function encode(): string { return (string)$this->getValue(); }

    function decode()
    {
        $output = null;
        if (is_numeric($this->getValue())) {
            if ($this->getValue() == 1) {
                $output = 1;
            } elseif ($this->getValue() == -1) {
                $output = -1;
            }
        } elseif (is_string($this->getValue())) {
            $val = filter_var(strtolower($this->getValue()), FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
            if ($val !== null) {
                $output = $val ? 1 : 0;
            }
        }
        return $output;
    }

    public function doesTypesMatch(): bool { return $this->getValue() === 0 || $this->getValue() === 1; }

}
