<?php
/**
 * Copyright (c) BlueSky Digital Labs Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Object\Entity\VirtualTypes\Type;

use DateTime;

/**
 * Class StdDate
 * @package BlueSky\Framework\Object\Entity\VirtualTypes\Type
 */
class StdDate extends Base
{

    public const MINIMUM_DATE_STRING = "1900-01-01";

    public const MINIMUM_YEAR = 1900;

    function encode(): string
    {
        /** @var DateTime $value */
        $value = $this->getValue();
        if ($value instanceof \DateTimeInterface) {
            $value = $value->format('Y-m-d');
        }

        //Return empty string if the date is invalid
        if (is_string($value) && $value < self::MINIMUM_DATE_STRING) {
            $value = '';
        }

        return $value;
    }

    function decode()
    {
        $value = $this->getValue();
        $output = null;

        //Return on value being a DateTime object
        if ($value instanceof \DateTimeInterface) {

            //If DateTime is invalid, return null
            if ($value->format('Y-m-d') >= self::MINIMUM_DATE_STRING) {
                $output = $value;
            }

        //Return on value being a string
        } else if (is_string($value)) {

            if ($format = $this->guessFormat($value)) {
                $d = DateTime::createFromFormat($format, $value);
                if ($d->format('Y-m-d') >= self::MINIMUM_DATE_STRING) {
                    $output = $d;
                }
            }

        }

        return $output;
    }

    public function displayFormat(): string
    {
        $output = '';
        if ($this->getValue() !== null) {
            $value = $this->getValue();
            if (is_string($value)) {
                try {
                    $value = new DateTime($value);
                } catch (\Exception $e) { }
            }
            if ($value instanceof \DateTimeInterface) {
                $output = $value->format('d/m/Y');
            }
        }
        return $output;
    }

    private function guessFormat(string $value): ?string {
        $output = null;
        $value = trim($value);
        $patterns = [
            '/\d{4}-\d{2}-\d{2}/' => 'Y-m-d',
            '/\d{2}\/\d{2}\/\d{4}/' => 'd/m/Y',
            '/\d{2}\/\d{2}\/\d{2}/' => 'd/m/y',
            '/\d{2}\/\d{1}\/\d{4}/' => 'd/n/Y',
            '/\d{1}\/\d{1}\/\d{4}/' => 'j/n/Y',
            '/\d{1}\/\d{2}\/\d{4}/' => 'j/m/Y',
            '/\d{2}\/\d{1}\/\d{2}/' => 'd/n/y',
            '/\d{1}\/\d{1}\/\d{2}/' => 'j/m/y',
            '/\d{1}\/\d{2}\/\d{2}/' => 'j/m/y',
        ];
        foreach ($patterns as $pattern => $format) {
            if (preg_match($pattern, $value)) {
                $output = $format;
                break;
            }
        }
        return $output;
    }

    public function apiEncode() { return $this->encode(); }

    public function doesTypesMatch(): bool
    {
        return $this->getValue() instanceof \DateTimeInterface;
    }

}
