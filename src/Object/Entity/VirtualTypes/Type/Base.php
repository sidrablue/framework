<?php
/**
 * Copyright (c) BlueSky Digital Labs Pty Ltd
 */

declare(strict_types=1);
namespace BlueSky\Framework\Object\Entity\VirtualTypes\Type;

/**
 * Class Base
 * @package BlueSky\Framework\Object\Entity\VirtualTypes\Type
 */
abstract class Base
{
    private $value = null;

    /**
     * Base constructor.
     * @param mixed $value
     */
    public function __construct($value) { $this->value = $value; }

    /**
     * This function is responsible for turning the object into a string which can be saved by the database
     *
     * @return string
     */
    public abstract function encode(): string;

    /**
     * This function is responsible for turning the string into an object which can be used by the runtime code
     *
     * @return mixed
     */
    public abstract function decode();

    public function apiEncode() { return $this->getValue(); }

    /**
     * This function is responsible for checking if the input and output have the same type or not
     * @return bool - true if types match
     */
    public abstract function doesTypesMatch(): bool;

    public function displayFormat(): string { return $this->encode(); }

    /** @return mixed|null */
    public function getValue() { return $this->value; }

    protected function setValue($value): void { $this->value = $value; }

}
