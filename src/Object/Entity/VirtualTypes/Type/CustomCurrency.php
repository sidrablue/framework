<?php
/**
 * Copyright (c) BlueSky Digital Labs Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Object\Entity\VirtualTypes\Type;

class CustomCurrency extends StdDecimal
{

    function decode() {
        $output = null;
        if (is_numeric($this->getValue())) {
            $output = floatval($this->getValue());
        } else {
            $matched = [];
            preg_match('/[\.0-9]+/', $this->getValue(), $matched);
            if (!empty($matched)) {
                $val = $matched[0];
                if (is_numeric($val)) {
                    $output = floatval($val);
                }
            }
        }
        return $output;
    }

}