<?php
/**
 * Copyright (c) BlueSky Digital Labs Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Object\Entity\VirtualTypes\Type;

class StdDecimal extends Base
{

    function encode(): string { return (string) $this->getValue(); }

    function decode() { return floatval($this->getValue()); }

    public function doesTypesMatch(): bool { return is_numeric($this->getValue()); }

}