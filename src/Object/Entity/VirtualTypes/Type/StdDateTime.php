<?php
/**
 * Copyright (c) BlueSky Digital Labs Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Object\Entity\VirtualTypes\Type;

use DateTime;

/**
 * Class StdDateTime
 * @package BlueSky\Framework\Object\Entity\VirtualTypes\Type
 */
class StdDateTime extends Base
{
    public const MINIMUM_DATETIME_STRING = "0000-01-01 00:00:00";
    public const MINIMUM_DATE_STRING = "1900-01-01";
    public const MINIMUM_YEAR = 1900;

    function encode(): string
    {
        /** @var \DateTime $value */
        $value = $this->getValue();
        if ($value instanceof \DateTimeInterface) {
            $value = $value->format('Y-m-d H:i:s');
        }

        //Check date is not below minimum
        if (is_string($value) && $value < self::MINIMUM_DATETIME_STRING) {
            $value = '';
        }
        return $value;
    }

    function decode()
    {
        $output = null;
        $value = $this->getValue();

        //Return on value being a DateTime object
        if ($value instanceof \DateTimeInterface) {

            //If DateTime is invalid, return null
            if ($value->format('Y-m-d H-i-s') >= self::MINIMUM_DATETIME_STRING) {
                $output = $value;
            }

            //Return on value being a string
        } else if (is_string($value)) {

            if ($value >= self::MINIMUM_DATETIME_STRING) {
                $output = new \DateTime($value);
            }
        }

        return $output;
    }

    public function displayFormat(): string
    {
        $output = '';
        if ($this->getValue() !== null) {
            $value = $this->getValue();
            if (is_string($value)) {
                try {
                    $value = new DateTime($value);
                } catch (\Exception $e) { }
            }
            if ($value instanceof \DateTimeInterface) {
                $output = $value->format('jS F Y g:ia');
            }
        }
        return $output;
    }

    public function apiEncode() { return $this->encode(); }

    public function doesTypesMatch(): bool
    {
        return $this->getValue() instanceof \DateTimeInterface;
    }

}
