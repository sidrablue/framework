<?php
/**
 * Copyright (c) BlueSky Digital Labs Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Object\Entity\VirtualTypes\Type;

/**
 * Class StdInt
 * @package BlueSky\Framework\Object\Entity\VirtualTypes\Type
 */
class StdInt extends Base
{

    function encode(): string { return (string) $this->getValue(); }

    function decode() { return (int) $this->getValue(); }

    public function doesTypesMatch(): bool { return is_int($this->getValue()); }

}
