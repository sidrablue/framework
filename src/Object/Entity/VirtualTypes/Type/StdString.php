<?php
/**
 * Copyright (c) BlueSky Digital Labs Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Object\Entity\VirtualTypes\Type;

/**
 * Class StdString
 * @package BlueSky\Framework\Object\Entity\VirtualTypes\Type
 */
class StdString extends Base
{
    
    function encode(): string { return (string) $this->getValue(); }

    function decode() { return (string) $this->getValue(); }

    public function doesTypesMatch(): bool { return is_string($this->getValue()); }

}
