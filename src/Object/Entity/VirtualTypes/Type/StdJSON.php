<?php
/**
 * Copyright (c) BlueSky Digital Labs Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Object\Entity\VirtualTypes\Type;

use BlueSky\Framework\Util\Strings;

/**
 * Class StdJSON
 * @package BlueSky\Framework\Object\Entity\VirtualTypes\Type
 */
class StdJSON extends Base
{
    
    function encode(): string
    {
        $output = '';
        if (is_array($this->getValue())) {
            $output = json_encode($this->getValue());
        } elseif (is_string($this->getValue()) && Strings::isJson($this->getValue())) {
            $output = $this->getValue();
        }
        return $output;
    }

    function decode() { return json_decode($this->getValue(), true); }

    public function doesTypesMatch(): bool { return is_array($this->getValue()); }

}
