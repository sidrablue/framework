<?php
/**
 * Copyright (c) BlueSky Digital Labs Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Object\Entity\VirtualTypes\Type;

/**
 * Class StdBool
 * @package BlueSky\Framework\Object\Entity\VirtualTypes\Type
 */
class StdBool extends Base
{
    
    function encode(): string { return $this->getValue() ? "1" : "0"; }

    function decode()
    {
        if (is_string($this->getValue())) {
            $this->setValue(strtolower($this->getValue()));
        }
        return filter_var($this->getValue(), FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
    }

    public function doesTypesMatch(): bool { return is_bool($this->getValue()); }

}
