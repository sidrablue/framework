<?php
/**
 * Copyright (c) BlueSky Digital Labs Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Object\Entity\VirtualTypes\Type;

use BlueSky\Framework\Util\Strings;

/**
 * Class StdJSON
 * @package BlueSky\Framework\Object\Entity\VirtualTypes\Type
 */
class CustomSingleSelect extends StdJSON
{

    public function displayFormat(): string
    {
        $output = '';
        $value = null;
        if (is_array($this->getValue())) {
            $value = $this->getValue();
        } elseif (is_string($this->getValue()) && Strings::isJson($this->getValue())) {
            $value = json_decode($this->getValue(), true);
        }
        if ($value !== null) {
            $output = $value['name'] ?? $value['value'];
        }
        return $output;
    }

}
