<?php
/**
 * Copyright (c) BlueSky Digital Labs Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Object\Entity\VirtualTypes\Type;

/**
 * Class StdDateTimeImmutable
 * @package BlueSky\Framework\Object\Entity\VirtualTypes\Type
 */
class StdDateTimeImmutable extends Base
{
    function encode(): string
    {
        /** @var \DateTime $value */
        $value = $this->getValue();
        if ($value instanceof \DateTimeInterface) {
            $value = $value->format('Y-m-d H:i:s');
        }

        //Return empty string if the date is invalid
        if (is_string($value) && $value < StdDateTime::MINIMUM_DATETIME_STRING) {
            $value = '';
        }
        return $value;
    }

    function decode()
    {
        $output = null;
        $value = $this->getValue();

        //Return on value being a DateTime object
        if ($value instanceof \DateTimeInterface) {

            //If DateTime is invalid, return null
            if ($value->format('Y-m-d H-i-s') >= StdDateTime::MINIMUM_DATETIME_STRING) {
                $output = $value;
            }

            //Return on value being a string
        } else if (is_string($value)) {

            //If DateTime is invalid, return null
            if ($value >= StdDateTime::MINIMUM_DATETIME_STRING) {
                $output = new \DateTimeImmutable($value);
            }
        }

        return $output;
    }

    public function displayFormat(): string
    {
        $output = '';
        if ($this->getValue() !== null) {
            $value = $this->getValue();
            if (is_string($value)) {
                try {
                    $value = new DateTime($value);
                } catch (\Exception $e) { }
            }
            if ($value instanceof \DateTimeInterface) {
                $output = $value->format('jS F Y g:ia');
            }
        }
        return $output;
    }

    public function apiEncode() { return $this->encode(); }

    public function doesTypesMatch(): bool
    {
        return $this->getValue() instanceof \DateTimeInterface;
    }

}
