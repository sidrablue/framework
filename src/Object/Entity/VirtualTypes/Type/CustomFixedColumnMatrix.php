<?php
/**
 * Copyright (c) BlueSky Digital Labs Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Object\Entity\VirtualTypes\Type;

use BlueSky\Framework\Util\Strings;

/**
 * @package BlueSky\Framework\Object\Entity\VirtualTypes\Type
 */
class CustomFixedColumnMatrix extends StdJSON
{

    public function displayFormat(): string
    {
        $output = '';
        $value = null;
        if (is_array($this->getValue())) {
            $value = $this->getValue();
        } elseif (is_string($this->getValue()) && Strings::isJson($this->getValue())) {
            $value = json_decode($this->getValue(), true);
        }
        if ($value !== null) {
            $maxIndex = count($value) - 1;
            foreach ($value as $rowIndex => $row) {
                $maxRowIndex = count($row) - 1;
                foreach ($row as $index => $columnValue) {
                    $output .= $columnValue;
                    if ($index !== $maxRowIndex) {
                        $output .= ', ';
                    }
                }
                if ($rowIndex !== $maxIndex) {
                    $output .= '; ';
                }
            }
        }
        return $output;
    }

}
