<?php
/**
 * Copyright (c) BlueSky Digital Labs Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Object\Entity\VirtualTypes;

use BlueSky\Framework\Object\Entity\VirtualTypes\Type\Base;

/**
 * Class Factory
 * @package BlueSky\Framework\Object\Entity\VirtualTypes
 */
final class Factory
{
    public const STANDARD_DATA_INT = 'int';
    public const STANDARD_DATA_INTEGER = 'integer';                             /// This is used as legacy support
    public const STANDARD_DATA_STRING = 'string';
    public const STANDARD_DATA_URL = 'url';
    public const STANDARD_DATA_PASSWORD = 'password';
    public const STANDARD_DATA_TELEPHONE = 'telephone';
    public const STANDARD_DATA_FILE = 'file';
    public const STANDARD_DATA_TEXT = 'text';
    public const STANDARD_DATA_RICHTEXT = 'richtext';
    public const STANDARD_DATA_TEXTAREA = 'textarea';
    public const STANDARD_DATA_TIME = 'time';
    public const STANDARD_DATA_DATETIME = 'datetime';
    public const STANDARD_DATA_DATETIME_IMMUTABLE = 'datetime_immutable';
    public const STANDARD_DATA_BOOL = 'bool';
    public const STANDARD_DATA_BOOLEAN = 'boolean';                             /// This is used as legacy support
    public const STANDARD_DATA_JSON = 'json';
    public const STANDARD_DATA_DECIMAL = 'decimal';

    public const CUSTOM_DATA_FKEY = 'fkey';
    public const CUSTOM_DATA_PRIMARY_KEY = 'primary_key_int';
    public const CUSTOM_DATA_DATE = 'date';
    public const CUSTOM_DATA_DATE_IMMUTABLE = 'date_immutable';
    public const CUSTOM_DATA_ENUM_INTEGER = 'enum_integer';
    public const CUSTOM_DATA_ENUM = 'enum';

    public const CUSTOM_DATA_EMAIL = 'email';
    public const CUSTOM_DATA_SINGLE_SELECT = 'single_select';
    public const CUSTOM_DATA_CURRENCY = 'currency';
    public const CUSTOM_DATA_MULTI_SELECT = 'multi_select';
    public const CUSTOM_DATA_RADIOBUTTON = 'radiobutton';
    public const CUSTOM_DATA_CHECKBOX = 'checkbox';
    public const CUSTOM_DATA_MATRIX = 'matrix';
    public const CUSTOM_DATA_FIXED_COLUMN_MATRIX = 'fixed_column_matrix';

    public const SPECIAL_DATA_HEADING = 'heading';
    public const SPECIAL_DATA_HEADER = 'header';
    public const SPECIAL_DATA_MESSAGE = 'message';
    public const SPECIAL_DATA_FOOTER = 'footer';
    public const SPECIAL_DATA_FOOTING = 'footing';
    public const SPECIAL_DATA_IMAGE = 'image';
    public const SPECIAL_DATA_RECAPTCHA = 'recaptcha';
    public const SPECIAL_DATA_CANVAS = 'canvas';

    public const CUSTOM_DATA_PUBLIC_GROUP = 'public_group';
    public const CUSTOM_DATA_COUNTRY = 'country';

    private const MAP = [
        self::STANDARD_DATA_INT => 'BlueSky\Framework\Object\Entity\VirtualTypes\Type\StdInt',
        self::STANDARD_DATA_INTEGER => 'BlueSky\Framework\Object\Entity\VirtualTypes\Type\StdInt',
        self::CUSTOM_DATA_FKEY => 'BlueSky\Framework\Object\Entity\VirtualTypes\Type\StdInt',
        self::CUSTOM_DATA_PRIMARY_KEY => 'BlueSky\Framework\Object\Entity\VirtualTypes\Type\StdInt',

        self::CUSTOM_DATA_ENUM_INTEGER => 'BlueSky\Framework\Object\Entity\VirtualTypes\Type\StdEnumInt',

        self::STANDARD_DATA_DECIMAL => 'BlueSky\Framework\Object\Entity\VirtualTypes\Type\StdDecimal',

        self::STANDARD_DATA_STRING => 'BlueSky\Framework\Object\Entity\VirtualTypes\Type\StdString',
        self::STANDARD_DATA_PASSWORD => 'BlueSky\Framework\Object\Entity\VirtualTypes\Type\StdString',
        self::STANDARD_DATA_URL => 'BlueSky\Framework\Object\Entity\VirtualTypes\Type\StdString',
        self::STANDARD_DATA_FILE => 'BlueSky\Framework\Object\Entity\VirtualTypes\Type\StdString',
        self::STANDARD_DATA_TELEPHONE => 'BlueSky\Framework\Object\Entity\VirtualTypes\Type\StdString',
        self::CUSTOM_DATA_ENUM => 'BlueSky\Framework\Object\Entity\VirtualTypes\Type\StdString',
        self::CUSTOM_DATA_EMAIL => 'BlueSky\Framework\Object\Entity\VirtualTypes\Type\StdString',
        self::CUSTOM_DATA_COUNTRY => 'BlueSky\Framework\Object\Entity\VirtualTypes\Type\StdString',

        self::SPECIAL_DATA_RECAPTCHA => 'BlueSky\Framework\Object\Entity\VirtualTypes\Type\StdString',
        self::SPECIAL_DATA_HEADER => 'BlueSky\Framework\Object\Entity\VirtualTypes\Type\StdString',
        self::SPECIAL_DATA_HEADING => 'BlueSky\Framework\Object\Entity\VirtualTypes\Type\StdString',
        self::SPECIAL_DATA_MESSAGE => 'BlueSky\Framework\Object\Entity\VirtualTypes\Type\StdString',
        self::SPECIAL_DATA_FOOTER => 'BlueSky\Framework\Object\Entity\VirtualTypes\Type\StdString',
        self::SPECIAL_DATA_FOOTING => 'BlueSky\Framework\Object\Entity\VirtualTypes\Type\StdString',
        self::SPECIAL_DATA_IMAGE => 'BlueSky\Framework\Object\Entity\VirtualTypes\Type\StdString',

        self::STANDARD_DATA_TEXT => 'BlueSky\Framework\Object\Entity\VirtualTypes\Type\StdText',
        self::STANDARD_DATA_TEXTAREA => 'BlueSky\Framework\Object\Entity\VirtualTypes\Type\StdText',
        self::STANDARD_DATA_RICHTEXT => 'BlueSky\Framework\Object\Entity\VirtualTypes\Type\StdText',
        self::SPECIAL_DATA_CANVAS => 'BlueSky\Framework\Object\Entity\VirtualTypes\Type\StdText',

        self::STANDARD_DATA_TIME => 'BlueSky\Framework\Object\Entity\VirtualTypes\Type\StdTime',
        self::STANDARD_DATA_DATETIME => 'BlueSky\Framework\Object\Entity\VirtualTypes\Type\StdDateTime',
        self::CUSTOM_DATA_DATE => 'BlueSky\Framework\Object\Entity\VirtualTypes\Type\StdDateTime',
        self::STANDARD_DATA_DATETIME_IMMUTABLE => 'BlueSky\Framework\Object\Entity\VirtualTypes\Type\StdDateTimeImmutable',
        self::CUSTOM_DATA_DATE_IMMUTABLE => 'BlueSky\Framework\Object\Entity\VirtualTypes\Type\StdDateTimeImmutable',

        self::STANDARD_DATA_BOOL => 'BlueSky\Framework\Object\Entity\VirtualTypes\Type\StdBool',
        self::STANDARD_DATA_BOOLEAN => 'BlueSky\Framework\Object\Entity\VirtualTypes\Type\StdBool',

        self::STANDARD_DATA_JSON => 'BlueSky\Framework\Object\Entity\VirtualTypes\Type\StdJSON',
        self::CUSTOM_DATA_CURRENCY => 'BlueSky\Framework\Object\Entity\VirtualTypes\Type\StdJSON',
        self::CUSTOM_DATA_MATRIX => 'BlueSky\Framework\Object\Entity\VirtualTypes\Type\StdJSON',
        self::CUSTOM_DATA_PUBLIC_GROUP => 'BlueSky\Framework\Object\Entity\VirtualTypes\Type\StdJSON',

        self::CUSTOM_DATA_FIXED_COLUMN_MATRIX => 'BlueSky\Framework\Object\Entity\VirtualTypes\Type\CustomFixedColumnMatrix',

        self::CUSTOM_DATA_SINGLE_SELECT => 'BlueSky\Framework\Object\Entity\VirtualTypes\Type\CustomSingleSelect',
        self::CUSTOM_DATA_RADIOBUTTON => 'BlueSky\Framework\Object\Entity\VirtualTypes\Type\CustomSingleSelect',

        self::CUSTOM_DATA_MULTI_SELECT => 'BlueSky\Framework\Object\Entity\VirtualTypes\Type\CustomMultiSelect',
        self::CUSTOM_DATA_CHECKBOX => 'BlueSky\Framework\Object\Entity\VirtualTypes\Type\CustomMultiSelect',
    ];

    private function __construct() { }
    
    private function __clone() { }

    /**
     * Even though the output can be null, we assume it never will be
     * Always check for the dataValue before passing it through the Factory
     *
     * @param string $dataType
     * @param mixed $dataValue
     * @return Base
     */
    public static function createInstance(string $dataType, $dataValue): Base
    {
        $output = null;
        if (isset(self::MAP[$dataType])) {
            $typeClass = self::MAP[$dataType];
            $output = new $typeClass($dataValue);
        }

        return $output;
    }
}
