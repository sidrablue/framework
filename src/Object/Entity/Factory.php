<?php
namespace BlueSky\Framework\Object\Entity;

class Factory
{

    private static $referenceMap = [];

    /**
     * Create an instance of the view
     * @param $state \BlueSky\Framework\State\Web
     * @param string $reference
     * @return Base
     * */
    public static function createInstance($state, $reference)
    {
        return; //todo
        $className = false;
        if(!$className) {
            $className = self::$referenceMap["html"];
            if ( array_key_exists($reference, self::$referenceMap)) {
                $className = self::$referenceMap[$reference];
            }
        }
        return new $className($state);
    }

    /**
     *
     * */
    public static function registerReference($reference, $object)
    {

    }

}
