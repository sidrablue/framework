<?php
namespace BlueSky\Framework\Object;

use BlueSky\Framework\State\Base;
use BlueSky\Framework\State\Web;

/**
 * Base class for all state based objects
 */
class State
{

    /**
     * @var Web $requestState
     * @access private
     * The model state
     * */
    private $requestState;

    /**
     * Base constructor to define the state object
     * @access public
     * @param Base $state
     * */
    public function __construct(Base $state)
    {
        $this->requestState = $state;
        $this->onCreate();
    }

    /**
     * Get the state object
     * @access public
     * @return Web
     * */
    public function getState()
    {
        return $this->requestState;
    }

    /**
     * Set the state object
     * @access public
     * @param Base $state
     * @return void
     * */
    public function setState(Base $state)
    {
        $this->requestState = $state;
    }

    /**
     * On create handler to be implemented in child
     * @access public
     * @return Web
     * */
    protected function onCreate()
    {

    }

}
