<?php
namespace BlueSky\Framework\Object\Model;

/**
 * Base class for all Master DB models
 */
class BaseMaster extends Base
{
    /**
     * Define the read DB function so that it can be overwritten
     * @access protected
     * @return \Doctrine\DBAL\Connection
     * */
    public function getReadDb()
    {
        if(!$result = $this->getState()->getMasterDb()) {
            throw new \Exception("No master read database is defined");
        }
        return $result;
    }

    /**
     * Define the write DB function so that it can be overwritten
     * @access protected
     * @return \Doctrine\DBAL\Connection
     * */
    public function getWriteDb()
    {
        if(!$result = $this->getState()->getMasterDb()) {
            throw new \Exception("No master write database is defined");
        }
        return $result;
    }

}
