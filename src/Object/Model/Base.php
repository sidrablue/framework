<?php
namespace BlueSky\Framework\Object\Model;

use Aura\Input\Builder;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Query\QueryBuilder;
use Exception;
use PDO;
use BlueSky\Framework\Model\Tag\Tag;
use BlueSky\Framework\Object\Container\PageContainer;
use BlueSky\Framework\Object\Container\QueryListContainer;
use BlueSky\Framework\State\Base as BaseState;
use BlueSky\Framework\Util\Input\Filter;
use Aura\Input\Form;
use BlueSky\Framework\Util\Paging;
use BlueSky\Framework\Object\StateDb as StateDbBase;
use BlueSky\Framework\Util\Time;
use BlueSky\Framework\Object\Entity\Base as BaseEntity;


/**
 * Base class for all models
 */
class Base extends StateDbBase
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName;

    /**
     * @var string $objectReference
     * The reference of the object, such as "user" or "house"
     * */
    protected $objectReference;

    /**
     * @var int $resultsPerPage - the number of results to get per page
     * */
    protected $resultsPerPage = 20;

    /**
     * @var array $phraseSearchFields
     * The names and data types of the objects searchable fields
     */
    protected $phraseSearchFields;

    /**
     * @param BaseState $state
     * @param String $objectReference
     * */
    public function __construct(BaseState $state = null, $objectReference = '')
    {
        parent::__construct($state);
        if ($objectReference) {
            $this->objectReference = $objectReference;
        }
        if (!$this->tableName) {
            $this->tableName = 'sb_' . $this->objectReference;
        }
        $this->objectReference = ucfirst(ltrim($this->objectReference, '_'));
        $this->onCreate();
    }

    protected function onCreate()
    {

    }

    /**
     * Get the name of the table for this object
     * @return string
     */
    public function getTableName()
    {
        return !empty($this->tableName) ? $this->tableName : get_called_class();
    }

    /**
     * Get a single objects data by id, returning an array
     *
     * @param integer $id
     * @return Object
     */
    public function getArray($id)
    {
        return $this->getObjectData($id);
    }

    /**
     * Get a single objects data by id, returning an array
     *
     * @param integer $id
     * @return Object
     */
    private function getObjectData($id)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('*')->from($this->getTableName(), 't')->where('t.id = :id')->setParameter('id', $id);
        $s = $q->execute();
        return $s->fetch(\PDO::FETCH_ASSOC);
    }

    /**
     * Get a single object by id, returning an Entity
     *
     * @param integer $id
     * @return Object
     */
    public function getEntity($id)
    {
        $result = false;
        if ($data = $this->getObjectData($id)) {
            $result = $data;
            $entityClassName = '\BlueSky\Framework\Entity\\' . $this->objectReference;
            if (class_exists($entityClassName)) {
                $e = new $entityClassName($this->getState(), $data);
                $result = $e;
            }
        }
        return $result;
    }

    /**
     * Get all objects
     *
     * @param int $maxResults
     * @return array
     */
    public function getAll($maxResults = 1000)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $s = $q->select('*')
            ->from($this->getTableName(), 't')
            ->where("t.lote_deleted is null")
            ->setMaxResults($maxResults)->execute();
        return $s->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * Delete an object
     *
     * @param int $id - the object to delete
     * @param bool $strongDelete - Whether or not to delete the table object completely
     * @return void
     */
    public function delete($id, $strongDelete = false)
    {
        if ($strongDelete) {
            $this->getWriteDb()->delete($this->getTableName(), ['id' => $id]);

        } else {
            $data = ['lote_deleted' => Time::getUtcNow(), 'lote_deleted_by' => $this->getState()->getUser()->id];
            $this->getWriteDb()->update($this->getTableName(), $data, ['id' => $id]);
        }

    }

    /**
     * Get a new form object to use for validating input
     * @access protected
     * @return Form
     */
    protected function getNewForm()
    {
        return new Form(new Builder, new Filter);
    }

    /**
     * Check if a value of a field exists
     * @todo - implement checking of custom fields as well
     * @access protected
     * @param string $fieldName - the name of the field to check
     * @param string $fieldValue - the value of the field to check for
     * @param int $ignoreId - an ID to optionally ignore in the check
     * @return boolean
     * */
    protected function valueExists($fieldName, $fieldValue, $ignoreId = 0)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('count(t.id)')
            ->from($this->getTableName(), 't')
            ->where($fieldName . ' = :value')
            ->andWhere($fieldName . ' is not null')
            ->setParameter('value', $fieldValue)
            ->andWhere('t.lote_deleted is null');
        if ($ignoreId) {
            $q->andWhere('id != :id')
                ->setParameter('id', $ignoreId);
        }
        $s = $q->execute();
        return $s->fetchColumn() > 0;
    }

    /**
     * Get the latest import list
     * @param string|array $tags
     * @param int $page
     * @param array $clauses
     * @param array $orderBy
     * @param bool $includeDeleted - Flag to also include rows that have been recorded as deleted
     * @param string $logic - the logic of this query
     * @param string $perPage
     * @param string $selectQuery
     * @param bool $showRestrictedTags
     * @return array
     */
    public function getAccessibleListByTag(
        $tags,
        $page,
        $clauses = [],
        $orderBy = [],
        $logic = 'and',
        $perPage = '',
        $selectQuery = '',
        $includeDeleted = false,
        $showRestrictedTags = false,
        $calendars = false
    ) {
        return $this->getCommonList($page, $clauses, $orderBy, $logic, $perPage, $selectQuery, $includeDeleted, true,
            $tags, $showRestrictedTags, $calendars);
    }

    public function getAllAccessibleListByTag(
        $page,
        $clauses = [],
        $orderBy = [],
        $logic = 'and',
        $perPage = '',
        $selectQuery = '',
        $includeDeleted = false
    ) {
        return $this->getCommonList($page, $clauses, $orderBy, $logic, $perPage, $selectQuery, $includeDeleted, true);
    }

    /**
     * Get the latest import list
     * @param int $page
     * @param array $clauses
     * @param array $orderBy
     * @param bool $includeDeleted - Flag to also include rows that have been recorded as deleted
     * @param string $logic - the logic of this query
     * @return array
     */
    public function getAccessibleList(
        $page,
        $clauses = [],
        $orderBy = [],
        $logic = 'and',
        $perPage = '',
        $selectQuery = '',
        $includeDeleted = false
    ) {
        return $this->getCommonList($page, $clauses, $orderBy, $logic, $perPage, $selectQuery, $includeDeleted, true);
    }

    protected function getCommonList(
        $page,
        $clauses = [],
        $orderBy = [],
        $logic = 'and',
        $perPage = '',
        $selectQuery = '',
        $includeDeleted = false,
        $checkAccess = false,
        $tags = false,
        $showRestrictedTags = false,
        $calendars = false
    ) {
        if (empty($perPage)) {
            $perPage = $this->getResultsPerPage();
        }
        $data = [];
        if ($checkAccess) {
            $checkAccess = $this->getTableName();
        }
        $data['total'] = $this->getListCount($clauses, $logic, $includeDeleted, $checkAccess, $tags,
            $showRestrictedTags, $calendars);
        if ($data['total'] > 0) {
            $data['paging'] = Paging::getPaging($data['total'], $this->getOffset($page, $perPage), $perPage);
            $data['rows'] = $this->getListData($page, $clauses, $orderBy, $logic, $perPage, $selectQuery,
                $includeDeleted, $checkAccess, $tags, $showRestrictedTags, $calendars);
        }
        return $data;
    }

    /**
     * Get the latest import list
     * @param int $page
     * @param array $clauses
     * @param array $orderBy
     * @param bool $includeDeleted - Flag to also include rows that have been recorded as deleted
     * @param string $logic - the logic of this query
     * @return array
     */
    public function getList(
        $page,
        $clauses = [],
        $orderBy = [],
        $logic = 'and',
        $perPage = '',
        $selectQuery = '',
        $includeDeleted = false
    ) {
        return $this->getCommonList($page, $clauses, $orderBy, $logic, $perPage, $selectQuery, $includeDeleted);
    }

    /**
     * Get the latest import list
     * @param int $page
     * @param array $filters
     * @param array $orderBy
     * @param string $logic - the logic of this query
     * @param bool $includeDeleted - Flag to also include rows that have been recorded as deleted
     * @param string $accessReference
     * @param array|bool|string $tags
     * @return array
     */
    public function getListData(
        $page,
        $filters = [],
        $orderBy = [],
        $logic = 'and',
        $perPage = '',
        $selectQuery = '',
        $includeDeleted = false,
        $accessReference = '',
        $tags = false,
        $showRestrictedTags = false,
        $calendars = false
    ) {
        if (empty($perPage)) {
            $perPage = $this->getResultsPerPage();
        }

        $q = $this->getBaseQuery($filters, $logic, $includeDeleted, $accessReference, $tags, $showRestrictedTags,
            $calendars);
        if (!empty($selectQuery)) {
            $q->select($selectQuery);
        } else {
            $q->select("distinct t.*");
        }
        $q->setMaxResults($perPage)
            ->setFirstResult($this->getOffset($page, $perPage));
        foreach ($orderBy as $k => $v) {
            if ($k) {
                $q->addOrderBy($k, $v);
            }
        }
        $s = $q->execute();

        return $s->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * @param array $clauses
     * @param string $logic - the logic of this query
     * @param bool $includeDeleted - Flag to also include rows that have been recorded as deleted
     * @param string $accessReference
     * @param array|bool|string $tags
     * @return int
     */
    public function getListCount(
        $clauses,
        $logic = 'and',
        $includeDeleted = false,
        $accessReference = '',
        $tags = false,
        $showRestrictedTags = false,
        $calendars = false
    ) {
        $q = $this->getBaseQuery($clauses, $logic, $includeDeleted, $accessReference, $tags, $showRestrictedTags,
            $calendars);
        $q->select("count(distinct t.id)");
        try {
            $s = $q->execute();
            $result = $s->fetchColumn();
        } catch (\PDOException $e) {
            $result = 0;
        }
        return $result;
    }

    /**
     * @param array $clauses
     * @param string $logic - the logic of this query
     * @param bool $includeDeleted - Flag to also include rows that have been recorded as deleted
     * @param string $accessReference
     * @param array|bool|string $tags
     * @return QueryBuilder
     * */
    private function getBaseQuery(
        $clauses,
        $logic = 'and',
        $includeDeleted = false,
        $accessReference = '',
        $tags = false,
        $showRestrictedTags = false,
        $calendars = false
    ) {
        $andQueryBuilder = $q = $this->getReadDb()->createQueryBuilder();
        $q->from($this->getTableName(), 't');

        $orQueryBuilder = $this->getReadDb()->createQueryBuilder();

        $logic != 'or' ? $logic = 'and' : null;

        $counter = 1;
        foreach ($clauses as $c) {
            if ($logic == 'and' || (isset($c['logic']) && $c['logic'] == 'and')) {
                $this->buildClause($andQueryBuilder, $c, "and", $counter);
            } elseif ($logic == 'or') {
                $this->buildClause($orQueryBuilder, $c, "or", $counter);
            }
            $counter++;
        }

        $orQueryParts = (string)$orQueryBuilder->getQueryPart('where');
        if ($orQueryParts != '') {
            $andQueryBuilder->andWhere($orQueryParts);
            foreach ($orQueryBuilder->getParameters() as $key => $value) {
                $andQueryBuilder->setParameter($key, $value, $orQueryBuilder->getParameterType($key));
            }
        }

        $orQueryJoins = $orQueryBuilder->getQueryPart('join');
        if (is_array($orQueryJoins)) {
            foreach ($orQueryJoins as $alias => $joinsList) {
                foreach ($joinsList as $join) {
                    if ($join['joinType'] == 'left') {
                        $andQueryBuilder->leftJoin($alias, $join['joinTable'], $join['joinAlias'],
                            $join['joinCondition']);
                    } else {
                        //throw new \Exception("Base getList function join condition not supported on OR dynamic query");
                    }
                }
            }
        }
        if (isset($orQueryJoins['t']) && count($orQueryJoins['t']) > 0) {

        }

        if (!$includeDeleted) {
            $q->andWhere('t.lote_deleted is null');
        }
        if ($accessReference) {
            $this->addLoteAccessClauses($q, 't', $accessReference);
        }
        if ($calendars) {
            $q->leftJoin('t', 'sb_calendar_event', 'ce', 'ce.event_id = t.id')
                ->andWhere("ce.calendar_id = :calendarID")
                ->setParameter("calendarID", $calendars);
        }
        if ($tags !== false) {
            if ($accessReference == 'sb_content_snippet') {
                $accessReference = 'content.holder';
            }
            $mt = new Tag($this->getState());
            $mt->addTagsQuery($q, $accessReference, $tags, true, '', $showRestrictedTags);
        }
        return $q;
    }

    private function getJoinSql($fromTablePrefix, $joinTablePrefix, $joinData, $clause)
    {
        $joinSql = "{$joinTablePrefix}.{$joinData['field_from']} = {$fromTablePrefix}.{$joinData['field_to']} and {$joinTablePrefix}.lote_deleted is null ";
        if (isset($joinData['clause']) && is_array($joinData['clause'])) {
            foreach ($joinData['clause'] as $joinClause) {
                if ($joinClause['value'] && $joinClause['operator'] && $joinClause['field_from']) {
                    if ($joinClause['operator'] == 'in') {
                        $joinSql .= " and {$joinTablePrefix}.{$joinClause['field_from']} in ({$joinClause['value']}) ";
                    } elseif ($joinClause['operator'] == 'equals') {
                        if ($joinClause['value'] == '%_value_%') {
                            $joinSql .= " and {$joinTablePrefix}.{$joinClause['field_from']} = {$clause['value'][0]} ";
                        } else {
                            $clauseTempValue = $joinClause['value'];
                            if (isset($joinClause['data_type']) && $joinClause['data_type'] == 'string') {
                                $clauseTempValue = $this->getState()->getReadDb()->quote($clauseTempValue);
                            }
                            $joinSql .= " and {$joinTablePrefix}.{$joinClause['field_from']} = {$clauseTempValue} ";
                        }
                    } elseif ($joinClause['operator'] == 'is_null') {
                        $joinSql .= " and {$joinTablePrefix}.{$joinClause['field_from']} is null";
                    }
                }
            }
        }
        return $joinSql;
    }

    /**
     * Build a join clause
     * @param QueryBuilder $q
     * @param $joinData
     * @param $clauseData
     * @param $logicFunction
     * @param $fromTablePrefix
     * @param $joinTablePrefix
     * @param $key
     * @param $field
     * @param $value
     * @param $operator
     * @return array
     */
    private function buildJoinClause(
        $q,
        $joinData,
        $clauseData,
        $logicFunction,
        &$counter,
        $fromTablePrefix,
        $joinTablePrefix,
        $key,
        $field,
        $value,
        $operator
    ) {

        if (isset($joinData['custom_alias'])) {
            $joinTablePrefix = $joinData['custom_alias'];
        }

        if (isset($joinData['from_alias'])) {
            $from_alias = $joinData['from_alias'];
        } else {
            $from_alias = "t";
        }

        $joinSql = $this->getJoinSql($fromTablePrefix, $joinTablePrefix, $joinData, $clauseData);
        if (isset($joinData['joins']) && isset($joinData['custom_alias'])) {
            $joinFrom = $joinData['custom_alias'];
            foreach ($joinData['joins'] as $subJoin) {
                $subJoinKey = isset($subJoin['field']) ? $subJoin['field'] . '_' . $counter : null;
                $subJoinField = isset($subJoin['field']) ? $subJoin['field'] : null;
                $subJoinValue = isset($subJoin['value']) ? $subJoin['value'] : null;
                $subJoinTo = $subJoin['custom_alias'];
                $subJoinOperator = $subJoin['operator'];
                $this->buildJoinClause($q, $subJoin, $joinData, $logicFunction, $counter, $joinFrom, $subJoinTo,
                    $subJoinKey, $subJoinField, $subJoinValue, $subJoinOperator);
                $counter++;
            }
        }
        //$logicFunction = 'andWhere';
        $q->leftJoin($from_alias, $joinData['table'], $joinTablePrefix, $joinSql);
        if ($field && $value && $operator) {
            if ($operator == 'in') {
                $q->{$logicFunction}("$joinTablePrefix.{$field} in (:{$key})")
                    ->setParameter($key, $value, Connection::PARAM_STR_ARRAY);
            } elseif ($operator == 'not_in') {
                $q->{$logicFunction}("$joinTablePrefix.{$field} not in (:{$key})")
                    ->setParameter($key, $value, Connection::PARAM_STR_ARRAY);
            } elseif ($operator == 'equals') {
                $q->{$logicFunction}("$joinTablePrefix.{$field} = :{$key}")
                    ->setParameter($key, $value);
            } elseif ($operator == 'not_equals') {
                $q->{$logicFunction}("$joinTablePrefix.{$field} != :{$key}")
                    ->setParameter($key, $value);
            } elseif ($operator == 'like') {
                $q->{$logicFunction}("$joinTablePrefix.{$field} like :{$key}")
                    ->setParameter($key, "%{$value}%");
            } elseif ($operator == 'not_like') {
                $q->{$logicFunction}("{$joinTablePrefix}.{$field} not like :{$key}")
                    ->setParameter($key, "%{$value}%");
            } elseif ($operator == 'ends_with') {
                $q->{$logicFunction}("{$joinTablePrefix}.{$field} like :{$key}")
                    ->setParameter($key, "%{$value}");
            } elseif ($operator == 'begins_with') {
                $q->{$logicFunction}("{$joinTablePrefix}.{$field} like :{$key}")
                    ->setParameter($key, "{$value}%");
            } elseif ($operator == 'is_null') {
                $q->{$logicFunction}("{$joinTablePrefix}.{$field} is null");
            }
        }
    }

    /**
     * Build the text of a specific clause and add it to the array
     * @param QueryBuilder $q
     * @param array $clause
     * @param string $logic - the logic of this query
     * @param int $counter - the counter to use for join table names and for param names
     * @return array
     * @todo - move this function to a separate class
     * @todo - improve the join [clauses] as currently its doing an equals comparison only
     * */
    private function buildClause($q, $clause, $logic = 'and', $counter = 1)
    {
        $logicFunction = 'andWhere';
        if ($logic == 'or') {
            $logicFunction = 'orWhere';
        }
        if ((isset($clause['field']) && isset($clause['data_type'])) || isset($clause['join'])) {
            $key = isset($clause['field'])?$clause['field'] . '_' . $counter:null;
            $field = isset($clause['field']) ? $clause['field'] : null;
            $value = isset($clause['value']) ? $clause['value'] : null;

            $mainTablePrefix = "t";

            if(isset($clause['custom_alias'])) {
                $mainTablePrefix = $clause['custom_alias'];
            }

            if (isset($clause['clause_type']) && $clause['clause_type'] == 'join') {
                $joinData = $clause['join'];
                $joinTablePrefix = "{$mainTablePrefix}{$counter}";
                $joinOperator = isset($clause['operator']) ? $clause['operator'] : null;
                $this->buildJoinClause($q, $joinData, $clause, $logicFunction, $counter, $mainTablePrefix, $joinTablePrefix, $key, $field, $value, $joinOperator);
            } elseif ((string) $clause['data_type'] == 'string' || $clause['data_type'] === \PDO::PARAM_STR) {
                if ($clause['operator'] == 'in') {
                    $q->{$logicFunction}("{$mainTablePrefix}.{$field} in (:{$key})")
                        ->setParameter($key, $value, Connection::PARAM_STR_ARRAY);
                }else if ($clause['operator'] == 'not_in') {
                    $q->{$logicFunction}("{$mainTablePrefix}.{$field} not in (:{$key})")
                        ->setParameter($key, $value, Connection::PARAM_STR_ARRAY);
                }else if ($clause['operator'] == 'equals') {
                    $q->{$logicFunction}("{$mainTablePrefix}.{$field} = :{$key}")
                        ->setParameter($key, $value);
                } elseif ($clause['operator'] == 'not_equals') {
                    $q->{$logicFunction}("{$mainTablePrefix}.{$field} != :{$key}")
                        ->setParameter($key, $value);
                } elseif ($clause['operator'] == 'like') {
                    $q->{$logicFunction}("{$mainTablePrefix}.{$field} like :{$key}")
                        ->setParameter($key, "%{$value}%");
                } elseif ($clause['operator'] == 'not_like') {
                    $q->{$logicFunction}("{$mainTablePrefix}.{$field} not like :{$key}")
                        ->setParameter($key, "%{$value}%");
                } elseif ($clause['operator'] == 'ends_with') {
                    $q->{$logicFunction}("{$mainTablePrefix}.{$field} like :{$key}")
                        ->setParameter($key, "%{$value}");
                } elseif ($clause['operator'] == 'begins_with') {
                    $q->{$logicFunction}("{$mainTablePrefix}.{$field} like :{$key}")
                        ->setParameter($key, "{$value}%");
                } elseif ($clause['operator'] == 'not_null') {
                    $q->{$logicFunction}("{$mainTablePrefix}.{$field} is not null");
                } elseif ($clause['operator'] == 'is_null') {
                    $q->{$logicFunction}("{$mainTablePrefix}.{$field} is null");
                }
            } elseif ((string) $clause['data_type'] == 'boolean' || $clause['data_type'] == \PDO::PARAM_BOOL) {
                if ($clause['operator'] == 'is_true') {
                    $q->{$logicFunction}("{$mainTablePrefix}.{$field} is true");
                } elseif ($clause['operator'] == 'is_false') {
                    $q->{$logicFunction}("({$mainTablePrefix}.{$field} is false or {$mainTablePrefix}.{$field} is null)");
                } elseif ($clause['operator'] == 'equals' && $clause['value']) {
                    $q->{$logicFunction}("{$mainTablePrefix}.{$field} is true");
                } elseif ($clause['operator'] == 'equals' && !$clause['value']) {
                    $q->{$logicFunction}("({$mainTablePrefix}.{$field} is false or {$mainTablePrefix}.{$field} is null)");
                }
            } elseif ((string) $clause['data_type'] == 'int' || $clause['data_type'] == \PDO::PARAM_INT) {
                if ($clause['operator'] == 'equals') {
                    $q->{$logicFunction}("{$mainTablePrefix}.{$field} = :{$key}")
                        ->setParameter($key, $value);
                } elseif ($clause['operator'] == 'not_equals') {
                    $q->{$logicFunction}("{$mainTablePrefix}.{$field} != :{$key}")
                        ->setParameter($key, $value);
                } elseif ($clause['operator'] == 'greater_than') {
                    $q->{$logicFunction}("{$mainTablePrefix}.{$field} > :{$key}")
                        ->setParameter($key, $value);
                } elseif ($clause['operator'] == 'less_than') {
                    $q->{$logicFunction}("{$mainTablePrefix}.{$field} < :{$key}")
                        ->setParameter($key, $value);
                } elseif ($clause['operator'] == 'greater_than_or_equal') {
                    $q->{$logicFunction}("{$mainTablePrefix}.{$field} >= :{$key}")
                        ->setParameter($key, $value);
                } elseif ($clause['operator'] == 'less_than_or_equal') {
                    $q->{$logicFunction}("{$mainTablePrefix}.{$field} <= :{$key}")
                        ->setParameter($key, $value);
                }
            } elseif ((string)$clause['data_type'] == 'date') {
                if ($clause['operator'] == 'equals') {
                    $q->{$logicFunction}("{$mainTablePrefix}.{$field} LIKE :{$key}")
                        ->setParameter($key, "$value%");
                } elseif ($clause['operator'] == 'not_equals') {
                    $q->{$logicFunction}("{$mainTablePrefix}.{$field} NOT LIKE :{$key}")
                        ->setParameter($key, "$value%");
                } elseif ($clause['operator'] == 'greater_than') {
                    $q->{$logicFunction}("{$mainTablePrefix}.{$field} > :{$key}")
                        ->setParameter($key, $value);
                } elseif ($clause['operator'] == 'less_than') {
                    $q->{$logicFunction}("{$mainTablePrefix}.{$field} < :{$key}")
                        ->setParameter($key, $value);
                } elseif ($clause['operator'] == 'greater_than_or_equal') {
                    $q->{$logicFunction}("{$mainTablePrefix}.{$field} >= :{$key}")
                        ->setParameter($key, $value);
                } elseif ($clause['operator'] == 'less_than_or_equal') {
                    $q->{$logicFunction}("{$mainTablePrefix}.{$field} <= :{$key}")
                        ->setParameter($key, $value);
                }
            } elseif ($clause['data_type'] == \PDO::PARAM_NULL) {
                if ($clause['operator'] == 'equals') {
                    $q->{$logicFunction}("{$mainTablePrefix}.{$field} is null");
                }
                if ($clause['operator'] == 'notequals') {
                    $q->{$logicFunction}("({$mainTablePrefix}.{$field} is not null)");
                }
            }
        } elseif (isset($clause['data_type']) && $clause['data_type'] == 'nested' && is_array($clause['_clauses'])) {
            $subQuery = $this->getBaseQuery($clause['_clauses'], $clause['logic']);
            $part = $subQuery->getQueryPart('where');
            $q->{$logicFunction}(" ( " . $part->__toString() . ' ) ');
            foreach ($subQuery->getParameters() as $k => $v) {
                $q->setParameter($k, $v);
            }
        } elseif (isset($clause['group_by'])) {
            $q->addGroupBy($clause['column']);
        }
        return $clause;
    }

    /**
     * Get the data for an object
     * @param int $id - the ID of the object
     * @param bool $includeDeleted
     * @return array|false
     * */
    public function get($id, $includeDeleted = false)
    {
        $result = false;
        if ($id > 0) {
            $q = $this->getReadDb()->createQueryBuilder();
            $q->select('o.*')
                ->from($this->getTableName(), 'o')
                ->andWhere('o.id = :id')
                ->setParameter('id', $id);
            if(!$includeDeleted) {
                $q->andWhere("o.lote_deleted is null");
            }
            $s = $q->execute();
            $result = $s->fetch(\PDO::FETCH_ASSOC);
        }
        return $result;
    }

    /**
     * Get the data for an object
     * @param array $ids - the ID of the object
     * @return array|false
     * */
    public function getByIds($ids)
    {
        $result = [];
        if (is_array($ids) && count($ids) > 0) {
            $q = $this->getReadDb()->createQueryBuilder();
            $q->select('o.*')
                ->from($this->getTableName(), 'o')
                ->andWhere('o.id in (:ids)')
                ->setParameter('ids', $ids, Connection::PARAM_INT_ARRAY)
                ->andWhere("o.lote_deleted is null");
            $s = $q->execute();
            $result = $s->fetchAll(\PDO::FETCH_ASSOC);
        }
        return $result;
    }

    /**
     * Get the offset for this request
     * @param int $page
     * @param int|string|bool $perPage
     * @return int
     * */
    public function getOffset($page, $perPage = '')
    {
        if (empty($perPage) || !$perPage) {
            $perPage = $this->getResultsPerPage();
        }

        //return max($page * $this->resultsPerPage - $this->resultsPerPage, 0);
        return max($page * $perPage - $perPage, 0);
    }

    /**
     * @return int
     */
    public function getResultsPerPage()
    {
        return $this->resultsPerPage;
    }

    /**
     * Set the results per page
     * @access public
     * @param int $resultsPerPage
     * @return void
     */
    public function setResultsPerPage($resultsPerPage)
    {
        if (is_numeric($resultsPerPage) && $resultsPerPage > 0) {
            $this->resultsPerPage = $resultsPerPage;
        }
    }

    /**
     * Add clauses to a query builder to validate its access via the lote_access mechanism
     * @param \Doctrine\DBAL\Query\QueryBuilder $queryBuilder
     * @param string $alias - the table alias to join for access queries
     * @param string|bool $entityName
     * @return QueryBuilder
     *  */
    protected function addLoteAccessClauses($queryBuilder, $alias = 't', $entityName = false)
    {
        if ($this->getState()->getUser()->id && !$this->getState()->getUser()->isAdmin()) {
            $q = $this->getReadDb()->createQueryBuilder();
            $q->select('count(sbe.id)')
                ->from('sb__auth_access_entity', 'sbe')

                //Role access
                ->leftJoin('sbe', 'sb__role', 'sbe_r', 'sbe_r.id = sbe.access_type_id and sbe.access_type = "role"')
                ->leftJoin('sbe_r', 'sb__user_role', 'sbe_u', 'sbe_u.role_id = sbe_r.id')

                //Group access
                ->leftJoin('sbe', 'sb__group', 'sbe_g', 'sbe_g.id = sbe.access_type_id and sbe.access_type = "group"')
                ->leftJoin('sbe_g', 'sb__user_group', 'sbe_ug', 'sbe_ug.group_id = sbe_g.id')

                ->andWhere('sbe.object_ref = :sb_access_entity_table_name')
                ->andWhere('sbe_u.user_id = :sbe_u_user_id or sbe_ug.user_id = :sbe_u_user_id')
                ->andWhere($alias . '.id = sbe.object_id');
            if ($entityName) {
                $queryBuilder->setParameter('sb_access_entity_table_name', $entityName);
            } else {
                $queryBuilder->setParameter('sb_access_entity_table_name', $this->getTableName());
            }
            $queryBuilder->setParameter('sbe_u_user_id', $this->getState()->getUser()->id);
            $queryBuilder->andWhere(" ($alias.lote_access = 1 or $alias.lote_access = 0 or ($alias.lote_access != -2 and (" . $q->getSQL() . ") > 0)) or $alias.lote_access is null");
        } elseif (!$this->getState()->getUser()->isAdmin()) {
            $queryBuilder->andWhere("($alias.lote_access = 1 or $alias.lote_access is null)");
        }
        return $queryBuilder;
    }


    /**
     * Add clauses to a query builder to validate its access via the lote_access mechanism
     * @param \Doctrine\DBAL\Query\QueryBuilder $queryBuilder
     * @param int $siteId - the ID of the current site
     * @param string $mainAlias
     * @return QueryBuilder
     *  */
    protected function addSiteClauses($queryBuilder, $siteId = 0, $mainAlias = "t")
    {
        if($siteId > 0) {
            $siteAlias = uniqid("site_");
            $q = $this->getReadDb()->createQueryBuilder();
            $q->select("case when site_id = {$siteId} then 1 when site_id > 0 then 0 else 1 end")
                ->from("sb_website_site_item", $siteAlias)
                ->andWhere("{$siteAlias}.object_ref = :object_ref_{$siteAlias}")
                ->andWhere("{$siteAlias}.object_id = {$mainAlias}.id")
                ->andWhere("{$siteAlias}.site_id = :site_id_{$siteAlias}")
                ->andWhere("{$siteAlias}.lote_deleted is null")
                ->setParameter("site_id_{$siteAlias}", $siteId)
                ->setMaxResults(1);
            $queryBuilder->setParameter("object_ref_{$siteAlias}", $this->getTableName());
            $queryBuilder->setParameter("site_id_{$siteAlias}", $siteId);
            $queryBuilder->andWhere("(" . $q->getSQL() . ") > 0");
        }
    }

    /**
     * Check if a specific entity is visible in a given site
     *
     * @access public
     * @param BaseEntity $baseEntity
     * @param int $siteId
     * @return bool
     *
     * */
    public function isVisibleInSite(BaseEntity $baseEntity, $siteId = 0)
    {
        $result = true;
        if($baseEntity->id) {
            $q = $this->getReadDb()->createQueryBuilder();
            $q->select("*")
                ->from("sb_website_site_item", "i")
                ->andWhere("i.object_ref = :object_ref")
                ->setParameter("object_ref", $baseEntity->getTableName())
                ->andWhere("i.object_id = :object_id")
                ->setParameter("object_id", $baseEntity->id)
                ->andWhere("i.lote_deleted is null");
            if($rows = $q->execute()->fetchAll(\PDO::FETCH_ASSOC)) {
                $result = false;
                foreach($rows as $v) {
                    if($v['site_id']==$siteId) {
                        $result = true;
                        break;
                    }
                }
            }
        }
        return $result;
    }

    /**
     * Add clauses to a query builder to validate its access via the lote_access mechanism
     * @param \Doctrine\DBAL\Query\QueryBuilder $queryBuilder
     * @param string $reference - the table alias to join for access queries
     * @param string|Array $tags
     * @param bool $wholeWordMatch
     * @param string $paramSuffix
     * @return QueryBuilder
     */
    protected function addTagClauses($queryBuilder, $reference, $tags, $wholeWordMatch = true, $paramSuffix = '')
    {
        $t = new Tag($this->getState());
        $t->addTagsQuery($queryBuilder, $reference, $tags, $wholeWordMatch, $paramSuffix);
        return $queryBuilder;
    }

    /**
     * Get a listing type format from a prebuilt query
     * @access public
     * @param QueryBuilder $q
     * @param int $page - the page
     * @param int $resultsPerPage
     * @param string $countQuery
     * @param bool $groupByStatus - true will accept group by queries in row count if $countQuery is set
     * @return array
     */
    public function getListByQuery(
        QueryBuilder $q,
        $page = 1,
        $resultsPerPage = 1000,
        $countQuery = '',
        $groupByStatus = false
    ) {
        $data = [];
        if (!empty($countQuery)) {
            $data['total'] = $this->getQueryCount($q, $countQuery, $groupByStatus);
        } else {
            $data['total'] = $this->getQueryCount($q);
        }
        if ($data['total'] > 0) {
            if (is_null($resultsPerPage) || $resultsPerPage < 1) {
                $resultsPerPage = $this->getResultsPerPage();
            }
            $data['paging'] = Paging::getPaging((int)$data['total'], $this->getOffset($page, $resultsPerPage),
                $resultsPerPage);
            $data['rows'] = $this->getQueryData($q, (int)$page, (int)$resultsPerPage);
        }
        return $data;
    }

    public function getListByRecursiveSQL(
        string $recursiveSql,
        array $params = [],
        ?PageContainer $pagingContainer = null,
        int $page = 1,
        int $resultsPerPage = 1000,
        ?string $countQuery = null
    ): QueryListContainer {
        $data = new QueryListContainer();

        $offset = $this->getOffset($page, $resultsPerPage);
        if ($pagingContainer === null) {
            $total = $this->getRecursiveQueryCount($recursiveSql, $params, $countQuery);
            $pagingContainer = PageContainer::getPaging($total, $offset, $resultsPerPage);
        }

        $data->setTotal($pagingContainer->total)->setPaging($pagingContainer);

        if ($pagingContainer->total > 0) {
            if ($resultsPerPage < 1) {
                $resultsPerPage = $this->getResultsPerPage();
            }
            $recursiveSql .= " limit {$resultsPerPage} offset {$offset}";

            $data->setRows($this->getReadDb()->executeQuery($recursiveSql, $params)->fetchAll(PDO::FETCH_ASSOC));
        }

        return $data;
    }

    /**
     * @param QueryBuilder $q
     * @param string $class
     * @param int $page
     * @param int $resultsPerPage
     * @param string|null $countQuery
     * @param bool $groupByStatus
     * @return BaseEntity[]
     */
    public function getListObjectsByQuery(QueryBuilder $q, string $class, int $page = 1, int $resultsPerPage = 1000, ?string $countQuery = 'count(*) as cnt', ?bool $groupByStatus = null): array
    {
        if ($groupByStatus === null) {
            $groupByStatus = !empty($q->getQueryPart('groupBy'));
        }
        $total = $this->getQueryCount($q, $countQuery, $groupByStatus);

        $data = $this->getDataObjectsByQuery($q, $class, $page, $resultsPerPage);

        return [
            'total' => $total,
            'rows' => $data,
            'paging' => Paging::getPaging($total, $this->getOffset($page, $resultsPerPage), $resultsPerPage)
        ];
    }

    /**
     * @param QueryBuilder $q
     * @param string $class
     * @param int $page
     * @param int $resultsPerPage
     * @return BaseEntity[]
     */
    public function getDataObjectsByQuery(QueryBuilder $q, string $class, int $page = 1, int $resultsPerPage = 1000): array
    {
        $data = [];
        $dataRaw = $this->getQueryData($q, $page, $resultsPerPage);
        foreach ($dataRaw as $item) {
            $data[] = new $class($this->getState(), $item);
        }
        return $data;
    }

    /**
     * Get a listing type format from a prebuilt query
     * @access public
     * @param QueryBuilder $q
     * @param string $countQuery
     * @param bool $groupBy
     * @return bool|int|string
     */
    public function getQueryCount(QueryBuilder $q, $countQuery = 'count(*) as cnt', $groupBy = false)
    {
        $b = clone $q;
        $b->select($countQuery);
        $b->resetQueryPart('orderBy');
        $s = $b->execute();
        if ($groupBy) {
            return $s->rowCount();
        } else {
            return $s->fetchColumn();
        }
    }

    public function getRecursiveQueryCount(string $fullSql, array $params = [], ?string $countQuery = null): int
    {
        if ($countQuery === null) {
            $tableName = uniqid('bd_');
            $countQuery = "select count(*) from ({$fullSql}) as {$tableName}";
        }

        return $this->getReadDb()->executeQuery($countQuery, $params)->fetchColumn();
    }

    /**
     * Get a listing type format from a prebuilt query
     * @access public
     * @param QueryBuilder $q
     * @param int $page - the page
     * @param bool|int $resultsPerPage - results per page
     * @return array
     * */
    public function getQueryData(QueryBuilder $q, int $page = 1, ?int $resultsPerPage = null): array
    {
        if ($resultsPerPage === null) {
            $resultsPerPage = $this->getResultsPerPage();
        }
        $q->setMaxResults($resultsPerPage)->setFirstResult($this->getOffset($page, $resultsPerPage));
        $s = $q->execute();
        return $s->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * Generates a search clause from a given phrase using the objects' phrase search fields
     *
     * @access public
     * @param string $phrase - Phrase to be queried
     * @return array
     */
    public function getPhraseSearchClauses($phrase)
    {
        $c = [];
        if (!empty($this->phraseSearchFields)) {
            $c = [];
            foreach ($this->phraseSearchFields as $field => $type) {
                $d['field'] = $field;
                $d['data_type'] = $type;
                $d['operator'] = 'like';
                $d['value'] = $phrase;
                $c[] = $d;
            }
        }
        return $c;
    }

    /**
     * Retrieve the objects phrase search fields
     *
     * @return array
     */
    public function getPhraseSearchFields()
    {
        return $this->phraseSearchFields;
    }


    /**
     * Count of groups the entity has access
     *
     * @param entity
     * @return int
     */
    public function checkAccessForEntity($entity)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('count(sbe.id)')
            ->from('sb__auth_access_entity', 'sbe')

            //Role access
            ->leftJoin('sbe', 'sb__role', 'sbe_r', 'sbe_r.id = sbe.access_type_id and sbe.access_type = "role"')
            ->leftJoin('sbe_r', 'sb__user_role', 'sbe_u', 'sbe_u.role_id = sbe_r.id')

            //Group access
            ->leftJoin('sbe', 'sb__group', 'sbe_g', 'sbe_g.id = sbe.access_type_id and sbe.access_type = "group"')
            ->leftJoin('sbe_g', 'sb__user_group', 'sbe_ug', 'sbe_ug.group_id = sbe_g.id')

            ->andWhere('sbe_u.user_id = :sbe_u_user_id or sbe_ug.user_id = :sbe_u_user_id')
            ->andWhere('sbe.object_ref = :sb_access_entity_table_name')
            ->andWhere('sbe.object_id = :sbe_object_id');
        $q->setParameter('sb_access_entity_table_name', $entity->getTableName());
        $q->setParameter('sbe_u_user_id', $this->getState()->getUser()->id);
        $q->setParameter('sbe_object_id', $entity->id);
        $s = $q->execute();
        return $s->fetchColumn();
    }

    /**
     * Return a custom field value for the specified owner ID and Value ID
     *
     * @param $ownerId
     * @param $valueId
     * @return mixed
     * @todo remove this once Lote and Schoolzine have had the EP-578 treatment
     * @deprecated
     */
    public function getCustomValueWithId($ownerId, $valueId)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('c.value_string')
            ->from('sb__user__cf_value', 'c')
            ->leftJoin('c', 'sb__cf_field', 'f', 'c.field_id = f.id')
            ->andWhere('c.object_id = :owner_id')
            ->andWhere('c.field_id = :value_id')
            ->setParameter('owner_id', $ownerId)
            ->setParameter('value_id', $valueId);
        $s = $q->execute();
        return $s->fetchColumn();
    }

    /**
     * Delete an object by field
     *
     * @param int $id - the object to delete
     * @param bool $strongDelete - Whether or not to delete the table object completely
     * @return void
     */
    public function deleteByField($field, $value, $strongDelete = false)
    {
        if ($strongDelete) {
            $this->getWriteDb()->delete($this->getTableName(), [$field => $value]);

        } else {
            $this->getWriteDb()->update($this->getTableName(), ['lote_deleted' => Time::getUtcNow()],
                [$field => $value]);
        }

    }

    /**
     * Delete an object by multiple fields
     *
     * @access public
     * @param array $fieldValues - column=>value criteria for deletion
     * @param bool $strongDelete - true if the object is to be strongly deleted from db
     * @return void
     * @throws \Doctrine\DBAL\Exception\InvalidArgumentException
     */
    public function deleteByFields($fieldValues, $strongDelete = false)
    {
        if ($strongDelete) {
            $this->getWriteDb()->delete($this->getTableName(), $fieldValues);
        } else {
            $now = Time::getUtcNow();
            $this->getWriteDb()->update(
                $this->getTableName(),
                ['lote_deleted' => $now, 'lote_deleted_by' => $this->getState()->getUser()->id],
                $fieldValues
            );
        }
    }

    public function getSettingValue($reference)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('s.value_custom')
            ->from('sb__setting', 's')
            ->andWhere('c.reference = :reference')
            ->setParameter('reference', $reference);
        $s = $q->execute();
        return $s->fetchColumn();
    }

    public function getPhraseSplitQuery(QueryBuilder $q, $phrase, $fields, $customMatch = [], $customString = '')
    {
        $phSplit = explode(" ", $phrase);
        $qString = "";
        $phSplitTwo = "";
        $spaceCount = substr_count($phrase ," ");

        if($customMatch){
            $last = array_pop($phSplit);
            $phSplit = [implode(' ', $phSplit), $last];
            if($spaceCount == 2){
                $phSplitTwo = explode(" ", $phrase);
                $first = array_shift($phSplitTwo);
                $phSplitTwo = [$first, implode(' ', $phSplitTwo)];
            }
            foreach($customMatch as $cusKey => $cusVal){
                $qString .= "(". $cusKey . " like :phrase)";
                $qString .= " or (".$cusVal." like :phrase) or ";
            }
        }

        foreach ($fields as $matchKey => $matchVal) {
            $qString .= "(";
            foreach ($phSplit as $key => $value) {
                if ($key > 0) {
                    $qString .= " and ";
                }
                if($customMatch){
                    foreach($customMatch as $cusKey => $cusVal){
                        if($cusKey == $matchVal){
                            if ($key > 0) {
                                $qString .= $cusVal . " like :param_" . $key;
                            }else{
                                $qString .= $matchVal . " like :param_" . $key;
                            }
                        }else if($cusVal == $matchVal){
                            if ($key > 0) {
                                $qString .= $cusKey . " like :param_" . $key;
                            }else{
                                $qString .= $matchVal . " like :param_" . $key;
                            }
                        }else{
                            $qString .= $matchVal . " like :param_" . $key;
                        }
                    }
                }else{
                    $qString .= $matchVal . " like :param_" . $key;
                }
            }
            if($phSplitTwo){
                foreach ($phSplitTwo as $key => $value) {
                    $qString .= " or ";
                    if($customMatch){
                        foreach($customMatch as $cusKey => $cusVal){
                            if($cusKey == $matchVal){
                                if ($key > 0) {
                                    $qString .= $cusVal . " like :param_2_" . $key;
                                }else{
                                    $qString .= $matchVal . " like :param_2_" . $key;
                                }
                            }else if($cusVal == $matchVal){
                                if ($key > 0) {
                                    $qString .= $cusKey . " like :param_2_" . $key;
                                }else{
                                    $qString .= $matchVal . " like :param_2_" . $key;
                                }
                            }else{
                                $qString .= $matchVal . " like :param_2_" . $key;
                            }
                        }
                    }else{
                        $qString .= $matchVal . " like :param_2_" . $key;
                    }
                }
            }
            $qString .= ")";
            if (isset($fields[$matchKey + 1])) {
                $qString .= ' or ';
            }
        }
        if($customString){
            $q->andWhere('('.$qString.') or ('.$customString.')');
        }else{
            $q->andWhere($qString);
        }

        foreach ($phSplit as $key => $value) {
            $q->setParameter('param_' . $key, '%' . $value . '%');
        }
        if($phSplitTwo){
            foreach ($phSplitTwo as $key => $value) {
                $q->setParameter('param_2_' . $key, '%' . $value . '%');
            }
        }
        if($customMatch){
            $q->setParameter('phrase', '%' . $phrase . '%');
        }
        return $q;
    }


    /**
     * Set the table name if there is a custom one
     * @access public
     * @param string $newName
     * @return void
     * */
    public function setTableName($newName)
    {
        $this->tableName = $newName;
    }

    /**
     * Get all objects
     *
     * @param int $maxResults
     * @param array $orderBy
     * @return array
     */
    public function getAllAsObjects($maxResults = 1000, $orderBy = [])
    {
        $result = [];
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('*')
            ->from($this->getTableName(), 't')
            ->where("t.lote_deleted is null")
            ->setMaxResults($maxResults);
        if($orderBy) {
            foreach($orderBy as $k=>$v) {
                $q->orderBy($k, $v);
            }
        }
        $className = $this->getEntityClassNameFromModel();
        if($data = $q->execute()->fetchAll(\PDO::FETCH_ASSOC)) {
            foreach($data as $d) {
                /** @var \BlueSky\Framework\Object\Entity\Base $t */
                $t = new $className($this->getState());
                $t->setData($d);
                $result[] = $t;
            }
        }
        return $result;
    }

    protected function arraysToObjects($objectData, $className = false) {
        $result = [];
        if(is_array($objectData)) {
            if(!$className) {
                $className = $this->getEntityClassNameFromModel();
            }
            foreach ($objectData as $object) {
                /** @var \BlueSky\Framework\Object\Entity\Base $t */
                $t = new $className($this->getState());
                $t->setData($object);
                $result[] = $t;
            }
        }
        return $result;
    }

    protected function getEntityClassNameFromModel()
    {
        return str_replace("\Model\\", "\Entity\\", get_class($this));
    }

    /**
     * Get a single object by a specific field
     *
     * @param string $fieldName
     * @param string|int|double|bool $fieldValue
     * @param bool $includeDeleted
     * @param string $sortField
     * @param string $sortOrder
     * @return array
     */
    public function listByField(string $fieldName, $fieldValue, bool $includeDeleted = false, string $sortField = '', string $sortOrder = ''): array
    {
        return $this->listByFields([$fieldName => $fieldValue], $includeDeleted, $sortField, $sortOrder);
    }

    /**
     * @param string $fieldName
     * @param $fieldValue
     * @param bool $includeDeleted
     * @param string $sortField
     * @param string $sortOrder
     * @return BaseEntity[]
     */
    public function listObjectsByField(string $fieldName, $fieldValue, bool $includeDeleted = false, string $sortField = '', string $sortOrder = ''): array
    {
        $dataRaw = $this->listByField($fieldName, $fieldValue, $includeDeleted, $sortField, $sortOrder);
        $data = array_map(function ($item) {
            $className = $this->getEntityClassNameFromModel();
            return new $className($this->getState(), $item);
        }, $dataRaw);
        return $data;
    }

    /**
     * Get a single object by a set of fields
     *
     * @param array $fieldNameValues - an array of field => value pairs
     * @param bool $includeDeleted
     * @param string $sortField
     * @param string $sortOrder
     * @return array
     */
    public function listByFields($fieldNameValues, $includeDeleted = false, $sortField = '', $sortOrder = 'asc'): array
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('t.*')
            ->from($this->getTableName(), 't');

        foreach ($fieldNameValues as $fn => $fv) {
            if (is_array($fv)) {
                $maxIndex = count($fv) - 1;
                $index = 0;
                $sqlStatement = '';
                if ($maxIndex >= 0) {
                    foreach ($fv as $fnOr => $fvOr) {
                        /// This block will trim any prefixed _
                        /// This is due to the limitations of the associative arrays.
                        /// Eg. so that email = null and email = '' can be placed in the same or block
                        $charIndex = 0;
                        $fnOrCount = strlen($fnOr);
                        while ($charIndex < $fnOrCount && $fnOr[$charIndex] === '_') { $charIndex++; }
                        if ($charIndex > 0) { $fnOr = substr($fnOr, $charIndex); }

                        $fnOrKey = 't.' . $fnOr;
                        if ($fvOr === null) {
                            $sqlStatement .= $fnOrKey . ' is null';
                        } else {
                            $key = $fnOr . '_' . uniqid();
                            $sqlStatement .= $fnOrKey . ' = :' . $key;
                            $q->setParameter($key, $fvOr);
                        }

                        if ($index++ < $maxIndex) {
                            $sqlStatement .= ' or ';
                        }
                    }
                }
                $q->andWhere($sqlStatement);
            } else {
                $key = $fn . '_' . uniqid();
                $fnKey = 't.' . $fn;
                $fv === null ? $q->andWhere($fnKey . ' is null') : $q->andWhere($fnKey . ' = :' . $key)->setParameter($key, $fv);
            }
        }

        if (!$includeDeleted) {
            $q->andWhere('t.lote_deleted is null');
        }
        if (!empty($sortField) && !empty($sortOrder)) {
            $q->orderBy('t.' . $sortField, $sortOrder);
        }

        $s = $q->execute();
        $data = $s->fetchAll(\PDO::FETCH_ASSOC);
//        $this->setData($data);
        return $data;
    }

    public function listObjectsByFields($fieldNameValues, $includeDeleted = false, $sortField = '', $sortOrder = 'asc'): array
    {
        $dataRaw = $this->listByFields($fieldNameValues, $includeDeleted, $sortField, $sortOrder);
        $data = array_map(function ($item) {
            $className = $this->getEntityClassNameFromModel();
            return new $className($this->getState(), $item);
        }, $dataRaw);
        return $data;
    }


    public function getEntityObject(?array $data): BaseEntity
    {
        $entityClass = $this->getEntityClassNameFromModel();
        $result = new $entityClass($this->getState());
        if($data) {
            /** @var BaseEntity $result*/
            $result->setData($data);
        }
        return $result;
    }

    public function getEntityIds(array $entityObjects): array
    {
        $result = [];
        foreach($entityObjects as $o) {
            if($o instanceof BaseEntity) {
                $result[] = $o->id;
            }
        }
        return $result;
    }

    public function getListByValues($values, $field = 'id', $indexByField = false): array
    {
        $result = [];
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("*")
            ->from($this->getTableName(), 't')
            ->andWhere("t.{$field} in (:values)")
            ->setParameter("values", $values, Connection::PARAM_STR_ARRAY)
            ->andWhere("t.lote_deleted is null");
        $s = $q->execute();
        while($row = $s->fetch()) {
            if($indexByField) {
                $result[$row[$field]] = $this->getEntityObject($row);
            } else {
                $result[] = $this->getEntityObject($row);
            }
        }
        return $result;
    }
    /**
     * Check if an object exist that matches the parameters given and if so return its ID
     * @param array $parameters
     * @return Boolean
     * @todo - consider multiple matches...
     */
    public function findObject(Array $parameters)
    {
        $result = false;
        if (count($parameters) > 0) {
            $q = $this->getReadDb()->createQueryBuilder();
            $q->select('*')
                ->from($this->getTableName(), 't');
            foreach ($parameters as $k => $v) {
                $q->andWhere($k . ' = :' . $k)
                    ->setParameter($k, $v);
            }
            $s = $q->setMaxResults(1)->execute();
            $result = $s->fetch(\PDO::FETCH_ASSOC);
        }
        return $result;
    }

    public function getPagingAsApiResponse($paging)
    {
        foreach ($paging as $key => $item) {
            $paging[$key] = (int)$item;
        }
        return $paging;
    }

}
