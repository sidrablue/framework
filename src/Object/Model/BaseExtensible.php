<?php
/**
 * Copyright (c) BlueSky Digital Labs Pty Ltd
 */

declare(strict_types = 1);
namespace BlueSky\Framework\Object\Model;

use Doctrine\DBAL\Query\QueryBuilder;
use BlueSky\Framework\Entity\Schema\Entity as SchemaEntity;
use BlueSky\Framework\Entity\Schema\Relation as RelationEntity;
use BlueSky\Framework\Entity\Schema\Field as FieldEntity;

class BaseExtensible extends Base
{

    public function getListObjectsByQuery(
        QueryBuilder $q,
        string $class,
        int $page = 1,
        int $resultsPerPage = 1000,
        ?string $countQuery = null,
        ?bool $groupByStatus = null,
        array $appendCustomFieldsToQuery = [],
        ?string $tableAlias = null
    ): array {
        if (!empty($appendCustomFieldsToQuery)) {
            $this->appendCustomFieldsToQuery($q, $class, $appendCustomFieldsToQuery, $tableAlias);
        }
        return parent::getListObjectsByQuery($q, $class, $page, $resultsPerPage, $countQuery ?? 'count(*) as cnt', $groupByStatus);
    }

    public function getDataObjectsByQuery(
        QueryBuilder $q,
        string $class,
        int $page = 1,
        int $resultsPerPage = 1000,
        array $appendCustomFieldsToQuery = [],
        ?string $tableAlias = null
    ): array {
        if (!empty($appendCustomFieldsToQuery)) {
            $this->appendCustomFieldsToQuery($q, $class, $appendCustomFieldsToQuery, $tableAlias);
        }
        return parent::getDataObjectsByQuery($q, $class, $page, $resultsPerPage);
    }

    public function appendCustomFieldsToQuery(
        QueryBuilder $q,
        string $class,
        array $customFieldsToAppend,
        ?string $tableAlias = null
    ): QueryBuilder {
        $alias = $tableAlias;
        if ($alias === null) {
            $from = $q->getQueryPart('from');
            if (!empty($from[0])) {
                $alias = $from[0]['table'];
                if (!empty($from[0]['alias'])) {
                    $alias = $from[0]['alias'];
                }
            }
        }

        if ($alias !== null) {
            /** @var BaseEntity|VirtualEntity $class - This is not an object instance */
            $e = $this->getState()->getSchema()->getEntityByReference($class::getObjectRef());
            $q = $this->addCustomFieldsToQuery($q, $e, $alias, $customFieldsToAppend);
        }
        unset($alias, $from, $e);
        return $q;
    }

    /**
     * This function is responsible for adding the additional query parameters required to load in all virtual groups as well
     *
     * @param QueryBuilder $q
     * @param SchemaEntity $rootEntity
     * @param string $tableAlias
     * @param FieldEntity[]|SchemaEntity[] $customFieldsToAppend
     * @return QueryBuilder
     */
    public function addCustomFieldsToQuery(QueryBuilder $q, SchemaEntity $rootEntity, string $tableAlias, array $customFieldsToAppend): QueryBuilder
    {
        $relationsManager = $rootEntity->getRelations();
        $xToManyGroupingFlagSet = false;
        $joinedEntitiesReferences = [];
        foreach ($customFieldsToAppend as $item) {
            $localFields = [];
            $localEntity = null;
            if ($item instanceof SchemaEntity) {
                $localFields = $item->getEntityFields();
                if ($item->is_virtual) {
                    $localFields = array_merge($localFields, SchemaEntity::getLoteFields());
                }
                $localEntity = $item;
            } elseif ($item instanceof FieldEntity) {
                $localFields = [$item];
                $localEntity = $item->getParentEntity();
            }
            if ($localEntity !== null) {
                if ($relation = $relationsManager->getRelationByToReference($localEntity->reference)) {
                    $isOneToOneRelation = $relation->type === RelationEntity::ONE_TO_ONE || $relation->type === RelationEntity::OBJECT_REF_ID_OO;
                    if (!$xToManyGroupingFlagSet && !$isOneToOneRelation) {
                        $xToManyGroupingFlagSet = true;
                        $q->addGroupBy("{$tableAlias}.id");
                    }

                    $tableName = $localEntity->getEntityTableName();
                    $formattedEntityRef = str_replace('\\', '.', $localEntity->reference);
                    if (!isset($joinedEntitiesReferences[$tableName])) {
                        $joinedEntitiesReferences[$tableName] = true;
                        $relation->leftJoin($q, $tableName, $tableAlias);
                        if ($isOneToOneRelation) {
                            if ($fe = $localEntity->getEntityField($relation->to_field_reference)) {
                                $this->addFormattedQueryColumnName($q, $formattedEntityRef, $tableName, $fe);
                            }
                            if ($fe = $localEntity->getEntityField('id')) {
                                $this->addFormattedQueryColumnName($q, $formattedEntityRef, $tableName, $fe);
                            }
                        }
                    }
                    if ($isOneToOneRelation) {
                        foreach ($localFields as $localField) {
                            $this->addFormattedQueryColumnName($q, $formattedEntityRef, $tableName, $localField);
                        }
                    } else {
                        $this->addIdsForManyColumn($q, $formattedEntityRef, $tableName);
                    }
                }
            }
        }
        return $q;
    }

    private function addIdsForManyColumn(QueryBuilder $q, string $formattedEntityRef, string $tableName): void
    {
        $columnName = "{$tableName}.id";
        $q->addSelect("group_concat(distinct({$columnName})) as `{$formattedEntityRef}.id`");
    }

    /**
     * This function will decompose the field definition into formatted column name that can be passed to the database
     *
     * @param QueryBuilder $q
     * @param string $entityReferenceFormatted - replace \ with .
     * @param string $tableName
     * @param FieldEntity $field
     * @see beforeFetch
     *
     */
    private function addFormattedQueryColumnName(QueryBuilder $q, string $entityReferenceFormatted, string $tableName, FieldEntity $field): void
    {
        $columnName = $tableName . '.' . $field->reference;
        $columnNameAlias = $entityReferenceFormatted . '.' . $field->reference;
        $q->addSelect("{$columnName} as `{$columnNameAlias}`");
        unset($columnName, $columnNameAlias);
    }

}
