<?php
namespace BlueSky\Framework\Object\Model;

/**
 * Base model for tables with custom fields
 */
abstract class CustomField extends Base
{

    /**
     *
     * */
    public function getCustomValue($userId, $reference, $default = null)
    {
        $result = $default;
        $q = $this->getState()->getWriteDb()->createQueryBuilder();
        $q->select('v.*, c.field_type')
            ->from($this->getTableName().'__cf_value', 'v')
            ->leftJoin('v','sb__cf_field', 'c', 'c.id = v.field_id')
            ->where('v.object_id = :user_id')
            ->setParameter('user_id', $userId)
            ->andWhere('c.reference = :reference')
            ->setParameter('reference', $reference);
        $r = $q->execute();
        if($data = $r->fetch(\PDO::FETCH_ASSOC)) {
            if($data['field_type']=='string') {
                $result = $data['value_string'];
            }
            elseif($data['field_type']=='int_array') {
                $result = json_decode($data['value_text']);
            }

        }
        return $result;
    }

    /**
     * Get all of the custom values for an object
     *  */
    public function getCustomValues($id) {
        $data = '';


    }

    /**
     * Get all of the custom fields for an object
     * @return array
     * */
    public function getCustomFields()
    {
        return [];
    }

    private function loadCustomFields($force = false)
    {

    }

    /**
     * Delete an object
     *
     * @access public
     * @param int $id - the object to delete
     * @param bool $strongDelete - Whether or not to delete the table object completely
     * @return void
     */
    public function delete($id, $strongDelete = false)
    {
        parent::delete($id, $strongDelete);
        $this->deleteCustomFields($id);
    }

    /**
     * Delete the custom fields for an entity
     *
     * @access public
     * @param int $id - the ID of the entity to delete the fields for
     * @return void
     * */
    public function deleteCustomFields($id)
    {
        $this->getState()->getWriteDb()->update($this->getTableName().'__cf_value', ['lote_deleted'=>date('c')], ['object_id' => $id]);
        $this->getState()->getWriteDb()->update($this->getTableName().'__cf_value_group', ['lote_deleted'=>date('c')], ['object_id' => $id]);
    }

}
