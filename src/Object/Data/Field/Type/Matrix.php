<?php
namespace BlueSky\Framework\Object\Data\Field\Type;

use Lote\App\Form\Entity\RecordValue;
use BlueSky\Framework\Entity\Cf\CfField;
use BlueSky\Framework\Entity\User\CfValue;
use BlueSky\Framework\Util\Strings;

class Matrix extends Base
{

    /**
     * @var boolean $supportsExporting
     * True if this object can be used in exports
     * */
    protected $supportsExporting = true;

    public function getIndices()
    {
        $this->getValue();
        $result = [];
        if ($this->configData && isset($this->configData['columns'])) {
            $result['columns'] = $this->configData['columns'];
        }
        if ($this->configData && isset($this->configData['rows'])) {
            $result['rows'] = $this->configData['rows'];
        }
        return $result;
    }

    /**
     * Function validate
     * @access public
     * @param
     * @return boolean $valid - true if this field is valid
     * */
    public function validate()
    {
        $data = [];
        if(is_string($this->definition->config_data)) {
            $data = json_decode($this->definition->config_data, true);
        }
        if(isset($data['matrix_field_type']) && in_array($data['matrix_field_type'], ['radio', 'checkbox'])) {
            $result = true;
            $submitData = json_decode($this->getValue(), true);
            if ($this->definition->mandatory) {
                foreach($data['rows'] as $v) {
                    if(!isset($submitData[Strings::generateHtmlId($v)]) || count($submitData[Strings::generateHtmlId($v)]) == 0) {
                        $result = false;
                        $this->errorMessages[] = 'Please specify selections in all rows';
                        break;
                    }
                }
            }
        }
        else {
            $result = parent::validate();
        }
        return $result;
    }

    /**
     * Get the export columns for this cfField
     * @param CfField $field
     * @return array
     * */
    public function getExportColumns(CfField $field, $includeGroupName = false)
    {
        $result = [$field->name];
        if(is_string($field->config_data)) {
            $data = json_decode($field->config_data, true);
        }
        elseif(is_array($field->config_data)) {
            $data = $field->config_data;
        }
        if (isset($data) && isset($data['rows']) && isset($data['columns'])) {
            foreach ($data['rows'] as $i) {
                foreach ($data['columns'] as $j) {
                    $result[] = "$i - $j";
                }
            }
        }
        return $result;
    }

    /**
     * Get the export columns for this cfField
     * @param CfField $field
     * @param array|false $exportData
     * @return array
     * */
    public function getExportCells(CfField $field, $exportData)
    {
        $exportData = json_decode($exportData['value_text'], true);
        if (is_string($exportData)) {
            $exportData = json_decode($exportData, true);
        }
        $result = [''];
        if(is_string($field->config_data)) {
            $data = json_decode($field->config_data, true);
        }
        elseif(is_array($field->config_data)) {
            $data = $field->config_data;
        }
        if (isset($data) && isset($data['rows']) && isset($data['columns'])) {
            foreach ($data['rows'] as $i) {
                foreach ($data['columns'] as $j) {
                    $val = '';
                    $key = Strings::generateHtmlId($i) . '_' . Strings::generateHtmlId($j);
                    if (isset($exportData[$key])) {
                        $val = trim($exportData[$key]);
                    }
                    $result[] = $val;
                }
            }
        }

        return $result;
    }

    /**
     * Get the value to be saved
     * @access protected
     * @param CfValue $v
     * @return string
     * */
    protected function setValueData(CfValue $v)
    {
        if(!Strings::isJson($this->getValue())) {
            $str = $this->getValue();
            if(!$str) {
                $v->value_text = "";
            }
            else {
                $v->value_text = json_encode($this->getValue());
            }
        }
        elseif(is_scalar($this->getValue())) {
            $v->value_text = $this->getValue();
        }
        return $v->value_text;
    }

    /**
     * Set the value of this field
     * @access public
     * @param CfValue $value
     * */
    public function setValueFromObject(CfValue $value)
    {
        $this->value = json_decode($value->value_text, true);
    }

    /**
     * Get the view data of this field
     * @access public
     * @param int $objectId
     * @return mixed
     * */
    public function getViewData($objectId = 0)
    {
        $result = $this->value;
        if ($result == null) {
            $handlers = $this->getState()->getSignal()->getHandlers(
                'form.field.get_edit_data.' . $this->definition->id
            );
            if ($handlers) {
                foreach ($handlers as $h) {
                    $callback = $h[0]->callback;
                    $result = $callback($result, $objectId, $this);
                }
            }
        }
        return $result;
    }

    /**
     * Get a string representation of the display value
     * @param RecordValue|array $recordValue
     * @return string
     * */
    public function getDisplayValue($recordValue)
    {
        $result = "";
        if($recordValue instanceof RecordValue) {
            $data = json_decode($recordValue->value_text, true);
        }
        elseif(is_array($recordValue) && isset($recordValue['value_text'])) {
            $data = json_decode($recordValue['value_text'], true);
        }
        if(isset($data) && is_array($data)) {
            $result = nl2br(implode(",", $data));
        }
        return $result;
    }

    /**
     * Get the column that is used to store the data of this custom field
     * @access public
     * @return string
     * */
    public function getValueColumn()
    {
        return 'value_text';
    }

    public function getEditData()
    {
        $this->getValue();
        $result = [];
        if($this->configData && isset($this->configData['matrix_field_type'])) {
            if(isset($this->configData['matrix_field_type'])) {
                $result['matrix_field_type'] = $this->configData['matrix_field_type'];
            }
        }
        return $result;
    }

    public function getPdfExportData($data)
    {
        $config = json_decode($data['config'], true);
        $rows = $config['rows'];
        $columns = $config['columns'];
        $type = $config['matrix_field_type'];

        $textBased = false;
        if ($type == 'text') {
            $textBased = true;
        }

        $answers = [];
        for ($i = 0; $i < count($rows); $i++) {
            $answers[$rows[$i]] = [];
            for ($j = 0; $j < count($columns); $j++) {
                $answers[$rows[$i]][$columns[$j]] = [];
            }
        }

        $totalAnswersPerRow = [];
        foreach ($data['answers'] as $answer) {
            $answer = json_decode($answer, true);
            foreach ($answer as $key => $valueArr) {
                $rowIndex = array_search(strtolower($key), array_map(function($row) { return str_replace(' ', '_', strtolower($row)); }, $rows));
                $totalAnswersPerRow[] = 0;
                foreach ($valueArr as $value => $text) {
                    $columnIndex = array_search(strtolower($value), array_map(function($column) { return str_replace(' ', '_', strtolower($column)); }, $columns));
                    if (!$textBased) {
                        if (!isset($answers[$rows[$rowIndex]][$columns[$columnIndex]]['count'])) {
                            $answers[$rows[$rowIndex]][$columns[$columnIndex]]['count'] = 0; // init the counter
                        }
                        $answers[$rows[$rowIndex]][$columns[$columnIndex]]['count']++;
                        $totalAnswersPerRow[count($totalAnswersPerRow) - 1]++;
                    } else {
                        $answers[$rows[$rowIndex]][$columns[$columnIndex]][] = $text;
                    }
                }
            }
        }

        // fill remaining fields if not full
        if (!$textBased) {
            for ($i = 0; $i < count($rows); $i++) {
                for ($j = 0; $j < count($columns); $j++) {
                    if (!isset($answers[$rows[$i]][$columns[$j]]['count'])) {
                        $answers[$rows[$i]][$columns[$j]]['count'] = 0;
                    }

                    $answers[$rows[$i]][$columns[$j]]['weighting'] = (float)$answers[$rows[$i]][$columns[$j]]['count'] / (float)$totalAnswersPerRow[$i];
                }
            }
        }

        $results = [];
        $results['results'] = $answers;
        $results['options'] = [];
        $results['options']['textBased'] = $textBased;

        return $results;
    }

}
