<?php
namespace BlueSky\Framework\Object\Data\Field\Type;

class Country extends Base
{

    public function validate()
    {
        $result = parent::validate();
        if($result && !is_numeric($this->getValue())) {
            $result = false;
            $this->error = 'Invalid numeric valid';
        }
        return $result;
    }

}
