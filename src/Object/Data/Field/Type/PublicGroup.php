<?php
namespace BlueSky\Framework\Object\Data\Field\Type;

use Lote\System\Schoolzine\Model\Metadata\Region;
use BlueSky\Framework\Entity\User\CfValue;
use BlueSky\Framework\Model\Group;
use BlueSky\Framework\Util\Arrays;

class PublicGroup extends Base
{

    const FIELD_ERROR_INVALID_GROUP_SELECTION = 100;

    /**
     * Get the view data of this field
     * @access public
     * @param int $objectId
     * @return mixed
     * */
    public function getViewData($objectId = 0)
    {
        $result = [];
        if($this->getValue()) {
            $g = new Group($this->getState());
            if(is_array($this->getValue())) {
                $groups = $g->getGroupList($this->getValue());
            }
            elseif(is_string($this->getValue())) {
                $value = json_decode($this->getValue(), true);
                $selectedGroups = [];
                foreach($value as $k=>$v) {
                    if($v) {
                        $selectedGroups[] = $k;
                    }
                }
                $groups = $g->getGroupList($selectedGroups);
            }

            if(is_array($groups)) {
                foreach ($groups as $v) {
                    $result[] = Arrays::cleanArray($v, ['id', 'name', 'description']);
                }
            }
        }
        return $result;
    }

    public function getEditData($isPreview = false)
    {
        $values = parent::getEditData($isPreview);
        if($values) {
            $values = json_decode($values, true);
        }
        $result = [];
        $m = new Group($this->getState());
        if($this->configData && $this->configData['tags'] && count($this->configData['tags']) > 0) {
            $groups = $m->getByTag($this->configData['tags']);
        }
        else {
            $groups = $m->getPublicGroupsList();
        }
        if(is_array($groups)) {
            $userGroups = [];
            if($this->getState()->getUser()->id > 0) {
                $userGroups = $m->getUserGroups($this->getState()->getUser()->id);
                $userGroups = Arrays::getFieldValuesFrom2dArray($userGroups, 'id');
            }
            foreach ($groups as $v) {
                if ($v['active'] && $v['is_public'] && ($v['lote_deleted'] == null || !$v['lote_deleted'])) {
                    $v = Arrays::cleanArray($v, ['id', 'name', 'description']);
                    if(($userGroups && !$isPreview && in_array($v['id'], $userGroups))
                        || ($values && isset($values[$v['id']]) && $values[$v['id']])) {
                        $v['_selected'] = true;
                    }
                    $result[] = $v;
                }
            }
        }
        return $result;
    }

    public function validate()
    {
        $result = true;
        $v = array_filter(json_decode($this->getValue(), true));
        if ($this->definition->mandatory && count($v) == 0) {
            $this->error = self::FIELD_ERROR_INVALID_GROUP_SELECTION;
            $result = false;
        }
        return $result;
    }

    /**
     * Get the error code of the current field, if one is specified
     * @access public
     * @return int
     * */
    public function getErrorText()
    {
        if($this->error = self::FIELD_ERROR_INVALID_GROUP_SELECTION) {
            $text = "Please select one or more groups";
        }
        else {
            $text = parent::getErrorText();
        }
        return $text;
    }


    /**
     * Get the value to be saved
     * @access protected
     * @param CfValue $v
     * @return string
     * */
    protected function setValueData(CfValue $v)
    {
        return $v->value_text = json_encode($this->getValue());
    }

    /**
     * Set the value of this field
     * @access public
     * @param CfValue $value
     * */
    public function setValueFromObject(CfValue $value)
    {
        $this->value = json_decode($value->value_text, true);
    }

    /**
     * Get the column that is used to store the data of this custom field
     * @access public
     * @return string
     * */
    public function getValueColumn()
    {
        return 'value_text';
    }

    public function getPdfExportData($data)
    {
        $config = $data['config'];

        $q = $this->getState()->getReadDb()->createQueryBuilder();
        $q->select('group_id, group_name')
            ->from('sb__group')
            ->where('lote_deleted is null')
            ->andWhere('is_public = :public')
            ->setParameter('public', 1)
            ->andWhere('kind = :user')
            ->setParameter('user', 'user')
            ->andWhere('active = :active')
            ->setParameter('active', 1);

        if ($config != 'null') { // all tags are not selectable
            $q->andWhere('name in (:names)')
                ->setParameter('names', $config['tags'], \Doctrine\DBAL\Connection::PARAM_STR_ARRAY);
        }

        $tags = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);

        $selectedTags = [];
        $totalSelected = 0;
        foreach ($data['answers'] as $answerRaw) {
            $answer = json_decode($answerRaw);

            foreach ($answer as $groupId => $selected) {
                if ($selected == 1) {
                    if (!isset($selectedTags[$groupId])) {
                        $selectedTags[$groupId] = 0;
                    }

                    $selectedTags[$groupId]++;
                    $totalSelected++;
                }
            }
        }

        $keys = array_keys($selectedTags);
        foreach ($tags as $index => $tag) {
            if (in_array($tag['group_id'], $keys)) {
                $tags[$index]['count'] = $selectedTags[$tag['group_id']];
            } else {
                $tags[$index]['count'] = 0;
            }

            $tags[$index]['weighting'] = $totalSelected > 0 ? ((float)$tags[$index]['count'] / (float)$totalSelected) : 0.0;
        }

        $results = [];
        $results['results'] = $tags;
        $results['options'] = [];

        return $results;
    }
}
