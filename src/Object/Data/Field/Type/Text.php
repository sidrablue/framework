<?php
namespace BlueSky\Framework\Object\Data\Field\Type;


class Text extends Base
{

    /**
     * @var boolean $supportsImporting
     * True if this object can be used in imports
     * */
    protected $supportsImporting = true;

    /**
     * @var boolean $supportsQueryBuilder
     * True if this object can be used in the query builder
     * */
    protected $supportsQueryBuilder = true;

    const FIELD_ERROR_RESTRICTION_ALPHA = 100;
    const FIELD_ERROR_RESTRICTION_NUMERIC = 101;
    const FIELD_ERROR_RESTRICTION_EMAIL = 102;
    const FIELD_ERROR_MIN_LENGTH = 103;
    const FIELD_ERROR_MAX_LENGTH = 104;
    const FIELD_ERROR_POSTCODE_FORMAT = 105;

    public function readOnly()
    {
        $result = 0;
        if (isset($this->configData['read_only'])) {
            $result = $this->configData['read_only'];
        }
        return $result;
    }

    public function inputRestriction()
    {
        $result = '';
        if (isset($this->configData['input_restriction'])) {
            $result = $this->configData['input_restriction'];
        }
        else if($this->definition->db_field == 'address_postcode') {
            $result = 'postcode';
        }
        return $result;
    }

    public function minLength()
    {
        $result = false;
        if (isset($this->configData['min_length']) && !empty($this->configData['min_length'])) {
            $result = $this->configData['min_length'];
        }
        return $result;
    }

    public function maxLength()
    {
        $result = false;
        if (isset($this->configData['max_length']) && !empty($this->configData['max_length'])) {
            $result = $this->configData['max_length'];
        }
        return $result;
    }

    public function validate()
    {
        $result = parent::validate();
        if($result && $this->inputRestriction() == 'alphabet' && !ctype_alpha($this->getValue())) {
            $result = false;
            $this->error = self::FIELD_ERROR_RESTRICTION_ALPHA;
        }
        else if($result && $this->inputRestriction() == 'numeric' && !preg_match('/^[0-9]*$/', $this->getValue())) {
            $result = false;
            $this->error = self::FIELD_ERROR_RESTRICTION_NUMERIC;
        }
        else if($result && $this->inputRestriction() == 'email' && !filter_var($this->getValue(), FILTER_VALIDATE_EMAIL) && strlen($this->getValue()) > 0) {
            $result = false;
            $this->error = self::FIELD_ERROR_RESTRICTION_EMAIL;
        }
        else if($result && $this->minLength() !== false && strlen($this->getValue()) < $this->minLength()) {
            $result = false;
            $this->error = self::FIELD_ERROR_MIN_LENGTH;
        }
        else if($result && $this->maxLength() !== false && strlen($this->getValue()) > $this->maxLength()) {
            $result = false;
            $this->error = self::FIELD_ERROR_MAX_LENGTH;
        }
        else if($result && $this->inputRestriction() == 'postcode' && !filter_var($this->getValue(), FILTER_VALIDATE_REGEXP, array(
                "options" => array("regexp"=>'/^[0-9]{4,}$/')
            )) && strlen($this->getValue()) > 0) {
            $result = false;
            $this->error = self::FIELD_ERROR_POSTCODE_FORMAT;
        }
        return $result;
    }

    /**
     * Get the error text
     * @access public
     * @return string
     * */
    public function getErrorText()
    {
        if($this->error == self::FIELD_ERROR_RESTRICTION_ALPHA) {
            $text = "Only alphabet characters allowed";
        }
        elseif($this->error == self::FIELD_ERROR_RESTRICTION_NUMERIC) {
            $text = "Only numeric characters allowed";
        }
        elseif($this->error == self::FIELD_ERROR_RESTRICTION_EMAIL) {
            $text = "Invalid email format";
        }
        elseif($this->error == self::FIELD_ERROR_MIN_LENGTH) {
            $text = "Input does not meet the required amount of characters (requires at least " . $this->minLength() . ")";
        }
        elseif($this->error == self::FIELD_ERROR_MAX_LENGTH) {
            $text = "Input exceeds the allowed amount of characters (" . $this->maxLength() . ")";
        }
        elseif($this->error == self::FIELD_ERROR_POSTCODE_FORMAT) {
            $text = "Invalid postcode format";
        }
        else {
            $text = parent::getErrorText();
        }
        return $text;
    }

    /**
     * Get the query builder clause for this field type
     * @access public
     * @param string $joinTable
     * @param string $value
     * @param string $labelPrefix
     * @return array
     * */
    public function getQueryBuilderClause($joinTable, $value, $labelPrefix)
    {
        return [
            'field' => 'value_string',
            'field_type' => 'custom_field',
            'data_type' => 'string',
            'label' => $labelPrefix . ' : ' . $this->definition->name,
            'clause_type' => 'join',
            'join_table' => 'sb__user__cf_value',
            'join_field_from' => 'object_id',
            'join_field_to' => 'id',
            'join_clause_field_from' => 'field_id',
            'join_clause_operator' => 'equals',
            'join_clause_value' => $value,
        ];
    }

}
