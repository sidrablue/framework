<?php
namespace BlueSky\Framework\Object\Data\Field\Type;

use BlueSky\Framework\Model\User;

class Email extends Base
{

    /**
     * @var boolean $supportsImporting
     * True if this object can be used in imports
     * */
    protected $supportsImporting = true;

    /**
     * @var boolean $supportsQueryBuilder
     * True if this object can be used in the query builder
     * */
    protected $supportsQueryBuilder = true;

    /**
     * Validate the value provided to this custom field type to ensure that it conforms to its structure
     * @access protected
     * @param mixed $value
     * @return boolean
     * */
    public function validateValue($value)
    {
        return (is_string($value) && ($value == '' || filter_var($value, FILTER_VALIDATE_EMAIL)));
    }

    const FIELD_ERROR_INVALID_EMAIL_FORMAT = 100;
    const FIELD_ERROR_DUPLICATE_EMAIL = 101;

    public function validate()
    {
        $result = parent::validate();
        if($result && !filter_var($this->getValue(), FILTER_VALIDATE_EMAIL)) {
            $result = false;
            $this->error = self::FIELD_ERROR_INVALID_EMAIL_FORMAT;
        }
/*        elseif($this->getState()->getUser()->id != null && $this->definition->db_field=='email' && $this->definition->storage_type=='user'){
            $u = new User($this->getState());
            if($u->emailExists($this->getValue(), $this->getState()->getUser()->id)) {
                $result = false;
                $this->error = self::FIELD_ERROR_DUPLICATE_EMAIL;
            }
        }*/
        return $result;
    }

    /**
     * Get the error text
     * @access public
     * @return string
     * */
    public function getErrorText()
    {
        if($this->error == self::FIELD_ERROR_INVALID_EMAIL_FORMAT) {
            $text = "Invalid email format";
        }
        elseif($this->error == self::FIELD_ERROR_DUPLICATE_EMAIL) {
            $text = "This email already exists";
        }
        else {
            $text = parent::getErrorText();
        }
        return $text;
    }

    /**
     * Get the query builder clause for this field type
     * @access public
     * @param string $joinTable
     * @param string $value
     * @param string $labelPrefix
     * @return array
     * */
    public function getQueryBuilderClause($joinTable, $value, $labelPrefix)
    {
        return [
            'field' => 'value_string',
            'field_type' => 'custom_field',
            'data_type' => 'string',
            'label' => $labelPrefix . ' : ' . $this->definition->name,
            'clause_type' => 'join',
            'join_table' => $joinTable,
            'join_field_from' => 'object_id',
            'join_field_to' => 'id',
            'join_clause_field_from' => 'field_id',
            'join_clause_operator' => 'equals',
            'join_clause_value' => $value,
        ];
    }

}
