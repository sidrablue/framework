<?php
namespace BlueSky\Framework\Object\Data\Field\Type;


use Lote\App\Form\Entity\RecordValue;
use BlueSky\Framework\Entity\User\CfValue;

class Boolean extends Base
{

    public function validate()
    {
        $result = parent::validate();
        return $result;
    }

    /**
     * Get the value to be saved
     * @access protected
     * @param CfValue $v
     * @return string
     * */
    protected function setValueData(CfValue $v)
    {
        $v->value_boolean = $this->getValue();
        return $this->getValue();
    }

    /**
     * Set the value of this field
     * @access public
     * @param CfValue $value
     * */
    public function setValueFromObject(CfValue $value)
    {
        $this->value = $value->value_boolean;
    }

    /**
     * Get a string representation of the display value
     * @param RecordValue $recordValue
     * @return string
     * */
    public function getDisplayValue($recordValue)
    {
        $result = "no";
        if($recordValue instanceof RecordValue) {
            $result = $recordValue->value_number?"yes":"no";
        }
        return $result;
    }

    /**
     * Get the column that is used to store the data of this custom field
     * @access public
     * @return string
     * */
    public function getValueColumn()
    {
        return 'value_boolean';
    }

}
