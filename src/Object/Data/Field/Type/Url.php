<?php
namespace BlueSky\Framework\Object\Data\Field\Type;


use BlueSky\Framework\Util\Url as UrlUtil;

class Url extends Base
{

    /**
     * @var boolean $supportsImporting
     * True if this object can be used in imports
     * */
    protected $supportsImporting = true;

    /**
     * @var boolean $supportsQueryBuilder
     * True if this object can be used in the query builder
     * */
    protected $supportsQueryBuilder = true;

    const FIELD_ERROR_INVALID_URL_FORMAT = 100;

    /**
     * Validate the value provided to this custom field type to ensure that it conforms to its structure
     * @access protected
     * @param mixed $value
     * @return boolean
     * */
    public function validateValue($value)
    {
        $value = UrlUtil::addScheme($value);
        return (is_string($value) && ($value == '' || filter_var($value, FILTER_VALIDATE_URL)));
    }

    public function validate()
    {
        $result = parent::validate();
        if (!($this->value == '' && !$this->definition->mandatory) && ($result && !filter_var(
                    $this->getValue(),
                    FILTER_VALIDATE_URL
                ))
        ) {
            $result = false;
            $this->error = self::FIELD_ERROR_INVALID_URL_FORMAT;
        }
        return $result;
    }

    /**
     * Get the error code of the current field, if one is specified
     * @access public
     * @return int
     * */
    public function getErrorText()
    {
        if ($this->error == self::FIELD_ERROR_INVALID_URL_FORMAT) {
            $text = "Invalid URL format. Please format as http://example.com";
        } else {
            $text = parent::getErrorText();
        }
        return $text;
    }

    /**
     * Get the query builder clause for this field type
     * @access public
     * @param string $joinTable
     * @param string $value
     * @param string $labelPrefix
     * @return array
     * */
    public function getQueryBuilderClause($joinTable, $value, $labelPrefix)
    {
        return [
            'field' => 'value_string',
            'field_type' => 'custom_field',
            'data_type' => 'string',
            'label' => $labelPrefix . ' : ' . $this->definition->name,
            'clause_type' => 'join',
            'join_table' => $joinTable,
            'join_field_from' => 'object_id',
            'join_field_to' => 'id',
            'join_clause_field_from' => 'field_id',
            'join_clause_operator' => 'equals',
            'join_clause_value' => $value,
        ];
    }

}