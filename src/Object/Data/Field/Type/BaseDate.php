<?php
namespace BlueSky\Framework\Object\Data\Field\Type;

use Lote\App\Form\Entity\RecordValue;
use BlueSky\Framework\Entity\User\CfValue;
use BlueSky\Framework\Util\Date;

class BaseDate extends Base
{

    /**
     * @var int - the error code for the date being otu of range
     * */
    const FIELD_ERROR_OUT_OF_RANGE = 100;

    /**
     * @var boolean $supportsImporting
     * True if this object can be used in imports
     * */
    protected $supportsImporting = true;

    /**
     * @var boolean $supportsQueryBuilder
     * True if this object can be used in the query builder
     * */
    protected $supportsQueryBuilder = true;

    /**
     * Get the value to be saved
     * @access protected
     * @param CfValue $v
     * @return string
     * */
    protected function setValueData(CfValue $v)
    {
        $v->value_date = $this->getValue();
        return $this->getValue();
    }

    /**
     * Set the value of this field
     * @access public
     * @param CfValue $value
     * */
    public function setValueFromObject(CfValue $value)
    {
        $this->value = $value->value_date;
    }

    /**
     * Get a string representation of the display value
     * @param RecordValue $recordValue
     * @return string
     * */
    public function getDisplayValue($recordValue)
    {
        $result = "";
        if($recordValue instanceof RecordValue) {
            $result = Date::asString($recordValue->value_date, $this->getState()->getSettings()->get("system.date_format", 'Y-m-d'));
        }
        elseif(is_array($recordValue) && isset($recordValue['value_date'])) {
            $result = Date::asString($recordValue['value_date'], $this->getState()->getSettings()->get("system.date_format", 'Y-m-d'));
        }
        return $result;
    }

    /**
     * Get the column that is used to store the data of this custom field
     * @access public
     * @return string
     * */
    public function getValueColumn()
    {
        return 'value_date';
    }

}
