<?php
namespace BlueSky\Framework\Object\Data\Field\Type;


class Heading extends Base
{

    /**
     * @var boolean $supportsImporting
     * True if this object can be used in imports
     * */
    protected $supportsImporting = false;

    protected $supportsExporting = false;

    /**
     * @var boolean $supportsQueryBuilder
     * True if this object can be used in the query builder
     * */
    protected $supportsQueryBuilder = false;

}
