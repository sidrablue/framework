<?php
namespace BlueSky\Framework\Object\Data\Field\Type;

use Lote\App\Form\Entity\RecordValue;
use BlueSky\Framework\Util\Strings;
use BlueSky\Framework\Util\Time as TimeUtil;
use BlueSky\Framework\Util\Date as DateUtil;

/**
 *
 * */
class DateTime extends BaseDate
{

    const DATE_INVALID_PAST = 101;
    const DATE_INVALID_FUTURE = 102;
    const DATE_INVALID_BEFORE = 103;
    const DATE_INVALID_AFTER = 104;
    const DATE_INVALID_BETWEEN = 105;

    const TIME_INVALID_PAST = 106;
    const TIME_INVALID_FUTURE = 107;
    const TIME_INVALID_BEFORE = 108;
    const TIME_INVALID_AFTER = 109;
    const TIME_INVALID_BETWEEN = 110;

    /**
     * Get the data for the editing of this field
     * @access public
     * @return mixed
     * */
    public function getEditData($isPreview = false)
    {
        if($this->value == "0000-00-00 00:00:00" || is_null($this->value) || $this->value == "") {
            $date = "";
        } else {
            $date = DateUtil::asString($this->value, $this->getState()->getSettings()->get('system.datetime.format', 'Y-m-d G:i:s'));
        }
        return $date;
    }

    /**
     * Validate the value provided to this custom field type to ensure that it conforms to its structure
     * @access protected
     * @param mixed $value
     * @return boolean
     * */
    public function validateValue($value)
    {
        return ($value instanceof \DateTime) || (is_string($value) && ($value == '' || Strings::isValidDate($value)));
    }

    /**
     * Get the view data of this field
     * @access public
     * @param int $objectId
     * @return mixed
     * */
    public function getViewData($objectId = 0)
    {
        if($this->value == "0000-00-00 00:00:00" || is_null($this->value) || $this->value == "") {
            $date = "";
        } else {
            $date = DateUtil::asString($this->value, $this->getState()->getSettings()->get('system.datetime.format', 'Y-m-d G:i:s'));
        }
        return $date;
    }

    public function validate()
    {
        $result = parent::validate();
        if($result && !TimeUtil::strToDateTime($this->getValue())) {
            $result = false;
            $this->error = 'Invalid time date';
        }

        $value = $this->getValue();

        if(isset($this->definition->entityData['config_data']) && $result != false) {
            $config_data = json_decode($this->definition->entityData['config_data'], true);
            if(isset($config_data['allowed_dates'])) {
                if($config_data['allowed_dates'] == 'past') {
//                    $current = date("Y-m-d");
//                    $value_date = date('Y-m-d', strtotime($value));
                    $current = time();
                    $value_date = strtotime($value);
                    if($value_date > $current) {
                        $this->error = self::DATE_INVALID_PAST;
                        $result = false;
                    }
                } else if($config_data['allowed_dates'] == 'future') {
//                    $current = date("Y-m-d");
//                    $value_date = date('Y-m-d', strtotime($value));
                    $current = time();
                    $value_date = strtotime($value);
                    if($value_date < $current) {
                        $this->error = self::DATE_INVALID_FUTURE;
                        $result = false;
                    }
                } else if($config_data['allowed_dates'] == 'after') {
                    $after = date("Y-m-d", strtotime($config_data['date']));
                    $value_date = date('Y-m-d', strtotime($value));
                    if($value_date < $after) {
                        $this->error = self::DATE_INVALID_AFTER;
                        $result = false;
                    }
                } else if($config_data['allowed_dates'] == 'before') {
                    $before = date("Y-m-d", strtotime($config_data['date']));
                    $value_date = date('Y-m-d', strtotime($value));
                    if($value_date > $before) {
                        $this->error = self::DATE_INVALID_BEFORE;
                        $result = false;
                    }
                } else if($config_data['allowed_dates'] == 'between') {
                    $from = date("Y-m-d", strtotime($config_data['date_from']));
                    $to = date("Y-m-d", strtotime($config_data['date_to']));
                    $value_date = date('Y-m-d', strtotime($value));
                    if($value_date < $from || $value_date > $to) {
                        $this->error = self::DATE_INVALID_BETWEEN;
                        $result = false;
                    }
                }
            }


            if(isset($config_data['allowed_times'])) {
                /*if($config_data['allowed_times'] == 'past') {
                    $current = date("H:i:s");
                    $value_time = date('H:i:s', strtotime($value));
                    if($value_time > $current) {
                        $this->error = self::TIME_INVALID_PAST;
                        $result = false;
                    }
                } else if($config_data['allowed_times'] == 'future') {
                    $current = date("H:i:s");
                    $value_time = date('H:i:s', strtotime($value));
                    if($value_time < $current) {
                        $this->error = self::TIME_INVALID_FUTURE;
                        $result = false;
                    }
                } else*/ if($config_data['allowed_times'] == 'after') {
                    $after = date("H:i:s", strtotime($config_data['time']));
                    $value_time = date('H:i:s', strtotime($value));
                    if($value_time < $after) {
                        $this->error = self::TIME_INVALID_AFTER;
                        $result = false;
                    }
                } else if($config_data['allowed_times'] == 'before') {
                    $before = date("H:i:s", strtotime($config_data['time']));
                    $value_time = date('H:i:s', strtotime($value));
                    if($value_time > $before) {
                        $this->error = self::TIME_INVALID_BEFORE;
                        $result = false;
                    }
                } else if($config_data['allowed_times'] == 'between') {
                    $from = date("H:i:s", strtotime($config_data['time_from']));
                    $to = date("H:i:s", strtotime($config_data['time_to']));
                    $value_time = date('H:i:s', strtotime($value));
                    if($value_time < $from || $value_time > $to) {
                        $this->error = self::TIME_INVALID_BETWEEN;
                        $result = false;
                    }
                }
            }
        }
        elseif(!$this->definition->mandatory && ($value == '' || $value == null)) {
            $result = true;
        }

        return $result;
    }

    /**
     * Get the error code of the current field, if one is specified
     * @access public
     * @return int
     * */
    public function getErrorText()
    {
        switch ($this->error) {
            case self::DATE_INVALID_PAST:
                $text = "Date must be in the past";
                break;
            case self::DATE_INVALID_FUTURE:
                $text = "Date must be in the future";
                break;
            case self::DATE_INVALID_BEFORE:
                $text = "Date must be before " . $this->getDateRange();
                break;
            case self::DATE_INVALID_AFTER:
                $text = "Date must be after " . $this->getDateRange();
                break;
            case self::DATE_INVALID_BETWEEN:
                $text = "Date must be between " . $this->getDateRange();
                break;
            case self::TIME_INVALID_PAST:
                $text = "Time must be in the past";
                break;
            case self::TIME_INVALID_FUTURE:
                $text = "Time must be in the future";
                break;
            case self::TIME_INVALID_BEFORE:
                $text = "Time must be before " . $this->getTimeRange();
                break;
            case self::TIME_INVALID_AFTER:
                $text = "Time must be after " . $this->getTimeRange();
                break;
            case self::TIME_INVALID_BETWEEN:
                $text = "Time must be between " . $this->getTimeRange();
                break;
            default:
                $text = parent::getErrorText();
        }
//        if($this->error == self::FIELD_ERROR_OUT_OF_RANGE) {
//            $text = "Out of range";
//        }
//        else {
//            $text = parent::getErrorText();
//        }
        return $text;
    }

    /**
     * returns the date range of the variable if it exists
     * @return string|false
     */
    private function getDateRange()
    {
        $text = false;
        if(isset($this->definition->entityData['config_data'])) {
            $configData = json_decode($this->definition->entityData['config_data'], true);
            if (isset($configData['allowed_dates'])) {
                if ($configData['allowed_dates'] == 'after' || $configData['allowed_dates'] == 'before') {
                    $text = date("d-m-Y", strtotime($configData['date']));
                } elseif ($configData['allowed_dates'] == 'between') {
                    $text = date("d-m-Y", strtotime($configData['date_from'])) . ' and ' . date("d-m-Y", strtotime($configData['date_to']));
                }
            }
        }
        return $text;
    }

    /**
     * returns the time range of the variable if it exists
     * @return string|false
     */
    private function getTimeRange()
    {
        $text = false;
        if(isset($this->definition->entityData['config_data'])) {
            $configData = json_decode($this->definition->entityData['config_data'], true);
            if (isset($configData['allowed_times'])) {
                if ($configData['allowed_times'] == 'after' || $configData['allowed_times'] == 'before') {
                    $text = date("h:i A", strtotime($configData['time']));
                } elseif ($configData['allowed_times'] == 'between') {
                    $text = date("h:i A", strtotime($configData['time_from'])) . ' and ' . date("h:i A", strtotime($configData['time_to']));
                }
            }
        }
        return $text;
    }

    /**
     * Validate the config data of this field
     * @access public
     * @param array|mixed $configData - the config data to validate
     * @param array $errors - the error messages array
     * @return boolean
     * */
    public function validateConfigData($configData, &$errors)
    {
        $result = true;
        if(isset($configData['allowed_dates'])) {
            $result = false;
            if($configData['allowed_dates']=='after' && !TimeUtil::strToDateTime($configData['date'])) {
                $errors[] = 'Please specify the date after';
            }
            elseif($configData['allowed_dates']=='before' && !TimeUtil::strToDateTime($configData['date'])) {
                $errors[] = 'Please specify the date before';
            }
            elseif($configData['allowed_dates']=='between' && !TimeUtil::strToDateTime($configData['date_from'])) {
                $errors[] = 'Please specify the date from';
            }
            elseif($configData['allowed_dates']=='between' && !TimeUtil::strToDateTime($configData['date_to'])) {
                $errors[] = 'Please specify the date to';
            }
            elseif($configData['allowed_dates']=='between' &&
                TimeUtil::strToDateTime($configData['date_from'])->format('c') >= TimeUtil::strToDateTime($configData['date_to'])->format('c')   ) {
                $errors[] = 'The date from must be greater than the date to';
            }
            else {
                $result = true;
            }
        }
        if (isset($configData['allowed_times'])) {
            $result = false;
            if($configData['allowed_times']=='after' && !TimeUtil::strToDateTime($configData['time'])) {
                $errors[] = 'Please specify the time after';
            }
            elseif($configData['allowed_times']=='before' && !TimeUtil::strToDateTime($configData['time'])) {
                $errors[] = 'Please specify the time before';
            }
            elseif($configData['allowed_times']=='between' && !TimeUtil::strToDateTime($configData['time_from'])) {
                $errors[] = 'Please specify the time from';
            }
            elseif($configData['allowed_times']=='between' && !TimeUtil::strToDateTime($configData['time_to'])) {
                $errors[] = 'Please specify the time to';
            }
            elseif($configData['allowed_times']=='between' &&
                TimeUtil::strToDateTime($configData['time_from'])->format('c') >= TimeUtil::strToDateTime($configData['time_to'])->format('c')   ) {
                $errors[] = 'The date from must be greater than the date to';
            }
            else {
                $result = true;
            }
        }
        return $result;
    }

    /**
     * Get the query builder clause for this field type
     * @access public
     * @param string $joinTable
     * @param string $value
     * @param string $labelPrefix
     * @return array
     * */
    public function getQueryBuilderClause($joinTable, $value, $labelPrefix)
    {
        return [
            'field' => 'value_string',
            'field_type' => 'custom_field',
            'data_type' => 'date_time',
            'label' => $labelPrefix . ' : ' . $this->definition->name,
            'clause_type' => 'join',
            'join_table' => $joinTable,
            'join_field_from' => 'object_id',
            'join_field_to' => 'id',
            'join_clause_field_from' => 'field_id',
            'join_clause_operator' => 'equals',
            'join_clause_value' => $value,
        ];
    }

    /**
     * Get a string representation of the display value
     * @param RecordValue $recordValue
     * @return string
     * */
    public function getDisplayValue($recordValue)
    {
        $result = "";
        if($recordValue instanceof RecordValue) {
            $result = DateUtil::asString($recordValue->value_date, $this->getState()->getSettings()->get("system.date_format", 'Y-m-d H:i'));
        }
        elseif(is_array($recordValue) && isset($recordValue['value_date'])) {
            $result = DateUtil::asString($recordValue['value_date'], $this->getState()->getSettings()->get("system.date_format", 'Y-m-d H:i'));
        }
        return $result;
    }

}
