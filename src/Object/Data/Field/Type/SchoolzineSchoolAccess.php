<?php
namespace BlueSky\Framework\Object\Data\Field\Type;

use Lote\System\Schoolzine\Model\Metadata\Region;
use Lote\System\Schoolzine\Model\School;
use BlueSky\Framework\Entity\User\CfValue;

class SchoolzineSchoolAccess extends Base
{

    /**
     * Get the view data of this field
     * @access public
     * @param int $objectId
     * @return mixed
     * */
    public function getViewData($objectId = 0)
    {
        $s = new School($this->getState());
        return $s->getSchoolsByIds($this->getValue());
    }

    public function getEditData($isPreview = false)
    {
        $s = new School($this->getState());
        $allSchools = $s->getSchoolList('');
        if(is_array($this->getValue())) {
            $cnt = count($allSchools);
            for ($i = 0; $i < $cnt; $i++) {
                if (array_search($allSchools[$i]['id'], $this->getValue()) !== false) {
                    $allSchools[$i]['_selected'] = true;
                }
            }
        }
        return $allSchools;
    }

    public function validate()
    {
        return true;
    }

    /**
     * Get the value to be saved
     * @access protected
     * @param CfValue $v
     * @return string
     * */
    protected function setValueData(CfValue $v)
    {
        return $v->value_text = json_encode($this->getValue());
    }

    /**
     * Set the value of this field
     * @access public
     * @param CfValue $value
     * */
    public function setValueFromObject(CfValue $value)
    {
        $this->value = json_decode($value->value_text, true);
    }

}
