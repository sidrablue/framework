<?php
namespace BlueSky\Framework\Object\Data\Field\Type;

use Lote\App\Form\Entity\RecordValue;
use BlueSky\Framework\Entity\User\CfValue;
use BlueSky\Framework\Util\Strings;

class MultiSelect extends BaseOption
{

    /**
     * @var boolean $supportsImporting
     * True if this object can be used in imports
     * */
    protected $supportsImporting = true;

    /**
     * @var boolean $supportsQueryBuilder
     * True if this object can be used in the query builder
     * */
    protected $supportsQueryBuilder = true;

//    /**
//     * Validate the input for this multi select field
//     * @access protected
//     * @return boolean
//     * */
//    protected function validateFieldInputData()
//    {
//        $result = false;
//        if ($this->value) {
//            if(is_string($this->value)) {
//                $data = json_decode($this->value, true);
//            }
//            elseif(is_array($this->value)) {
//                $data = $this->value;
//            }
//            if(isset($data) && is_array($data)) {
//                $result = true;
//                foreach($data as $v) {
//                    if(!$this->isValidOption($v)) {
//                        $result = false;
//                        $this->error = self::FIELD_ERROR_OPTION_DOES_NOT_EXIST;
//                    }
//                }
//            }
//        }
//        return $result;
//    }

    // todo function needs to be deleted as base option fields use getOptions method
    public function getEditData($isPreview = false)
    {
        $this->getValue();
        $result = [];
        if($this->configData && isset($this->configData['options'])) {
//        if($this->configData) {
            if(isset($this->configData['display_alphabetically']) && $this->configData['display_alphabetically'] == '1') {
                natcasesort($this->configData['options']);
            }
//            $options = $this->getOptions();
            foreach($this->configData['options'] as $v) {
                $val = $this->getValue();
                if(is_string($val)) {
                    $val = json_decode($val, true);
                }
                $result[$v] = is_array($val) && array_search($v, $val)!==false;
            }
        }
        return $result;
    }

    /**
     * Get the query builder clause for this field type
     * @access public
     * @param string $joinTable
     * @param string $value
     * @param string $labelPrefix
     * @return array
     * @todo - add in "IN" clause support for this multi select field
     * */
    public function getQueryBuilderClause($joinTable, $value, $labelPrefix)
    {
        return parent::getQueryBuilderClause($joinTable, $value, $labelPrefix);
    }

    /**
     * Get the value to be saved
     * @access protected
     * @param CfValue $v
     * @return string
     * */
//    protected function setValueData(CfValue $v)
//    {
//        if(!Strings::isJson($this->getValue())) {
//            $str = $this->getValue();
//            if(!$str) {
//                $v->value_text = "";
//            }
//            else {
//                $v->value_text = json_encode($this->getValue());
//            }
//        }
//        elseif(is_scalar($this->getValue())) {
//            $v->value_text = $this->getValue();
//        }
//        return $this->getValue();
//    }

    /**
     * Set the value of this field
     * @access public
     * @param CfValue $value
     * */
    public function setValueFromObject(CfValue $value)
    {
        $this->value = json_decode($value->value_text, true);
    }

    public function getPdfExportData($data)
    {
        $config = json_decode($data['config'], true);

        $options = $this->getOptions($data['field_id']);
        $displayAlphabetically = (boolean) $config['display_alphabetically'];
        $allowCustomInput = (boolean) $config['custom_user_options'];

        if ($displayAlphabetically) {
            usort($options, function($a, $b) {
                return strcmp($a['value'], $b['value']);
            });
        }

        $optionsTrimmed = [];
        foreach ($options as $v) {
            $optionsTrimmed[$v['value']] = array('count' => 0, 'weighting' => 0.0);
        }

        if ($allowCustomInput) {
            $answers['other'] = [];
        }

//        $options = array_combine($options, $options);
//        $options = array_map(function($val) { return 0; }, $options);
        $totalAnswers = 0;
        foreach ($data['answers'] as $value) {
//            foreach ((array) $answerArr as $value) {
//                if (!isset($optionsTrimmed[$value]['count'])) {
//                    $optionsTrimmed[$value]['count'] = 0;
//                }
            if (isset($optionsTrimmed[$value])) {
                $optionsTrimmed[$value]['count']++;
            } elseif (isset($answers['other'][$value])) {
                $optionsTrimmed['other'][$value]['count']++;
            } else {
                $optionsTrimmed['other'][$value] = ['count' => 1];
            }
                $totalAnswers++;
//            }
        }
        foreach ($optionsTrimmed as $key => $value) {
            if ($allowCustomInput && $key == 'other') {
                foreach ($value as $otherKey => $otherValue) {
                    $count = $otherValue['count'];
                    $optionsTrimmed[$key][$otherKey]['weighting'] = $totalAnswers > 0 ? ((float)$count / (float)$totalAnswers) : 0.0;
                }
            } else {
                $count = $value['count'];
                $optionsTrimmed[$key]['weighting'] = $totalAnswers > 0 ? ((float)$count / (float)$totalAnswers) : 0.0;
            }
        }

        $results = [];
        $results['results'] = $optionsTrimmed;
        $results['options'] = ['custom_user_options' => $allowCustomInput];

        return $results;
    }

    protected function getSelectionType()
    {
        return 'multi';
    }

}
