<?php
namespace BlueSky\Framework\Object\Data\Field\Type;


use BlueSky\Framework\Util\Strings;
use BlueSky\Framework\Util\Time as TimeUtil;
use BlueSky\Framework\Util\Date as DateUtil;

/**
 *
 * */
class Date extends BaseDate
{

    /**
     * Get the data for the editing of this field
     * @access public
     * @return mixed
     * */
    public function getEditData($isPreview = false)
    {
        if($this->value == "0000-00-00 00:00:00" || is_null($this->value) || $this->value == "") {
            $date = "";
        } else {
            $date = DateUtil::asString($this->value, $this->getState()->getSettings()->get('system.datetime.format', 'Y-m-d'));
        }
        return $date;
    }

    /**
     * Validate the value provided to this custom field type to ensure that it conforms to its structure
     * @access protected
     * @param mixed $value
     * @return boolean
     * */
    public function validateValue($value)
    {
        return ($value instanceof \DateTime) || (is_string($value) && ($value == '' || Strings::isValidDate($value)));
    }

    /**
     * Get the view data of this field
     * @access public
     * @param int $objectId
     * @return mixed
     * */
    public function getViewData($objectId = 0)
    {
        if($this->value == "0000-00-00 00:00:00" || is_null($this->value) || $this->value == "") {
            $date = "";
        } else {
            $date = DateUtil::asString($this->value, $this->getState()->getSettings()->get('system.date.format', 'Y-m-d'));
        }
        return $date;
    }

    public function validate()
    {
        $result = parent::validate();

        $value = $this->getValue();

        if(isset($this->definition->entityData['config_data'])) {
            $config_data = json_decode($this->definition->entityData['config_data'], true);
            if(isset($config_data['allowed_dates'])) {
                if($config_data['allowed_dates'] == 'past') {
                    $current = date("Y-m-d");
                    $value_date = date('Y-m-d', strtotime($value));
                    if($value_date > $current) {
                        $this->error = 100;
                        $result = false;
                    }
                } else if($config_data['allowed_dates'] == 'future') {
                    $current = date("Y-m-d");
                    $value_date = date('Y-m-d', strtotime($value));
                    if($value_date < $current) {
                        $this->error = 100;
                        $result = false;
                    }
                } else if($config_data['allowed_dates'] == 'after') {
                    $after = date("Y-m-d", strtotime($config_data['date']));
                    $value_date = date('Y-m-d', strtotime($value));
                    if($value_date < $after) {
                        $this->error = 100;
                        $result = false;
                    }
                } else if($config_data['allowed_dates'] == 'before') {
                    $before = date("Y-m-d", strtotime($config_data['date']));
                    $value_date = date('Y-m-d', strtotime($value));
                    if($value_date > $before) {
                        $this->error = 100;
                        $result = false;
                    }
                } else if($config_data['allowed_dates'] == 'between') {
                    $from = date("Y-m-d", strtotime($config_data['date_from']));
                    $to = date("Y-m-d", strtotime($config_data['date_to']));
                    $value_date = date('Y-m-d', strtotime($value));
                    if($value_date < $from || $value_date > $to) {
                        $this->error = 100;
                        $result = false;
                    }
                }
            }
        } elseif(!$this->definition->mandatory && ($value == '' || $value == null)) {
            $result = true;
        }
        return $result;
    }

    /**
     * Get the error code of the current field, if one is specified
     * @access public
     * @return int
     * */
    public function getErrorText()
    {
        if($this->error == self::FIELD_ERROR_OUT_OF_RANGE) {
            $text = "Out of range";
        }
        else {
            $text = parent::getErrorText();
        }
        return $text;
    }

    /**
     * Validate the config data of this field
     * @access public
     * @param array|mixed $configData - the config data to validate
     * @param array $errors - the error messages array
     * @return boolean
     * */
    public function validateConfigData($configData, &$errors)
    {
        $result = true;
        if(isset($configData['allowed_dates'])) {
            $result = false;
            if($configData['allowed_dates']=='after' && !TimeUtil::strToDateTime($configData['date'])) {
                $errors[] = 'Please specify the date after';
            }
            elseif($configData['allowed_dates']=='before' && !TimeUtil::strToDateTime($configData['date'])) {
                $errors[] = 'Please specify the date before';
            }
            elseif($configData['allowed_dates']=='between' && !TimeUtil::strToDateTime($configData['date_from'])) {
                $errors[] = 'Please specify the date from';
            }
            elseif($configData['allowed_dates']=='between' && !TimeUtil::strToDateTime($configData['date_to'])) {
                $errors[] = 'Please specify the date to';
            }
            elseif($configData['allowed_dates']=='between' &&
                    TimeUtil::strToDateTime($configData['date_from'])->format('c') >= TimeUtil::strToDateTime($configData['date_to'])->format('c')   ) {
                $errors[] = 'The date from must be greater than the date to';
            }
            else {
                $result = true;
            }
        }
        return $result;
    }

    /**
     * Get the query builder clause for this field type
     * @access public
     * @param string $joinTable
     * @param string $value
     * @param string $labelPrefix
     * @return array
     * */
    public function getQueryBuilderClause($joinTable, $value, $labelPrefix)
    {
        return [
            'field' => 'value_date',
            'field_type' => 'custom_field',
            'data_type' => 'date',
            'label' => $labelPrefix . ' : ' . $this->definition->name,
            'clause_type' => 'join',
            'join_table' => $joinTable,
            'join_field_from' => 'object_id',
            'join_field_to' => 'id',
            'join_clause_field_from' => 'field_id',
            'join_clause_operator' => 'equals',
            'join_clause_value' => $value,
        ];
    }

}
