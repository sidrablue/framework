<?php
namespace BlueSky\Framework\Object\Data\Field\Type;

use BlueSky\Framework\Entity\User\CfValue;
use BlueSky\Framework\Util\Strings;

class RadioButton extends BaseOption
{

    /**
     * @var boolean $supportsImporting
     * True if this object can be used in imports
     * */
    protected $supportsImporting = true;

    /**
     * @var boolean $supportsQueryBuilder
     * True if this object can be used in the query builder
     * */
    protected $supportsQueryBuilder = true;

    // todo function needs to be deleted as base option fields use getOptions method
    public function getEditData($isPreview = false)
    {
        $this->getValue();
        $result = [];
        if($this->configData && isset($this->configData['options'])) {
//        if($this->configData) {
            if(isset($this->configData['display_alphabetically']) && $this->configData['display_alphabetically'] == '1') {
                natcasesort($this->configData['options']);
            }
            if(isset($this->configData['options_adv'])) {
                foreach($this->configData['options_adv'] as $k => $v) {
                    $v['selected'] = ($v['value'] == $this->getValue());
                    $result[$k] = $v;
                }
            }
            else {
//            $options = $this->getOptions();
                foreach($this->configData['options'] as $v) {
                    $result[$v] = ($v == $this->getValue());
                }
            }
        }
        return $result;
    }

    /**
     * Set the value of this field
     * @access public
     * @param CfValue $value
     * */
    public function setValueFromObject(CfValue $value)
    {
        $this->value = json_decode($value->value_text, true);
    }

    public function getPdfExportData($data)
    {
        $config = json_decode($data['config'], true);
        $options = $this->getOptions($data['field_id']);

        $displayAlphabetically = (boolean) $config['display_alphabetically'];
        $allowCustomInput = (boolean) $config['custom_user_options'];

        if ($displayAlphabetically) {
            usort($options, function($a, $b) {
                return strcmp($a['value'], $b['value']);
            });
        }

        $answers = [];
        $totalAnswers = 0;
        foreach ($options as $value) {
            $answers[$value['value']] = array('count' => 0);
        }

        if ($allowCustomInput) {
            $answers['other'] = [];
        }

        foreach ($data['answers'] as $value) {
            if (isset($answers[$value])) {
                $answers[$value]['count']++;
            } elseif (isset($answers['other'][$value])) {
                $answers['other'][$value]['count']++;
            } else {
                $answers['other'][$value] = ['count' => 1];
            }
            $totalAnswers++;
        }

        foreach ($answers as $key => $value) {
            if ($allowCustomInput && $key == 'other') {
                foreach ($value as $otherKey => $otherValue) {
                    $count = $otherValue['count'];
                    $answers[$key][$otherKey]['weighting'] = $totalAnswers > 0 ? ((float)$count / (float)$totalAnswers) : 0.0;
                }
            } else {
                $count = $value['count'];
                $answers[$key]['weighting'] = $totalAnswers > 0 ? ((float)$count / (float)$totalAnswers) : 0.0;
            }
        }

        $results = [];
        $results['results'] = $answers;
        $results['options'] = ['custom_user_options' => $allowCustomInput];

        return $results;
    }

    public function importValidateInput($input, $delimiter, &$errorMessage)
    {
        $valid = true;
        if (count(explode($delimiter, $input)) > 1) {
            $valid = false;
            $errorMessage = "Radio button has more than 1 value assigned";
        }

        if ($valid) {
            $valid = parent::importValidateInput($input, $delimiter, $errorMessage);
        }

        return $valid;
    }

}
