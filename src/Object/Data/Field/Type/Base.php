<?php
namespace BlueSky\Framework\Object\Data\Field\Type;

use Lote\App\Form\Entity\RecordValue;
use BlueSky\Framework\Entity\User\CfField;
use BlueSky\Framework\Entity\CfGroup;
use BlueSky\Framework\Entity\User\CfValue;
use BlueSky\Framework\Entity\User as UserEntity;
use BlueSky\Framework\Entity\Group as GroupEntity;
use BlueSky\Framework\Entity\User\User;
use BlueSky\Framework\Object\Model\Base as BaseModel;
use BlueSky\Framework\Service\Content;
use BlueSky\Framework\Service\InputExploitValidator;
use BlueSky\Framework\State\Web;

/**
 * Class Base
 */
class Base extends BaseModel
{

    const FIELD_ERROR_NOT_SPECIFIED = 1;
    const FIELD_ERROR_NOT_UNIQUE = 7;
    const FIELD_ERROR_CONFIRM_UNMATCHED = 10;
    const FIELD_ERROR_CONFIRM_UNSPECIFIED = 11;

    /**
     * @var string $object - the object reference
     */
    protected $object;

    /**
     * @var int $error - the validation error, if one exists
     */
    protected $error;

    /**
     * @var array $errorMessages - the error messages if they exist for the validation
     */
    protected $errorMessages;

    /**
     * @var array $groupRestrictions - an array of the groups to which this field is restricted to
     * */
    protected $groupRestrictions = [];

    /**
     * @var CfValue $valueObject
     * */
    protected $valueObject;

    /**
     * @var mixed $value
     * */
    protected $value;

    /**
     * @var boolean $supportsImporting
     * True if this object can be used in imports
     * */
    protected $supportsImporting = false;

    /**
     * @var boolean $supportsExporting
     * True if this object can be used in exports
     * */
    protected $supportsExporting = true;

    /**
     * @var boolean $supportsQueryBuilder
     * True if this object can be used in the query builder
     * */
    protected $supportsQueryBuilder = false;

    /**
     * @var mixed $configData
     * True if this object can be used in the query builder
     * */
    protected $configData;

    /**
     * @var array $validationHooks
     * An array of anonymous functions to be used as validation hooks
     * */
    protected $validationHooks = [];

    /**
     * @var CfField $definition
     * The definition of the field
     * */
    public $definition;

    protected $ignoreMandatory = false;

    public $printedToScreen = true;

    /**
     * Default constructor for the Field class.
     * @param Web $state - The state object
     * @param String $object - The object that the fields are for
     * @param CfField $definition - the field definition
     * @param mixed $value - the field value
     * @access public
     * */
    public function __construct($state, $object = '', CfField $definition = null, $value = null)
    {
        parent::__construct($state);
        $this->object = $object;
        $this->value = $value;
        $this->definition = $definition;
    }

    /**
     * Event handler for execution after a save of a field input
     * @access public
     * @param CfValue $cfValue
     * @param bool $setByText
     * @param array $fieldDeletedInfo definition is updateValue
     * @return void
     * */
    protected function afterCfValueSave(CfValue $cfValue, $setByText, $fieldDeletedInfo)
    {
        //do nothing
    }

    /**
     * Set the config data for this field
     * @param string $configData
     * */
    public function setConfigData($configData)
    {
        if (is_string($configData)) {
            $this->configData = json_decode($configData, true);
        } elseif (is_array($configData)) {
            $this->configData = $configData;
        }
    }

    /**
     * Set the config data for this field
     * @param string $configData
     * */
    public function setValidationData($configData)
    {
        if (is_string($configData)) {
            $this->configData = json_decode($configData, true);
        } elseif (is_array($configData)) {
            $this->configData = $configData;
        }
    }

    /**
     * Set the field definition
     * @access public
     * @param CfField $definition
     * @return void
     * */
    public function setDefinition(CfField $definition)
    {
        $this->definition = $definition;
        $this->setConfigData($definition->config_data);
    }

    /**
     * Function validate
     * @access public
     * @param
     * @return boolean $valid - true if this field is valid
     * */
    public function validate()
    {
        $result = true;
        if ($this->definition->mandatory) {
            $result = $this->validateMandatory();
        }
        if ($this->definition->is_unique) {
            if ($this->customValueExists() && $result == true) {
                $result = false;
            }
        }
        if($result) {
            $result = $this->validateFieldInputData();
        }
        if ($result) {
            $result = $this->validateHooks();
        }

//        if ($result) {
//            if ($this->allowExecutableCode()) {
//                $result = $this->validateCodeInput();
//            } elseif ($this->containsCodeInput()) {
//                $result = false;
//            }
//        }

        return $result;
    }

    /**
     * Function used in validate to check if code should be allowed through
     * @access protected
     * @return bool true if code allowed for field
     */
    protected function allowExecutableCode()
    {
        return false;
    }

    /**
     * Function is used to check if code exists in input
     * @return bool true if code exists in input
     */
    protected function containsCodeInput()
    {
        $result = true;
        try {
            if (!empty($this->value)) {
                $result = !InputExploitValidator::containsAnyCode($this->getValue());
            }
        } catch (\Exception $e) {
            // input was probably an array or file, leave it be for now
        }
        return $result;
    }

    /**
     * Function used to return if a valid code block is used. eg only js and not php and sql
     * @return bool true if code in system is valid
     */
    protected function validateCodeInput()
    {
        return true;
    }

    /**
     * Validate the input on a per field level
     * @access protected
     * @return boolean
     * */
    protected function validateFieldInputData()
    {
        return true;
    }

    private function validateHooks()
    {
        $result = true;
        $handlers = $this->getHooksToValidate();
        if ($handlers) {
            foreach ($handlers as $h) {
                $callback = false;
                if(is_array($h) && isset($h[0])) {
                    $callback = $h[0]->callback;
                }
                elseif($h instanceof \Closure) {
                    $callback = $h;
                }
                if($callback) {
                    $errorMessage = "";
                    if (!$callback($this->getValue(), $this, $errorMessage)) {
                        $result = false;
                        $this->errorMessages[] = $errorMessage;
                        break;
                    }
                }
            }
        }
        return $result;
    }

    private function getHooksToValidate()
    {
        $result = [];
        $signalHandlers = $this->getState()->getSignal()->getHandlers('form.field.validate.' . $this->definition->id);
        if(is_array($signalHandlers) && count($signalHandlers) > 0) {
            $result = array_merge($result, $signalHandlers);
        }
        if(is_array($this->validationHooks) && count($this->validationHooks) > 0) {
            $result = array_merge($result, $this->validationHooks);
        }
        return $result;

    }

    /**
     * Add a validation hook to this field
     * @param \Closure $closure
     * */
    public function addValidationHook($closure)
    {
        if (is_callable($closure) && $closure instanceof \Closure) {
            $this->validationHooks[] = $closure;
        }
    }

    /**
     * Validate the data of this field to determine if it meets the mandatory requirements
     * @access protected
     * @return boolean
     * */
    protected function validateMandatory()
    {
        if ($this->ignoreMandatory) {
            return true;
        }

        $result = true;
        if ((!isset($this->value) || empty($this->value)) && !($this->value === false || $this->value === '0')) {
            $result = false;
            $this->error = self::FIELD_ERROR_NOT_SPECIFIED;
        } else {
            if (is_array($this->value) && isset($this->value['original']) && $this->value['original'] == "") {
                $result = false;
                $this->error = self::FIELD_ERROR_NOT_SPECIFIED;
            }
        }
        return $result;
    }

    /**
     * Check if this field already exists
     * @param string $objectRef - the reference of the object to which the field belongs
     * @return boolean
     * @access public
     * */
    public function customValueExists($objectRef = null)
    {
        $result = false;
        if ($this->object) {
            $q = $this->getState()->getReadDb()->createQueryBuilder();
            $q->select('count(*)')->from($this->object . '__cf_value', 'c');
        }
        return $result;
    }

    /**
     * Get the value of this field
     * @access public
     * @return mixed
     * */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set the value of this field
     * @access public
     * @param mixed $value
     * */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * Get the value to be saved
     * @access protected
     * @param CfValue $v
     * @return string
     * */
    protected function setValueData(CfValue $v)
    {
        $v->value_string = $this->getValue();
        return $this->getValue();
    }

    /**
     * Check if the current value is considered to be empty
     * @acess protected
     * @return boolean
     * */
    protected function isEmpty()
    {
        return $this->getValue() == '';
    }

    /**
     * Perform actions after user save
     *
     * @access public
     * @param UserEntity $userEntity - User entity
     * @param string value - Field value
     * @return void
     */
    public function afterUserSave(UserEntity $userEntity, $value) {
        //Do Nothing
    }

    /**
     * Get the value to be saved
     * @access protected
     * @param int $objectId - the ID of the object
     * @param int $fieldId - the ID of the field
     * @param string $value - the value of the
     * @param string $table - the name of the table which stores the data
     * @param boolean $updateOnEmpty - update the value if the new value is empty and the existing one is not empty
     * @param boolean $setByText - For option type fields, use this given value as a text representation rather than an ID
     * @return string
     * */
    public function updateValue($objectId, $fieldId, $value, $table, $updateOnEmpty = true, $setByText = false)
    {
        $result = 0;
        if ($this->validateValue($value)) {
            $v = new CfValue($this->getState());
            $v->setTableName($table);
            $v->loadByObjectAndField($objectId, $fieldId);
            $v->object_id = $objectId;
            $v->field_id = $fieldId;
            $this->setValue($value);
            if (!$v->id || (!$this->isEmpty() || $updateOnEmpty)) {
                $this->setValueData($v);

                $fieldDeletedInfo = ['deleted' => false];
                $result = false;
                if (is_null($this->getValue()) || $this->getValue() === '') { // delete field if empty
                    if ($v->id) { // only delete if item existed beforehand
                        $fieldDeletedInfo['value_id'] = $v->id;
                        $fieldDeletedInfo['table_name'] = $v->getTableName();
                        $fieldDeletedInfo['deleted'] = true;
                        $result = $v->delete();
                    }
                } else {
                    $result = $v->save();
                }

                if ($result) {
                    $this->afterCfValueSave($v, $setByText, $fieldDeletedInfo);
                }
            }
        }
        return $result;
    }

    /**
     * Validate the value provided to this custom field type to ensure that it conforms to its structure
     * @access protected
     * @param mixed $value
     * @return boolean
     * */
    public function validateValue($value)
    {
        return is_string($value) || is_numeric($value) || is_array($value);
    }

    /**
     * Set the value of this field
     * @access public
     * @param CfValue $value
     * */
    public function setValueFromObject(CfValue $value)
    {
        $this->value = $value->value_string;
    }

    /**
     * Set the value of this field
     * @access public
     * @return CfValue
     * */
    public function getValueObject()
    {
        return $this->valueObject;
    }

    /**
     * Set the value of this field
     * @access public
     * @param CfValue $value
     * */
    public function setValueObject(CfValue $value)
    {
        $this->valueObject = $value;
        $this->setValueFromObject($this->valueObject);
    }

    /**
     * Get the name of this field
     * @access public
     * @return string
     * */
    public function getName()
    {
        return $this->definition->name;
    }

    /**
     * Set the default value for this field
     * @access public
     * @param mixed $value - the default value of the field
     * @throws \Exception
     * @return void
     */
    public function setDefault($value)
    {
        throw new \Exception('The set default function must be implemented in the child');
    }

    /**
     * Get the error code of the current field, if one is specified
     * @access public
     * @return int
     * */
    public function getError()
    {
        return $this->error;
    }

    /**
     * Get the error code of the current field, if one is specified
     * @access public
     * @return int
     * */
    public function getErrorText()
    {
        $text = 'Invalid';
        if ($this->error == self::FIELD_ERROR_NOT_SPECIFIED) {
            $text = 'Please enter a value';
        } elseif ($this->errorMessages && isset($this->errorMessages[0])) {
            $text = $this->errorMessages[0];
        }
        return $text;
    }

    /**
     * Event handler for on getting error text which can be overridden by the child
     * @access protected
     * @param string $text - the original error text
     * @return string
     * */
    protected function onGetErrorText($text)
    {
        //implement in child
        return $text;
    }

    /**
     * Get the view data of this field
     * @access public
     * @param int $objectId
     * @return mixed
     * */
    public function getViewData($objectId = 0)
    {
        return $this->value;
    }

    /**
     * Get the data for the editing of this field
     * @access public
     * @param boolean $isPreview - True if field is being previewed
     * @return mixed
     * */
    public function getEditData($isPreview = false)
    {
        $result = $this->value;
        if (!isset($this->value) || empty($this->value)) {
            if (isset($this->definition->default_value) && $this->definition->default_value) {
                $result = $value = $this->definition->default_value;
                if (strstr($value, '%%')) {
                    // check for wildcards
                    $c = new Content($this->getState());
                    $ue = new User($this->getState());
                    $ue->load($this->getState()->getUser()->id);
                    $result = $c->replaceUserWildcards($value, $ue);
                }
            } elseif ($value = $this->getState()->getRequest()->get($this->definition->reference, false)) {
                $result = $value;
            }
        }
        return $result;
    }

    /**
     * Check if the data for this field supports importing
     * @access public
     * @return bool
     * */
    public function supportsImporting()
    {
        return $this->supportsImporting;
    }

    /**
     * Check if the data for this field supports exporting
     * @access public
     * @return bool
     * */
    public function supportsExporting()
    {
        return $this->supportsExporting;
    }

    /**
     * Check if the data for this field supports the dynamic query builder
     * @access public
     * @return bool
     * */
    public function supportsQueryBuilder()
    {
        return $this->supportsQueryBuilder;
    }

    /**
     * Get the export columns for this cfField
     * @param CfField $field
     * @return array
     * */
    public function getExportColumns(CfField $field, $includeGroupName = false)
    {
        $name = $field->name;
        if ($includeGroupName) {
            $ge = new CfGroup($this->getState());
            $ge->load($field->group_id);
            $name = $ge->name . '-' . $name;
        }
        return [$name];
    }

    /**
     * Get the export columns for this cfField
     * @param CfField $field
     * @param array|false $exportData
     * @return array
     * */
    public function getExportCells(CfField $field, $exportData)
    {
        $result = [''];
        if ($exportData) {
            switch ($field->field_type) {
                case "checkbox":
                case "multi_select":
                    $decoded = json_decode($exportData['value_text'], true);
                    if (is_array($decoded)) {
                        $decoded = array_filter($decoded);
                        $result = ['["' . implode('", "', $decoded) . '"]'];
                    } else {
                        $result = [$decoded];
                    }
                    break;
                case "public_group":
                    $groupsString = json_decode($exportData['value_text'], true);
                    $decodedGroups = json_decode($groupsString, true);
                    if (is_array($decodedGroups)) {
                        $selectedGroups = array_filter($decodedGroups);
                        $groupIds = array_keys($selectedGroups);

                        $groups = [];
                        foreach($groupIds as $groupId) {
                            $ge = new GroupEntity($this->getState());
                            if($ge->load($groupId)) {
                                $groups[] = $ge->name;
                            }
                        }

                        $result = ['["' . implode('", "', $groups) . '"]'];
                    } else {
                        $result = [$decodedGroups];
                    }
                    break;
                case "matrix":
                case "textarea":
                    $result = [$exportData['value_text']];
                    break;
                case "signature":
                    $result = [$this->getState()->getUrl()->getBaseUrl()."_img/".$exportData['value_number']];
                    break;
                case "date":
                    $dt = new \DateTime($exportData['value_date']);
                    $result = [$dt->format('d/m/Y')];
                    break;
                case "datetime":
                    $result = [$exportData['value_date']];
                    break;
                case "recaptcha":
                    $result = [''];
                    break;
                case "file":
                    $result = [$this->getState()->getUrl()->getBaseUrl()."_d/" . $exportData['value_string']];
                    break;
                default:
                    $result = [$exportData['value_string']];
                    break;
            }
        }
        return $result;
    }

    /**
     * Validate the config data of this field
     * @access public
     * @param array|mixed $configData - the config data to validate
     * @param array $errors - the error message Array
     * @return boolean
     * */
    public function validateConfigData($configData, &$errors)
    {
        return true;
    }

    /**
     * Set the ignore mandatory
     * @access public
     * @param bool
     * */
    public function setIgnoreMandatory($bool)
    {
        $this->ignoreMandatory = $bool;
    }

    /**
     * Get the query builder clause for this field type
     * @access public
     * @param string $joinTable
     * @param string $value
     * @param string $labelPrefix
     * @throws \Exception - if not implemented in the child field type
     * @return array
     * */
    public function getQueryBuilderClause($joinTable, $value, $labelPrefix)
    {
        throw new \Exception("Implement in child");
    }

    /**
     * Get a string representation of the display value
     * @param RecordValue $recordValue
     * @return string
     * */
    public function getDisplayValue($recordValue)
    {
        $result = "";
        if($recordValue instanceof RecordValue) {
            $result = $recordValue->value_string;
        }
        elseif(is_array($recordValue)) {
            $columnKey = false;
            if (isset($recordValue['value_string'])) {
                $columnKey = 'value_string';
            } elseif (isset($recordValue['value_text'])) {
                $columnKey = 'value_text';
            } elseif (isset($recordValue['value_boolean'])) {
                $columnKey = 'value_boolean';
            } elseif (isset($recordValue['value_number'])) {
                $columnKey = 'value_number';
            } elseif (isset($recordValue['value_date'])) {
                $columnKey = 'value_date';
            }

            if ($columnKey) {
                $result = $recordValue[$columnKey];
            }
        }
        return $result;
    }

    /**
     * Get the column that is used to store the data of this custom field
     * @access public
     * @return string
     * */
    public function getValueColumn()
    {
        return 'value_string';
    }

    public function getPdfExportData($data)
    {
        $answers = [];
        $totalAnswers = 0;
        foreach ($data['answers'] as $answer) {
            if (!isset($answers[$answer])) {
                $answers[$answer] = array('count' => 0);
            }

            $answers[$answer]['count']++;
            $totalAnswers++;
        }

        foreach ($answers as $key => $value) {
            $count = $value['count'];
            $answers[$key]['weighting'] = $totalAnswers > 0 ? ((float)$count / (float)$totalAnswers) : 0.0;
        }

        $results = [];
        $results['results'] = $answers;
        $results['options'] = [];

        return $results;
    }

    public function getFieldConfigOptions($mandatory, $config)
    {
        $options = [];
        if (boolval($mandatory)) {
            $options[] = "Mandatory";
        }
        if ($config != 'null') {
            $configJson = json_decode($config, true);
            if (!empty($configJson)) {
                foreach ($configJson as $key => $value) {
                    if (!empty($value)) {
                        $options[] = ucwords(str_replace('_', ' ', $key));
                    }
                }
            }
        }
        return $options;
    }

    /**
     * Save any field specific data
     * @param int $fieldId
     * @param array $allData
     * @return boolean
     * */
    public function saveFieldTypeData($fieldId, $allData)
    {
        return true;
    }

    public function debug(){

    }

}
