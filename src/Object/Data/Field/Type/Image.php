<?php
namespace BlueSky\Framework\Object\Data\Field\Type;


class Image extends Base
{

    /**
     * @var boolean $supportsImporting
     * True if this object can be used in imports
     * */
    protected $supportsImporting = false;

    /**
     * @var boolean $supportsQueryBuilder
     * True if this object can be used in the query builder
     * */
    protected $supportsQueryBuilder = false;

    public function readOnly()
    {
        $result = 0;
        if (isset($this->configData['read_only'])) {
            $result = $this->configData['read_only'];
        }
        return $result;
    }

}
