<?php
namespace BlueSky\Framework\Object\Data\Field\Type;

use BlueSky\Framework\Entity\User\CfValue;
use BlueSky\Framework\Model\File as FileModel;

/**
 * Class File
 * @package BlueSky\Framework\Object\Data\Field\Type
 */
class File extends Base
{
    const FIELD_ERROR_FILE_INVALID_TYPE = 300;

    /**
     * Set current value from existing value object if current value not already set
     *
     * @access public
     * @param CfValue $value - Existing cf value
     * @return void
     */
    public function setValueFromObject(CfValue $value)
    {
        if(!$this->value) {
            $this->value = $value->value_string;
        }
    }

    /**
     * Validate field
     *
     * @access public
     * @return boolean
     */
    public function validate()
    {
        $result = parent::validate();
        return $result;
    }

    /**
     * Validate the input for the file input
     *
     * @access protected
     * @return boolean
     **/
    protected function validateFieldInputData()
    {
        $result = true;
        if($fileData = $this->getFile()) {
            $fileExtension = $fileData['extension'];
            $allowedExtensions = $this->getAllowedExtensions();
            if($allowedExtensions){
                if(array_search($fileExtension, $allowedExtensions) === false) {
                    $this->error = self::FIELD_ERROR_FILE_INVALID_TYPE;
                    $result = false;
                }
            }
        }

        return $result;
    }

    /**
     * Retrieve list of accepted extensions
     *
     * @access public
     * @return array
     */
    public function getAllowedExtensions()
    {
        $allowedExtensions = [];
        $configData = json_decode($this->definition->config_data, true);
        if(isset($configData['allowed_extensions']) && $configData['allowed_extensions']) {
            $allowedExtensions = explode(',', $configData['allowed_extensions']);
        }
        return $allowedExtensions;
    }

    /**
     * Retrieve file entity associated with this field
     *
     * @access public
     * @return array
     */
    public function getFile()
    {
        $fileId = $this->getValue();
        $m = new FileModel($this->getState());
        return $m->get($fileId);
    }

    /**
     * Get the error code of the current field, if one is specified
     *
     * @access public
     * @return string
     * */
    public function getErrorText()
    {
        if ($this->error == self::FIELD_ERROR_FILE_INVALID_TYPE) {
            $allowedExtensions = $this->getAllowedExtensions();
            $allowedExtensionsString = implode(', ', $allowedExtensions);
            $text = "File type not accepted. Valid extensions: {$allowedExtensionsString}";
        } else {
            $text = parent::getErrorText();
        }
        return $text;
    }
}
