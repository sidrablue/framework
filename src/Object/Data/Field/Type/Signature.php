<?php
namespace BlueSky\Framework\Object\Data\Field\Type;

use BlueSky\Framework\Entity\User\CfValue;
use BlueSky\Framework\Util\Dir;

class Signature extends BaseText
{

    /**
     * @var boolean $supportsImporting
     * True if this object can be used in imports
     * */
    protected $supportsImporting = false;

    /**
     * @var boolean $supportsQueryBuilder
     * True if this object can be used in the query builder
     * */
    protected $supportsQueryBuilder = false;

    /**
     * Validate the value provided to this custom field type to ensure that it conforms to its structure
     * @access protected
     * @param mixed $value
     * @return boolean
     * */
    public function validateValue($value)
    {
        return parent::validateValue($value);
    }




    /**
     * Get the error code of the current field, if one is specified
     * @access public
     * @return int
     * */
    public function getErrorText()
    {

        return "Your signature is required";
    }


    /**
     * Event handler for execution after a save of a field input
     * @access public
     * @param CfValue $cfValue
     * @param bool $setByText
     * @param array $fieldDeletedInfo
     * @return void
     * */
    protected function afterCfValueSave(CfValue $cfValue, $setByText = false, $fieldDeletedInfo)
    {
        $cfValue->value_number = $this->saveValueToFile($cfValue->value_text, $cfValue->id);
        $cfValue->save();
    }

    /**
     * Save the contents of the signature to a file
     * @access private
     * @param $base64FileContent
     * @param $valueId
     * @return int
     */
    private function saveValueToFile($base64FileContent, $valueId) {
        $result = 0;
        $tmpFileName = LOTE_TEMP_PATH.''.uniqid('file_form_signature_');
        Dir::make(LOTE_TEMP_PATH);
        $data = explode( ',', $base64FileContent );
        if(is_array($data) && isset($data[1])) {
            file_put_contents($tmpFileName, base64_decode($data[1]));
            $m = new \BlueSky\Framework\Model\File($this->getState());
            $file = $m->saveUpdate($tmpFileName, "signature.png", 'form/field', $valueId);
            if($file->id) {
                $result = $file->id;
            }
            unlink($tmpFileName);
        }
        return $result;
    }

    public function getViewData($objectId = 0)
    {
        $data = $this->value;
        if(!is_array($this->value)) {
            $data = json_decode($this->value);
        }
        return $data;
    }

    public function getSignatureFile()
    {
        $result = false;
        if($this->getValueObject()->value_number) {
            $f = new \BlueSky\Framework\Entity\File($this->getState());
            if($f->load($this->getValueObject()->value_number)) {
                $result = $f->getData();
            }
        }
        return $result;
    }

}
