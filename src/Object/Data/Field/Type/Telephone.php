<?php
namespace BlueSky\Framework\Object\Data\Field\Type;


use BlueSky\Framework\Util\Number\Mobile;

class Telephone extends Base
{

    /**
     * @var boolean $supportsImporting
     * True if this object can be used in imports
     * */
    protected $supportsImporting = true;

    /**
     * @var boolean $supportsQueryBuilder
     * True if this object can be used in the query builder
     * */
    protected $supportsQueryBuilder = true;

    const FIELD_ERROR_INVALID_FORMAT = 100;

    public function readOnly()
    {
        $result = 0;
        if (isset($this->configData['read_only'])) {
            $result = $this->configData['read_only'];
        }
        return $result;
    }

    public function validate()
    {
        $result = parent::validate();
        $validNumberFormat = strlen($this->getValue()) == 0 || Mobile::standardise($this->getValue(), $this->getState()->getSettings()->getSettingOrConfig("system.locale"));
        if($result && !$validNumberFormat) {
            $result = false;
            $this->error = self::FIELD_ERROR_INVALID_FORMAT;
        }
        return $result;
    }

    /**
     * Get the error text
     * @access public
     * @return string
     * */
    public function getErrorText()
    {
        if($this->error == self::FIELD_ERROR_INVALID_FORMAT) {
            $text = "Invalid phone format";
        }
        else {
            $text = parent::getErrorText();
        }
        return $text;
    }

    /**
     * Get the query builder clause for this field type
     * @access public
     * @param string $joinTable
     * @param string $value
     * @param string $labelPrefix
     * @return array
     * */
    public function getQueryBuilderClause($joinTable, $value, $labelPrefix)
    {
        return [
            'field' => 'value_string',
            'field_type' => 'custom_field',
            'data_type' => 'string',
            'label' => $labelPrefix . ' : ' . $this->definition->name,
            'clause_type' => 'join',
            'join_table' => $joinTable,
            'join_field_from' => 'object_id',
            'join_field_to' => 'id',
            'join_clause_field_from' => 'field_id',
            'join_clause_operator' => 'equals',
            'join_clause_value' => $value,
        ];
    }

}
