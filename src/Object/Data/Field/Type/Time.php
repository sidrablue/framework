<?php
namespace BlueSky\Framework\Object\Data\Field\Type;

use BlueSky\Framework\Util\Time as TimeUtil;

/**
 *
 * */
class Time extends Base
{

    const TIME_INVALID_PAST = 106;
    const TIME_INVALID_FUTURE = 107;
    const TIME_INVALID_BEFORE = 108;
    const TIME_INVALID_AFTER = 109;
    const TIME_INVALID_BETWEEN = 110;

    /**
     * @var boolean $supportsImporting
     * True if this object can be used in imports
     * */
    protected $supportsImporting = true;

    /**
     * @var boolean $supportsQueryBuilder
     * True if this object can be used in the query builder
     * */
    protected $supportsQueryBuilder = true;

    /**
     * Validate the value provided to this custom field type to ensure that it conforms to its structure
     * @access protected
     * @param mixed $value
     * @return boolean
     * */
    public function validateValue($value)
    {
        $value = str_replace(" ", '', $value);
        return (is_string($value) && ($value == '' || preg_match('#^([01]?[0-9]|2[0-3]):[0-5][0-9](:[0-5][0-9])?$#', $value)));
    }

    public function validate()
    {
        $result = parent::validate();
        if($result && !TimeUtil::strToDateTime($this->getValue())) {
            $result = false;
            $this->error = 'Invalid time date';
        }

        $value = $this->getValue();

        if(isset($this->definition->entityData['config_data']) && $result != false) {
            $config_data = json_decode($this->definition->entityData['config_data'], true);
            if(isset($config_data['allowed_times'])) {
                if($config_data['allowed_times'] == 'past') {
                    $current = date("H:i:s");
                    $value_time = date('H:i:s', strtotime($value));
                    if($value_time > $current) {
                        $this->error = self::TIME_INVALID_PAST;
                        $result = false;
                    }
                } else if($config_data['allowed_times'] == 'future') {
                    $current = date("H:i:s");
                    $value_time = date('H:i:s', strtotime($value));
                    if($value_time < $current) {
                        $this->error = self::TIME_INVALID_FUTURE;
                        $result = false;
                    }
                } else if($config_data['allowed_times'] == 'after') {
                    $after = date("H:i:s", strtotime($config_data['time']));
                    $value_time = date('H:i:s', strtotime($value));
                    if($value_time < $after) {
                        $this->error = self::TIME_INVALID_AFTER;
                        $result = false;
                    }
                } else if($config_data['allowed_times'] == 'before') {
                    $before = date("H:i:s", strtotime($config_data['time']));
                    $value_time = date('H:i:s', strtotime($value));
                    if($value_time > $before) {
                        $this->error = self::TIME_INVALID_BEFORE;
                        $result = false;
                    }
                } else if($config_data['allowed_times'] == 'between') {
                    $from = date("H:i:s", strtotime($config_data['time_from']));
                    $to = date("H:i:s", strtotime($config_data['time_to']));
                    $value_time = date('H:i:s', strtotime($value));
                    if($value_time < $from || $value_time > $to) {
                        $this->error = self::TIME_INVALID_BETWEEN;
                        $result = false;
                    }
                }
            }
        }
        elseif(!$this->definition->mandatory && ($value == '' || $value == null)) {
            $result = true;
        }

        return $result;
    }


    /**
     * Get the error code of the current field, if one is specified
     * @access public
     * @return int
     * */
    public function getErrorText()
    {
        switch ($this->error) {
            case self::TIME_INVALID_PAST:
                $text = "Time must be in the past";
                break;
            case self::TIME_INVALID_FUTURE:
                $text = "Time must be in the future";
                break;
            case self::TIME_INVALID_BEFORE:
                $text = "Time must be before " . $this->getTimeRange();
                break;
            case self::TIME_INVALID_AFTER:
                $text = "Time must be after " . $this->getTimeRange();
                break;
            case self::TIME_INVALID_BETWEEN:
                $text = "Time must be between " . $this->getTimeRange();
                break;
            default:
                $text = parent::getErrorText();
        }
//        if($this->error == self::FIELD_ERROR_OUT_OF_RANGE) {
//            $text = "Out of range";
//        }
//        else {
//            $text = parent::getErrorText();
//        }
        return $text;
    }


    /**
     * returns the time range of the variable if it exists
     * @return string|false
     */
    private function getTimeRange()
    {
        $text = false;
        if(isset($this->definition->entityData['config_data'])) {
            $configData = json_decode($this->definition->entityData['config_data'], true);
            if (isset($configData['allowed_times'])) {
                if ($configData['allowed_times'] == 'after' || $configData['allowed_times'] == 'before') {
                    $text = date("h:i A", strtotime($configData['time']));
                } elseif ($configData['allowed_times'] == 'between') {
                    $text = date("h:i A", strtotime($configData['time_from'])) . ' and ' . date("h:i A", strtotime($configData['time_to']));
                }
            }
        }
        return $text;
    }


    /**
     * Validate the config data of this field
     * @access public
     * @param array|mixed $configData - the config data to validate
     * @param array $errors - the error messages array
     * @return boolean
     * */
    public function validateConfigData($configData, &$errors)
    {
        $result = true;

        if (isset($configData['allowed_times'])) {
            $result = false;
            if($configData['allowed_times']=='after' && !TimeUtil::strToDateTime($configData['time'])) {
                $errors[] = 'Please specify the time after';
            }
            elseif($configData['allowed_times']=='before' && !TimeUtil::strToDateTime($configData['time'])) {
                $errors[] = 'Please specify the time before';
            }
            elseif($configData['allowed_times']=='between' && !TimeUtil::strToDateTime($configData['time_from'])) {
                $errors[] = 'Please specify the time from';
            }
            elseif($configData['allowed_times']=='between' && !TimeUtil::strToDateTime($configData['time_to'])) {
                $errors[] = 'Please specify the time to';
            }
            elseif($configData['allowed_times']=='between' &&
                TimeUtil::strToDateTime($configData['time_from'])->format('c') >= TimeUtil::strToDateTime($configData['time_to'])->format('c')   ) {
                $errors[] = 'The date from must be greater than the date to';
            }
            else {
                $result = true;
            }
        }

        return $result;
    }

    /**
     * Get the query builder clause for this field type
     * @access public
     * @param string $joinTable
     * @param string $value
     * @param string $labelPrefix
     * @return array
     * */
    public function getQueryBuilderClause($joinTable, $value, $labelPrefix)
    {
        return [
            'field' => 'value_string',
            'field_type' => 'custom_field',
            'data_type' => 'string',
            'label' => $labelPrefix . ' : ' . $this->definition->name,
            'clause_type' => 'join',
            'join_table' => $joinTable,
            'join_field_from' => 'object_id',
            'join_field_to' => 'id',
            'join_clause_field_from' => 'field_id',
            'join_clause_operator' => 'equals',
            'join_clause_value' => $value,
        ];
    }

}
