<?php
namespace BlueSky\Framework\Object\Data\Field\Type;

class Recaptcha extends Base {

    public $printedToScreen = false;

    /**
     * @var boolean $supportsExporting
     * True if this object can be used in exports
     * */
    protected $supportsExporting = false;

    protected $supportsImporting = false;

    protected $supportsQueryBuilder = false;

    protected $errorMessages = ['Please complete captcha'];

    public function validate()
    {
        $result = parent::validate();
        $captcha = $this->getValue();
        if($captcha) {
            $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$this->secretKey()."&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']);
            if(is_string($response)) {
                $response = json_decode($response);
                if($response->success == false) {
                    $this->error = 'Invalid captcha';
                }
            }
        }
        else {
            $result = false;
            $this->error = 'Please validate that you are not a robot';
        }
        return $result;
    }

    public function siteKey()
    {
        $result = 0;
        if(isset($this->configData['site_key']) && $this->configData['site_key']) {
            $result = $this->configData['site_key'];
        }
        elseif($key = $this->getState()->getSettings()->get('system.recaptcha.site_key')){
            $result = $key;
        }
        return $result;
    }

    public function secretKey()
    {
        $result = 0;
        if(isset($this->configData['secret_key']) && $this->configData['secret_key']) {
            $result = $this->configData['secret_key'];
        }
        elseif($key = $this->getState()->getSettings()->get('system.recaptcha.secret_key')){
            $result = $key;
        }
        return $result;
    }

}
