<?php
namespace BlueSky\Framework\Object\Data\Field\Type;

use Lote\App\Form\Entity\RecordValue;
use BlueSky\Framework\Entity\User\CfField;
use BlueSky\Framework\Entity\User\CfFieldOption;
use BlueSky\Framework\Entity\User\CfValue;
use BlueSky\Framework\Entity\User\CfValueOptionSelected;
use BlueSky\Framework\Entity\User as UserEntity;
use BlueSky\Framework\Model\CfFieldOption as CfFieldOptionModel;
use BlueSky\Framework\Object\Data\Field\Factory;
use BlueSky\Framework\Service\InputExploitValidator;
use BlueSky\Framework\Util\Arrays;
use BlueSky\Framework\Util\Strings;

class BaseOption extends Base
{

    const FIELD_ERROR_OPTION_DOES_NOT_EXIST = 150;

    const SORT_OPTIONS_DEFAULT = 0;
    const SORT_OPTIONS_ALPHA_ASC = 1;
    const SORT_OPTIONS_ALPHA_DESC = 2;
    const SORT_OPTIONS_WEIGHTING_ASC = 3;
    const SORT_OPTIONS_WEIGHTING_DESC = 4;

    /**
     * @var bool $isMatrix - true if this field is a matrix and we expect 2D values
     * */
    protected $isMatrix = false;


    /**
     * Validate the input on a per field level
     * @access protected
     * @return boolean
     * */
    protected function validateFieldInputData()
    {
        $result = true;
        if ($this->value && !$this->isValidOption($this->value)) {
            $result = false;
            $this->error = self::FIELD_ERROR_OPTION_DOES_NOT_EXIST;
        }
        return $result;
    }

    /**
     * Get a string representation of the display value
     * @param RecordValue $recordValue
     * @return string
     * */
    public function getDisplayValue($recordValue)
    {
        $result = "";
        $cfOptionModel = new \BlueSky\Framework\Model\CfValueOptionSelected($this->getState());
        if($recordValue instanceof RecordValue) {
            $cfOptionModel->setTableName('sb_form_record__cf_value_option_selected');
            $data = $cfOptionModel->getValueOptions($recordValue->id);
        }
        elseif (is_array($recordValue)) {
            $id = '';
            if (isset($recordValue['value_id'])) {
                $id = $recordValue['value_id'];
            } elseif ($recordValue['id']) {
                $id = $recordValue['id'];
            }
            $cfOptionModel->setTableName('sb__user__cf_value_option_selected');
            $data = $cfOptionModel->getValueOptions($id);
        }
        if(isset($data) && is_array($data)) {
            $dc = count($data);
            $i = 0;
            foreach ($data as $val) {
                $result .= $val['value_text'];
                if (++$i < $dc) {
                    $result .= ', ';
                }
            }
        }
        return $result;
    }

    /**
     * Get the view data of this field
     * @access public
     * @param int $objectId
     * @return mixed
     * */
    public function getViewData($objectId = 0)
    {
        $result = [];
        if ($this->valueObject->getTableName() != 'sb__cf') {
            $cfOptionModel = new \BlueSky\Framework\Model\CfValueOptionSelected($this->getState());
            $cfOptionModel->setTableName($this->valueObject->getTableName() . '_option_selected');
            $result = $cfOptionModel->getValueOptions($this->getValueObject()->id);
        }
        return $result;
    }

    /**
     * Check if an option specified is valid
     * @access protected
     * @param int $values - the ID(s) and values of the option which is usually in json encoding
     * @return boolean
     * */
    protected function isValidOption($values)
    {
        $result = false;
        if ($values) {
            if (Strings::isJson($values)) {
                $values = json_decode($values, true);
            }
            /// option is automatically valid if its 'other (0)'
            foreach ($values as $value) {
                if (Strings::isJson($value)) {
                    $value = json_decode($value, true);
                }

                $isCode = false;
                if(is_string($value['value']) && !is_null($value['value'])) {
                    $isCode = InputExploitValidator::containsAnyCode($value['value']);
                }
                $result = !$isCode;

                if (!$isCode) { // value should not contain any code
                    $options = $this->getOptions($this->definition->id);
                    if ($value['id'] != '0') {
                        $optionIds = Arrays::getFieldValuesFrom2dArray($options, 'id');
                        if ($result = in_array($value['id'], $optionIds)) {
                            foreach ($options as $option) {
                                if ($option['id'] == $value['id']) {
                                    if (strtolower($option['value']) != strtolower($value['value'])) {
                                        $result = false;
                                    }
                                    break; // if id is found then the value will determine the validity of the input either way
                                }
                            }
                        } else {
                            break; // if the id is not in the existing option ids and not a other option then its a flag for injection
                        }
                    } else { // we assume that the other option is automatically valid
                        $optionValues = Arrays::getFieldValuesFrom2dArray($options, 'value');
                        array_map('strtolower', $optionValues);
                        if (in_array(strtolower($value['value']), $optionValues)) {
                            $result = false;
                            break; // if the other value is the same as the existing options values then fail
                        }
                    }
                } else {
                    break; // if input value is detected to be an injection attempt then exit straight away
                }
            }
        }
        return $result;
    }

    /**
     * Get the error code of the current field, if one is specified
     * @access public
     * @return int
     * */
    public function getErrorText()
    {
        if ($this->error = self::FIELD_ERROR_OPTION_DOES_NOT_EXIST) {
            $text = "Invalid option selected";
        } else {
            $text = parent::getErrorText();
        }
        return $text;
    }

    /**
     * Validate the option data of this field
     * @access public
     * @param array|mixed $optionData - the config data to validate
     * @param array $errors - the error messages array
     * @return boolean
     * */
    public function validateOptionData($optionData, &$errors)
    {
        $options = $this->generateReferences($optionData);
        return $this->hasOptions($options, $errors) && $this->hasValidSelectionElements($options,
            $errors) && $this->hasUniqueOptionsOnly($options, $errors);
    }

    /**
     * Generate references for a list of options that are being built for this field
     * @access public
     * @param array $options
     * @return array
     * */
    private function generateReferences($options)
    {
        $result = [];
        if (is_array($options) && count($options) > 0) {
            foreach ($options as $o) {
                if (!isset($o['id']) || !$o['id']) {
                    $o['reference'] = Strings::generateHtmlId($o['value']);
                } else {
                    $e = new CfFieldOption($this->getState());
                    $e->load($o['id']);
                    $o['reference'] = $e->reference;
                }
                $result[] = $o;
            }
        }
        return $result;
    }

    /**
     * Validate the option data of this field
     * @access public
     * @param array|mixed $optionData - the config data to validate
     * @param array $errors - the error messages array
     * @return boolean
     * */
    private function hasUniqueOptionsOnly($optionData, &$errors)
    {
        $result = true;
        $optionScope = [];
        foreach ($optionData as $o) {
            if ($o['is_selectable']) {
                if (!isset($optionScope[$o['direction']]) || !isset($optionScope[$o['direction']][$o['parent_counter']]) || !isset($optionScope[$o['direction']][$o['parent_counter']][$o['reference']])) {
                    $optionScope[$o['direction']][$o['parent_counter']][$o['reference']] = true;
                } else {
                    $result = false;
                    $errors[] = 'Duplicate options detected';
                    break;
                }
            }
        }
        return $result;
    }

    /**
     * Validate the option data of this field
     * @access public
     * @param array|mixed $optionData - the config data to validate
     * @param array $errors - the error messages array
     * @return boolean
     * */
    private function hasValidSelectionElements($optionData, &$errors)
    {
        $result = true;
        foreach ($optionData as $o) {
            if (trim($o['value']) == '' && $o['is_selectable']) {
                $result = false;
                $errors[] = 'Please remove all empty selectable options';
                break;
            }
        }
        return $result;
    }

    /**
     * Validate the option data of this field
     * @access public
     * @param array|mixed $optionData - the config data to validate
     * @param array $errors - the error messages array
     * @return boolean
     * */
    private function hasOptions($optionData, &$errors)
    {
        if (!$result = count($optionData) > 0) {
            $errors[] = 'Please specify some options';
        }
        return $result;
    }

    public function allowCustomUserOptions()
    {
        $result = 0;
        if($this->configData && isset($this->configData['custom_user_options'])) {
            $result = $this->configData['custom_user_options'];
        }
        return $result;
    }

    /**
     * Save any field specific data
     * @param int $fieldId
     * @param array $allData
     * @return boolean
     * */
    public function saveFieldTypeData($fieldId, $allData)
    {
        if (isset($allData['option_list'])) {
            $currentOptions = $this->getOptions($fieldId);
            $options = $allData['option_list'];
            $options = $this->generateReferences($options);
            $existingIds = Arrays::getFieldValuesFrom2dArray($currentOptions, "id");
            $submittedIds = Arrays::getFieldValuesFrom2dArray($options, "id");
            $submittedIds = array_filter($submittedIds, function ($value) {
                return $value !== '';
            });
            $deletedIds = array_diff($existingIds, $submittedIds);
            if (is_array($deletedIds) && count($deletedIds) > 0) {
                foreach ($deletedIds as $dId) {
                    $e = new CfFieldOption($this->getState());
                    $e->load($dId);
                    $e->delete();
                }
            }

            foreach ($options as $opt) {
                $e = new CfFieldOption($this->getState());
                if (isset($opt['id'])) {
                    $e->load($opt['id']);
                }
                $e->field_id = $fieldId;
                $e->parent_id = 0;
                $e->value = trim($opt['value']);
                $e->reference = $opt['reference'];
                $e->sort_order = $opt['sort_order'];
                $e->is_selectable = $opt['is_selectable'];
                $e->is_active = $opt['is_active'];
                $e->is_hidden = $opt['is_hidden'];
                $e->selection_limit = $opt['selection_limit'];
                $e->direction = $opt['direction'];
                $e->weighting = isset($opt['weighting']) ? $opt['weighting'] : 0; // TODO the weighting isset check can be removed once all projects send through the new weighting option
                $e->save();
            }
        } elseif ( // called when a custom field is tried to be added
            isset($allData['storage_type']) &&
            $allData['storage_type'] == 'custom_user_field' &&
            !isset($allData['option_list']) &&
            is_subclass_of(Factory::createInstance($this->getState(), $allData['field_type']), 'BlueSky\Framework\Object\Data\Field\Type\BaseOption') &&
            isset($allData['custom_user_field_id'])
        ) {
            $currentOptions = $this->getOptions($allData['custom_user_field_id']);
            foreach ($currentOptions as $opt) {
                unset($opt['id']);
                $opt['field_id'] = $fieldId;
                $e = new CfFieldOption($this->getState());
                $e->setData($opt);
                $e->save();
            }
        }
        return true;
    }

    /**
     * Get the options available for this field
     * @access public
     * @param int|boolean $fieldId - a field ID if one is not already specified as part of the field definition
     * @return array
     * */
    public function getOptions($fieldId = false)
    {
        $m = new CfFieldOptionModel($this->getState());
        $fieldId ? $id = $fieldId : $id = $this->definition->id;
        $sortOrder = isset($this->configData['sort_options_order']) ? $this->configData['sort_options_order'] : self::SORT_OPTIONS_DEFAULT;

        // TODO this if block needs to be deleted after all display_alphabetically references are removed from all projects
        if(isset($this->configData['display_alphabetically']) && $this->configData['display_alphabetically'] == '1') {
            $sortOrder = self::SORT_OPTIONS_ALPHA_ASC;
        }

        $options = $m->getFieldOptions($id, $sortOrder);

//        if(isset($this->configData['display_alphabetically']) && $this->configData['display_alphabetically'] == '1') {
//            usort($options, function($i1, $i2) {
//                return strcmp(strtolower($i1['value']), strtolower($i2['value']));
//            });
//        }

//        if(isset($this->configData['sort_options_order'])) {
//            $this->sortOptions($this->configData['sort_options_order'], $options);
//        }

        if (isset($this->configData['custom_user_options']) && $this->configData['custom_user_options'] == '1') {
            $options['Other'] = array('id' => '0', 'value' => "Other", 'custom_value' => '');
        }

        if(isset($this->valueObject) && $this->valueObject->id > 0) {
            $viewData = $this->getViewData();
            $options = $this->populateOptionsWithData($options, $viewData);
            if (is_array($viewData)) {
                $selectedOptions = Arrays::getFieldValuesFrom2dArray($viewData, 'option_id');
                foreach ($options as $k => $v) {
                        if (in_array($v['id'], $selectedOptions)) {
                            $options[$k]['_selected'] = true;
                        }
                }
            }
        }
        return $options;
    }

    /**
     * @param array CfFieldOption $options
     * @param array $viewData
     */
    private function populateOptionsWithData($options, $viewData) {
        foreach ($viewData as $data) {
            foreach ($options as $key => $option) {
                if ($data['option_id'] == $option['id']) {
                    $options[$key]['_custom_val'] = $data['value_text'];
                    break;
                }
            }
        }
        return $options;
    }

    public function debug()
    {
        return $this->getViewData();
    }

    /**
     * Get the value to be saved
     * @access protected
     * @param CfValue $v
     * @return string
     * */
    protected function setValueData(CfValue $v)
    {
        return $v->value_text = Strings::isJson($this->getValue()) ? $this->getValue() : json_encode($this->getValue());
    }

    /**
     * Perform actions after user save
     *
     * @access public
     * @param UserEntity $userEntity - User entity
     * @param string value - Field value
     * @return void
     */
    public function afterUserSave(UserEntity $userEntity, $value) {
        //Do Nothing
    }


    /**
     * Event handler for execution after a save of a field input
     * @access public
     * @param CfValue $cfValue
     * @param bool $setByText
     * @param array $fieldDeletedInfo
     * @return void
     * */
    protected function afterCfValueSave(CfValue $cfValue, $setByText = true, $fieldDeletedInfo)
    {
        // todo 28/07/2017 is the if block below redundant?
        if (is_scalar($this->getValue())) {
            $om = new CfFieldOption($this->getState());
            if ($om->load($this->getValue())) {
                $cfValue->value_string = $om->value;
                $cfValue->save();
            }
        }
        $cfOptionModel = new \BlueSky\Framework\Model\CfValueOptionSelected($this->getState());

        if (!$fieldDeletedInfo['deleted']) {
            $values = json_decode($cfValue->value_text, true);

            $cfOptionModel->setTableName($cfValue->getTableName() . '_option_selected');
            /*if (is_numeric($values)) {
                $cfOptionModel->syncOptions($cfValue, [$values], $this->isMatrix,
                    is_subclass_of($this, 'BlueSky\Framework\Object\Data\Field\Type\SingleSelect'));
            } else*/
            if (is_array($values)) {
                $cfOptionModel->syncOptions($cfValue, $values, $this->isMatrix,
                    is_subclass_of($this, 'BlueSky\Framework\Object\Data\Field\Type\SingleSelect'));
            } elseif (!($this instanceof Matrix) && is_string($values)) { // attempt to auto fix the input
                $this->getState()->getLoggers()->getAccountLogger('cffields')->error('option should not still be a string, attempting to fix data');
                $this->getState()->getLoggers()->getAccountLogger('cffields')->info('original value: ' . $cfValue->value_text);
                $this->getState()->getLoggers()->getAccountLogger('cffields')->info('original json decode: ' . $values);

                $count = 1;
                $values = json_decode($values, true);
                while ($values !== null && Strings::isJson($values)) {
                    $this->getState()->getLoggers()->getAccountLogger('cffields')->info('fix attempt: ' . $count);
                    $this->getState()->getLoggers()->getAccountLogger('cffields')->info('value before decode ' . $count . ': ' . $values);
                    $values = json_decode($values, true);
                    if (!is_null($values) && is_string($values)) {
                        $this->getState()->getLoggers()->getAccountLogger('cffields')->info('value after decode ' . $count . ': ' . $count);
                    }
                    $count++;
                }

                $invalidInput = false;
                if ($values === null) {
                    $this->getState()->getLoggers()->getAccountLogger('cffields')->error('value is invalid');
                    $invalidInput = true;
                } elseif (is_array($values)) {
                    if (isset($values['id']) && isset($values['value'])) {
                        $this->getState()->getLoggers()->getAccountLogger('cffields')->info('value was json encoded multiple times but its valid. Continuing with syncing');
                        $cfOptionModel->syncOptions($cfValue, $values, false, false);
                    } else {
                        $this->getState()->getLoggers()->getAccountLogger('cffields')->error('value was json encoded multiple times but its not valid. Stopping sync');
                        $invalidInput = true;
                    }
                }

                if ($invalidInput) {
                    $this->getState()->getLoggers()->getAccountLogger('cffields')->error('Value was invalid, therefore, deleting row from __cf_value');
                    $cfValue->delete();
                }


//            $cfOptionModel->syncOptions($cfValue, $values, $this->isMatrix, true);
            }
        } else/* ($values === null)*/ { // the value has been deselected
            $cfOptionModel->valueDeselected($fieldDeletedInfo['value_id'], $fieldDeletedInfo['table_name']);
//            $cfValue->delete();
        }
    }


    /**
     * Get the export columns for this cfField
     * @param CfField $field
     * @param array|false $exportData
     * @return array
     * */
    public function getExportCells(CfField $field, $exportData)
    {
        $result = [];
        $viewData = $this->getViewData();
        foreach ($viewData as $v) {
            $result[] = $v['value_string'];
        }
        return [implode(",", $result)];
    }


    /**
     * Get the query builder clause for this field type
     * @access public
     * @param string $joinTable
     * @param string $value
     * @param string $labelPrefix
     * @return array
     * */
    public function getQueryBuilderClause($joinTable, $value, $labelPrefix)
    {
        return [
            '_field_class' => 'option',
            'field_type' => 'custom_field',
            'data_type' => 'string',
            'label' => $labelPrefix . ' : ' . $this->definition->name,
            'clause_type' => 'join',
            'join' => [
                'custom_alias' => 'sbucf',
                'table' => $joinTable,
                'field_from' => 'object_id',
                'field_to' => 'id',
                'clause' => [
                    [
                        'field_from' => 'field_id',
                        'operator' => 'equals',
                        'value' => $value
                    ],
                ],
                'joins' => [
                    [
                        'field' => 'value_string',
                        "operator" => 'equals',
                        'value' => '',
                        'from_alias' => 'sbucf',
                        'custom_alias' => 'sbucfos',
                        'table_from' => $joinTable,
                        'table' => $joinTable . '_option_selected',
                        'field_from' => 'value_id',
                        'field_to' => 'id'
                    ]
                ]
            ]
            /* 'field' => 'value_string',
             'field_type' => 'custom_field',
             'field_class' => 'option',
             'data_type' => 'string',
             'label' => $labelPrefix . ' : ' . $this->definition->name,
             'clause_type' => 'join',
             'join_table' => 'sb__user__cf_value',
             'join_field_from' => 'object_id',
             'join_field_to' => 'id',
             'join_clause_field_from' => 'field_id',
             'join_clause_operator' => 'equals',
             'join_clause_value' => $value,*/
        ];
    }

    /**
     * @param number $fieldId the id of the field from sb__cf_field
     * @param string $separator the separator used in the data
     * @param string $fieldData the raw data to be processed for saving
     * @return string contains the raw field for __cf_value
     */
    public function importPreProcessFieldRawData($fieldId, $separator, $fieldData)
    {
        /// from optionA; optionB;optionC to array(optionA,optionB,optionC)
        $valueData = array_map('trim', explode($separator, $fieldData));

        $valuesRaw = [];

        foreach ($valueData as $key => $value) {
            $valueRaw = ['id' => '0', 'value' => $value];

            $foe = new CfFieldOption($this->getState());
            $fe = new CfField($this->getState());
            $fe->load($fieldId); /// We assume that the field will be loaded

            if ($foe->loadByFields(['field_id' => $fieldId, 'value' => $value])) { // field option exists
                $valueRaw['id'] = $foe->id;
            } elseif(isset($fe->config_data['custom_user_options']) && !((boolean) $fe->config_data['custom_user_options'])) { // error since field does not exist and config data for the field shows that custom options cannot be selected
                // todo throw exception here
            }
            $valuesRaw[] = $valueRaw;
        }

        return json_encode($valuesRaw);
    }

    public function importValidateInput($input, $delimiter, &$errorMessage)
    {
        $result = true;
        $inputValues = array_map('trim', explode($delimiter, $input));
        if (is_array($inputValues) && count($inputValues) > 0) {
            $options = $this->definition->getOptions();
            $optionValues = Arrays::getFieldValuesFrom2dArray($options, 'value');
            foreach ($inputValues as $input) {
                if (!in_array($input, $optionValues) && // value not in array
                    !isset($this->definition->config_data['custom_user_options']) && // custom options key not set so default to false
                    !(isset($this->definition->config_data['custom_user_options']) && ((boolean)$this->definition->config_data['custom_user_options']))) { // custom options key is set but its false
                    $result = false;
                    $errorMessage = 'Item not in options list and field does not allow custom values';
                    break; // Item is not in the options list and field does not allow custom values either
                }
            }
        }

        return $result;
    }

    /**
     * Get the column that is used to store the data of this custom field
     * @access public
     * @return string
     * */
    public function getValueColumn()
    {
        return 'value_text';
    }

}
