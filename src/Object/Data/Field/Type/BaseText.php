<?php
namespace BlueSky\Framework\Object\Data\Field\Type;

use BlueSky\Framework\Entity\User\CfValue;

class BaseText extends Base
{

    /**
     * @var boolean $supportsImporting
     * True if this object can be used in imports
     * */
    protected $supportsImporting = true;

    /**
     * @var boolean $supportsQueryBuilder
     * True if this object can be used in the query builder
     * */
    protected $supportsQueryBuilder = true;

    /**
     * Check if the current field is defined as read only
     * @access public
     * @return bool
     * */
    public function readOnly()
    {
        $result = 0;
        if (isset($this->configData['read_only'])) {
            $result = $this->configData['read_only'];
        }
        return $result;
    }

    /**
     * Get the value to be saved
     * @access protected
     * @param CfValue $v
     * @return string
     * */
    protected function setValueData(CfValue $v)
    {
        $v->value_text = $this->getValue();
        return $this->getValue();
    }

    /**
     * Set the value of this field
     * @access public
     * @param CfValue $value
     * */
    public function setValueFromObject(CfValue $value)
    {
        $this->value = $value->value_text;
    }

    /**
     * Get the query builder clause for this field type
     * @access public
     * @param string $joinTable
     * @param string $value
     * @param string $labelPrefix
     * @return array
     * */
    public function getQueryBuilderClause($joinTable, $value, $labelPrefix)
    {
        return [
            'field' => 'value_text',
            'field_type' => 'custom_field',
            'data_type' => 'string',
            'label' => $labelPrefix . ' : ' . $this->definition->name,
            'clause_type' => 'join',
            'join_table' => $joinTable,
            'join_field_from' => 'object_id',
            'join_field_to' => 'id',
            'join_clause_field_from' => 'field_id',
            'join_clause_operator' => 'equals',
            'join_clause_value' => $value,
        ];
    }

    /**
     * Get the column that is used to store the data of this custom field
     * @access public
     * @return string
     * */
    public function getValueColumn()
    {
        return 'value_text';
    }

}
