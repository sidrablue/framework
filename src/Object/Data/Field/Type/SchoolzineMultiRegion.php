<?php
namespace BlueSky\Framework\Object\Data\Field\Type;

use Lote\System\Schoolzine\Model\Metadata\Region;
use BlueSky\Framework\Entity\User\CfValue;

class SchoolzineMultiRegion extends Base
{

    /**
     * Get the view data of this field
     * @access public
     * @param int $objectId
     * @return mixed
     * */
    public function getViewData($objectId = 0)
    {
        $m = new Region($this->getState());
        return $m->getRegionsByIds($this->getValue(), 'advertising');
    }

    public function getEditData($isPreview = false)
    {
        $m = new Region($this->getState());
        $allRegions = $m->getRegionItems('advertising');
        if(is_array($this->getValue())) {
            $cnt = count($allRegions);
            for ($i = 0; $i < $cnt; $i++) {
                if (array_search($allRegions[$i]['id'], $this->getValue()) !== false) {
                    $allRegions[$i]['_selected'] = true;
                }
            }
        }
        return $allRegions;
    }

    public function validate()
    {
        return true;
    }

    /**
     * Get the value to be saved
     * @access protected
     * @param CfValue $v
     * @return string
     * */
    protected function setValueData(CfValue $v)
    {
        return $v->value_text = json_encode($this->getValue());
    }

    /**
     * Set the value of this field
     * @access public
     * @param CfValue $value
     * */
    public function setValueFromObject(CfValue $value)
    {
        $this->value = json_decode($value->value_text, true);
    }

}
