<?php
namespace BlueSky\Framework\Object\Data\Field\Type;

use Lote\App\Form\Entity\RecordValue;
use BlueSky\Framework\Entity\Cf\CfFieldOption;
use BlueSky\Framework\Entity\User\CfValue;
use BlueSky\Framework\Util\Strings;
use BlueSky\Framework\Util\Arrays;
use BlueSky\Framework\Model\Cf\CfFieldOption as CfFieldOptionModel;

class CheckBox extends BaseOption
{

    /**
     * @var boolean $supportsImporting
     * True if this object can be used in imports
     * */
    protected $supportsImporting = true;

    /**
     * @var boolean $supportsQueryBuilder
     * True if this object can be used in the query builder
     * */
    protected $supportsQueryBuilder = true;

    const FIELD_ERROR_MINIMUM_AMOUNT = 103;
    const FIELD_ERROR_MAXIMUM_AMOUNT = 104;

    public function minimumAmount()
    {
        $result = false;
        if (isset($this->configData['minimum_amount']) && !empty($this->configData['minimum_amount'])) {
            $result = $this->configData['minimum_amount'];
        }
        return $result;
    }

    public function maximumAmount()
    {
        $result = false;
        if (isset($this->configData['maximum_amount']) && !empty($this->configData['maximum_amount'])) {
            $result = $this->configData['maximum_amount'];
        }
        return $result;
    }

//    /**
//     * Check if an option specified is valid
//     * @access protected
//     * @param int $value - the ID of the option
//     * @return boolean
//     * */
//    protected function isValidOption($value)
//    {
//        $result = false;
//        if ($value) {
//            if (Strings::isJson($value)) {
//                $value = json_decode($value, true);
//            }
//            $options = $this->getOptions($this->definition->id);
//            $optionIds = Arrays::getFieldValuesFrom2dArray($options, 'id');
//            if ( (is_string($value) && !$result = in_array($value, $optionIds)) || (is_array($value) && array_intersect($value, $optionIds)) ) {
//                $optionValues = Arrays::getFieldValuesFrom2dArray($options, 'value');
//                $result = in_array($value, $optionValues) || (is_array($value) && array_intersect($value, $optionIds));
//            }
//        }
//        return $result;
//    }


    public function validate()
    {
        $result = parent::validate();

        $v = json_decode($this->getValue(), true);

        if($result && $this->minimumAmount() !== false && count($v) < $this->minimumAmount()) {
            $result = false;
            $this->error = self::FIELD_ERROR_MINIMUM_AMOUNT;
        }
        else if($result && $this->maximumAmount() !== false && count($v) > $this->maximumAmount()) {
            $result = false;
            $this->error = self::FIELD_ERROR_MAXIMUM_AMOUNT;
        }

        return $result;
    }

    /**
     * Get the error text
     * @access public
     * @return string
     * */
    public function getErrorText()
    {
        if($this->error == self::FIELD_ERROR_MINIMUM_AMOUNT) {
            $text = "Select an option";
        }
        elseif($this->error == self::FIELD_ERROR_MAXIMUM_AMOUNT) {
            $text = "Too many options selected";
        }
        else {
            $text = parent::getErrorText();
        }

        return $text;
    }

    // todo function needs to be deleted as base option fields use getOptions method
    public function getEditData($isPreview = false)
    {
        $this->getValue();
        $result = [];
        if ($this->configData && isset($this->configData['options'])) {
            if ($this->configData) {
                if (isset($this->configData['display_alphabetically']) && $this->configData['display_alphabetically'] == '1') {
                    natcasesort($this->configData['options']);
                }
                if (isset($this->configData['options_adv'])) {
//                    $options = $this->getOptions();
                    foreach ($this->configData['options_adv'] as $k => $v) {
//                foreach($options as $k => $v) {
                        $v['selected'] = ($v['value'] == $this->getValue());
                        $result[$k] = $v;
                    }
                } else {
                    foreach ($this->configData['options'] as $v) {
                        $val = $this->getValue();
                        if (is_string($val)) {
                            $val = json_decode($val, true);
                        }
                        $result[$v] = is_array($val) && array_search($v, $val) !== false;
                    }
                }
            }
        }
        return $result;
    }

    /**
     * Get the value to be saved
     * @access protected
     * @param CfValue $v
     * @return string
     * */
//    protected function setValueData(CfValue $v)
//    {
//        if(is_array($this->getValue())) {
//            $v->value_text = json_encode($this->getValue());
//        }
//        elseif(!Strings::isJson($this->getValue())) {
//            $str = $this->getValue();
//            if(!$str) {
//                $v->value_text = "";
//            }
//            else {
//                $v->value_text = json_encode($this->getValue());
//            }
//        }
//        elseif(is_scalar($this->getValue())) {
//            $v->value_text = $this->getValue();
//        }
//        return $this->getValue();
//    }

    /**
     * Set the value of this field
     * @access public
     * @param CfValue $value
     * */
    public function setValueFromObject(CfValue $value)
    {
        $this->value = json_decode($value->value_text, true);
    }

    public function getPdfExportData($data)
    {
        $config = json_decode($data['config'], true);
        $options = $this->getOptions($data['field_id']);
        $displayAlphabetically = (boolean) $config['display_alphabetically'];
        $allowCustomInput = (boolean) $config['custom_user_options'];

        if ($displayAlphabetically) {
            usort($options, function ($a, $b) {
                return strcmp($a['value'], $b['value']);
            });
        }

        $answers = [];
        $totalAnswers = 0;
        foreach ($options as $value) {
            $answers[$value['value']] = array('count' => 0);
        }

        if ($allowCustomInput) {
            $answers['other'] = [];
        }

        foreach ($data['answers'] as $valueArr) {
            $valueArr = (array) $valueArr;
            foreach ($valueArr as $value) {
                if (isset($answers[$value])) {
                    $answers[$value]['count']++;
                } elseif (isset($answers['other'][$value])) {
                    $answers['other'][$value]['count']++;
                } else {
                    $answers['other'][$value] = ['count' => 1];
                }
                $totalAnswers++;
            }
        }

        foreach ($answers as $key => $value) {
            if ($allowCustomInput && $key == 'other') {
                foreach ($value as $otherKey => $otherValue) {
                    $count = $otherValue['count'];
                    $answers[$key][$otherKey]['weighting'] = $totalAnswers > 0 ? ((float)$count / (float)$totalAnswers) : 0.0;
                }
            } else {
                $count = $value['count'];
                $answers[$key]['weighting'] = $totalAnswers > 0 ? ((float)$count / (float)$totalAnswers) : 0.0;
            }
        }

        $results = [];
        $results['results'] = $answers;
        $results['options'] = ['custom_user_options' => $allowCustomInput];

        return $results;
    }

    protected function getSelectionType()
    {
        return 'multi';
    }

}
