<?php
namespace BlueSky\Framework\Object\Data\Field;

use BlueSky\Framework\Object\Data\Field\Type\Base;

class Factory
{

    private static $map = [
        'checkbox' => 'BlueSky\Framework\Object\Data\Field\Type\CheckBox',
        'publicgroup' => 'BlueSky\Framework\Object\Data\Field\Type\PublicGroup'
    ];


    /**
     * Create an instance of field data
     * @param $state \BlueSky\Framework\State\Web
     * @param string $reference
     * @return Base
     * */
    public static function createInstance($state, $reference = "text")
    {
        $className = __NAMESPACE__ . '\Type\Base';
        $reference = str_replace(' ', '', ucwords(str_replace('_', ' ', $reference)));
        if ($reference) {
            if (isset(self::$map[strtolower($reference)])) {
                $referenceClass = self::$map[strtolower($reference)];
            } else {
                $referenceClass = __NAMESPACE__ . '\Type\\' . $reference;
            }
            if (class_exists($referenceClass)) {
                $className = $referenceClass;
            }
        }
        return new $className($state);
    }

    /**
     * Register a field type
     * @param string $reference
     * @param string $namespace
     * @access public static
     * @return void
     * */
    public static function registerField($reference, $namespace)
    {
        self::$map[$reference] = $namespace;
    }

}
