<?php
namespace BlueSky\Framework\Release;

use Doctrine\DBAL\Schema\Schema as DBALSchema;

class Schema
{

    /**
     * @param \Doctrine\DBAL\Schema\Table $table
     * @param boolean $hasPath
     * */
    protected function addLoteFields($table, $hasPath = false)
    {
        $table->addColumn('lote_access', 'integer', ['default' => 0, 'length' => 4, 'notnull' => false]);
        $table->addColumn('lote_created', 'datetime', ['notnull' => false]);
        $table->addColumn('lote_updated', 'datetime', ['notnull' => false]);
        $table->addColumn('lote_deleted', 'datetime', ['notnull' => false]);
        $table->addColumn('lote_deleted_by', 'integer', ['notnull' => false]);
        $table->addColumn('lote_author_id', 'integer', ['notnull' => false]);
        $table->addIndex(['lote_created']);
        $table->addIndex(['lote_deleted']);
        $table->addIndex(['lote_author_id']);
        if ($hasPath) {
            $table->addColumn('lote_path', 'text', ['notnull' => false]);
        }
    }

    /**
     * @param DBALSchema $schema
     * @param string $prefix
     * */
    protected function createCfValueTable($schema, $prefix){
        $table = $schema->createTable($prefix.'__cf_value');
        $table->addOption('engine' , 'MyISAM');
        $table->addColumn('id', "integer", ['autoincrement' => true]);
        $table->addColumn('object_id', "integer", ['notnull' => false]);
        $table->addColumn('field_id', "integer", ['notnull' => false]);
        $table->addColumn('value_string', "string", ['length' => 1500, 'notnull' => false]);
        $table->addColumn('value_number', "decimal", ['length' => '18,6', 'notnull' => false]);
        $table->addColumn('value_date', "datetime", ['notnull' => false]);
        $table->addColumn('value_text', "text", ['notnull' => false]);
        $table->addColumn('value_boolean', "boolean", ['length' => 1, 'notnull' => false]);
        $table->setPrimaryKey(['id']);
        $table->addIndex(['value_number'], 'IDX_CFV_VALUE_NUMBER_' . $prefix);
        $table->addIndex(['value_date'], 'IDX_CFV_VALUE_DATE_' . $prefix);
        $table->addIndex(['object_id'], 'IDX_CFV_VALUE_OBJECT_ID_' . $prefix);
        $table->addIndex(['field_id'], 'IDX_CFV_VALUE_FIELD_ID_' . $prefix);
        $table->addIndex(['field_id', 'value_date']);

        $table->addIndex(['value_string'], 'IDX_CFV_VALUE_STRING_' . strtoupper($prefix));
        $index = $table->getIndex('IDX_CFV_VALUE_STRING_' . strtoupper($prefix));
        $index->addFlag('fulltext');

        //$table->addIndex(['value_text'], 'IDX_CFV_VALUE_TEXT_' . strtoupper($prefix));
        //$index = $table->getIndex('IDX_CFV_VALUE_TEXT_' . strtoupper($prefix));
        //$index->addFlag('fulltext');

        $this->addLoteFields($table);

    }

    /**
     * @param DBALSchema $schema
     * @param string $prefix
     * */
    protected function createCfValueGroupTable($schema, $prefix){
        $table = $schema->createTable($prefix.'__cf_value_group');
        $table->addColumn('id', "integer", ['autoincrement' => true]);
        $table->addColumn('object_id', "integer", ['notnull' => false]);
        $table->addColumn('group_id', "integer", ['notnull' => false]);
        $table->addColumn('hidden', "boolean", ['length' => 1, 'default' => false, 'notnull' => false]);
        $table->addColumn('active', "boolean", ['length' => 1, 'default' => true, 'notnull' => false]);
        $table->addIndex(['object_id'], 'IDX_CFV_VALUE_OBJECT_ID_' . $prefix);
        $table->addIndex(['group_id'], 'IDX_CFV_VALUE_GROUP_ID_' . $prefix);
        $table->setPrimaryKey(['id']);
        $this->addLoteFields($table);
    }

    /**
     * @param DBALSchema $schema
     * @param string $prefix
     * */
    protected function createCfValueOptionTable($schema, $prefix){
        $table = $schema->createTable($prefix.'__cf_value_option_selected');
        $table->addColumn('id', "integer", ['autoincrement' => true]);
        $table->addColumn('value_id', "integer", ['notnull' => false]);
        $table->addColumn('option_id', "integer", ['notnull' => false]);
        $table->addColumn('option_id_col', "integer", ['notnull' => false]);
        $table->addColumn('value_string', "string", ['length' => 1500, 'notnull' => false]);
        $table->addColumn('value_string_col', "string", ['length' => 1500, 'notnull' => false]);
        $table->addColumn('value_number', "decimal", ['length' => '18,6', 'notnull' => false]);
        $table->addColumn('value_date', "datetime", ['notnull' => false]);
        $table->addColumn('value_text', "text", ['notnull' => false]);
        $table->addColumn('value_boolean', "boolean", ['length' => 1, 'notnull' => false]);
        $table->addIndex(['value_id']);
        $table->addIndex(['option_id']);
        $table->addIndex(['option_id_col']);
        $table->setPrimaryKey(['id']);
        $this->addLoteFields($table);
    }

}
