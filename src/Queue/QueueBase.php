<?php
declare(strict_types=1);

namespace BlueSky\Framework\Queue;

use Enqueue\Dbal\DbalConnectionFactory;
use Interop\Queue\Context;

class QueueBase
{
    protected Context $context;

    public function __construct(array $config)
    {
        $factory = new DbalConnectionFactory($this->getConnectionDetails($config));
        $this->context = $factory->createContext();
    }

    private function getConnectionDetails(array $dbConfig): array
    {
        $config = [];
        $config['connection'] = [
            'dbname' => $dbConfig['dbname'],
            'user' => $dbConfig['user'],
            'password' => $dbConfig['password'],
            'host' => $dbConfig['host'],
            'driver' => $dbConfig['driver'] ?? 'pdo_mysql',
            'charset' => $dbConfig['charset'] ?? 'utf8mb4',
            'port' => $dbConfig['port'] ?? 3306
        ];
        $config['table_name'] = 'sb__enqueue';
        $config['polling_interval'] = 1000;
        $config['lazy'] = true;
        return $config;
    }

    public function pingDb(): bool
    {
        $conn = $this->context->getDbalConnection();
        if (!$result = $conn->ping()) {
            $conn->close();
            $result = $conn->connect();
        }
        return $result;
    }
}
