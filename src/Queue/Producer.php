<?php
declare(strict_types=1);

namespace BlueSky\Framework\Queue;

use Enqueue\Dbal\DbalProducer as QueueProducer;
use Interop\Queue\Exception;

class Producer extends QueueBase
{
    private QueueProducer $producer;

    public function __construct(array $config)
    {
        parent::__construct($config);
        $this->producer = $this->context->createProducer();
    }

    public function add($queue, $class, $args = []): ?string
    {
        $result = null;
        try {
            $test = $this->context->createQueue($queue);
            $message = $this->context->createMessage($class, $args);
            $message->setHeader('queue_id', uniqid('', true));
            $this->producer->send($test, $message);
            $result = (string)$message->getHeader('queue_id');
        } catch (Exception $e) {

        }
        return $result;
    }
}
