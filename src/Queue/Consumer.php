<?php
declare(strict_types=1);
declare(ticks=1);

namespace BlueSky\Framework\Queue;

use BlueSky\Framework\Log\LoteLog;
use BlueSky\Framework\State\Cli;
use BlueSky\Framework\Util\Time;
use Doctrine\DBAL\DBALException;
use Interop\Queue\Consumer as InteropConsumer;
use Interop\Queue\Exception\SubscriptionConsumerNotSupportedException;
use Interop\Queue\Message;
use BlueSky\Framework\Registry\Config;
use BlueSky\Framework\Controller\Queue\Master as MasterQueueController;

class Consumer extends QueueBase
{
    private InteropConsumer $consumer;
    private LoteLog $logger;
    private const HISTORY_TABLE_NAME = 'sb__enqueue_history';
    private const CONSUMER_TABLE_NAME = 'sb__enqueue_consumer';
    private const LOG_STATUS_SUCCESS = 'success';
    private const LOG_STATUS_PROCESSING = 'processing';
    private const LOG_STATUS_FAIL = 'rejected';
    private const LOG_STATUS_FAIL_MESSAGE_CLASS_NOT_FOUND = 'invalid class';
    private const CONSUMER_STATUS_RUNNING = 'running';
    private const CONSUMER_STATUS_TERMINATED = 'terminated';
    private int $consumerRegistrationId = 0;

    public function __construct(Config $config, $queueName)
    {
        parent::__construct($config->get('queue.enqueue'));
        $this->logger = new LoteLog('master', $config->get('log.master'), LOTE_LOG_MASTER , 'enqueue.txt', 'enqueue');
        $this->consumer = $this->context->createConsumer($this->context->createTopic($queueName));
        $this->registerConsumer($this->consumer, gethostname(), (int)getmypid());
        if (function_exists('pcntl_signal')) {
            pcntl_async_signals(true);
            pcntl_signal( SIGUSR1, function () {
                $this->registerShutdown();
                die;
            });
            pcntl_signal( SIGHUP, function () {
                $this->registerShutdown();
                die;
            });
            pcntl_signal( SIGINT, function () {
                $this->registerShutdown();
                die;
            });
        }
    }

    public function registerShutdown()
    {
        $data = [];
        $data['status'] = self::CONSUMER_STATUS_TERMINATED;
        $data['is_terminated'] = true;
        $data['terminated_at'] = Time::getUtcNow();
        $this->context->getDbalConnection()->update(self::CONSUMER_TABLE_NAME, $data, ['id' => $this->consumerRegistrationId]);
    }

    public function registerActivity()
    {
        $data = [];
        $data['active_at'] = Time::getUtcNow();
        $this->context->getDbalConnection()->update(self::CONSUMER_TABLE_NAME, $data, ['id' => $this->consumerRegistrationId]);
    }

    private function registerConsumer(InteropConsumer $consumer, string $hostname, int $pid): void
    {
        $data = [];
        $data['hostname'] = $hostname;
        $data['pid'] = $pid;
        $data['name'] = $consumer->getQueue()->getQueueName();
        $data['status'] = self::CONSUMER_STATUS_RUNNING;
        $data['created_at'] = Time::getUtcNow();
        $data['active_at'] = Time::getUtcNow();
        $data['is_terminated'] = false;
        $this->context->getDbalConnection()->insert(self::CONSUMER_TABLE_NAME, $data);
        $this->consumerRegistrationId = (int)$this->context->getDbalConnection()->lastInsertId();
        echo("Started, with PID=" . getmypid()."\n\n");
    }

    public function run(): void
    {
        try {
            $subscriptionConsumer = $this->context->createSubscriptionConsumer();
            $subscriptionConsumer->subscribe($this->consumer, function (Message $message, InteropConsumer $consumer) {
                $historyId = 0;
                try {
                    $class = '\\' . trim($message->getBody(), '\\');
                    $consumer->acknowledge($message);
                    /** @var MasterQueueController $object */
                    $this->pingDb();
                    $historyId = $this->logToDb($historyId, $message, self::LOG_STATUS_PROCESSING);
                    $properties = $message->getProperties();
                    $cli = null;
                    if(isset($properties['reference'])) {
                        $cli = new Cli($properties['reference']);
                        $cli->getApps();
                    }
                    if (class_exists($class)) {
                        $object = new $class();
                        $object->process($message->getProperties(), $message->getMessageId());
                        $this->pingDb();
                        $this->logToDb($historyId, $message, self::LOG_STATUS_SUCCESS);
                        $this->logger->debug($message->getQueue() . "-completed processing", ['queue' => $message->getQueue(), 'status' => self::LOG_STATUS_SUCCESS, 'data' => $message->getProperties()]);
                    } else {
                        $this->pingDb();
                        $this->logger->error($message->getQueue() . '-failed to find class', ['queue' => $message->getQueue(), 'status' => self::LOG_STATUS_FAIL, 'message' => self::LOG_STATUS_FAIL_MESSAGE_CLASS_NOT_FOUND]);
                        $this->logToDb($historyId, $message, self::LOG_STATUS_FAIL, self::LOG_STATUS_FAIL_MESSAGE_CLASS_NOT_FOUND);
                    }
                } catch (\Exception $e) {
                    $this->pingDb();
                    $this->logger->error($message->getQueue() . "-failed to process", ['queue' => $message->getQueue(), 'status' => self::LOG_STATUS_FAIL, 'message' => $e->getMessage()]);
                    $this->logToDb($historyId, $message, self::LOG_STATUS_FAIL, $e->getMessage());
                }
                return true;
            });
            $subscriptionConsumer->consume();
        } catch (SubscriptionConsumerNotSupportedException $e) {
            dump($e->getMessage());
        }
    }

    private function logToDb(int $historyLogId, Message $message, $status = 'success', $statusMessage = 'ok'): int
    {
        $result = 0;
        try {
            $data = [];
            $data['original_id'] = $message->getMessageId();
            $data['delivery_id'] = $message->getDeliveryId();
            $data['published_at'] = $message->getPublishedAt();
            $data['body'] = $message->getBody();
            $data['headers'] = json_encode($message->getHeaders(), JSON_THROW_ON_ERROR, 512);
            $data['properties'] = json_encode($message->getProperties(), JSON_THROW_ON_ERROR, 512);
            $data['redelivered'] = $message->isRedelivered();
            $data['queue'] = $message->getQueue();
            $data['priority'] = $message->getPriority();
            $data['delayed_until'] = $message->getDeliveryDelay();
            $data['time_to_live'] = $message->getTimeToLive();
            $data['redeliver_after'] = $message->getRedeliverAfter();
            $data['processed_at'] = Time::getUtcNow();
            $data['status'] = $status;
            $data['status_message'] = $statusMessage;
            if ($historyLogId) {
                $this->context->getDbalConnection()->update(self::HISTORY_TABLE_NAME, $data, ['id' => $historyLogId]);
            } else {
                $this->context->getDbalConnection()->insert(self::HISTORY_TABLE_NAME, $data);
                $result = (int)$this->context->getDbalConnection()->lastInsertId();
            }
        } catch (DBALException $e) {
            $this->logger->error("Could not log outcome: " . $e->getMessage(), ['data' => $data]);
        }
        return $result;
    }
}
