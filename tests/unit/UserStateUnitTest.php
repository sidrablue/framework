<?php

use BlueSky\Framework\Test\BaseStateUnitTest;

class UserStateUnitTest extends BaseStateUnitTest {

    function testUserNameWithDb()
    {
        $results = $this->getState()->getReadDb()->createQueryBuilder()
            ->select('*')
            ->from('sb__user')
            ->andWhere('lote_deleted is null')
            ->andWhere('username = :username')
            ->setParameter('username', 'admin')
            ->execute()->fetchAll(\PDO::FETCH_ASSOC);

        $this->assertEquals(1, count($results), 'username is unique, only a single item should be returned');

        $item = $results[0];

        $this->assertArrayHasKey('first_name', $item);
        $this->assertNotEmpty($item['first_name'], 'First name not empty test');

        $this->assertEquals('Amel', $item['first_name']);
    }

}